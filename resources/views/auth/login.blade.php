@extends('layouts.layout')
@section('content')
    <section id="main">
        <div class="login-container">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3">
                <div class="card p-4">
                    <div class="row">
                        <div class="col-12 col-md-9">
                            <h1>Welcome to <span class="color-orange">Vivo</span><span class="color-blue">Doc</span></h1>
                            <p>Let's help you stay on top of your health</p>
                        </div>
                        <div class="col-12 col-md-3 d-none d-md-block">
                            <img src="{{asset('web/v2/img/login-img.svg')}}" width="100%" alt="VivoDoc Login">
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label for="" class="py-2">User Name</label>
                                <input id="email" type="text" class="form-control @error('username') is-invalid @enderror"
                                       name="username" value="{{ old('username') }}" required autocomplete="username" autofocus placeholder="Enter your registered email id / mobile no">
                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group pt-2">
                                <label for="" class="py-2">Password</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                       name="password" required placeholder="Enter your password" autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-check pt-2">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"
                                           name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                            <div class="row pt-4">
                                <button type="submit" class="btn btn-full btn btn-read-more col-12">
                                    {{ __('Login') }}
                                </button>
                            </div>
                            <div class="row py-4">
                                <div class="col-6 text-right">
                                <a href="{{url('password-reset')}}">{{__('Forget Your Password?')}}</a>
                            </div>
                            </div>
                        </form>
{{--                        <div class="row py-4">--}}
{{--                            <div class="col-12 text-center">--}}
{{--                                <span>OR</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row social-login-btns">--}}
{{--                            <div class="col-12 col-md-4">--}}
{{--                                <button type="button" class="btn google-btn">--}}
{{--                                    <img src="{{asset('web/v2/img/google-login.svg')}}" alt="Google login">--}}
{{--                                    <span >Google</span>--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                            <div class="col-12 col-md-4">--}}
{{--                                <button type="button" class="btn fb-btn">--}}
{{--                                    <img src="{{asset('web/v2/img/facebook-login.svg')}}" alt="Facebook">--}}
{{--                                    <span >Facebook</span>--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                            <div class="col-12 col-md-4">--}}
{{--                                <button type="button" class="btn apple-btn">--}}
{{--                                    <img src="{{asset('web/v2/img/apple-login.svg')}}" alt="Apple">--}}
{{--                                    <span >Apple</span>--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    </section>
@endsection
