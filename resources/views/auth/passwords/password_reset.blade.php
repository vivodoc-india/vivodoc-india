@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">{{ __('Reset Password') }}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid cp-lg bg-light">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                <div class="card rounded-0">
                    <div class="card-header bg-white text-center">
                        <img src="{{asset('web/v2/img/logo/logo.svg')}}" width="150">
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 mb-1">
                                <a href="{{url('password/reset-via-otp')}}" class="font-weight-bold"> <i class="fas fa-sms"></i> Reset via OTP

                                </a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <a href="{{ route('password.request') }}" class="font-weight-bold">
                                    <i class="fas fa-envelope"></i>  Reset via Email
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
