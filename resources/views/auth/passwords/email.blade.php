
@extends('layouts.layout')
@section('content')
<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">{{ __('Reset Password') }}</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid cp-lg bg-light">
	<div class="row justify-content-center">
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
			<div class="card rounded-0">
				<div class="card-header bg-white text-center">
					<img src="{{asset('web/v2/img/logo/logo.svg')}}" width="150">
				</div>
				<div class="card-body">
@if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
 <form method="POST" action="{{ route('password.email') }}">
                        @csrf
	<div class="form-group">
                          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter your registered email id">
                           @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>
 <button type="submit" class="btn btn-primary rounded-pill btn-block">
                                     {{ __('Send Password Reset Link') }}
                                </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
