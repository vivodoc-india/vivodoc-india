@extends('layouts.layout')
@section('content')
<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">{{ __('Reset Password') }}</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid cp-lg bg-light">
	<div class="row justify-content-center">
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
			<div class="card rounded-0">
				<div class="card-header bg-white text-center">
					<img src="{{asset('web/v2/img/logo/logo.svg')}}" width="150">
				</div>
				<div class="card-body">
                      <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

<div class="form-group">

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="Enter your registered email id">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>

                        <div class="form-group ">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="enter new password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group ">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="confirm password">

                        </div>

<button type="submit" class="btn btn-primary rounded-pill btn-block">
                                     {{ __('Reset Password') }}
                                </button>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
