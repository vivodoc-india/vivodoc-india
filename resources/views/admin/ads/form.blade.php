{{ ! list(, $action) =explode('@', Route::getCurrentRoute()->getActionName()) }}
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5>Ads</h5>
                </div>
                <div class="widget-content nopadding">
                        {{csrf_field()}}
                        <div class="control-group">
                            <label class="control-label">Title<spam> * </spam>:</label>
                            <div class="controls">
                                {{ Form::text('title',null,['required'=>'""'])}}
                                @if ($errors->has('title'))
                                    <span class="form_error">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Link:</label>
                            <div class="controls">
                                {{ Form::text('link',null)}}
                                @if ($errors->has('link'))
                                    <span class="form_error">
                                            <strong>{{ $errors->first('link') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Sort Order:</label>
                            <div class="controls">
                                {{ Form::number('sort_by',null)}}
                                @if ($errors->has('sort_by'))
                                    <span class="form_error">
                                                <strong>{{ $errors->first('sort_by') }}</strong>
                                            </span>
                                @endif
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Image :</label>
                            <div class="controls">
                                <div class="uploader" id="uniform-undefined">
                                    {{Form::file('image',array('size'=>'19','style'=>'opacity: 0;'))}}
                                    <span class="filename">No file selected</span><span class="action">Choose File
                                    </span></div>
                                @if(isset($page->image) && File::exists('media/ads/'.$page->image))
                                    <img src="{{asset('media/ads/'.$page->image)}}" style="width:100px;"/>
                                @endif
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Status :</label>
                            <div class="controls">
                                <label><div class="radio" id="uniform-undefined"><span>{{Form::radio('status','1',true,['style'=>'opacity: 0;'])}}</span></div>Active</label>
                                <label><div class="radio" id="uniform-undefined"><span>{{Form::radio('status','0',false,['style'=>'opacity: 0;'])}}</span></div>Idle</label>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" name="save" value="yes" class="btn btn-success">
                                @if($action=='create') Save
                                @else Update
                                @endif
                            </button>
                            <a href="{{url('page')}}" type="button" class="btn btn-danger">Cancel</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>