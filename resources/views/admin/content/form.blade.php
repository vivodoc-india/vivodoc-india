{{! $param = explode('.',Route::currentRouteName())}}
<?php
if ($param[1] == 'edit') {
    $method = 'PATCH';
    $function = 'update';
    $button = 'Update';
} else {
    $method = 'POST';
    $function = 'store';
    $button = 'Submit';
}
?>
@extends('layouts.admin.home')
@section('content')
<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{url('admin/'.$param[0].'?type='.$content_category->title)}}">{{ucfirst($param[0])}}</a></li>
                    <li class="breadcrumb-item active">{{ucfirst($param[1])}}</li>
                </ul>
                <h4 class="page-title"> {{ucfirst($param[0])}} Management</h4>
            </div>
        </div>
    </div>
    <!-- end page title end breadcrumb -->

</div>

</div>
<div class="wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12 mx-auto">
                <div class="card m-b-20">
                    <div class="card-body">

                        <!--<h4 class="mt-0 header-title">Password</h4>-->
                        {{ Form::model($data,['method'=>$method,'url'=>$action,'id'=>'form_validation','files'=>true]) }}
                        <div class="row clearfix">
                            <input type="hidden" name="content_category_id" value="{{$content_category->id}}" />
                            <div class="col-sm-6">
                                <label for="email_address">Name</label>
                                <div class="form-group">
                                    {{Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter name', ($content_category->title!='content' || Auth::user()->user_role=='developer')?'required':'readonly'])}}
                                    @if ($errors->has('name'))<span class="error"> {{ $errors->first('name') }}</span>@endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="email_address">Title</label>
                                <div class="form-group">
                                    {{Form::text('meta_title',null,['class'=>'form-control text-capitalize','placeholder'=>'Enter title','required'])}}
                                    @if ($errors->has('meta_title'))<span class="error"> {{ $errors->first('meta_title') }}</span>@endif
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label for="email_address">Image</label>
                                <div class="form-group">
                                    <input type="file" class="form-control" name="file" id="exampleInputFile" aria-describedby="fileHelp" accept="image/*">
                                    @if(isset($data->file))<p><a href="{{asset('public/uploads/page/'.$data->file)}}">View File</a></p>@endif
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea name="content" id="editor">{!! $data->content !!}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label for="email_address">Meta Keywords</label>
                                <div class="form-group">
                                    {{Form::text('meta_keywords',null,['class'=>'form-control','placeholder'=>'Enter meta keywords'])}}
                                    @if ($errors->has('meta_keywords'))<span class="error"> {{ $errors->first('meta_keywords') }}</span>@endif
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label for="email_address">Meta Description</label>
                                <div class="form-group">
                                    {{Form::text('meta_description',null,['class'=>'form-control','placeholder'=>'Enter meta description'])}}
                                    @if ($errors->has('meta_description'))<span class="error"> {{ $errors->first('meta_description') }}</span>@endif
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label for="email_address">Sort Order</label>
                                <div class="form-group">
                                    {{Form::text('sort_order','0',['class'=>'form-control','placeholder'=>'Enter sort order'])}}
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label for="email_address">Status</label>
                                <div class="form-group">
                                    {{Form::select('status',['1'=>'Active','0'=>'Idle'],null,['class'=>'form-control show-tick'])}}
                                </div>
                            </div>
                        </div>

                        <div class="form-group m-b-0 text-right">
                            <div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light">
                                    {{$button}}
                                </button>
                                <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                    Cancel
                                </button>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->


    </div> <!-- end container -->
</div>
@endsection
@section('footer')
<!--<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
CKEDITOR.replace('editor');
</script>-->
<script>
    var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{asset('public/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('public/ckeditor/adapters/jquery.js')}}"></script>
<script>
    $('textarea[name=content]').ckeditor({
        height: 300,
        filebrowserImageBrowseUrl: route_prefix + '?type=Images',
        filebrowserBrowseUrl: route_prefix + '?type=Files',
    });
</script>
@endsection