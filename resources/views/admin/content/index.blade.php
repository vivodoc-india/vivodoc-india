@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="#">Content Management</a> <span class="bread-slash"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!-- Static Table Start -->
    <div class="data-table-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline13-list shadow-reset">
                        <div class="sparkline13-hd">
                            <div class="main-sparkline13-hd">
                                <h1>All Content <span class="table-project-n">Data</span> </h1>
                                <div class="sparkline13-outline-icon">
                                    <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{-- <span><i class="fa fa-wrench"></i></span> --}}
                                    {{-- <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span> --}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline13-graph">
                            <div class="datatable-dashv1-list custom-datatable-overright">
                                <div id="toolbar">
                                    <select class="form-control">
                                        <option value="">Export Basic</option>
                                        <option value="all">Export All</option>
                                        <option value="selected">Export Selected</option>
                                    </select>
                                </div>
                                <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                    <tr>
                                        <th data-field="state" data-checkbox="true"></th>
                                        <th data-field="id">S.No.</th>
                                        <th data-field="name">Title</th>
                                        <th data-field="mobile">Sort Order</th>
                                        <th data-field="email">Image</th>
                                        <th data-field="user_role">Status</th>

                                        <th data-field="action">Action</th>
                                        {{--                                    <th data-field="edit">Edit</th>--}}
                                        {{--                                   <th data-field="delete">Remove</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $num = 1; ?>
                                    @foreach($list as $data)
                                        <tr>
                                            <td>{{$num}}</td>
                                            <td>{{$data->meta_title}}</td>
                                            <td>{{$data->sort_order}}</td>
                                            <td>@if($data->file && \File::exists(public_path('uploads/'.$content_category->title.'/'.$data->file)))<a href="{{asset('public/uploads/'.$content_category->title.'/'.$data->file)}}" target="__blank"><img src="{{asset('public/uploads/'.$content_category->title.'/'.$data->file)}}" width="50px;"/></a> @endif</td>
                                            <td><a href="javascript:;" title="Change Status" onclick="changeFlag(this,'contents','status',{{$data->id}});" class="badge @if ($data->status==1) {{'badge-success'}} @else {{'badge-danger'}} @endif btn-mini">@if ($data->status==1) {{'Active'}} @else {{'Idle'}} @endif</a></td>
                                            <td>
                                                <a href="{{action('Admin\ContentController@edit',$data->id)}}" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                                                @if ($content_category->title!='content' || Auth::user()->user_role=='developer')
                                                    <a href="javascript:void(0)" title="Delete" data-url="{{route($param[0].'.destroy',$data->id)}}" data-id="{{$data->id}}" data-toggle="modal" data-target="#delete_button_modal" class="delete_button_action" ><i class="fa fa-trash-o text-danger"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        <?php $num++ ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Static Table End -->
@endsection
