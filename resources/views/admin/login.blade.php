<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login | Adminpro - Admin Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.ico')}}">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <!-- adminpro icon CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/adminpro-custon-icon.css')}}">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/meanmenu.min.css')}}">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/jquery.mCustomScrollbar.min.css')}}">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <!-- form CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/form.css')}}">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('css/Lobibox.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/notifications.css')}}">
    <!-- modernizr JS
		============================================ -->
    <script src="{{asset('js/vendor/modernizr-2.8.3.min.js')}}"></script>
</head>

<body class="materialdesign">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Header top area start-->
<div class="wrapper-pro">
    <!-- Header top area start-->
    <div class="content-inner-all">
        <!-- login Start-->
        <div class="login-form-area mg-t-30 mg-b-40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <form id="adminpro-form" class="adminpro-form" action="{{ route('admin-login') }}" method="POST">
                        @csrf
                        <div class="col-lg-4">
                            <div class="login-bg">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="logo">
                                            <a href="#"><img src="{{asset('web/v2/img/logo/logo.svg')}}" alt="" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="login-title">
                                            <h1>Login Form</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="login-input-head">
                                            <p>E-mail</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="login-input-area">
                                            <input type="email" name="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required autofocus/>
                                            <i class="fa fa-envelope login-user" aria-hidden="true"></i>
                                        </div>
                                        @if ($errors->has('email'))
                                                            <span class="invalid-feedback form_error" role="alert">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="login-input-head">
                                            <p>Password</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="login-input-area">
                                            <input type="password" class=" {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required id="password" />
                                            <i class="fa fa-lock login-user"></i>
                                        </div>
                                        @if ($errors->has('password'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                        @endif
{{--                                        <div class="row">--}}
{{--                                            <div class="col-lg-12">--}}
{{--                                                <div class="forgot-password">--}}
{{--                                                    <a href="{{ route('password.request') }}">Forgot password?</a>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="login-keep-me">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}><i></i>{{ __('Remember Me') }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">

                                    </div>
                                    <div class="col-lg-8">
                                        <div class="login-button-pro">
{{--                                            <button type="submit" class="login-button login-button-rg">Register</button>--}}
                                            <button type="submit" class="login-button login-button-lg">Log in</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-lg-4"></div>
                </div>
            </div>
        </div>
        <!-- login End-->
    </div>
</div>
<!-- Footer Start-->
<div class="footer-copyright-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer-copy-right">
                    <p>Copyright &#169; {{date('Y')}} AAHPL All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer End-->
<!-- jquery
    ============================================ -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js" integrity="sha512-ju6u+4bPX50JQmgU97YOGAXmRMrD9as4LE05PdC3qycsGQmjGlfm041azyB1VfCXpkpt1i9gqXCT6XuxhBJtKg==" crossorigin="anonymous"></script>
{{--<script src="{{asset('js/vendor/jquery-1.11.3.min.js')}}"></script>--}}
<!-- bootstrap JS
    ============================================ -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- meanmenu JS
    ============================================ -->
<script src="{{asset('js/jquery.meanmenu.js')}}"></script>
<!-- mCustomScrollbar JS
    ============================================ -->
<script src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- sticky JS
    ============================================ -->
<script src="{{asset('js/jquery.sticky.js')}}"></script>
<!-- scrollUp JS
    ============================================ -->
<script src="{{asset('js/jquery.scrollUp.min.js')}}"></script>
<!-- form validate JS
    ============================================ -->
<script src="{{asset('js/jquery.form.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/form-active.js')}}"></script>
<!-- main JS
    ============================================ -->
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/Lobibox.js')}}"></script>
<script src="{{asset('js/notification-active.js')}}"></script>
@if ($message = Session::get('success'))
    <script>
        Lobibox.notify('success', {
            delay: 15000,
            sound:true,
            msg: '{{ $message }}'
        });

    </script>
@endif
@if ($message = Session::get('error'))
    <script>
        Lobibox.notify('error', {
            delay: 15000,
            sound:true,
            msg: '{{ $message }}'
        });

    </script>
@endif
@if ($message = Session::get('info'))
    <script>
        Lobibox.notify('info', {
            delay: 15000,
            sound:true,
            msg: '{{ $message }}'
        });

    </script>
@endif
</body>

</html>


{{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}

{{--<head>--}}
{{--<title>{{$title}}</title><meta charset="UTF-8" />--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1.0" />--}}
{{--     <link rel="icon" type="image/png" href="{{asset('web/v2/img/logo/logo.svg')}}">--}}
{{--    <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}" />--}}
{{--    <link rel="stylesheet" href="{{asset('admin/css/bootstrap-responsive.min.css')}}" />--}}
{{--    <link rel="stylesheet" href="{{asset('admin/css/matrix-login.css')}}" />--}}
{{--    <link href="{{asset('admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />--}}
{{--    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>--}}
{{--</head>--}}
{{--<body>--}}
{{--<div id="loginbox">--}}
{{--    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">--}}
{{--        --}}{{-- <form method="POST" action="" aria-label=""> --}}
{{--        @csrf--}}
{{--    <div class="control-group normal_text"><img style="width:200px;" src="{{asset('admin/img/logo.png')}}" alt="Logo" /></div>--}}

{{--    <div class="control-group">--}}
{{--        <div class="controls">--}}
{{--            <div class="main_input_box">--}}
{{--                <span class="add-on bg_lg"><i class="icon-user"> </i></span>--}}
{{--                <input id="email" type="email" class=" {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>--}}

{{--                @if ($errors->has('email'))--}}
{{--                    <span class="invalid-feedback form_error" role="alert">--}}
{{--                        <strong>{{ $errors->first('email') }}</strong>--}}
{{--                    </span>--}}
{{--                @endif--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="control-group">--}}
{{--        <div class="controls">--}}
{{--            <div class="main_input_box">--}}
{{--                <span class="add-on bg_ly"><i class="icon-lock"></i></span>--}}
{{--     <input id="password" type="password" class=" {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

{{--                @if ($errors->has('password'))--}}
{{--                    <span class="invalid-feedback" role="alert">--}}
{{--                        <strong>{{ $errors->first('password') }}</strong>--}}
{{--                    </span>--}}
{{--                @endif--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="controls pull-right " style="color:white;margin-right: 17px;">--}}
{{--        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}--}}
{{--    </div>--}}

{{--    <div class="form-actions">--}}
{{--        <span class="pull-left">--}}
{{--            <a class="btn btn-link" href="">--}}
{{--                --}}{{-- <a class="btn btn-link" href="{{ route('password.request') }}"> --}}
{{--                    {{ __('Forgot Your Password?') }}--}}
{{--                </a>--}}
{{--        </span>--}}
{{--        <span class="pull-right">--}}
{{--             <button type="submit" class="btn btn-primary">--}}
{{--                    {{ __('Login') }}--}}
{{--                </button>--}}

{{--        </span>--}}
{{--    </div>--}}
{{--	 <div class="form-actions text-center " style="color:white;">--}}
{{--        <span>Design & Development by--}}
{{--                <a class="btn btn-link" target="__blank" href="http://www.friendzitsolutions.biz/">--}}
{{--                    Friendz IT Solutions--}}
{{--                </a>--}}
{{--        </span>--}}
{{--    </div>--}}
{{--    </form>--}}

{{--</div>--}}

{{--<script src="{{asset('js/jquery.min.js')}}"></script>--}}
{{--<script src="{{asset('js/matrix.login.js')}}"></script>--}}
{{--</body>--}}

{{--</html>--}}

