{{! $param = explode('.',Route::currentRouteName())}}
<?php
if ($param[1] == 'edit') {
$method = 'PATCH';
$function = 'update';
$button = 'Update';
} else {
$method = 'POST';
$function = 'store';
$button = 'Submit';
}
?>
@extends('layouts.master')
@section('breadcumb')
<!-- Breadcome start-->
<div class="breadcome-area mg-b-30 small-dn">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                <ul class="breadcome-menu">
                    <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                    </li>
                    <li><a href="#">Content Management</a> <span class="bread-slash"></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('content')
<div class="basic-form-area mg-b-15">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="sparkline12-list shadow-reset mg-t-30">
                <div class="sparkline12-hd">
                    <div class="main-sparkline12-hd">
                        <h1>@if($param[1] == 'edit'){{'Update'}}@else{{'Create'}} @endif Media</h1>
                        <div class="sparkline12-outline-icon">
                            <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                            <span><i class="fa fa-wrench"></i></span>
                            <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>
                        </div>
                    </div>
                </div>
                <div class="sparkline12-graph">
                    <div class="basic-login-form-ad">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="all-form-element-inner">
{{--                                    {{ Form::model($data,['method'=>$method,'url'=>$action,'id'=>'form_validation','files'=>true]) }}--}}
    <form action="{{$action}}" method="post" id="form_validation" enctype="multipart/form-data">
        @csrf
@method($method)
                                        <input type="hidden" name="media_category_id" value="{{$media_category->id}}" />

                                    <div class="form-group-inner" style="margin-bottom: 10px;">
                                        <div class="row" >
                                            <div class="col-lg-3">
                                                <label class="login2 pull-right pull-right-pro">Title</label>
                                            </div>
                                            <div class="col-lg-6">
                                                <input type="text" name="title" class="form-control text-capitalize" placeholder="Enter title" required value="{{isset($data)?$data->title:''}}">
{{--                                                {{Form::text('title',null,['class'=>'form-control text-capitalize','placeholder'=>'Enter title','required'])}}--}}
                                                @if ($errors->has('title'))<span class="error"> {{ $errors->first('title') }}</span>@endif
                                            </div>
                                        </div>
                                    </div>

                                                <div class="form-group-inner">
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
            <label class="login2 pull-right pull-right-pro">Image :</label>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="file-upload-inner file-upload-inner-right ts-forms">
                                                                <div class="input append-big-btn">
                                                                    <label class="icon-left" for="append-big-btn">
                                                                        <i class="fa fa-download"></i>
                                                                    </label>
                                                                    <div class="file-button">
                                                                        Browse
                                                                        <input type="file" name="file" onchange="document.getElementById('append-big-btn').value = this.value;">
                                            @if ($errors->has('file'))<span class="error"> {{ $errors->first('file') }}</span>@endif
                                                                    </div>
                                                                    <input type="text" id="append-big-btn" placeholder="no file selected">
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    @if($data->id==1)
                                                        <span>(Note: Allow Type: jpg/png, Max Size: 500Kb, width: 370px, Height: 570px) &nbsp;@if(isset($data->file))<a href="{{asset('uploads/'.$media_category->title.'/'.$data->file)}}" target="_blank">View File</a>@endif</span>
                                                    @elseif($data->id==2)
                                                        <span>(Note: Allow Type: jpg/png, Max Size: 500Kb, width: 285px, Height: 235px) &nbsp;@if(isset($data->file))<a href="{{asset('uploads/'.$media_category->title.'/'.$data->file)}}" target="_blank">View File</a>@endif</span>
                                                    @elseif($data->id==3)
                                                        <span>(Note: Allow Type: jpg/png, Max Size: 500Kb, width: 285px, Height: 235px) &nbsp;@if(isset($data->file))<a href="{{asset('uploads/'.$media_category->title.'/'.$data->file)}}" target="_blank">View File</a>@endif</span>
                                                    @elseif($media_category->title=='banner')
                                                        <span>(Note: Allow Type: jpg/png, Max Size: 500Kb, width: 1970px, Height: 670px) &nbsp;@if(isset($data->file))<a href="{{asset('uploads/'.$media_category->title.'/'.$data->file)}}" target="_blank">View File</a>@endif</span>
                                                    @elseif($media_category->title=='app_banner')
                                                        <span>(Note: Allow Type: jpg/png, Max Size: 500Kb, width: 800px, Height: 350px) &nbsp;@if(isset($data->file))<a href="{{asset('uploads/'.$media_category->title.'/'.$data->file)}}" target="_blank">View File</a>@endif</span>
                                                    @endif

                                                </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Status</label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-select-list">
                                                            <select name="status" class="form-control custom-select" id="cat-parent-id">
                                                                <option value="1" {{isset($data) && $data->status==1?'selected':''}}>Active</option>
                                                                <option value="0" {{isset($data) && $data->status==0?'selected':''}}>Idle</option>
                                                            </select>
{{--                                                {{Form::select('status',['1'=>'Active','0'=>'Idle'],null,['class'=>'form-control custom-select ','id'=>'cat-parent-id'])}}--}}
                                            </div>
                                        </div>
                                                </div>
                                            </div>

                                    <div class="form-group-inner">
                                        <div class="row" >
                                            <div class="col-lg-3">
                                                <label class="login2 pull-right pull-right-pro">Description</label>
                                            </div>
                                            <div class="col-lg-6">
                                                <textarea name ="description" class="form-control description" placeholder="enter Description" required>
                                                    {{isset($data)?$data->description:''}}
                                                </textarea>
{{--                                                {{Form::textarea('description',null,['class'=>'form-control text-capitalize description','placeholder'=>'Enter description','required'])}}--}}
                                                @if ($errors->has('description'))<span class="error"> {{ $errors->first('title') }}</span>@endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group-inner">
                                        <div class="login-btn-inner">
                                            <div class="row">
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-6">
                                                    <div class="login-horizental cancel-wp pull-left">
                                                        <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>
    <button class="btn btn-sm btn-primary login-submit-cs" type="submit">{{$button}}
                                                            </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('additional_script')
<script src="https://cdn.ckeditor.com/ckeditor5/18.0.0/classic/ckeditor.js"></script>
 <script>
ClassicEditor
    .create( document.querySelector( '.description' ) )
    .catch( error => {
        console.error( error );
    } );
</script>
@endsection
