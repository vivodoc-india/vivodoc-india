@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="#">Content Management</a> <span class="bread-slash"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!-- Static Table Start -->
    <div class="data-table-area mg-b-15">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <div style="margin-bottom: 10px; padding-bottom:10px;">
                        <button title="Add New" onclick="window.location.href ='{{url('omed/media/create?type='.$_GET['type'])}}'" class="btn btn-primary add-button" type="button" style="float:right;">
                            <i class="fa fa-plus"></i>
                        </button> <br>
                    </div>

                    <div class="sparkline13-list shadow-reset">
                        <div class="sparkline13-hd">
                            <div class="main-sparkline13-hd">
                                <h1>All Media <span class="table-project-n">Data</span> </h1>
                                <div class="sparkline13-outline-icon">
                                    <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>

                                </div>
                            </div>
                        </div>
                        <div class="sparkline13-graph">
                            <div class="datatable-dashv1-list custom-datatable-overright">
                                <div id="toolbar">
                                    <select class="form-control">
                                        <option value="">Export Basic</option>
                                        <option value="all">Export All</option>
                                        <option value="selected">Export Selected</option>
                                    </select>
                                </div>
                                <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                    <tr>
                                        <th data-field="state" data-checkbox="true"></th>
                                        <th data-field="id">S.No.</th>
                                        <th data-field="name">Title</th>
                                        <th data-field="image">Image</th>
                                        <th data-field="user_role">Status</th>

                                        <th data-field="action">Action</th>
                                        {{--                                    <th data-field="edit">Edit</th>--}}
                                        {{--                                   <th data-field="delete">Remove</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $num = 1 @endphp
                                    @foreach($list as $data)
                                        <tr>
                                            <td></td>
                                            <td>{{$num}}</td>
                                            <td>{{$data->title}}</td>

                                            <td>@if($data->file && \File::exists(public_path('uploads/'.$media_category->title.'/'.$data->file)))<a href="{{asset('uploads/'.$media_category->title.'/'.$data->file)}}" target="__blank"><img src="{{asset('uploads/'.$media_category->title.'/'.$data->file)}}" width="50px;"/></a> @endif</td>
                                            <td>
                                                  <a href="javascript:;" onclick="changeFlag(this,'media','status',{{$data->id}});" class="btn @if ($data->status==1) {{'btn-success'}} @else {{'btn-danger'}} @endif btn-sm">@if ($data->status==1) {{'Active'}} @else {{'Idle'}} @endif</a>

                                                {{-- <a href="javascript:;" title="Change Status" onclick="changeFlag(this,'contents','status',{{$data->id}});" class="badge @if ($data->status==1) {{'badge-success'}} @else {{'badge-danger'}} @endif btn-mini">@if ($data->status==1) {{'Active'}} @else {{'Idle'}} @endif</a></td> --}}
                                            <td>
<div class="fr">
                                            <a href="{{route('media.edit',$data->id)}}" class="btn btn-custon-four btn-primary"><span class="adminpro-icon adminpro-informatio"></span> Edit</a>
                                            <a href="javascript:void(0)" data-url="{{route('media.destroy',$data->id)}}" data-id="{{$data->id}}" data-toggle="modal" data-target="#WarningModalhdbgcl" class="btn btn-custon-four btn-danger delete_button_action"><span class="adminpro-icon adminpro-danger-error"></span> Delete</a>
                                        </div>



                                                {{-- <a href="" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                                                @if ($media_category->title!='content' || Auth::user()->user_role=='developer')
                                                    <a href="javascript:void(0)" title="Delete" data-url="{{route('media.destroy',$data->id)}}" data-id="{{$data->id}}" data-toggle="modal" data-target="#delete_button_modal" class="delete_button_action" ><i class="fa fa-trash-o text-danger"></i></a> --}}

                                            </td>
                                        </tr>
                                        @php $num @endphp
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Static Table End -->
@endsection

