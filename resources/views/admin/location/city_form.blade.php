@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="{{url('omed/'.$page)}}">{{ucwords($page)}}</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="#">Create</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="basic-form-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline12-list shadow-reset mg-t-30">
                        <div class="sparkline12-hd">
                            <div class="main-sparkline12-hd">
                                <h1>{{ucwords($page)}}</h1>
                                <div class="sparkline12-outline-icon">
                                    <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    <span><i class="fa fa-wrench"></i></span>
                                    <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="all-form-element-inner">
    @if(!isset($city))
                                                <form action="{{route('district.store')}}" method="post">
                                                    @csrf



{{--                                            {!! Form::open(array('url'=>route($url),'method'=>'POST')) !!}--}}
                                            @else
                                                        <form action="{{route('district.update',$city->id)}}" method="post">
                                                            @csrf
                                                            @method('PATCH')

{{--                                                {!! Form::model($city,['method'=>'PATCH','url'=>[$url,$city->id]]) !!}--}}
                                            @endif
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">State</label>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <select class="select2_demo_2 form-control state-select "   name="state" required style="width:100%;" >
                                                                <option selected disabled>State</option>
                                                                @foreach($states as $s)
                                                                    <option value="{{$s->name}}" @if(isset($city) && $s->name == $city->state){{'selected'}} @endif>{{ucwords($s->name)}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">City</label>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <input type="text" class="form-control" name="name" value="@if(isset($city)){{$city->name}}@endif" placeholder="Enter City Name" required>
                                                        </div>
                                                    </div>
                                                </div>


                                            <div class="form-group-inner">
                                                <div class="login-btn-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3"></div>
                                                        <div class="col-lg-6">
                                                            <div class="login-horizental cancel-wp pull-left">
                                                                <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>
                                                                <button class="btn btn-sm btn-primary login-submit-cs" type="submit">Save
                                                                   </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                        </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
