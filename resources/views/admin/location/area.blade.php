@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="#">Area</a> <span class="bread-slash"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- Static Table Start -->
    <div class="data-table-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12"><div style="margin-bottom: 10px; padding-bottom:10px;">
                        <button title="Add New" onclick="window.location.href ='{{url('omed/area/create')}}'" class="btn btn-primary add-button" type="button" style="float:right;">
                            <i class="fa fa-plus"></i>
                        </button> <br>
                    </div>


                    <div class="sparkline12-list shadow-reset mg-b-15">
                        <div class="sparkline12-hd">
                            <div class="main-sparkline12-hd">
                                <h1>Search Area<span class="table-project-n"> List</span> </h1>
                                <div class="sparkline12-outline-icon">
                                    <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{-- <span><i class="fa fa-wrench"></i></span> --}}
                                    {{-- <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span> --}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <form method="post" action="{{url('omed/get-area')}}">
                                @csrf
                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="login2 pull-right pull-right-pro">State</label>
                                        </div>
                                        <div class="col-lg-4">
                                            <select class="select2_demo_2 form-control state-select "   name="state" required style="width:100%;" >
                                                <option selected disabled>State</option>
                                                @foreach($states as $s)
                                                    <option value="{{$s->name}}" >{{ucwords($s->name)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="login2 pull-right pull-right-pro">City</label>
                                        </div>
                                        <div class="col-lg-4">
                                            <select class="select2_demo_2 form-control city-select "   name="city" required style="width:100%;"  >
                                                <option selected disabled>City</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group-inner">
                                    <div class="login-btn-inner">
                                        <div class="row">
                                            <div class="col-lg-3"></div>
                                            <div class="col-lg-6">
                                                <div class="login-horizental cancel-wp pull-left">
                                                    <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>
                                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit">Show</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>



                    <div class="sparkline13-list shadow-reset">
                        <div class="sparkline13-hd">
                            <div class="main-sparkline13-hd">
                                <h1>Area<span class="table-project-n"> List</span> </h1>
                                <div class="sparkline13-outline-icon">
                                    <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{-- <span><i class="fa fa-wrench"></i></span> --}}
                                    {{-- <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span> --}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline13-graph">
                            <div class="datatable-dashv1-list custom-datatable-overright">
                                <div id="toolbar">
                                    <select class="form-control">
                                        <option value="">Export Basic</option>
                                        <option value="all">Export All</option>
                                        <option value="selected">Export Selected</option>
                                    </select>
                                </div>
                                <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                    <tr>
                                        <th data-field="state" data-checkbox="true"></th>
                                        <th data-field="id">S.No.</th>
                                        <th data-field="area"></th>
                                        <th data-field="district">District</th>
                                        <th data-field="state_name">State</th>
                                        <th data-field="pincode">Pincode</th>
                                        <th data-field="action">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($list))
                                    @php $num=1 @endphp
                                    @foreach($list as $data)
                                        <tr>
                                            <td></td>
                                            <td>{{$num}}</td>

                                            <td>{{$data->area}}</td>
    <td>{{$data->city}}</td>
                                            <td>{{$data->state}}</td>
<td>{{$data->pincode}}</td>
                                            <td>
                                                <div class="fr">
                                                    <a href="{{route('area.edit',$data->id)}}" class="btn btn-custon-four btn-primary"><span class="adminpro-icon adminpro-informatio"></span> Edit</a>
                                                    <a href="javascript:void(0)" data-url="{{route('area.destroy',$data->id)}}" data-id="{{$data->id}}" data-toggle="modal" data-target="#WarningModalhdbgcl" class="btn btn-custon-four btn-danger delete_button_action"><span class="adminpro-icon adminpro-danger-error"></span> Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                        @php $num++ @endphp

                                    @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Static Table End -->
@endsection
@section('additional_script')
    <script>
        $('.state-select').change(function() {
            $.ajax({
                type:"GET",
                url:"{{url('ajax/state')}}?state="+$(this).val(),
                success: function(response) {
                    if (response.success) {
                        $('.city-select').find('option').remove().end().append('<option selected hidden disabled>City</option>');
                        var city = response.city;
                        $.each(city, function(i, d) {
                            $('.city-select').append('<option>' + d.name + '</option>');
                        });
                    }
                },
            });
        });

        // City Change
        $('.city-select').change(function() {
            $.ajax({
                type:"GET",
                url:"{{url('ajax/city')}}?city="+$(this).val()+'&state='+$('.state-select').val(),
                success: function(response) {
                    if (response.success) {
                        $('.area-select').find('option').remove().end().append('<option selected hidden disabled>Area</option>');
                        var area = response.area;
                        $.each(area, function(i, d) {
                            $('.area-select').append('<option>' + d.area + '</option>');
                        });
                    }
                },
            });
        });
    </script>
@endsection
