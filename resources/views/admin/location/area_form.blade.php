@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="{{url('omed/'.$page)}}">{{ucwords($page)}}</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="#">Create</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="basic-form-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline12-list shadow-reset mg-t-30">
                        <div class="sparkline12-hd">
                            <div class="main-sparkline12-hd">
                                <h1>{{ucwords($page)}}</h1>
                                <div class="sparkline12-outline-icon">
                                    <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    <span><i class="fa fa-wrench"></i></span>
                                    <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="all-form-element-inner">
                                            @if(!isset($area))
                                                <form action="{{route('area.store')}}" method="post">

                                            @else
                                                        <form action="{{route('area.update',$area->id)}}" method="post">
                                                            @method('PATCH')
                                            @endif
                                                    @csrf

                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">State</label>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <select class="select2_demo_2 form-control state-select "   name="state" required style="width:100%;" >
                                                            <option selected disabled>State</option>
                                                            @foreach($states as $s)
                                                                <option value="{{$s->name}}" @if(isset($area) && $s->name == $area->state) {{'selected'}} @endif>{{ucwords($s->name)}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">City</label>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <select class="select2_demo_2 form-control city-select "   name="city" required style="width:100%;"  >
                                                            @if(isset($city))
                                                            @foreach($city as $s)
                                                                <option value="{{$s->name}}" @if(isset($area) && $s->name == $area->city) {{'selected'}} @endif>{{ucwords($s->name)}}</option>
                                                            @endforeach
                                                            @else
                                                            <option selected disabled>City</option>
                                                                @endif

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Area</label>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" name="area" value="@if(isset($area)){{$area->area}}@endif" placeholder="Enter Area Name" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Pincode</label>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" name="pincode" value="@if(isset($area)){{$area->pincode}}@endif" placeholder="Enter Pincode" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="login-btn-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3"></div>
                                                        <div class="col-lg-6">
                                                            <div class="login-horizental cancel-wp pull-left">
                                                                <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>
                                                                <button class="btn btn-sm btn-primary login-submit-cs" type="submit">Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additional_script')
    <script>
        $('.state-select').change(function() {
            $.ajax({
                type:"GET",
                url:"{{url('ajax/state')}}?state="+$(this).val(),
                success: function(response) {
                    if (response.success) {
                        $('.city-select').find('option').remove().end().append('<option selected hidden disabled>City</option>');
                        var city = response.city;
                        $.each(city, function(i, d) {
                            $('.city-select').append('<option>' + d.name + '</option>');
                        });
                    }
                },
            });
        });

        {{--// City Change--}}
        {{--$('.city-select').change(function() {--}}
            {{--$.ajax({--}}
                {{--type:"GET",--}}
                {{--url:"{{url('ajax/city')}}?city="+$(this).val()+'&state='+$('.state-select').val(),--}}
                {{--success: function(response) {--}}
                    {{--if (response.success) {--}}
                        {{--$('.area-select').find('option').remove().end().append('<option selected hidden disabled>Area</option>');--}}
                        {{--var area = response.area;--}}
                        {{--$.each(area, function(i, d) {--}}
                            {{--$('.area-select').append('<option>' + d.area + '</option>');--}}
                        {{--});--}}
                    {{--}--}}
                {{--},--}}
            {{--});--}}
        {{--});--}}
    </script>
@endsection
