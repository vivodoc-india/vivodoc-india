
{{ ! list(, $action) =explode('@', Route::getCurrentRoute()->getActionName()) }}
{{--{{csrf_field()}}--}}

    <div class="form-group-inner">
        <div class="row">
            <div class="col-lg-3">
                <label class="login2 pull-right pull-right-pro">State</label>
            </div>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="state" value=" @if(isset($state)){{$state->name}}  @endif" placeholder="Enter State Name" required>
            </div>
        </div>
    </div>

{{--<div class="form-group-inner">--}}
{{--    <div class="row">--}}
{{--        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">--}}
{{--            <label class="login2 pull-right pull-right-pro">Status </label>--}}
{{--        </div>--}}
{{--        <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12">--}}
{{--            <div class="bt-df-checkbox pull-left">--}}

{{--                <div class="row">--}}
{{--                    <div class="col-lg-12">--}}
{{--                        <div class="i-checks pull-left">--}}
{{--                            <label>--}}
{{--                                {{Form::radio('status','1',true,['style'=>'opacity: 0;'])}} <i></i>--}}
{{--                                Active </label>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-lg-12">--}}
{{--                        <div class="i-checks pull-left">--}}
{{--                            <label>--}}
{{--                                {{Form::radio('status','0',false,['style'=>'opacity: 0;'])}} <i></i> Idle </label>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--</div>--}}
<div class="form-group-inner">
    <div class="login-btn-inner">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="login-horizental cancel-wp pull-left">
                    <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>
                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit">@if($action=='create') Save
                        @else Update
                        @endif</button>
                </div>
            </div>
        </div>
    </div>
</div>


