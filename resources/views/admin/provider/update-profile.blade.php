@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li>  <a href="{{url('omed/user-management')}}">User Management</a> <span class="bread-slash">/</span>
                            </li>
                            <li>    <a href="#"> Update</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="basic-form-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline12-list shadow-reset mg-t-30">
                        <div class="sparkline12-hd">
                            <div class="main-sparkline12-hd">
                                <h1>User Management</h1>
                                <div class="sparkline12-outline-icon">
                                    <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{--                                    <span><i class="fa fa-wrench"></i></span>--}}
                                    {{--                                    <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>--}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="all-form-element-inner">
                                            <ul class="nav nav-pills">
                                                <li class="active"><a data-toggle="pill" href="#home"><i class="fa fa-user" aria-hidden="true"></i> Personal Info</a></li>
                                                @if(isset($list))
                                                    <li><a data-toggle="pill" href="#menu1"><i class="fa fa-user-md"></i> Services Info</a></li>
                                                @else

                                                    <li><a data-toggle="pill" href="#menu1"> <i class="fa fa-user-md"></i> New Services</a></li>
                                                @endif
                                                <li><a data-toggle="pill" href="#menu3"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Photo Upload</a></li>
                                            </ul>
                                            <hr>

                                            <div class="tab-content">
                                                <!-- Personal Info Update -->
                                                <div id="home" class="tab-pane fade in active">
                                                    <form action="{{url('omed/updateProviderPersonalInfo')}}" method="post" id="frm-personal">
                                                        @csrf
                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro">Name<spam> * </spam> : </label>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <input name="name"  placeholder="Name" class="form-control border-round  rounded-0 @error('name') is-invalid @enderror" required value=" @if(isset($user->name)){{$user->name}}@endif">
                                                                    @error('name')
                                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro">Mobile : </label>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <input name="mobile"  placeholder="Mobile No." class="form-control border-round  rounded-0 @error('mobile') is-invalid @enderror" value=" @if(isset($user->mobile)){{$user->mobile}}@endif">
                                                                    @error('mobile')
                                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro">Email Id : </label>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <input name="email" type="email" placeholder="Email" class="form-control border-round  rounded-0 @error('email') is-invalid @enderror" value=" @if(isset($user->email)){{$user->email}}@endif">
                                                                    @error('email')
                                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                                    <label class="login2 pull-right pull-right-pro">Gender<spam> * </spam>: </label>
                                                                </div>
                                                                <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
                                                                    <div class="bt-df-checkbox pull-left">

                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <div class="i-checks pull-left">
                                                                                    <label>
                                                                                        <input type="radio" class="custom-control-input  @error('gender') is-invalid @enderror" required id="customRadio" name="gender" value="Male" @if(isset($user->profile->gender) && $user->profile->gender =="Male"){{'checked'}}@endif> <i></i>
                                                                                        Male </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <div class="i-checks pull-left">
                                                                                    <label>
                                                                                        <input type="radio" class="custom-control-input  @error('gender')
                                                                                            is-invalid @enderror" id="customRadio2" name="gender"
                                                                                               value="Female"  @if(isset($user->profile->gender) &&
                                                       $user->profile->gender=="Female"){{'checked'}}@endif> <i></i>
                                                                                        Female </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro">Speciality<spam> * </spam> : </label>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <input name="speciality"  placeholder="speciality" class="form-control border-round  rounded-0 @error('speciality') is-invalid @enderror" required value=" @if(isset($user->profile->speciality)){{$user->profile->speciality}}@endif">
                                                                    @error('sepciality')
                                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro">Other Speciality<spam> * </spam> :
                                                                    </label>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <input name="other_speciality"  placeholder="i.e. languages know or something else" class="form-control border-round  rounded-0 @error('other_speciality') is-invalid @enderror" required value=" @if(isset($user->profile->other_speciality)){{$user->profile->other_speciality}}@endif">
                                                                </div>
                                                                @error('other_sepciality')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro">Experiance<spam> * </spam> :
                                                                    </label>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <input name="experiance"  placeholder="experiance" class="form-control border-round  rounded-0 @error('experiance') is-invalid @enderror" required value=" @if(isset($user->profile->experiance)){{$user->profile->experiance}}@endif">
                                                                </div>
                                                                @error('experiance')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro">Qualification<spam> * </spam> :
                                                                    </label>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control rounded-0 border-round @error('qualification') is-invalid @enderror" placeholder="qualification : i.e. Phd.,M.B.B.S.,M.S. etc" name="qualification" required title="Use ',' to seperate entires" value="@if(isset($user->profile->qualification)){{$user->profile->qualification}}@endif">
                                                                </div>
                                                                @error('qualification')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro">Address<spam> * </spam> :
                                                                    </label>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <textarea name="address" required class="form-control rounded-0 @error('address') is-invalid @enderror"  placeholder="Address">@if(isset($user->profile->address)){{$user->profile->address}}@endif</textarea>

                                                                </div>
                                                                @error('address')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <input type="hidden" name="id" value="{{$user->id}}">
                                                        <div class="form-group-inner">
                                                            <div class="login-btn-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3"></div>
                                                                    <div class="col-lg-6">
                                                                        <div class="login-horizental cancel-wp pull-left">
                                                                            <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>
                                                                            <button class="btn btn-sm btn-primary login-submit-cs" type="submit"> Save

                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </form>

                                                </div>

                                                <!-- Services Update -->
                                                <div id="menu1" class="tab-pane fade" style="margin-top:50px;">
                                                    <h3 class="text-primary">Choose service provider type(s).</h3>
                                                    <form action="{{url('omed/updateProviderServicesInfo')}}" method="post" id="frm-service">
                                                        @csrf

                                                        @if(isset($list))
                                                            @foreach($list as $l)
                                                                <div class="form-group-inner">
                                                                    <div class="row">
                                                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                                            <label class="login2 pull-right pull-right-pro">{{$l->name}}: </label>
                                                                        </div>
                                                                        <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
                                                                            <div class="bt-df-checkbox pull-left">

                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <div class="i-checks pull-left">
                                                                                            <label>
                                                                                                <input type="checkbox" class="custom-control-input
" name="user_role_id[]" value="{{$l->id}}"> <i></i>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            @endforeach
                                                            <input type="hidden" name="id" value="{{$user->id}}">
                                                            <div class="form-group-inner">
                                                                <div class="login-btn-inner">
                                                                    <div class="row">
                                                                        <div class="col-lg-3"></div>
                                                                        <div class="col-lg-6">
                                                                            <div class="login-horizental cancel-wp pull-left">
                                                                                <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>
                                                                                <button class="btn btn-sm btn-primary login-submit-cs" type="submit"> Save

                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @else
                                                            @if(isset($list_add) && count($list_add) > 0)
                                                                @foreach($list_add as $l)
                                                                    <div class="form-group-inner">
                                                                        <div class="row">
                                                                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                                                <label class="login2 pull-right pull-right-pro">{{$l->name}}: </label>
                                                                            </div>
                                                                            <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="bt-df-checkbox pull-left">

                                                                                    <div class="row">
                                                                                        <div class="col-lg-12">
                                                                                            <div class="i-checks pull-left">
                                                                                                <label>
                                                                                                    <input type="checkbox" class="custom-control-input
"  name="user_role_id[]" value="{{$l->id}}"> <i></i>
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>


                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>


                                                                @endforeach
                                                                <input type="hidden" name="id" value="{{$user->id}}">
                                                                <div class="form-group-inner">
                                                                    <div class="login-btn-inner">
                                                                        <div class="row">
                                                                            <div class="col-lg-3"></div>
                                                                            <div class="col-lg-6">
                                                                                <div class="login-horizental cancel-wp pull-left">
                                                                                    <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>
                                                                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit"> Save

                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @else
                                                                <h3 class="text-primary">This service provider listed in all service provider category.</h3>
                                                            @endif
                                                        @endif
                                                    </form>
                                                </div>
                                                <!-- profile Photo Update -->
                                                <div id="menu3" class="tab-pane fade " style="margin-top:50px;">
                                                    <form action="{{url('omed/updateProviderDocuments')}}" method="post" id="frm-document"
                                                          enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="form-group-inner">
                                                            <div class="row" style="margin-bottom: 10px;">
                                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                                    <label class="login2 pull-right pull-right-pro">Image :</label>
                                                                </div>
                                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="file-upload-inner file-upload-inner-right ts-forms">
                                                                        <div class="input append-big-btn">
                                                                            <label class="icon-left" for="append-big-btn">
                                                                                <i class="fa fa-download"></i>
                                                                            </label>
                                                                            <div class="file-button">
                                                                                Browse
                                                                                <input type="file" name="photo" onchange="document.getElementById
                                                  ('append-big-btn').value = this.value;">
                                                                            </div>
                                                                            <input type="text" id="append-big-btn" placeholder="no file selected">
                                                                            <span class="misc">Allowed file type: jpg, jpeg, png.</span>

                                                                        </div>
                                                                    </div>
                                                                    @error('photo')
                                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                                    @enderror
                                                                </div>

                                                            </div>

                                                            <div class="row">
                                                                <div class="col-lg-3"></div>
                                                                <div class="col-lg-4">
                                                                    @if(isset($user->profile->photo) && $user->profile->photo !='')
                                                                        <a href="{{asset('uploads/profile/'.$user->profile->photo)}}"><img src="{{asset
                              ('uploads/profile/'.$user->profile->photo)}}" class="img-fluid img-thumbnail"
                                                                                                                                               style="width:150px;"></a>
                                                                    @else
                                                                        <a href=""><img src="{{asset('web/img/nobody.jpg')}}" class="img-fluid img-thumbnail"
                                                                                        style="width:150px;"></a>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="id" value="{{$user->id}}">
                                                        <div class="form-group-inner">
                                                            <div class="login-btn-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3"></div>
                                                                    <div class="col-lg-6">
                                                                        <div class="login-horizental cancel-wp pull-left">
                                                                            <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>
                                                                            <button class="btn btn-sm btn-primary login-submit-cs" type="submit"> Save

                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">

                                                    <br>
                                                    <div class="tab-content">
                                                        <!-- Personal Info Body -->
                                                        <div class="tab-pane fade show active" id="pills-personal">

                                                            <form action="{{url('updatePersonalInfo')}}" method="post" id="frm-personal">
                                                                @csrf
                                                                <div class="row mb-3">

                                                                    <div class="form-group-inner">
                                                                        <div class="row">
                                                                            <div class="col-lg-3">
                                                                                <label class="login2 pull-right pull-right-pro">Speciality <span class="req">*</span>:</label>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="form-select-list">

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    {{--        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">--}}
                                                                    {{--      <label class="control-label"></label>--}}
                                                                    {{--       <div class="form-group">--}}
                                                                    {{--     --}}
                                                                    {{--         </div>--}}
                                                                    {{--  --}}
                                                                    {{--        </div>--}}
                                                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                                                                        <label class="control-label">Other Speciality </label>
                                                                        <div class="form-group">

                                                                        </div>
                                                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                                                                            <label class="control-label">Experiance </label>
                                                                            <div class="form-group">

                                                                            </div>
                                                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                                                <label class="control-label">Landmark <span class="req">*</span></label>
                                                                                <div class="form-group">
                                                                                    <input type="text" name="landmark" class="form-control rounded-0 border-round @error('landmark') is-invalid @enderror" placeholder="landmark" required value="@if(isset($user->profile->landmark)){{$user->profile->landmark}}@endif">
                                                                                </div>
                                                                                @error('landmark')
                                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                                @enderror
                                                                            </div>
                                                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                                                <label class="control-label">Area <span class="req">*</span></label>
                                                                                <div class="form-group">
                                                                                    <input type="text" name="area" class="form-control rounded-0 border-round @error('area') is-invalid @enderror" placeholder="area" required value="@if(isset($user->profile->area)){{$user->profile->area}}@endif">
                                                                                </div>
                                                                                @error('area')
                                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                                @enderror
                                                                            </div>

                                                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                                                <label class="control-label">City <span class="req">*</span></label>
                                                                                <div class="form-group">
                                                                                    <input type="text" name="city" class="form-control rounded-0 border-round @error('city') is-invalid @enderror" placeholder="city" required value="@if(isset($user->profile->city)){{$user->profile->city}}@endif">
                                                                                    @error('city')
                                                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                                                    @enderror
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                                                <label class="control-label">State <span class="req">*</span></label>
                                                                                <div class="form-group">
                                                                                    <input type="text" name="state" class="form-control rounded-0 border-round @error('state') is-invalid @enderror" placeholder="state" required value="@if(isset($user->profile->state)){{$user->profile->state}}@endif">
                                                                                </div>
                                                                                @error('state')
                                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                                @enderror
                                                                            </div>
                                                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                                                <label class="control-label">Pincode <span class="req">*</span></label>
                                                                                <div class="form-group">
                                                                                    <input type="text" name="pincode" class="form-control rounded-0 border-round @error('pincode') is-invalid @enderror" placeholder="pincode" required oninput='this.value=(parseInt(this.value)||" ")' maxlength="6" value="@if(isset($user->profile->pincode)){{$user->profile->pincode}}@endif">
                                                                                </div>
                                                                                @error('pincode')
                                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                                @enderror
                                                                            </div>
                                                                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                                                                                <label class="control-label">Qualification <span class="req">*</span></label>
                                                                                <div class="form-group">

                                                                                </div>
                                                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                                                    <label class="control-label">Address <span class="req">*</span></label>
                                                                                    <div class="form-group">

                                                                                    </div>
                                                                                </div>
                                                                                <button type="submit" class="btn btn-dark" id="btn-save-personal"><i class="fa fa-user-circle" aria-hidden="true"></i> Save</button>
                                                            </form>

                                                        </div>

                                                        <!-- Personal Info Body Ends Here -->
                                                        <!-- Services Body -->

                                                        <!-- ENd of Services Tab Body-->
                                                        <!-- Add New Services Info -->



                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


