@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li>  <a href="{{url('omed/user-management')}}">User Management</a> <span class="bread-slash">/</span>
                            </li>
                            <li>   <a href="#">Show</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')

    <div class="project-details-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="project-details-wrap shadow-reset">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                <div class="project-details-title">
                                    <h2><span class="profile-details-name-nn">Use Data</span> </h2>
                                </div>

                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <div class="admin-comment-month project-details-action">
                                    <button class="comment-setting" data-toggle="collapse" data-target="#adminpro-demo2">More <i class="fa fa-caret-down"
                                                                                                                                 aria-hidden="true"></i>
                                    </button>
                                    <ul id="adminpro-demo2" class="comment-action-st collapse">
{{--                                        <li><a href="{{url('omed/service-provider/ratings/'.$user->id)}}" target="__blank">Reviews & Ratings</a>--}}
{{--                                        </li>--}}
{{--                                        <li><a href="{{url('omed/service-provider/patient-enquiries/'.$user->id)}}" target="__blank">Patient--}}
{{--                                                Enquiries</a>--}}
{{--                                        </li>--}}
                                        <li><a href="{{url('omed/service-provider/offline-bookings/'.$user->id)}}" target="__blank">Appointment Bookings</a>
                                        </li>
                                        <li><a href="{{url('omed/service-provider/online-bookings/'.$user->id)}}" target="__blank">Live Consultation Bookings</a>
                                        </li>
                                        {{--        <li><a href="#">Hide Profile</a>--}}
                                        {{--        </li>--}}
                                        {{--        <li><a href="#">Turn on Profile</a>--}}
                                        {{--        </li>--}}
                                        {{--        <li><a href="#">Turn off Profile</a>--}}
                                        {{--        </li>--}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Basic Details Starts Here -->
                        @if(isset($user))
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="project-details-mg">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="project-details-st">
                                                    <span><strong>Status:</strong></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <div class="btn-group project-list-ad">
                                                    <button class="btn btn-white btn-xs @if ($user->approval==0){{'bg-danger'}} @endif" onclick="changeFlag3(this,'users','approval',{{$user->id}});">@if ($user->approval==1) {{'Approved'}} @else {{'Not Approved'}} @endif</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        $has_doctor = \App\Models\ServiceProfile::where('user_id',$user->id)->where('user_role_id','1')->first();
                                    @endphp
                                    @if($has_doctor)
                                    <div class="project-details-mg">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="project-details-st">
                                                    <span><strong>Live Status:</strong></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <div class="btn-group project-list-ad">

                                                        <button class="btn btn-white btn-xs @if ($has_doctor->mark_live==0){{'bg-danger'}} @endif"
                                                                onclick="changeFlag4(this,'service_profiles','mark_live',{{$has_doctor->id}});">
                                                            @if ($has_doctor->mark_live==1) {{'Online'}} @else {{'Offline'}} @endif</button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @php
                                        $has_ambulance = \App\Models\ServiceProfile::where('user_id',$user->id)->where('user_role_id','6')->first();
                                    @endphp
                                    @if($has_ambulance)
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Ambulance Status:</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <button class="btn btn-white btn-xs @if ($has_ambulance->mark_live==0){{'bg-danger'}} @endif"
                                                                onclick="changeFlag4(this,'service_profiles','mark_live',{{$has_ambulance->id}});">
                                                            @if ($has_ambulance->mark_live==1) {{'Online'}} @else {{'Offline'}} @endif</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    @endif
                                    <div class="project-details-mg">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="project-details-st">
                                                    <span><strong>Name:</strong></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <div class="project-details-dt">
                                                    <span>{{ucwords($user->name)}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="project-details-mg">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="project-details-st">
                                                    <span><strong>Email ID:</strong></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <div class="project-details-dt">
                                                    <span>{{$user->email}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="project-details-mg">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="project-details-st">
                                                    <span><strong>Mobile No.:</strong></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <div class="project-details-dt">
                                                    <span>{{$user->mobile}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="project-details-mg">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="project-details-st">
                                                    <span><strong>Gender:</strong></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <div class="project-details-dt">
                                                    <span>{{$user->profile->gender}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="project-details-mg">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="project-details-st">
                                                    <span><strong>Qualifications:</strong></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <div class="project-details-dt">
                                                    <span>{{$user->profile->qualification}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="project-details-mg">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="project-details-st">
                                                    <span><strong>Address:</strong></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <div class="project-details-dt">
                                                    <span>Landmark- {{$user->profile->landmark}} , Area- {{$user->profile->area}}, Address- {{$user->profile->address}}, {{$user->profile->state}},{{$user->profile->pincode}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="project-details-mg">
                                        <div class="row">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <a href="{{asset('uploads/profile/'.$user->profile->photo)}}" target="__blank">
                                                    <img src="{{asset('uploads/profile/'.$user->profile->photo)}}" class="img-responsive img-thumbnail" style="width:100px;"/></a>
                                            </div>
                                        </div>
                                    </div>



                                    {{-- <div class="project-details-mg">
                                    <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="project-details-st">
                                            <span><strong>Phone:</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div class="project-details-dt">
                                            <span>01962067309</span>
                                        </div>
                                    </div>
                                    </div>
                                    </div> --}}
                                    {{-- <div class="project-details-mg">
                                    <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="project-details-st">
                                            <span><strong>Participants:</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div class="project-details-img">
                                            <a href="#"><img src="img/notification/1.jpg" alt="" />
                                            </a>
                                            <a href="#"><img src="img/notification/2.jpg" alt="" />
                                            </a>
                                            <a href="#"><img src="img/notification/3.jpg" alt="" />
                                            </a>
                                            <a href="#"><img src="img/notification/4.jpg" alt="" />
                                            </a>
                                            <a href="#"><img src="img/notification/5.jpg" alt="" />
                                            </a>
                                        </div>
                                    </div>
                                    </div>
                                    </div> --}}
                                </div>
                            </div>
                        @endif

                    <!-- Basic Details End Here -->
                        {{-- <div class="row">
                        <div class="col-lg-2">
                        <div class="project-pregress-details">
                        <span><strong>Completed:</strong></span>
                        </div>
                        </div>
                        <div class="col-lg-10">
                        <div class="skill-content-3 project-details-progress">
                        <div class="skill">
                        <div class="progress">
                            <div class="progress-bar wow fadeInLeft" data-progress="95%" style="width: 95%;" data-wow-duration="1.5s" data-wow-delay="1.2s"> <span>95%</span>
                            </div>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div> --}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="project-details-tab">
                                    <ul class="nav nav-tabs res-pd-less-sm">

                                        @if(isset($services))
                                            @php $ns=1 @endphp
                                            @foreach($services as $service)
                                                @if(!empty($service['serv']))
                                                    <li @if($ns == 1) {{'class=active'}} @endif><a data-toggle="tab" href="#tab{{$service['serv']->user_role->id}}">{{$service['serv']->user_role->name}}</a>
                                                    </li>
                                                @endif
                                                @php $ns++ @endphp
                                            @endforeach
                                        @endif

                                    </ul>
                                    <div class="tab-content res-tab-content-project">

                                        @if(isset($services))
                                            @php $n=1 @endphp
                                            @foreach($services as $service)
                                                @if(!empty($service['serv']))
                                                    <div id="tab{{$service['serv']->user_role->id}}" class="tab-pane fade in @if($n == 1) {{'active'}} @endif animated zoomInLeft">
                                                        <div class="user-profile-comment-list">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <h6>Categories Selected</h6>
                                                                </div>
                                                                <div class="col-lg-9">
                                                                    @if(count($service['cat']) > 0)
                                                                        @for($j=0;$j<count($service['cat']);$j++)

                                                                            <span class="label label-info">{{ucwords($service['cat'][$j]->title)}}</span>
                                                                        @endfor
                                                                    @else
                                                                        <span class="label label-info">No category selected yet.</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="row mt-5">
                                                                <div class="col-lg-3">
                                                                    <h6>Related Documents Submitted</h6>
                                                                </div>
                                                                <div class="col-lg-9 col-md-9 col-sm-12">
                                                                    <div class="row">
                                                                        @php $count=1 @endphp
{{--                                                                        @php $profile= explode(",", $service['serv']['related_document']) @endphp--}}
                                                                        @if(count($service['rel_doc']) > 0)
                                                                            @for($x=0;$x<count($service['rel_doc']);$x++)
{{--                                                                        @for($x=0;$x<count($profile);$x++)--}}
                                                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                                                <a href="{{asset('uploads/service/documents/'.$service['rel_doc'][$x]->folder_name.'/'.$service['rel_doc'][$x]->file)}}" target="__blank">
                                                                                    <img src="{{asset('uploads/service/documents/'.$service['rel_doc'][$x]->folder_name.'/'.$service['rel_doc'][$x]->file)}}" class="img-responsive img-thumbnail "></a>
                                                                            </div>
                                                                            @if($count % 3 == 0)
                                                                    </div>
                                                                    <div class="row mt-5">
                                                                        @endif
                                                                        @php $count++ @endphp

                                                                        @endfor
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="row mt-10">
                                                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                                                        <h6>Images Uploaded Related To This Service</h6>
                                                                    </div>
                                                                    <div class="col-lg-9 col-md-9 col-sm-12">
                                                                        <!-- row starts here -->
                                                                        <div class="row">
                                                                            @for($z=1; $z<=5; $z++)

                                                                                <div class="col-lg-4 col-md-4 col-xl-4 col-sm-12 ">
                                                                                    <div class="panel mb-5">
                                                                                        <div class="panel-body">
                                                                                            <form action="{{url('omed/update-services-images')}}" method="post"  enctype="multipart/form-data">
                                                                                                <input type="hidden" name="sp_id" value="{{$service['serv']['id']}}">
                                                                                                <input type="hidden" name="sp_ur_id" value="{{$service['serv']['user_role_id']}}">
                                                                                                <input type="hidden" name="image_count" value="{{$z}}">
                                                                                                @csrf

                                                                                                @if(!empty($service['serv']['image'.$z]))
                                                                                                    <a href="{{asset($service['serv']['image'.$z])}}" target="__blank">
                                                                                                        <img src="{{asset($service['serv']['image'.$z])}}" class="img-fluid img-thumbnail rounded"></a>
                                                                                                @else
                                                                                                    <img src="{{asset('web/img/no_image.jpg')}}" class="img-fluid img-thumbnail rounded">

                                                                                                @endif
                                                                                                <div class="file-upload-inner file-upload-inner-right ts-forms mt-10">
                                                                                                    <div class="input append-small-btn">
                                                                                                        <label class="icon-left" for="append-small-btn{{$z.$n}}">
                                                                                                            <i class="fa fa-download"></i>
                                                                                                        </label>
                                                                                                        <div class="file-button">
                                                                                                            Browse
                                                                                                            <input type="file" name="image" class="@error('image') is-invalid @enderror" onchange="document.getElementById('append-small-btn{{$z.$n}}').value = this.value;">
                                                                                                        </div>
                                                                                                        <input type="text" id="append-small-btn{{$z.$n}}" placeholder="no file selected">
                                                                                                    </div>
                                                                                                </div>

                                                                                                <span class="misc mt-5">Allowed file type: jpg, jpeg, png.</span>
                                                                                                @error('image')
                                                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                                                @enderror
                                                                                                <button type="submit" class="btn btn-sm btn-primary login-submit-cs mt-5"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Upload</button>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endfor
                                                                        </div>
                                                                        <!-- Row ends here -->
                                                                    </div>
                                                                </div>
                                                                {{--    <div class="row">--}}
                                                                {{--@php $profile= explode(",", $service['serv']['related_document']) @endphp--}}
                                                                {{--@for($n=0; $n<count($profile); $n++)--}}

                                                                {{-- <div class="col-lg-4">--}}
                                                                {{--<a href="{{asset('uploads/service-images/related_document/'.$profile[$n])}}" target="__blank">--}}
                                                                {{--<img src="{{asset('uploads/service-images/related_document/'.$profile[$n])}}" class="img-thumbnail img-responsive"/>--}}
                                                                {{--</a>--}}
                                                                {{--</div>--}}
                                                                {{--@endfor--}}
                                                                {{--    </div>--}}
                                                                {{--    </div>--}}
                                                            </div>
                                                        </div>
                                                    </div>

                                                @endif
                                                @php $n++ @endphp
                                            @endforeach
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection
@section('additional_script')
    <script>
        function changeFlag4(data,table,column,id){
            var that=$(data);
            $.ajax({
                type:"GET",
                url:"{{url('ajax/change-flag')}}?table="+table+"&column="+column+"&id="+id,
                success:function(res){
                    if(res.status=='1'){
                        that.text('Online');
                        that.removeClass('bg-danger');
                        that.addClass('green');
                        Lobibox.notify('success', {
                            delay: 15000,
                            sound:true,
                            msg: 'Status Changed Successfully'
                        });

                    }
                    if(res.status=='0'){
                        that.text('Offline');
                        that.removeClass('green');
                        that.addClass('bg-danger');

                        Lobibox.notify('success', {
                            delay: 15000,
                            sound:true,
                            msg: 'Status Changed Successfully'
                        });

                    }
                    if(res.status=='2'){
                        Lobibox.notify('warning', {
                            delay: 15000,
                            sound:true,
                            msg: res.message
                        });

                    }
                }
            });
        }
    </script>
@endsection
