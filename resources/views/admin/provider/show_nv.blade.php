@extends('layouts.master')
@section('breadcumb')
<!-- Breadcome start-->
            <div class="breadcome-area mg-b-30 small-dn">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                 <ul class="breadcome-menu">
                                 <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                                            </li>
    <li>  <a href="{{url('omed/user-management')}}">User Management</a> <span class="bread-slash">/</span>
                                            </li>
                                             <li>   <a href="#">Show</a>
                                            </li>
                                        </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@section('content')

    <div id="content">

        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>User Data</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered">
                                <tbody>
                                     @if(isset($user))
                                <tr><th>Name</th> <td>{{ucwords($user->name)}}</td> </tr>
                                <tr><th>Email Id</th> <td>{{$user->email}}</td></tr>
                                <tr><th>Mobile No.</th> <td>{{$user->mobile}}</td></tr>
                                <tr><th>Gender</th> <td>{{$user->profile->gender}}</td></tr>
                                <tr><th>Qualification</th><td>{{$user->profile->qualification}}</td></tr>
                                <tr><th>Address</th> <td>Landmark- {{$user->profile->landmark}} , Area- {{$user->profile->area}}, Address- {{$user->profile->address}}, {{$user->profile->state}},{{$user->profile->pincode}}</td> </tr>
                                <tr><th>Approval Status</th> <td><a href="javascript:;" onclick="changeFlag(this,'users','approval',{{$user->id}});" class="btn @if ($user->approval==1) {{'btn-success'}} @else {{'btn-danger'}} @endif btn-mini">@if ($user->approval==1) {{'Approved'}} @else {{'Not Approved'}} @endif</a></td> </tr>
                                <tr><th>Profile Photo</th><td><a href="{{asset('uploads/userprofile/'.$user->profile->photo)}}" target="__blank"><img src="{{asset('uploads/userprofile/'.$user->profile->photo)}}" style="width:100px;" /></a></td> </tr>
                                {{-- <tr><th>Degree/Qualification Certificates</th><td><a href="{{asset('uploads/qualification/'.$user->profile->degree)}}" target="__blank"><img src="{{asset('uploads/qualification/'.$user->profile->degree)}}" style="width:100px;" /></a></td>
                            </tr> --}}
                             <tr><th>Service List</th> <td>
                                <table class="table table-bordered">
                                    <tr>
                               @if(isset($services))
@foreach($services as $service)
@if(!empty($service['serv']))
<th>{{$service['serv']->user_role->name}}</th>
@endif
<tr><td>
    <h6>Categories Selected</h6>
@if(count($service['cat']) > 0)
@for($j=0;$j<count($service['cat']);$j++)
<span class="label label-info">{{ucwords($service['cat'][$j]->title)}}</span>
@endfor
@else
<span class="label label-info">No category selected yet.</span>
@endif
<br>
<hr>
<h6>Related Documents Submitted</h6>
<?php $profile= explode(",", $service['serv']['related_document']); ?>
@for($n=0;$n<count($profile);$n++)
<a href="{{asset('uploads/service-images/related_document/'.$profile[$n])}}" target="__blank">
<img src="{{asset('uploads/service-images/related_document/'.$profile[$n])}}" style="width:100px;">
</a>
{{-- <div style="width:1px; height:100px; background:#000;"></div> --}}
@endfor


</td>
</tr>
@endforeach
@endif
</tr>

                                    </table>
                                         </td>
                                    </tr>
                            @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>Users Review/Rating</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                <thead><tr><th>Name</th><th>Rating</th><th>Review</th> </tr></thead>
                                <tbody>
                                    {{-- @foreach($review as $list)
                                        <tr><td><a href="{{url('customer/'.$list->id)}}">{{$list->name}}</a></td>
                                            <td>@for($i=0;$i<$list->rating;$i++)
                                                    <img src="{{asset('img/star.png')}}" />
                                                @endfor</td>
                                            <td>{{$list->review}}</td></tr>
                                    @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>Customer Enquiry</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                    <thead><tr><th>Customer</th><th>Date</th><th>Details</th><Th>Address</Th> </tr></thead>
                                        <tbody>
                                            {{-- @foreach($enquiry as $list)
                                                <tr><td><a href="{{url('customer/'.$list->customer_id)}}">{{$list->name}}</a></td>
                                                    <td>{{$list->created_at}}</td>
                                                    <td>{{$list->details}}</td>
                                                    <td>{{$list->address}}</td>
                                                </tr>
                                            @endforeach --}}
                                        </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
