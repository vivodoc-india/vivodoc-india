
<style>
    .project-details-tab > .nav > li > a{
   margin-bottom: 10px!important;
}
</style>
{{-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" /> --}}
<div class="container-fluid">

{{--    @if(isset($services))--}}
{{--        @foreach($services as $service)--}}
{{--            @if(!empty($service['serv']))--}}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="project-details-tab">
                            <ul class="nav nav-pills res-pd-less-sm">

                                @if(isset($services))
                                    @php $ns=1 @endphp
                                    @foreach($services as $service)
                                        @if(!empty($service['serv']))
  <li @if($ns == 1) {{'class=active'}} @endif><a data-toggle="pill" href="@if($service['serv']->user_role->name == "Other Health Services"){{'#other'}}@else{{'#'.$service['serv']->user_role->name}}@endif">{{$service['serv']->user_role->name}}</a>
                                            </li>
                                        @endif
                                        @php $ns++ @endphp
                                    @endforeach
                                @endif

                            </ul>

                            <div class="tab-content res-tab-content-project">

                                @if(isset($services))
                                    @php $n=1 @endphp
                                    @foreach($services as $service)
                                        @if(!empty($service['serv']))
          <div id="@if($service['serv']->user_role->name == "Other Health Services"){{'other'}}@else{{$service['serv']->user_role->name}}@endif"
               class="tab-pane fade in @if($n == 1) {{'active'}} @endif animated zoomInLeft">
                                                <!-- Add New CAtegory Section Starts Here -->
         <div class="col-lg-6 col-md-6 col-sm-12 mt-5">
             <div class="sparkline9-list shadow-reset">
                   <div class="sparkline9-hd">
                  <div class="main-sparkline9-hd">
                  <h1>Add New Category</h1>
                    <div class="sparkline9-outline-icon">
       <span class="sparkline9-collapse-link"><i class="fa fa-chevron-up"></i></span>
        </div>
            </div>
               </div>
                 <div class="sparkline9-graph">
                    <div class="basic-login-form-ad">

                    <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="basic-login-inner">
                      <form action="{{url('omed/addNewCategory')}}" method="post" enctype="multipart/form-data">
                          @method('POST')
                          @csrf
                          <input type="hidden" name="id" value="{{$service['serv']['id']}}">
                          <input type="hidden" name="user_id" value="{{$service['serv']['user_id']}}">
                       <div class="form-group-inner">
                        <div class="row">
                            <div class="col-lg-3">
                                <label class="login2">Categories</label>
                            </div>

                      <div class="col-lg-9">

                          <div class="chosen-select-single">
                              <select class="select2_demo_2 form-control @error('category_id') is-invalid @enderror"  multiple="multiple" name="category_id[]" required style="width:100%;">
                                  @if(count($service['cat_to_add_new']) > 0)
                                      @for($m=0;$m<count($service['cat_to_add_new']);$m++)
                                          <option value="{{$service['cat_to_add_new'][$m]->id}}">
                                              {{ucwords($service['cat_to_add_new'][$m]->title)}}</option>
                                      @endfor
                                  @endif
                              </select>
                              @error('category_id')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                          </div>
                      </div>
                        </div>
                 </div>
                          <div class="form-group-inner">
                              <div class="row">
                                  <div class="col-lg-3">
                                      <label class="login2">Related Documents</label>
                                  </div>
                                  <div class="col-lg-9">
                                      <div class="file-upload-inner file-upload-inner-right ts-forms">
                                          <div class="input append-big-btn">
                                              <label class="icon-left" for="append-big-btn{{$n}}">
                                                  <i class="fa fa-download"></i>
                                              </label>
                                              <div class="file-button">
                                                  Browse
                                  <input type="file" class=" @error('related_document') is-invalid @enderror" id="re-doc{{$n}}" name="related_document[]" multiple="true"
                                         onchange="document.getElementById('append-big-btn{{$n}}').value = this.value;">
                                              </div>
                                              <input type="text" id="append-big-btn{{$n}}" placeholder="no file selected">
                                          </div>
                                          @error('related_document')
                                          <div class="alert alert-danger">{{ $message }}</div>
                                          @enderror
                                      </div>
                                      <span class="mt-5"><small>Allowed file type: jpg, jpeg, png, pdf. Max file size: 500KB. Use </small> <span class="badge badge-secondary"> 'CTRL' </span><small> button for multiple file selection.</small></span>
                                  </div>
                              </div>
                          </div>
                          <div class="login-btn-inner">
                              <div class="row">
                                  <div class="col-lg-3"></div>
                                  <div class="col-lg-9">
                                      <div class="login-horizental">
                                          <button class="btn btn-lg btn-primary login-submit-cs" type="submit">Submit</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
                </div>
              </div>
            </div>
           </div>
          </div>
        </div>

                                                <!-- Add new Categories Section Ends Here -->

                                                <!-- Remove From Selected Category Start Here -->
                                                <div class="col-lg-3 col-md-3 col-sm-12 mt-5">
                                                    <div class="sparkline8-list shadow-reset">
                                                        <div class="sparkline8-hd">
                                                            <div class="main-sparkline8-hd">
                                                              <h1>Remove</h1>
                                                                <div class="sparkline8-outline-icon">
                                                                    <span class="sparkline8-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="sparkline8-graph">
                                                            <div class="basic-login-form-ad">

                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                                        <label class="login2">Remove From This Service Provider Category</label>
                                                                    </div>
                                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                                        <div class="basic-login-inner "   style="margin-top:50px;margin-bottom:20px;">
                                                                            <form action="{{url('omed/removeFromService')}}" method="post">
                                                                                @csrf
                                                                                <input type="hidden" name="id" value="{{$service['serv']['id']}}">
                                                                                <div class="i-checks pull-left">
                                                                                    <label>
                                                                                        <input type="checkbox" class="@error('chk_agree') is-invalid @enderror" id="customCheck{{$n}}" name="chk_agree" required > <i></i> I agree. </label><br>
                                                                                </div>
                                                                                <div class="login-btn-inner">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-3"></div>
                                                                                        <div class="col-lg-9">
                                                                                            <div class="login-horizental">
                                     <button class="btn btn-custon-four btn-danger " type="submit">Submit</button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>


                                                <!-- Remove From Selected Category Ends Here -->

              <!-- Put In Top Category -->
              <div class="col-lg-3 col-md-3 col-sm-12 mt-5">
                  <div class="sparkline11-list shadow-reset">
                      <div class="sparkline11-hd">
                          <div class="main-sparkline11-hd">
                              <h1>Top Category</h1>
                              <div class="sparkline11-outline-icon">
                                  <span class="sparkline11-collapse-link"><i class="fa fa-chevron-up"></i></span>
                              </div>
                          </div>
                      </div>
                      <div class="sparkline11-graph">
                          <div class="basic-login-form-ad">

                              <div class="row">
                                  <div class="col-lg-12 col-md-12 col-sm-12">
                                      <label class="login2">Put this service provider in Top {{$service['serv']->user_role->name}} category.</label>
                                  </div>

                                  <div class="col-lg-12 col-md-12 col-sm-12 mt-5">
                                      <label class="login2">Top {{$service['serv']->user_role->name}} : @if($service['serv']['mark_top'] == 1) <span class="text-success">Active</span>@else<span class="text-danger">Idle</span>@endif</label>
                                  </div>
                                  <div class="col-lg-12 col-md-12 col-sm-12">
                                      <div class="basic-login-inner "  style="margin-top:40px;margin-bottom:20px;">
                                          <form action="{{url('omed/markTopCategory')}}" method="post">
                                              @csrf
                                              <input type="hidden" name="id" value="{{$service['serv']['id']}}">

                                              <div class="login-btn-inner">
                                                  <div class="row">
                                                      <div class="col-lg-3"></div>
                                                      <div class="col-lg-9">
                                                          <div class="login-horizental">
                                                              <button class="btn btn-custon-four btn-danger " type="submit">@if($service['serv']['mark_top'] == 1){{'Mark Idle'}}@else {{'Mark Active'}}@endif</button>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </form>
                                      </div>
                                  </div>
                              </div>

                          </div>
                      </div>
                  </div>
              </div>


              <!-- Put in top category Ends Here -->

                                                <!-- Edit Details Of  selected Category Starts Here -->
                                                <div class="col-lg-8 col-md-8 col-sm-12 mt-5">
                                                    <div class="sparkline10-list shadow-reset">
                                                        <div class="sparkline10-hd">
                                                            <div class="main-sparkline10-hd">
                                                                <h1>Update Details</h1>
                                                                <div class="sparkline10-outline-icon">
                                                                    <span class="sparkline10-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="sparkline10-graph">
                                                            <div class="basic-login-form-ad">

                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                                        <div class="basic-login-inner">
                                                                            <form action="{{url('omed/user-management/'.$service['serv']['id'])}}" method="post">
                                                                                @method('PUT')
                                                                                @csrf

                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-3">
                                                                                            <label class="login2 pull-right pull-right-pro">Categories</label>
                                                                                        </div>
                                                                                        <div class="col-lg-6">
                                                                                            <div class="form-select-list">
                                                                                                <select class="select2_demo_2 form-control  @error('category_id') is-invalid @enderror"
                                                                                                        multiple="multiple" name="category_id[]" required style="width:100%;">
                                                    @if(count($service['cat_to_update']) > 0)
                                            @for($m=0;$m<count($service['cat_to_update']);$m++)
                                                                                                            <option value="{{$service['cat_to_update'][$m]->id}}" {{in_array($service['cat_to_update'][$m]->id,$service['cat'])?'selected':'' }}>
                                                                                                                {{ucwords($service['cat_to_update'][$m]->title)}}</option>
                                                                                                        @endfor
                                                                                                    @endif
                                                                                                </select>
                                                                                                @error('category_id')
                                                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                                                @enderror
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-3">
                                                                                            <label class="login2 pull-right pull-right-pro">Registration No</label>
                                                                                        </div>
                                                                                        <div class="col-lg-6">
                                                                                            <input type="text" class=" form-control" name="reg_no" placeholder="Enter registration number" value="{{$service['serv']['reg_no']}}">

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                @if($service['serv']['user_role_id']==6)
                                                                                    <div class="form-group-inner">
                                                                                        <div class="row">
                                                                                            <div class="col-lg-3">
                                                                                                <label class="login2 pull-right pull-right-pro">Vehicle No</label>
                                                                                            </div>
                                                                                            <div class="col-lg-6">
                                                                                                <input type="text" class=" form-control" name="vehicle_no" placeholder="Enter vehicle number" value="{{$service['serv']['vehicle_no']}}">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    @endif
{{--                                                                                <div class="form-group-inner">--}}
{{--                                                                                    <div class="row">--}}
{{--                                                                                        <div class="col-lg-3">--}}
{{--                                                                                            <label class="login2 pull-right pull-right-pro">Availability Schedule</label>--}}
{{--                                                                                        </div>--}}
{{--                                                                                        <div class="col-lg-6">--}}
{{--                                                                                            <input type="text" class=" form-control" name="availability" placeholder="Mon-Sun 10:00AM - 07:00PM" value="{{$service['serv']['availability']}}">--}}

{{--                                                                                        </div>--}}
{{--                                                                                    </div>--}}
{{--                                                                                </div>--}}

                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-3">
                                                                                            <label class="login2 pull-right pull-right-pro">Minimum Fee</label>
                                                                                        </div>
                                                                                        <div class="col-lg-6">
        <input type="text" class="form-control" name="min_fee" value="{{$service['serv']['min_fee']}}" placeholder="0" required>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-3">
                                                                                            <label class="login2 pull-right pull-right-pro">Maximum Fee</label>
                                                                                        </div>
                                                                                        <div class="col-lg-6">
                                                                                            <input type="text" class="form-control" name="max_fee" value="{{$service['serv']['max_fee']}}" placeholder="0" required>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-3">
                                                                                            <label class="login2 pull-right pull-right-pro">Landmark</label>
                                                                                        </div>
                                                                                        <div class="col-lg-6">
                                                                                            <input type="text" class="form-control" name="landmark" value="{{$service['serv']['landmark']}}" placeholder="Enter landmark" required>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                @if($service['serv']['user_role_id']==6)
                                                                                    <div class="form-group-inner">
                                                                                        <div class="row">
                                                                                            <div class="col-lg-3">
                                                                                                <label class="login2 pull-right pull-right-pro">Longitude</label>
                                                                                            </div>
                                                                                            <div class="col-lg-6">
                                                                                                <input type="text" class="form-control" name="longitude" value="{{$service['serv']['longitude']}}" placeholder="Enter longitude" >
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group-inner">
                                                                                        <div class="row">
                                                                                            <div class="col-lg-3">
                                                                                                <label class="login2 pull-right pull-right-pro">Latitude</label>
                                                                                            </div>
                                                                                            <div class="col-lg-6">
                                                                                                <input type="text" class="form-control" name="latitude" value="{{$service['serv']['latitude']}}" placeholder="Enter latitude" >
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    @endif
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-3">
                                                                                            <label class="login2 pull-right pull-right-pro">State</label>
                                                                                        </div>
                                                                                        <div class="col-lg-4">
                             <select class="select2_demo_2 form-control state-select "   name="state" style="width:100%;" >
                                 <option selected disabled>State</option>
                                 @foreach($states as $s)
                                <option value="{{$s->name}}" >{{ucwords($s->name)}}</option>
                                 @endforeach
                                                                                                </select>
                                                                                        </div>
                                                                                        @if(!empty($service['serv']['state']))
                                                                                        <div class="col-lg-3 ">
                                                                                                <label class="login2 pull-left pull-left-pro">Selected : {{$service['serv']['state']}}</label>
                                                                                        </div>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-3">
                                                                                            <label class="login2 pull-right pull-right-pro">City</label>
                                                                                        </div>
                                                                                        <div class="col-lg-4">
                                                                                            <select class="select2_demo_2 form-control city-select "   name="city" style="width:100%;" >
<option selected disabled>City</option>
                                                                                               </select>
                                                                                        </div>
                                                                                        @if(!empty($service['serv']['city']))
                                                                                            <div class="col-lg-3 ">
                                                                                                <label class="login2 pull-left pull-left-pro">Selected : {{$service['serv']['city']}}</label>
                                                                                            </div>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
{{--                                                                                <div class="form-group-inner">--}}
{{--                                                                                    <div class="row">--}}
{{--                                                                                        <div class="col-lg-3">--}}
{{--                                                                                            <label class="login2 pull-right pull-right-pro">City Fee</label>--}}
{{--                                                                                        </div>--}}
{{--                                                                                        <div class="col-lg-6">--}}
{{--                                                                                            <input type="text" class="form-control" name="city" value="{{$service['serv']['city']}}" placeholder="Enter city" required>--}}

{{--                                                                                        </div>--}}
{{--                                                                                    </div>--}}
{{--                                                                                </div>--}}
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-3">
                                                                                            <label class="login2 pull-right pull-right-pro">Area</label>
                                                                                        </div>
                                                                                        <div class="col-lg-4">
                                                                                            <select class="select2_demo_2 form-control area-select" name="area" style="width:100%;" >
                                                                                                <option selected disabled>Area</option>
                                                                                            </select>
                                                                                        </div>
                                                                                        @if(!empty($service['serv']['area']))
                                                                                            <div class="col-lg-3 ">
                                                                                                <label class="login2 pull-left pull-left-pro">Selected : {{$service['serv']['area']}}</label>
                                                                                            </div>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
{{--                                                                                <div class="form-group-inner">--}}
{{--                                                                                    <div class="row">--}}
{{--                                                                                        <div class="col-lg-3">--}}
{{--                                                                                            <label class="login2 pull-right pull-right-pro">Area</label>--}}
{{--                                                                                        </div>--}}
{{--                                                                                        <div class="col-lg-6">--}}
{{--                                                                                            <input type="text" class="form-control" name="area" value="{{$service['serv']['area']}}" placeholder="Enter area" required>--}}

{{--                                                                                        </div>--}}
{{--                                                                                    </div>--}}
{{--                                                                                </div>--}}



                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-3">
                                                                                            <label class="login2 pull-right pull-right-pro">Address</label>
                                                                                        </div>
                                                                                        <div class="col-lg-6">
                                                                                            <textarea class="form-control" name="address" placeholder="Enter address" required>{{$service['serv']['address']}}</textarea>


                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-3">
                                                                                            <label class="login2 pull-right pull-right-pro">Description</label>
                                                                                        </div>
                                                                                        <div class="col-lg-6">
                                                                                            <textarea class="form-control" name="description" placeholder="enter a brief description for this service category" required>{{$service['serv']['description']}}</textarea>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="hidden" name="id" value="{{$service['serv']['id']}}">
                                                                                <div class="login-btn-inner">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-3"></div>
                                                                                        <div class="col-lg-6">
                                                                                            <div class="login-horizental">
                                                                                                <button class="btn btn-lg btn-primary login-submit-cs" type="submit">Submit</button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>



                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End of Edit Selected Category Details Here -->
              <div class="col-lg-4 col-md-4 col-sm-12 mt-5">
                  <div class="sparkline7-list shadow-reset">
                      <div class="sparkline7-hd">
                          <div class="main-sparkline11-hd">
                              <h1>Manage Contact Buttons</h1>
                              <div class="sparkline7-outline-icon">
                                  <span class="sparkline7-collapse-link"><i class="fa fa-chevron-up"></i></span>
                              </div>
                          </div>
                      </div>
                      <div class="sparkline7-graph">
                          <div class="basic-login-form-ad">

                              <div class="row">
                                  <div class="col-lg-12 col-md-12 col-sm-12">
                                      <label class="login2">Manage Call Now , Live Consultation and Appointment Booking buttons</label>
                                  </div>


                                  <div class="col-lg-12 col-md-12 col-sm-12">
                                      <div class="basic-login-inner "  style="margin-top:40px;margin-bottom:20px;">
                                          <form action="{{url('omed/manageContactButton')}}" method="post">
                                              @csrf
                                              <input type="hidden" name="id" value="{{$service['serv']['id']}}">
                                              <div class="row">
                                                  <div class="col-md-12 col-sm-12">
                                                      <div class="i-checks pull-left">
                                                          <label>
                                                              <input type="checkbox" class="@error('btn_call') is-invalid @enderror" id="btn-call{{$n}}"
                                                                     name="btn_call"  {{$service['serv']['btn_call']==1?'checked':''}}> <i></i> Call Now </label><br>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-12 col-sm-12">
                                                      <div class="i-checks pull-left">
                                                          <label>
                                                              <input type="checkbox" class="@error('btn_booknow') is-invalid @enderror" id="btn-booknow{{$n}}"
                                                                     name="btn_booknow"  {{$service['serv']['btn_booknow']==1?'checked':''}}> <i></i> Book Appointment </label><br>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-12 col-sm-12">
                                                      <div class="i-checks pull-left">
                                                          <label>
                                                              <input type="checkbox" class="@error('btn_video') is-invalid @enderror" id="btn-video{{$n}}"
                                                                     name="btn_video"  {{$service['serv']['btn_video']==1?'checked':''}}> <i></i> Live Consultation </label><br>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-12 col-sm-12">
                                                      <div class="i-checks pull-left">
                                                          <label>
                                                              <input type="checkbox" class="@error('btn_enquiry') is-invalid @enderror" id="btn-enquiry{{$n}}"
                                                                     name="btn_enquiry"  {{$service['serv']['btn_enquiry']==1?'checked':''}}> <i></i> Enquiry </label><br>
                                                      </div>
                                                  </div>
                                              </div>



                                              <div class="login-btn-inner">
                                                  <div class="row">
                                                      <div class="col-lg-3"></div>
                                                      <div class="col-lg-9">
                                                          <div class="login-horizental">
                                                              <button class="btn btn-custon-four btn-danger " type="submit">Submit</button>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </form>
                                      </div>
                                  </div>
                              </div>

                          </div>
                      </div>
                  </div>
              </div>

                                                <div class="user-profile-comment-list">
                                                    <div class="row">

                                                        <div class="col-lg-12 col-md-12 col-sm-12">


                                                        </div>
                                                    </div>




                                                </div>
                                            </div>

                                        @endif
                                        @php $n++ @endphp
                                    @endforeach
                                @endif

                            </div>

                        </div>
                    </div>
                </div>
</div>



