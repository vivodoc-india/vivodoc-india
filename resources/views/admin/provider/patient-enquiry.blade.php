@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="#">User Management</a> <span class="bread-slash"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- Static Table Start -->
    <div class="data-table-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline13-list shadow-reset">
                        <div class="sparkline13-hd">
                            <div class="main-sparkline13-hd">
                                <h1>Patient Enquiries <span class="table-project-n">Data</span> </h1>
                                <div class="sparkline13-outline-icon">
                                    <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{-- <span><i class="fa fa-wrench"></i></span> --}}
                                    {{-- <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span> --}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline13-graph">
                            <div class="datatable-dashv1-list custom-datatable-overright">
                                <div id="toolbar">
                                    <select class="form-control">
                                        <option value="">Export Basic</option>
                                        <option value="all">Export All</option>
                                        <option value="selected">Export Selected</option>
                                    </select>
                                </div>
                                <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                    <tr>
                                        <th data-field="state" data-checkbox="true"></th>
                                        <th data-field="id">S.No.</th>
                                        <th data-field="name">Patient Name</th>
                                        <th data-field="question">Question</th>
                                        <th data-field="status">Status</th>
                                        <th data-field="action">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $num=1;?>
                                    @if(isset($record))
                                        @foreach($record as $rr)
                                            <tr>
                                                <td></td>
                                                <td>{{$num}}</td>
                                                <td>{{ucwords($rr->patient->name)}}</td>
                                                <td>
                                                    {{ucwords($rr->question)}}
                                                        </td>
  <td>@if($rr->provider_reply == '' && $rr->admin_reply == '')<span class="text-danger">Not Answered</span> @else<span class="text-primary">Answered</span>@endif</td>
                                                <td>
                                                    <div class="btn-group project-list-action">
  <button class="btn btn-white btn-action btn-xs blue"> <a href="{{url('omed/user-management/show-enquiry/'.$rr->id)}}"  title="View"><i class="fa fa-folder
"></i>View</a></button>
                                                        <form action="{{url('omed/user-management/remove-enquiry')}}"
                                                              method="post">
                                                            <input type="hidden" name="record_id" value="{{$rr->id}}">
                                                            @csrf
  <button type="submit" class="btn btn-white btn-action btn-xs danger"> <i class="fa
                                fa-trash "></i> Remove</button>
                                                        </form>
                                                    </div>
                                                </td>
                                                @php $num++ @endphp
                                            </tr>
                                        @endforeach

                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Static Table End -->
@endsection
