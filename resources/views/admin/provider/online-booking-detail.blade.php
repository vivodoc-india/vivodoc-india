@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashboard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li>  <a href="{{url('omed/user-management')}}">User Management</a>
                                <span
                                    class="bread-slash">/</span>
                            </li>

                            <li>    <a href=""> Live Consultation Booking Detail</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="basic-form-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline12-list shadow-reset mg-t-30">
                        <div class="sparkline12-hd">
                            <div class="main-sparkline12-hd">
                                <h1>Booking Detail</h1>
                                <div class="sparkline12-outline-icon">
                                    <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{--                                    <span><i class="fa fa-wrench"></i></span>--}}
                                    {{--                                    <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>--}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">

                                <div class="row mb-3" >
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-12" style="text-align: left;text-decoration: underline;color:#007ee5;">
                                            <h4><strong>Patient's Details</strong></h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Patient Name :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->patient_name)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Patient Age :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->age)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Patient Gender :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->gender)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Patient Address :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->address)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Service Provider :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span class="text-success" style="font-weight:bold;">{{ucwords($detail->provider->name)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Booked Date :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span class="text-success" style="font-weight:bold;">{{date
                                                        ('d-m-Y',strtotime
                                                        ($detail->booking_date))
                                                        }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Booked By :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span style="font-weight:bold;">{{ucwords
                                                        ($detail->patient->name)
                                                        }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Booked On :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{date('d-m-Y',strtotime($detail->created_at))}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12">

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>BP :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{$detail->bp}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Pulse :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{$detail->pulse}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Temperature :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{$detail->temperature}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>SPO2 :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{$detail->spo}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Weight :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{$detail->weight}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>History :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{$detail->history}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Complain :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->complain)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Diagnosic :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->diagnostic)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <hr>

                                <div class="row mb-3">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-12" style="text-align: left;text-decoration: underline;color:#007ee5;">
                                            <h4><strong>Request Details</strong></h4>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Is Patient Send Request ? :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>
                                                            <a href="javascript:void(0);" onclick="changeFlag2(this,'online_bookings','patient_requested',{{$detail->id}});"
                                                               class=" @if ($detail->patient_requested==1) {{'green'}} @else {{'danger'}} @endif ">
                                                                @if ($detail->patient_requested==1)
                                                                    <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                                                                @else <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                                                                @endif
                                                            </a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Is Service Provider Reject Request ? :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>
                                                            <a href="javascript:void(0);" onclick="changeFlag2(this,'online_bookings','is_request_rejected',{{$detail->id}});"
                                                               class=" @if ($detail->is_request_rejected==1) {{'green'}} @else {{'danger'}} @endif ">
                                                                @if ($detail->is_request_rejected==1)
                                                                    <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                                                                @else <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                                                                @endif
                                                            </a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Is Request Accepted By Service Provider ? :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>
                                                            <a href="javascript:void(0);" onclick="changeFlag2(this,'online_bookings','is_request_accepted',{{$detail->id}});"
                                                               class=" @if ($detail->is_request_accepted==1) {{'green'}} @else {{'danger'}} @endif ">
                                                                @if ($detail->is_request_accepted==1)
                                                                    <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                                                                @else <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                                                                @endif
                                                            </a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row mb-3">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-12" style="text-align: left;text-decoration: underline;color:#007ee5;">
                                            <h4><strong>Conference Details</strong></h4>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Is Conference Link Available ? :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>
                                                            <a href="javascript:void(0);" onclick="changeFlag2(this,'online_bookings','is_con_link_available',{{$detail->id}});"
                                                               class=" @if ($detail->is_con_link_available==1) {{'green'}} @else {{'danger'}} @endif ">
                                                                @if ($detail->is_con_link_available==1)
                                                                    <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                                                                @else <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                                                                @endif
                                                            </a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>LiveBox Conference Hash :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>
                                                         {{$detail->con_hash}}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>LiveBox Conference Name :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>
                                                         {{$detail->con_name}}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Is Conference Link Access Allowed :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>
                                                            <a href="javascript:void(0);" onclick="changeFlag2(this,'online_bookings','con_link_access',{{$detail->id}});"
                                                               class=" @if ($detail->con_link_access==1) {{'green'}} @else {{'danger'}} @endif ">
                                                                @if ($detail->con_link_access==1)
                                                                    <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                                                                @else <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                                                                @endif
                                                            </a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Live Box Conference Creation Response :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>
                                                            {{ $detail->con_response }}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Is Conference Link Expired ? :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>
                                                            <a href="javascript:void(0);" onclick="changeFlag2(this,'online_bookings','is_con_link_expired',{{$detail->id}});"
                                                               class=" @if ($detail->is_con_link_expired==1) {{'green'}} @else {{'danger'}} @endif ">
                                                                @if ($detail->is_con_link_expired==1)
                                                                    <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                                                                @else <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                                                                @endif
                                                            </a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>LiveBox Conference Link :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>
                                                      <a href="{!! $detail->con_link  !!}" target="_blank">{!! $detail->con_link !!}</a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>LiveBox Conference Link Created On :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>
                                                         {{date('d-m-Y H:i:s',strtotime($detail->con_created_at))}}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Service Provider's Charge For Live Consultation :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>
                                                         <i class="fa fa-inr" aria-hidden="true"></i> {{$detail->cons_amount}}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row mb-3">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-12" style="text-align: left;text-decoration: underline;color:#007ee5;">
                                            <h4><strong>Payment Details</strong></h4>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Is Payment Done ? :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">

                                                        <span>
                                                            <a href="javascript:void(0);" onclick="changeFlag2(this,'online_bookings','is_paid',{{$detail->id}});"
                                                               class=" @if ($detail->is_paid==1) {{'green'}} @else {{'danger'}} @endif ">
                                                                @if ($detail->is_paid==1)
                                                                    <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                                                                @else <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                                                                @endif
                                                            </a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Transaction Amount :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                       <span>
       <i class="fa fa-inr" aria-hidden="true"></i> {{ $detail->txn_amount }}
                            </span>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Transaction Response From CashFree :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                       <span>
                                   {{ $detail->txn_response }}
                            </span>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Who Pay :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                       <span>
                                   {{ $detail->who_pay }}
                            </span>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Transaction Number :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                       <span>
                                   {{ $detail->txn_number }}
                            </span>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Transaction Date :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                       <span>
                                   {{ $detail->txn_date }}
                            </span>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Transaction Status :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                       <span>
                                   {{ $detail->txn_status }}
                            </span>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row mb-3">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-12" style="text-align: left;text-decoration: underline;color:#007ee5;">
                                            <h4><strong>Prescription</strong></h4>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <a href="{{url('omed/view-prescription/'.$detail->id)}}" class="btn btn-lg btn-primary login-submit-cs" target="_blank"><i class="fa
    fa-eye fa-lg" aria-hidden="true"></i> View Prescription</a>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

