@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li>  <a href="{{url('omed/user-management')}}">User Management</a> <span class="bread-slash">/</span>
                            </li>
                            <li>    <a href="#"> Enquiry Detail</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="basic-form-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline12-list shadow-reset mg-t-30">
                        <div class="sparkline12-hd">
                            <div class="main-sparkline12-hd">
                                <h1>Enquiry Detail</h1>
                                <div class="sparkline12-outline-icon">
                                    <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{--                                    <span><i class="fa fa-wrench"></i></span>--}}
                                    {{--                                    <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>--}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row mb-3" >
                                    <div class="col-lg-12">
                                        <div align="left"><h5>Q. {{ucwords($detail->question)}}</h5></div>
                                    </div>
                                    @if($detail->provider_reply!='' || $detail->admin_reply!='')
                                        <div class="col-lg-11 col-lg-offset-1 " align="left">

                                            @if($detail->provider_reply!='')  <h5 class="h6-responsive"> <span><i
                                                        class="fa fa-reply" aria-hidden="true"></i></span> {{ucfirst($detail->provider_reply)}}</h5>
                                            <p>Replied By: {{ucwords($detail->provider->name)}} &nbsp;&nbsp; {{date('d-m-Y',
                                            strtotime($detail->updated_at))}}</p>
                                            @endif
                                            @if($detail->admin_reply!='')  <h5 class="h6-responsive"><span><i
                                                            class="fa fa-reply" aria-hidden="true"></i></span> {{ucfirst($detail->admin_reply)}}</h5>
                                            <p class="ml-2">Replied By: Admin  {{date('d-m-Y',strtotime($detail->admin_updated_at))}}</p>
                                            @endif
                                        </div>
                                    @endif


                                </div>
                                <div class="row mt-10">
                                    <div class="col-lg-12">
                                        <div class="all-form-element-inner">

                                            <form action="{{url('omed/user-management/admin-reply')}}" method="post">
                                                @csrf
                                                <input type="hidden" name="reply_id" value="{{$detail->id}}">
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right
                                                            pull-right-pro">Admin Reply</label>
                                                        </div>
                                                        <div class="col-lg-9">
                           <textarea name="admin_reply" class="form-control" required placeholder="Enter your reply"></textarea>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <div class="login-btn-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3"></div>
                                                            <div class="col-lg-6">
                                                                <div class="login-horizental cancel-wp pull-left">
                                                                    <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>
                                                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit">Update
                                                                       </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

