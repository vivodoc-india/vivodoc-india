@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="{{url('omed/patient-management')}}">Patient Management</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="#">OMED Coins </a> <span class="bread-slash"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="basic-form-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline12-list shadow-reset mg-t-30">
                        <div class="sparkline12-hd">
                            <div class="main-sparkline12-hd">
                                <h1>Add / Update Coins</h1>
                                <div class="sparkline12-outline-icon">
                                    <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{--                                    <span><i class="fa fa-wrench"></i></span>--}}
                                    {{--                                    <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>--}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-md-6" align="left">
                                        <h3>{{$patient->name}}</h3>
                                    </div>
                                    <div class="col-md-6">
                                        <h3 class="text-primary">Current OMED Coins: ₹{{$patient->wallet_amount}}</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="all-form-element-inner">
    <div class="col-lg-12 col-md-12 col-sm-12 mt-5">
        <div class="basic-login-form-ad">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="basic-login-inner">
                        <form action="{{route('omed-coins.store')}}" method="post">
                            @csrf
                            <input type="hidden" name="user_id" value="{{$patient->id}}">
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="login2 pull-right pull-right-pro">Amount</label>
                                    </div>
                                    <div class="col-lg-6">
                                        <input type="text" name="amount" class="form-control
@error('amount') is-invalid @enderror" placeholder="Enter Amount" required oninput='this.value=(parseInt(this.value)||" ")'
                                               maxlength="6" value="" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="login2 pull-right pull-right-pro">Type of action</label>
                                    </div>
                                    <div class="col-lg-6">
                                        <select class="form-control @error ('action_type') is-invalid @enderror" name="action_type" required>
                                            <option selected disabled>Choose any one</option>
                                            <option value="credit"> Credit</option>
                                            <option value="debit">Debit</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="login2 pull-right pull-right-pro">Remarks</label>
                                    </div>
                                    <div class="col-lg-6">
                                        <textarea name="remarks" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="login-btn-inner">
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-6">
                                        <div class="login-horizental">
                                            <button class="btn btn-lg btn-primary login-submit-cs" type="submit">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <!-- Static Table Start -->
    <div class="data-table-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline13-list shadow-reset">
                        <div class="sparkline13-hd">
                            <div class="main-sparkline13-hd">
                                <h1>All Transactions <span class="table-project-n">Data</span> </h1>
                                <div class="sparkline13-outline-icon">
                                    <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{-- <span><i class="fa fa-wrench"></i></span> --}}
                                    {{-- <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span> --}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline13-graph">
                            <div class="datatable-dashv1-list custom-datatable-overright">
                                <div id="toolbar">
                                    <select class="form-control">
                                        <option value="">Export Basic</option>
                                        <option value="all">Export All</option>
                                        <option value="selected">Export Selected</option>
                                    </select>
                                </div>
                                <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                    <tr>
                                        <th data-field="state" data-checkbox="true"></th>
                                        <th data-field="id">S.No.</th>
                                        <th data-field="date">Date</th>
                                        <th data-field="amount">Amount</th>
                                        <th data-field="status">Status</th>
                                        <th data-field="action">Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $num=1;?>
                                    @if(isset($transaction))
                                        @foreach($transaction as $trans)
                                            <tr>
                                                <td></td>
                                                <td>{{$num}}</td>
                                                <td>{{date('d-m-Y',strtotime($trans->transaction_date))}}</td>
                                                <td>
                                                    @if($trans->transaction_type=="credit")
                                                        <span class="text-success">+  ₹{{$trans->transaction_amount}}</span>
                                                    @endif
                                                    @if($trans->transaction_type=="debit")
                                                        <span class="text-danger">-  ₹{{$trans->transaction_amount}}</span>
                                                    @endif
                                                </td>
                                                <td>{{$trans->transaction_status}}</td>

                                                <td>
                                                    <div class="btn-group project-list-action">
                                                        <button class="btn btn-white btn-action btn-xs blue">
                                                            <a href="{{route('omed-coins.show',$trans->id)}}"
                                                               title="View"> <i class="fa fa-folder "></i> View</a></button>

                                                        <button class="btn btn-white btn-action btn-xs danger">
                                                            <a href="javascript:void(0)" data-url="{{route('omed-coins.destroy',$trans->id)}}"
                                                               data-id="{{$trans->id}}" data-toggle="modal" data-target="#WarningModalhdbgcl"
                                                               class=" delete_button_action" title="Remove">
                                                                <i class="fa fa-trash "></i> Remove</a></button>
                                                    </div>
                                                    {{--    <div class="fr">--}}
                                                    {{--    <a href="javascript:void(0)" data-url="{{route('user-management.destroy',$trans->id)}}" data-id="{{$trans->id}}" data-toggle="modal" data-target="#WarningModalhdbgcl" class="danger m5 delete_button_action" title="Remove"> <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i> </a>--}}
                                                    {{--                                        </div>--}}
                                                </td>
                                                <?php $num++;?>
                                            </tr>
                                        @endforeach

                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Static Table End -->
@endsection

