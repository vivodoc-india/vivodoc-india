@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li>  <a href="{{url('omed/patient-management')}}">Patient Management</a> <span class="bread-slash">/</span>
                            </li>
                            <li>  <a href="{{url('omed/patient-management')}}">OMED Coins</a> <span class="bread-slash">/</span>
                            </li>
                            <li>   <a href="#">Show</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')

    <div class="project-details-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="project-details-wrap shadow-reset">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                <div class="project-details-title">
                                    <h2><span class="profile-details-name-nn">Patient Data</span> </h2>
                                </div>

                            </div>


                        </div>
                        <!-- Basic Details Starts Here -->


                                @if(isset($transaction) && $transaction!='')
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="control-label">Transaction Date </label><br>
                                            <span>{{date('d-m-Y',strtotime($transaction->transaction_date))}}</span>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="control-label">Transaction Id</label><br>
                                            <span>{{$transaction->transaction_number}}</span>

                                        </div>
                                        <div class="col-md-4">
                                            <label class="control-label">
                                                Transaction Amount
                                            </label><br>
                                            <span>Rs. {{$transaction->transaction_amount}}</span>
                                        </div>

                                        <div class="col-md-4 mt-3">
                                            <label class="control-label">
                                                Transaction Status
                                            </label><br>
                                            <span>{{$transaction->transaction_status}}</span>
                                        </div>
                                        <div class="col-md-4 mt-3">
                                            <label class="control-label">
                                                Transaction Type
                                            </label><br>
                                            <span>{{$transaction->transaction_type}}</span>
                                        </div>
                                        <div class="col-md-4 mt-3">
                                            <label class="control-label">Reference Id</label><br>
                                            <span>{{$transaction->referenceId}}</span>
                                        </div>
                                        <div class="col-md-12 mt-3">
                                            <label class="control-label">Remarks</label><br>
                                            <span>{{$transaction->transaction_response}}</span>
                                        </div>
                                    </div>
                                @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>


@endsection
