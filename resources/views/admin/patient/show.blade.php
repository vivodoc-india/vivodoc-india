@extends('layouts.master')
@section('breadcumb')
<!-- Breadcome start-->
<div class="breadcome-area mg-b-30 small-dn">
<div class="container-fluid">
<div class="row">
<div class="col-lg-12">
<div class="breadcome-list map-mg-t-40-gl shadow-reset">
<ul class="breadcome-menu">
<li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
</li>
<li>  <a href="{{url('omed/patient-management')}}">Patient Management</a> <span class="bread-slash">/</span>
</li>
<li>   <a href="#">Show</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
@endsection
@section('content')

<div class="project-details-area mg-b-15">
<div class="container-fluid">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="project-details-wrap shadow-reset">
<div class="row">
<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
<div class="project-details-title">
<h2><span class="profile-details-name-nn">Patient Data</span> </h2>
</div>

</div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <div class="admin-comment-month project-details-action">
            <button class="comment-setting" data-toggle="collapse" data-target="#adminpro-demo2">More <i class="fa fa-caret-down" aria-hidden="true"></i>
            </button>
            <ul id="adminpro-demo2" class="comment-action-st collapse">
{{--                <li><a href="{{url('omed/service-provider/ratings/'.$user->id)}}" target="__blank">Reviews & Ratings</a>--}}
{{--                </li>--}}
{{--                <li><a href="{{url('omed/service-provider/patient-enquiries/'.$user->id)}}" target="__blank">Patient--}}
{{--                        Enquiries</a>--}}
{{--                </li>--}}
                <li><a href="{{url('omed/patient/offline-bookings/'.$user->id)}}" target="__blank">Appointment Bookings</a>
                </li>
                <li><a href="{{url('omed/patient/online-bookings/'.$user->id)}}" target="__blank">Live Consultations</a>
                </li>
                <li><a href="{{url('omed/patient/omed-coins/'.$user->id)}}" target="__blank">OMED Coin</a>
                </li>
            </ul>
        </div>
    </div>

</div>
<!-- Basic Details Starts Here -->
 @if(isset($user))
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
{{--<div class="project-details-mg">--}}
{{--<div class="row">--}}
{{--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">--}}
{{--    <div class="project-details-st">--}}
{{--        <span><strong>Status:</strong></span>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">--}}
{{--    <div class="btn-group project-list-ad">--}}
{{--        <button class="btn btn-white btn-xs @if ($user->approval==0){{'bg-danger'}} @endif" onclick="changeFlag3(this,'users','approval',{{$user->id}});">@if ($user->approval==1) {{'Approved'}} @else {{'Not Approved'}} @endif</button>--}}
{{--    </div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
<div class="project-details-mg">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="project-details-st">
        <span><strong>Name:</strong></span>
    </div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="project-details-dt">
        <span>{{ucwords($user->name)}}</span>
    </div>
</div>
</div>
</div>
<div class="project-details-mg">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="project-details-st">
        <span><strong>Email ID:</strong></span>
    </div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="project-details-dt">
        <span>{{$user->email}}</span>
    </div>
</div>
</div>
</div>
<div class="project-details-mg">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="project-details-st">
        <span><strong>Mobile No.:</strong></span>
    </div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="project-details-dt">
      <span>{{$user->mobile}}</span>
    </div>
</div>
</div>
</div>
<div class="project-details-mg">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="project-details-st">
        <span><strong>Gender:</strong></span>
    </div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="project-details-dt">
        <span>{{$user->profile->gender}}</span>
    </div>
</div>
</div>
</div>
<div class="project-details-mg">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="project-details-st">
        <span><strong>Blood Group:</strong></span>
    </div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="project-details-dt">
        <span>{{$user->profile->blood_group}}</span>
    </div>
</div>
</div>
</div>
<div class="project-details-mg">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="project-details-st">
        <span><strong>Address:</strong></span>
    </div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="project-details-dt">
        <span>Landmark- {{$user->profile->landmark}} , Area- {{$user->profile->area}}, Address- {{$user->profile->address}}, {{$user->profile->state}},{{$user->profile->pincode}}</span>
    </div>
</div>
</div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="project-details-mg">
<div class="row">
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <a href="{{asset('uploads/profile/'.$user->profile->photo)}}" target="__blank"><img src="{{asset('uploads/profile/'.$user->profile->photo)}}" class="img-responsive img-thumbnail" style="width:100px;"/></a>
</div>
</div>
</div>



{{-- <div class="project-details-mg">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="project-details-st">
        <span><strong>Phone:</strong></span>
    </div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="project-details-dt">
        <span>01962067309</span>
    </div>
</div>
</div>
</div> --}}
{{-- <div class="project-details-mg">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="project-details-st">
        <span><strong>Participants:</strong></span>
    </div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="project-details-img">
        <a href="#"><img src="img/notification/1.jpg" alt="" />
        </a>
        <a href="#"><img src="img/notification/2.jpg" alt="" />
        </a>
        <a href="#"><img src="img/notification/3.jpg" alt="" />
        </a>
        <a href="#"><img src="img/notification/4.jpg" alt="" />
        </a>
        <a href="#"><img src="img/notification/5.jpg" alt="" />
        </a>
    </div>
</div>
</div>
</div> --}}
</div>
</div>
@endif

<!-- Basic Details End Here -->
{{-- <div class="row">
<div class="col-lg-2">
<div class="project-pregress-details">
<span><strong>Completed:</strong></span>
</div>
</div>
<div class="col-lg-10">
<div class="skill-content-3 project-details-progress">
<div class="skill">
<div class="progress">
    <div class="progress-bar wow fadeInLeft" data-progress="95%" style="width: 95%;" data-wow-duration="1.5s" data-wow-delay="1.2s"> <span>95%</span>
    </div>
</div>
</div>
</div>
</div>
</div> --}}
<div class="row">
<div class="col-lg-12">
<div class="project-details-tab">
<ul class="nav nav-tabs res-pd-less-sm">
    <li class="active" ><a data-toggle="tab" href="#tab1">Medical History</a></li>
</ul>
<div class="tab-content res-tab-content-project">
    <div class="data-table-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline13-list shadow-reset">
                        <div class="sparkline13-hd">
                            <div class="main-sparkline13-hd">
                                <h1>All Patients <span class="table-project-n">Data</span> </h1>
                                <div class="sparkline13-outline-icon">
                                    <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{-- <span><i class="fa fa-wrench"></i></span> --}}
                                    {{-- <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span> --}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline13-graph">
                            <div class="datatable-dashv1-list custom-datatable-overright">
                                <div id="toolbar">
                                    <select class="form-control">
                                        <option value="">Export Basic</option>
                                        <option value="all">Export All</option>
                                        <option value="selected">Export Selected</option>
                                    </select>
                                </div>
                                <table id="table" data-toggle="table" data-pagination="true" data-search="true"
                                       data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true"
                                       data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                       data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true"
                                       data-toolbar="#toolbar">
                                    <thead>
                                    <tr>
                                        <th data-field="id">S.No.</th>
                                        <th data-field="doc_type">Document Type</th>
                                        <th data-field="upload">Uploaded On</th>
                                        <th data-field="action">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $n=1 @endphp
                                    @if(isset($medical_data))
                                        @if(count($medical_data) > 0)
                                            @foreach($medical_data as $md)
                                                <tr>
                                                    <td>{{$n}}</td>
                                                    <td>{{ucwords($md->data_type)}}</td>
                                                    <td>{{date('d-m-Y',strtotime($md->created_at))}}</td>
                                                    <td>
                                                        <button class="btn btn-white btn-action btn-xs blue"> <a href="{{url('omed/show-medical-data/'.$md->id)}}"  title="View" target="__blank"> <i class="fa fa-folder "></i> Brows Directory</a></button>
                                                        {{--                            <button class="btn btn-white btn-action btn-xs green"><a href="{{action('Admin\PatientManagementController@edit',$sp->id)}}"  title="Edit"><i class="fa fa-pencil "></i> Edit </a></button>--}}
                                                        <form action="{{url('omed/patient-management/remove-directory')}}" method="post">
                                                            <input type="hidden" name="record_id" value="{{$md->id}}">
                                                            @csrf
                                                            <button type="submit" class="btn btn-white btn-action btn-xs danger"> <i class="fa
                                fa-trash "></i>
                                                                Remove Directory</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                @php $n++ @endphp
                                            @endforeach
                                        @endif
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
    </div>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>


@endsection
