{{ ! list(, $action) =explode('@', Route::getCurrentRoute()->getActionName()) }}
<style>
.box1, .box2{
    width: 48%;
    padding-left: 1%;
    float: left;
}
</style>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5>Company</h5>
                </div>
                <div class="widget-content nopadding">
                    {{csrf_field()}}
                    <div class="control-group">
                        <label class="control-label">Category<spam> * </spam>:</label>
                        <div class="controls">
                            <select class="form-control  show-tick" multiple name="category_id[]" size='10' multiple="multiple" >
                                <option value="0">None</option>
                                @foreach($category_list as $key=>$value)
                                    @if(in_array($key,$company_category_array))
                                        @php  $selected='selected' @endphp
                                    @else @php  $selected='' @endphp
                                    @endif
                                    <option value="{{$key}}" {{$selected}}>{!! $value !!}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Country<spam> * </spam>:</label>
                        <div class="controls">
                            <span class="append_service1">
                                {{Form::select('country_id',$country_list->toarray(),null,['class'=>'form-control input-sm ','required'=>'""'])}}
                            </span>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Location<spam> * </spam>:</label>
                        <div class="controls">
                            <span class="append_service1">
                                {{Form::select('state_id',[''=>'Select']+$state_list->toarray(),null,['class'=>'form-control input-sm ','id'=>'state','required'=>'""'])}}
                            </span>
                            <span class="append_service1">
                                {{Form::select('city_id',$city_list,null,['class'=>'form-control input-sm ','id'=>'city','required'=>'""'])}}
                            </span>
                            <span class="append_service1">
                                {{Form::select('area_id',$area_list,null,['data-show-subtext'=>"true", 'data-live-search'=>"true", 'class'=>'selectpicker   ','id'=>'area','required'=>'""'])}}
                            </span>
                            @if ($errors->has('area_id'))
                                <span class="form_error">
                                        <strong>{{ $errors->first('area_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Pincode :</label>
                        <div class="controls">
                            {{ Form::number('pincode',null,[ ])}}
                            @if ($errors->has('pincode'))
                                <span class="form_error">
                                        <strong>{{ $errors->first('pincode') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label">Reg No:</label>
                        <div class="controls">
                            {{ Form::text('reg_no',null,[])}}
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Company Name<spam> * </spam>:</label>
                        <div class="controls">
                            {{ Form::text('name',null,['required'=>'""'])}}
                            @if ($errors->has('name'))
                                <span class="form_error">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Company Address:</label>
                        <div class="controls">
                            {{ Form::text('address',null,[])}}
                            @if ($errors->has('address'))
                                <span class="form_error">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="control-group" id="default_date">
                        <label class="control-label">Company Establish:</label>
                        <div class="controls">
                            {{ Form::text('establish',null,[])}}
                            @if ($errors->has('establish'))
                                <span class="form_error">
                                        <strong>{{ $errors->first('establish') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Contact Person Name:</label>
                        <div class="controls">
                            {{ Form::text('contact_name',null,[])}}
                            @if ($errors->has('contact_name'))
                                <span class="form_error">
                                        <strong>{{ $errors->first('contact_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Company Phone<spam> * </spam>:</label>
                        <div class="controls">
                            {{ Form::number('phone',null,['required'=>'""'])}}
                            @if ($errors->has('phone'))
                                <span class="form_error">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                            @endif
                        </div>
						<div class="controls">
                            {{ Form::number('phone1',null,[])}}
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Company Email :</label>
                        <div class="controls">
                            {{ Form::email('email',null,[ ])}}
                            @if ($errors->has('email'))
                                <span class="form_error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Company Website:</label>
                        <div class="controls">
                            {!! Form::text('website',null) !!}
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Experience:</label>
                        <div class="controls">
                            {{ Form::text('experience',null,[])}}
                            @if ($errors->has('experience'))
                                <span class="form_error">
                                        <strong>{{ $errors->first('experience') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Company Description:</label>
                        <div class="controls">
                            {!! Form::textarea('description',null,['style'=>'width:300px !important;height:100px !important;']) !!}
                        </div>
                    </div>
                    <div class="control-group">
                            <label class="control-label">Image :</label>
                            <div class="controls">
                                <div class="uploader" id="uniform-undefined">
                                    {{Form::file('upload',array('size'=>'19','style'=>'opacity: 0;'))}}
                                    <span class="filename">No file selected</span><span class="action">Choose File
                                    </span></div>
                                @if(isset($company->image) && File::exists('media/provider/'.$company->image))
                                    <img src="{{asset('media/provider/'.$company->image)}}" style="width:100px;"/>
                                @endif
                                <span class="date badge badge-success">Size: 100 * 100 </span>
                            </div>
                        </div>
                    <div class="control-group">
                        <label class="control-label">Status :</label>
                        <div class="controls">
                            <label><div class="radio" id="uniform-undefined"><span>{{Form::radio('status','1',true,['style'=>'opacity: 0;'])}}</span></div>Active</label>
                            <label><div class="radio" id="uniform-undefined"><span>{{Form::radio('status','0',false,['style'=>'opacity: 0;'])}}</span></div>Idle</label>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="save" value="yes" class="btn btn-success">
                            @if($action=='create') Save
                            @else Update
                            @endif
                        </button>
                        <a href="{{url('/provider')}}" type="button" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
$('#category_list').on('change',function(){
    var categoryID = $(this).val();
    if(categoryID){
        $.ajax({
            type:"GET",
            url:"{{url('ajax/get-subcategory-list')}}?category_id="+categoryID,
            success:function(res){
                if(res){
                    $("#sub_category_list").empty();
                    $("#sub_category_list").append('<option value="" disabled selected>Select Sub Category</option>');
                    $.each(res,function(key,value){
                        $("#sub_category_list").append('<option value="'+key+'">'+value+'</option>');
                    });

                }else{
                    $("#sub_category_list").empty();
                }
            }
        });
    }else{
        $("#sub_category_list").empty();
    }
});
$('#sub_category_list').on('change',function(){
    var categoryID = $(this).val();
    if(categoryID){
        $.ajax({
            type:"GET",
            url:"{{url('ajax/get-subcategory-filter')}}?category_id="+categoryID,
            success:function(res){
                if(res){
                    $(".append_service").show();
                    $("#filter_list").empty();
                    $.each(res,function(key,value){
                        $("#filter_list").append('<option value="'+value+'">'+value+'</option>');
                    });

                }else{
                    $("#filter_list").empty();
                    $(".append_service").hide();
                }
            }
        });
    }else{
        $("#sub_category_list").empty();
    }
});
$('#state').on('change',function(){
    var stateID = $(this).val();
    if(stateID){
        $.ajax({
            type:"GET",
            url:"{{url('ajax/get-city-list')}}?state_id="+stateID,
            success:function(res){
                if(res){
                    $("#city").empty();
                    $("#city").append('<option value="" disabled selected>Select City</option>');
                    $.each(res,function(key,value){
                        $("#city").append('<option value="'+key+'">'+value+'</option>');
                    });

                }else{
                    $("#city").empty();
                }
            }
        });
    }else{
        $("#city").empty();
    }
});
$('#city').on('change',function(){
    var cityID = $(this).val();
    if(cityID){
        $.ajax({
            type:"GET",
            url:"{{url('ajax/get-area-list')}}?city_id="+cityID,
            success:function(res){
                if(res){
                    $("#area").empty();
                    $("#area").append('<option value="" disabled selected>Select Area</option>');
                    $.each(res,function(key,value){
                        $("#area").append('<option value="'+key+'">'+value+'</option>');
                    });

                }else{
                    $("#area").empty();
                }

                $('.selectpicker').selectpicker('refresh');
            }
        });
    }else{
        $("#area").empty();
    }
});
jQuery(function($){
    var demo1 = $('select[name="category_id[]"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">Filtered</span>'});
    var container1 = demo1.bootstrapDualListbox('getContainer');
    container1.find('.btn').addClass('btn-white btn-info btn-bold');

    $(document).one('ajaxloadstart.page', function(e) {
        $('[class*=select2]').remove();
        $('select[name="category_id[]"]').bootstrapDualListbox('destroy');
        $('.rating').raty('destroy');
        $('.multiselect').multiselect('destroy');
    });

});
</script>
