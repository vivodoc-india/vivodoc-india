@extends('layouts.master')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb">
                <a href="{{url('omed/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
                <a href="{{url('user-management')}}">User Management</a>
                <a href="#" class="current">Show</a>
            </div>
            <h1> <a href="{{url('omed/user-management')}}" class="btn btn-inverse btn-mini">Back</a></h1>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>User Data</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered">
                                <tbody>
                                <tr><th>Name</th> <td>{{$company->name}}</td> </tr>
                                <tr><th>Address</th> <td>{{$company->address}} Country- {{$company->country_name}} State- {{$company->state_name}}
                                        City- {{$company->city_name}} Area- {{$company->area_name}} Pincode- {{$company->pincode}}</td> </tr>
                                <tr><th>Industry</th> <td>{{$category_list}}  </td> </tr>
                                <tr><th>Established</th> <td>{{$company->establish}}</td> </tr>
                                <tr><th>Experience</th> <td>{{$company->experience}}</td> </tr>
                                <tr><th>Contact</th> <td>{{$company->phone}}   -   {{$company->phone1}}</td> </tr>
                                <tr><th>Email</th> <td>{{$company->email}}</td> </tr>
                                <tr><th>Contact Person</th> <td>{{$company->contact_name}}</td> </tr>
                                <tr><th>Status</th> <td>@if($company->status==1) Active @else Deactive @endif</td> </tr>
                                <tr><th>Registration Date</th> <td>{{$company->created_at}}</td> </tr>

                                @if(File::exists('media/provider/'.$company->image) && $company->image)
                                <tr><th>Logo</th> <td>
                                <img src="{{asset('media/provider/'.$company->image)}}" style="width:100px;" />
                                </td> </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>Company Review/Rating</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                <thead><tr><th>Name</th><th>Rating</th><th>Review</th> </tr></thead>
                                <tbody>
                                    @foreach($review as $list)
                                        <tr><td><a href="{{url('customer/'.$list->id)}}">{{$list->name}}</a></td>
                                            <td>@for($i=0;$i<$list->rating;$i++)
                                                    <img src="{{asset('img/star.png')}}" />
                                                @endfor</td>
                                            <td>{{$list->review}}</td></tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>Customer Enquiry</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                    <thead><tr><th>Customer</th><th>Date</th><th>Details</th><Th>Address</Th> </tr></thead>
                                        <tbody>
                                            @foreach($enquiry as $list)
                                                <tr><td><a href="{{url('customer/'.$list->customer_id)}}">{{$list->name}}</a></td>
                                                    <td>{{$list->created_at}}</td>
                                                    <td>{{$list->details}}</td>
                                                    <td>{{$list->address}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
