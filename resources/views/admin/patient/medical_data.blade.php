@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li>  <a href="{{url('omed/patient-management')}}">Patient Management</a> <span class="bread-slash">/</span>
                            </li>
                            <li>   <a href="#">Brows Directory</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')

    <div class="project-details-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="project-details-wrap shadow-reset">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                <div class="project-details-title">
                                    <h2><span class="profile-details-name-nn">Directory Details</span> </h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <h5><strong>Document Type</strong></h5>
                                <p>{{ucfirst($doc->data_type)}}</p>
                            </div>
                            <div class="col-md-8">
                                <h5><strong>Description</strong></h5>
                                <p>{{ucfirst($doc->title)}}</p>
                            </div>
                        </div>
                        <br>
                        <!-- Basic Details Starts Here -->
                        <div class="row">
                        @if(isset($files) && count($files) > 0)
                            @foreach($files as $f)
                                    <div class="col-lg-4 col-md-4 col-sm-12" >
                                        <div class="panel h-100">
                                        <a href="{{asset($f->file)}}"
                                           target="__blank">
                                            <img src="{{asset($f->file)}}" class="img-responsive img-thumbnail" style="height:300px;" ></a>
                                        <div class="panel-body">
                                    </div>
                                        </div>
                                    </div>


                            @endforeach
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
