<!-- Edit Details Of  selected Category Starts Here -->
<div class="col-lg-12 col-md-12 col-sm-12 mt-5">
            <div class="basic-login-form-ad">

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="basic-login-inner">
                            <form action="{{route('patient-management.update',$user->id)}}" method="post">
                                @method('PUT')
                                @csrf

                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="login2 pull-right pull-right-pro">Gender</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="bt-df-checkbox pull-left">
                                                <label>
                                                    <input type="radio" class="pull-left radio-checked"  required
                                                           name="gender" value="Male" @if(isset($user->profile->gender) && $user->profile->gender =="Male"){{'checked'}}@endif> <i></i> Male </label>
                                                <label>
                                                    <input type="radio" class="pull-left radio-checked"  required
                                                           name="gender" value="Female" @if(isset($user->profile->gender) && $user->profile->gender =="Female"){{'checked'}}@endif> <i></i> Female </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="login2 pull-right pull-right-pro">Landmark</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="landmark" value="@if(isset($user->profile->landmark)){{$user->profile->landmark}}@endif" placeholder="Enter landmark" required>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="login2 pull-right pull-right-pro">Area</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="area"
                                                   value="@if(isset($user->profile->area)){{$user->profile->area}}@endif"
                                                   placeholder="Enter area" required>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="login2 pull-right pull-right-pro">City</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="city"
                                                   value="@if(isset($user->profile->city)){{$user->profile->city}}@endif"
                                                   placeholder="Enter city" required>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="login2 pull-right pull-right-pro">State</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="state"
                                                   value="@if(isset($user->profile->state)){{$user->profile->state}}@endif"
                                                   placeholder="Enter state" required>

                                        </div>
                                    </div>
                                </div>

                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="login2 pull-right pull-right-pro">Pincode</label>
                                    </div>
                                    <div class="col-lg-6">
                                        <input type="text" name="pincode" class="form-control
@error('pincode') is-invalid @enderror" placeholder="pincode" required oninput='this.value=(parseInt(this.value)||" ")'
                                               maxlength="6" value="@if(isset($user->profile->pincode)){{$user->profile->pincode}}@endif">

                                    </div>
                                </div>
                            </div>

                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="login2 pull-right pull-right-pro">Blood Group</label>
                                    </div>
                                    <div class="col-lg-6">
                                        <select class="form-control @error ('blood_groop') is-invalid @enderror" name="blood_group" required >
                                            <option selected disabled>Choose your blood group</option>
                                            <option value="A+"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="A+"){{'selected'}}@endif>A+</option>
                                            <option value="A-"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="A-"){{'selected'}}@endif>A-</option>
                                            <option value="B+"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="B+"){{'selected'}}@endif>B+</option>
                                            <option value="B-"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="B-"){{'selected'}}@endif>B-</option>
                                            <option value="AB+"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="AB+"){{'selected'}}@endif>AB+</option>
                                            <option value="AB-"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="AB-"){{'selected'}}@endif>AB-</option>
                                            <option value="O+"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="O+"){{'selected'}}@endif>O+</option>
                                            <option value="O-"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="O-"){{'selected'}}@endif>O-</option>
                                        </select>
                                    </div>
                                    </div>
                                </div>
                            </div>
                                <div class="form-group-inner">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="login2 pull-right pull-right-pro">Address</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <textarea name="address" required class="form-control @error('address') is-invalid @enderror"  placeholder="Address">@if(isset($user->profile->address)){{$user->profile->address}}@endif</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="login-btn-inner">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                        <div class="col-lg-6">
                                            <div class="login-horizental">
                                                <button class="btn btn-lg btn-primary login-submit-cs" type="submit">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

<!-- End of Edit Selected Category Details Here -->
