@extends('layouts.master')
@section('breadcumb')
<!-- Breadcome start-->
            <div class="breadcome-area mg-b-30 small-dn">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                 <ul class="breadcome-menu">
                                 <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                                            </li>
    <li><a href="#">Patient Management</a> <span class="bread-slash"></span>
                                            </li>
                                        </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@section('content')
<!-- Static Table Start -->
            <div class="data-table-area mg-b-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline13-list shadow-reset">
                                <div class="sparkline13-hd">
                                    <div class="main-sparkline13-hd">
                                        <h1>All Patients <span class="table-project-n">Data</span> </h1>
                                        <div class="sparkline13-outline-icon">
                                            <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                            {{-- <span><i class="fa fa-wrench"></i></span> --}}
                                            {{-- <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span> --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="sparkline13-graph">
                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                        <div id="toolbar">
                                            <select class="form-control">
                                                <option value="">Export Basic</option>
                                                <option value="all">Export All</option>
                                                <option value="selected">Export Selected</option>
                                            </select>
                                        </div>
                                        <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <th data-field="state" data-checkbox="true"></th>
                                                    <th data-field="id">S.No.</th>
                                                    <th data-field="name">Name</th>
                                    <th data-field="mobile">Contact</th>
                                    <th data-field="email">Email ID</th>
                                     <th data-field="puid">Patient Unique Id</th>
                                    <th data-field="user_role">Registered On</th>

                                    <th data-field="action">Action</th>
{{--                                    <th data-field="edit">Edit</th>--}}
{{--                                   <th data-field="delete">Remove</th>--}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $num=1;?>
@if(isset($user))
  @foreach($user as $sp)
  <tr>
      <td></td>
 <td>{{$num}}</td>
                                <td>{{ucwords($sp->name)}}</td>
                                <td>{{$sp->mobile}}</td>
                                <td>{{$sp->email}}</td>
      <td>{!! isset($sp->puid) && !is_null($sp->puid) ? $sp->puid:'<a href="javascript:;" onclick="generatePuid('.$sp->id.');" class="green btn-white btn-action btn-xs"><i class="fa fa-cog" aria-hidden="true"></i> Generate</a>' !!}</td>

                              <td>{{date('d-m-Y',strtotime($sp->created_at))}}</td>
<td>
    <div class="btn-group project-list-action">
        <button class="btn btn-white btn-action btn-xs blue"> <a href="{{route('patient-management.show',$sp->id)}}"  title="View"> <i class="fa fa-folder "></i> View</a></button>
        <button class="btn btn-white btn-action btn-xs green"><a href="{{route('patient-management.edit',$sp->id)}}"  title="Edit"><i class="fa fa-pencil "></i> Edit </a></button>
        <button class="btn btn-white btn-action btn-xs danger">
            <a href="javascript:void(0)" data-url="{{route('patient-management.destroy',$sp->id)}}"
               data-id="{{$sp->id}}" data-toggle="modal" data-target="#WarningModalhdbgcl"
               class=" delete_button_action" title="Remove">
                <i class="fa fa-trash "></i> Remove</a></button>
    </div>
{{--    <div class="fr">--}}
{{--    <a href="javascript:void(0)" data-url="{{route('user-management.destroy',$sp->id)}}" data-id="{{$sp->id}}" data-toggle="modal" data-target="#WarningModalhdbgcl" class="danger m5 delete_button_action" title="Remove"> <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i> </a>--}}
{{--                                        </div>--}}
</td>
                 <?php $num++;?>
            </tr>
        @endforeach

@endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Static Table End -->
@endsection
@section('additional_script')
<script>
    function generatePuid(id)
    {
        $.ajax({
            type:"GET",
            url:"{{url('ajax/generate-puid')}}?id="+id,
            success:function(res){
                if(res.status=='1'){
                   location.reload();
                }
                else{
                    $.notify(res.message,'error');
                }
            }
        });
    }
</script>


@endsection
