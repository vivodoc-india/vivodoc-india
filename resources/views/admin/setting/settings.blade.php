@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashboard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="#">Settings</a> <span class="bread-slash"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- Static Table Start -->
    <div class="data-table-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline13-list shadow-reset">
                        <div class="sparkline13-hd">
                            <div class="main-sparkline13-hd">
                                <h1>Manage <span class="table-project-n">Settings</span> </h1>
                                <div class="sparkline13-outline-icon">
                                    <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{-- <span><i class="fa fa-wrench"></i></span> --}}
                                    {{-- <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span> --}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline13-graph">
                            <div style="margin-bottom: 10px; padding-bottom:10px;">
                                <button title="Add New Config" onclick="window.location.href ='{{url('omed/manage-settings/create')}}'"
                                        class="btn btn-primary add-button" type="button" style="float:right;">
                                    <i class="fa fa-plus"></i>
                                </button> <br>
                            </div>
                            <div class="datatable-dashv1-list custom-datatable-overright">
                                <div id="toolbar">
                                    <select class="form-control">
                                        <option value="">Export Basic</option>
                                        <option value="all">Export All</option>
                                        <option value="selected">Export Selected</option>
                                    </select>
                                </div>
                                <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                    <tr>
                                        <th data-field="state" data-checkbox="true"></th>
                                        <th data-field="id">S.No.</th>
                                        <th data-field="param">Params</th>
                                        <th data-field="data">Data</th>
                                        <th data-field="created">Created At</th>
                                        <th data-field="updated">Updated At</th>
                                        <th data-field="action">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $num=1;?>
                                    @if(isset($settings))
                                        @foreach($settings as $sp)
                                            <tr>
                                                <td></td>
                                                <td>{{$num}}</td>
                                                <td>{{$sp->param}}</td>
                                                <td>{{$sp->data}}</td>
                                                <td>{{date('d-m-Y',strtotime($sp->created_at))}}</td>
                                                <td>{{date('d-m-Y',strtotime($sp->updated_at))}}</td>
                                                <td>
                                                    <div class="btn-group project-list-action">
                                                        <button class="btn btn-white btn-action btn-xs green">
                                                            <a href="{{route('manage-settings.edit',$sp->id)}}"  title="Edit">
                                                                <i class="fa fa-pencil "></i> Edit </a>
                                                        </button>
                                                        <button class="btn btn-white btn-action btn-xs danger">
                                                            <a href="javascript:void(0)" data-url="{{route('manage-settings.destroy',$sp->id)}}"
                                                               data-id="{{$sp->id}}" data-toggle="modal" data-target="#WarningModalhdbgcl"
                                                               class=" delete_button_action" title="Remove">
                                                                <i class="fa fa-trash "></i> Remove</a></button>
                                                    </div>

                                                </td>
                                                <?php $num++;?>
                                            </tr>
                                        @endforeach

                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Static Table End -->
@endsection
