@extends('layouts.master')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb">
                <a href="{{url('dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
                <a href="#" class="current">Change Password</a>
            </div>
        </div>
        {!! Form::open(array('url'=>'changePassword','method'=>'post','class'=>'form-horizontal')) !!}
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                            <h5>Password</h5>
                        </div>
                        <div class="widget-content nopadding">
                            {{csrf_field()}}
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            <div class="control-group">
                                <label class="control-label">Current Password<spam> * </spam>:</label>
                                <div class="controls">
                                    {{ Form::password('current-password',null,['required'=>'""'])}}
                                    @if ($errors->has('current-password'))
                                        <span class="form_error">
                                        <strong>{{ $errors->first('current-password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">New Password<spam> * </spam>:</label>
                                <div class="controls">
                                    {{ Form::password('new-password',null,['required'=>'""'])}}
                                    @if ($errors->has('new-password'))
                                        <span class="form_error">
                                        <strong>{{ $errors->first('new-password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Re-type New Password<spam> * </spam>:</label>
                                <div class="controls">
                                    {{ Form::password('new-password_confirmation',null,['required'=>'""'])}}
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" name="save" value="yes" class="btn btn-success">  Change Password
                                </button>
                                <a href="{{url('dashboard')}}" type="button" class="btn btn-danger">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
