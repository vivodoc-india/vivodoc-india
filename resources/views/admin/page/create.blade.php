@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="{{url('omed/pages')}}">Pages</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="#">Create</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="basic-form-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline12-list shadow-reset mg-t-30">
                        <div class="sparkline12-hd">
                            <div class="main-sparkline12-hd">
                                <h1>Page</h1>
                                <div class="sparkline12-outline-icon">
                                    <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    <span><i class="fa fa-wrench"></i></span>
                                    <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="all-form-element-inner">
                                            {!! Form::open(array('url'=>'omed/pages','method'=>'post')) !!}
                                            @include('admin.page.form')

                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
{{--@section('additional_script')--}}
{{--    <script src="https://cdn.ckeditor.com/ckeditor5/22.0.0/classic/ckeditor.js"></script>--}}
{{--    <script>--}}
{{--    ClassicEditor--}}
{{--    .create( document.querySelector( '#editor' ),{--}}
{{--    toolbar: {--}}
{{--    items: [--}}
{{--    "heading",--}}
{{--    "|",--}}
{{--    "bold",--}}
{{--    "italic",--}}
{{--    "bulletedList",--}}
{{--    "numberedList",--}}
{{--    "blockQuote",--}}
{{--    "|",--}}
{{--    "undo",--}}
{{--    "redo"--}}
{{--    ]--}}
{{--    },--}}
{{--    language: 'en'--}}
{{--    } )--}}
{{--    .then( editor => {--}}
{{--    console.log( editor );--}}
{{--    } )--}}
{{--    .catch( error => {--}}
{{--    console.error( error );--}}
{{--    } );--}}
{{--    </script>--}}
{{--    @endsection--}}
