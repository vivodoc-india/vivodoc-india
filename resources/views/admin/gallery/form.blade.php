
{{ ! list(, $action) =explode('@', Route::getCurrentRoute()->getActionName()) }}
{{csrf_field()}}

    <div class="form-group-inner">
        <div class="row" style="margin-bottom: 10px;">
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <label class="login2 pull-right pull-right-pro">Image :</label>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="file-upload-inner file-upload-inner-right ts-forms">
                    <div class="input append-big-btn">
                        <label class="icon-left" for="append-big-btn">
                            <i class="fa fa-download"></i>
                        </label>
                        <div class="file-button">
                            Browse
                            <input type="file" name="image" onchange="document.getElementById('append-big-btn').value = this.value;">
                        </div>
                        <input type="text" id="append-big-btn" placeholder="no file selected">
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
<div class="col-lg-3"></div>
<div class="col-lg-4">
     @if(isset($gallery->image))
                                    <img src="{{asset('web/img/gallery/'.$gallery->image)}}" class="img-responsive img-thumbnail" style=""/>
                                @endif
</div>
        </div>
    </div>
       <div class="form-group-inner">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <label class="login2 pull-right pull-right-pro">Status </label>
            </div>
            <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
                <div class="bt-df-checkbox pull-left">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="i-checks pull-left">
                                <label>
                                   {{Form::radio('status','1',true,['style'=>'opacity: 0;'])}} <i></i>
                                     Active </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="i-checks pull-left">
                                <label>
                                   {{Form::radio('status','0',false,['style'=>'opacity: 0;'])}} <i></i> Idle </label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <div class="form-group-inner">
        <div class="login-btn-inner">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <div class="login-horizental cancel-wp pull-left">
         <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>
                        <button class="btn btn-sm btn-primary login-submit-cs" type="submit">@if($action=='create') Save
                                @else Update
                                @endif</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


