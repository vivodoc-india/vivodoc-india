@extends('layouts.master')
@section('breadcumb')
<!-- Breadcome start-->
<div class="breadcome-area mg-b-30 small-dn">
<div class="container-fluid">
<div class="row">
<div class="col-lg-12">
<div class="breadcome-list map-mg-t-40-gl shadow-reset">
<ul class="breadcome-menu">
<li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
</li>
<li>  <a href="{{url('omed/enquiry-management')}}">Enquiries</a> <span class="bread-slash">/</span>
</li>
<li>   <a href="#">Show</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
@endsection
@section('content')

<div class="project-details-area mg-b-15">
<div class="container-fluid">
<div class="row">
<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
<div class="project-details-wrap shadow-reset">
<div class="row">
<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
<div class="project-details-title">
<h2><span class="profile-details-name-nn">Enquiry</span> </h2>
</div>
</div>

</div>
<!-- Basic Details Starts Here -->
 @if(isset($enquiry))
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="project-details-mg">
</div>
<div class="project-details-mg">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="project-details-st">
        <span><strong>Name:</strong></span>
    </div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="project-details-dt">
        <span>{{ucwords($enquiry->name)}}</span>
    </div>
</div>
</div>
</div>
<div class="project-details-mg">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="project-details-st">
        <span><strong>Email ID:</strong></span>
    </div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="project-details-dt">
        <span>{{$enquiry->email}}</span>
    </div>
</div>
</div>
</div>
<div class="project-details-mg">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="project-details-st">
        <span><strong>Mobile No.:</strong></span>
    </div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="project-details-dt">
      <span>{{$enquiry->mobile}}</span>
    </div>
</div>
</div>
</div>

    <div class="project-details-mg">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="project-details-st">
                    <span><strong>Message:</strong></span>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="project-details-dt">
                    <span>{{$enquiry->message}}</span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endif
</div>
</div>

</div>
</div>
</div>


@endsection
