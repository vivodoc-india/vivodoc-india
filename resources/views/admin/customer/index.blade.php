@extends('layouts.master')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb">
                <a href="{{url('dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
                <a href="#" class="current">Customer</a>
            </div>
            <h1>All Customer  </h1>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>Data</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Address</th>
                                    <th>Member Type</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $num=1?>
                                @foreach($list as $data)
                                    <tr class="gradeU">
                                    <td>{{$num}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>Phone: {{$data->mobil}}<br>Email: {{$data->email}}</td>
                                    <td>Address: {{$data->address}}</td>
                                        <td>
                                            <a href="javascript:;" onclick="changeMemberType(this,'customer','member_type',{{$data->id}});" class="btn @if ($data->member_type==0) {{'btn-warning'}} @else {{'btn-success'}} @endif btn-mini">@if ($data->status==0) {{'Free'}} @else {{'Paid'}} @endif</a>
                                        </td>
                                    <td>
                                        <a href="javascript:;" onclick="changeFlag(this,'customer','status',{{$data->id}});" class="btn @if ($data->status==1) {{'btn-success'}} @else {{'btn-danger'}} @endif btn-mini">@if ($data->status==1) {{'Active'}} @else {{'Idle'}} @endif</a>
                                    </td>
                                    <td>
                                        <div class="fr">
                                              <a href="{{action('CustomerController@show',$data->id)}}" class="btn btn-success btn-mini">Show</a>
                                        </div>
                                    </td>
                                </tr>
                                <?php $num++?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function changeMemberType(data,table,column,id){
            var that=$(data);
            $.ajax({
                type:"GET",
                url:"{{url('ajax/change-flag')}}?table="+table+"&column="+column+"&id="+id,
                success:function(res){
                    if(res.status=='1'){
                        that.text('Paid');
                        that.removeClass('btn-warning');
                        that.addClass('btn-success');
                        $.notify("Member type Changed Successfully", 'success');
                    }else{
                        that.text('Free');
                        that.removeClass('btn-success');
                        that.addClass('btn-warning');
                        $.notify("Member type Changed Successfully",'success');
                    }
                }
            });
        }
    </script>
@endsection