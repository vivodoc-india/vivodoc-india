@extends('layouts.master')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb">
                <a href="{{url('dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
                <a href="{{url('customer')}}"  >Customer</a>
                <a href="#" class="current">Show</a>
            </div>
            <h1>{{$detail->name}} <a href="{{url('/customer')}}" class="btn btn-inverse btn-mini">Back</a></h1>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>Customer Data</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered">
                                <tbody>
                                <tr><th width="30%">Name</th> <td>{{$detail->name}}</td> </tr>
                                <tr><th>Address</th> <td>{{$detail->address}} </td> </tr>
                                <tr><th>Contact</th> <td>{{$detail->mobile}}</td> </tr>
                                <tr><th>Email</th> <td>{{$detail->email}}</td> </tr>
                                <tr><th>Member Type</th> <td>@if($detail->member_type==1) Paid @else Free @endif</td> </tr>
                                <tr><th>Status</th> <td>@if($detail->status==1) Active @else Deactive @endif</td> </tr>
                                <tr><th>Registration Date</th> <td>{{$detail->created_at}}</td> </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>Company Enquiry</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                    <thead><tr><th>Company</th><th>Date</th><th>Details</th><Th>Address</Th> </tr></thead>
                                        <tbody>
                                            @foreach($enquiry as $list)
                                                <tr><td><a href="{{url('provider/'.$list->company_id)}}">{{$list->name}}</a></td>
                                                    <td>{{$list->created_at}}</td>
                                                    <td>{{$list->details}}</td>
                                                    <td>{{$list->address}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
