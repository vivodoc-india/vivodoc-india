@extends('layouts.master')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb">
                <a href="{{url('dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
                <a href="#" class="current">Area</a>
            </div>
            <h1>All City Area <a href="{{url('area/create')}}" class="btn btn-inverse btn-mini">Add New</a></h1>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>Data</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Area</th>
                                    <th>City</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $num=1?>
                                @foreach($area as $data)
                                    <tr class="gradeU">
                                    <td>{{$num}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->cname}}</td>
                                    <td>
                                        <a href="javascript:;" onclick="changeFlag(this,'area','status',{{$data->id}});" class="btn @if ($data->status==1) {{'btn-success'}} @else {{'btn-danger'}} @endif btn-mini">@if ($data->status==1) {{'Active'}} @else {{'Idle'}} @endif</a>
                                    </td>
                                    <td>
                                        <div class="fr">
                                            <a href="{{action('AreaController@edit',$data->id)}}" class="btn btn-primary btn-mini">Edit</a>
                                            <a href="javascript:void(0)" data-url="{{route('area.destroy',$data->id)}}" data-id="{{$data->id}}" data-toggle="modal" data-target="#delete_button_modal" class="btn btn-danger btn-mini delete_button_action">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                <?php $num++?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection