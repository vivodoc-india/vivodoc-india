@extends('layouts.master')
@section('content')
{{ ! list(, $action) =explode('@', Route::getCurrentRoute()->getActionName()) }}
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb">
                <a href="{{url('dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
                <a href="{{url('area')}}"  >Area</a>
                <a href="#" class="current">@if($action=='create') New @else Update @endif</a>
            </div>
        </div>
        {!! Form::model($area,['method'=>'PATCH','url'=>['area',$area->id],'files'=>true,'class'=>'form-horizontal']) !!}
        @include('area.form');
        {!! Form::close() !!}
    </div>
@endsection