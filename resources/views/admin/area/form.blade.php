{{ ! list(, $action) =explode('@', Route::getCurrentRoute()->getActionName()) }}
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5>Area</h5>
                </div>
                <div class="widget-content nopadding">
                    {{csrf_field()}}
                    <div class="control-group">
                        <label class="control-label">City:</label>
                        <div class="controls">
                            <span class="append_service1">
                                {{Form::select('city_id',$city_list->toarray(),null,['class'=>'form-control input-sm '])}}
                            </span>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Area Name<spam> * </spam>:</label>
                        <div class="controls">
                            {{ Form::text('name',null,['required'=>'""'])}}
                            @if ($errors->has('name'))
                                <span class="form_error">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Status :</label>
                        <div class="controls">
                            <label><div class="radio" id="uniform-undefined"><span>{{Form::radio('status','1',true,['style'=>'opacity: 0;'])}}</span></div>Active</label>
                            <label><div class="radio" id="uniform-undefined"><span>{{Form::radio('status','0',false,['style'=>'opacity: 0;'])}}</span></div>Idle</label>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="save" value="yes" class="btn btn-success">
                            @if($action=='create') Save
                            @else Update
                            @endif
                        </button>
                        <a href="{{url('area')}}" type="button" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>