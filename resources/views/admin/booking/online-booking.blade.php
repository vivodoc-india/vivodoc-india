
@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashboard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li><a href="{{url('omed/online-booking-management')}}">Online Bookings</a>
                                <span
                                    class="bread-slash"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- Static Table Start -->
    <div class="data-table-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline13-list shadow-reset">
                        <div class="sparkline13-hd">
                            <div class="main-sparkline13-hd">
                                <h1>Online Bookings <span class="table-project-n">Data</span> </h1>
                                <div class="sparkline13-outline-icon">
                                    <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{-- <span><i class="fa fa-wrench"></i></span> --}}
                                    {{-- <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span> --}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline13-graph">
                            <div class="datatable-dashv1-list custom-datatable-overright">
                                <div id="toolbar">
                                    <select class="form-control">
                                        <option value="">Export Basic</option>
                                        <option value="all">Export All</option>
                                        <option value="selected">Export Selected</option>
                                    </select>
                                </div>
                                <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>
                                    <tr>
                                        <th data-field="state" data-checkbox="true"></th>
                                        <th data-field="id">S.No.</th>
                                        <th data-field="name">Patient Name</th>
                                        <th data-field="age">Patient Age</th>
                                        <th data-field="gender">Patient Gender</th>
                                        <th data-field="booked-date">Booked Date</th>
                                        <th data-field="provider">Service Provider</th>
                                        <th data-field="booked-by">Booked By</th>
                                        <th data-field="action">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $num=1;?>
                                    @if(isset($record))

                                        @foreach($record as $rr)

                                            <tr>
                                                <td></td>
                                                <td>{{$num}}</td>
                                                <td>{{ucwords($rr->patient_name)}}</td>
                                                <td>
                                                    {{$rr->age}}
                                                </td>
                                                <td>
                                                    {{$rr->gender}}
                                                </td>
                                                <td>{{date('d-m-Y',strtotime($rr->booking_date))}}</td>
                                                <td>{{$rr->provider->name}}</td>
                                                <td>{{$rr->patient->name}}</td>
{{--                                                <td>--}}
{{--        @php echo(($rr->patient)? $rr->patient->name:'') @endphp--}}
{{--                                                </td>--}}

                                                <td>
                                                    <div class="fr">
                                                        <a href="{{route('online-booking-management.show',$rr->id)}}" class="green m5" title="View"><i class="fa
    fa-eye fa-lg" aria-hidden="true"></i> </a>
  <a href="javascript:void(0)" data-url="{{route('online-booking-management.destroy',$rr->id)}}"
   data-id="{{$rr->id}}" data-toggle="modal" data-target="#WarningModalhdbgcl" class="danger m5 delete_button_action" title="Remove">
      <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
  </a>
                                                    </div>
                                                </td>
                                                @php $num++ @endphp
                                            </tr>
                                        @endforeach

                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Static Table End -->
@endsection


