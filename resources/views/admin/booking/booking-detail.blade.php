@extends('layouts.master')
@section('breadcumb')
    <!-- Breadcome start-->
    <div class="breadcome-area mg-b-30 small-dn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                        <ul class="breadcome-menu">
                            <li><a href="{{url('omed/dashboard')}}">Dashboard</a> <span class="bread-slash">/</span>
                            </li>
                            <li>  <a href="{{url('omed/offline-booking-management')}}">Offline Bookings</a>
                            <span
                            class="bread-slash">/</span>
                            </li>

                            <li>    <a href="#"> Booking Detail</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="basic-form-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline12-list shadow-reset mg-t-30">
                        <div class="sparkline12-hd">
                            <div class="main-sparkline12-hd">
                                <h1>Booking Detail</h1>
                                <div class="sparkline12-outline-icon">
                                    <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    {{--                                    <span><i class="fa fa-wrench"></i></span>--}}
                                    {{--                                    <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>--}}
                                </div>
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row mb-3" >
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Patient Name :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->patient_name)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Patient Age :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->age)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Patient Gender :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->gender)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Patient Address :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->address)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Diagnosis :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->diagnosis)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Vital :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->vital)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Complain :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{ucwords($detail->complain)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Service Provider :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                <span class="text-success" style="font-weight:bold;">{{ucwords($detail->provider->name)}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Booked Date :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span class="text-success" style="font-weight:bold;">{{date
                                                        ('d-m-Y',strtotime
                                                        ($detail->booking_date))
                                                        }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Booking Number:</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span><strong>{{ucwords($detail->booking_number)
                                                        }}</strong></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Booked By :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span style="font-weight:bold;">{{ucwords
                                                        ($detail->patient->name)
                                                        }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="project-details-mg">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="project-details-st">
                                                        <span><strong>Booked On :</strong></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group project-list-ad">
                                                        <span>{{date('d-m-Y',strtotime($detail->created_at))}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
{{--                                <div class="row mt-10">--}}
{{--                                    <div class="col-lg-12">--}}
{{--                                        <div class="all-form-element-inner">--}}

{{--                                            <form action="{{url('omed/user-management/admin-reply')}}" method="post">--}}
{{--                                                @csrf--}}
{{--                                                <input type="hidden" name="reply_id" value="{{$detail->id}}">--}}
{{--                                                <div class="form-group-inner">--}}
{{--                                                    <div class="row">--}}
{{--                                                        <div class="col-lg-3">--}}
{{--                                                            <label class="login2 pull-right--}}
{{--                                                            pull-right-pro">Admin Reply</label>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-lg-9">--}}
{{--                                                            <textarea name="admin_reply" class="form-control" required placeholder="Enter your reply"></textarea>--}}

{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group-inner">--}}
{{--                                                    <div class="login-btn-inner">--}}
{{--                                                        <div class="row">--}}
{{--                                                            <div class="col-lg-3"></div>--}}
{{--                                                            <div class="col-lg-6">--}}
{{--                                                                <div class="login-horizental cancel-wp pull-left">--}}
{{--                                                                    <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>--}}
{{--                                                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit">Update--}}
{{--                                                                    </button>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </form>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

