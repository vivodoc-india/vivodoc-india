@extends('layouts.master')
@section('breadcumb')
<!-- Breadcome start-->
            <div class="breadcome-area mg-b-30 small-dn">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                 <ul class="breadcome-menu">
                                 <li><a href="{{url('omed/dashbard')}}">Dashboard</a> <span class="bread-slash">/</span>
                                            </li>
    <li><a href="#">Category</a> <span class="bread-slash"></span>
                                            </li>
                                        </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@section('content')
<!-- Static Table Start -->
            <div class="data-table-area mg-b-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline13-list shadow-reset">
                                <div class="sparkline13-hd">
                                    <div class="main-sparkline13-hd">
                                        <h1>Categories <span class="table-project-n">Data</span> </h1>
                                        <div class="sparkline13-outline-icon">
                                            <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                            {{-- <span><i class="fa fa-wrench"></i></span> --}}
                                            {{-- <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span> --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="sparkline13-graph">
                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                        <div id="toolbar">
                                            <select class="form-control">
                                                <option value="">Export Basic</option>
                                                <option value="all">Export All</option>
                                                <option value="selected">Export Selected</option>
                                            </select>
                                        </div>
                                        <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
{{--                                                    <th data-field="state" data-checkbox="true"></th>--}}
                                                    <th data-field="id">S.No.</th>
                                                    <th data-field="title" >Title</th>
                                                    <th data-field="user_role_id">User Role</th>
                                                    <th data-field="image">Image</th>
                                                    <th data-field="featured">Featured</th>
                                                    <th data-field="is_private">Accessibility</th>
                                                    <th data-field="status" >Status</th>
                                                    <th data-field="action">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $num=1?>
                                @foreach($category as $data)

                                    <tr>
{{--                                        <td></td>--}}
                                    <td>{{$num}}</td>
                                    <td>{{$data->title}}</td>
                                    <td>{{$data->userRole->name}}</td>
                                    <td>
                                        @if( $data->image!='' && File::exists($data->image))
                                        <img src="{{asset($data->image)}}" style="max-width:50px;" />
                                        @endif
                                    </td>
                                    <td>
                                        <a href="javascript:void(0)" onclick="changeFlag(this,'categories','featured',{{$data->id}});"
                                           class="btn @if ($data->featured==1) {{'btn-success'}} @else {{'btn-danger'}} @endif btn-sm">@if ($data->featured==1) {{'Active'}} @else {{'Idle'}} @endif</a>
                                    </td>
                                        <td>
                                            @if($data->is_private==1)
                                                <span class="badge badge-danger">Private</span>
                                                <br>
                                                <label><b>Secret Key:</b> {{$data->secret_key}}</label>
                                            @else
                                                <span class="badge badge-success">Public</span>
                                            @endif
                                        </td>
                                    <td>
                                        <a href="javascript:void(0)" onclick="changeFlag(this,'categories','status',{{$data->id}});"
                                           class="btn @if ($data->status==1) {{'btn-success'}} @else {{'btn-danger'}} @endif btn-sm">@if ($data->status==1) {{'Active'}} @else {{'Idle'}} @endif</a>
                                    </td>
                                    <td>
                                        <div class="fr">
                                            <a href="{{route('category.edit',$data->id)}}" class="btn btn-custon-four btn-primary"><span class="adminpro-icon adminpro-informatio"></span> Edit</a>
                                            <a href="javascript:void(0)" data-url="{{route('category.destroy',$data->id)}}" data-id="{{$data->id}}" data-toggle="modal" data-target="#WarningModalhdbgcl" class="btn btn-custon-four btn-danger delete_button_action"><span class="adminpro-icon adminpro-danger-error"></span> Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                <?php $num++?>
                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Static Table End -->
@endsection
