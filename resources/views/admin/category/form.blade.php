
{{ ! list(, $action) =explode('@', Route::getCurrentRoute()->getActionName()) }}
{{csrf_field()}}
    <div class="form-group-inner">
        <div class="row">
            <div class="col-lg-3">
                <label class="login2 pull-right pull-right-pro">Title<span> * </span> : <br>(Medical Terminology)</label>
            </div>
            <div class="col-lg-6">
                <input type="text" class="form-control" placeholder="Title for medical terminology"
                       required name="title" value="{{isset($category)?$category->title:''}}">

                                @if ($errors->has('title'))
                                    <span class="form_error">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
            </div>
        </div>

    </div>
<div class="form-group-inner">
  <div class="row">
            <div class="col-lg-3">
                <label class="login2 pull-right pull-right-pro">Title<span> * </span> : <br>(General Terminology)</label>
            </div>
            <div class="col-lg-6">
                <input type="text" class="form-control" placeholder="Title for general terminology"
                       name="general_title" value="{{isset($category)?$category->general_title:''}}">

                                @if ($errors->has('general_title'))
                                    <span class="form_error">
                                        <strong>{{ $errors->first('general_title') }}</strong>
                                    </span>
                                @endif
            </div>
        </div>
</div>
    <div class="form-group-inner">
        <div class="row">
            <div class="col-lg-3">
                <label class="login2 pull-right pull-right-pro">Accessible by user roles<span> * </span>:</label>
            </div>
            <div class="col-lg-6">
                <div class="form-select-list">
                    <select name="user_role_id" class="form-control custom-select" required id="cat-user-role">
                        @if(isset($user_roles))
                            @foreach($user_roles as $usr)
                                <option value="{{$usr->id}}" {{isset($category) && $category->user_role_id==$usr->id?'selected':''}}>
                                    {{$usr->name}}</option>
                                @endforeach

                            @endif
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group-inner">
        <div class="row" style="margin-bottom: 10px;">
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <label class="login2 pull-right pull-right-pro">Image :</label>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="file-upload-inner file-upload-inner-right ts-forms">
                    <div class="input append-big-btn">
                        <label class="icon-left" for="append-big-btn">
                            <i class="fa fa-download"></i>
                        </label>
                        <div class="file-button">
                            Browse
                            <input type="file" name="image" onchange="document.getElementById('append-big-btn').value = this.value;">
                        </div>
                        <input type="text" id="append-big-btn" placeholder="no file selected">
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
<div class="col-lg-3"></div>
<div class="col-lg-4">
     @if(isset($category->image) && File::exists($category->image))
                                    <img src="{{asset($category->image)}}" class="img-responsive img-thumbnail" style=""/>
                                @endif
</div>
        </div>
    </div>
       <div class="form-group-inner">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <label class="login2 pull-right pull-right-pro">Status </label>
            </div>
            <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
                <div class="bt-df-checkbox pull-left">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="i-checks pull-left">
                                <label>
                                    <input type="radio" name="status" style="opacity: 0"
                                           value="1" {{isset($category) && $category->status==1?'checked':''}}><i></i>
{{--                                   {{Form::radio('status','1',true,['style'=>'opacity: 0;'])}} <i></i>--}}
                                     Active </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="i-checks pull-left">
                                <label>
                                    <input type="radio" name="status" style="opacity: 0" value="0"
                                        {{isset($category) && $category->status==0?'checked':''}}><i></i> Idle
{{--                                   {{Form::radio('status','0',false,['style'=>'opacity: 0;'])}} <i></i> Idle </label>--}}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
<div class="form-group-inner">
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <label class="login2 pull-right pull-right-pro">Is Private </label>
        </div>
        <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
            <div class="bt-df-checkbox pull-left">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="i-checks pull-left" >
                            <label>
                                <input type="radio" name="is_private"
                                       style="opacity: 0" value="1"
                                    {{isset($category) && $category->is_private==1?'checked':''}} id="make-private">
                                Yes </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="i-checks pull-left">
                            <label>
                                <input type="radio" name="is_private"
                                       style="opacity: 0" value="0"
                                    {{isset($category) && $category->is_private==0?'checked':''}} id="make-public"> No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="secret-key">
<div class="form-group-inner">
    <div class="row">
        <div class="col-lg-3">
            <label class="login2 pull-right pull-right-pro">Secret Key</label>
        </div>
        <div class="col-lg-6">
            <input type="text" class="form-control" placeholder="Enter a secret key for this category"
                   name="secret_key" value="{{isset($category)?$category->secret_key:''}}">

            @if ($errors->has('secret_key'))
                <span class="form_error">
                                        <strong>{{ $errors->first('secret_key') }}</strong>
                                    </span>
            @endif
        </div>
    </div>
</div>
</div>
    <div class="form-group-inner">
        <div class="login-btn-inner">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <div class="login-horizental cancel-wp pull-left">
         <button class="btn btn-white" type="button" onclick="window.history.back();">Cancel</button>
                        <button class="btn btn-sm btn-primary login-submit-cs" type="submit">@if($action=='create') Save
                                @else Update
                                @endif</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


