<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{isset($title)?$title:config('app.name')}}</title>
    <link rel="icon" type="image/png" href="{{asset('web/img/favicon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Raleway:500|Roboto:500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body class="drawer drawer--left">
<div class="header cp-lg">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="text-white h2-responsive text-uppercase">CheckOut</div>
            </div>
        </div>
    </div>
</div>
<div class="container cp-md">
    <div class="card shadow-none border">
        <div class="card-body">

            @if(isset($orderDetails))
                <form id="redirectForm" method="post" action="https://www.cashfree.com/checkout/post/submit">
{{--                    <form id="redirectForm" method="post" action="https://test.cashfree.com/billpay/checkout/post/submit">--}}
                    <input type="hidden" name="appId" value="{{$orderDetails['appId']}}"/>
                    <input type="hidden" name="orderId" value="{{$orderDetails['orderId']}}"/>
                    <input type="hidden" name="orderAmount" value="{{$orderDetails['orderAmount']}}"/>
                    <input type="hidden" name="orderCurrency" value="{{$orderDetails['orderCurrency']}}"/>
                    <input type="hidden" name="orderNote" value="{{$orderDetails['orderNote']}}"/>
                    <input type="hidden" name="customerName" value="{{$orderDetails['customerName']}}"/>
                    <input type="hidden" name="customerEmail" value="{{$orderDetails['customerEmail']}}"/>
                    <input type="hidden" name="customerPhone" value="{{$orderDetails['customerPhone']}}"/>
                    <input type="hidden" name="returnUrl" value="{{$orderDetails['returnUrl']}}"/>
                    <input type="hidden" name="notifyUrl" value="{{$orderDetails['notifyUrl']}}"/>
                    <input type="hidden" name="signature" value="{{$orderDetails['signature']}}"/>
                </form>
            @endif
            <script>document.getElementById("redirectForm").submit();</script>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>
