<!DOCTYPE html>
<html>
<head>
    <title>VivoDoc | Live Consultation</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
   <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>

    <link rel="stylesheet" href="https://vivodoc.in/public/web/css/all.css">
    <link rel="stylesheet" href="https://vivodoc.in/public/web/css/alertify.css">
    <link rel="stylesheet" href="https://vivodoc.in/public/web/css/bootstrap.css">
    <link rel="stylesheet" href="https://vivodoc.in/public/web/css/mdb.css">
    <link rel="stylesheet" href="https://vivodoc.in/public/web/css/drawer.css">
    <link rel="stylesheet" href="https://vivodoc.in/public/web/css/owl.carousel.css">
    <link rel="stylesheet" href="https://vivodoc.in/public/web/css/owl.theme.css">
    <link rel="stylesheet" href="https://vivodoc.in/public/web/css/viewbox.css">
    <link rel="stylesheet" href="https://vivodoc.in/public/web/css/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://vivodoc.in/public/web/rating/bootstrap-rating.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.css" rel="stylesheet">

    <link href="https://vivodoc.in/public/web/dist/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('webrtc/bootstrap_icons/fonts/bootstrap-icons.css')}}">
    <script src="https://www.gstatic.com/firebasejs/8.8.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.8.1/firebase-firestore.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{asset('webrtc/main.css')}}">
    <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>

</head>
<body>
@include('webrtc.index')
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
{{--<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
<script src="{{asset('webrtc/webViewApp.js')}}" async></script>
<script src="{{asset('webrtc/deviceAccess.js')}}" async></script>
<script src="{{asset('webrtc/main.js')}}" async></script>
@include('webrtc.js-config')
</body>

</html>
