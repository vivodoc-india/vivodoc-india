
<script>
    var firebaseConfig = {
        apiKey: "AIzaSyDAo3iV_T3C0njiXXzIA0kTb6dJyV2mEaU",
        authDomain: "omed-d39e0.firebaseapp.com",
        projectId: "omed-d39e0",
        databaseURL:"https://omed-d39e0-default-rtdb.asia-southeast1.firebasedatabase.app",
        storageBucket: "omed-d39e0.appspot.com",
        messagingSenderId: "139501672892",
        appId: "1:139501672892:web:bc8346561f16b42a9333b9",
        measurementId: "G-KHRPJ0SZPV"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
</script>
<script>
$('document').ready(function(){
    setTimeout(function(){
        openUserMedia();
        }, 500);
});
    @if(isset($booking))
    function initiateCall(res_data)
    {
        $.ajax({
            type:'ajax',
            method:'post',
            data:{
                '_token':"{{ csrf_token() }}",
                'id':res_data,
                'caller':"{{$user}}",
                'res_code':"{{$booking->id}}",
                'ident':"{{$user}}",
            },
            url:"{{url('omed-api/room-created')}}",
            dataType:'json',
            async:true,
            success:function(response){
                if(response.success)
                {
                    if(response.roomExists)
                    {
                        showAlert('Please wait ...');
                        joinRoomById(response.id);
                        $('#hangupBtn').addClass("btn-danger");
                    }
                    showAlert('Please wait ...');
                }
                else{
                    showAlert('Unable to initiate call');
                    hangUp();
                }
            }
        });


    }
    async function joinRoom(){
        await joinRoomById("{{isset($booking)?$booking->con_link:''}}");
        $('#hangupBtn').addClass("btn-danger");
    }
    function showAlert(message){
        $('#alert-notification').show();
        $('#alert-msg').html(message);
    }
    function askForCall(){
        // $('#alert-notification').show();
        // $('#alert-msg').html('Please click on <button type="button" class="btn btn-sm btn-success" style="height:30px;width:30px;border-radius:50px;padding:0px;margin:0px;">'+'<i class="bi bi-telephone-fill"></i>'+'</button> button');
    }
    function audioMuted(){
        $('#disableAudio').html('<i class="bi bi-mic-mute"></i>');
        $('#disableAudio').addClass("btn-info");
    }
    function audioUnMuted(){
        $('#disableAudio').html('<i class="bi bi-mic"></i>');
        $('#disableAudio').removeClass("btn-info");
    }
    function videoMuted(){
        $('#disableVideo').html('<i class="bi bi-camera-video-off"></i>');
        $('#disableVideo').addClass("btn-success");
    }
    function videoUnMuted(){
        $('#disableVideo').html('<i class="bi bi-camera-video"></i>');
        $('#disableVideo').removeClass("btn-success");
    }
    function hangCall(){
        $.ajax({
            type:'ajax',
            method:'post',
            data:{
                '_token':"{{ csrf_token() }}",
                'res_code':"{{$booking->id}}",
            },
            url:"{{url('omed-api/remove-room')}}",
            dataType:'json',
            async:true,
            success:function(response){
                if(response.success)
                {
                    hangUp();
                }
                else{
                    showAlert('Something went wrong.');
                }
            }
        });
    }
    @endif
</script>
