<div id="lb-card">
    {{--<div id="lb-card" style="width:100%; height:500px;display:none;">--}}
    @if(isset($booking) && $booking && $booking->is_con_link_available==1)
        <div class="main-container cp-md">
            <div id="videos" class="video-container">
                <video id="localVideo" class="localVideo" data="local" muted autoplay playsinline></video>
                <video id="remoteVideo" class="remoteVideo remote" data="remote" autoplay playsinline></video>
                <div class="live-alert" id="alert-notification" style="display:none;">
                    <div class="live-alert-header">
                        <!--<div class="live-alert-header-text">-->
                        <!--    <h4>Live Consultation</h4>-->
                        <!--</div>-->
                        <!--<div class="live-alert-close">-->
                        <!--    <i class="bi bi-x"></i>-->
                        <!--</div>-->
                    </div>
                    <div class="live-alert-body">
                        <div class="live-alert-message">
                            <p id="alert-msg"></p>
                        </div>
                        <div class="live-alert-action">
                            <button class="live-alert-action-button btn btn-danger btn-sm">OK</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="control-button-container">
                @if($booking->caller==$user)
                    <div class="start-button">
                        <button type="button" class="btn btn-sm btn-info" disabled id="createBtn">
                            <!--<i class="bi bi-telephone-fill"></i>-->
                            JOIN
                        </button>
                    </div>
                @elseif(empty($booking->caller))
                    <div class="start-button">
                        <button type="button" class="btn btn-sm btn-info" disabled id="createBtn">
                            <!--<i class="bi bi-telephone-fill"></i>-->
                            JOIN
                        </button>
                    </div>
                @else
                    <div class="start-button">
                        <button type="button" class="btn btn-sm btn-info" disabled id="joinBtn">
                            <!--<i class="bi bi-telephone-fill"></i>-->
                            JOIN
                        </button>
                    </div>
                @endif
                <div class="mic-button">
                    <button type="button" class="btn btn-sm" id="disableAudio">
                        <i class="bi bi-mic"></i>
                    </button>
                </div>
                
                <div class="end-button">
                    <button type="button" class="btn btn-sm" disabled id="hangupBtn">
                        <i class="bi bi-telephone-fill"></i>
                    </button>
                </div>
                <div class="video-button">
                    <button type="button" class="btn btn-sm" id="disableVideo">
                        <i class="bi bi-camera-video"></i>
                    </button></div>
            </div>
            <div class="setting-button">
                <div class="sett-button"><i class="bi bi-gear"></i></div>
            </div>
            <div class="full-button">
                <i class="bi bi-fullscreen"></i>
            </div>
            <div class="setting-container">
                <div class="select">
                    <label for="audioSource">Audio input source: </label>
                    <select id="audioSource" class="form-control"></select>
                </div>

                <div class="select">
                    <label for="audioOutput">Audio output destination: </label>
                    <select id="audioOutput" class="form-control"></select>
                </div>

                <div class="select">
                    <label for="videoSource">Video source: </label>
                    <select id="videoSource" class="form-control"></select>
                </div>

                <div class="close-button">
                    <button class="setting-close">Close</button>
                </div>
            </div>
        </div>
    @endif
</div>
