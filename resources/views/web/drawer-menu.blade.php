<nav class="drawer-nav" role="navigation">
    <ul class="drawer-menu">
    @if(Auth::user()->user_role_id==3)
{{--            <li> <a href="{{url('patient/dashboard')}}" class="drawer-menu-item">My Profile</a></li>--}}
            <li><a href="{{url('patient/update-profile')}}" class="drawer-menu-item">Update Profile</a></li>
            <li><a href="{{url('patient/enquiries')}}" class="drawer-menu-item">Enquiries</a></li>
{{--            <li><a href="{{url('patient/reviews-ratings')}}" class="drawer-menu-item">Reviews &amp; Rating</a></li>--}}
            <li><a href="{{url('patient/medical-data')}}" class="drawer-menu-item">Medical History</a></li>
{{--            <li><a href="{{url('patient/appoinment-booking')}}" class="drawer-menu-item">Appointment Bookings</a></li>--}}
{{--            <li><a class="drawer-menu-item" href="#">Medical History</a></li>--}}
{{--            <li>--}}
{{--                <a class="drawer-menu-item mb-1" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Parent <i class="fa fa-caret-down"></i></a>--}}

{{--                <div class="collapse multi-collapse pl-3" id="multiCollapseExample1">--}}
{{--                    <a class="drawer-menu-item mb-1" data-toggle="collapse" href="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">Children 1 <i class="fa fa-caret-down"></i></a>--}}
{{--                    <div class="collapse multi-collapse pl-3" id="multiCollapseExample2">--}}
{{--                        <a class="drawer-menu-item mb-1">Child 1</a>--}}
{{--                        <a class="drawer-menu-item mb-1">Child 2</a>--}}
{{--                        <a class="drawer-menu-item mb-1">Child 3</a>--}}
{{--                        <a class="drawer-menu-item mb-1">Child 4</a>--}}
{{--                        <a class="drawer-menu-item mb-1">Child 5</a>--}}
{{--                    </div>--}}

{{--                    <a class="drawer-menu-item mb-1" data-toggle="collapse" href="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">Children 2 <i class="fa fa-caret-down"></i></a>--}}
{{--                    <div class="collapse multi-collapse pl-3" id="multiCollapseExample3">--}}
{{--                        <a class="drawer-menu-item mb-1">Child 1</a>--}}
{{--                        <a class="drawer-menu-item mb-1">Child 2</a>--}}
{{--                        <a class="drawer-menu-item mb-1">Child 3</a>--}}
{{--                        <a class="drawer-menu-item mb-1">Child 4</a>--}}
{{--                        <a class="drawer-menu-item mb-1">Child 5</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </li>--}}
    @else
{{--            <li> <a href="{{url('/dashboard')}}" class="drawer-menu-item">My Profile</a></li>--}}
            <li><a href="{{url('update-profile')}}" class="drawer-menu-item">Update Profile</a></li>
            <li><a href="{{url('services')}}" class="drawer-menu-item">Update Services</a></li>

{{--            <li><a href="{{url('reviews-ratings')}}" class="drawer-menu-item">Reviews &amp; Rating</a></li>--}}
{{--            <li><a href="{{url('patient/appoinment-booking')}}" class="drawer-menu-item">Appoinment Bookings</a></li>--}}
{{--            <li><a class="drawer-menu-item" href="#">Medical History</a></li>--}}
            <li>
                <a class="drawer-menu-item mb-1" data-toggle="collapse"
                href="#multiCollapseExample1" role="button" aria-expanded="false"
                aria-controls="multiCollapseExample1">Appointment Schedule <i class="fa fa-caret-down"></i></a>

                <div class="collapse multi-collapse pl-3" id="multiCollapseExample1">
                    <a class="drawer-menu-item mb-1" href="{{url('event/add')}}">Add Schedule</a>
                    <a class="drawer-menu-item mb-1" href="{{url('event/manage')}}">Manage Schedule</a>
{{--                    <a class="drawer-menu-item mb-1" href="{{url('event')}}">View Calendar</a>--}}
                </div>
            </li>

            <li><a href="{{url('booking/get-bookings')}}" class="drawer-menu-item">Appointment Bookings</a></li>
            <li><a href="{{url('live-consultation')}}" class="drawer-menu-item">Live Consultations</a></li>
            @if(Auth::user()->service_profile->where('user_role_id',6)->count())
                <li><a href="{{url('ambulance-bookings')}}" class="drawer-menu-item">Ambulance Bookings</a></li>
            @endif
            <li><a href="{{url('enquiries')}}" class="drawer-menu-item">Enquiries</a></li>
         @endif


    </ul>
</nav>
