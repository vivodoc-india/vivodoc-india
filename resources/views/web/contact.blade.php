@extends('layouts.layout')
@section('content')
    <section class="main">
        <div class="profile-view-section">

<div class="container">
	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
            <div class="card h-100">
                <div class="card-body">


			<div class="h5-responsive mb-2">Corporate Office</div>
			<div class="mb-3"><i class="fas fa-map-marker-alt"></i> G-2 Jagdeesh Apt. Indraapuri, Samanpura , Patna.800014 Bihar.</div>
			<div class="mb-3"><i class="fa fa-clock"></i> Mon-Sat (10:00-08:00)</div>
			<div class="mb-3"><i class="fa fa-envelope"></i> aalihealthcare@gmail.com</div>
			<div class="mb-3"><i class="fas fa-phone-alt"></i> +91 9308463744</div>
                </div>
            </div>
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
            <div class="card h-100">
                <div class="card-body">
			<div class="h5-responsive mb-2">Office</div>
			<div class="mb-3"><i class="fas fa-map-marker-alt"></i> AAH Digital Health Center AIIMS Road, Naya Tola Phulwarisharif, Patna</div>
			<div class="mb-3"><i class="fa fa-clock"></i> Mon-Sat (10:00-08:00)</div>
			<div class="mb-3"><i class="fas fa-phone-alt"></i> +91 9471002110</div>
        </div>
    </div>
		</div>
	</div>
	<div class="row mb-5">
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4">
            <div class="card h-100">
                <div class="card-body">
			<div class="h5-responsive mb-2">Branch Office</div>
			<div class="mb-3"><i class="fas fa-map-marker-alt"></i> AAH Digital Health Center DIGITAL MOBILE CLINIC JB-8, Lachipura, VDA Colony Nadesar, Andhrapul Varanasi</div>
			<div class="mb-3"><i class="fas fa-phone-alt"></i> +91 9102134744</div>
                </div>
            </div>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4">
            <div class="card h-100">
                <div class="card-body">
			<div class="h5-responsive mb-2">Branch Office</div>
			<div class="mb-3"><i class="fas fa-map-marker-alt"></i> AAH Digital Health Center DIGITAL MOBILE CLINIC Jivdhara Bazar, Near Masjid, Jivdhara, East Champaran, Bihar</div>
			<div class="mb-3"><i class="fas fa-phone-alt"></i> +91 8603047746</div>
                </div>
            </div>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4">
            <div class="card h-100">
                <div class="card-body">
			<div class="h5-responsive mb-2">Branch Office</div>
			<div class="mb-3"><i class="fas fa-map-marker-alt"></i> AAH Digital Health Center DIGITAL MOBILE CLINIC Sheikh Muhalla, Near Gyarahwi Masjid, Bangle street, Siwan Bihar</div>
			<div class="mb-3"><i class="fas fa-phone-alt"></i> +91 7992394071</div>
                </div>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
			<div class="card shadow-none border border-light">
				<div class="card-body">
                <form action="/contact-post" method="post">
                    @csrf
					<div class="form-group">
                        <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Your Name" name="name" value="{{old('name')}}" required>

					</div>
					 @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
                    <div class="form-group">
                        <input type="text" class="form-control @error('mobile') is-invalid @enderror" placeholder="Your Mobile No." name="mobile" value="{{old('mobile')}}" required oninput='this.value=(parseInt(this.value)||"")' maxlength="10">
                    </div>
                    @error('mobile')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
					<div class="form-group">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Your Email" name="email" value="{{old('email')}}" required>
					</div>
					@error('email')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
					<div class="form-group">
                        <textarea rows="4" class="form-control @error('message') is-invalid @enderror" placeholder="Your Message" name="message" value="{{old('message')}}" required></textarea>

					</div>
					 @error('message')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
                    <button type="submit" class="btn btn-primary rounded-pill mt-3">Send Message</button>
                    </form>
				</div>
			</div>
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14426.327964015598!2d82.98240154999999!3d25.3182446!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1578558017130!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		</div>
	</div>
</div>
</div>
    </section>
@endsection
