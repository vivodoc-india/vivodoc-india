@extends('layouts.layout')
@section('content')
    <section class="main">
        <div class="profile-view-section">
            <div class="container-fluid">
    <div class="container cp-md">
        <div class="card shadow-none border">
            <div class="card-body">

                @if(isset($orderDetails))
                    <!-- Test Mode URI -->
{{--                    <form id="redirectForm" method="post" action="https://test.cashfree.com/billpay/checkout/post/submit">--}}
                        <!-- Production Mode URI -->
                <form id="redirectForm" method="post" action="https://www.cashfree.com/checkout/post/submit">
                    <input type="hidden" name="appId" value="{{$orderDetails['appId']}}"/>
                    <input type="hidden" name="orderId" value="{{$orderDetails['orderId']}}"/>
                    <input type="hidden" name="orderAmount" value="{{$orderDetails['orderAmount']}}"/>
                    <input type="hidden" name="orderCurrency" value="{{$orderDetails['orderCurrency']}}"/>
                    <input type="hidden" name="orderNote" value="{{$orderDetails['orderNote']}}"/>
                    <input type="hidden" name="customerName" value="{{$orderDetails['customerName']}}"/>
                    <input type="hidden" name="customerEmail" value="{{$orderDetails['customerEmail']}}"/>
                    <input type="hidden" name="customerPhone" value="{{$orderDetails['customerPhone']}}"/>
                    <input type="hidden" name="returnUrl" value="{{$orderDetails['returnUrl']}}"/>
                    <input type="hidden" name="notifyUrl" value="{{$orderDetails['notifyUrl']}}"/>
                    <input type="hidden" name="signature" value="{{$orderDetails['signature']}}"/>
                </form>
                    @endif
                    <script>document.getElementById("redirectForm").submit();</script>
            </div>
        </div>
    </div>
            </div>
        </div>
    </section>
@endsection

