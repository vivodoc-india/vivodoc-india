
@extends('layouts.layout')
@section('content')

<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
            <div class="text-white h2-responsive text-uppercase">@if($category){{$category}}@endif</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid bg-blue-gradiant cp-md">
<div class="container">
	<div class="row">
        @if(isset($list))
            <div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4'>
                <div class='card h-100'>
                    <div class='row no-gutters'>
                        <div class='col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3'>
                            <div class='view zoom'>
                                <a href="{{url('service/'.$role.'/'.strtolower($category))}}">
                                    <img src="{{asset("web/img/$com_image")}}" class='img-fluid '>
                                </a>
                            </div>
                        </div>
                        <div class='col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8'>
                            <div class='card-body p-2'>
                                <div>
                                    <a href="{{url('service/'.$role.'/'.strtolower($category))}}"  style="font-weight:bold; color:#27166D;">All</a>
                                </div>
{{--                                <a href='{{url('service/'.$role.'/'.strtolower($category))}}' class='text-dark'>view all</a>--}}
                            </div>
                        </div>
                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 mt-4 ">
                            <a href="{{url('service/'.$role.'/'.strtolower($category))}}"
                               class='text-dark'> <i class="fa fa-angle-right" aria-hidden="true"></i></a>

                        </div>

                    </div>
                </div>
            </div>

        @foreach($list as $cat)

<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4'>
							<div class='card h-100'>
                                <div class='row no-gutters'>
                                    <div class='col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3'>
								<div class='view zoom'>
                                <a href='{{url('service/'.$role.'/'.preg_replace('/\s+/', '-', strtolower($cat->title)).'/'.$cat->id)}}'>

                                 <img src="{{asset("web/img/$com_image")}}" class='img-fluid'>
                                </a>
								</div>
                                    </div>
                                    <div class='col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8'>
								<div class='card-body p-1'>
                                <div>
                                    <a href='{{url('service/'.$role.'/'.preg_replace('/\s+/', '-', strtolower($cat->title)).'/'.$cat->id)}}'  style="font-weight:bold;color:#27166D;">{{ucwords($cat->title)}}</a>
									</div>
{{--                                <div class='h4-responsive text-danger mb-3'>--}}
                                    {{-- {{$cat->total}}&nbsp;@if( $cat->total > 1){{ $cat->title.'s'}}@else{{ $cat->title}}@endif --}}
{{--                                </div>--}}
                                    <div class="text-left">
                                        <a href='{{url('service/'.$role.'/'.preg_replace('/\s+/', '-', strtolower($cat->title)).'/'.$cat->id)}}'
                                           class='text-truncate text-dark' style="font-size: 9px; font-weight: bold;">{{ucwords($cat->general_title)}}</a>
                                    </div>


								</div>

                                    </div>
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 mt-4 ">
                                        <a href='{{url('service/'.$role.'/'.preg_replace('/\s+/', '-', strtolower($cat->title)).'/'.$cat->id)}}'
                                           class='text-dark'> <i class="fa fa-angle-right" aria-hidden="true"></i></a>

                                    </div>
							</div>
						</div>
</div>

        @endforeach
@endif

	</div>
</div>
</div>
@endsection
@section('additional_script')
    <script>
    $('document').ready(function(){

        @if($role==6)
            getLocation();
        @endif
            });

    function showLocation(position) {

    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    $.ajax({
    type:"GET",
    url:"{{url('ajax/saveLocation')}}?latitude="+latitude+"&longitude="+longitude,
    success:function(msg){

    }
    });
    // var latlongvalue = position.coords.latitude + ","
    //     + position.coords.longitude;
    {{--var img_url = "https://maps.googleapis.com/maps/api/staticmap?center="--}}
    {{--+latlongvalue+"&amp;zoom=14&amp;size=400x300&amp;key= {{env('GOOGLE_MAP_API_KEY')}}";--}}

    // document.getElementById("mapholder").innerHTML =
    //     "<img src='"+img_url+"'>";
    }
    function errorHandler(err) {
    if(err.code == 1) {
    alert("Error: Access is denied!");
    } else if( err.code == 2) {
    alert("Error: Position is unavailable!");
    }
    }
    function getLocation(){
    if(navigator.geolocation){
    // timeout at 60000 milliseconds (60 seconds)
    var options = {timeout:60000};
    navigator.geolocation.getCurrentPosition
    (showLocation, errorHandler, options);
    } else{
    alert("Sorry, browser does not support geolocation!");
    }
    }
    </script>
    @endsection

