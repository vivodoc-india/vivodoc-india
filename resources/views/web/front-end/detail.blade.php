@extends('layouts.layout')
@section('custom_style')
    <style>
        .hidden{
            display:none;
        }
        .swal2-title{
            font-size:1.5em!important;
            font-weight: 400!important;
        }
    </style>
@endsection
@section('content')
    <section class="main">
        <div class="profile-view-section">
            <div class="container">
                @if(isset($detail))
                    <div class="row">
                        <div class="col-12 col-md-8">
                            <div class="card provider-profile-container p-4 mb-4 profile">
                                <div class="row" style="border-bottom: 2px dashed #d9d9d9;margin-bottom: 24px; padding-bottom: 10px;">
                                    <div class="col-12 col-md-7">
                                        <div class="row">
                                            <div class="col-12 col-md-4">
                                                <div class="profile-image">
                                                    @if(!empty($detail->user->profile->photo))
                                                        <img src="{{asset('uploads/profile/'.$detail->user->profile->photo)}}"
                                                             class="rounded-circle" alt="{{$detail->user->name}}">
                                                    @else
                                                        <img src="{{asset('web/img/doctor-lg.png')}}" class="rounded-circle" alt="{{$detail->user->name}}">
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="col-12 col-md-8">
                                                <h1 style="font-weight: 700; font-size: 30px;" class="p-0 m-0 text-bold">{{ucwords($detail->user->name)}}</h1>
                                                <p class="p-0 m-0"><span style="font-weight: 700;color: #7887f8;">{{ucwords($detail->user->profile->speciality)}}</span></p>
                                                <span class="provider-label">
                                                 @if($detail->btn_call==1)
                                                        <a class="view-link" href="tel:{{$detail->user->mobile}}">
                                                        <i class="bi bi-telephone"></i>&nbsp; Call Now</a>
                                                    @else
                                                        <a class="view-link" href="#" disabled><i class="bi bi-telephone"></i>&nbsp; Call Now</a>
                                                    @endif
                                            </span>
                                                <span class="provider-label">
                                                    <a class="view-link" href="#"><i class="bi bi-chat"></i>&nbsp; Enquiry</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <div class="row">
                                            <div class="col-12 text-center text-warning" style="font-size: 32px; font-weight: 700;">
                                                <i class="bi bi-star-fill"></i>&nbsp;<span >4.5</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row provider-gellary">
                                    <div class="col-2">
                                        <a href="{{asset($detail->image1)}}" lass="gellary-images" data-title="Image 1">
                                            <img src="{{asset($detail->image1)}}"
                                                 alt="{{$detail->image1}}" class="img-fluid">
                                        </a>

                                    </div>
                                    <div class="col-2">
                                        <a href="{{asset($detail->image2)}}" lass="gellary-images" data-title="Image 2">
                                            <img src="{{asset($detail->image2)}}"
                                                 alt="{{$detail->image2}}" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="col-2">
                                        <a href="{{asset($detail->image3)}}" lass="gellary-images" data-title="Image 3">
                                            <img src="{{asset($detail->image3)}}"
                                                 alt="{{$detail->image3}}" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="col-2">
                                        <a href="{{asset($detail->image4)}}" lass="gellary-images" data-title="Image 4">
                                            <img src="{{asset($detail->image4)}}"
                                                 alt="{{$detail->image4}}" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="col-2">
                                        <a href="{{asset($detail->image5)}}" lass="gellary-images" data-title="Image 5">
                                            <img src="{{asset($detail->image5)}}"
                                                 alt="{{$detail->image5}}" class="img-fluid">
                                        </a>
                                    </div>
                                </div>
                                <div class="">
                                    <h5>About Doctor</h5>
                                    <p>{{$detail->description}}</p>
                                </div>

                            </div>
                            <div class="card p-4 mb-4 profile">
                                <div class="practice-datail-group" style="border-bottom: 2px dashed #d9d9d9;margin-bottom: 24px; padding-bottom: 10px;">
                                    <label class="text-label">Address - Location</label>
                                    <img src="https://vivodoc.com/assets/images/address-icon.svg">&nbsp;
                                    <span style="font-weight: 700;color: #7887f8;">{{$detail->landmark.' '.$detail->area}}</span>
                                    <p>{{$detail->address}}</p>
                                    <p>{{$detail->city.", ".$detail->state." ".$detail->user->profile->pincode}}</p>
                                    {{--                                <p>Our goal is to provide comprehensive care to our patients, focusing on quality and improved health outcomes rather than traditional clinic measures. This means more time spent with our patients and more services offered in one location for your convenience.</p>--}}
                                </div>
                                <div class="practice-datail-group">
                                    <label class="text-label">Specialty</label>
                                    <p>{{$detail->user->profile->speciality}}</p>
                                </div>
                                <div class="practice-datail-group">
                                    <label class="text-label">Consultation Fee</label>
                                    <div style="display: inline-flex;">
                                        <div class="">
                                            <span>In Person:</span>
                                            <span>₹{{$detail->max_fee}}</span>
                                        </div>
                                        <div style="margin-left: 20px;">
                                            <span>Live Video Consultation:</span>
                                            <span>₹{{$detail->min_fee}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="practice-datail-group">
                                    <label class="text-label">Degrees</label>
                                    <p>{{$detail->user->profile->qualification}}</p>
                                </div>
                                <div class="practice-datail-group">
                                    <label class="text-label">Languages</label>
                                    <p>English, Hindi</p>
                                </div>
                                <div class="practice-datail-group">
                                    <label class="text-label">Provider Gender</label>
                                    <p>{{ucwords($detail->user->profile->gender)}}</p>
                                </div>
                                <div class="practice-datail-group">
                                    <label class="text-label">Reg. Number</label>
                                    <p>{{$detail->reg_no}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 pr-4">
                            <div class="card p-3 appointment-form">
                                <div id="overlay">
                                    <div id="text">
                                       <h1 style="font-size:1.5rem; font-weight:600">Login to continue</h1>
                                       <p>To access our appointment & consultation form </p>
                                        <a href="{{url('login')}}" class="btn btn-full btn btn-read-more col-12">Login</a>
                                        <div class="text-center" style="font-size:1rem; font-weight:600">OR</div>
                                        <a href="{{route('register_patient')}}" class="btn btn-full btn btn-light col-12">Register</a>
                                    </div>
                                </div>
                                <h2>Book an appointment</h2>
                                <div class="appointment-banner">
                                    <img src="https://vivodoc.com/assets/images/map-calendar.svg">
                                </div>

                                <form id="request-form" method="post" action="@if(!empty($radio_check) && $radio_check==1){{url('patient/consultation-request')}}@endif @if(!empty($radio_check) && $radio_check==2){{url('patient/save-appointment')}} @endif
                                    ">
                                    @csrf
                                    <input type="hidden" name="category_id" value="{{Session::has('category_id')?Session::get('category_id'):0}}">
                                    <input type="hidden" name="provider_id" value="@if(isset($detail)){{$detail->user->id}}@endif">
                                    <input type="hidden" name="service_profile_id" value="@if(isset($detail)){{$detail->id}}@endif">
                                    <div class="text-left">
                                        <div class="row pb-3">
                                            <label>Looking in</label>
                                            <div class="col-12">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="request_for" id="appointment-radio"
                                                           value="2" {{!empty($radio_check)&& $radio_check==2?"checked":""}} required>
                                                    <label class="form-check-label" for="appointment-radio">Book an Appointment</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="request_for" id="live-radio"
                                                           value="1" {{!empty($radio_check)&& $radio_check==1?"checked":""}}>
                                                    <label class="form-check-label" for="live-radio">Live Video Consultation</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="con-stat" {{empty($radio_check) || (!empty($radio_check)&& $radio_check==2)?"class=hidden":""}}>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <label class="control-label">Today's Total Consultation</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <span class="badge badge-success" id="today_con">{{isset($today_consultation)?$today_consultation:'NA'}}</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <label class="control-label">Provider Availability</label>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                            <span class="badge {{isset($detail) && $detail->is_available==1?'badge-success':'badge-danger'}} text-wrap" id="provider_availability">
                        {{isset($detail) && $detail->is_available==1?'Available':'Busy'}}
                                            </span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    @if(isset($mynumber))
                                                        <label class="control-label">Your Term</label>
                                                    @else
                                                        <label class="control-label">Your Term (If you want to consult)</label>
                                                    @endif
                                                </div>
                                                <div class="col-md-2">
                                                    @if(isset($mynumber))
                                                        <span class="badge badge-info" id="const_number">{{$mynumber}}</span>
                                                    @else
                                                        <span class="badge badge-info" id="const_number">{{isset($today_consultation)?($today_consultation + 1):'NA'}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div id="appoint-stat" {{empty($radio_check) || (!empty($radio_check)&& $radio_check==1)?"class=hidden":""}}>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 ">
                                                    <h5 id="no-sched-msg">{{isset($schedule) && !empty($schedule)?$schedule:""}}</h5>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-10 col-sm-12 col-md-10 col-xl-10">
                                                        Today's Booking Allowed
                                                    </div>
                                                    <div class="col-md-2 col-sm-12 col-lg-2 col-xl-2">
                                                        <span id="appoint-total-book">{{isset($today_schedule)?$today_schedule->total_bookings:""}}</span>
                                                    </div>

                                                    <div class="col-lg-10 col-sm-12 col-md-10 col-xl-10">
                                                        Today's Total Bookings
                                                    </div>
                                                    <div class="col-md-2 col-lg-2 col-sm-12 col-xl-2">
                                                        <span id="appoint-today-book">{{isset($today_schedule)?$today_booking:""}}</span>
                                                    </div>
                                                    <div class="col-lg-10 col-sm-12 col-md-10 col-xl-10">
                                                        Today's Booking Remaining
                                                    </div>
                                                    <div class="col-md-2 col-lg-2 col-sm-12 col-xl-2">
                                                        <span id="appoint-rem-book"> {{isset($today_schedule)?$today_schedule->total_bookings - $today_booking:""}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card mb-5 hidden" id="bk-schd"
                                         style="background:#00FFFF; font-weight:bold;">
                                        <div class="card-body" id="bk-card-body">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3 {{empty($radio_check) || (!empty($radio_check)&& $radio_check==1)?"hidden":""}}" id="appoint-date">
                                            <label class="control-label">Appointment Date <span class="req">*</span></label><br>
                                            <div class="form-group">
                                                <input name="booking_date" value="{{old('booking_date')}}"
                                                       type="date"
                                                       placeholder="dd/mm/yyyy"
                                                       class="form-control border-round  rounded-0 @error('booking_date') is-invalid @enderror"
                                                       id="booking_date">
                                            </div>
                                            @error('booking_date')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="col-12 pb-3">
                                            <label>Patient's Name <span class="req">*</span></label>
                                            <div class="form-group">
                                                <input type="text" class="form-control @error('patient_name') is-invalid @enderror"
                                                       placeholder="Patient Name" name="patient_name"
                                                       value="@if(auth()->user() && auth()->user()->name!=''){{auth()->user()->name}} @else{{old('patient_name')}}@endif" required>
                                            </div>
                                            @error('patient_name')
                                            <div class="alert alert-danger">{{$message}}</div>
                                            @enderror
                                        </div>
                                        <div class="col-12 pb-3">
                                            <label class="control-label">Mobile No. <span class="req"></span></label>
                                            <div class="form-group">
                                                <input type="text" class="form-control @error('mobile') is-invalid @enderror"
                                                       placeholder="Mobile No." name="mobile"
                                                       value="@if(auth()->user() && auth()->user()->mobile!=''){{auth()->user()->mobile}} @else{{old('mobile')}}@endif"
                                                       oninput="this.value=(parseInt(this.value)||'')" maxlength="10">
                                            </div>
                                            @error('mobile')
                                            <div class="alert alert-danger">{{$message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 pb-3">
                                            <label class="control-label">Patient's Age <span class="req">*</span></label>
                                            <div class="form-group">
                                                <input type="text" class="form-control @error('age') is-invalid @enderror"
                                                       placeholder="Age" name="age" value="{{old('age')}}" required>
                                            </div>
                                            @error('age')
                                            <div class="alert alert-danger">{{$message}}</div>
                                            @enderror
                                        </div>
                                        <div class="col-6 pb-3">
                                            <label class="control-label">Patient's Gender <span class="req">*</span></label>
                                            <select class="form-control @error('gender') is-invalid @enderror" name="gender" required>
                                                <option selected disabled>Choose Gender</option>
                                                <option value="Male"
                                                 @if(auth()->user() && auth()->user()->profile->gender!='' && auth()->user()->profile->gender=="Male")
                                                     {{'selected'}}
                                                     @else
                                                @if(old('gender')=="Male"){{'selected'}} @endif
                                                @endif>Male
                                                </option>
                                                <option value="Female" @if(auth()->user() && auth()->user()->profile->gender!='' && auth()->user()->profile->gender=="Female")
                                                                            {{'selected'}}
                                                                            @else
                                                    @if( old('gender')=="Female"){{'selected'}} @endif
                                                    @endif>Female</option>
                                            </select>
                                            @error('gender')
                                            <br>
                                            <div class="alert alert-danger">{{$message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row pb-3">
                                        <div class="form-group">
                                            <label class="control-label">Address <span class="req">*</span></label>
                                            <input type="text" class="form-control @error('address') is-invalid @enderror"
                                                   placeholder="Address" name="address" value="{{old('address')}}" required>
                                        </div>
                                        @error('address')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div class="row">
                                        <div class="accordion" id="accordionExample">
                                            <div id="headingOne">
                                                <label class="text-danger">
                                                    <div class="" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Vital & Other Details <i class="bi bi-caret-down"></i>
                                                    </div>
                                                </label>
                                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                    <div class="card-body p-0">
                                                        <div class="row mt-2">
                                                            <div class="col-md-12 col-12 pb-3">
                                                                <label class="control-label active">Clinic Name /BikeDr</label>
                                                                <input type="text" class="form-control @error('bike_dr') is-invalid @enderror" placeholder="Clinic Name/BikeDr" name="bike_dr"
                                                                       value="@if(isset($booking)){{$booking->bike_dr}}@else{{old('bike_dr')}}@endif">

                                                                @error('bikedr')
                                                                <div class="alert alert-danger">{{$message}}</div>
                                                                @enderror
                                                            </div>


                                                            <div class="col-md-6 col-12 pb-3">
                                                                <label class="control-label">Patient's BP</label>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" placeholder="BP" name="bp"
                                                                           value="@if(isset($booking)){{$booking->bp}}@elseif(isset($vitals) && $vitals!=''){{$vitals->sys.'/'.$vitals->dia}}@else{{old('bp')}}@endif">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12 pb-3">
                                                                <label class="control-label">Patient's Pulse</label>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" placeholder="Pulse" name="pulse"
                                                                           value="@if(isset($booking)){{$booking->pulse}}@elseif(isset($vitals) && $vitals!='')
                                                                           {{$vitals->pulse}}@else {{old('pulse')}}@endif">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12 pb-3">
                                                                <label class="control-label">Patient's Temperature</label>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" placeholder="Temperature" name="temperature"
                                                                           value="@if(isset($booking)){{$booking->temperature}}@elseif(isset($vitals) && $vitals!='')
                                                                           {{$vitals->temperature}}@else {{old('temperature')}}@endif">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12 pb-3">
                                                                <label class="control-label">Patient's SPO<sub>2</sub></label>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" placeholder="SPO2" name="spo"
                                                                           value="@if(isset($booking)){{$booking->spo}}@elseif(isset($vitals) && $vitals!='')
                                                                           {{$vitals->spo}} @else {{old('spo')}}@endif">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12 pb-3">
                                                                <label class="control-label">Patient's Weight</label>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" placeholder="Weight" name="weight"
                                                                           value="@if(isset($booking)){{$booking->weight}}@else {{old('weight')}}@endif">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12 pb-3">
                                                                <label class="control-label">History</label>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" placeholder="History" name="history"
                                                                           value="@if(isset($booking)){{$booking->history}}@else{{old('history')}}@endif">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12 pb-3">
                                                                <label class="control-label">Complain</label>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" placeholder="Complain" name="complain"
                                                                           value="@if(isset($booking)){{$booking->complain}}@else {{old('complain')}}@endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-12 mb-2">
                                            <p id="def-msg"></p>
                                            <p class="">I agree to share my medical records.</p>
                                            <button type="submit"
                                                class="btn btn-read-more btn-lg btn-block">
                                                Send Request
                                            </button>
                                        </div>
                                    </div>
                            </form>
                        </div>
{{--                        <div class="col-12 card p-3">--}}
{{--                            <div class="card-body p-0">--}}
{{--                                <h2>Available Appointments</h2>--}}
{{--                                <!-- <div id="caleandar"></div> -->--}}
{{--                                <div class="pt-2">--}}
{{--                                    <div class="swiper apptCaleandar">--}}
{{--                                        <div class="swiper-wrapper">--}}
{{--                                            <div class="swiper-slide">--}}
{{--                                                <div class="row">--}}
{{--                                                    <div class="col-3">--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <div class="appt-date-cell">--}}
{{--                                                                12<sup>th</sup><br><span>Thu</span>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="col-3">--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <div class="appt-date-cell">--}}
{{--                                                                13<sup>th</sup><br><span>Fri</span>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="col-3">--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <div class="appt-date-cell">--}}
{{--                                                                14<sup>th</sup><br><span>Sat</span>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="col-3">--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <div class="appt-date-cell">--}}
{{--                                                                15<sup>th</sup><br><span>Sun</span>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-12">--}}
{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            --}}{{--                                            <div class="swiper-slide">--}}
{{--                                            --}}{{--                                                <div class="row">--}}
{{--                                            --}}{{--                                                    <div class="col-3">--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <div class="appt-date-cell">--}}
{{--                                            --}}{{--                                                                16<sup>th</sup><br><span>Mon</span>--}}
{{--                                            --}}{{--                                                            </div>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                    </div>--}}
{{--                                            --}}{{--                                                    <div class="col-3">--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <div class="appt-date-cell">--}}
{{--                                            --}}{{--                                                                17<sup>th</sup><br><span>Mon</span>--}}
{{--                                            --}}{{--                                                            </div>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                    </div>--}}
{{--                                            --}}{{--                                                    <div class="col-3">--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <div class="appt-date-cell">--}}
{{--                                            --}}{{--                                                                18<sup>th</sup><br><span>Tue</span>--}}
{{--                                            --}}{{--                                                            </div>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                    </div>--}}
{{--                                            --}}{{--                                                    <div class="col-3">--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <div class="appt-date-cell">--}}
{{--                                            --}}{{--                                                                19<sup>th</sup><br><span>Wed</span>--}}
{{--                                            --}}{{--                                                            </div>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                        <div class="col-12">--}}
{{--                                            --}}{{--                                                            <button type="button" class="btn time-btn">8:30 am</button>--}}
{{--                                            --}}{{--                                                        </div>--}}
{{--                                            --}}{{--                                                    </div>--}}
{{--                                            --}}{{--                                                </div>--}}
{{--                                            --}}{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-ec5b456c6da9556e" aria-disabled="false"></div>--}}
{{--                                        <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-ec5b456c6da9556e" aria-disabled="false"></div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
            </div>
            @endif
        </div>
{{--        <div class="similar-section">--}}
{{--            <div class="container">--}}
{{--                <div class="similar-sec-head">--}}
{{--                    <div>--}}
{{--                        <h2>Similar highly-rated Nurse Practitioner nearby</h2>--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        <a href="#" class="text-right">Find More Nurse Practitioner</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-12 col-lg-3">--}}
{{--                        <div class="dr-card">--}}
{{--                            <img src="https://i.pravatar.cc/100/u=22" class="rounded-circle">--}}
{{--                            <p class="dr-name">Jerria Bernestine</p>--}}
{{--                            <p class="dr-speciality">Nurse Practitioner</p>--}}
{{--                            <div class="star-rating">--}}
{{--                                <i class="bi bi-star-fill"></i>&nbsp;<span >2.5</span>--}}
{{--                            </div>--}}
{{--                            <button class="btn view-btn" type="button">View Profile</button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-12 col-lg-3">--}}
{{--                        <div class="dr-card">--}}
{{--                            <img src="https://i.pravatar.cc/100?u=12" class="rounded-circle">--}}
{{--                            <p class="dr-name">Jerria Bernestine</p>--}}
{{--                            <p class="dr-speciality">Nurse Practitioner</p>--}}
{{--                            <div class="star-rating">--}}
{{--                                <i class="bi bi-star-fill"></i>&nbsp;<span >2.5</span>--}}
{{--                            </div>--}}
{{--                            <button class="btn view-btn" type="button">View Profile</button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class=" col-12 col-lg-3">--}}
{{--                        <div class="dr-card">--}}
{{--                            <img src="https://i.pravatar.cc/100?u=2" class="rounded-circle">--}}
{{--                            <p class="dr-name">Jerria Bernestine</p>--}}
{{--                            <p class="dr-speciality">Nurse Practitioner</p>--}}
{{--                            <div class="star-rating">--}}
{{--                                <i class="bi bi-star-fill"></i>&nbsp;<span >2.5</span>--}}
{{--                            </div>--}}
{{--                            <button class="btn view-btn" type="button">View Profile</button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-12 col-lg-3">--}}
{{--                        <div class="dr-card">--}}
{{--                            <img src="https://i.pravatar.cc/100?u=3" class="rounded-circle">--}}
{{--                            <p class="dr-name">Jerria Bernestine</p>--}}
{{--                            <p class="dr-speciality">Nurse Practitioner</p>--}}
{{--                            <div class="star-rating">--}}
{{--                                <i class="bi bi-star-fill"></i>&nbsp;<span >2.5</span>--}}
{{--                            </div>--}}
{{--                            <button class="btn view-btn" type="button">View Profile</button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        </div>
    </section>


@endsection
@section('additional_script')
    <script>
        $(document).ready(function(){
            $('#booking_date').change(function(){
                var date=$(this).val();
                var provider = $('#provider_id').val();
                $.ajax({
                    type:"GET",
                    url:"{{url('ajax/check-booking-schedule')}}?date="+date+"&provider="+provider,
                    success:function(response){
                        if(response.success){
                            var html='';
                            html+='<div class="row">' +' <div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">' + ' Booking Allowed :  '+response.allowed_booking+' </div>' +
                                ' <div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">' +' Total Bookings Done : '+response.total_booked+' </div>' + '<div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">' +
                                'Booking Remaining :  '+ (response.allowed_booking - response.total_booked) + '</div></div>'+'<div class="row mt-2">' +'<div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">' + ' Doctor\'s Schedule :  '+ response.schedule + '</div><div class="col-lg-4 col-sm-12 col-md-4 col-xl-4"> Date : '+date+'</div></div>';
                            $('#bk-schd').show();
                            $('#bk-card-body').html(html);
                        }
                        else{
                            $('#bk-schd').hide();
                            Swal.fire({
                                position: 'top-end',
                                icon: 'error',
                                title: response.message,
                                showConfirmButton: false,
                                timer: 3000
                            })
                        }
                    }
                });
            });


        $('#appointment-radio').click(function(){
            $('#appoint-stat,#appoint-date').show();
            $('#con-stat').hide();
            $('#request-form').attr('action','{{url('patient/save-appointment')}}');
        });
        $('#live-radio').click(function(){
            $('#con-stat').show();
            $('#appoint-stat,#appoint-date').hide();
            $('#request-form').attr('action','{{url('patient/consultation-request')}}');
        });
        });
        @if(!auth()->user())
            $('#overlay').css('display','block');
        @else
        $('#overlay').css('display','none');
        @endif
    </script>
@endsection
