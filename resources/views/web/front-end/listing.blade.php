@extends('layouts.layout')
@section('content')
    <div class="modal fade" id="privateModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-notify modal-primary modal-dialog-centered" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header white-text text-center">
                    <h4 class="modal-title white-text w-100 font-weight-bold py-2">Verify Membership<br><small>This service is for members only</small></h4>
                </div>

                <!--Body-->
                <div class="modal-body">
                    <form action="{{route('validate-membership')}}" method="post" id="private-verification">
                        @csrf
                        <div class="md-form mb-5">
                            <i class="fas fa-key prefix grey-text"></i>
                            <input type="text" name="secret_key" id="secret" class="form-control" required>
                            <label data-error="wrong" data-success="right" for="secret">Enter Membership Key</label>
                        </div>
                        <input type="hidden" name="category" value="{{isset($is_private) && $is_private==1?$private_cat->id:''}}">
                        <input type="hidden" name="validate" value="" id="validate">
                        <!--Footer-->
                        <div class="justify-content-center">
                            <center>
                                <button type="button" class="btn btn-info rounded-0 " id="btn-verify">Verify</button>
                                <button type="button" class="btn btn-secondary rounded-0 " id="btn-cancel">Cancel</button>
                            </center>
                        </div>
                    </form>
                </div>

                <!--/.Content-->
            </div>
        </div>
    </div>
    <section class="main">
        <div class="profile-view-section">
        </div>
    </section>
@endsection
@section('additional_script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVe5kp9h2SDqZ-WN_-5koyH05qRIaXDL4&libraries=places&callback=initialize" async defer></script>
    <script src="{{asset('web/js/mapInput.js')}}"></script>
    <script src="{{asset('web/v2/js/main.js')}}"></script>
    <script>
        @if(Session::has('validate_private') && Session::get('validate_private'))
        $('#privateModal').modal('show');
        @endif
        @if(isset($list))
        @foreach($list as $sp)
            @if($sp['user_role_id']==6)
        $('document').ready(function(){
            @if(!Session::has('cur_latitude') && !Session::has('cur_longitude'))
            getLocation();
@endif
        });
@endif
        @endforeach
@endif
        function showLocation(position) {

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            $.ajax({
                type:"GET",
                url:"{{url('ajax/saveLocation')}}?latitude="+latitude+"&longitude="+longitude,
                success:function(success){
                if(success)
                {
                    location.reload();
                }
                }
            });
        }
        function errorHandler(err) {
            if(err.code == 1) {
                alert("Error: Access is denied!");
            } else if( err.code == 2) {
                alert("Error: Position is unavailable!");
            }
        }
        function getLocation(){
            if(navigator.geolocation){
                // timeout at 60000 milliseconds (60 seconds)
                var options = {timeout:60000};
                navigator.geolocation.getCurrentPosition
                (showLocation, errorHandler, options);
            } else{
                alert("Sorry, browser does not support geolocation!");
            }
        }
        @if(Session::has('cur_latitude') && Session::has('cur_longitude'))
        @foreach($list as $sp)
        @if($sp->longitude!='' && $sp->latitude!='')
            getDistanceFromLatLon({{Session::get('cur_latitude')}},{{Session::get('cur_longitude')}},{{$sp->latitude}},{{$sp->longitude}},{{$sp->id}});
    @endif
            @endforeach
        @endif
           $('.state-select').change(function() {
            $.ajax({
                type:"GET",
                url:"{{url('ajax/state')}}?state="+$(this).val(),
                success: function(response) {
                    if (response.success) {
                        $('.city-select').find('option').remove().end().append('<option selected hidden disabled>City</option>');
                        var city = response.city;
                        $.each(city, function(i, d) {
                            $('.city-select').append('<option>' + d.name + '</option>');
                        });
                    }
                },
            });
        });

        // City Change
        $('.city-select').change(function() {
            $.ajax({
                type:"GET",
                url:"{{url('ajax/city')}}?city="+$(this).val()+'&state='+$(this).parent().parent().parent().find('.state-select').val(),
                success: function(response) {
                    if (response.success) {
                        $('.area-select').find('option').remove().end().append('<option selected hidden disabled>Area</option>');
                        var area = response.area;
                        $.each(area, function(i, d) {
                            $('.area-select').append('<option>' + d.area + '</option>');
                        });
                    }
                },
            });
        });

        function submit()
        {
            $('#frm-filter').submit();
        }
        function resetForm()
        {
            $('.search-provider')[0].reset();
        }

        $('#customRange1').change(function(){
            $('#min_fee_val1').val($(this).val());
            submit();
        });
        $('#min_fee_val1').change(function(){
            $('#customRange1').val($(this).val());
            submit();
        });
        $('#customRange2').change(function(){
            $('#max_fee_val2').val($(this).val());
            submit();
        });
        $('#max_fee_val2').change(function(){
            $('#customRange2').val($(this).val());
            submit();
        });

        $('#customRange3').change(function(){
            $('#min_fee_val3').val($(this).val());
            submit();
        });
        $('#min_fee_val3').change(function(){
            $('#customRange3').val($(this).val());
            submit();
        });
        $('#customRange4').change(function(){
            $('#max_fee_val4').val($(this).val());
            submit();
        });
        $('#max_fee_val4').change(function(){
            $('#customRange4').val($(this).val());
            submit();
        });
        function getDistanceFromLatLon(lat1,lon1,lat2,lon2,id) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2-lat1);  // deg2rad below
            var dLon = deg2rad(lon2-lon1);
            var a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                Math.sin(dLon/2) * Math.sin(dLon/2)
            ;
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = Math.round(R * c); // Distance in km
            $('#dis'+id).html('<b>'+'Dist.: '+ d + ' KM </b>');
        }

        function deg2rad(deg) {
            return deg * (Math.PI/180)
        }

        $('document').ready(function(){
            $('#btn-find-ambulance').click(function(){
                var latitude = $('#address-latitude').val();
                var longitude = $('#address-longitude').val();
                if(latitude.length > 0 && longitude.length > 0)
                {
                    $.ajax({
                        type:"GET",
                        url:"{{url('ajax/saveLocation')}}?latitude="+latitude+"&longitude="+longitude,
                        success:function(msg){
                        if(msg.success)
                        {
                            location.reload();
                        }
                        }
                    });
                }
            });
        });
        $('#btn-verify').click(function(){
            $('#validate').val(1);
            $('#private-verification').submit();
        });
        $('#btn-cancel').click(function(){
            $('#validate').val(0);
            $('#private-verification').submit();
        });
    </script>
@endsection
