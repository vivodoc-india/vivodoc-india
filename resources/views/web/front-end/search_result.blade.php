
<div class="profile-view-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="search-input-box" style="margin-bottom: 20px; ">
                    <div class="container-fluid px-0">
                        <div class="row">
                            <div class="col-12 col-md-4 col-lg-4 ">
                                <div class="input-group search-box-group box-shadow py-2">
                                    <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i
                                                        class="bi bi-search"></i></span>
                                    </div>
                                    <select class="form-control" id="search_for" name="search_for" required>
                                        @if(isset($categories) && $categories->isNotEmpty())
                                            @foreach($categories as $cat)
                                                <option value="{{$cat->id}}" {{isset($search_for) && $search_for==$cat->id?"selected":""}}>{{$cat->id!==9?$cat->name:"Other ".$cat->name}}</option>
                                            @endforeach

                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 col-lg-3 bl">
                                <div class="input-group search-box-group box-shadow py-2">
                                    <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i
                                                        class="bi bi-person"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Name, Specialty, Symptoms                                                    "
                                           aria-label="Name" name="search_field" aria-describedby="basic-addon1" value="{{$search_field ?? ""}}">
                                </div>
                            </div>
                            <div class="col-12 col-md-3 col-lg-3 bl">
                                <div class="input-group search-box-group box-shadow py-2">
                                    <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i
                                                        class="bi bi-map"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="State, City, Area"
                                           aria-label="Name" name="search_loc" aria-describedby="basic-addon1" value="{{$search_loc ?? ""}}">
                                </div>
                            </div>
                            <div class="col-12 col-md-2 col-lg-2 p-0 px-0 search-box-button">
                                <a href=""
                                   class="btn-read-more d-inline-flex align-items-center justify-content-center align-self-center">
                                    <span>SEARCH</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 list-tab">
                <ul class="nav nav-pills text-center mb-1" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-tab1" data-toggle="pill" href="#pills1" role="tab"
                           aria-controls="pills1" aria-selected="true">All Doctors</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="pills-tab2" data-toggle="pill" href="#pills2" role="tab"
                           aria-controls="pills2" aria-selected="true">Video Consultations</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="pills-tab3" data-toggle="pill" href="#pills3" role="tab"
                           aria-controls="pills3" aria-selected="true">Appointments</a>
                    </li>
                </ul>
            </div>
            <div class="col-12">
                <div class="filters-row">
                    <div class="filter-btn dropdown">
                                <span class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    General Illness
                                </span>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <div class="datepicker"></div>
                        </div>
                    </div>
                    <div class="filter-btn">Language</div>
                    <div class="filter-btn">Speciality</div>
                    <div class="filter-btn dropdown">
                                <span class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Sort by <i class="bi bi-filter"></i>
                                </span>
                        <div class="dropdown-menu p-2" aria-labelledby="dropdownMenuButton">
                            <div><a class="link">Fee</a></div>
                            <div><a class="link">Rating</a></div>
                            <div><a class="link">A-Z alphabet</a></div>
                        </div>


                    </div>
                    </ul>
                </div>
            </div>
            <div class="provider-list-head">
                <div class="col-12">
                    <h1>{{$total_count ?? '' }} {{$search_type[0]}}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-8">
                @if(isset($list) && $list->isNotEmpty())
                    @foreach($list as $sp)
                        <!-- Provider's List-->
                            <div class="col-12 col-sm-12 mb-4">
                                <div class="card h-90">
                                    <div class="card-body">
                                        <div class="row provider-info">
                                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 h-mob d-inline-flex
                                    align-items-center justify-content-center align-self-center">
{{--                                                <div class="provider-img">--}}
{{--                                                    <a href='{{url('detail/'.$sp['id'])}}'>--}}
{{--                                                        @if(!empty($sp->user->profile->photo))--}}
{{--                                                            <img src="{{asset('uploads/profile/'.$sp->user->profile->photo)}}" class="rounded-circle profile-image">--}}
{{--                                                        @else--}}
{{--                                                            <img src="{{asset('web/img/doctor-lg.png')}}" class="rounded-circle profile-image">--}}
{{--                                                        @endif--}}
{{--                                                    </a>--}}
{{--                                                    @if($sp->mark_live == 1)--}}
{{--                                                        @if($sp['btn_video']==1)--}}
{{--                                                            <div class="rounded-circle profile-img-button bg-success">--}}
{{--                                                                <a href="{{url('detail/'.$sp['id'].'?req_type=1')}}"--}}
{{--                                                                   --}}{{--                                                        <a href="{{url('patient/consult-now/'.$sp['id'])}}" --}}
{{--                                                                   class="provider-active-button">--}}
{{--                                                                    <i class="fa-solid fa-video"></i>--}}
{{--                                                                </a>--}}
{{--                                                            </div>--}}
{{--                                                        @else--}}
{{--                                                            <div class="rounded-circle profile-img-button bg-success" disabled>--}}
{{--                                                                <a href="#" class="provider-active-button"><i class="fa-solid fa-video"></i></a>--}}
{{--                                                            </div>--}}
{{--                                                        @endif--}}
{{--                                                    @else--}}
{{--                                                        @if($sp['btn_booknow']==1)--}}
{{--                                                            <div class="rounded-circle profile-img-button bg-danger">--}}
{{--                                                                <a href="{{url('detail/'.$sp['id'].'?req_type=2')}}"--}}
{{--                                                                   --}}
                                                {{--    <a href="{{url('patient/book-appointment/'.$sp['user_id'])}}" --}}
{{--                                                                   class="provider-active-button">--}}
{{--                                                                    <i class="fa-solid fa-calendar"></i>--}}
{{--                                                                </a>--}}
{{--                                                            </div>--}}
{{--                                                        @else--}}
{{--                                                            <div class="rounded-circle profile-img-button bg-danger" disabled>--}}
{{--                                                                <a href="#" class="provider-active-button"><i class="fa-solid fa-calendar"></i></a>--}}
{{--                                                            </div>--}}
{{--                                                        @endif--}}
{{--                                                    @endif--}}
{{--                                                </div>--}}
                                                <div class="provider-img">
                                                    <div class="provider-img-box">
                                                        <img
                                                            src="{{$sp->user->profile->photo!=''?asset('uploads/profile/'.$sp->user->profile->photo):asset('web/img/doctor-lg.png')}}"
                                                             class="rounded-circle profile-image" alt="{{$sp->user->name}}" width="100px" height="100px">
                                                        @if($sp->mark_live == 1)
                                                            @if($sp['btn_video']==1)
                                                                <div class="rounded-circle profile-img-button bg-success">
                                                                    <a href="{{url('detail/'.$sp['id'].'?req_type=1')}}"
                                                                       class="provider-active-button"><i class="fa-solid fa-video"></i></a>
                                                                </div>
                                                            @else
                                                                <div class="rounded-circle profile-img-button bg-success" disabled>
                                                                    <a href="#" class="provider-active-button">
                                                                        <i class="fa-solid fa-video"></i></a>
                                                                </div>
                                                            @endif
                                                        @else
                                                            @if($sp['btn_booknow']==1)
                                                                <div class="rounded-circle profile-img-button bg-danger">
                                                                    <a href="{{url('detail/'.$sp['id'].'?req_type=2')}}"
                                                                       class="provider-active-button">
                                                                        <i class="fa-solid fa-calendar"></i>
                                                                    </a>
                                                                </div>
                                                            @else
                                                                <div class="rounded-circle profile-img-button bg-danger" disabled>
                                                                    <a href="#" class="provider-active-button"><i class="fa-solid fa-calendar"></i></a>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                                                <div class="row">
                                                    <div class="provider-name">
                                                        <h5>{{$sp->user->name}}</h5>
                                                        <p>{{$sp->user->profile->qualification ?? ''}}</p>
                                                        <p>{{$sp->user->profile->speciality ?? ''}}</p>
                                                        <p>{{$sp['city'].', '. $sp['state']}}</p>
                                                        <!-- <div class="provider-rating"><i class="bi bi-star-fill"></i><span>2.5</span></div> -->
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="text-dark mb-1 font-sm control-label d-block text-truncate">
                                                            <i class="bi bi-star-fill text-warning"></i>&nbsp;<span class="text-warning" >4.5</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-6 mb-1 service-info-text">
                                                        <b>Exp : {{$sp->user->profile->experiance ?? 'NA'}}
                                                            Year(s)</b></div>
                                                </div>
                                                <div class="row small text-black-50 no-gutters">

                                                    <div class="col-6 mb-1 text-success service-info-text"> <b> <i class="bi bi-camera-video"></i>&nbsp; Fee: &#8377;  {{$sp['min_fee']}}
                                                        </b>
                                                    </div>
                                                    <div class="col-6 mb-1 text-danger service-info-text">
                                                        <b> <i class="bi bi-calendar2-check"></i>&nbsp;Fee: &#8377;
                                                            {{$sp['max_fee']}}
                                                        </b>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 p-0">
                                                <div class="appt-time-row">
                                                    <div class="appt-time-column">
                                                        <div class="appt-time-cell">
                                                            <button type="button" class="btn time-btn">8:30 am</button>
                                                        </div>
                                                        <div class="appt-time-cell">
                                                            <button type="button" class="btn time-btn">8:30 am</button>
                                                        </div>
                                                        <div class="appt-time-cell">
                                                            <button type="button" class="btn time-btn">8:30 am</button>
                                                        </div>
                                                    </div>
                                                    <div class="appt-time-column">
                                                        <div class="appt-time-cell">
                                                            <button type="button" class="btn time-btn">8:30 am</button>
                                                        </div>
                                                        <div class="appt-time-cell">
                                                            <button type="button" class="btn time-btn">8:30 am</button>
                                                        </div>
                                                        <div class="appt-time-cell">
                                                            <button type="button" class="btn time-btn">8:30 am</button>
                                                        </div>
                                                    </div>
                                                    <div class="appt-time-column">
                                                        <div class="appt-time-cell">
                                                            <button type="button" class="btn time-btn">8:30 am</button>
                                                        </div>
                                                        <div class="appt-time-cell">
                                                            <button type="button" class="btn time-btn">8:30 am</button>
                                                        </div>
                                                        <div class="appt-time-cell">
                                                            <button type="button" class="btn time-btn">8:30 am</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="link-margin col-12 col-md-4 text-center">
                                                <a class="view-link" href="{{url('detail/'.$sp['id'])}}">
                                                    <i class="bi bi-person"></i>&nbsp; View Profile</a>
                                            </div>
                                            <div class="link-margin col-12 col-md-4 text-center">
                                                @if($sp->btn_call==1)
                                                    <a class="view-link" href="tel:{{$sp->user->mobile}}">
                                                        <i class="bi bi-telephone"></i>&nbsp; Call Now</a>
                                                @else
                                                    <a class="view-link" href="#" disabled><i class="bi bi-telephone"></i>&nbsp; Call Now</a>
                                                @endif

                                            </div>
                                            <div class="link-margin col-12 col-md-4 text-center">
                                                <a class="view-link" href="#"><i class="bi bi-chat"></i>&nbsp; Enquiry</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    @endforeach
                @endif

                <!--End Of provider's List -->
                    @if(isset($list) && $list->isNotEmpty())
                        <div class="row mb-3" style="height:42px;">
                            <div class="col-12">
                                <div class="mt-1">
                                    {{$list->appends($quiries)->links()}}
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-12 col-md-4 pr-4">
                    <div class="mapouter"><div class="gmap_canvas"><iframe height="500" width="400" id="gmap_canvas" src="https://maps.google.com/maps?q=2880%20Broadway,%20New%20York&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><style>.mapouter{position:relative;height:500px;}</style><style>.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style></div></div>
                </div>
            </div>
        </div>
    </div>
</div>
