@extends('layouts.layout')
@section('content')
    <section class="main">


    <div class="modal fade" id="privateModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-notify modal-primary modal-dialog-centered" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header white-text text-center">
                    <h4 class="modal-title white-text w-100 font-weight-bold py-2">Verify Membership<br><small>This service is for members only</small></h4>
                </div>

                <!--Body-->
                <div class="modal-body">
                    <form action="{{route('validate-membership')}}" method="post" id="private-verification">
                        @csrf
                        <div class="md-form mb-5">
                            <i class="fas fa-key prefix grey-text"></i>
                            <input type="text" name="secret_key" id="secret" class="form-control" required>
                            <label data-error="wrong" data-success="right" for="secret">Enter Membership Key</label>
                        </div>
                        <input type="hidden" name="category" value="{{isset($is_private) && $is_private==1?$private_cat->id:''}}">
                        <input type="hidden" name="validate" value="" id="validate">
                        <!--Footer-->
                        <div class="justify-content-center">
                            <center>
                                <button type="button" class="btn btn-info rounded-0 " id="btn-verify">Verify</button>
                                <button type="button" class="btn btn-secondary rounded-0 " id="btn-cancel">Cancel</button>
                            </center>
                        </div>
                    </form>
                </div>

                <!--/.Content-->
            </div>
        </div>
    </div>
{{--<form action="" method="get" class="search-provider" id="frm-search">--}}
{{--        <div class="container-fluid fnmf cp-md" id="fnmf">--}}
{{--        	<div class="">--}}
{{--        		<div class="row justify-content-center ">--}}
{{--        			<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">--}}
{{--        				<div class="text-white h3-responsive mb-1">Find the Nearest Healthcare Facility</div>--}}
{{--        				<div class="tab-content p-3" id="myTabContent">--}}
{{--                    <div class="tab-pane active">--}}
{{--                        <div class="row no-gutters">--}}
{{--  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 mb-2">--}}
{{--      <label class="text-white">Search For</label>--}}
{{--      <div class="form-group mb-0">--}}
{{--     <select class="form-control rounded-0 opt-value" style="width:100%" required name="search_for" id="role-select">--}}
{{--     <option disabled selected>Select one</option>--}}
{{--         @foreach($categories as $cat)--}}
{{-- <option value="{{$cat->id}}" {{isset($search_for) && $search_for==$cat->id?'selected':''}}>{{ucwords($cat->name)}}</option>--}}
{{--  @endforeach--}}
{{--   </select>--}}
{{--     </div>--}}
{{--  </div>--}}

{{--                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 mb-2">--}}
{{--                                <label class="text-white">Name</label>--}}
{{--                                <div class="form-group mb-0">--}}
{{--                                    <input type="text" name="name" class="form-control" placeholder="Enter name ">--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 mb-2">--}}
{{--                                <label class="text-white">Category</label>--}}
{{--                                <div class="form-group mb-0">--}}
{{--                                    <select class="form-control rounded-0 opt-value category-select" style="width:100%" name="category" id="category">--}}
{{--                                        @if(isset($all_category) && $all_category->isNotEmpty())--}}
{{--                                            @foreach($all_category as $cat)--}}
{{--                                                <option value="{{$cat->id}}" {{isset($searched_category) && $searched_category==$cat->id?'selected':''}}>{{$cat->title}}</option>--}}

{{--                                                @endforeach--}}

{{--                                            @endif--}}
{{--                                                                  </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-xl-3 col-lg-3 co-md-3 col-sm-12 col-12 align-self-end pb-2">--}}
{{--                                <button type="submit" class="btn btn-warning btn-block" style="height:40px;">Search</button>--}}
{{--                            </div>--}}
{{--                    </div>--}}
{{--                        <div class="row no-gutters" id="advance-row" style="display:none;">--}}
{{--                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">--}}
{{--                                <div class="form-group mb-0">--}}
{{--                                    <select class="form-control rounded-0 opt-value state-select" name="state"  style="width:100%">--}}
{{--                                        <option disabled selected>State</option>--}}
{{--                                        @foreach($states as $s)--}}
{{--                                            <option value="{{$s->name}}">{{ucwords($s->name)}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">--}}
{{--                                <div class="form-group mb-0">--}}
{{--                                    <select class="form-control rounded-0 opt-value city-select" name="city" style="width:100%">--}}

{{--                                                                 </select>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}

{{--    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 ">--}}
{{--   <div class="form-group mb-0">--}}
{{--                                                       <select class="form-control rounded-0 opt-value area-select" name="area" style="width:100%">--}}
{{--                                                        </select>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                            <div class="row">--}}
{{--                                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-right">--}}
{{--<button type="button" class="btn btn-link text-white text-decoration-none" id="btn-advance-search">Advance Search</button>--}}

{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--        				</div>--}}
{{--        			</div>--}}
{{--        		</div>--}}

{{--        	</div>--}}
{{--        </div>--}}
{{--    @if(isset($search) && $search!='')--}}
{{--        <div class="container-fluid cp-md">--}}
{{--            <div class="row">--}}
{{--                    <div class="col-lg-12 col-md-12 mt-5">--}}
{{--                        <div class="card">--}}
{{--                            <div class="card-body">--}}
{{--                                <h3 class="text-muted text-center">{{$search}}</h3>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--        @endif--}}
    @if(isset($list) && $list->isNotEmpty())
    @include('web.front-end.search_result')
    @else
</form>
    @endif
    @endsection

    </section>
@section('additional_script')
    <script>
        @if(Session::has('validate_private') && Session::get('validate_private'))
        $('#privateModal').modal('show');
        @endif
        @if(isset($search_for))
        window.scrollBy(0, 550);
            @endif
        $('#btn-advance-search').click(function(){
            $('#advance-row').toggle();
        });
        $('#role-select').change(function(){
            $.ajax({
                type:"GET",
                url:"{{url('ajax/subcategory')}}/"+$(this).val(),
                success: function(response) {
                    if (response.success) {
                        $('#category').find('option').remove().end().append('<option selected hidden disabled>Select Category</option>');
                        var category = response.category;
                        $.each(category, function(i, d) {
                            $('#category').append('<option value="'+d.id+'">' + d.title + '</option>');
                        });
                    }
                },
            });
        });
        $('.state-select').change(function() {
            $.ajax({
                type:"GET",
                url:"{{url('ajax/state')}}?state="+$(this).val(),
                success: function(response) {
                    if (response.success) {
                        $('.city-select').find('option').remove().end().append('<option selected hidden disabled>City</option>');
                        var city = response.city;
                        $.each(city, function(i, d) {
                            $('.city-select').append('<option>' + d.name + '</option>');
                        });
                    }
                },
            });
            $('input[name="state"]').val($(this).val());
        });

        // City Change
        $('.city-select').change(function() {
            $.ajax({
                type:"GET",
                url:"{{url('ajax/city')}}?city="+$(this).val()+'&state='+$(this).parent().parent().parent().find('.state-select').val(),
                success: function(response) {
                    if (response.success) {
                        $('.area-select').find('option').remove().end().append('<option selected hidden disabled>Area</option>');
                        var area = response.area;
                        $.each(area, function(i, d) {
                            $('.area-select').append('<option>' + d.area + '</option>');
                        });
                    }
                },
            });
            $('input[name="city"]').val($(this).val());
        });

    $('.area-select').change(function(){
        $('input[name="area"]').val($(this).val());
    });
    // $('.btn-search').click(function(e){
    //     e.preventDefault();
    //
    // });
        function submit()
        {
            $('.search-provider').submit();
        }
        function resetForm()
        {
            $('.search-provider')[0].reset();
        }

        $('#customRange1').change(function(){
            $('#min_fee_val1').val($(this).val());
            submit();
        });
        $('#min_fee_val1').change(function(){
            $('#customRange1').val($(this).val());
            submit();
        });
        $('#customRange2').change(function(){
            $('#max_fee_val2').val($(this).val());
            submit();
        });
        $('#max_fee_val2').change(function(){
            $('#customRange2').val($(this).val());
            submit();
        });

        $('#customRange3').change(function(){
            $('#min_fee_val3').val($(this).val());
            submit();
        });
        $('#min_fee_val3').change(function(){
            $('#customRange3').val($(this).val());
            submit();
        });
        $('#customRange4').change(function(){
            $('#max_fee_val4').val($(this).val());
            submit();
        });
        $('#max_fee_val4').change(function(){
            $('#customRange4').val($(this).val());
            submit();
        });
        $(document).ready(function(){
            $("#btn-filter-serach").click(function(){
                $("#filter-box-search").toggle();
            });
        });
        $('#btn-verify').click(function(){
            $('#validate').val(1);
            $('#private-verification').submit();
        });
        $('#btn-cancel').click(function(){
            $('#validate').val(0);
            $('#private-verification').submit();
        });
</script>
        @endsection
