@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Ambulance Booking</div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid  cp-md">

        <div class="card mb-5" style="background: #33B5E5; font-weight: bold;color:#fff;">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 ">
{{--                            <h5>{{$schedule}}</h5>--}}
                                <div class="row">
                                    <div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">
                                        Provider Name : {{isset($provider) ? $provider->user->name:''}}
                                    </div>

                                    <div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">
                                        Vehicle Number : {{isset($provider) ? $provider->vehicle_no:''}}
                                    </div>

                                    <div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">
                                        Distance : <span id="dist"></span>
                                    </div>
                                    @if(isset($booking) && $booking->is_request_accepted==1 && $booking->is_paid==1)
                                        <div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">
                                            Mobile Number : {{$booking->provider->mobile}}
                                        </div>
                                         @endif
                                    @if(isset($booking) && $booking->is_request_accepted==1 && $booking->approx_fare!='')
                                        <div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">
                                            Approx Fair : Rs. {{$booking->approx_fare}}
                                        </div>
                                        @endif
                                </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-5" id="bk-schd" style="display:none; background:#00FFFF; font-weight:bold;">
            <div class="card-body" id="bk-card-body">

            </div>
        </div>


        <div class="row">
            <div class="col-lg-6 col-sm-12 col-md-6 col-xl-6">
                <div class="card">
                    <div class="card-header bg-white">Book Ambulance</div>
                    <div class="card-body">
                        @php $user =  Auth::user(); @endphp
                        <form action="{{url('save-ambulance-booking')}}" method="post">
                            @csrf
                            <div class="row mb-3">
                                 <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Patient Name <span class="req">*</span></label>
                                    <div class="form-group">
                                        <input name="patient_name" type="text"  placeholder="patient name"
                                               class="form-control border-round  rounded-0 @error('patient_name')
                                                   is-invalid @enderror" required value="{{isset($booking) ? $booking->patient_name:$user->name}}">
                                    </div>
                                    @error('patient_name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Patient Age <span class="req">*</span></label><br>
                                    <div class="form-group">
                                        <input name="age" type="text" placeholder="patient age"
                                               class="form-control border-round  rounded-0 @error('age') is-invalid @enderror"
                                               required value="{{isset($booking)?$booking->age:old('age')}}">
                                    </div>
                                    @error('age')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Patient's Mobile </label>
                                    <div class="form-group">

                                        <input name="mobile"  type="text" placeholder="Enter mobile no."
                                               class="form-control border-round  rounded-0 @error('mobile')
                                                   is-invalid @enderror" id="mobile" value="{{isset($booking)?$booking->mobile:$user->mobile}}" oninput='this.value=(parseInt(this.value)||" ")'>
                                    </div>
                                    @error('mobile')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Patient's Gender <span class="req">*</span></label><br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input  @error('gender') is-invalid @enderror" required id="customRadio"
                                               name="gender" value="Male"
                                               @if(isset($booking) && $booking->gender=="Male")
                                                   {{'checked'}}
                                                   @endif
                                            @if($user->profile->exists() && $user->profile->gender=="Male")
                                                {{'checked'}}
                                                @endif
                                            >
                                        <label class="custom-control-label" for="customRadio">Male</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input  @error('gender') is-invalid @enderror"
                                               id="customRadio2" name="gender" value="Female"
                                        @if(isset($booking) && $booking->gender=="Female")
                                            {{'checked'}}
                                            @endif
                                        @if($user->profile->exists() && $user->profile->gender=="Female")
                                            {{'checked'}}
                                            @endif
                                        >
                                        <label class="custom-control-label" for="customRadio2">Female</label>
                                    </div>
                                    @error('gender')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <label class="control-label">Pickup Address <span class="req">*</span></label><br>
                                    <div class="form-group">
                                        <input name="address" id="pic-location" type="text" placeholder="address"
                                               class="form-control border-round  rounded-0 @error('address') is-invalid @enderror"
                                               required value="{{isset($booking)?$booking->address:old('address')}}">
                                    <input type="hidden" name="longitude" value="{{isset($booking)?$booking->longitude:Session::get('cur_longitude')}}">
                                    <input type="hidden" name="latitude" value="{{isset($booking)?$booking->latitude:Session::get('cur_latitude')}}">
                                    </div>
                                    @error('address')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <label class="control-label">Destination Address <span class="req">*</span></label><br>
                                    <div class="form-group">
                                        <input name="destination" type="text" placeholder="address"
                                               class="form-control border-round  rounded-0 @error('destination') is-invalid @enderror"
                                               required value="{{isset($booking)?$booking->destination:old('destination')}}">
                                    </div>
                                    @error('destination')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <input type="hidden" name="provider_id" value="@if(isset($provider)){{$provider->user_id}}@endif" id="provider_id">
                            <input type="hidden" name="service_profile_id" value="@if(isset($provider)){{$provider->id}}@endif" id="sp_id">
                            @if(isset($booking) && $booking->advance_payment!='' && $booking->advance_payment > 0)
                                <div class="col-md-12 col-sm-12 col-lg-12">
                                    <h4 class="text-info">Pay Rs.  {{$booking->advance_payment}} to get the ambulance.</h4>
                                </div>
                            @endif
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                @if(!isset($booking))
                                    <button type="submit" class="btn btn-success"> Send Request</button>
                                    @endif
                            </div>
                        </form>
                        @if(isset($booking) && $booking->is_request_accepted==1 && $booking->is_paid==0)
                            <form action="{{url('patient/pay-ambulance-fee')}}" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{$booking->id}}">
                                <button type="submit" class="btn btn-success"> Pay Now</button>
                            </form>
                        @endif
                    </div>

                </div>

            </div>
            <div class="col-lg-6 col-sm-12 col-md-6 col-xl-6">

                    <div class="card  mb-4">
                        <div class="card-header bg-white">Ambulance Route</div>
                        <div class="card-body">
                            <div id="map-layer" style="height: 700px; ">

                            </div>

                        </div>

                    </div>
            </div>
        </div>
    </div>

@endsection
@section('additional_script')
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVe5kp9h2SDqZ-WN_-5koyH05qRIaXDL4&callback=initMap">
    </script>

    <script>
        @if(isset($booking) && !empty($booking))
        @if($booking->is_request_rejected==0 && $booking->is_request_accepted==0)
        bookingRequestStat({{$booking->id}});
        @endif
        @endif
        function bookingRequestStat(res_code) {
            interval = setInterval( function() {
                $.ajax({
                    type:'ajax',
                    method:'post',
                    data:{
                        '_token':"{{ csrf_token() }}",
                        'res_code' : res_code,
                        'uid': "{{auth()->user()->id}}",

                    },
                    url:"{{url('patient/checkAmbulanceBookingResponse')}}",
                    dataType:'json',
                    asycn:true,
                    success:function(response){
                        if(response.success==="true")
                        {
                            clearInterval(interval);
                            // $('#frm-pay,#btn-pay').show();
                            location.reload();
                            // $('#btn-call').show().attr('data',response.link);
                        }
                        else{
                            if(response.stop)
                            {
                                clearInterval(interval);
                                // $('#frm-pay,#btn-pay').hide();
                                location.reload();
                                // $('#btn-call').hide().removeAttr('data');
                            }
                            else{
                                bookingRequestStat(response.res_code);
                            }
                        }
                    }
                });
            }, 1000*20);
        }
        @if($provider->longitude!='' && $provider->latitude!='')
        getDistanceFromLatLon({{!isset($booking)?Session::get('cur_latitude'):$booking->latitude}},{{!isset($booking)?Session::get('cur_longitude'):$booking->longitude}},{{$provider->latitude}},{{$provider->longitude}});
            @endif
        var map;
        var pathCoordinates = Array();
        function initMap() {
            setPicupLocation();
            var mapLayer = document.getElementById("map-layer");
            // var centerCoordinates = new google.maps.LatLng(28.7041, 77.1025);
            var centerCoordinates = new google.maps.LatLng({{!isset($booking)?Session::get('cur_latitude'):$booking->latitude}}, {{!isset($booking)?Session::get('cur_longitude'):$booking->longitude}});
            var defaultOptions = {
                center : centerCoordinates,
                zoom : 6
            }
            map = new google.maps.Map(mapLayer, defaultOptions);
            geocoder = new google.maps.Geocoder();
            {{--addresses = geocoder.getFromLocation({{Session::get('cur_latitude')}}, {{Session::get('cur_longitude')}}, 1);--}}
            {{--$('#pic-location').val(addresses.get(0).getAddressLine(0));--}}
            @for($i=1;$i<=2;$i++)
            geocoder.geocode({
                'address' : 'Test'

            }, function(LocationResult, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    var latitude = {{isset($provider) && $provider->latitude!=''?$provider->latitude:''}};
                    var longitude = {{isset($provider) && $provider->longitude!=''?$provider->longitude:''}};
                    pathCoordinates.push({
                        lat : {{!isset($booking)?Session::get('cur_latitude'):$booking->latitude}},
                        lng : {{!isset($booking)?Session::get('cur_longitude'):$booking->longitude}}
                    });
                    pathCoordinates.push({
                        lat : latitude,
                        lng : longitude
                    });

                    new google.maps.Marker({
                        position : new google.maps.LatLng(latitude, longitude),
                        map : map,
                        title :'{{isset($provider)?$provider->user->name:''}}',
                        icon:'https://www.omeddr.com/public/web/img/transport.png'
                    });
                    new google.maps.Marker({
                        position : new google.maps.LatLng({{!isset($booking)?Session::get('cur_latitude'):$booking->latitude}}, {{!isset($booking)?Session::get('cur_longitude'):$booking->longitude}}),
                        map : map,
                        title :'{{auth()->user()->name}}',
                        {{--icon:'{{isset($profile) && $profile!=''?asset("uploads/patientprofile/".$profile->photo):''}}'--}}
                    });

                    // if (countryLength == pathCoordinates.length) {
                        drawPath();
                    // }
                    latitude : {{!isset($booking)?Session::get('cur_latitude'):$booking->latitude}};
                    longitude : {{!isset($booking)?Session::get('cur_longitude'):$booking->longitude}};

                }
            });
        @endfor
        }
        function drawPath() {
            new google.maps.Polyline({
                path : pathCoordinates,
                geodesic : true,
                strokeColor : '#FF0000',
                strokeOpacity : 1,
                strokeWeight : 4,
                map : map
            });
        }

        function setPicupLocation() {
            const map = new google.maps.Map(document.getElementById("map-layer"), {
                zoom: 8,
                center: { lat: {{!isset($booking)?Session::get('cur_latitude'):$booking->latitude}}, lng: {{!isset($booking)?Session::get('cur_longitude'):$booking->longitude}}},
            });
            const geocoder = new google.maps.Geocoder();
            const infowindow = new google.maps.InfoWindow();
            // document.getElementById("submit").addEventListener("click", () => {
                geocodeLatLng(geocoder, map, infowindow);
            // });
        }

        function geocodeLatLng(geocoder, map, infowindow) {
            const input = "{{!isset($booking)?Session::get('cur_latitude'):$booking->latitude}},{{!isset($booking)?Session::get('cur_longitude'):$booking->longitude}}";
            const latlngStr = input.split(",", 2);
            const latlng = {
                lat: parseFloat(latlngStr[0]),
                lng: parseFloat(latlngStr[1]),
            };
            geocoder.geocode({ location: latlng }, (results, status) => {
                if (status === "OK") {
                    if (results[0]) {
                        $('#pic-location').val(results[0].formatted_address);
                        // map.setZoom(11);
                        // const marker = new google.maps.Marker({
                        //     position: latlng,
                        //     map: map,
                        // });
                        // infowindow.setContent(results[0].formatted_address);
                        // infowindow.open(map, marker);
                    } else {
                        window.alert("No results found");
                    }
                } else {
                    window.alert("Geocoder failed due to: " + status);
                }
            });
        }
        function getDistanceFromLatLon(lat1,lon1,lat2,lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2-lat1);  // deg2rad below
            var dLon = deg2rad(lon2-lon1);
            var a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                Math.sin(dLon/2) * Math.sin(dLon/2)
            ;
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = Math.round(R * c); // Distance in km
            $('#dist').html( d + ' KM ');
        }

        function deg2rad(deg) {
            return deg * (Math.PI/180)
        }
    </script>

@endsection


