@extends('layouts.layout')
@section('content')
    <section class="main">
        <!-- ======= Hero Section ======= -->
        <section id="hero" class="hero d-flex align-items-center">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-12 col-md-8 col-lg-8 d-flex flex-column justify-content-center">

                    </div>
                    <div class="col-12 col-md-4 col-lg-4 hero-img"  >
                        <img src="{{asset('web/v2/img/logo_patient/find-best.png')}}" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
            <div class="search-box">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h1 class="search-box-heading"> <span id="changethewords">
                                Find the best
                                <span data-id="1">Doctors</span>
                                <span data-id="2">Hospitals</span>
                                <span data-id="3">Diagnostics</span>
                                <span data-id="4">Ambulances</span>
                                <span data-id="5">Pharmacy</span>
                            <br>
                                & Other Healthcare services near you</span></h1>
                        </div>
                        <div class="col-12">
                            <form action="{{url('/provider/search/')}}" method="GET">
                                <div class="search-input-box">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-12 col-md-4 col-lg-4 ">
                                                <div class="input-group search-box-group py-2">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">
                                                            <i class="bi bi-search"></i></span>
                                                    </div>
                                                    <select class="form-control" id="search_for" name="search_for" required>
                                                        @if(isset($categories) && $categories->isNotEmpty())
                                                           @foreach($categories as $cat)
                                                           <option value="{{$cat->id}}">{{$cat->id!==9?$cat->name:"Other ".$cat->name}}</option>
                                                           @endforeach

                                                         @endif
                                                    </select>
{{--                                                    <input type="text" class="form-control"--}}
{{--                                                           placeholder="Doctor,Hospital,Diagnostic,Medicine, ambulance, Other Health Services"--}}
{{--                                                           aria-label="Name"--}}
{{--                                                           aria-describedby="basic-addon1">--}}
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-3 col-lg-3 bl">
                                                <div class="input-group search-box-group py-2">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1"><i
                                                                class="bi bi-person"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="Name, Specialty, Symptoms                                                    "
                                                           aria-label="Name" name="search_field" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-3 col-lg-3 bl">
                                                <div class="input-group search-box-group py-2">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1"><i
                                                                class="bi bi-map"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="State, City, Area"
                                                           aria-label="Name" name="search_loc" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-2 col-lg-2 px-0 search-box-button">
                                                <button type="submit" class="btn btn-read-more">Search</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- End Hero -->

        <!-- ======= Top Specility Section ======= -->
        <section class="my-4">
            <header class="section-header">
                <h2>Top Doctors & Services Providers</h2>
            </header>
            <div class="container">
                <div class="row justify-content-center">
                    @if(isset($categories))
                        @foreach($categories as $category)
                            <div class="col">
                                <a href="{{url('/provider/search?search_for='.$category->id)}}">
                                    <div class="icon-container">
                                        <img src="{{asset("web/v2/img/icons/$category->image")}}">
                                        <a href="{{url('/provider/search?search_for='.$category->id)}}">
                                            {{$category->prefix ?? ""}} {{$category->name}}</a>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </section>
        <!-- End Top Specility Section -->

        <!-- ======= About Section ======= -->
        <section class="features" id="live-consultation">
            <img src="https://vivodoc.com/assets/images/landing_page/orange-oval.png" class="orange-oval">
            {{-- <header class="section-header">
                <h2></h2>
            </header> --}}
            <div class="container">
                <div class="row gx-0">
                    <div class="col-lg-6 d-flex flex-column justify-content-center order-2 order-md-1"
                    >
                        <div class="mt-2 mt-md-0 text-center text-md-start">
                            <h2 class="section-heading">Live Consultation</h2>
                            <p>
                                VivoDoc is moving healthcare into the modern era with virtual Doctor's Consultation and easy online booking.
Certified and Top rated Doctors are available online to address immediate medical issues or healthcare needs via
a video call and Chat.</p>
                            <ul class="list-process">
                                <li><img src="{{asset('web/v2/img/orange-check.svg')}}" width="20rem" /> Access healthcare you need from anywhere</li>
                                <li><img src="{{asset('web/v2/img/orange-check.svg')}}" width="20rem" /> Pay direct at discount prices or use your coins</li>
                                <li><img src="{{asset('web/v2/img/orange-check.svg')}}" width="20rem" /> Instantly connect with the best doctors or specialists near you</li>
                            </ul>
                            <div class="text-center text-lg-start py-4">
                                <a href="#"
                                   class="btn btn-read-more">
                                    <span>Consult a Doctor Now</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 d-flex align-items-center justify-content-center order-1 order-md-2"
                    >
                        <img src="{{asset('web/v2/img/live.png')}}" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!-- End About Section -->
        <!-- ======= Payment Section ======= -->
        <section class="features" id="appointment">
            <div class="container">
                <div class="row gx-0">
                    <div class="col-lg-6 d-flex align-items-center justify-content-center"
                    >
                        <img src="{{asset('web/v2/img/appointment.png')}}" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-6 d-flex flex-column justify-content-center"
                    >
                        <div class="content mt-2 mt-md-0 text-center text-md-start">
                            <h2 class="section-heading">In-Person Consultation (Appointment)</h2>
                            <p>When you do need an in-person visit, book appointments online and access the nearby top rated doctors or same
great doctor you consulted via video. VivoDoc also connects you with Hospitals, Diagnostics and Pharmacies
via Call/Chat. You can Fix an Appointment or upload your prescription for sample collection at Home or
Medicine Delivery at Doorstep.</p>
                            <ul class="list-process">
                                <li><img src="{{asset('web/v2/img/orange-check.svg')}}" width="20rem" /> Chat or upload your prescription for Services at Home </li>
                                <li><img src="{{asset('web/v2/img/orange-check.svg')}}" width="20rem" /> Check-in online for the appointment of Services providers</li>
                                <li><img src="{{asset('web/v2/img/orange-check.svg')}}" width="20rem" /> Get follow up reminders when it's time for your appointment</li>
                            </ul>
                            <div class="text-center text-lg-start py-4">
                                <a href="#"
                                   class="btn btn-read-more">
                                    <span>Book an appointment</span>
                                </a>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </section>
        <!-- End Payment Section -->
        <!-- ======= Ambulance ======= -->
        <section class="features" id="ambulance">
            <div class="container">
                <div class="row gx-0">
                    <div class="col-lg-6 d-flex flex-column justify-content-center order-2 order-md-1"
                    >
                        <div class="mt-2 mt-md-0 text-center text-md-start">
                            <h2 class="section-heading">Ambulance</h2>
                            <p>
                                VivoDoc is moving healthcare into the modern era with Certified and Top rated Doctors, Hospitals, Diagnostic
Centres, Medical stores and Ambulances are available to address immediate medical issues. On VivoDoc
Ambulance can be available immediately in easy steps, GPS based Ambulances can be booked just like a Cab.</p>
                            <ul class="list-process">
                                <li><img src="{{asset('web/v2/img/orange-check.svg')}}" width="20rem" /> Access healthcare you need from anywhere</li>
                                <li><img src="{{asset('web/v2/img/orange-check.svg')}}" width="20rem" /> Instantly connects with the nearest Ambulance</li>
                                <li><img src="{{asset('web/v2/img/orange-check.svg')}}" width="20rem" /> Just pay the service fee and get the Ambulance</li>
                            </ul>
                            <div class="text-center text-lg-start py-4">
                                <a href="#"
                                   class="btn btn-read-more">
                                    <span>Book an ambulance</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 d-flex align-items-center justify-content-center order-1 order-md-2"
                    >
                        <img src="{{asset('web/v2/img/bikedr.png')}}" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!-- End About Section -->
        <!-- Top Doctors And Services -->
        <section id="services" class="pt-4">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center ">
                        <header class="section-header pt-4">
                            <h2>Top Most Doctors &amp; Services</h2>
                        </header>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <ul class="nav nav-pills nav-justified text-center mb-1" id="pills-tab" role="tablist">
                            @if(isset($categories))
                                @php $x=1 @endphp
                                @foreach($categories as $category)
                                    <li class="nav-item">
                                        <a class="nav-link @if($x==1){{'active'}}@endif" id="pills-tab{{$category->id}}"
                                           data-toggle="pill"
                                           href="#pills{{$category->id}}" role="tab" aria-controls="pills{{$category->id}}" aria-selected="true">{{$category->name}}</a>
                                    </li>
                                    @php $x++ @endphp
                                @endforeach
                            @endif
                        </ul>
                        <div class="tab-content pt-4" id="pills-tabContent">
                            @if(isset($categories))
                                                        @php $n=1 @endphp
                                                        @foreach($categories as $category)
                                                            <div class="tab-pane fade @if($n==1){{'show active'}}@endif" id="pills{{$category->id}}"
                                                                 role="tabpanel" aria-labelledby="pills-tab{{$category->id}}">
                                                                @php $provider=\App\Models\ServiceProfile::where('user_role_id',$category->id)->where('mark_top',1)->limit(6)->orderby('id','desc')->get() @endphp

                                                                {{-- <div class="swipper" id="swiper-{{$category->name!="other health services"?$category->name:"ohs"}}"> --}}
                                                                    <div class="owl-carousel owl-theme owl-top-sevices">
                                                                    @foreach($provider as $sp)
                                                                    <div class="item">
                                                                        <div class="card h-90">
                                                                            <div class="card-body">
                                                                                <div class="row provider-info">
                                                                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 h-mob">
                                                                                        <div class="provider-img">
                                                                                            <div class="provider-img-box">
                                                                                                <img
                                                                                                    src="{{$sp->user->profile->photo!=''?asset('uploads/profile/'.$sp->user->profile->photo):asset('web/img/doctor-lg.png')}}"
                                                                                                    class="rounded-circle profile-image" alt="{{$sp->user->name}}" width="100px" height="100px">
                                                                                                @if($sp->mark_live == 1)
                                                                                                    @if($sp['btn_video']==1)
                                                                                                        <div class="rounded-circle profile-img-button bg-success">
                                                                                                            <a href="{{url('detail/'.$sp['id'].'?req_type=1')}}"
                                                                                                               class="provider-active-button"><i class="fa-solid fa-video"></i></a>
                                                                                                        </div>
                                                                                                    @else
                                                                                                        <div class="rounded-circle profile-img-button bg-success" disabled>
                                                                                                            <a href="#" class="provider-active-button">
                                                                                                                <i class="fa-solid fa-video"></i></a>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                @else
                                                                                                    @if($sp['btn_booknow']==1)
                                                                                                        <div class="rounded-circle profile-img-button bg-danger">
                                                                                                            <a href="{{url('detail/'.$sp['id'].'?req_type=2')}}"
                                                                                                               class="provider-active-button">
                                                                                                                <i class="fa-solid fa-calendar"></i>
                                                                                                            </a>
                                                                                                        </div>
                                                                                                    @else
                                                                                                        <div class="rounded-circle profile-img-button bg-danger" disabled>
                                                                                                            <a href="#" class="provider-active-button"><i class="fa-solid fa-calendar"></i></a>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                @endif
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                                                                                        <div class="row">
                                                                                            <div class="provider-name">
                                                                                                <h5>{{$sp->user->name}}</h5>
                                                                                                <p>{{$sp->user->profile->qualification!=''?($sp->user->profile->qualification):''}}</p>
                                                                                                <p>{{$sp->user->profile->speciality}}</p>
                                                                                                <p>{{$sp['city']}}{{$sp['state']!=''?', '.$sp['state']:''}}</p>
                                                                                                <!-- <div class="provider-rating"><i class="bi bi-star-fill"></i><span>2.5</span></div> -->
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-6">
                                                                                                <div
                                                                                                    class="text-dark mb-1 font-sm control-label d-block text-truncate">
                                                                                                    <i class="fa fa-star text-warning"></i>
                                                                                                    <i class="fa fa-star-half-o text-warning"></i>
                                                                                                    <i class="fa fa-star-o text-warning"></i>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-6 mb-1 text-muted text-truncate service-info-text"><b>Exp : {{$sp->user->profile->experiance}}
                                                                                                Year(s)</b></div>
                                                                                        </div>

                                                                                        <div class="row small text-black-50 no-gutters">
                                                                                            <div class="col-6 mb-1 text-success service-info-text"> <b> <i class="bi bi-camera-video"></i>&nbsp; Fee: &#x20B9; {{$sp['min_fee']}} </b></div>
                                                                                            <div class="col-6 mb-1 text-danger service-info-text">
                                                                                                <b> <i class="bi bi-calendar2-check"></i>&nbsp;Fee: &#x20B9;
                                                                                                    {{$sp['max_fee']}}
                                                                                                </b>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                                <hr/>
                                                                                <div class="row">
                                                                                    <div class="col-12 col-md-4 text-center link-margin">
                                                                                        <a class="view-link" href="{{url('detail/'.$sp['id'])}}"><i class="bi bi-person"></i>&nbsp; View Profile</a>
                                                                                    </div>
                                                                                    <div class="col-12 col-md-4 text-center link-margin">
                                                                                        <a class="view-link" href="tel://8299096863" disabled><i class="bi bi-telephone"></i>&nbsp; Call Now</a>
                                                                                    </div>
                                                                                    <div class="col-12 col-md-4 text-center link-margin">
                                                                                        <a class="view-link" href="./profile.html"><i class="bi bi-chat"></i>&nbsp; Enquiry</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    @endforeach
                                                                    {{-- <div class="swiper-pagination mt-2"></div> --}}
                                                                </div>
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <a href="{{url('service/'.$category->id.'/'.strtolower($category->name))}}" type="button"
                                               class="btn btn-outline-dark rounded-pill shadow-none waves-effect waves-light">Show
                                                more</a>
                                        </div>
                                    </div>
                                                     </div>
                                                            @php $n++ @endphp
                                @endforeach
                                @endif

                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- Top Doctors And Services End -->

        <!-- ======= Testimonials Section ======= -->
        <section id="testimonials" class="testimonials">

            <div class="container">

                <header class="section-header pt-4">
                    <h2>Testimonials</h2>
                </header>

                <div class="testimonials-slider swiper-container" >
                    <div class="owl-carousel owl-theme owl-testimonials">
                        @if(isset($testimonial))
                            @foreach($testimonial as $tes)
                        <div class="item">
                            <div class="testimonial-item">
                                <div class="stars">
                                    <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i
                                        class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i
                                        class="bi bi-star-fill"></i>
                                </div>
                                <p>
                                    {{$tes->description}}
                                </p>
                                <div class="profile mt-auto">
                                    <img src="{{asset("uploads/testimonial/".$tes->file)}}" class="testimonial-img"
                                         alt="">
                                    <h3>{{$tes->title}}</h3>
{{--                                    <h4>Ceo &amp; Founder</h4>--}}
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif

                    </div>
                    <div class="swiper-pagination"></div>
                </div>

            </div>

        </section>
        <!-- End Testimonials Section -->
        <div class="records py-5 px-2">
            <div class="row justify-content-center">
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 text-center text-white">
                    <div class="circle">
                        <i class="bi bi-people h3"></i>
                    </div>
                    <div class="lead">Service Providers</div>
                    <h2> {{$t_sp ?? 0}}</h2>
                </div>

                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 text-center text-white">
                    <div class="circle ">
                        <i class="bi bi-building h3"></i>
                    </div>
                    <div class="lead">OMED Centers</div>
                    <h2> {{$t_omed_center ?? 0}}</h2>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 text-center text-white">
                    <div class="circle ">
                        <i class="bi bi-emoji-smile h3"></i>
                    </div>
                    <div class="lead">Patients</div>
                    <h2> {{$t_patient ?? 0}}</h2>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 text-center text-white">
                    <div class="circle">
                        <i class="bi bi-globe h3"></i>
                    </div>
                    <div class="lead">Cities</div>
                    <h2> {{$t_city ?? 0}}</h2>
                </div>
            </div>
        </div>
        <div class="container-fluid bg-blue cp-md">
            <div class="container">
                <div class="row">
                    <header class="section-header mt-5">
                        <h2>Our Partners</h2>
                    </header>
                </div>
                <div class="row mb-5">
                    <div class="col-12">
                        <div class="clients-slider swiper-container">
                            <div class="owl-carousel owl-theme owl-partners">
                                @if(isset($partners))
                                                        @foreach($partners as $p)
                                <div class="item">
                                    <div class="h-100" style="min-width:100px; height:100px;">
                                        <div class="card-body p-0 d-flex justify-content-center">
                                            <img src="{{asset("uploads/partner/".$p->file)}}"
                                                 class="img-fluid" style="height:90px;">
                                        </div>
                                    </div>
                                </div>

                                    @endforeach
                                    @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ======= How its works ======= -->
        <div class="how-it-works">
            <img src="https://vivodoc.com/assets/images/landing_page/violet-oval.png" class="violet-oval">
            <div class="container">
                <div class="row">
                    <div class="col-12 container py-4">
                        <div class="row">
                            <div class="col-md-4">
                                <header class="section-header">
                                    <h2>How does it work for patients?</h2>
                                </header>
                            </div>
                            <div class="col-md-8 content">
                                <p>Regardless of your location, VivoDoc is the fastest, easiest way to access healthcare for everyone. Need
                                    to see a specialist, but the closest one is several hours away, booked for the next weeks, or need to go
                                    for just getting an Appointment. You’re not alone—many patients live in underserved areas that lack or
                                    have limited access to some specialties and other healthcare services. But with VivoDoc, you can
                                    schedule or instantly connect to a Doctor on Video call, no matter how far away they are. You can Fix
                                    an Appointment with Doctors, Hospitals, Pharmacies and Diagnostics or upload your prescription for
                                    sample collection at Home or Medicine Delivery at Doorstep. GPS based Ambulances can be available
                                    immediately in easy steps, It can be booked just like a Cab.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-12 p-0">
                        <div class="image-section">
                            <img src="{{asset('web/v2/img/how-it-works-02.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ======= How its works End ======= -->
        <!-- Download app Intro -->
        <section class="download-app">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4 col-lg-4">
                        <h1 class="section-title">Our easy to use responsive app for providers</h1>
                        <div class="btn-container">
                            <a href="#" class="btn btn-outline-primary">
                                <img src="{{asset('web/v2/img/icons/apple.svg')}}" alt="">
                                <span>Get on the App Store</span>
                            </a>
                            <a href="#" class="btn btn-outline-primary">
                                <img src="{{asset('web/v2/img/icons/google.svg')}}" alt="">
                                <span>Get it on Google Play</span>
                            </a>
                        </div>
                        <img src="{{asset('web/v2/img/logo_patient/cta-girl.svg')}}">
                    </div>
                    <div class="col-12 col-md-8 col-lg-8">
                        <img src="{{asset('web/v2/img/logo_patient/cta-phone-2.png')}}">
                    </div>
                </div>
            </div>
        </section>
        <!-- Download app intro end -->
    </section>
    {{--    <div class="container-fluid">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col-12">--}}
    {{--                    <div id="clients" class="owl-carousel owl-theme">--}}
    {{--                                <div class='item'>--}}
    {{--                                    <div class='card h-100' style="height:250px; width:320px;">--}}
    {{--                                        <div class='card-body p-0' >--}}

    {{--                                            <div class="view zoom">--}}
    {{--                                                <a href={{url('service/9/amd-(free)/100?live_status=1')}}>--}}
    {{--                                            <img src='{{asset("web/img/banner/b1.jpeg")}}' class="img-fluid" style="height:250px; width:320px;"/>--}}
    {{--                                                </a>--}}
    {{--                                            </div>--}}

    {{--                                        </div>--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                        <div class='item'>--}}
    {{--                            <div class='card h-100' style="height:250px; width:320px;">--}}
    {{--                                <div class='card-body p-0'>--}}
    {{--                                    <div class="view zoom">--}}
    {{--                                        <a href="{{url('service/6/aah-mobile-dr./130')}}">--}}
    {{--                                    <img src='{{asset("web/img/banner/b2.jpeg")}}' class="img-fluid" style="height:250px; width:320px;"/>--}}
    {{--                                        </a>--}}
    {{--                                </div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class='item'>--}}
    {{--                            <div class='card h-100' style="height:250px; width:320px;">--}}
    {{--                                <div class='card-body p-0'>--}}
    {{--                                    <div class="view zoom">--}}
    {{--                                        <a href={{url('register/provider')}}>--}}
    {{--                                    <img src='{{asset("web/img/banner/b4.jpeg")}}' class="img-fluid" style="height:250px; width:320px;"/>--}}
    {{--                                        </a>--}}
    {{--                                </div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class='item'>--}}
    {{--                            <div class='card h-100' style="height:250px; width:320px;">--}}
    {{--                                <div class='card-body p-0'>--}}
    {{--                                    <div class="view zoom">--}}
    {{--                                        <a href="{{url('detail/40')}}">--}}
    {{--                                    <img src='{{asset("web/img/banner/b5.jpeg")}}' class="img-fluid" style="height:250px; width:320px;"/>--}}
    {{--                                        </a>--}}
    {{--                                </div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class='item'>--}}
    {{--                            <div class='card h-100'>--}}
    {{--                                <div class='card-body p-0'>--}}
    {{--                                    <div class="view zoom">--}}
    {{--                                    <img src='{{asset("web/img/banner/b8.jpeg")}}' class="img-fluid" style="height:250px; width:300px;"/>--}}
    {{--                                </div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class='item '>--}}
    {{--                            <div class='card h-100' style="height:250px; width:320px;">--}}
    {{--                                <div class='card-body p-0'>--}}
    {{--                                    <div class="view zoom">--}}
    {{--                                        <a href="{{url('contactus')}}">--}}
    {{--                                        <img src='{{asset("web/img/banner/b10.jpeg")}}' class="img-fluid" style="height:250px; width:320px;"/>--}}
    {{--                                        </a>--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class='item'>--}}
    {{--                            <div class='card h-100' style="height:250px; width:320px;">--}}
    {{--                                <div class='card-body p-0'>--}}
    {{--                                    <div class="view zoom">--}}
    {{--                                        <a href="{{url('detail/828')}}">--}}
    {{--                                    <img src='{{asset("web/img/banner/b9.jpeg")}}' class="img-fluid" style="height:250px; width:320px;"/>--}}
    {{--                                        </a>--}}
    {{--                                </div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}

    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    {{--    <div class="container-fluid bg-blue cp-md">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col-12 text-center">--}}
    {{--                    <div class="h2-responsive mb-2">One Platform To Get All Types Of Health Services</div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="row">--}}
    {{--                @if(isset($categories))--}}
    {{--                    @foreach($categories as $category)--}}
    {{--                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 mb-2 ">--}}
    {{--                            <a href="{{url('category/'.strtolower($category->name).'/'.$category->id)}}" class="text-dark">--}}
    {{--                                <div class="card h-100">--}}
    {{--                                    <div class="card-body text-center">--}}
    {{--                                        <div class="mb-1"><img src="{{asset("web/img/$category->image")}}" class="img-fluid">--}}
    {{--                                        </div>--}}
    {{--                                        <div class="mb-1 font-lg">@if($category->prefix!=''){{ucwords($category->prefix)}}<br>--}}
    {{--                                            @endif {{ucwords--}}
    {{--							($category->name)--}}
    {{--							}}</div>--}}
    {{--                                        <div>--}}
    {{--                            <span class="rounded-pill bg-dark text-white pl-3 pr-3">--}}
    {{--                                {{\App\Models\ServiceProfile::where('user_role_id',$category->id)->count()}}--}}
    {{--                            </span>--}}
    {{--                                        </div>--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                            </a>--}}
    {{--                        </div>--}}
    {{--                    @endforeach--}}
    {{--                @endif--}}

    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--    <div class="container-fluid cp-md">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col-md-4">--}}
    {{--                    <div class="card h-100">--}}
    {{--                    <div class="card-body p-0">--}}
    {{--                        <div class="view zoom">--}}
    {{--                            <a href="{{url('service/1/doctors?live_status=1')}}">--}}
    {{--                        <img src="{{asset('web/img/banner/b3.jpeg')}}" class="img-fluid">--}}
    {{--                            </a>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-md-4">--}}
    {{--                    <div class="card h-100">--}}
    {{--                        <div class="card-body p-0">--}}
    {{--                             <div class="view zoom">--}}
    {{--                            <img src="{{asset('web/img/banner/b6.jpeg')}}" class="img-fluid">--}}
    {{--                        </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-md-4">--}}
    {{--                    <div class="card h-100">--}}
    {{--                        <div class="card-body p-0">--}}
    {{--                             <div class="view zoom">--}}
    {{--                            <img src="{{asset('web/img/banner/b7.jpeg')}}" class="img-fluid">--}}
    {{--                             </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    {{--    <div class="container-fluid bg-blue p-0">--}}
    {{--        <div class="row no-gutters">--}}
    {{--            <div class="col-md-4 col-sm-12 col-lg-4">--}}
    {{--                <div class="card rounded-0" style="height:300px;background: #78069C;--}}
    {{--background: -webkit-linear-gradient(to right, #E100FF, #78069C);--}}
    {{--background: linear-gradient(to right, #E100FF, #78069C);">--}}
    {{--                    <div class="card-body">--}}
    {{--                        <br>--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-12 text-center">--}}
    {{--                                <div class="h2-responsive text-white">Download OMEDDr App</div>--}}
    {{--                       <div class="mb-2 text-white">One app for all your health needs.</div>--}}
    {{--                            </div>--}}
    {{--                        </div><br>--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-12 text-center">--}}

    {{--                                <a href="https://play.google.com/store/apps/details?id=com.omed.dr" target="_blank">--}}
    {{--                                    <img src="{{asset("web/img/google-plus.png")}}" class="img-fluid" width="180"></a>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}

    {{--                        --}}{{--                <a href="#"><img src="img/google-plus.png" width="180"></a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="col-md-8 col-sm-12 col-lg-8">--}}
    {{--                <div class="card rounded-0 " style="height: 300px">--}}
    {{--                    <div class="card-body p-0">--}}
    {{--                        <div id="slider" class="owl-carousel owl-theme" style="height:300px;">--}}
    {{--                                                            @if(isset($banners))--}}
    {{--                                                                @foreach($banners as $ban)--}}
    {{--                                                                    <div class="item">--}}
    {{--                                                                        <a href="{{$ban->url!=''?$ban->url:''}}">--}}
    {{--                                                                            <img src="{{asset('uploads/banner/'.$ban->file)}}" alt="{{$ban->title}}" style="height:300px;">--}}
    {{--                                                                        </a>--}}
    {{--                                                                    </div>--}}
    {{--                                                                @endforeach--}}
    {{--                                                            @endif--}}
    {{--                                                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}

    {{--    </div>--}}
    {{--    <div class="container-fluid cp-md">--}}
    {{--        <div class="row">--}}
    {{--            <div class="col-4 text-center">--}}
    {{--                <div class="h2-responsive">Download App</div>--}}
    {{--                <div class="mb-5">Doctor's online consultation and all other health services.</div>--}}

    {{--        </div>--}}
    {{--        <div class="row">--}}
    {{--            <div class="text-center">--}}
    {{--                <a href="https://play.google.com/store/apps/details?id=com.omed.dr" target="_blank">--}}
    {{--                    <img src="{{asset("web/img/google-plus.png")}}" class="img-fluid" width="180"></a>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--            <div class="col-8">--}}
    {{--                  <div id="slider" class="owl-carousel owl-theme">--}}
    {{--                                @if(isset($banners))--}}
    {{--                                    @foreach($banners as $ban)--}}
    {{--                                        <div class="item">--}}
    {{--                                            <a href="{{$ban->url!=''?$ban->url:''}}">--}}
    {{--                                                <img src="{{asset('uploads/banner/'.$ban->file)}}" alt="{{$ban->title}}">--}}
    {{--                                            </a>--}}
    {{--                                        </div>--}}
    {{--                                    @endforeach--}}
    {{--                                @endif--}}
    {{--                            </div>--}}

    {{--            </div>--}}
    {{--    </div>--}}
    {{--        </div>--}}
    {{--    <div style="height: 350px;" >--}}
    {{--        <div class="row no-gutters">--}}
    {{--            <div class="col-md-12 col-lg-12 col-sm-12">--}}
    {{--                <a href="https://play.google.com/store/apps/details?id=com.omed.dr" target="_blank">--}}
    {{--                    <img src="{{asset("web/img/user.jpeg")}}" class="img-fluid" style="height:350px; width: 100%;"></a>--}}
    {{--            </div>--}}
    {{--            <div class="col-md-6 col-lg-6 col-sm-12">--}}
    {{--                <a href="https://play.google.com/store/apps/details?id=com.omed.biz" target="_blank">--}}
    {{--                    <img src="{{asset("web/img/biz.jpeg")}}" class="img-fluid"></a>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    {{--    <div class="container-fluid bg-light cp-md" >--}}
    {{--        <div class="container">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col-12 text-center ">--}}
    {{--                    <div class="h2-responsive mb-5">Top Most Doctors & Services</div>--}}
    {{--                    --}}{{--				<div class="mb-5">Vivamus suscipit quis sem pellentesque tincidunt Etiam luctus elementum eros ac laoreet.</div>--}}
    {{--                </div>--}}
    {{--            </div>--}}

    {{--            <div class="row">--}}
    {{--                <div class="col-12">--}}
    {{--                    <ul class="nav nav-pills mb-1" id="pills-tab" role="tablist">--}}
    {{--                        @if(isset($categories))--}}
    {{--                            @php $x=1 @endphp--}}
    {{--                            @foreach($categories as $category)--}}
    {{--                                <li class="nav-item">--}}
    {{--                                    <a class="nav-link @if($x==1){{'active'}}@endif" id="pills-tab{{$category->id}}"--}}
    {{--                                       data-toggle="pill"--}}
    {{--                                       href="#pills{{$category->id}}" role="tab" aria-controls="pills{{$category->id}}" aria-selected="true">{{$category->name}}</a>--}}
    {{--                                </li>--}}
    {{--                                @php $x++ @endphp--}}
    {{--                            @endforeach--}}
    {{--                        @endif--}}
    {{--                    </ul>--}}
    {{--                    <div class="tab-content bg-light" id="pills-tabContent" >--}}
    {{--                        @if(isset($categories))--}}
    {{--                            @php $n=1 @endphp--}}
    {{--                            @foreach($categories as $category)--}}
    {{--                                <div class="tab-pane fade @if($n==1){{'show active'}}@endif" id="pills{{$category->id}}"--}}
    {{--                                     role="tabpanel" aria-labelledby="pills-tab{{$category->id}}">--}}
    {{--                                    @php $provider=\App\Models\ServiceProfile::where('user_role_id',$category->id)->where('mark_top',1)->limit(6)->orderby('id','desc')->get() @endphp--}}

    {{--                                    <div class="row">--}}
    {{--                                        @foreach($provider as $sp)--}}
    {{--                                            <div class='col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4'>--}}
    {{--                                                <div class='card h-100'>--}}
    {{--                                                    <div class='card-body'>--}}
    {{--                                                        <div class="row ">--}}
    {{--                                                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 h-mob">--}}
    {{--                                                                <div class='view'>--}}
    {{--                                                                    <a href='{{url('detail/'.$sp['id'])}}'>--}}
    {{--                                                                        @if($sp->user->profile->photo!='')--}}
    {{--                                                                            <img src="{{asset('uploads/profile/'.$sp->user->profile->photo)}}" class="img-fluid" style="border-radius: 20px;">--}}
    {{--                                                                        @else--}}
    {{--                                                                            <img src="{{asset('web/img/doctor-lg.png')}}" class="img-fluid" style="border-radius: 30px;">--}}
    {{--                                                                        @endif--}}
    {{--                                                                    </a>--}}
    {{--                                                                </div>--}}
    {{--                                                            </div>--}}
    {{--                                                            <div class='col-xl-9 col-lg-9 col-md-9 col-sm-12'>--}}
    {{--                                                                <div class="row">--}}
    {{--                                                                    <div class="col-sm-3 col-3 d-mob">--}}
    {{--                                                                        <div class='view'>--}}
    {{--                                                                            <a href='{{url('detail/'.$sp['id'])}}'>--}}
    {{--                                                                                @if($sp->user->profile->photo!='')--}}
    {{--                                                                                    <img src="{{asset('uploads/profile/'.$sp->user->profile->photo)}}" class="img-fluid" style="border-radius: 20px;">--}}
    {{--                                                                                @else--}}
    {{--                                                                                    <img src="{{asset('web/img/doctor-lg.png')}}" class="img-fluid" style="border-radius: 30px;">--}}
    {{--                                                                                @endif--}}
    {{--                                                                            </a>--}}
    {{--                                                                        </div>--}}
    {{--                                                                    </div>--}}
    {{--                                                                    <div class="col-md-12 col-lg-12 col-sm-9 col-9">--}}
    {{--                                                                        <a href="{{url('detail/'.$sp['id'])}}" class='mb-1 d-block h5-responsive text-truncate' style="color:#27166D;">{{$sp->user->name}} ({{$sp->user->profile->speciality}})</a>--}}
    {{--                                                                        <div class="mb-1 font-sm d-block text-truncate">--}}
    {{--                                                                            {{$sp->user->profile->qualification}},&nbsp;{{$sp['city'].', '. $sp['state']}}</div>--}}
    {{--                                                                    </div>--}}
    {{--                                                                </div>--}}


    {{--                                                                --}}{{--                                                    <a href="{{url('detail/'.$sp['id'])}}" class='mb-1 d-block h5-responsive text-truncate' style="color:#27166D;">{{$sp->user->name}} ({{$sp->user->profile->speciality}})</a>--}}
    {{--                                                                --}}{{--                                                    <div class="mb-1 font-sm d-block text-truncate">--}}
    {{--                                                                --}}{{--                                                        {{$sp->user->profile->qualification}},&nbsp;{{$sp['city'].', '. $sp['state']}}</div>--}}
    {{--                                                                <div class='row'>--}}
    {{--                                                                    <div class="col-4">--}}
    {{--                                                                        <div class="text-dark mb-1 font-sm control-label d-block text-truncate">--}}
    {{--                                                                            <i class="fa fa-star text-warning"></i>--}}
    {{--                                                                            <i class="fa fa-star-half-o text-warning"></i>--}}
    {{--                                                                            <i class="fa fa-star-o text-warning"></i>--}}
    {{--                                                                        </div>--}}
    {{--                                                                    </div>--}}
    {{--                                                                    <div class='col-8 text-right'>--}}
    {{--                                                                        @if($sp->mark_live == 1)--}}
    {{--                                                                            @if($sp['btn_video']==1)--}}
    {{--                                                                                <a href='{{url('patient/consult-now/'.$sp['id'])}}' class='text-dark'><i class='fa fa-video text-success'></i> Consult Now</a>--}}
    {{--                                                                            @else--}}
    {{--                                                                                <i class="fa fa-circle text-success" aria-hidden="true"></i>--}}
    {{--                                                                            @endif--}}
    {{--                                                                        @else--}}
    {{--                                                                            @if($sp['btn_booknow']==1)--}}
    {{--                                                                                <a href='{{url('book-appoinment/'.$sp['user_id'])}}' class='text-dark'><i class="fa fa-pencil-square-o text-danger" ></i> Get Appointment</a>--}}
    {{--                                                                            @else--}}
    {{--                                                                                <i class="fa fa-circle text-danger" aria-hidden="true"></i>--}}
    {{--                                                                            @endif--}}
    {{--                                                                        @endif--}}

    {{--                                                                    </div>--}}
    {{--                                                                </div>--}}

    {{--                                                                <div class='row small text-black-50 no-gutters'>--}}
    {{--                                                                    <div class="col-4 mb-1 text-muted text-truncate"><b>Exp : {{$sp->user->profile->experiance}} Year(s)</b></div>--}}
    {{--                                                                    <div class='col-4 mb-1 text-success '> <b>@if($sp['user_role_id']==1)Online: @else Minimum: @endif <i class="fas fa-rupee-sign"></i> {{$sp['min_fee']}} </b></div>--}}
    {{--                                                                    <div class='col-4 mb-1 text-danger '>--}}
    {{--                                                                        <b>@if($sp['user_role_id']==1)Offline: @else Maximum: @endif--}}
    {{--                                                                            <i class="fas fa-rupee-sign"></i>--}}
    {{--                                                                            @if(isset($config))--}}
    {{--                                                                               {{($sp['max_fee'] - (($sp['max_fee']*$config['appointment_discount'])/100))}} <del class="text-muted">{{$sp['max_fee']}}</del>--}}
    {{--                                                                            @else--}}
    {{--                                                                                {{$sp['max_fee']}}--}}
    {{--                                                                                @endif--}}
    {{--                                                                        </b>--}}
    {{--                                                                    </div>--}}
    {{--                                                                </div>--}}
    {{--                                                            </div>--}}
    {{--                                                        </div>--}}
    {{--                                                    </div>--}}
    {{--                                                </div>--}}
    {{--                                            </div>--}}
    {{--                                        @endforeach--}}
    {{--                                    </div>--}}
    {{--                                    <div class="row">--}}
    {{--                                        <div class="col-12 text-center">--}}
    {{--                                            <a href="{{url('category/'.strtolower($category->name).'/'.$category->id)}}"--}}
    {{--                                            <a href="{{url('service/'.$category->id.'/'.strtolower($category->name))}}"--}}
    {{--                                               type="button" class="btn btn-outline-dark rounded-pill shadow-none">Show more</a>--}}
    {{--                                        </div>--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                                @php $n++  @endphp--}}
    {{--                            @endforeach--}}
    {{--                        @endif--}}
    {{--                    </div>--}}

    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--    <div class="records cp-md ">--}}
    {{--        <div class="row justify-content-center">--}}
    {{--            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 text-center text-white">--}}
    {{--                <div class="circle">--}}
    {{--                    <i class="fas fa-users fa-2x"></i>--}}
    {{--                </div>--}}
    {{--                <div class="lead">Service Providers</div>--}}
    {{--                <div class="h2-responsive">@if(isset($t_sp)) {{$t_sp}}@endif</div>--}}
    {{--            </div>--}}

    {{--            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 text-center text-white">--}}
    {{--                <div class="circle ">--}}
    {{--                    <i class="fas fa-door-open fa-2x"></i>--}}
    {{--                </div>--}}
    {{--                <div class="lead">OMED Centers</div>--}}
    {{--                <div class="h2-responsive">@if(isset($t_omed_center)) {{$t_omed_center}}@endif</div>--}}
    {{--            </div>--}}
    {{--            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 text-center text-white">--}}
    {{--                <div class="circle ">--}}
    {{--                    <i class="fa fa-smile fa-lg text-white fa-2x"></i>--}}
    {{--                </div>--}}
    {{--                <div class="lead">Patients</div>--}}
    {{--                <div class="h2-responsive">@if(isset($t_patient)) {{$t_patient}}@endif</div>--}}
    {{--            </div>--}}
    {{--            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 text-center text-white">--}}
    {{--                <div class="circle">--}}
    {{--                    <i class="fa fa-globe fa-lg text-white fa-2x"></i>--}}
    {{--                </div>--}}
    {{--                <div class="lead">Cities</div>--}}
    {{--                <div class="h2-responsive">@if(isset($t_city)) {{$t_city}}@endif</div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--<div class="container-fluid cp-md" >--}}
    {{--<div class="container">--}}
    {{--	<div class="row">--}}
    {{--		<div class="col-12 text-center">--}}
    {{--			<div class="h2-responsive ">New Registered</div>--}}
    {{--		</div>--}}
    {{--	</div>--}}
    {{--	<div class="row">--}}
    {{--		<div class="col-12">--}}
    {{--			<div id="new-registered" class="owl-carousel owl-theme">--}}

    {{--                @if(isset($new_registration))--}}
    {{--                    @foreach($new_registration as $sp)--}}

    {{--                        <div class='item h-100'>--}}
    {{--                            <div class='card h-100'>--}}
    {{--                                <div class='card-body'>--}}
    {{--                                    <div class="row ">--}}
    {{--                                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 h-mob">--}}
    {{--                                            <div class='view'>--}}
    {{--                                                <a href='{{url('detail/'.$sp->service_profile()->first()->id)}}'>--}}
    {{--                                                <a href=''>--}}
    {{--                                                    @if($sp->profile->photo!='')--}}
    {{--                                                        <img src="{{asset('uploads/profile/'.$sp->profile->photo)}}" class="img-fluid" style="border-radius: 20px;height:100px;">--}}
    {{--                                                    @else--}}
    {{--                                                        <img src="{{asset('web/img/doctor-lg.png')}}" class="img-fluid" style="border-radius: 30px;">--}}
    {{--                                                    @endif--}}
    {{--                                                </a>--}}
    {{--                                            </div>--}}
    {{--                                        </div>--}}
    {{--                                        <div class='col-xl-9 col-lg-9 col-md-9 col-sm-12'>--}}
    {{--                                            <div class="row">--}}
    {{--                                                <div class="col-sm-3 col-3 d-mob">--}}
    {{--                                                    <div class='view'>--}}
    {{--                                                        <a href='{{url('detail/'.$sp['id'])}}'>--}}
    {{--                                                            @if($sp->profile->photo!='')--}}
    {{--   <img src='{{asset('uploads/profile/'.$sp->profile->photo)}}' class='img-fluid' style="border-radius: 30px; "/>--}}
    {{-- @else--}}
    {{--     <img src='{{asset('web/img/doctor-lg.png')}}' class='img-fluid' style="border-radius: 30px;"/>--}}
    {{--         @endif--}}
    {{--                                                        </a>--}}
    {{--                                                    </div>--}}
    {{--                                                </div>--}}
    {{--                                                <div class="col-md-12 col-lg-12 col-sm-9 col-9">--}}
    {{--                                                    <a href="{{url('detail/'.$sp->service_profile()->first()->id)}}"--}}
    {{--                                                    <a href=""--}}
    {{--                                                       class='mb-1 d-block h5-responsive text-truncate'--}}
    {{--                                                       style="color:#27166D;">{{$sp->name}} ({{$sp->profile->speciality}})</a>--}}
    {{--                                                    <div class="mb-1 font-sm d-block text-truncate">--}}
    {{--                                                        {{$sp->profile->qualification}},&nbsp;--}}
    {{--                                                        {{$sp->service_profile()->first()->city.', '. $sp->service_profile()->first()->state}}--}}
    {{--                                                    </div>--}}
    {{--                                                </div>--}}
    {{--                                            </div>--}}
    {{--                                            <div class='row'>--}}
    {{--                                                <div class="col-4">--}}
    {{--                                                    <div class="text-dark mb-1 font-sm control-label d-block text-truncate">--}}
    {{--                                                        <i class="fa fa-star text-warning"></i>--}}
    {{--                                                        <i class="fa fa-star-half-o text-warning"></i>--}}
    {{--                                                        <i class="fa fa-star-o text-warning"></i>--}}
    {{--                                                    </div>--}}
    {{--                                                </div>--}}
    {{--                                                <div class='col-8 text-right'>--}}
    {{--                                                    @if($sp->service_profile()->exists() && $sp->service_profile()->first()->mark_live == 1)--}}
    {{--                                                        @if($sp->service_profile()->first()->btn_video ==1)--}}
    {{--                                                            <a href='{{url('patient/consult-now/'.$sp->service_profile()->first()->id)}}' class='text-dark'>--}}
    {{--                                                                <i class='fa fa-video text-success'></i> Consult Now</a>--}}
    {{--                                                        @else--}}
    {{--                                                            <i class="fa fa-circle text-success" aria-hidden="true"></i>--}}
    {{--                                                        @endif--}}
    {{--                                                    @else--}}
    {{--                                                        @if($sp->service_profile()->first()->btn_booknow==1)--}}
    {{--                                                            <a href='{{url('book-appoinment/'.$sp->service_profile()->first()->user_id)}}' class='text-dark'><i class="fa fa-pencil-square-o text-danger" ></i> Get Appointment</a>--}}
    {{--                                                        @else--}}
    {{--                                                            <i class="fa fa-circle text-danger" aria-hidden="true"></i>--}}
    {{--                                                        @endif--}}
    {{--                                                    @endif--}}

    {{--                                                </div>--}}
    {{--                                            </div>--}}

    {{--                                            <div class='row small text-black-50 no-gutters'>--}}
    {{--                                                <div class="col-4 mb-1 text-muted text-truncate"><b>Exp : {{$sp->profile->experiance}} Year(s)</b></div>--}}
    {{--                                                <div class='col-4 mb-1 text-success '> <b>--}}
    {{--                                                        @if($sp->service_profile()->first()->user_role_id==1)Online: @else Minimum: @endif--}}
    {{--                                                            <i class="fas fa-rupee-sign"></i> {{$sp->service_profile()->first()->min_fee}} </b></div>--}}
    {{--                                                <div class='col-4 mb-1 text-danger '><b>@if($sp->service_profile()->first()->user_role_id==1)Offline: @else Maximum: @endif <i class="fas fa-rupee-sign"></i>--}}
    {{--                                                        @if(isset($config))--}}
    {{--                                                            {{($sp->service_profile()->first()->max_fee - (($sp->service_profile()->first()->max_fee*$config['appointment_discount'])/100))}} <del class="text-muted">{{$sp->service_profile()->first()->max_fee}}</del>--}}
    {{--                                                        @else--}}
    {{--                                                            {{$sp->service_profile()->first()->max_fee}}--}}
    {{--                                                        @endif--}}
    {{--                                                    </b></div>--}}
    {{--                                            </div>--}}

    {{--                                        </div>--}}

    {{--                                    </div>--}}
    {{--                                </div>--}}

    {{--                            </div>--}}
    {{--                       	</div>--}}

    {{--                        @endforeach--}}
    {{--                    @endif--}}

    {{--			</div>--}}
    {{--			<div class="customNavigation float-right">--}}
    {{--				<a class="btn btn-dark btn-sm prev"><i class="fa fa-angle-left fa-lg"></i></a>--}}
    {{--				<a class="btn btn-dark btn-sm next"><i class="fa fa-angle-right fa-lg"></i></a>--}}
    {{--			</div>--}}
    {{--		</div>--}}
    {{--	</div>--}}
    {{--</div>--}}
    {{--</div>--}}


    {{--<div class="container-fluid bg-blue cp-md">--}}
    {{--	<div class="container">--}}
    {{--		<div class="row">--}}
    {{--			<div class="col-12 text-center">--}}
    {{--				<div class="h2-responsive mb-2">Latest Articles</div>--}}
    {{--			</div>--}}
    {{--		</div>--}}
    {{--		<div class="row">--}}
    {{--			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-1">--}}
    {{--				<div class="card h-100">--}}
    {{--					<div class="view zoom">--}}
    {{--						<a href="https://dtnehasuryawanshi.wordpress.com/2018/11/01/diet-for-dementia/?preview=true"><img src="{{asset("web/img/article1.png")}}" class="img-fluid"></a>--}}
    {{--					</div>--}}
    {{--					<div class="card-body ">--}}
    {{--						<a href="https://dtnehasuryawanshi.wordpress.com/2018/11/01/diet-for-dementia/?preview=true" class="lead text-dark">Diet For Dementia </a>--}}
    {{--						<div class="font-sm text-black-50 mb-4">Dt Neha Suryawanshi</div>--}}
    {{--						<p class="text-justify">Foods that will nourish your brain. Aim to boost your intake of the following... <br> Avocado: This is packed with the healthy fats that support brain structure and blood flow.</p>--}}
    {{--						<div>--}}
    {{--							<a href="#" class="text-black-50"><i class="fa fa-share-alt"></i></a>--}}
    {{--						</div>--}}
    {{--					</div>--}}
    {{--				</div>--}}
    {{--			</div>--}}
    {{--			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-1">--}}
    {{--				<div class="card h-100">--}}
    {{--					<div class="view zoom">--}}
    {{--						<a href="https://dtnehasuryawanshi.wordpress.com/2020/04/28/hypothyroid-during-pregnancy-its-dietary-treatment/?preview=true"><img src="{{asset("web/img/article2.png")}}" class="img-fluid"></a>--}}
    {{--					</div>--}}
    {{--					<div class="card-body ">--}}
    {{--						<a href="https://dtnehasuryawanshi.wordpress.com/2020/04/28/hypothyroid-during-pregnancy-its-dietary-treatment/?preview=true" class="lead text-dark">Hypothyroid During Pregnancy & it’s Dietary Treatment </a>--}}
    {{--						<div class="font-sm text-black-50 mb-4">Dt Neha Suryawanshi</div>--}}
    {{--						<p class="text-justify">The thyroid gland in your body is responsible for producing hormones for metabolism. Metabolism is actually the powerhouse of energy as it controls various processes in the body. </p>--}}
    {{--						<div>--}}
    {{--							<a href="#" class="text-black-50"><i class="fa fa-share-alt"></i></a>--}}
    {{--						</div>--}}
    {{--					</div>--}}
    {{--				</div>--}}
    {{--			</div>--}}
    {{--			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-1">--}}
    {{--				<div class="card h-100">--}}
    {{--					<div class="view zoom">--}}
    {{--						<a href="https://dtnehasuryawanshi.wordpress.com/2018/10/12/artificial-sweeteners-impact-on-our-health/?preview=true"><img src="{{asset("web/img/article3.png")}}" class="img-fluid"></a>--}}
    {{--					</div>--}}
    {{--					<div class="card-body ">--}}
    {{--						<a href="https://dtnehasuryawanshi.wordpress.com/2018/10/12/artificial-sweeteners-impact-on-our-health/?preview=true" class="lead text-dark">Artificial Sweeteners Impact On Our Health</a>--}}
    {{--						<div class="font-sm text-black-50 mb-4">Dt Neha Suryawanshi</div>--}}
    {{--						<p class="text-justify">Artificial sweeteners are not food. They are completely synthetic. Any ‘food-like’ substance or chemical that we put into our body that is not from nature increases the toxic load we carry. </p>--}}
    {{--						<div>--}}
    {{--							<a href="#" class="text-black-50"><i class="fa fa-share-alt"></i></a>--}}
    {{--						</div>--}}
    {{--					</div>--}}
    {{--				</div>--}}
    {{--			</div>--}}
    {{--		</div>--}}
    {{--	</div>--}}
    {{--</div>--}}

    {{--<div class="bg-primary cp-md">--}}
    {{--	<div class="container">--}}
    {{--		<div class="row">--}}
    {{--			<div class="col-12 text-center text-white">--}}
    {{--				<div class="h2-responsive mb-2">Testimonials</div>--}}
    {{--			</div>--}}
    {{--		</div>--}}
    {{--		<div class="row justify-content-center">--}}
    {{--			<div class="col-xl-8 col-lg-8 col-md-8 col-12">--}}
    {{--				<div id="testimonial" class="owl-carousel owl-theme">--}}
    {{--                    @if(isset($testimonial))--}}
    {{--                        @foreach($testimonial as $tes)--}}
    {{--                            <div class="item text-center text-white">--}}
    {{--                                <p class="mb-2">{{$tes->description}}</p>--}}
    {{--                                <div class="lead">{{$tes->title}}</div>--}}
    {{--                                <div><img src="{{asset("uploads/testimonial/".$tes->file)}}" class="img-fluid img-thumbnail"></div>--}}
    {{--                            </div>--}}
    {{--                        @endforeach--}}
    {{--                    @endif--}}
    {{--				</div>--}}
    {{--			</div>--}}
    {{--		</div>--}}
    {{--	</div>--}}
    {{--</div>--}}

    {{--<div class="container-fluid bg-blue cp-md">--}}
    {{--	<div class="container">--}}
    {{--		<div class="row">--}}
    {{--			<div class="col-12 text-center">--}}
    {{--				<div class="h2-responsive mb-2 text-white">Our Partners</div>--}}
    {{--			</div>--}}
    {{--		</div>--}}
    {{--		<div class="row">--}}
    {{--			<div class="col-12">--}}
    {{--				<div id="clients2" class="owl-carousel owl-theme">--}}
    {{--                    @if(isset($partners))--}}
    {{--                        @foreach($partners as $p)--}}
    {{--                    <div class='item'>--}}
    {{--                        <div class='card h-100' style="min-width:100px; height:100px;">--}}
    {{--                            <div class='card-body p-0 d-flex justify-content-center'>--}}
    {{--                                <img src='{{asset("uploads/partner/".$p->file)}}' class="img-fluid" style="height:90px;"/>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                        @endforeach--}}
    {{--                        @endif--}}
    {{--				</div>--}}
    {{--			</div>--}}
    {{--		</div>--}}
    {{--	</div>--}}
    {{--</div>--}}
@endsection
@section('additional_script')
    <script>
        $('.state-select').change(function() {
            $.ajax({
                type:"GET",
                url:"{{url('ajax/state')}}?state="+$(this).val(),
                success: function(response) {
                    if (response.success) {
                        $('.city-select').find('option').remove().end().append('<option selected hidden disabled>City</option>');
                        var city = response.city;
                        $.each(city, function(i, d) {
                            $('.city-select').append('<option>' + d.name + '</option>');
                        });
                    }
                },
            });
        });

        // City Change
        $('.city-select').change(function() {
            $.ajax({
                type:"GET",
                url:"{{url('ajax/city')}}?city="+$(this).val()+'&state='+$(this).parent().parent().parent().find('.state-select').val(),
                success: function(response) {
                    if (response.success) {
                        $('.area-select').find('option').remove().end().append('<option selected hidden disabled>Area</option>');
                        var area = response.area;
                        $.each(area, function(i, d) {
                            $('.area-select').append('<option>' + d.area + '</option>');
                        });
                    }
                },
            });
        });

    </script>
    <script type="text/javascript">
        $(function() {
          $("#changethewords").changeWords({
            time: 2500,
            animate: "fadeIn",
            selector: "span",
            repeat:true,

          });
        });

        $('.owl-top-sevices').owlCarousel({
            loop:true,
            margin:5,
            nav:false,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1.5
                },
                1000:{
                    items:2
                }
            }
        });
        $('.owl-testimonials').owlCarousel({
            loop:true,
            autoplay:true,
            autoplayTimeout:5000,
            margin:5,
            nav:false,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $('.owl-partners').owlCarousel({
            loop:false,
            margin:5,
            autoplay:true,
            autoplayTimeout:2000,
            nav:false,
            responsiveClass:true,
            responsive:{
                0:{
                    items:3
                },
                600:{
                    items:4
                },
                1000:{
                    items:5.6
                }
            }
        });
// $('#search_for').select2();


      </script>
@endsection
