@extends('layouts.layout')
@section('custom_style')
    <link rel="stylesheet" href="{{asset('webrtc/bootstrap_icons/fonts/bootstrap-icons.css')}}">
    <script src="https://www.gstatic.com/firebasejs/8.8.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.8.1/firebase-firestore.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('webrtc/main.css')}}">
    <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
@endsection
@section('content')
    <section clas="main">
        <div class="profile-view-section">
    @include('web.custome-menu')
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
        <div class="card mb-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 ">
                        <h4>Response to this request</h4>
                    </div>
                    @if(date('d-m-Y') == date('d-m-Y',strtotime($booking->booking_date)))
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        @if($booking->is_con_link_available != 0 && $booking->is_con_link_expired!=1)
                            <button type="button" class="btn btn-info btn-sm" id="btn-join-confrence"
                            @if($booking->is_con_link_available == 0){{'style=display:none;'}} @endif>
                                <i class="fas fa-video text-white"></i> Consult Now
                            </button>
                            {{--                                    <button type="button" class="btn btn-info btn-sm"  @if($booking->is_con_link_available == 0){{'style=display:none;'}} @endif>--}}
                            {{--                                        <i class="fas fa-video text-white"></i> Consult Now--}}
                            {{--                                    </button>--}}
                            <a href="tel:{{$booking->patient->mobile}}"
                               class="btn btn-success btn-sm" @if($booking->is_con_link_available == 0){{'style=display:none;'}} @endif><i class="fas fa-phone-alt"></i> Call Now</a>
                        @else
                            <h6 class="text-danger">
                                Consultation link is not available.
                            </h6><br>
                        @endif

                            <!-- Example split danger button -->
                                @if($booking->is_con_link_available == 1)
                                <div class="btn-group btn-group-sm">
                                    <button type="button" class="btn btn-primary"><i class="fas fa-file-medical-alt"></i> Medical Records</button>
                                    <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="sr-only"></span>
                                    </button>
                                    <div class="dropdown-menu">
                                        <a href="{{url('medical-data/'.$booking->patient_id)}}" target="_blank"
                                           class="dropdown-item text-info"> Uploaded Records
                                        </a>
                                        <a href="{{url('consultation-data/'.$booking->patient_id)}}" target="_blank"
                                           class="dropdown-item text-info"> Live Consultations
                                        </a>
{{--                                        <a href="{{url('appointment-data/'.$booking->patient_id)}}" target="_blank"--}}
{{--                                           class="dropdown-item text-info"> Appointment Bookings--}}
{{--                                        </a>--}}
                                    </div>
                                </div>
                                    @endif


                    </div>
                    @else
                        <div class="col-lg-12 col-md-12 col-sm-12">
                        <h6 class="text-danger"> Request Expired</h6>
                        </div>
                        @endif
                </div>
            </div>
        </div>


        <div id="conference-frame" class="mb-3 hide">
            <div class="card">
                <div class="card-body">
                    @if(isset($booking) && $booking && $booking->is_con_link_available==1)
{{--                        <iframe scrolling src="{{$booking->con_link}}" width="100%" height="100%" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen allow="camera; microphone; display-capture; autoplay"></iframe>--}}

                        <div class="main-container cp-md">
                            <div id="videos" class="video-container">
                                <video id="localVideo" class="localVideo" data="local" muted autoplay playsinline></video>
                                <video id="remoteVideo" class="remoteVideo remote" data="remote" autoplay playsinline></video>
                                <div class="live-alert" id="alert-notification" style="display:none;">
                                    <div class="live-alert-header">
                                        <!--<div class="live-alert-header-text">-->
                                        <!--    <h4>Live Consultation</h4>-->
                                        <!--</div>-->
                                        <!--<div class="live-alert-close">-->
                                        <!--    <i class="bi bi-x"></i>-->
                                        <!--</div>-->
                                    </div>
                                    <div class="live-alert-body">
                                        <div class="live-alert-message">
                                            <p id="alert-msg"></p>
                                        </div>
                                        <div class="live-alert-action">
                                            <button class="live-alert-action-button btn btn-danger btn-sm">OK</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="control-button-container">
                                @if(!empty($booking->caller) && $booking->caller==auth()->user()->id)
                                    <div class="start-button">
                                        <button type="button" class="btn btn-sm btn-info" disabled id="createBtn">
                                            <!--<i class="bi bi-telephone-fill"></i>-->
                                            Join
                                        </button>
                                    </div>
                                @elseif(empty($booking->caller))
                                    <div class="start-button">
                                        <button type="button" class="btn btn-sm btn-info"  disabled id="createBtn">
                                            <!--<i class="bi bi-telephone-fill"></i>-->
                                            Join
                                        </button>
                                    </div>
                                @else
                                    <div class="start-button">
                                        <button type="button" class="btn btn-sm btn-info"  disabled id="joinBtn">
                                            <!--<i class="bi bi-telephone-fill"></i>-->
                                            Join
                                        </button>
                                    </div>
                                @endif
                                <div class="mic-button">
                                    <button class="btn btn-sm" id="disableAudio">
                                        <i class="bi bi-mic"></i>
                                    </button>
                                </div>
                                <div class="end-button">
                                    <button type="button" class="btn btn-sm"  disabled id="hangupBtn">
                                        <i class="bi bi-telephone-fill"></i>
                                    </button>
                                </div>
                                <div class="video-button">
                                    <button class="btn btn-sm" id="disableVideo">
                                        <i class="bi bi-camera-video"></i></button>
                                </div>
                            </div>
                            <div class="full-button">
                                <i class="bi bi-fullscreen"></i>
                            </div>
                            <div class="setting-button">
                                <div class="sett-button"><i class="bi bi-gear"></i></div>
                            </div>
                            <!--<div class="cameraSwitch-button">-->
                            <!--    <div class="cam-button"><i class="bi bi-arrow-repeat"></i></div>-->
                            <!--</div>-->
                            <div class="setting-container">
                                <!-- <div id="logs-error" style="display: block; height:100%; width: 500px; border: red solid 2px;">
                                    logs
                                </div> -->

                                <div class="select">
                                    <label for="audioSource">Audio input source: </label>
                                    <select id="audioSource" class="form-control"></select>
                                </div>

                                <div class="select">
                                    <label for="audioOutput">Audio output destination: </label>
                                    <select id="audioOutput" class="form-control"></select>
                                </div>

                                <div class="select">
                                    <label for="videoSource">Video source: </label>
                                    <select id="videoSource" class="form-control"></select>
                                </div>

                                <div class="close-button">
                                    <button class="setting-close">Close</button>
                                </div>
                            </div>
                        </div>

                    @endif
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                @if(isset($booking))
                <form action="{{url('live-consultation/'.$booking->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}


                    <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                            <img src="{{asset('web/v2/img/logo/logo.svg')}}" class="img-fluid">
                        </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="h3-responsive mb-2">{{$booking->provider->name}}</div>
                                <div class="mb-2">{{$booking->provider->profile->speciality}}</div>
                                {{-- <div class="mb-2">Specification</div> --}}
                                <div class="mb-2">{{$booking->provider->profile->qualification}}</div>
                                <div class="mb-2">{{$booking->provider->profile->other_speciality}}</div>
                                <div class="mb-2">{{$booking->provider->profile->address}}</div>
                            </div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <label class="control-label">Patient's Name <span class="req">*</span></label>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Patient Name" name="patient_name" required value="{{$booking->patient_name}}">
                            </div>
                            @error('patient_name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="row">
                                <div class="col-6">
                                    <label class="control-label">Patient's Age <span class="req">*</span></label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Age" name="age" required value="{{$booking->age}}">
                                    </div>
                                    @error('age')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-6">
                                    <label class="control-label">Patient's Mobile </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Mobile" name="mobile" value="{{$booking->mobile}}">
                                    </div>
                                    @error('mobile')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <label class="control-label">Patient's Gender <span class="req">*</span></label>
                                    <select class="form-control" name="gender" required>
                                        <option selected disabled>Choose Gender</option>
                                        <option @if($booking->gender === "Male"){{'selected'}}@endif>Male</option>
                                        <option @if($booking->gender === "Female"){{'selected'}}@endif>Female</option>
                                    </select>
                                    @error('gender')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="control-label">OMED Code / Center Name </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control @error('omed_code') is-invalid @enderror" placeholder="OMED Code" name="omed_code" value="@if(isset($booking)){{$booking->omed_code}}@endif">
                                    </div>
                                    @error('omed_code')
                                    <div class="alert alert-danger">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <label class="control-label">Name (Clinic/BikeDr)</label>
                                    <input type="text" class="form-control @error('bike_dr') is-invalid @enderror" placeholder="Clinic/BikeDr" name="bike_dr" value="@if(isset($booking)){{$booking->bike_dr}}@endif">

                                    @error('bike_dr')
                                    <div class="alert alert-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Patient's Address <span class="req">*</span></label>
                                <input type="text" class="form-control" placeholder="Address" name="address" required value="{{$booking->address}}">
                            </div>
                            @error('address')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            {{--					<div class="form-group">--}}
                            {{--						<input type="text" class="form-control" placeholder="Omed Clinic">--}}
                            {{--					</div>--}}
                        </div>
                    </div>
                    <hr>

                    <div class="row mt-4">
                        <div class="col-md-3 col-12">
                            <label class="control-label">Patient's BP </label>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="BP" name="bp" value="{{$booking->bp}}">
                            </div>
                            @error('bp')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-3 col-12">
                            <label class="control-label">Patient's Pulse </label>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Pulse" name="pulse" value="{{$booking->pulse}}">
                            </div>
                            @error('pulse')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-3 col-12">
                            <label class="control-label">Patient's Temperature </label>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Temperature" name="temperature" value="{{$booking->temperature}}">
                            </div>
                            @error('temperature')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-3 col-12">
                            <label class="control-label">Patient's SPO<sub>2</sub> </label>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="SPO2" name="spo" value="{{$booking->spo}}">
                            </div>
                            @error('spo')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-3 col-12">
                            <label class="control-label">Patient's Weight </label>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Weight" name="weight" value="{{$booking->weight}}">
                            </div>
                            @error('weight')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-3 col-12">
                            <label class="control-label">History </label>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="History" name="history" value="{{$booking->history}}">
                            </div>
                            @error('history')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-3 col-12">
                            <label class="control-label">Complain </label>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Complain" name="complain" value="{{$booking->complain}}">
                            </div>
                            @error('complain')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-3 col-12">
                            <label class="control-label">P. Diagnosis <span class="req">*</span></label>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Diagnosis" name="diagnostic"
                                       required value="@if(isset($booking) && $booking->diagnostic!=''){{$booking->diagnostic}} @else {{old('diagnostic')}}@endif">
                            </div>
                            @error('diagnostic')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
<hr>
                   <div class="row">
                        <div class="col-md-6 col-12 mb-4">
                            <label class="control-label">Doctor Note/Prescriptions </label>
                            <div class="form-group mb-3">
                                <textarea rows="16" class="form-control" placeholder="Doctor Note / Prescriptions"
                                          name="prescription" id="editor">@if(isset($booking))
                                        {{$booking->prescription}} @endif</textarea>
                            </div>
                            @error('prescription')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="mt-1 mb-1">
                                <h6 class="text-center" style="font-weight:bold">OR</h6>
                            </div>
{{--                            <div class="form-group">--}}
                                <label class="control-label">Upload Prescriptions</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="prescription_image">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            @error('prescription_image')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                        </div>
                        <div class="col-md-6 col-12">
                           <label class="control-label">Uploaded Note/Prescriptions</label>
                            <div style="min-height:400px; width:100%; border:1px solid #C7C7C7; padding: 5px;">

                                   @if(!empty($booking->prescription))
                                   {!! $booking->prescription !!}
                                   @endif
                                       @if($booking->prescription_image!='')
    <img src="{{asset('uploads/patientprofile/prescription/'.$booking->prescription_folder.'/'.$booking->prescription_image)}}" class="img-fluid">
                                       @endif
                                @if($booking->prescription=='' && $booking->precsription_image=='')
                                       {{'Uploaded Note & Prescriptions'}}
                                    @endif
                            </div>
                        </div>
                    </div>
                        <input type="hidden" name="provider_id" value="{{$booking->provider_id}}">
                        <input type="hidden" name="patient_id" value="{{$booking->patient_id}}">
                    <div class="row mt-1">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <button type="submit" class="btn btn-primary" id="">Update</button>
                        </div>
                    </div>

                </form>
                @endif
                 </div>
        </div>
    </div>
    </div>
    </section>
@endsection
@section('additional_script')
<script src="https://cdn.ckeditor.com/ckeditor5/22.0.0/classic/ckeditor.js"></script>

    <script>
        @if($booking->is_request_accepted==1 && $booking->is_paid==0 && $booking->is_con_link_available==0)
        paymentStat({{$booking->id}});
            @endif
              ClassicEditor
            .create( document.querySelector( '#editor' ),{
                toolbar: {
                    items: [
                        "heading",
                        "|",
                        "bold",
                        "italic",
                        "bulletedList",
                        "numberedList",
                        "blockQuote",
                        "|",
                        "undo",
                        "redo"
                    ]
                },
                language: 'en'
        } )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );

        $('document').ready(function(){

            $('#btn-join-confrence').click(function(){
                $('#conference-frame').toggle();
            });
       $('#btn-accept').click(function(){
       var req_type= "is_request_accepted";
           var status= true;
       var book_id = $(this).attr('data');
           requestResponse(req_type,status,book_id);
        });

       $('#btn-reject').click(function(){
           var req_type= "is_request_rejected";
           var status= true;
           var book_id = $(this).attr('data');
           requestResponse(req_type,status,book_id);
        });
        });

        function requestResponse(req_type,status,book_id)
        {
            $.ajax({
                        type:'ajax',
                        method:'post',
                        data:{'resp_request':req_type,'status':status,'id':book_id,'_token':"{{ csrf_token() }}"},
                        url:'{{url('consultation-response')}}',
                        asycn:true,
                        dataType:'JSON',
                        success:function(response){
                            if(response.success){
                                location.reload();
                                // alertify.set('notifier','position', 'top-right');
                                // alertify.success(response.message);
                                // $('#mg-info').removeClass('text-danger').addClass('text-info').text(response.message);
                                // $('#btn-reject,#btn-accept').hide();
                                // if(response.hasOwnProperty(resp_code))
                                // {
                                //    paymentStat(response.resp_code);
                                // }

                            }
                            else{
                                alertify.set('notifier','position', 'top-right');
                                alertify.error(response.message);
                            }
                        },
                        error:function(){
                            alertify.set('notifier','position', 'top-right');
                            alertify.error('Unable to process your request.');
                        }
                    });
        }
        function paymentStat(res_code) {
            interval = setInterval( function() {
                $.ajax({
                    type:'ajax',
                    method:'post',
                    data:{
                        '_token':"{{ csrf_token() }}",
                        'res_code' : res_code,
                        'uid': {{Auth::user()->id}},
                    },
                    url:"{{url('checkPaymentStatus')}}",
                    dataType:'json',
                    asycn:true,
                    success:function(response){
                        if(response.success)
                        {
                            clearInterval(interval);
                            location.reload();
                        }
                        else{
                            if(response.stop)
                            {
                                clearInterval(interval);
                                location.reload();
                            }
                            else{
                                paymentStat(response.res_code);
                            }
                        }
                    }
                });
            }, 1000*15);
        }

      {{--$('#btn-join-confrence').click(function(){--}}
      {{--    $.ajax({--}}
      {{--        type:'ajax',--}}
      {{--        method:"POST",--}}
      {{--        url:"{{url('change-flag')}}",--}}
      {{--        data:{--}}
      {{--            "id":{{$booking->id}},--}}
      {{--            "_token":"{{csrf_token()}}",--}}
      {{--            "table":'online_bookings',--}}
      {{--            "column":"doc_consulted",--}}
      {{--        },--}}
      {{--        dataType:"json",--}}
      {{--        success:function(res){--}}

      {{--        }--}}
      {{--    });--}}
      {{--})--}}
    </script>
<script src="{{asset('webrtc/app.js')}}" async></script>
<script src="{{asset('webrtc/deviceAccess.js')}}" async></script>
<script src="{{asset('webrtc/main.js')}}" async></script>
<script>
    var firebaseConfig = {
        apiKey: "AIzaSyDAo3iV_T3C0njiXXzIA0kTb6dJyV2mEaU",
        authDomain: "omed-d39e0.firebaseapp.com",
        projectId: "omed-d39e0",
        databaseURL:"https://omed-d39e0-default-rtdb.asia-southeast1.firebasedatabase.app",
        storageBucket: "omed-d39e0.appspot.com",
        messagingSenderId: "139501672892",
        appId: "1:139501672892:web:bc8346561f16b42a9333b9",
        measurementId: "G-KHRPJ0SZPV"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
</script>
<script async>

    @if(isset($booking))

    window.onbeforeunload = function() {
        return hangCall();
    }

    function initiateCall(res_data)
    {
        $.ajax({
            type:'ajax',
            method:'post',
            data:{
                '_token':"{{ csrf_token() }}",
                'id':res_data,
                'caller':"{{auth()->user()->id}}",
                'res_code':"{{$booking->id}}",
                'ident':"{{auth()->user()->id}}",
            },
            url:"{{url('initiate-consultation-call')}}",
            dataType:'json',
            async:true,
            success:function(response){
                if(response.success)
                {
            if(response.roomExists)
            {
                showAlert('Please wait ...');
                joinRoomById(response.id);
                $('#hangupBtn').addClass("btn-danger");
            }
                    showAlert('Please wait ...');
                }
                else{
                    showAlert('Unable to initiate call');
                    hangUp();
                }
            }
        });


    }
    async function joinRoom(){
       await joinRoomById("{{isset($booking)?$booking->con_link:''}}");
       $('#hangupBtn').addClass("btn-danger");
    }
    function showAlert(message){
        $('#alert-notification').show();
        $('#alert-msg').html(message);
    }
    function askForCall(){
        // $('-infolert-msg').html('Please click on <button type="button" class="btn btn-sm btn-success" style="height:30px;width:30px;border-radius:50px;padding:0px;margin:0px;">'+'<i class="bi bi-telephone-fill"></i>'+'</button> button');
    }
    function audioMuted(){
        $('#disableAudio').html('<i class="bi bi-mic-mute"></i>');
        $('#disableAudio').addClass("btn-info");
    }
    function audioUnMuted(){
                $('#disableAudio').html('<i class="bi bi-mic"></i>');
                $('#disableAudio').removeClass("btn-info");
    }
    function videoMuted(){
        $('#disableVideo').html('<i class="bi bi-camera-video-off"></i>');
        $('#disableVideo').addClass("btn-success");
    }
    function videoUnMuted(){
        $('#disableVideo').html('<i class="bi bi-camera-video"></i>');
        $('#disableVideo').removeClass("btn-success");
    }
    function hangCall(){
        $.ajax({
            type:'ajax',
            method:'post',
            data:{
                '_token':"{{ csrf_token() }}",
                'res_code':"{{$booking->id}}",
                'ident':"{{auth()->user()->id}}",
            },
            url:"{{url('hang-call')}}",
            dataType:'json',
            async:true,
            success:function(response){
                if(response.success)
                {
                    hangUp();
                }
                else{
                    showAlert('Something went wrong.');
                }
            }
        });
    }
    @endif
</script>

@endsection
