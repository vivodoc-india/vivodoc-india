@extends('layouts.layout')
@section('content')
<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">Disgnostic &amp; Lab Services</div>
			</div>
		</div>
	</div>
</div>
<div class="container cp-lg">
	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
			<div class="view zoom">
				<img src="{{asset('web/img/40568.jpeg')}}" class="img-fluid">
			</div>
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
			<div class="h2-responsive mb-2">All type of Diagnostic services</div>
			<p>Get appointment for all type of diagnostic services, appointment will be taken on your behalf with online payment to us. (our Charges will be less than the actual charges of the respsctive Diagnostic service provider )</p>
			<p>You can also ask appointment of your favourite diagnostic centeres, the appointment will be taken on your behalf with online payment to us. (our Charges will be less than the actual charges of the respective Diagnostic service provider )</p>
			<p>Lab investigations ( All type of blood &amp; urine tests) along with ECG can be done at our multiple  AAH Digital Health centers ( AAH Centers &amp; Bike Doc ) on ultra low charges. You can also get the sample collection services for all type of Blood &amp; Urine tests along with ECG services at your doorstep (Home Service Charges extra..)</p>
		</div>
	</div>
</div>
@endsection
