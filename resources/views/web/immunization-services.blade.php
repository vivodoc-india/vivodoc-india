@extends('layouts.layout')
@section('content')
<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">Immunization services</div>
			</div>
		</div>
	</div>
</div>
<div class="container cp-md">
	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
			<div class="view zoom">
				<img src="{{asset('web/img/45842.jpeg')}}" class="img-fluid">
			</div>
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
			<div class="h2-responsive mb-2">Immunization services</div>
			<p>All type of Immunization for Children & Adults at Affordable charges.</p>
			<p>At our multiple Digital Health Centers Home services.on extra charges.</p>
		</div>
	</div>
</div>
@endsection
