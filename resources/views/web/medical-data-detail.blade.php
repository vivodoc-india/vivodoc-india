@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Medical Data Detail</div>
                </div>
            </div>
        </div>
    </div>
    @include('web.custome-menu')
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">

                <div class="card">
                    <div class="card-body">
                        @if(isset($record) && $record->isNotEmpty())
                            <div class="row">
                                @foreach($record as $rec)
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-3">
                                        <!-- Card -->
                                        <div class="card h-100">
                                            <!-- Card image -->
                                            <a href="{{asset($rec->file)}}" target="_blank">
                                                <img class="card-img-top" src="{{asset($rec->file)}}" alt="{{$rec->file}}" height="200">
                                            </a>
                                            <!-- Card content -->
                                            <div class="card-body">
                                                <label class="control-label">Created On: {{date('d-m-Y h:i:s',strtotime($rec->created_at))}}</label>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                            <!-- Card -->
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
