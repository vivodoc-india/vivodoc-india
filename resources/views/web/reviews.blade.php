@extends('layouts.layout')
@section('content')
<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">Reviews &amp; Ratings</div>
			</div>
		</div>
	</div>
</div>
@include('web.custome-menu')
		<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
            @if(isset($review) && count($review) > 0)
			<div class="card bg-light shadow-none">
				<div class="card-body">
                        @foreach($review as $rev)
					<div class="card mb-3">
						<div class="card-body">
							<div class="row">
								<div class="col-12">
									<div class="mb-2">
                                        @for($n=1;$n<=$rev->rating;$n++)
										<i class="fa fa-star text-warning"></i>
										@endfor

									</div>
									<div class="lead">{{ucwords($rev->patient->name)}}</div>
									<div class="mb-2">{{$rev->review}}</div>
									<div>{{date('d M Y',strtotime($rev->created_at))}}</div>
								</div>
							</div>
						</div>
					</div>
                        @endforeach

                        @else
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">

                                       <h3 class="h3-responsive"> No reviews & ratings are available.</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
				</div>
			</div>
            <div class="row mt-3">
                {{$review->links()}}
            </div>

		</div>

	</div>
</div>
@endsection
