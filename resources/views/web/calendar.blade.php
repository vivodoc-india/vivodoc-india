@extends('layouts.layout')
@section('content')
<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">Calendar</div>
			</div>
		</div>
	</div>
</div>
@include('web.custome-menu')
		<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
			<div class="card">
			    <div class="card-body">
                    {!! $calendar->calendar() !!}

                </div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('additional_script')
    {!! $calendar->script() !!}
    @endsection
