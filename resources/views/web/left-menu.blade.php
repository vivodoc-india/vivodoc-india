<div class="d-flex justify-content-start">
    <button class="btn btn-light d-sm-block d-md-none" id="sidebarButton">
        <i class="fa fa-bars"></i> Dashboard Menu
    </button>
</div>
<div class="left-side-menu">
    <ul class="dashboard-side-menu">
        @if(Auth::user()->user_role_id==3)

            <li class="active">
                <a href="{{url('patient/update-profile')}}">
                    <i class="fa fa-user-edit" aria-hidden="true"></i>
                    <div class="link-name">Update Profile</div>
                </a>
            </li>
            <li>
                <a href="{{url('patient/medical-data')}}" >
                    <i class="fa fa-file-medical" aria-hidden="true"></i>
                    <div class="link-name">Medical History</div>
                </a>
            </li>
            <li>
                <a href="{{url('patient/appoinment-booking')}}">
                    <i class="fa fa-calendar-check" aria-hidden="true"></i>
                    <div class="link-name">Appointment Bookings</div></a>
            </li>
            <li>
                <a href="{{url('patient/consultation-booking')}}" >
                    <i class="fa fa-video" aria-hidden="true"></i>
                    <div class="link-name">Live Consultation Bookings</div>
                </a>
            </li>
            <li>
                <a href="{{url('patient/ambulance-bookings')}}" >
                    <i class="fa fa-ambulance" aria-hidden="true"></i>
                    <div class="link-name">Ambulance Bookings</div>
                </a>
            </li>
        @else
            <li>
                <a href="{{url('update-profile')}}" >
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <div class="link-name">Update Profile</div>
                </a>
            </li>
            <li>
                <a href="{{url('services')}}" >
                    <i class="fa fa-user-gear" aria-hidden="true"></i>
                    <div class="link-name">Update Services</div>
                </a>
            </li>
            <li>
                <a href="{{url('event/add')}}" >
                    <i class="fa fa-calendar-plus"></i>
                    <div class="link-name">Add Schedule</div>

                </a>
            </li>
            <li>
                <a href="{{url('event/manage')}}" >
                    <i class="fa fa-calendar-days"></i>
                    <div class="link-name">Manage Schedule</div>

                </a>
            </li>
            {{--<li><a href="{{url('event')}}" >View Calendar</a></li>--}}
{{--            <li>--}}
{{--                <a href="{{url('enquiries')}}" >--}}
{{--                    <div class="link-name">Enquiries</div>--}}
{{--                    <i class="fa fa-angle-right" aria-hidden="true"></i>--}}
{{--                </a>--}}
{{--            </li>--}}
            <li>
                <a href="{{url('booking/get-bookings')}}" >
                    <i class="fa fa-calendar-check" aria-hidden="true"></i>
                    <div class="link-name">Appointment Bookings</div>
                </a>
            </li>
            <li>
                <a href="{{url('live-consultation')}}" >
                    <i class="fa fa-video" aria-hidden="true"></i>
                    <div class="link-name">Live Consultation Bookings</div>
                </a>
            </li>
            @if(Auth::user()->service_profile->where('user_role_id',6)->count())
                <li>
                    <a href="{{url('ambulance-bookings')}}"  >
                        <i class="fa fa-ambulance" aria-hidden="true"></i>
                        <div class="link-name">Ambulance Bookings</div>
                    </a>
                </li>
            @endif
        @endif
    </ul>
</div>
