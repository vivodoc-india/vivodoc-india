@extends('layouts.layout')
@section('content')
    <section class="main">
        <div class="profile-view-section">
<div class="container">
	<div class="row">
		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-4">
			<h3>Who are we?</h3>
			<p>AAHPL (VivoDoc) is a healthcare service provider with services in Bihar, Jharkhand, Assam, West Bengal, Uttar Pradesh, Rajasthan, Telangana, Andhra Pradesh, Goa, Patna, Varanasi and Delhi. We are going to be operational in more cities very soon.</p>
			<h3>Why join us</h3>
			<ul>
				<li>Become a part of India’s first complete healthcare solution platform</li>
				<li>Play an active role in providing affordable and accessible healthcare services to every citizen</li>
				<li>Learn and grow at the same time</li>
				<li>Ideate and implement at work</li>
				<li>Take your career to the next level</li>
				<li>Empowerment and recognition at regular intervals</li>
				<li>Incentives at each and every step</li>
				<li>Opportunity for a secure lifetime earnings.</li>
			</ul>
			<p>We have requirements PAN India for the Marketing Field staffs (Part Time) on incentives</p>
			<p>We have requirements for the Nurses and Paramedical staffs in the cities we are serving</p>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
			<div class="card">
				<div class="card-header">Qualification &amp; Experience</div>
				<div class="card-body">
					<h4 class="red-text">Nursing Staff</h4>
					<div>- ANM, G.N.M. &amp; B.Sc</div>
					<div>- Nursing with their Region Registration Certificate</div>
					<div class="mb-5">- Experience - 1-5 year</div>
					<h4 class="red-text">Paramedical Staff</h4>
					<div>- B.Sc.Nursing,OT, Critical care, Lab Technology etc</div>
					<div>- Lab, OT, X-Ray, Dialysis Technician etc</div>
					<div>- Diploma in Anaesthesia, Nursing Care Assistant etc.</div>
					<div class="mb-5">- Experience - 1-5 year</div>
					<h4 class="red-text">Marketing Field Staff</h4>
					<div>- Min 12th Pass-out</div>
					<div>- Languages - Any Regional Language</div>
				</div>
			</div>
		</div>
	</div>
</div>
        </div>
    </section>
@endsection
