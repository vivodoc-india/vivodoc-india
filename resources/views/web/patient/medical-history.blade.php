@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
<div class="container-fluid">
	<div class="row">
		<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 side-bg">
		@include("web.left-menu")
		</div>
		<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 pb-4 pt-4">
			<div class="card mb-4">
				<div class="card-body">
					<table id="example" class="display responsive nowrap" style="width:100%">
						<thead>
							<tr>
								<th>S.No.</th>
								<th>Document Type</th>
								<th>Uploaded On</th>
								<th>Action</th>

							</tr>
						</thead>
						<tbody>
                        @if(isset($list) && count($list) > 0)
                            @php $n=1 @endphp
                            @foreach($list as $rec)
                                <tr>
                                    <td>{{$n}}</td>
                                    <td>{{ucwords($rec->title)}}</td>
                                    <td>{{date('d-m-Y',strtotime($rec->updated_at))}}</td>
                                    <td>
                                        <a type="button" class="btn btn-light btn-sm" href="{{url('patient/medical-data/'.$rec->id)}}"> <i class="fas fa-eye"></i> </a>
{{--                                        <a type="button" class="btn btn-light btn-sm delete_button_action" href="javascript:void(0)" data-url="{{route('medical-data.destroy',$rec->id)}}" data-id="{{$rec->id}}" data-toggle="modal" data-target="#deleteModal"> <i class="far fa-trash-alt"></i> </a>--}}

                                    </td>

                                </tr>
@php $n++ @endphp
                                @endforeach
                            @endif
						</tbody>

					</table>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
                    <form action="{{route('medical-data.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
					<div class="row">
						<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 mb-3">
						<input type="text" name="title" class="form-control @error('title') is-invalid @enderror" placeholder="what type of documnts these are ?" required value="{{old('title')}}">
						</div>
						<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 mb-3">

                            <select class="custom-select form-control" name="data_type" required>
                                <option selected disabled>Please choose type of data</option>
                                <option value="prescription">Prescription</option>
                                <option value="lab_test">Lab Test Report</option>
                            </select>
                            @error('data_type')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
{{--                            <div class="custom-file">--}}
{{--                                <input type="file" class="custom-file-input  @error('docs') is-invalid @enderror" id="File" name="docs[]"  multiple required>--}}
{{--                                <label class="custom-file-label" for="File" >Choose file</label>--}}
{{--                            </div>--}}
{{--                            <span class="misc">Allowed file type: jpg, jpeg, png. Use <span class="badge badge-secondary">'CTRL'</span> for multiple files selection.</span>--}}
{{--                            @error('docs')--}}
{{--                            <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                            @enderror--}}
						</div>
						<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 mb-3">
							<button type="submit" class="btn btn-read-more btn-sm rounded-pill">Submit</button>
						</div>
					</div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>
        </div>
    </section>
@endsection
