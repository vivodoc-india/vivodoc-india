
@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Login</div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cp-lg bg-light">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                <div class="card rounded-0">
                    <div class="card-header bg-white text-center">
                        <img src="{{asset('web/v2/img/logo/logo.svg')}}" width="150">
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('patient/login') }}">
                            @csrf
                            <div class="form-group">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter your registered email id">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">

                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="Enter your password" autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary rounded-pill btn-block">
                                {{ __('Login') }}
                            </button>


                            <div class="mt-3 text-center">
                                @if (Route::has('password.request'))
                                    <a  href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                            {{-- <button type="button" class="btn btn-primary rounded-pill btn-block">Login</button>
                            <div class="mt-3 text-center"><a href="#">Forgot Password?</a></div> --}}
                            <div class="mt-3 text-center">Don't have an account? <a href="{{url('patient/register')}}">Sign Up</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
