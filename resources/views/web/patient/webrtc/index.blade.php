@extends('layouts.layout')
@section('custom_style')
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{asset('webrtc/bootstrap_icons/fonts/bootstrap-icons.css')}}">
    <script src="https://www.gstatic.com/firebasejs/8.8.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.8.1/firebase-firestore.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('webrtc/main.css')}}">
    <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
@endsection
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Consult Now</div>
                </div>
            </div>
        </div>
    </div>
 <div class="main-container cp-md">
     <div id="videos" class="video-container">
         <video id="localVideo" class="localVideo" data="local" muted autoplay playsinline></video>
         <video id="remoteVideo" class="remoteVideo remote" data="remote" autoplay playsinline></video>
     </div>
     <div class="control-button-container">
         <div class="start-button"><i class="bi bi-telephone-fill"></i></div>
         <div class="mic-button"><i class="bi bi-mic"></i></div>
         <div class="end-button"><i class="bi bi-telephone-fill"></i></div>
         <div class="video-button"><i class="bi bi-camera-video"></i></div>
     </div>
     <div class="setting-button">
         <div class="sett-button"><i class="bi bi-gear"></i></div>
     </div>
     <!--<div class="cameraSwitch-button">-->
     <!--    <div class="cam-button"><i class="bi bi-arrow-repeat"></i></div>-->
     <!--</div>-->
     <div class="setting-container">

         <div id="buttons">
             <button class="mdc-button mdc-button--raised" id="cameraBtn">
                 <i class="material-icons mdc-button__icon" aria-hidden="true">perm_camera_mic</i>
                 <span class="mdc-button__label">Open camera & microphone</span>
             </button>
             <button class="mdc-button mdc-button--raised" disabled id="createBtn">
                 <i class="material-icons mdc-button__icon" aria-hidden="true">group_add</i>
                 <span class="mdc-button__label">Create room</span>
             </button>
             <button class="mdc-button mdc-button--raised" disabled id="joinBtn">
                 <i class="material-icons mdc-button__icon" aria-hidden="true">group</i>
                 <span class="mdc-button__label">Join room</span>
             </button>
             <button class="mdc-button mdc-button--raised" disabled id="hangupBtn">
                 <i class="material-icons mdc-button__icon" aria-hidden="true">close</i>
                 <span class="mdc-button__label">Hangup</span>
             </button>
         </div>
         <div class="select">
             <label for="audioSource">Audio input source: </label>
             <select id="audioSource" class="form-control"></select>
         </div>

         <div class="select">
             <label for="audioOutput">Audio output destination: </label>
             <select id="audioOutput" class="form-control"></select>
         </div>

         <div class="select">
             <label for="videoSource">Video source: </label>
             <select id="videoSource" class="form-control"></select>
         </div>

         <div>
             <span id="currentRoom"></span>
         </div>
         <div class="close-button">
             <button class="setting-close">Close</button>
         </div>
     </div>
 </div>
 <div class="mdc-dialog" id="room-dialog" role="alertdialog"
      aria-modal="true" aria-labelledby="my-dialog-title"
      aria-describedby="my-dialog-content" style="z-index: 9999;">
     <div class="mdc-dialog__container">
         <div class="mdc-dialog__surface">
             <h2 class="mdc-dialog__title" id="my-dialog-title">Join room</h2>
             <div class="mdc-dialog__content" id="my-dialog-content">
                 Enter ID for room to join:
                 <div class="mdc-text-field">
                     <input type="text" id="room-id" class="mdc-text-field__input">
                     <label class="mdc-floating-label" for="room-id">Room ID</label>
                     <div class="mdc-line-ripple"></div>
                 </div>
             </div>
             <footer class="mdc-dialog__actions">
                 <button type="button" class="mdc-button mdc-dialog__button" data-mdc-dialog-action="no">
                     <span class="mdc-button__label">Cancel</span>
                 </button>
                 <button id="confirmJoinBtn" type="button" class="mdc-button mdc-dialog__button" data-mdc-dialog-action="yes">
                     <span class="mdc-button__label">Join</span>
                 </button>
             </footer>
         </div>
     </div>
     <div class="mdc-dialog__scrim"></div>

 </div>

 @endsection
@section('additional_script')
    <script src="{{asset('webrtc/app.js')}}" async></script>
    <script src="{{asset('webrtc/deviceAccess.js')}}" async></script>
    <script>
        var firebaseConfig = {
            apiKey: "AIzaSyDNO0OA1pwI-WYCGJw5fi9gKXYZ_Lf274g",
            authDomain: "omedrtc.firebaseapp.com",
            databaseURL: "https://omedrtc-default-rtdb.asia-southeast1.firebasedatabase.app",
            projectId: "omedrtc",
            storageBucket: "omedrtc.appspot.com",
            messagingSenderId: "721463878307",
            appId: "1:721463878307:web:7302e7787319767c765516",
            measurementId: "G-95WQBSRRBY"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
    </script>
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>--}}
<script>
function isMobile() {
  var check = false;
  // (function(a){
  //   if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))
  //     check = true;
  //     alert("LIVE CONSULTATION FEATURE NOT AVAILABLE IN MOBILE BROWSER\n\nPLEASE DOWNLOAD OMEDDR APP ON PLAYSTORE");
  // })(navigator.userAgent||navigator.vendor||window.opera);
    (function(a,b){
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))
            b
    }
        )
    (navigator.userAgent||navigator.vendor||window.opera,alert("LIVE CONSULTATION FEATURE NOT AVAILABLE IN MOBILE BROWSER\n\nPLEASE DOWNLOAD OMEDDR APP ON PLAYSTORE"));
  // return check;

};

console.log('mobile detected:',isMobile());


        $(document).on("click", ".remote", function() {
            var rmt = $(this).attr("data");
            $(this).removeClass("remote");
            if (rmt == "remote") {
                $(".localVideo").addClass("remote");
            } else {
                $(".remoteVideo").addClass("remote");
            }
            console.log(rmt);
        });
        $(document).on("click", ".setting-button", function() {
            $(".setting-container").css({
                "left": "0px"
            });
        });
        $(document).on("click", ".setting-close", function() {
            $(".setting-container").css({
                "left": "-300px"
            });
        });
        $(document).on("click", ".video-container", function() {
            $(".setting-container").css({
                "left": "-300px"
            });
        });
    </script>

@endsection
