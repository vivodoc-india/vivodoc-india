@extends('layouts.layout')
@section('custom_style')
    <link rel="stylesheet" href="{{asset('webrtc/bootstrap_icons/fonts/bootstrap-icons.css')}}">
    <script src="https://www.gstatic.com/firebasejs/8.8.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.8.1/firebase-firestore.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('webrtc/main.css')}}">
    <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
@endsection
@section('content')
<section class="main">
    <div class="profile-view-section">
<div class="container-fluid">
<div class="container cp-md">
    <div class="card mb-2">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">Today's Total Consultation Request To {{isset($provider) ? $provider->user->name:''}}</label>
                    <br>
                    <span class="badge badge-success">{{isset($today_consultation)?$today_consultation:'NA'}}</span>
                </div>
                <div class="col-md-4">
                    <label class="control-label">Provider Availability</label>
                    <br>
                    <span class="badge {{isset($provider) && $provider->is_available==1?'badge-success':'badge-danger'}}">
                        {{isset($provider) && $provider->is_available==1?'Available':'Busy in Another Consultation'}}</span>
                </div>
                <div class="col-md-4">
                    @if(isset($mynumber))
                        <label class="control-label">Your Term</label>
                        <br>
                        <span class="badge badge-info">{{$mynumber}}</span>
                    @else
                        <label class="control-label">Your Term (If you want to consult)</label>
                        <br>
                        <span class="badge badge-info">{{isset($today_consultation)?($today_consultation + 1):'NA'}}</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
	<div class="card">
		<div class="card-body">
<input type="hidden" name="category_id" value="{{Session::has('category_id')?Session::get('category_id'):0}}">
			<div class="row">
				<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
					<img src="{{asset('web/v2/img/logo/logo.svg')}}" class="img-fluid">
                </div>
                @if(isset($provider))
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
					<div class="h3-responsive mb-2">{{$provider->user->name}}</div>
					<div class="mb-2">{{$provider->user->profile->speciality}}</div>
					{{-- <div class="mb-2">Specification</div> --}}
					<div class="mb-2">{{$provider->user->profile->qualification}}</div>
					<div class="mb-2">{{$provider->user->profile->address}}</div>
                </div>
                @endif

				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6">
                            <label class="control-label">Patient's Name <span class="req">*</span></label>
                            <div class="form-group">
                                <input type="text" class="form-control @error('patient_name') is-invalid @enderror" placeholder="Patient Name"
                                       name="patient_name"
                                       value="@if(isset($booking)){{$booking->patient_name}} @elseif(auth()->user()->name!=''){{auth()->user()->name}} @else {{old('patient_name')}} @endif" required>
                            </div>
                            @error('patient_name')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                                </div>
                                <div class="col-6">
                                    <label class="control-label">Mobile No. <span class="req"></span></label>
                                    <div class="form-group">
                                        <input type="text" class="form-control @error('mobile') is-invalid @enderror" placeholder="Mobile No."
                                               name="mobile" value="@if(isset($booking)){{$booking->mobile}} @elseif(auth()->user()->mobile!=''){{auth()->user()->mobile}} @else
                                        {{old('mobile')}}@endif" oninput="this.value=(parseInt(this.value)||'')" maxlength="10">
                                    </div>
                                    @error('mobile')
                                    <div class="alert alert-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="control-label">Patient's Age <span class="req">*</span></label>
                                    <div class="form-group">
                                        <input type="text" class="form-control @error('age') is-invalid @enderror"
                                               placeholder="Age" name="age" value="@if(isset($booking)){{$booking->age}} @else {{old('age')}}@endif">
                                    </div>
                                    @error('age')
                                    <div class="alert alert-danger">{{$message}}</div>
                                    @enderror
                                </div>

                                <div class="col-6">
                                    <label class="control-label">Patient's Gender <span class="req">*</span></label>
                                    <select class="form-control @error('gender') is-invalid @enderror" name="gender">
                                        <option selected disabled>Choose Gender</option>
                                        <option @if(isset($booking) && $booking->gender === "Male"){{'selected'}} @endif>Male</option>
                                        <option @if(isset($booking) && $booking->genedr === "Female"){{'selected'}} @endif>Female</option>
                                    </select>
                                    @error('gender')
                                    <br>
                                    <div class="alert alert-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>

                            <label class="control-label">Address <span class="req">*</span></label>
                            <div class="form-group">
                                <input type="text" class="form-control @error('address') is-invalid @enderror"
                                       placeholder="Address" name="address" value="@if(isset($booking)){{$booking->address}} @else{{old('address')}}@endif"
                                >
                            </div>
                            @error('address')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
				</div>
			</div>
			<hr>
<div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <span class="text-danger" id="btn-more-info" style="font-weight: bold;"> Vitals & Other Details <i class="far fa-caret-square-down"></i></span>
        </div>
    </div>

            <div id="more-info">
                <div class="row mt-4">
                    <div class="col-md-4 col-12">
                        <label class="control-label">Name (Clinic/BikeDr)</label>
                        <input type="text" class="form-control @error('bike_dr') is-invalid @enderror" placeholder="Clinic/BikeDr" name="bike_dr"
                               value="@if(isset($booking)){{$booking->bike_dr}}@else{{old('bike_dr')}}@endif">

                        @error('bikedr')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
{{--                </div>--}}
{{--                <div class="row">--}}
                    <div class="col-md-4 col-12">
                        <label class="control-label">Patient's BP</label>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="BP" name="bp"
                                   value="@if(isset($booking)){{$booking->bp}}@elseif(isset($vitals) && $vitals!=''){{$vitals->sys.'/'.$vitals->dia}}@else{{old('bp')}}@endif">
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <label class="control-label">Patient's Pulse</label>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Pulse" name="pulse"
                                   value="@if(isset($booking)){{$booking->pulse}}@elseif(isset($vitals) && $vitals!='')
                                   {{$vitals->pulse}}@else {{old('pulse')}}@endif">
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <label class="control-label">Patient's Temperature</label>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Temperature" name="temperature"
                                   value="@if(isset($booking)){{$booking->temperature}}@elseif(isset($vitals) && $vitals!='')
                                   {{$vitals->temperature}}@else {{old('temperature')}}@endif">
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <label class="control-label">Patient's SPO<sub>2</sub></label>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="SPO2" name="spo"
                                   value="@if(isset($booking)){{$booking->spo}}@elseif(isset($vitals) && $vitals!='')
                                   {{$vitals->spo}} @else {{old('spo')}}@endif">
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <label class="control-label">Patient's Weight</label>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Weight" name="weight"
                                   value="@if(isset($booking)){{$booking->weight}}@else {{old('weight')}}@endif">
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <label class="control-label">History</label>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="History" name="history"
                                   value="@if(isset($booking)){{$booking->history}}@else{{old('history')}}@endif">
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <label class="control-label">Complain</label>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Complain" name="complain"
                                   value="@if(isset($booking)){{$booking->complain}}@else {{old('complain')}}@endif">
                        </div>
                    </div>
                </div>
            </div>

			<input type="hidden" name="provider_id" value="@if(isset($provider)){{$provider->user->id}}@endif">
			<input type="hidden" name="service_profile_id" value="@if(isset($provider)){{$provider->id}}@endif">
            <input type="hidden" name="patient_id" value="{{Auth::user()->id}}">

            <hr>
            <div class="row mt-2">
                <div class="col-lg-8 col-md-8 col-sm-12 mb-2">
                    <p id="def-msg">
                        @if(isset($booking) && !empty($booking) && $booking->request_status < 3)
                            {{'Request initiated. Please pay the consultation fee.'}}
                        @endif
                    </p>

                </div>
            </div>

            <div class="row mt-2">
                <div class="col-lg-4 col-md-4 col-sm-12 mb-4">
                    @if(isset($booking) && $booking->request_status < 3)
                    <div class="card">
                        <div class="card-header">
                            <h6>Payment Summery</h6>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8 col-12 col-sm-12">
                                    <label class="control-label">Consultation Fee :</label>
                                </div>
                                <div class="col-md-4 col-12 col-sm-12">
                                    <span> Rs. {{$provider->min_fee}}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-12 col-sm-12">
                                    <label class="control-label">OMED Coins :</label>
                                </div>
                                <div class="col-md-4 col-12 col-sm-12">
                                    <span class="{{auth()->user()->wallet_amount > 0 ?'text-success':'text-danger'}}"> {{auth()->user()->wallet_amount}}</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-8 col-12 col-sm-12">
                                    <label class="control-label">Payable Amount :</label>
                                </div>
                                <div class="col-md-4 col-12 col-sm-12">
                                    <span> Rs. {{$provider->min_fee}}</span>
                                </div>
                            </div>
                            <hr>
                            <form id="frm-pay" action="@if(isset($booking)){{url('patient/pay-consultation-fee')}}@endif"
                                  method="post" class="@if(empty($booking)){{'hide'}}@endif">
                                @csrf
                                @if(isset($booking) && !empty($booking))
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox"
                                                       id="gridCheck1" name="wallet_pay" value="1"
                                                    {{auth()->user()->wallet_amount <= 0 ?'disabled':'checked="checked"'}}>
                                                <label class="form-check-label control-label" for="gridCheck1">
                                                    Pay using OMED Coins
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="booking" value="{{base64_encode($booking->id)}}">
                                    <button type="submit" class="btn btn-success @if($booking->request_status>=3)hide @endif
                                        " id="btn-pay"> Pay Now</button>
                                @endif
                            </form>
                        </div>
                    </div>
                    @endif


                    @if(isset($booking) && !empty($booking))
                        @if($booking->is_con_link_expired == 1)
                            <h5 class="text-danger"> Consultation link is not available.</h5>
                            @endif
                            @if($booking->is_con_link_available == 1)
                            @if($booking->serviceProfile->btn_video==1)
                            <button type="button" class="btn btn-info" id="btn-join-confrence">
                                <i class="fas fa-video text-white"></i> Consult Now
                            </button>
                            @endif
                        @if($booking->serviceProfile->btn_call==1)
                            <a href="tel:{{$booking->provider->mobile}}" class="btn btn-success"><i class="fas fa-phone-alt"></i> Call Now</a>
                            @endif
                            @endif
                        @if($booking->prescription!='' || $booking->prescription_image!='')
                                <a href="{{url('patient/view-consultation/'.$booking->id)}}" class="btn btn-success"><i class="fas fa-eye text-white"></i> View Prescription</a>
                            @else
                            @if($booking->doc_consulted)
                                <p>Kindly wait for doctor to upload your prescription after video consultation. You can view your prescription in your dashboard's <a href="{{url('patient/consultation-booking')}}">Live Consultation</a> option / refresh this page after a few minutes.</p>
                            @endif
                            @endif
                    @endif
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-12 @if(isset($booking) && $booking->request_status >= 3) {{'hide'}} @endif ">
                    <div class="row float-right">
                        <div class="col-md-10 col-sm-12 col-12">
                            <p>After sending request, wait a few seconds for payment link.<br> Need Help? </p>
                        </div>
                        <div class="col-md-2 col-sm-12 col-12">
                            <a href="tel:{{$help_support}}" class="text-white mt-3">
                                <div style="background-color: #00C851; height:50px; width: 50px;text-align: center; border-radius: 50px;">
                                    <i class="fas fa-phone-alt" style="margin-top: 19px;"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
                                 <div id="lb-card" style="width:100%; height:500px;display:none;">
                        @if(isset($booking) && $booking && $booking->is_con_link_available==1)
                            <div class="main-container cp-md">
                                <div id="videos" class="video-container">
                                    <video id="localVideo" class="localVideo" data="local" muted autoplay playsinline></video>
                                    <video id="remoteVideo" class="remoteVideo remote" data="remote" autoplay playsinline></video>
                                    <div class="live-alert" id="alert-notification" style="display:none;">
                                        <div class="live-alert-header">
                                        </div>
                                        <div class="live-alert-body">
                                            <div class="live-alert-message">
                                                <p id="alert-msg"></p>
                                            </div>
                                            <div class="live-alert-action">
                                                <button class="live-alert-action-button btn btn-danger btn-sm">OK</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-button-container">
                                    @if($booking->caller==auth()->user()->id)
                                        <div class="start-button">
                                            <button type="button" class="btn btn-sm btn-info" disabled id="createBtn">
                                                <!--<i class="bi bi-telephone-fill"></i>-->
                                                Join
                                            </button>
                                        </div>
                                    @elseif(empty($booking->caller))
                                        <div class="start-button">
                                            <button type="button" class="btn btn-sm btn-info" disabled id="createBtn">
                                                <!--<i class="bi bi-telephone-fill"></i>-->
                                                Join
                                            </button>
                                        </div>
                                    @else
                                        <div class="start-button">
                                            <button type="button" class="btn btn-sm btn-info" disabled id="joinBtn">
                                                <!--<i class="bi bi-telephone-fill"></i>-->
                                                Join
                                            </button>
                                        </div>
                                    @endif
                                    <div class="mic-button">
                                        <button type="button" class="btn btn-sm" id="disableAudio">
                                            <i class="bi bi-mic"></i>
                                        </button>
                                    </div>
                                    <div class="end-button">
                                        <button type="button" class="btn btn-sm" disabled id="hangupBtn">
                                            <i class="bi bi-telephone-fill"></i>
                                        </button>
                                    </div>
                                    <div class="video-button">
                                        <button type="button" class="btn btn-sm" id="disableVideo">
                                            <i class="bi bi-camera-video"></i>
                                        </button></div>
                                </div>
                                <div class="setting-button">
                                    <div class="sett-button"><i class="bi bi-gear"></i></div>
                                </div>
                                <div class="full-button">
                                    <i class="bi bi-fullscreen"></i>
                                </div>
                                <div class="setting-container">
                                    <div class="select">
                                        <label for="audioSource">Audio input source: </label>
                                        <select id="audioSource" class="form-control"></select>
                                    </div>

                                    <div class="select">
                                        <label for="audioOutput">Audio output destination: </label>
                                        <select id="audioOutput" class="form-control"></select>
                                    </div>

                                    <div class="select">
                                        <label for="videoSource">Video source: </label>
                                        <select id="videoSource" class="form-control"></select>
                                    </div>

                                    <div class="close-button">
                                        <button class="setting-close">Close</button>
                                    </div>
                                </div>
                            </div>

                        @endif
                    </div>
                </div>
            </div>

		</div>
	</div>
</div>
    </div>
    </div>
</section>
@endsection
@section('additional_script')
    <script>
        $('document').ready(function(){
            $('#btn-more-info').click(function(){
                $('#more-info').toggle();
            });
            $('#btn-join-confrence').click(function(){
                $('#lb-card').toggle();
            });
            @if(isset($booking) && !empty($booking))
{{--                @if($booking->patient_requested == 1 && $booking->is_request_rejected==0 && $booking->is_request_accepted==0)--}}
{{--            bookingRequestStat({{$booking->id}});--}}
{{--                @endif--}}
            checkPrescription({{$booking->id}});
                @endif
            // $('#btn-video-con-request').click(function(){
            //
            //     var data=$('#frm-video-consult').serialize();
            //     var url = $('#frm-video-consult').attr('action');
            //
            //     $.ajax({
            //         type:'ajax',
            //         method:'post',
            //         data:data,
            //         url:url,
            //         asycn:true,
            //         dataType:'JSON',
            //         success:function(response){
            //             if(response.success){
            //                 alertify.set('notifier','position', 'top-right');
            //                 alertify.success(response.message);
            //                 $('#btn-video-con-request').hide();
            //                 $('#def-msg').text('Request Sent to the service provider. Please wait for his/her response.').show();
            //
            //                 bookingRequestStat(response.resp_code);
            //             }
            //             else{
            //                 alertify.set('notifier','position', 'top-right');
            //                 alertify.error(response.message);
            //             }
            //         },
            //         error:function(){
            //             alertify.set('notifier','position', 'top-right');
            //             alertify.error('Unable to process your request.');
            //         }
            //     });
            // });

            $('#btn-pay').click(function(){
               $('#frm-pay').submit();
                // window.open($(this).attr('data'), '_blank');
            });
        });

        function bookingRequestStat(res_code) {
            interval = setInterval( function() {
              $.ajax({
                  type:'ajax',
                  method:'post',
                  data:{
                      '_token':"{{ csrf_token() }}",
                      'res_code' : res_code,
                      'uid': "{{Auth::user()->id}}",

                  },
                  url:"{{url('checkBookingResponse')}}",
                  dataType:'json',
                  asycn:true,
                  success:function(response){
                      if(response.success==="true")
                      {
                          clearInterval(interval);
                          $('#frm-pay,#btn-pay').show();
                          location.reload();
                          // $('#btn-call').show().attr('data',response.link);
                      }
                      else{
                          if(response.stop)
                          {
                              clearInterval(interval);
                              $('#frm-pay,#btn-pay').hide();
                              location.reload();
                              // $('#btn-call').hide().removeAttr('data');
                          }
                          else{
                              bookingRequestStat(response.res_code);
                          }
                      }
                  }
              });
            }, 1000*20);
        }
        function checkPrescription(res_code)
        {
            interval = setInterval( function() {
                $.ajax({
                    type:'ajax',
                    method:'post',
                    data:{
                        '_token':"{{ csrf_token() }}",
                        'res_code' : res_code,
                        'uid': "{{Auth::user()->id}}",
                    },
                    url:"{{url('checkPrescription')}}",
                    dataType:'json',
                    asycn:true,
                    success:function(response){
                        if(response.success)
                        {
                            clearInterval(interval);
                            location.reload();

                        }
                        else{
                            if(response.stop)
                            {
                                clearInterval(interval);
                                location.reload();
                            }
                            else{
                                checkPrescription(res_code);
                            }
                        }
                    }
                });
            }, 1000*60);
        }
    </script>
    <script src="{{asset('webrtc/app.js')}}" async></script>
    <script src="{{asset('webrtc/deviceAccess.js')}}" async></script>
    <script src="{{asset('webrtc/main.js')}}" async></script>
    <script>
        var firebaseConfig = {
            apiKey: "AIzaSyDAo3iV_T3C0njiXXzIA0kTb6dJyV2mEaU",
            authDomain: "omed-d39e0.firebaseapp.com",
            projectId: "omed-d39e0",
            databaseURL:"https://omed-d39e0-default-rtdb.asia-southeast1.firebasedatabase.app",
            storageBucket: "omed-d39e0.appspot.com",
            messagingSenderId: "139501672892",
            appId: "1:139501672892:web:bc8346561f16b42a9333b9",
            measurementId: "G-KHRPJ0SZPV"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
    </script>
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>--}}
    <script>


        // Don't Touch this
@if(isset($booking))
    window.onbeforeunload = function() {
    return hangCall();
}
        function initiateCall(res_data)
        {
            $.ajax({
                type:'ajax',
                method:'post',
                data:{
                    '_token':"{{ csrf_token() }}",
                    'id':res_data,
                    'caller':"{{auth()->user()->id}}",
                    'res_code':"{{$booking->id}}",
                    'ident':"{{auth()->user()->id}}",
                },
                url:"{{url('patient/initiate-call')}}",
                dataType:'json',
                async:true,
                success:function(response){
                    if(response.success)
                    {
                        if(response.roomExists)
                        {
                            joinRoomById(response.id);
                            showAlert('Please wait ...');
                            $('#hangupBtn').addClass("btn-danger");
                        }
                        showAlert('Please wait ...');
                    }
                    else{
                        showAlert('Unable to initiate call');
                        hangUp();
                    }
                }
            });


        }

       async function joinRoom(){
            await joinRoomById("{{isset($booking)?$booking->con_link:''}}");
            $('#hangupBtn').addClass("btn-danger");
        }

        function showAlert(message){
            $('#alert-notification').show();
            $('#alert-msg').html(message);
}

function askForCall(){
            // $('#alert-notification').show();
            // $('#alert-msg').html('Please click on <button type="button" class="btn btn-sm btn-success" style="height:30px; width: 30px;border-radius: 50px; padding:0px;margin:0px;">'+'<i class="bi bi-telephone-fill"></i>'+'</button> button');
}
function audioMuted(){
    $('#disableAudio').html('<i class="bi bi-mic-mute"></i>');
    $('#disableAudio').addClass("btn-info");
}
function audioUnMuted(){
            $('#disableAudio').html('<i class="bi bi-mic"></i>');
            $('#disableAudio').removeClass("btn-info");
}
        function videoMuted(){
            $('#disableVideo').html('<i class="bi bi-camera-video-off"></i>');
            $('#disableVideo').addClass("btn-success");
        }
        function videoUnMuted(){
            $('#disableVideo').html('<i class="bi bi-camera-video"></i>');
            $('#disableVideo').removeClass("btn-success");
        }
        function hangCall(){
            $.ajax({
                type:'ajax',
                method:'post',
                data:{
                    '_token':"{{ csrf_token() }}",
                    'res_code':"{{$booking->id}}",
                    'ident':"{{auth()->user()->id}}",
                },
                url:"{{url('patient/hang-call')}}",
                dataType:'json',
                async:true,
                success:function(response){
                    if(response.success)
                    {
                        hangUp();
                    }
                    else{
                        showAlert('Something went wrong.');
                    }
                }
            });
        }
        @endif
    </script>


    @endsection
