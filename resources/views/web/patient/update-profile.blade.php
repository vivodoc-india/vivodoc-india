@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
                
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 side-bg">
                                @include('web.left-menu')
                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 pb-4 pt-4">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active  btn-block radius-0" id="pills-home-tab"
                        data-toggle="pill" href="#pills-personal" role="tab" aria-controls="pills-home"
                        aria-selected="true">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                    Personal Info</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link radius-0  btn-block" id="pills-contact-tab btn-primary"
                        data-toggle="pill" href="#pills-doc" role="tab" aria-controls="pills-contact"
                        aria-selected="false">
                            <i class="fas fa-cloud-upload-alt"></i> Photo Upload</a>
                    </li>
                        <li class="nav-item">
                            <a class="nav-link radius-0  btn-block" id="pills-contact-tab btn-primary" data-toggle="pill" href="#pills-password" role="tab" aria-controls="pills-password" aria-selected="false">
                                <i class="fas fa-key"></i> Change Password</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <!-- Personal Info Body -->
                    <div class="tab-pane fade show active" id="pills-personal" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="card">
                                    <div class="card-body">
                                    <form action="{{url('patient/updateProfileInfo')}}" method="post" id="frm-personal">
                                        @csrf
                            <h3 class="mb-3">Personal Info</h3>
                                        <div class="row mb-3">
                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                                                <label class="control-label">Name <span class="req">*</span></label>
                                                <div class="form-group">
                                                    <input type="text" name="name"  placeholder="name"
                                                        class="form-control border-round  rounded-0 @error('name') is-invalid @enderror"
                                                        required value=" @if(isset($user->name)){{$user->name}}@endif">
                                                </div>
                                                @error('name')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                                                <label class="control-label">Mobile </label>
                                                <div class="form-group">
                                                    <input type="text" name="mobile"  placeholder="mobile"
                                                        class="form-control border-round  rounded-0 @error('mobile') is-invalid @enderror"
                                                        value=" @if(isset($user->mobile)){{$user->mobile}}@endif">
                                                </div>
                                                @error('mobile')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                                                <label class="control-label">Email Id </label>
                                                <div class="form-group">
                                                    <input name="email"  type="email" placeholder="email"
                                                        class="form-control border-round  rounded-0 @error('email') is-invalid @enderror"
                                                        value=" @if(isset($user->email)){{$user->email}}@endif">
                                                </div>
                                                @error('email')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-3">
                                                <label class="control-label">Gender</label><br>
                                                <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input  @error('gender') is-invalid @enderror" required id="customRadio" name="gender" value="Male"
                        @if(isset($user->profile->gender) && $user->profile->gender =="Male"){{'checked'}}@endif>
                        <label class="custom-control-label" for="customRadio">Male</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input  @error('gender') is-invalid @enderror" id="customRadio2" name="gender" value="Female"
                        @if(isset($user->profile->gender) && $user->profile->gender=="Female"){{'checked'}}@endif>
                        <label class="custom-control-label" for="customRadio2">Female</label>
                    </div>
                    @error('gender')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                                            </div>
                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                <label class="control-label">Landmark <span class="req">*</span></label>
                                                <div class="form-group">
                                                <input type="text" name="landmark" class="form-control rounded-0 border-round @error('landmark') is-invalid @enderror" placeholder="landmark" required value="@if(isset($user->profile->landmark)){{$user->profile->landmark}}@endif">
                                                </div>
                                                @error('landmark')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                <label class="control-label">Area <span class="req">*</span></label>
                                                <div class="form-group">
                                                <input type="text" name="area" class="form-control rounded-0 border-round @error('area') is-invalid @enderror" placeholder="area" required value="@if(isset($user->profile->area)){{$user->profile->area}}@endif">
                                                </div>
                                            @error('area')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                                </div>

                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                <label class="control-label">City <span class="req">*</span></label>
                                                <div class="form-group">
                                                <input type="text" name="city" class="form-control rounded-0 border-round @error('city') is-invalid @enderror" placeholder="city" required value="@if(isset($user->profile->city)){{$user->profile->city}}@endif">
                                                    @error('city')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                <label class="control-label">State <span class="req">*</span></label>
                                                <div class="form-group">
                                                <input type="text" name="state" class="form-control rounded-0 border-round @error('state') is-invalid @enderror" placeholder="state" required value="@if(isset($user->profile->state)){{$user->profile->state}}@endif">
                                                </div>
                                                @error('state')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                                            </div>
                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                <label class="control-label">Pincode <span class="req">*</span></label>
                                                <div class="form-group">
                                                <input type="text" name="pincode" class="form-control rounded-0 border-round @error('pincode') is-invalid @enderror" placeholder="pincode" required oninput='this.value=(parseInt(this.value)||" ")' maxlength="6" value="@if(isset($user->profile->pincode)){{$user->profile->pincode}}@endif">
                                                </div>
                                                @error('pincode')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                                            </div>
                                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                                <label class="control-label">Blood Group </label>
                                                <div class="form-group">
                                                <select class="form-control @error ('blood_groop') is-invalid @enderror" name="blood_group" >
                                                    <option selected disabled>Choose your blood group</option>
                                                    <option value="A+"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="A+"){{'selected'}}@endif>A+</option>
                                                    <option value="A-"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="A-"){{'selected'}}@endif>A-</option>
                                                    <option value="B+"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="B+"){{'selected'}}@endif>B+</option>
                                                    <option value="B-"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="B-"){{'selected'}}@endif>B-</option>
                                                    <option value="AB+"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="AB+"){{'selected'}}@endif>AB+</option>
                                                    <option value="AB-"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="AB-"){{'selected'}}@endif>AB-</option>
                                                    <option value="O+"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="O+"){{'selected'}}@endif>O+</option>
                                                    <option value="O-"@if(isset($user->profile->blood_group) && $user->profile->blood_group=="O-"){{'selected'}}@endif>O-</option>





                                                </select>
                                                </div>
                                                @error('blood_group')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <label class="control-label">Address <span class="req">*</span></label>
                                                <div class="form-group">
                                                <textarea name="address" required class="form-control rounded-0 @error('address') is-invalid @enderror"  placeholder="Address">@if(isset($user->profile->address)){{$user->profile->address}}@endif</textarea>

                                                </div>
                                                @error('address')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
				</div>
			</div>
                    <button type="submit" class="btn btn-read-more" id="btn-save-personal"><i class="fa fa-user-circle" aria-hidden="true"></i> Save</button>
                </form>
            </div>
      </div>
  </div>
  <!-- Personal Info Body Ends Here -->
    <!-- Document Upload Body-->
  <div class="tab-pane fade" id="pills-doc" role="tabpanel" aria-labelledby="pills-contact-tab">
<div class="card">
<div class="card-body">
<form action="{{url('patient/updateDocuments')}}" method="post" id="frm-document" enctype="multipart/form-data">
@csrf
<h3 class="mb-3">Upload Profile Photo</h3>
<div class="row">
  <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
<label class="control-label ml-2">Upload Your Photo</label>
 <div class="custom-file">
    <input type="file" class="custom-file-input  @error('photo') is-invalid @enderror" id="picFile" name="photo" reuqired>
    <label class="custom-file-label" for="picFile">Choose file</label>
  </div>
  <span class="misc">Allowed file type: jpg, jpeg, png.</span>
   @error('photo')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">

                            @if(isset($user->profile->photo) && $user->profile->photo !='')
<a href=""><img src="{{asset('uploads/patientprofile/'.$user->profile->photo)}}" class="img-fluid img-thumbnail"></a>
                            @else
<a href=""><img src="{{asset('web/img/nobody.jpg')}}" class="img-fluid img-thumbnail"></a>
                            @endif

						</div>

</div>
 <button type="submit" class="btn btn-read-more mt-3" id="btn-save-document"><i class="fas fa-cloud-upload-alt "></i> Save</button>
</form>
    </div>
</div>

  </div>
  <!-- Document Upload Body Ends Here -->
    <!-- Change Password -->
    <div class="tab-pane fade" id="pills-password" role="tabpanel" aria-labelledby="pills-contact-tab">
        <div class="card">
            <div class="card-body">
                <form action="{{url('patient/changePassword')}}" method="post" id="frm-password">
                    @csrf
                    <h3 class="mb-3">Change Password</h3>
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 offset-md-3">
                            <label class="control-label">Current Password <span class="req">*</span></label>
                            <div class="form-group">
                                <input type="text" class="form-control rounded-0 border-round @error('current_password') is-invalid @enderror"
                                       placeholder="Enter your current password"
                                       name="current_password" required
                                       value="">
                            </div>
                            @error('current_password')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 offset-md-3">
                            <label class="control-label">New Password <span class="req">*</span></label>
                            <div class="form-group">
                                <input type="password" class="form-control rounded-0 border-round @error('new_password') is-invalid @enderror"
                                       placeholder="Enter your new password"
                                       name="new_password" required
                                       value="">
                            </div>
                            @error('new_password')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 offset-md-3">
                            <label class="control-label">Re-enter New Password <span class="req">*</span></label>
                            <div class="form-group">
                                <input type="password" class="form-control rounded-0 border-round @error('password_confirmation') is-invalid @enderror"
                                       placeholder="Re-nter your new password"
                                       name="password_confirmation" required
                                       value="" >
                            </div>
                            @error('password_confirmation')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-read-more mt-3" id="btn-save-document">
                        <i class="fas fa-key "></i> Change Password</button>
                </form>
            </div>
        </div>

    </div>
    <!--Change Password Body Ends Here -->
  </div>

</div>
			</div>
		</div>
        </div>
    </section>
@endsection
