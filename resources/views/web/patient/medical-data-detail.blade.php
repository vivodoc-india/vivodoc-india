@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
    <div class="container-fluid cp-md">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">
                @include('web.left-menu')
            </div>
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">

                <div class="card">
                    <div class="card-body">

                            <div class="card mb-3">
                                <div class="card-body">
                                    <form action="{{url('patient/add-more-records')}}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="parent" value="{{$parent}}">
                                        <div class="row">
                                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                               <div class="h4-responsive text-center "> Add More Documents</div>
                                            </div>
                                            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input  @error('docs') is-invalid @enderror" id="File" name="docs[]"  multiple required>
                                                    <label class="custom-file-label" for="File" >Choose file</label>
                                                </div>
                                                <span class="misc">Allowed file type: jpg, jpeg, png. Use <span class="badge badge-secondary">'CTRL'</span> for multiple files selection.</span>
                                                @error('docs')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 ">
                                                <button type="submit" class="btn btn-dark btn-sm rounded-pill">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @if(isset($record) && $record->isNotEmpty())
                        <div class="row">
@foreach($record as $rec)
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-3">
                                <!-- Card -->
                                    <div class="card h-100">
                                        <!-- Card image -->
                                        <a href="{{asset($rec->file)}}" target="_blank">
           <img class="card-img-top" src="{{asset($rec->file)}}" alt="{{$rec->file}}" height="200">
                                        </a>
                                        <!-- Card content -->
                                        <div class="card-body">
                                            <label class="control-label">Created On: {{date('d-m-Y h:i:s',strtotime($rec->created_at))}}</label>
                        <form action="{{url('patient/remove-medical-data')}}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{$rec->id}}">
                            <button type="submit" class="btn btn-outline-danger waves-effect"><i class="far fa-trash-alt"></i> Remove</button>
                        </form>
                                        </div>
                                    </div>
                            </div>
                                @endforeach
                                    <!-- Card -->
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    </section>
@endsection
