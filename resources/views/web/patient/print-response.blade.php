<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{$title}}</title>
    <style>
        .text-danger
        {
            color:#ff0000;
        }
        @media print {
            @page {
                margin: 0;
                size: A4;
            }
            body, div{
                font-size:14px;
            }
            #btn-print{
                display:none;
            }
            #pres-img
            {
                height:500px;
            }
            .text-danger
            {
                color:#ff0000;
            }
            #presc-img{
                height:600px!important;
            }
        }

    </style>
</head>
<body bgcolor="#abd3e6" style="font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';">
<div style="max-width: 1024px; height: auto; margin: auto; background: #ffffff;">
    <table width="100%" cellpadding="10">
        <tr>
            <td>
                <table width="100%">
                    <tr valign="top">
                        <td >
{{--                            <div style="font-size: 22px; margin-bottom:10px;">Doctor's Profile</div>--}}
                            <div style="font-size: 22px; margin-bottom:10px;">{{$booking->provider->name}}</div>
                            <div style="margin-bottom:5px;">{{$booking->provider->profile->speciality}}</div>
                            <div style="margin-bottom:5px;">{{$booking->provider->profile->qualification}}</div>
                            <div style="margin-bottom:5px;">{{$booking->provider->profile->other_speciality}}</div>
                            <div style="margin-bottom:5px;">{{$booking->provider->profile->address}}</div>
                            <div style="margin-bottom:5px;">{{$booking->serviceProfile->reg_no}}</div>
                        </td>

                        <td align="center">
                            <div style="margin-bottom: 7px;"><img src="{{asset('web/v2/img/logo/logo.svg')}}" width="300" alt=""></div>
                            <div>{{$booking->omed_code}} @if($booking->bikedr!=''){{'/ '.$booking->bikedr}} @endif</div>
                        </td>
                        <td align="right">
                            <div style="font-size: 22px; margin-bottom:10px;">Patient Detail  </div>
                            <div style="margin-bottom:5px; font-weight:bold;">{{ucwords($booking->patient_name)}}</div>
                            <div style="margin-bottom:5px;">Age : {{$booking->age}}</div>
                            <div style="margin-bottom:5px;">Gender : {{$booking->gender}}</div>
                            <div style="margin-bottom:5px;">Mobile No. : {{$booking->mobile}}</div>
                            <div style="margin-bottom:5px;">Address : {{$booking->address}}</div>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <hr width="100%" size="1" color="#ABABAB">
{{--                <hr width="100%" size="1" color="#ABABAB">--}}
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="1" cellpadding="10" cellspacing="0" bordercolor="#ababab">
                    <thead>
                    <tr>
                        <th>BP</th>
                        <th>Pulse</th>
                        <th>Temp.</th>
                        <th>SPO2</th>
                        <th>Weight</th>
                        <th>History</th>
                        <th width="15%">Complain</th>
                        <th>P. Diagnosis</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$booking->bp}}</td>
                        <td>{{$booking->pulse}}</td>
                        <td>{{$booking->temperature}}</td>
                        <td>{{$booking->spo}}</td>
                        <td>{{$booking->weight}}</td>
                        <td>{{$booking->history}}</td>
                        <td>{{$booking->complain}}</td>
                        <td>{{$booking->diagnostic}}</td>
                    </tr>

                    </tbody>
                </table>
                <p>
                <span class="text-danger">Disclaimer :</span> Doctor generated this prescription Online based on your inputs. If you get any problem please visit your Doctor.</p>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <div style="min-height:300px; padding: 5px;" id="pres-img">
                                @if(!empty($booking->prescription))
                                    {!! $booking->prescription !!}
                                @endif
                                @if(!empty($booking->prescription_image))
      <img src="{{asset('uploads/patientprofile/prescription/'.$booking->prescription_folder.'/'.$booking->prescription_image)}}" style="width:768px;height:768px;" id="presc-img">
                                @endif
                                    @if(empty($booking->prescription) && empty($booking->prescription_image))
                                    {{'Uploaded Note & Prescriptions'}}
                                @endif
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>

        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td></td></tr>


        <tr>
            <td align="center"><button type="button" style="width:100px; height:40px; font-size: 14px; cursor: pointer;" id="btn-print" onclick="window.print();">Print</button></td>
        </tr>
    </table>
</div>

</body>
</html>
