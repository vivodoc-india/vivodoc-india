@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 side-bg">
                @include("web.left-menu")
            </div>
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 pb-4 pt-4">
                <div class="card">
                    <div class="card-header bg-white">Appoinment Edit</div>
                    <div class="card-body">
                        <form action="{{url('save-appoinment')}}" method="post">
                            @csrf
                            <div class="row mb-3">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <label class="control-label">Booking Date <span class="req">*</span></label><br>
                                    <div class="form-group">
                                        <input name="booking_date" value="{{old('booking_date')}}" type="date" placeholder="dd/mm/yyyy" class="form-control border-round  rounded-0 @error('booking_date') is-invalid @enderror" required id="booking_date">
                                    </div>
                                    @error('booking_date')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <label class="control-label">Patient's Gender <span class="req">*</span></label><br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input  @error('gender') is-invalid @enderror" required id="customRadio"
                                               name="gender" value="Male">
                                        <label class="custom-control-label" for="customRadio">Male</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input  @error('gender') is-invalid @enderror"
                                               id="customRadio2" name="gender" value="Female" >
                                        <label class="custom-control-label" for="customRadio2">Female</label>
                                    </div>
                                    @error('gender')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>


                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Patient Name <span class="req">*</span></label>
                                    <div class="form-group">
                                        <input name="patient_name" type="text"  placeholder="patient name"
                                               class="form-control border-round  rounded-0 @error('patient_name')
                                                   is-invalid @enderror" required value="{{old('patient_name')}}" >
                                    </div>
                                    @error('sepciality')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Patient Age <span class="req">*</span></label><br>
                                    <div class="form-group">
                                        <input name="age" type="text" placeholder="patient age"
                                               class="form-control border-round  rounded-0 @error('age') is-invalid @enderror"
                                               required value="{{old('age')}}">
                                    </div>
                                    @error('age')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Patient's Mobile </label>
                                    <div class="form-group">

                                        <input name="mobile"  type="text" placeholder="Enter mobile no."
                                               class="form-control border-round  rounded-0 @error('mobile')
                                                   is-invalid @enderror" id="mobile" value="{{old('mobile')
                                                   }}" oninput='this.value=(parseInt(this.value)||" ")'>
                                    </div>
                                    @error('mobile')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <label class="control-label">Patient Address <span class="req">*</span></label><br>
                                    <div class="form-group">
                                        <input name="address" type="text" placeholder="address"
                                               class="form-control border-round  rounded-0 @error('address') is-invalid @enderror"
                                               required value="{{old('address')}}">
                                    </div>
                                    @error('address')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Diagnosis </label><br>
                                    <div class="form-group">
                                        <input name="diagnosis" type="text" placeholder="diagnosis"
                                               class="form-control border-round  rounded-0 @error('diagnosis') is-invalid @enderror"
                                               value="{{old('diagnosis')}}">
                                    </div>
                                    @error('diagnosis')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Vitals </label><br>
                                    <div class="form-group">
                                        <input name="vital" type="text" placeholder="i.e. Height,Weight, BP, Pulse, Temp."
                                               class="form-control border-round  rounded-0 @error('vital') is-invalid @enderror"
                                               value="{{old('vital')}}">
                                    </div>
                                    @error('vital')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Complain/History </label><br>
                                    <div class="form-group">
                                        <input name="complain" type="text" placeholder="complain/history"
                                               class="form-control border-round  rounded-0 @error('complain') is-invalid @enderror"
                                               value="{{old('complain')}}">
                                    </div>
                                    @error('complain')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                            </div>
                            <input type="hidden" name="provider_id" value="@if(isset($provider)){{$provider}}@endif" id="provider_id">
                            <input type="hidden" name="service_profile_id" value="@if(isset($service_profile)){{$service_profile}}@endif" id="sp_id">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <button type="submit" class="btn btn-success">Book Now</button>
                            </div>
                        </form>

                    </div>

                </div>


            </div>
        </div>
    </div>
        </div>
    </section>
@endsection
@section('additional_script')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="{{asset('web/rating/bootstrap-rating.js')}}"></script>
    <script>
        $(function () {

            $('.rating').on('change', function () {
                // $(this).next('.label').text($(this).val());
                $('#rv').val($(this).val());
            });
        });
    </script>
@endsection

