@extends('layouts.daslayout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">OMED Coins</div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Purchase OMED Coins</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="{{route('patient.wallet-recharge')}}" method="post">
                        @csrf
                <div class="form-group">
                    <label class="control-label">Amount</label>
                    <input type="text" name="amount" class="form-control"
                    placeholder="Enter amount" min="1" required
                           oninput='this.value=(parseInt(this.value)||" ")'>
<span class="text-muted text-sm-left">1 OMED Coin =  ₹ 1</span>
                 <div class="mt-5 text-right">
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                    <button type="submit" class="btn btn-primary">Proceed</button>
                 </div>
                </div>
                    </form>
                </div>
{{--                <div class="modal-footer">--}}
{{--                   --}}
{{--                </div>--}}

            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row ">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 side-bg">
                @include('web.left-menu')
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 mb-2">
                <div class="row mt-1">
                    <div class="col-md-12 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 text-center">
                                    <i class="fas fa-wallet fa-2x text-primary"></i> <span style="font-size: 25px;" class="text-primary">{{auth()->user()->wallet_amount}}</span>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button type="button" id="btn-add-money"
                                            class="btn btn-warning" data-toggle="modal" data-target="#exampleModalCenter">
                                        <i class="fas fa-wallet"></i> Purchase OMED Coins</button>

                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="example" class="display responsive nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>S.no.</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $n=1 @endphp
                                @foreach($transaction as $trans)
                                    <tr>
                                        <td>{{$n}}</td>
                                        <td>{{date('d-m-Y',strtotime($trans->transaction_date))}}</td>
                                        <td>
                                            @if($trans->transaction_type=="credit")
                                                <span class="text-success">+  {{$trans->transaction_amount}}</span>
                                                @endif
                                                @if($trans->transaction_type=="debit")
                                                    <span class="text-danger">-  {{$trans->transaction_amount}}</span>
                                                @endif
                                        </td>
                                        <td>{{$trans->transaction_status}}</td>
                                        <td>
                                            <a type="button" class="btn btn-info btn-sm" href="{{route('patient.wallet-detail',$trans->id)}}">
                                                <i class="fa fa-eye text-white" aria-hidden="true"></i> View</a>
                                        </td>
                                    </tr>
                                    @php $n++ @endphp
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
