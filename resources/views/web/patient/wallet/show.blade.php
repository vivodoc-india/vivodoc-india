@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Wallet Transaction Detail</div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row ">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 side-bg">
                @include('web.left-menu')
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 mb-2">
                        <div class="card mt-3">
                            <div class="card-body">
                                @if(isset($detail) && $detail!='')
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label">Transaction Date </label><br>
                                        <span>{{date('d-m-Y',strtotime($detail->transaction_date))}}</span>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label">Transaction Id</label><br>
                                        <span>{{$detail->transaction_number}}</span>

                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label">
                                            Transaction Amount
                                        </label><br>
                                        <span>Rs. {{$detail->transaction_amount}}</span>
                                    </div>

                                    <div class="col-md-4 mt-3">
                                        <label class="control-label">
                                            Transaction Status
                                        </label><br>
                                        <span>{{$detail->transaction_status}}</span>
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="control-label">
                                            Transaction Type
                                        </label><br>
                                        <span>{{$detail->transaction_type}}</span>
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="control-label">Reference Id</label><br>
                                        <span>{{$detail->referenceId}}</span>

                                    </div>
                                </div>
                                    @endif
                            </div>
                        </div>



            </div>
        </div>
    </div>
@endsection
