@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Ambulance Booking Detail</div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cp-md">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 side-bg">
                @include("web.left-menu")
            </div>
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 pb-4 pt-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h3 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Ambulance booking Detail
                                </button>
                            </h3>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-body">
                                        @if(isset($booking))

                                            <div class="row">
                                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                                                    <div class="h3-responsive mb-2">{{$booking->provider->name}}</div>

                                                    <div class="mb-2">{{$booking->provider->profile->speciality}}</div>
                                                    <div class="mb-2">{{$booking->provider->profile->qualification}}</div>
                                                    <div class="mb-2">{{$booking->provider->profile->other_speciality}}</div>
                                                    <div class="mb-2">{{$booking->provider->profile->address}}</div>
                                                    <div class="mb-2">{{$booking->serviceProfile->vehicle_no}}</div>
                                                </div>

                                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 " >
                                                    <img src="{{asset('web/v2/img/logo/logo.svg')}}" class="img-fluid">
                                                </div>



                                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">

                                                    <div class="h3-responsive mb-2 text-right">{{$booking->patient_name}}</div>
                                                    <div class="mb-2 text-right"><label class="control-label">Age : </label> {{$booking->age}}</div>
                                                    <div class="mb-2 text-right"><label class="control-label">Mobile : </label> {{$booking->mobile}}</div>
                                                    <div class="mb-2 text-right"><label class="control-label">Gender : </label> {{$booking->gender}}</div>
                                                    <div class="mb-2 text-right"><label class="control-label">Address : </label> {{$booking->address}}</div>
{{--                                                    <div class="mb-2 text-right"><label class="control-label">Booking Number : </label> {{$booking->booking_number}}</div>--}}
                                                </div>
                                            </div>
                                            <hr>
                                            <h6 class="control-label" style="text-decoration: underline; font-weight: bold">Booking Details</h6>
                                    <hr>
                                            <div class="row mt-4">

                                                <div class="col-md-4 col-12">
                                                    <div class="mb-2"><label class="control-label">Pickup Location  </label><br> {{$booking->address}}</div>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-2"><label class="control-label">Drop Location  </label><br> {{$booking->destination}}</div>

                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-2"><label class="control-label">Approx Journey Fair  </label><br> {{$booking->approx_fare!=''?'Rs. '.$booking->approx_fare:'Wait for provider response.'}}</div>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-2"><label class="control-label">Advance Payment  </label><br> {{$booking->advance_payment!=''?'Rs. '.$booking->advance_payment:'Wait for provider response.'}}</div>
                                                </div>
                                                <div class="col-md-4 col-12">
                                                    <div class="mb-2"><label class="control-label">Payment Status  </label><br>
                                                       <span class={{$booking->is_paid==1?"text-success":'text-danger'}}> {{$booking->is_paid==1?'Paid':'Not Paid'}}</span>
                                                        <br>
                                                    @if($booking->is_paid==0 && $booking->serviceProfile->mark_live==1 && $booking->serviceProfile->is_available==1)
                                                        <a href="{{route('ambulance-bookings.edit',$booking->id)}}" class="control-label">Pay Now</a>
                                                        @endif
                                                    </div>
                                                </div>
                                                @if($booking->is_paid==1)
                                                    <div class="col-md-4 col-12">
                                                        <div class="mb-2"><label class="control-label">Provider's Mobile Number  </label><br> {{$booking->provider->mobile}}</div>
                                                    </div>
                                                    @endif
                                            </div>

{{--                                            <div class="row">--}}
{{--                                                <div class="col-md-12 col-lg-12 mb-4 col-12">--}}
{{--                                                    <div style="min-height:400px; width:100%; border:1px solid #C7C7C7; padding: 5px;">--}}
{{--                                                        @if(!empty($booking->prescription) || !empty($booking->prescription_image))--}}
{{--                                                            {!! $booking->prescription !!}--}}

{{--                                                            <img src="{{asset('uploads/patientprofile/prescription/'.$booking->prescription_folder.'/'.$booking->prescription_image)}}" class="img-fluid">--}}
{{--                                                        @else--}}
{{--                                                            {{'Uploaded Note & Prescriptions'}}--}}
{{--                                                        @endif--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

                                        @endif
                                        {{--                                        <div class="row mt-2">--}}
                                        {{--                                            <div class="col text-right">--}}
                                        {{--                                                <a class="btn btn-sm btn-info" href="{{url('patient/print/appointment-prescription/'.$booking->id)}}" target="_blank">--}}
                                        {{--                                                    <i class="fas fa-print"></i></a>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
{{--                                        <div class="row mt-2">--}}
{{--                                            <p> <span class="text-danger">Disclaimer :</span> Doctor generated this prescription Online based on your inputs. If you get any problem please visit your Doctor.</p>--}}
{{--                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="card">--}}
{{--                        <div class="card-header" id="headingTwo">--}}
{{--                            <h3 class="mb-0">--}}
{{--                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">--}}
{{--                                    Reviews & Ratings--}}
{{--                                </button>--}}
{{--                            </h3>--}}
{{--                        </div>--}}
{{--                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">--}}
{{--                            <div class="card-body">--}}
{{--                                <div class="card">--}}
{{--                                    <div class="card-body">--}}
{{--                                        <h4 class="mb-2"> Your reviews and ratings about this consultation.</h4>--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-lg-6 col-md-6 col-sm-12">--}}
{{--                                                <div class="row">--}}
{{--                                                    <form action="{{url('patient/update-appointment-reviews')}}" method="post">--}}
{{--                                                        @csrf--}}
{{--                                                        <input type="hidden" name="appoint_id" value="{{$booking->id}}">--}}
{{--                                                        <div class="col-lg-12 col-md-12 col-sm-12 ">--}}
{{--                                                            <h6>Ratings</h6>--}}
{{--                                                            <div class="survey-builder container">--}}
{{--                                                                <input type="hidden" class="rating" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" >--}}
{{--                                                                <input type="hidden" name="rating" id="rv" value="">--}}
{{--                                                            </div>--}}
{{--                                                            @error('rating')--}}
{{--                                                            <div class="alert alert-danger">{{$message}}</div>--}}
{{--                                                            @enderror--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-lg-12 col-md-12 col-sm-12">--}}
{{--                                                            <h6>Review</h6>--}}
{{--                                                            <div class="form-group">--}}
{{--                                                                <textarea class="form-control @error('review') is-invalid @enderror" name="review" >{{isset($booking) && $booking->review!=''?$booking->review:''}}</textarea>--}}
{{--                                                                @error('review')--}}
{{--                                                                <div class="alert alert-danger">{{$message}}</div>--}}
{{--                                                                @enderror--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}

{{--                                                        <button type="submit" class="btn btn-info">Submit</button>--}}
{{--                                                    </form>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-lg-6 col-md-6 col-sm-12">--}}
{{--                                                @if(isset($booking) && $booking->rating!='' && $booking->rating >0 )--}}
{{--                                                    <div class="mt-1 mb-1">--}}
{{--                                                        <h6>Previous Rating</h6>--}}
{{--                                                        @php $n=1 @endphp--}}
{{--                                                        @php while($n <= $booking->rating){ @endphp--}}
{{--                                                        <i class="fa fa-star" aria-hidden="true"></i>--}}
{{--                                                        @php--}}
{{--                                                            $n++ @endphp--}}
{{--                                                        @php    }   @endphp--}}
{{--                                                    </div>--}}

{{--                                                @endif--}}
{{--                                                @if(isset($booking) && $booking->review!='')--}}
{{--                                                    <div class="mt-1">--}}
{{--                                                        <h6>Previous Review</h6>--}}
{{--                                                        <p>{{$booking->review}}</p>--}}
{{--                                                    </div>--}}

{{--                                                @endif--}}
{{--                                            </div>--}}

{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                </div>


            </div>
        </div>
    </div>
@endsection
@section('additional_script')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="{{asset('web/rating/bootstrap-rating.js')}}"></script>
    <script>
        $(function () {

            $('.rating').on('change', function () {
                // $(this).next('.label').text($(this).val());
                $('#rv').val($(this).val());
            });
        });
    </script>
@endsection

