@extends('layouts.layout')
@section('content')

    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Register</div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cp-lg bg-light">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                <div class="card rounded-0">
                    <div class="card-header bg-white text-center">
                        <img src="{{asset('web/v2/img/logo/logo.svg')}}" width="150">
                    </div>
                    <form method="POST" action="{{ url('patient/register') }}" autocomplete="false" class="frm-reg">
                        @csrf
                        <div class="card-body">
                            {{-- @if(isset($list))
                            <div class="form-group">
                                <select name="user_role_id" class="custom-select @error('user_role_id') is-invalid @enderror">
                                    <option selected disabled>Register As</option>
                                @foreach($list as $ul)
                                    <option value="{{$ul->id}}">{{ucwords($ul->name)}}</option>
                            @endforeach
                                    </select>
                                </div>

                            @endif
                             @error('user_role_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror --}}
                            <div class="form-group">
                                <input type="text" name="name" class="form-control rounded-0 @error('name') is-invalid @enderror " placeholder="Your Name" value="{{old('name')}}" required>
                            </div>
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <input type="text" name="mobile" class="form-control rounded-0 @error('mobile') is-invalid @enderror " placeholder="Your mobile number" maxlength="10" value="{{old('mobile')}}" oninput='this.value=(parseInt(this.value)||" ")' required id="reg-mobile">
                            </div>
                            @error('mobile')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <input type="email" class="form-control rounded-0 @error('email') is-invalid @enderror " placeholder="Email address" name="email" value="{{old('email')}}" required>
                            </div>
                            @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <input type="password" class="form-control rounded-0 @error('password') is-invalid @enderror " placeholder="Enter password" name="password" maxlength="8" value="{{old('password')}}" required>
                            </div>
                            @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <input id="password-confirm" type="password" class="form-control rounded-0" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password" maxlength="8">
                                {{-- <input type="password" class="form-control rounded-0 @error('c_pass') is-invalid @enderror " placeholder="Confirm Password" name="c_pass"> --}}
                            </div>
                            {{-- @error('c_pass')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror --}}
                            <button type="submit" class="btn btn-primary rounded-pill btn-block">Register</button>
                            <div class="mt-3 text-center">Already have an account? <a href="{{url('patient/login')}}">Sign In</a></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
