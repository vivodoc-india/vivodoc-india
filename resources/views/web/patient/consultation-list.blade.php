@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 side-bg">
                @include("web.left-menu")
            </div>
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 pb-4 pt-4">
                <div class="card">
                    <div class="card-body">
                        <table id="example" class="display responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>S.no.</th>
                                <th>Service Provider</th>
                                <th>Patient Detail</th>
{{--                                <th>Patient Age</th>--}}
{{--                                <th>Patient Gender</th>--}}

                                <th>Requested On</th>
                                <th>Status</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php $n=1 @endphp
                            @if(isset($details))
                                @foreach($details as $en)
                                    <tr>
                                        <td>{{$n}}</td>
                                        <td>{{$en->provider->name}}</td>
                                        <td>{{ucwords($en->patient_name)}}<br>Age : {{$en->age}}<br>Gender : {{$en->gender}}</td>
{{--                                        <td>{{$en->age}}</td>--}}
{{--                                        <td>{{$en->gender}}</td>--}}

                                        <td>{{date('d-m-Y',strtotime($en->booking_date))}}</td>

                                        <td>
                                            @if($en->prescription!='' || $en->prescription_folder!='')
                                                <span class="badge badge-pill badge-success font-bold"> {{'Completed'}}</span>
                                            @elseif($en->is_request_rejected == 1)
                                                {{'Request Rejected'}}
                                            @elseif($en->is_request_accepted == 1)
                                                {{'Request Accepted'}}
                                            @else
                                                <span class="badge badge-pill badge-info font-bold">  {{'Not Answered'}}</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-success btn-sm" href="{{url('patient/view-consultation/'.$en->id)}}"> <i class="fas fa-eye text-white"></i> </a>
                                            @if($en->prescription=='' && $en->prescription_image=='')
                                                <a class="btn btn-info btn-sm " href="{{url('patient/review-consultation-request/'.$en->id)}}"> <i class="fas fa-edit"></i></a>
                                            @endif
                                            {{--     <a type="button" class="btn btn-danger btn-sm no-gutters delete_button_action" href="javascript:void(0)" data-url="{{route('live-consultation.destroy',$en->id)}}" data-id="{{$en->id}}" data-toggle="modal" data-target="#deleteModal"> <i class="fas fa-trash-alt text-white"></i> </a>--}}

                                        </td>
                                    </tr>
                                    @php $n++ @endphp
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    </section>
@endsection
{{--@section('additional_script')--}}

{{--@endsection--}}
