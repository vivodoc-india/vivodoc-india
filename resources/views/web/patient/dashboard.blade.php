@extends('layouts.layout')
@section('content')
<section clas="main">
    <div class="profile-view-section">

<div class="container-fluid">
	<div class="row">
		<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 side-bg">
@include('web.left-menu')
		</div>
		<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 pt-4 pb-4">
			<div class="card">
				<div class="card-body">
                    @if(isset($profile))

					<div class="row mb-5">
						<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">

                            @if($profile->photo !='')
<a href="">
    <img src="{{asset('uploads/profile/'.$profile->photo)}}" class="img-fluid img-thumbnail rounded-circle" style="width:150px;"></a>
                            @else
<a href=""><img src="{{asset('web/img/nobody.jpg')}}" class="img-fluid img-thumbnail rounded-circle"></a>
                            @endif

						</div>
						<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                            <div class="row">
                            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">
                        <div class="h3-responsive">{{$profile->user->name}}</div>
                                <div class="mb-2"><i class="fa fa-id-badge" aria-hidden="true"></i>
                                    {{$profile->user->puid}}</div>
 <div class="mb-2"><i class="fas fa-tint"></i> {{$profile->blood_group}}</div>
                                <div class="mb-2">
                                    @if($profile->gender == "Male")
                                        <i class="fas fa-male"></i> {{$profile->gender}}
                                    @elseif($profile->gender == "Female")
                                        <i class="fas fa-female"></i> {{$profile->gender}}
                                    @endif
                                </div>

    <div class="mb-2"><i class="fas fa-map-marker-alt"></i> {{ucwords($profile->city.",".$profile->state)}}</div>
                        </div>

</div>
							{{-- <div class="mb-2">ODEM564789</div> --}}

							{{-- <div class="mb-2">28 Years</div>
							<div class="mb-2">28 Oct, 1991</div> --}}

						</div>
                        <div class="col-md-3 col-sm-12 col-lg-3">

                            <a href="{{route('patient.wallet')}}" class="font-weight-bold"> <i class="fas fa-wallet fa-2x"></i> <span style="font-size: 25px;">{{$profile->user->wallet_amount}}</span></a>
                        </div>
					</div>
					<div class="row mb-5">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
							<div class="h3-responsive mb-3">Contact Details</div>
                        <div class="mb-2">{{$profile->user->email}}</div>
                        <div class="mb-2">{{$profile->user->mobile}}</div>
                        <div class="mb-2">{{$profile->address}}</div>
                        <div class="mb-2">{{$profile->state}}</div>
                        <div class="mb-2">{{$profile->pincode}}</div>
						</div>
                    </div>

                        @else
                        <h3 class="text-center">Kindly Update Your Profile</h3>
                    <div align="center">
                        <a class="btn btn-info mt-5" href="{{url('patient/update-profile')}}">Update Profile</a>
                    </div>

                @endif
                </div>

			</div>
		</div>
	</div>
</div>
    </div>
</section>
@endsection
