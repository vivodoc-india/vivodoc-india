<?php

$images = array( 'img/1.png', 'img/2.png', 'img/3.png', 'img/4.png', 'img/5.png', 'img/6.png', 'img/7.png', 'img/8.png', 'img/9.png', 'img/10.png', 'img/11.png', 'img/12.png', );
shuffle( $images );

$clients = array( 'img/c1.png', 'img/c2.png', 'img/c3.png', 'img/c4.png', 'img/c5.png', 'img/c6.png', 'img/c7.png', 'img/c8.png' );
shuffle( $clients );

$events = array( 'img/events1.png', 'img/events2.png', 'img/events3.png', 'img/events4.png', 'img/events5.png', 'img/events6.png', );
shuffle( $events );

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>OMED</title>
	<link href="https://fonts.googleapis.com/css?family=Raleway:500|Roboto:500&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" />
	<link rel="stylesheet" href="css/all.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/mdb.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/viewbox.css">
	<link rel="stylesheet" href="css/style.css">
</head>

<body>
	<div class="container cp-xs">
		<div class="d-flex justify-content-end">
			<div class="pl-4 font-sm"><a href="register.php" class="text-dark"><i class="fa fa-user text-primary"></i> Register</a>
			</div>
			<div class="pl-4 font-sm"><a href="login.php" class="text-dark"><i class="fa fa-sign-in-alt text-primary"></i> Login</a>
			</div>
			<div class="pl-4 font-sm"><a href="dashboard.php" class="text-dark"><i class="fa fa-home text-primary"></i> Dashboard</a>
			</div>
		</div>
	</div>
	<div class="hr-line"></div>
	<div class="container cp-xs">
		<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<a href="index.php" class=""><img src="img/logo.PNG"></a>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3"></div>
			<div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 align-self-center">
				<div class="row align-self-center no-gutters">
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-5 align-self-center">
						<div class="row">
							<div class="col-2 align-self-center"><i class="fa fa-phone-alt text-primary"></i>
							</div>
							<div class="col-10">
								<div>+91 9308463744</div>
								<span class="text-black-50 font-sm">24/7 available</span>
							</div>
						</div>
					</div>
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-7 align-self-center">
						<div class="row">
							<div class="col-2 align-self-center"><i class="fa fa-envelope text-primary"></i>
							</div>
							<div class="col-10">
								<div>aalihealthcare@gmail.com</div>
								<span class="text-black-50 font-sm">Ask for any question</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="hr-line"></div>
	<nav class="navbar navbar-expand-lg navbar-light shadow-none">
		<div class="container">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			  </button>

			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="#">Home</a>
					</li>
					<li class="nav-item"><a class="nav-link" href="about.php">About Us</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Our Services</a>
						<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="lab-services.php">Diagnostic &amp; Lab Services</a>
							<a class="dropdown-item" href="medical-product.php">Medicine &amp; Medical Products</a>
							<a class="dropdown-item" href="checkup-package.php">Checkup Packages</a>
							<a class="dropdown-item" href="immunization-services.php">Immunization Services</a>
							<a class="dropdown-item" href="doctor-appointment.php">Doctor Appointment</a>
						</div>
					</li>
					<li class="nav-item"><a class="nav-link" href="gallery">Gallery</a>
					</li>
					<!--<li class="nav-item"><a class="nav-link" href="#">Blood Bank</a>
					</li>-->
					<li class="nav-item"><a class="nav-link" href="contact.php">Contact Us</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
