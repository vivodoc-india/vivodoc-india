@extends('layouts.layout')
@section('content')
<!--<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">Article</div>
			</div>
		</div>
	</div>
</div>-->
<div class="container cp-md">

	<div class="row">
		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-4">
			<h3>Vaccination Rate Drop Dangerously as Parents Avoid Doctor’s Visits</h3>
			<div class="mb-4"><img src="https://images.pexels.com/photos/3985170/pexels-photo-3985170.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="img-fluid img-thumbnail"></div>
			<p>As parents around the country cancel well-child checkups to avoid coronavirus exposure, public health experts fear they are inadvertently sowing the seeds of another health crisis. Immunizations are dropping at a dangerous rate, putting millions of children at risk for measles, whooping cough and other life-threatening illnesses. “The last thing we want as the collateral damage of Covid-19 are outbreaks of vaccine-preventable diseases, which we will almost certainly see if there continues to be a drop in vaccine uptake,” said Dr. Sean T. O’Leary, a member of the American Academy of Pediatrics’ committee on infectious diseases.</p>
			<p>In the last few years, early childhood immunization rates have been slipping in some hot spots around the country, and in 2019, the United States very nearly lost its measles elimination status. While current nationwide vaccine figures are not available, anecdotal evidence and subsets of data are alarming.</p>
			<p>PCC, a pediatric electronic health records company, gathered vaccine information from 1,000 independent pediatricians nationwide. Using the week of February 16 as a pre-coronavirus baseline, PCC found that during the week of April 5, the administration of measles, mumps and rubella shots dropped by 50 percent; diphtheria and whooping cough shots by 42 percent; and HPV vaccines by 73 percent.</p>
			<p>The doses that states distribute in a federally funded program for uninsured patients called Vaccines for Children have also dropped significantly since the beginning of March. The Massachusetts health department said its doses were down 68 percent in the first two weeks of April, compared with the previous year. Minnesota reported that its doses of measles, mumps and rubella vaccine dropped by 71 percent toward the end of March.</p>
			<p>In Washington State, dozens of practices and clinics have had to reduce hours or even temporarily close. The state already had its biggest measles outbreak in nearly 30 years last year.</p>
			<p>“We know our vaccine rates were already tenuous, so any additional hit to that is a great worry,” said Dr. Elizabeth Meade, president of the state’s chapter of the American Academy of Pediatrics. Dr. Meade leads calls twice a week with physicians throughout the state about how to maintain immunizations and stay solvent.</p>
			<p>The problem is global. National immunization programs in more than two dozen countries have been suspended, which could also leave more than 100 million children vulnerable, a consortium of international organizations, including UNICEF and the World Health Organization, recently reported.</p>
			<p>“Internationally, measles and diphtheria will pop up around the world. Even with limited travel, they can make it into the United States,” Dr. O’Leary, an immunization expert at Children’s Hospital in Denver, said.</p>
			<p>According to immunization experts, the optimum rate of coverage for many vaccines, known as herd immunity, is about 90 to 95 percent.</p>
			<p>The Centers for Disease Control and Prevention, the American Academy of Pediatrics, and the American Academy of Family Physicians have each been urging doctors to maintain vaccination schedules as rigorously as reasonably possible, particularly for the youngest children. Vaccinate Your Family, a national nonprofit group, is pushing families to set reminders to reschedule canceled vaccine appointments.</p>
			<p>Though many doctors note that vaccine-preventable diseases can be more deadly to children than Covid-19 seems to be, parents are understandably focused on the threat at hand. Over the last six weeks, the loud, consistent public message has been to keep children at home, and to take them to the doctor only if necessary.</p>
			<p>Initially, medical practices were apprehensive too. In early March, the health clinic in Barre, Mass., called Emily Hoag to say it had postponed her baby’s vaccine appointment for a month to prevent the spread of coronavirus infection. Ms. Hoag felt conflicted: If her baby, Karson, missed his two-month immunization, she feared, he would be susceptible to any number of diseases. But if she took him to the clinic for his shots, they both might be exposed to Covid-19.</p>
			<p>A few hours later, Dr. Kristina Gracey, a family medicine physician at the Barre Family Health Clinic, reached out to the new mother: Would she like a house call?</p>
			<p>That afternoon, Dr. Gracey showed up at Ms. Hoag’s doorstep. After removing her shoes, meticulously washing her hands and wiping her stethoscope and baby scale, Dr. Gracey gave Karson his shots. Both mother and baby immediately burst into tears.</p>
			<p>“Karson cried for a minute and then calmed down, and I was just so grateful that Dr. Gracey was there and able to give him the vaccinations he needed,” Ms. Hoag said.</p>
			<p>Dr. Gracey, who has a degree in public health and has practiced in Uganda, has no qualms about home visits, which she and her colleagues are making several times a week.</p>
			<p>“We have so many women who are struggling with what it feels like to have a child in the setting of Covid-19,” she said. “And especially for a new mom who has concerns about the risks of coming into the office, it can feel comfortable to receive care within the home.”</p>
			<p>Dr. Gracey’s colleagues and other medical practices are experimenting with other ways to boost vaccine rates during the outbreak, including setting up a vaccination tent in a field.</p>
			<p>Many practices now schedule well-child visits exclusively in the morning and sick visits in the afternoon, so that an office can be decontaminated at the end of the day. Some have families wait in the car and, when an exam room is ready, a gowned nurse escorts parent and child in for the vaccine.</p>
			<p>Dr. Jeanne M. Marconi’s practice in Norwalk, Conn., which had been doing flu clinics in parking lots for years, has adapted that procedure for vaccines generally: Parents pull up, briefly roll down a car window, the child extends an arm, and a masked, gowned healthcare worker does a quick jab. Over and out.</p>
			<p>“We’re trying to alleviate all of the fears they have and keep up with the care,” Dr. Marconi said.</p>
			<p>Last week, the pediatric ambulatory department at Boston Medical Center, which treats nearly 15,000 children, began sending vaccination mobile units into city neighborhoods. It also stationed a dedicated van for vaccines and well-baby checkups in front of the hospital.</p>
			<p>In the early weeks of the shelter-in-place orders, doctors concentrated efforts on vaccinating infants up to 2 years old, and waved off the disruption to the schedule for older children as temporary, saying it could readily be addressed once the restrictions lifted. But the longer that the orders continue, the more worried doctors have also become about vaccine protection for older children.</p>
			<p>One concern is that if booster shots are missed — for diseases like measles, mumps and rubella for 4- and 5-year-olds, and tetanus and whooping cough, for 11-year-olds — immunity will begin to wane.</p>
			<p>At 11, children should also receive their first meningitis vaccine. Preteens are recommended to get the HPV vaccine series, which protects against certain types of cancer.</p>
			<p>Beginning next month, Dr. Eleanor Menzin, managing partner of Longwood Pediatrics in Boston, will try to vaccinate older children, when the practice’s waiting rooms will still be relatively empty. “Looking ahead, I think it’s unwise to get behind even on older kids, because of the logistics of catching them up, given what I predict will be a long period of avoiding crowds,” she said.</p>
			<p>Many doctors already report that the backlog from canceled appointments for younger children is staggering. But summer appointment calendars are typically filled by older children, who need vaccine documentation for school and college. Pediatricians, who report that visits have dropped by 50 to 70 percent, are laying off staff; they do not know whether they will be able to handle the rush of last-minute visits in a few months.</p>
			<p>Some health officials are wondering whether school registration policies will need to be adjusted: Because of these extreme circumstances, will states temporarily ease school vaccination requirements?</p>
			<p>Health experts are also worried about day care centers. Licensed centers require proof of vaccination. Even assuming that parents returning to work could swiftly get immunization appointments for their young children, most vaccines take between two and four weeks before providing full protection.</p>
			<p>Dr. Menzin, an instructor of pediatrics at Harvard Medical School, said the pandemic was a wake-up call for doctors to rethink their messaging:</p>
			<p>“It no longer suffices for us to say, ‘We’re open if you want to come in,’ versus, ‘We want you to come in because this is important. What is keeping you from getting your child vaccinated and let’s solve that together,’” said Dr. Menzin, who has studied bus schedules with patients and helped pick days when the weather forecast suggests that they could walk to the visit.</p>
			<p>A common retort to fears about outbreaks of vaccine-preventable illnesses is that transmission will have also dropped because of social distancing. But that assertion leaves many pediatricians shaking their heads.</p>
			<p>“They’re at less risk right now, but that risk isn’t zero,” said Dr. Menzin. While sick visits are down, they have not disappeared: Viruses of all sorts are still making plenty of children miserable.</p>
			<p>Despite parents’ reluctance to bring in their children to be vaccinated now, several doctors remarked on a noticeable positive shift in attitude toward immunizations, after years of a vocal anti-vaccine movement raising questions in parents’ minds.</p>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
			<div class="card shadow-none border border-light">
				<div class="card-header">Articles</div>
				<div class="card-body">
					<ul class="list-group list-group-flush">
						<li class="list-group-item"><a href="article_1.blade.php" class="text-dark">Vaccination Rate Drop Dangerously as Parents Avoid Doctor’s Visits</a></li>
						<li class="list-group-item"><a href="article_2.blade.php" class="text-dark">COVID-19, Rural Healthcare and Digitalization of Healthcare</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection
