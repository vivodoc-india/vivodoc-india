@extends('layouts.layout')
@section('content')
@if(Session::has('cat_id') || Session::has('cat_title')  || Session::has('search_for') ||
Session::has('search_field'))
    @php
    Session::forget('cat_id')
    @endphp
    @php
        Session::forget('cat_title')
    @endphp
    @php
        Session::forget('search_for')
    @endphp
    @php
        Session::forget('search_field')
    @endphp
    @endif
<div class="container-fluid fnmf cp-md" id="fnmf">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-10 col-md-10 col-sm-12">
				<div class="text-white h1-responsive text-center mb-5">Find the Nearest Healthcare Facility</div>
				<ul class="nav nav-tabs" id="myTab" role="tablist">
                    @php $n=1 @endphp
                    @foreach($categories as $cat)
					<li class="nav-item">
                    <a class="nav-link @if($n == 1 ) {{'active'}} @endif" id="home-tab" data-toggle="tab" href="#@if($cat->name== "Other Services"){{'other'}}@else{{$cat->name}}@endif" role="tab" aria-controls="home" aria-selected="true">{{ucwords($cat->name)}}</a>
                    </li>
                    @php $n++ @endphp
                    @endforeach
					{{-- <li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Doctors</a>
					</li> --}}
					{{-- <li class="nav-item">
						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Diagnostic</a>
					</li> --}}
					{{-- <li class="nav-item">
						<a class="nav-link" id="ambulance-tab" data-toggle="tab" href="#ambulance" role="tab" aria-controls="ambulance" aria-selected="false">Ambulance</a>
					</li> --}}
					{{-- <li class="nav-item">
						<a class="nav-link" id="medicine-tab" data-toggle="tab" href="#medicine" role="tab" aria-controls="medicine" aria-selected="false">Medicine</a>
					</li> --}}
					{{-- <li class="nav-item">
						<a class="nav-link" id="health-tab" data-toggle="tab" href="#health" role="tab" aria-controls="health" aria-selected="false">Other Services</a>
					</li> --}}
				</ul>
				<div class="tab-content p-3" id="myTabContent">
                    @php $p=1 @endphp
					@foreach($categories as $cat)
                    <div class="tab-pane fade @if($p == 1 ) {{'show active'}} @endif" id="@if($cat->name== "Other Services"){{'other'}}@else{{$cat->name}}@endif" role="tabpanel" aria-labelledby="home-tab">
                        <form action="{{url('search-provider')}}" method="get" class="search-provider">
                            <div class="row no-gutters">
                                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control rounded-0 search-box" placeholder="Search Area,City" name="search_field" required>
                                    </div>
                                </div>
                                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                                    <select class="form-control rounded-0 opt-value" name="category" required>
                                        <option disabled selected>Type of {{$cat->name}}</option>
                                        {{$subcat=$cat->category()->where('status','1')->select('id','title')->get()}}
                                        @foreach($subcat as $sc)
                                            <option value="{{$sc->id}}">{{ucwords($sc->title)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <input type="hidden" name="search_for" value="{{$cat->id}}" class="search-for">
                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
                                    <button type="submit" class="btn btn-warning m-0 rounded-0 btn-md btn-block shadow-none btn-search"><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    @php $p++ @endphp
                    @endforeach
					{{-- <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
						<div class="row no-gutters">
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control rounded-0" placeholder="Search Location">
								</div>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Type of Doctors</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Distance</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Max Fees</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<button type="button" class="btn btn-warning m-0 rounded-0 btn-md btn-block shadow-none"><i class="fa fa-search"></i> Search</button>
							</div>
						</div>
					</div> --}}
					{{-- <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
						<div class="row no-gutters">
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control rounded-0" placeholder="Search Location">
								</div>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Diagnostic</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Distance</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Max Fees</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<button type="button" class="btn btn-warning m-0 rounded-0 btn-md btn-block shadow-none"><i class="fa fa-search"></i> Search</button>
							</div>
						</div>
					</div> --}}
					{{-- <div class="tab-pane fade" id="ambulance" role="tabpanel" aria-labelledby="ambulance-tab">
						<div class="row no-gutters">
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control rounded-0" placeholder="Search Location">
								</div>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Ambulance</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Distance</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Max Fees</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<button type="button" class="btn btn-warning m-0 rounded-0 btn-md btn-block shadow-none"><i class="fa fa-search"></i> Search</button>
							</div>
						</div>
					</div> --}}
					{{-- <div class="tab-pane fade" id="medicine" role="tabpanel" aria-labelledby="medicine-tab">
						<div class="row no-gutters">
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control rounded-0" placeholder="Search Location">
								</div>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Medicine</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Distance</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Max Fees</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<button type="button" class="btn btn-warning m-0 rounded-0 btn-md btn-block shadow-none"><i class="fa fa-search"></i> Search</button>
							</div>
						</div>
					</div> --}}
					{{-- <div class="tab-pane fade" id="health" role="tabpanel" aria-labelledby="health-tab">
						<div class="row no-gutters">
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control rounded-0" placeholder="Search Location">
								</div>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Health</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Distance</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<select class="form-control rounded-0">
									<option>Max Fees</option>
								</select>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
								<button type="button" class="btn btn-warning m-0 rounded-0 btn-md btn-block shadow-none"><i class="fa fa-search"></i> Search</button>
							</div>
						</div>
					</div> --}}
				</div>
			</div>
		</div>
        <div class="text-center d-mob"><button type="button" class="btn btn-link" id="btn-close">Close</button></div>
	</div>
</div>


<div class="container-fluid bg-light cp-md">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="h2-responsive mb-5">Categories</div>
				</div>
		</div>
		<div class="row">
@if(isset($categories))
@foreach($categories as $category)
<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 mb-3">
<a href="{{url('category/'.strtolower($category->name).'/'.$category->id)}}" class="text-dark">
					<div class="card h-100">
						<div class="card-body text-center">
							<div class="mb-3"><img src="{{asset("web/img/$category->image")}}" class="img-fluid">
							</div>
							<div class="mb-3 font-lg">{{ucwords($category->name)}}</div>
                        {{-- <div><span class="rounded-pill bg-light pl-3 pr-3">{{$category->total}}</span>
							</div> --}}
						</div>
					</div>
				</a>
			</div>
@endforeach
@endif

			{{-- <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 mb-3">
				<a href="" class="text-dark">
					<div class="card">
						<div class="card-body text-center">
							<div class="mb-3"><img src="{{asset("web/img/doctor.png")}}" class="img-fluid">
							</div>
							<div class="mb-3 font-lg">Doctor</div>
							<div><span class="rounded-pill bg-light pl-3 pr-3">25</span>
							</div>
						</div>
					</div>
				</a>
			</div> --}}
			{{-- <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 mb-3">
				<a href="" class="text-dark">
					<div class="card">
						<div class="card-body text-center">
							<div class="mb-3"><img src="{{asset("web/img/pharmacy.png")}}" class="img-fluid">
							</div>
							<div class="mb-3 font-lg">Diagnostic</div>
							<div><span class="rounded-pill bg-light pl-3 pr-3">25</span>
							</div>
						</div>
					</div>
				</a>
			</div> --}}
			{{-- <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 mb-3">
				<a href="" class="text-dark">
					<div class="card">
						<div class="card-body text-center">
							<div class="mb-3"><img src="{{asset("web/img/blood-bank.png")}}" class="img-fluid">
							</div>
							<div class="mb-3 font-lg">Ambulance</div>
							<div><span class="rounded-pill bg-light pl-3 pr-3">25</span>
							</div>
						</div>
					</div>
				</a>
			</div> --}}
			{{-- <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 mb-3">
				<a href="" class="text-dark">
					<div class="card">
						<div class="card-body text-center">
							<div class="mb-3"><img src="{{asset("web/img/medicine.png")}}" class="img-fluid">
							</div>
							<div class="mb-3 font-lg">Medicine</div>
							<div><span class="rounded-pill bg-light pl-3 pr-3">25</span>
							</div>
						</div>
					</div>
				</a>
			</div> --}}
			{{-- <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 mb-3">
				<a href="" class="text-dark">
					<div class="card">
						<div class="card-body text-center">
							<div class="mb-3"><img src="{{asset("web/img/heartbeat.png")}}" class="img-fluid">
							</div>
							<div class="mb-3 font-lg">Health</div>
							<div><span class="rounded-pill bg-light pl-3 pr-3">25</span>
							</div>
						</div>
					</div>
				</a>
			</div> --}}
		</div>
	</div>
</div>

<div class="container cp-md">
	<div class="row">
		<div class="col-12 text-center">
			<div class="h2-responsive ">New Registered</div>
{{--			<div class="mb-5">Vivamus suscipit quis sem pellentesque tincidunt Etiam luctus elementum eros ac laoreet.</div>--}}
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div id="new-registered" class="owl-carousel owl-theme">
                @if(isset($new_registration))
                    @foreach($new_registration as $sp)

{{--                       @php $profile=$sp->profile()->get() @endphp--}}
                        @if(count($sp->profile()->get())> 0)
                        <div class='item'>
                 		<div class='card rounded-0 ' style="height:250px!important;">
                   				<div class='view zoom'>
{{--                  				<a href='#' class='badge-off'><i class='fa fa-heart'></i></a>--}}
{{--                      			<a href='#' class='badge-new'><i class='fa fa-share-alt'></i></a>--}}
{{--                 				<a href='{{url('detail/'.$sp->service_profile()->first()->id)}}'>--}}
{{--                                    @if($sp->profile->photo!='')--}}
{{--                                        <img src='{{asset('uploads/userprofile/'.$sp->profile->photo)}}' class='img-fluid' style="width:100%; height:200px;"/>--}}
{{--                                @else--}}
{{--                                        <img src='{{asset('web/img/doctor-lg.png')}}' class='img-fluid' style="width:100%; height:200px;"/>--}}
{{--                                        @endif--}}
{{--                                </a>--}}
                			</div>
                			<div class='card-body'>

                  <div class='mb-2'><a href='{{url('detail/'.$sp->service_profile()->first()->id)}}' class='black-text h6-responsive '><strong>{{ucwords($sp->name)}}</strong></a></div>
   <div class='font-sm text-justify'>{{ucwords($sp->profile->qualification).",".ucwords($sp->profile->speciality)}}</div>
                                <div class="font-sm mb-2 text-justify">{{ucwords($sp->profile->other_speciality)}}</div>
                                <div class="'font-sm"><i class="fas fa-briefcase-medical"></i> {{ucwords($sp->profile->experiance)}}</div>
                                <div class="font-sm"><i class="fa fa-clock"></i> {{$sp->service_profile()->first()->availability}}</div>
            <div class="font-sm"><i class="fas fa-rupee-sign"></i> {{$sp->service_profile()->first()->min_fee." - ".$sp->service_profile()->first()->max_fee}}</div>
{{--                          		<div class='font-sm mb-3'>--}}
{{--                      					<i class='fa fa-star text-warning'></i>--}}
{{--                       					<i class='fa fa-star text-warning'></i>--}}
{{--                       					<i class='fa fa-star text-warning'></i>--}}
{{--                      					<i class='fa fa-star text-warning'></i>--}}
{{--                   					<i class='fa fa-star text-warning'></i>--}}
{{--                     				</div>--}}
              				<div class='font-sm mb-1'><i class='fa fa-map-marker-alt'></i> {{ucwords($sp->profile->city).", ".ucwords($sp->profile->state)}}</div>
                         			</div>
                   		<div class='card-footer bg-white font-sm'>
                      					<div class='row'>
                           					<div class='col-6'>
                                						<a href='{{url('detail/'.$sp->service_profile()->first()->id)}}' class='text-dark'><i class='fa fa-eye'></i> View</a>
                             					</div>
{{--              					<div class='col-6'>--}}
{{--                              						<a href='#' class='text-dark'><i class='fa fa-phone-alt'></i> Appointment</a>--}}
{{--                                   				</div>--}}
                    				</div>
                        			</div>
                           			</div>
                       	</div>
@endif
                        @endforeach
                    @endif

			</div>
			<div class="customNavigation float-right">
				<a class="btn btn-dark btn-sm prev"><i class="fa fa-angle-left fa-lg"></i></a>
				<a class="btn btn-dark btn-sm next"><i class="fa fa-angle-right fa-lg"></i></a>
			</div>
		</div>
	</div>
</div>

{{--<div class="container-fluid bg-light cp-md">--}}
{{--	<div class="container">--}}
{{--		<div class="row">--}}
{{--			<div class="col-12 text-center">--}}
{{--				<div class="h2-responsive mb-5">Top Rated List</div>--}}
{{--				<div class="mb-5">Vivamus suscipit quis sem pellentesque tincidunt Etiam luctus elementum eros ac laoreet.</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--		<div class="row">--}}
{{--			<div class="col-12">--}}
{{--				<ul class="nav nav-pills mb-4" id="pills-tab" role="tablist">--}}
{{--					<li class="nav-item">--}}
{{--						<a class="nav-link active" id="pills-doctor-tab" data-toggle="pill" href="#pills-doctor" role="tab" aria-controls="pills-doctor" aria-selected="true">Doctor</a>--}}
{{--					</li>--}}
{{--					<li class="nav-item">--}}
{{--						<a class="nav-link" id="pills-hospital-tab" data-toggle="pill" href="#pills-hospital" role="tab" aria-controls="pills-hospital" aria-selected="false">Hospital</a>--}}
{{--					</li>--}}
{{--					<li class="nav-item">--}}
{{--						<a class="nav-link" id="pills-diagnostic-tab" data-toggle="pill" href="#pills-diagnostic" role="tab" aria-controls="pills-clinic" aria-selected="false">Diagnostic</a>--}}
{{--					</li>--}}
{{--					<li class="nav-item">--}}
{{--						<a class="nav-link" id="pills-medicine-tab" data-toggle="pill" href="#pills-medicine" role="tab" aria-controls="pills-medicine" aria-selected="false">Medicine</a>--}}
{{--					</li>--}}
{{--				</ul>--}}
{{--				<div class="tab-content bg-light" id="pills-tabContent">--}}
{{--					<div class="tab-pane fade show active" id="pills-doctor" role="tabpanel" aria-labelledby="pills-doctor-tab">--}}
{{--						<div class="row">--}}

{{--									<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4'>--}}
{{--										<div class='card rounded-0'>--}}
{{--											<div class='view zoom'>--}}
{{--												<a href='#' class='badge-off'><i class='fa fa-heart'></i></a>--}}
{{--												<a href='#' class='badge-new'><i class='fa fa-share-alt'></i></a>--}}
{{--												<a href='#'><img src='' class='img-fluid'/></a>--}}
{{--											</div>--}}
{{--											<div class='card-body'>--}}
{{--												<div class='font-sm'>MEDICAL TYPE</div>--}}
{{--												<div class='mb-2'><a href='#' class='black-text'>Doctor, Clinic, Hospital Name</a></div>--}}
{{--												<div class='font-sm'><i class='fa fa-clock'></i> 10:00 AM - 08:00 PM</div>--}}
{{--												<div class='font-sm mb-3'>--}}
{{--													<i class='fa fa-star text-warning'></i>--}}
{{--													<i class='fa fa-star text-warning'></i>--}}
{{--													<i class='fa fa-star text-warning'></i>--}}
{{--													<i class='fa fa-star text-warning'></i>--}}
{{--													<i class='fa fa-star text-warning'></i>--}}
{{--												</div>--}}
{{--												<div class='font-sm mb-3'><i class='fa fa-map-marker-alt'></i> Varanasi</div>--}}
{{--											</div>--}}
{{--											<div class='card-footer bg-white font-sm'>--}}
{{--												<div class='row'>--}}
{{--													<div class='col-6'>--}}
{{--														<a href='#' class='text-dark'><i class='fa fa-eye'></i> Visit Website</a>--}}
{{--													</div>--}}
{{--													<div class='col-6'>--}}
{{--														<a href='#' class='text-dark'><i class='fa fa-phone-alt'></i> Appointment</a>--}}
{{--													</div>--}}
{{--												</div>--}}
{{--											</div>--}}
{{--										</div>--}}
{{--									</div>--}}

{{--						</div>--}}
{{--						<div class="row">--}}
{{--							<div class="col-12 text-center">--}}
{{--								<button type="button" class="btn btn-outline-dark rounded-pill shadow-none">Show more</button>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--					<div class="tab-pane fade" id="pills-hospital" role="tabpanel" aria-labelledby="pills-hospital-tab">--}}
{{--						<div class="row">--}}
							<?php
							// for ( $i = 0; $i < 4; $i++ )
							// 	echo "
							// 		<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4'>
							// 			<div class='card rounded-0'>
							// 				<div class='view zoom'>
							// 					<a href='#' class='badge-off'><i class='fa fa-heart'></i></a>
							// 					<a href='#' class='badge-new'><i class='fa fa-share-alt'></i></a>
							// 					<a href='#'><img src='$images[$i]' class='img-fluid'/></a>
							// 				</div>
							// 				<div class='card-body'>
							// 					<div class='font-sm'>MEDICAL TYPE</div>
							// 					<div class='mb-2'><a href='#' class='black-text'>Doctor, Clinic, Hospital Name</a></div>
							// 					<div class='font-sm'><i class='fa fa-clock'></i> 10:00 AM - 08:00 PM</div>
							// 					<div class='font-sm mb-3'>
							// 						<i class='fa fa-star text-warning'></i>
							// 						<i class='fa fa-star text-warning'></i>
							// 						<i class='fa fa-star text-warning'></i>
							// 						<i class='fa fa-star text-warning'></i>
							// 						<i class='fa fa-star text-warning'></i>
							// 					</div>
							// 					<div class='font-sm mb-3'><i class='fa fa-map-marker-alt'></i> Varanasi</div>
							// 				</div>
							// 				<div class='card-footer bg-white font-sm'>
							// 					<div class='row'>
							// 						<div class='col-6'>
							// 							<a href='#' class='text-dark'><i class='fa fa-eye'></i> Visit Website</a>
							// 						</div>
							// 						<div class='col-6'>
							// 							<a href='#' class='text-dark'><i class='fa fa-phone-alt'></i> Appointment</a>
							// 						</div>
							// 					</div>
							// 				</div>
							// 			</div>
							// 		</div>
							// 		";
							?>
{{--						</div>--}}
{{--						<div class="row">--}}
{{--							<div class="col-12 text-center">--}}
{{--								<button type="button" class="btn btn-outline-dark rounded-pill shadow-none">Show more</button>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--					<div class="tab-pane fade" id="pills-diagnostic" role="tabpanel" aria-labelledby="pills-diagnostic-tab">--}}
{{--						<div class="row">--}}
							<?php
							// for ( $i = 0; $i < 4; $i++ )
							// 	echo "
							// 		<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4'>
							// 			<div class='card rounded-0'>
							// 				<div class='view zoom'>
							// 					<a href='#' class='badge-off'><i class='fa fa-heart'></i></a>
							// 					<a href='#' class='badge-new'><i class='fa fa-share-alt'></i></a>
							// 					<a href='#'><img src='$images[$i]' class='img-fluid'/></a>
							// 				</div>
							// 				<div class='card-body'>
							// 					<div class='font-sm'>MEDICAL TYPE</div>
							// 					<div class='mb-2'><a href='#' class='black-text'>Doctor, Clinic, Hospital Name</a></div>
							// 					<div class='font-sm'><i class='fa fa-clock'></i> 10:00 AM - 08:00 PM</div>
							// 					<div class='font-sm mb-3'>
							// 						<i class='fa fa-star text-warning'></i>
							// 						<i class='fa fa-star text-warning'></i>
							// 						<i class='fa fa-star text-warning'></i>
							// 						<i class='fa fa-star text-warning'></i>
							// 						<i class='fa fa-star text-warning'></i>
							// 					</div>
							// 					<div class='font-sm mb-3'><i class='fa fa-map-marker-alt'></i> Varanasi</div>
							// 				</div>
							// 				<div class='card-footer bg-white font-sm'>
							// 					<div class='row'>
							// 						<div class='col-6'>
							// 							<a href='#' class='text-dark'><i class='fa fa-eye'></i> Visit Website</a>
							// 						</div>
							// 						<div class='col-6'>
							// 							<a href='#' class='text-dark'><i class='fa fa-phone-alt'></i> Appointment</a>
							// 						</div>
							// 					</div>
							// 				</div>
							// 			</div>
							// 		</div>
							// 		";
							?>
						</div>
{{--						<div class="row">--}}
{{--							<div class="col-12 text-center">--}}
{{--								<button type="button" class="btn btn-outline-dark rounded-pill shadow-none">Show more</button>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--					<div class="tab-pane fade" id="pills-medicine" role="tabpanel" aria-labelledby="pills-medicine-tab">--}}
{{--						<div class="row">--}}
{{--									<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4'>--}}
{{--										<div class='card rounded-0'>--}}
{{--											<div class='view zoom'>--}}
{{--												<a href='#' class='badge-off'><i class='fa fa-heart'></i></a>--}}
{{--												<a href='#' class='badge-new'><i class='fa fa-share-alt'></i></a>--}}
{{--												 <a href='#'><img src='$images[$i]' class='img-fluid'/></a>--}}
{{--											</div>--}}
{{--											<div class='card-body'>--}}
{{--												<div class='font-sm'>MEDICAL TYPE</div>--}}
{{--												<div class='mb-2'><a href='#' class='black-text'>Doctor, Clinic, Hospital Name</a></div>--}}
{{--												<div class='font-sm'><i class='fa fa-clock'></i> 10:00 AM - 08:00 PM</div>--}}
{{--												<div class='font-sm mb-3'>--}}
{{--													<i class='fa fa-star text-warning'></i>--}}
{{--													<i class='fa fa-star text-warning'></i>--}}
{{--													<i class='fa fa-star text-warning'></i>--}}
{{--													<i class='fa fa-star text-warning'></i>--}}
{{--													<i class='fa fa-star text-warning'></i>--}}
{{--												</div>--}}
{{--												<div class='font-sm mb-3'><i class='fa fa-map-marker-alt'></i> Varanasi</div>--}}
{{--											</div>--}}
{{--											<div class='card-footer bg-white font-sm'>--}}
{{--												<div class='row'>--}}
{{--													<div class='col-6'>--}}
{{--														<a href='#' class='text-dark'><i class='fa fa-eye'></i> Visit Website</a>--}}
{{--													</div>--}}
{{--													<div class='col-6'>--}}
{{--														<a href='#' class='text-dark'><i class='fa fa-phone-alt'></i> Appointment</a>--}}
{{--													</div>--}}
{{--												</div>--}}
{{--											</div>--}}
{{--										</div>--}}
{{--									</div>--}}

{{--						</div>--}}
{{--						<div class="row">--}}
{{--							<div class="col-12 text-center">--}}
{{--								<button type="button" class="btn btn-outline-dark rounded-pill shadow-none">Show more</button>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--	</div>--}}
{{--</div>--}}

<div class="records cp-md h-100">
	<div class="row justify-content-center">
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 mb-4 text-center text-white">
			<div class="circle mb-3">
                <i class="fas fa-users fa-2x"></i>
			</div>
			<div class="lead">Service Providers</div>
			<div class="h2-responsive">@if(isset($t_sp)) {{$t_sp}}@endif</div>
		</div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 mb-4 text-center text-white">
			<div class="circle mb-3">
                <i class="fas fa-user-md fa-2x"></i>
			</div>
			<div class="lead">Doctors</div>
			<div class="h2-responsive">@if(isset($t_doc)) {{$t_doc}}@endif</div>
		</div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 mb-4 text-center text-white">
			<div class="circle mb-3">
                <i class="fas fa-door-open fa-2x"></i>
			</div>
			<div class="lead">OMED Centers</div>
			<div class="h2-responsive">@if(isset($t_omed_center)) {{$t_omed_center}}@endif</div>
		</div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 mb-4 text-center text-white">
			<div class="circle mb-3">
				<i class="fa fa-smile fa-lg text-white fa-2x"></i>
			</div>
			<div class="lead">Patients</div>
			<div class="h2-responsive">@if(isset($t_patient)) {{$t_patient}}@endif</div>
		</div>
        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 mb-4 text-center text-white">
            <div class="circle mb-3">
                <i class="fa fa-globe fa-lg text-white fa-2x"></i>
            </div>
            <div class="lead">Cities</div>
            <div class="h2-responsive">@if(isset($t_city)) {{$t_city}}@endif</div>
        </div>

	</div>
</div>

{{--<div class="container-fluid bg-light cp-md">--}}
{{--	<div class="container">--}}
{{--		<div class="row">--}}
{{--			<div class="col-12 text-center">--}}
{{--				<div class="h2-responsive mb-5">Latest Articles</div>--}}
{{--				<div class="mb-5">Vivamus suscipit quis sem pellentesque tincidunt Etiam luctus elementum eros ac laoreet.</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--		<div class="row">--}}
{{--			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4">--}}
{{--				<div class="card">--}}
{{--					<div class="view zoom">--}}
{{--						<a href="#"><img src="{{asset("web/img/article1.png")}}" class="img-fluid"></a>--}}
{{--					</div>--}}
{{--					<div class="card-body">--}}
{{--						<a href="#" class="lead text-dark"></a>--}}
{{--						<div class="font-sm text-black-50 mb-4"></div>--}}
{{--						<p></p>--}}
{{--						<div>--}}
{{--							<a href="#" class="text-black-50 mr-4"><i class="fa fa-heart"></i> 0</a>--}}
{{--							<a href="#" class="text-black-50"><i class="fa fa-share-alt"></i></a>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4">--}}
{{--				<div class="card">--}}
{{--					<div class="view zoom">--}}
{{--						<a href="#"><img src="{{asset("web/img/article2.png")}}" class="img-fluid"></a>--}}
{{--					</div>--}}
{{--					<div class="card-body">--}}
{{--						<a href="#" class="lead text-dark"></a>--}}
{{--						<div class="font-sm text-black-50 mb-4"></div>--}}
{{--						<p></p>--}}
{{--						<div>--}}
{{--							<a href="#" class="text-black-50 mr-4"><i class="fa fa-heart"></i> 0</a>--}}
{{--							<a href="#" class="text-black-50"><i class="fa fa-share-alt"></i></a>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4">--}}
{{--				<div class="card">--}}
{{--					<div class="view zoom">--}}
{{--						<a href="#"><img src="{{asset("web/img/article3.png")}}" class="img-fluid"></a>--}}
{{--					</div>--}}
{{--					<div class="card-body">--}}
{{--						<a href="#" class="lead text-dark"></a>--}}
{{--						<div class="font-sm text-black-50 mb-4"></div>--}}
{{--						<p></p>--}}
{{--						<div>--}}
{{--							<a href="#" class="text-black-50 mr-4"><i class="fa fa-heart"></i> 0</a>--}}
{{--							<a href="#" class="text-black-50"><i class="fa fa-share-alt"></i></a>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--	</div>--}}
{{--</div>--}}

{{--<div class="bg-primary cp-md">--}}
{{--	<div class="container">--}}
{{--		<div class="row">--}}
{{--			<div class="col-12 text-center text-white">--}}
{{--				<div class="h2-responsive mb-5">Testimonials</div>--}}
{{--				<div class="mb-5">Vivamus suscipit quis sem pellentesque tincidunt Etiam luctus elementum eros ac laoreet.</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--		<div class="row justify-content-center">--}}
{{--			<div class="col-xl-8 col-lg-8 col-md-8 col-12">--}}
{{--				<div id="testimonial" class="owl-carousel owl-theme">--}}
{{--					<div class="item text-center text-white">--}}
{{--						<p class="mb-5"></p>--}}
{{--						<div class="lead"></div>--}}
{{--						<div class="font-sm mb-3">--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--						</div>--}}
{{--						<div><img src="{{asset("web/img/test1.png")}}" class="img-fluid img-thumbnail"></div>--}}
{{--					</div>--}}
{{--					<div class="item text-center text-white">--}}
{{--						<p class="mb-5"></p>--}}
{{--						<div class="lead"></div>--}}
{{--						<div class="font-sm mb-3">--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--						</div>--}}
{{--						<div><img src="{{asset("web/img/test2.png")}}" class="img-fluid img-thumbnail"></div>--}}
{{--					</div>--}}
{{--					<div class="item text-center text-white">--}}
{{--						<p class="mb-5"></p>--}}
{{--						<div class="lead"></div>--}}
{{--						<div class="font-sm mb-3">--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--							<i class="fa fa-star text-warning"></i>--}}
{{--						</div>--}}
{{--						<div><img src="{{asset("web/img/test3.png")}}" class="img-fluid img-thumbnail"></div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--	</div>--}}
{{--</div>--}}

{{--<div class="container-fluid bg-light cp-md">--}}
{{--	<div class="container">--}}
{{--		<div class="row">--}}
{{--			<div class="col-12 text-center">--}}
{{--				<div class="h2-responsive mb-5">Our Clients</div>--}}
{{--				<div class="mb-5">Vivamus suscipit quis sem pellentesque tincidunt Etiam luctus elementum eros ac laoreet.</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--		<div class="row">--}}
{{--			<div class="col-12">--}}
{{--				<div id="clients" class="owl-carousel owl-theme">--}}

{{--				</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--	</div>--}}
{{--</div>--}}

{{--<div class="container cp-md">--}}
{{--	<div class="row">--}}
{{--		<div class="col-12 text-center">--}}
{{--			<div class="h2-responsive mb-5">Best Rated Locations</div>--}}
{{--			<div class="mb-5">Vivamus suscipit quis sem pellentesque tincidunt Etiam luctus elementum eros ac laoreet.</div>--}}
{{--		</div>--}}
{{--	</div>--}}
{{--	<div class="row">--}}
{{--		<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">--}}
{{--			<div class="card">--}}
{{--				<div class="view overlay zoom">--}}
{{--					<a href="#"><img src="{{asset("web/img/location1.png")}}" class="img-fluid"></a>--}}
{{--					<div class="mask flex-center waves-effect waves-light rgba-blue-strong"></div>--}}
{{--				</div>--}}
{{--				<div class="card-body">--}}
{{--					<div class="row">--}}
{{--						<div class="col-6 h3 align-self-center">12,345</div>--}}
{{--						<div class="col-6 font-sm align-self-center"><i class="fa fa-map-marker-alt"></i> Varanasi</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--		<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">--}}
{{--			<div class="card">--}}
{{--				<div class="view overlay zoom">--}}
{{--					<a href="#"><img src="{{asset("web/img/location2.png")}}" class="img-fluid"></a>--}}
{{--					<div class="mask flex-center waves-effect waves-light rgba-blue-strong"></div>--}}
{{--				</div>--}}
{{--				<div class="card-body">--}}
{{--					<div class="row">--}}
{{--						<div class="col-6 h3 align-self-center">18,345</div>--}}
{{--						<div class="col-6 font-sm align-self-center"><i class="fa fa-map-marker-alt"></i> Lucknow</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--		<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">--}}
{{--			<div class="card">--}}
{{--				<div class="view overlay zoom">--}}
{{--					<a href="#"><img src="{{asset("web/img/location3.png")}}" class="img-fluid"></a>--}}
{{--					<div class="mask flex-center waves-effect waves-light rgba-blue-strong"></div>--}}
{{--				</div>--}}
{{--				<div class="card-body">--}}
{{--					<div class="row">--}}
{{--						<div class="col-6 h3 align-self-center">14,305</div>--}}
{{--						<div class="col-6 font-sm align-self-center"><i class="fa fa-map-marker-alt"></i> Kanpur</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--		<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">--}}
{{--			<div class="card">--}}
{{--				<div class="view overlay zoom">--}}
{{--					<a href="#"><img src="{{asset("web/img/location4.png")}}" class="img-fluid"></a>--}}
{{--					<div class="mask flex-center waves-effect waves-light rgba-blue-strong"></div>--}}
{{--				</div>--}}
{{--				<div class="card-body">--}}
{{--					<div class="row">--}}
{{--						<div class="col-6 h3 align-self-center">10,045</div>--}}
{{--						<div class="col-6 font-sm align-self-center"><i class="fa fa-map-marker-alt"></i> Allahabad</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--	</div>--}}
{{--</div>--}}

{{--<div class="container-fluid bg-light cp-md">--}}
{{--	<div class="container">--}}
{{--		<div class="row">--}}
{{--			<div class="col-12 text-center">--}}
{{--				<div class="h2-responsive mb-5">News &amp; Events</div>--}}
{{--				<div class="mb-5">Vivamus suscipit quis sem pellentesque tincidunt Etiam luctus elementum eros ac laoreet.</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--		<div class="row">--}}
{{--			<div class="col-12">--}}
{{--				<div id="events" class="owl-carousel owl-theme">--}}
{{--										<div class='item'>--}}
{{--							<div class='card'>--}}
{{--								<div class='view zoom'>--}}
{{--									 <a href='#'><img src='' class='img-fluid'></a>--}}
{{--								</div>--}}
{{--								<div class='card-body'>--}}
{{--									<div class='row text-black-50'>--}}
{{--										<div class='col-7'><i class='fa fa-calendar-alt'></i> Jan-02-2020</div>--}}
{{--										<div class='col-5'><i class='fa fa-comment-alt'></i> 5 Comments</div>--}}
{{--									</div>--}}
{{--									<a href='#' class='lead text-blue d-block mb-2'></a>--}}
{{--									<div class='mb-4'></div>--}}
{{--									<div class='font-sm'>--}}
{{--										<span class='mr-3 text-black-50'><i></i></span>--}}
{{--										<a href='#' class='text-black-50 mr-3'><i class='fa fa-heart'></i></a>--}}
{{--										<a href='#' class='text-black-50'><i class='fa fa-thumbs-up'></i></a>--}}
{{--									</div>--}}
{{--								</div>--}}
{{--							</div>--}}
{{--						</div>--}}

{{--				</div>--}}
{{--			</div>--}}
{{--		</div>--}}
{{--	</div>--}}
{{--</div>--}}

{{--<div class="container cp-md">--}}
{{--	<div class="row">--}}
{{--		<div class="col-12 text-center">--}}
{{--			<div class="h2-responsive mb-2">Download App</div>--}}
{{--		</div>--}}
{{--	</div>--}}
{{--	<div class="row">--}}
{{--		<div class="col-12 text-center">--}}
{{--			<a href="https://play.google.com/store/apps/details?id=com.app.omeddr&hl=en"><img src="{{asset("web/img/google-plus.png")}}" width="180"></a>--}}
{{--		</div>--}}
{{--	</div>--}}
{{--</div>--}}
@endsection
