
@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Payment Response</div>
                </div>
            </div>
        </div>
    </div>
    <div class="container cp-md">
        <div class="card">
            <div class="card-body">
                @if(isset($transaction_status))
@if($transaction_status == "SUCCESS" || $transaction_status == "PENDING")
    <script>
        location.replace('{{url('review-consultation-request/'.$booking_id)}}');
    </script>
                    @endif
                @endif

                    @if(isset($txStatus) && ($txStatus == "CANCELLED" || $txStatus == "FAILED"))
                        <div class="text-center">
                            <h3>Transaction has been failed / canceled.</h3>
                            @if(isset($booking) && $booking!='')
                                <a href="{{url('review-consultation-request/'.$booking->id)}}" class="btn btn-info">Click here to retry.</a>
                            @endif
                        </div>

                    @endif

            </div>
        </div>
    </div>
@endsection

