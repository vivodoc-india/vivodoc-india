@extends('layouts.layout')
@section('content')
<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">Register</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid cp-lg bg-light">
	<div class="row justify-content-center">
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
			<div class="card rounded-0">
				<div class="card-header bg-white text-center">
					<img src="{{asset('web/v2/img/logo/logo.svg')}}" width="150">
				</div>
				<div class="card-body">
					<div class="form-group">
						<input type="text" class="form-control rounded-0" placeholder="Email address">
					</div>
					<div class="form-group">
						<input type="password" class="form-control rounded-0" placeholder="Password">
					</div>
					<button type="button" class="btn btn-primary rounded-pill btn-block">Login</button>
					<div class="mt-3 text-center"><a href="#">Forgot Password?</a></div>
				<div class="mt-3 text-center">Don't have an account? <a href="{{url('registration')}}">Sign Up</a></div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
