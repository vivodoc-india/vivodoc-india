@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Guidelines on Online Consultation</div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cp-md">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="h3-responsive mb-2">Guidelines on Online Consultation</div>
                    <div class="row">
                        <div class="col-xl-8 col-md-8 col-lg-8 col-sm-12">
                            <div class="lead mb-2">
                                <p>Check the Guideline for Telemedecine Provided by Government Of India. </p>
                            </div>
                            <div class="text-center">
                                <a class="btn btn-md btn-amber" href="{{asset('uploads/219374.pdf')}}" download="219374.pdf">Download Now</a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-pg-4 col-md-4 col-sm-12">

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection
