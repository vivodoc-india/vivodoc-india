@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Patient Enquiry Reply</div>
                </div>
            </div>
        </div>
    </div>
    @include('web.custome-menu')
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                    <div class="card bg-light shadow-none">
                        <div class="card-body">
                                <div class="card mb-3">
                                    <div class="card-body">
                                        @if(isset($enquiry))
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 mb-2">
<h5 class="h5-responsive">Q. {{ucfirst($enquiry->question)}}</h5>
                                            </div>
                                            @if($enquiry->provider_reply!='' || $enquiry->admin_reply!='')
           <div class="col-lg-11 col-md-11 col-sm-11 offset-1 mb-2">

               @if($enquiry->provider_reply!='')  <h6 class="h6-responsive"> <span><i class="fas fa-reply"></i></span> {{ucfirst($enquiry->provider_reply)}}</h6>
                   <p>Replied By: You  {{date('d-m-Y',strtotime($enquiry->updated_at))}}</p>
                   @endif
                   @if($enquiry->admin_reply!='')  <h6 class="h6-responsive"><span><i class="fas fa-reply"></i></span> {{ucfirst($enquiry->admin_reply)}}</h6>
                   <p class="ml-2">Replied By: Admin  {{date('d-m-Y',strtotime($enquiry->admin_updated_at))}}</p>
                   @endif
                                                </div>
                                                @endif
                                        </div>
                                            @endif
                                        <!-- Files Uploaded By Patient If Any -->
                                            @if($enquiry->pat_directory !='' && $enquiry->pat_files!= '')
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 mb-3">
                                                        <h6>File Uploaded By Patient:</h6>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    @php $images=explode(',',$enquiry->pat_files)  @endphp
                                                    @for($i=0;$i<count($images);$i++)
                                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-3">

                                                            <!-- Card -->
                                                            <div class="card h-100">

                                                                <!-- Card image -->
                                                                <a href="{{asset('uploads/patientprofile/enquiry/'.$enquiry->pat_directory.'/'.$images[$i])}}" target="_blank">
                                                                    <img class="card-img-top" src="{{asset('uploads/patientprofile/enquiry/'.$enquiry->pat_directory.'/'.$images[$i])}}" alt="{{$images[$i]}}" height="200">
                                                                </a>
                                                                <!-- Card content -->
                                                                {{--                                                                <div class="card-body">--}}
                                                                {{--                                                                    --}}
                                                                {{--                                                                </div>--}}

                                                            </div>
                                                        </div>
                                                @endfor
                                                <!-- Card -->

                                                </div>

                                            @endif

                                            <!-- Files uploaded by service provider id any -->
                                            @if(!empty($enquiry->prov_directory) && !empty($enquiry->prov_files!= ''))
                                                <div class="row mt-3">
                                                    <div class="col-lg-12 col-md-12 mb-3">
                                                        <h6>File Uploaded By You:</h6>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    @php $images=explode(',', $enquiry->prov_files)  @endphp
                                                    @for($i=0;$i<count($images);$i++)
                                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-3">

                                                            <!-- Card -->
                                                            <div class="card h-100">

                                                                <!-- Card image -->
                                                                <a href="{{asset('uploads/patientprofile/enquiry/'.$enquiry->prov_directory.'/'.$images[$i])}}" target="_blank">
                                                                    <img class="card-img-top" src="{{asset('uploads/patientprofile/enquiry/'.$enquiry->prov_directory.'/'.$images[$i])}}" alt="{{$images[$i]}}" height="200">
                                                                </a>
                                                                <!-- Card content -->
                                                                {{--                                                                <div class="card-body">--}}
                                                                {{--                                                                    --}}
                                                                {{--                                                                </div>--}}

                                                            </div>
                                                        </div>
                                                @endfor
                                                <!-- Card -->

                                                </div>

                                            @endif

                                        <div class="row mt-5">
                                            <div class="col-lg-8 col-md-8 col-sm-12 mb-2">
                                                <form action="{{url('save-reply')}}" method="post" enctype="multipart/form-data">
                   <input type="hidden" name="reply_of" value="{{$enquiry->id}}">
                                                    @csrf

                       <textarea name="provider_reply" placeholder="enter your reply here" required class="form-control"></textarea>
                                     <div class="row mt-2">
                                         <div class="col-xl-12 col-lg-12 col-sm-12">
                                             <label class="control-label ml-2">Upload Files (i.e. Prescription, Reports etc.)</label>
                                             <div class="custom-file">
                                                 <input type="file" class="custom-file-input  @error('prov_doc') is-invalid @enderror" id="picFile" name="prov_doc[]" multiple="multiple">
                                                 <label class="custom-file-label" for="picFile">Choose file</label>
                                             </div>
                                             <span class="misc">Allowed file type: jpg, jpeg, png.</span>
                                             @error('prov_doc')
                                             <div class="alert alert-danger">{{ $message }}</div>
                                             @enderror
                                         </div>
                                     </div>

                                                    <button type="submit" class="btn btn-info mt-3 mb-2">Submit</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
            </div>

        </div>
    </div>
@endsection

