@extends('layouts.layout')
@section('content')
<section class="main">
    <div class="profile-view-section">

    <div class="container">
	<div class="row">
		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-4">
			<p>VivoDoc works with leading hospitals, experienced doctors, diagnostic centers, Pharmacies, Ambulances and other Health services providers, to improve health outcomes for patients everywhere, as well as profitability for our partners. Our strong partner relationship makes deployment and speed-to-market easy to achieve. Join the growing community of VivoDoc partners across India. </p>
			<p>VivoDoc also offers Franchise of its Telemedicine Services.</p>
			<p>Vivo Clinics (An advance Telemedicine system with X-ray viewer, ECG, Otoscope, Dermascope, Digital stethoscope etc.) </p>
			<p>And</p>
			<p>Bike Doc (An advance Telemedicine system on a Bike Ambulance (24X7) services with Instant diagnostic, ECG, Emergency drugs and Sample collection facilities)</p>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
			<img src="https://images.pexels.com/photos/3184603/pexels-photo-3184603.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" class="img-fluid img-thumbnail">
		</div>
	</div>
</div>
    </div>
</section>
@endsection
