@extends('layouts.layout')
@section('content')
<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">Doctor Appointment</div>
			</div>
		</div>
	</div>
</div>
<div class="container cp-md">
	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
			<div class="view zoom">
				<img src="{{asset('web/img/162583.jpeg')}}" class="img-fluid">
			</div>
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
			<div class="h2-responsive mb-2">Doctor Appointment</div>
			<div class="h5-responsive mb-3">Fix Appointment with Multi specialty Doctors and Also for Doctor's Home Visit ( AAH Doctors only)</div>
			<div class="h5-responsive mb-3">Get Online Consultation of Multi specialty Doctors with Digital stethoscope, ENTscope etc. @ AAH Digital Health Centers &amp; AAH Digital Mobile Clinics (BIKE DOC)</div>
			<p>Get appointment of our Organization`s Multi specialist Doctors & Super specialist Doctors, Dentists & AYUSH (Ayurvidic, Unani, Homeopathic etc.) Doctors along with Physiotherapists & Dietitians.</p>
			<ul>
				<li>You can also ask appointment of your favorite doctors, the appointment will be taken on your behalf with online payment to us. (our Fee will be < than the actual Fee  of the respective Doctor)</li>
				<li>Online Consultation can be done with digital devices (Digital Stethoscope, ENT Scopes, Dermascope ECG etc.) at our multiple AAH Digital Health Centers and @ Home by Bike Doc (Digital Mobile Clinic)</li>
				<li>The Online Fee for the Superspecialist Doctors is Rs-350, for Other Specialist Doctors is Rs-250 and for the rest of the Doctors (MBBS, Dentist, AYUSH etc.) is Rs-150 (This Fee structure is only for the Doctors of our Organization)</li>
				<li>The Online Fee for the Doctors of your choice will be as per the respective Doctors actual Fee.</li>
			</ul>
			<a type="button" class="btn btn-danger rounded-pill" href="{{url('search?search_for=1')}}">Book Now</a>
		</div>
	</div>
</div>
@endsection
