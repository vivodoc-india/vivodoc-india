@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
    @include('web.custome-menu')
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h6>Approx Appointment Date & Time </h6>
            </div>
            <div class="card-body">
            <div class="row">
                <div class="col-4">
                  <h6 class="font-weight-bold">  Approx Date & Time </h6> <span class="badge badge-success">{{date('d-m-Y h:i:s a',strtotime($booking->appointment_time))}}</span>
                </div>

                    <div class="col-8">
                <h6>Update Appointment Date & Time</h6>
                        <br>
                        <form action="{{url('update-appointment-time')}}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{$booking->id}}">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="control-label">Appointment Date</label>
                                        <input type="date" name="date" required class="form-control" placeholder="Select date">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="control-label">Appointment Time</label>
                                        <input type="time" name="time" required class="form-control" placeholder="Select time">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-sm btn-info">Save</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
           <div class="card mt-2">
            <div class="card-body">
                @if(isset($booking))
                    <form action="{{url('appointment-update')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="booking_id" value="{{$booking->id}}">
                        <div class="row">
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                                <img src="{{asset('web/v2/img/logo/logo.svg')}}" class="img-fluid">
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="h3-responsive mb-2">{{$booking->provider->name}}</div>
                                <div class="mb-2">{{$booking->provider->profile->speciality}}</div>
                                {{-- <div class="mb-2">Specification</div> --}}
                                <div class="mb-2">{{$booking->provider->profile->qualification}}</div>
                                <div class="mb-2">{{$booking->provider->profile->other_speciality}}</div>
                                <div class="mb-2">{{$booking->provider->profile->address}}</div>
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <label class="control-label">Patient's Name <span class="req">*</span></label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Patient Name" name="patient_name" required value="{{$booking->patient_name}}">
                                </div>
                                @error('patient_name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="row">
                                    <div class="col-6">
                                        <label class="control-label">Patient's Age <span class="req">*</span></label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Age" name="age" required value="{{$booking->age}}">
                                        </div>
                                        @error('age')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <label class="control-label">Patient's Gender <span class="req">*</span></label>
                                        <select class="form-control" name="gender" required>
                                            <option selected disabled>Choose Gender</option>
                                            <option @if($booking->gender === "Male"){{'selected'}}@endif>Male</option>
                                            <option @if($booking->gender === "Female"){{'selected'}}@endif>Female</option>
                                        </select>
                                        @error('gender')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <label class="control-label">Patient's Mobile </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Mobile" name="mobile" value="{{$booking->mobile}}">
                                        </div>
                                        @error('mobile')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="col-6">
                                        <label class="control-label">Booking No </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" value="{{$booking->booking_number}}" disabled>
                                        </div>
                                        @error('booking_number')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Patient's Address <span class="req">*</span></label>
                                    <input type="text" class="form-control" placeholder="Address" name="address" required value="{{$booking->address}}">
                                </div>
                                @error('address')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror

                            </div>
                        </div>
                        <hr>

                        <div class="row mt-4">
                            <div class="col-md-3 col-12">
                                <label class="control-label">Patient's BP </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="BP" name="bp" value="{{$booking->bp}}">
                                </div>
                                @error('bp')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-3 col-12">
                                <label class="control-label">Patient's Pulse </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Pulse" name="pulse" value="{{$booking->pulse}}">
                                </div>
                                @error('pulse')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-3 col-12">
                                <label class="control-label">Patient's Temperature </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Temperature" name="temperature" value="{{$booking->temperature}}">
                                </div>
                                @error('temperature')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-3 col-12">
                                <label class="control-label">Patient's SPO<sub>2</sub> </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="SPO2" name="spo" value="{{$booking->spo}}">
                                </div>
                                @error('spo')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-3 col-12">
                                <label class="control-label">Patient's Weight </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Weight" name="weight" value="{{$booking->weight}}">
                                </div>
                                @error('weight')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-3 col-12">
                                <label class="control-label">History </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="History" name="history" value="{{$booking->history}}">
                                </div>
                                @error('history')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-3 col-12">
                                <label class="control-label">Complain </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Complain" name="complain" value="{{isset($booking->complain) && $booking->complain!=''?$booking->complain:old('complain')}}">
                                </div>
                                @error('complain')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-3 col-12">
                                <label class="control-label">P. Diagnosis </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Diagnosis" name="diagnostic"
                                           required value="{{isset($booking->diagnostic) && $booking->diagnostic!=''?$booking->diagnostic:old('diagnostic')}}">
                                </div>
                                @error('diagnostic')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-md-6 col-12 mb-4">
                                <label class="control-label">Doctor Note/Prescriptions </label>
                                <div class="form-group mb-3">
                                <textarea rows="16" class="form-control" placeholder="Doctor Note / Prescriptions"
                                          name="prescription" id="editor">@if(isset($booking))
                                        {{$booking->prescription}} @endif</textarea>
                                </div>
                                @error('prescription')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="mt-1 mb-1">
                                    <h6 class="text-center" style="font-weight:bold">OR</h6>
                                </div>
                                                            <div class="form-group">
                                <label class="control-label">Upload Prescriptions</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="prescription_image">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                                @error('prescription_image')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror

                                                            </div>

                                <div class="form-group">
                                    <label class="control-label">Remarks <span class="req">*</span></label>
                                    <textarea rows="5" class="form-control" placeholder="Completed/ Canceled/ Pending"
                                              name="remarks">{{isset($booking) && $booking->remarks!=''?$booking->remarks:''}}</textarea>
                                    @error('remarks')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>


                            </div>

                            <div class="col-md-6 col-12">
                                <label class="control-label">Uploaded Note/Prescriptions</label>
                                <div style="min-height:400px; width:100%; border:1px solid #C7C7C7; padding: 5px;">

                                    @if(!empty($booking->prescription))
                                        {!! $booking->prescription !!}
                                    @endif
                                    @if($booking->prescription_image!='')
                                        <img src="{{asset('uploads/patientprofile/prescription/'.$booking->prescription_folder.'/'.$booking->prescription_image)}}" class="img-fluid">
                                    @endif
                                    @if(empty($booking->prescription) || empty($booking->precsription_image))
                                        {{'Uploaded Note & Prescriptions'}}
                                    @endif
                                </div>
                            </div>

                        </div>
                        <input type="hidden" name="provider_id" value="{{$booking->provider_id}}">
                        <input type="hidden" name="patient_id" value="{{$booking->patient_id}}">
                        <div class="row mt-1">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <button type="submit" class="btn btn-primary" id="">Update</button>
                            </div>
                        </div>

                    </form>
                @endif
            </div>
        </div>
    </div>
    </div>
    </section>
@endsection
@section('additional_script')
    <script src="https://cdn.ckeditor.com/ckeditor5/22.0.0/classic/ckeditor.js"></script>

    <script>
        @if($booking->is_request_accepted==1 && $booking->is_paid==0 && $booking->is_con_link_available==1)
        paymentStat({{$booking->id}});
        @endif
        ClassicEditor
            .create( document.querySelector( '#editor' ),{
                toolbar: {
                    items: [
                        "heading",
                        "|",
                        "bold",
                        "italic",
                        "bulletedList",
                        "numberedList",
                        // "|",
                        // "indent",
                        // "outdent",
                        "blockQuote",
                        "|",
                        "undo",
                        "redo"
                    ]
                },
                language: 'en'
            } )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );

        $('document').ready(function(){

            $('#btn-join-conference').click(function(){
                $('#conference-frame').toggle();
            });
            $('#btn-accept').click(function(){
                var req_type= "is_request_accepted";
                var status= true;
                var book_id = $(this).attr('data');
                requestResponse(req_type,status,book_id);
            });

            $('#btn-reject').click(function(){
                var req_type= "is_request_rejected";
                var status= true;
                var book_id = $(this).attr('data');
                requestResponse(req_type,status,book_id);
            });
        });

        function requestResponse(req_type,status,book_id)
        {
            $.ajax({
                type:'ajax',
                method:'post',
                data:{'resp_request':req_type,'status':status,'id':book_id,'_token':"{{ csrf_token() }}"},
                url:'{{url('consultation-response')}}',
                asycn:true,
                dataType:'JSON',
                success:function(response){
                    if(response.success){
                        alertify.set('notifier','position', 'top-right');
                        alertify.success(response.message);
                        $('#mg-info').removeClass('text-danger').addClass('text-info').text(response.message);
                        $('#btn-reject,#btn-accept').hide();
                        if(response.hasOwnProperty(resp_code))
                        {
                            paymentStat(response.resp_code);
                        }

                    }
                    else{
                        alertify.set('notifier','position', 'top-right');
                        alertify.error(response.message);
                    }
                },
                error:function(){
                    alertify.set('notifier','position', 'top-right');
                    alertify.error('Unable to process your request.');
                }
            });
        }
        function paymentStat(res_code) {
            interval = setInterval( function() {
                $.ajax({
                    type:'ajax',
                    method:'post',
                    data:{
                        '_token':"{{ csrf_token() }}",
                        'res_code' : res_code,
                        'uid': "{{Auth::user()->id}}",

                    },
                    url:"{{url('checkPaymentStatus')}}",
                    dataType:'json',
                    asycn:true,
                    success:function(response){
                        if(response.success)
                        {
                            clearInterval(interval);
                            location.reload();
                            // $('#btn-call').show().attr('data',response.link);
                        }
                        else{
                            if(response.stop)
                            {
                                clearInterval(interval);
                                // $('#btn-call').hide().removeAttr('data');
                                location.reload();
                            }
                            else{
                                paymentStat(response.res_code);
                            }
                        }
                    }
                });
            }, 1000*15);
        }
    </script>
@endsection

