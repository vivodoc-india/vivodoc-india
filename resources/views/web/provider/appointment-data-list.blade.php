@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Medical History</div>
                </div>
            </div>
        </div>
    </div>
    @include('web.custome-menu')
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
        <div class="card mb-4">
            <div class="card-body">
                <table id="example" class="display responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Patient Name</th>
                        <th>Doctor's Name</th>
                        <th>Patient Age</th>
                        <th>Booking Date</th>
                        <th>Requested By</th>
{{--                        <th>Status</th>--}}
                        <th>View</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($list) && count($list) > 0)
                        @php $n=1 @endphp
                        @foreach($list as $rec)
                            <tr>
                                <td>{{$n}}</td>
                                <td>{{ucwords($rec->patient_name)}}</td>
                                <td>{{ucwords($rec->provider->name)}}</td>
                                <td>{{$rec->age}}</td>
                                <td>{{date('d-m-Y',strtotime($rec->booking_date))}}</td>
                                <td>{{ucwords($rec->patient->name)}}</td>
{{--                                <td>{{$rec->is_completed==1 ? 'Completed':'Pending'}}</td>--}}
                                <td>
                                    <a type="button" class="btn btn-light btn-sm"
                                       href="{{url('view-appointment-booking-data/'.$rec->id)}}">
                                        <i class="fas fa-eye"></i> </a>
                                </td>

                            </tr>
                            @php $n++ @endphp
                        @endforeach
                    @endif
                    </tbody>

                </table>
            </div>
        </div>

    </div>
    </div>
    </div>
@endsection
