@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
    @include('web.custome-menu')
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
        <div class="card mb-4">
            <div class="card-body">
                <table id="example" class="display responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Patient Name</th>
                        <th>Doctor's Name</th>
                        <th>Patient Age</th>
                        <th>Consulted On</th>
                        <th>Requested By</th>
                        <th>View</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($list) && count($list) > 0)
                        @php $n=1 @endphp
                        @foreach($list as $rec)
                            <tr>
                                <td>{{$n}}</td>
                                <td>{{ucwords($rec->patient_name)}}</td>
                                <td>{{ucwords($rec->provider->name)}}</td>
                                <td>{{$rec->age}}</td>
                                <td>{{date('d-m-Y',strtotime($rec->booking_date))}}</td>
                                <td>{{ucwords($rec->patient->name)}}</td>
                                <td>
                                    <a type="button" class="btn btn-light btn-sm"
                                       href="{{url('view-live-consultation-data/'.$rec->id)}}">
                                        <i class="fas fa-eye"></i> </a>
                                </td>

                            </tr>
                            @php $n++ @endphp
                        @endforeach
                    @endif
                    </tbody>

                </table>
            </div>
        </div>

    </div>
    </div>
    </section>
@endsection
