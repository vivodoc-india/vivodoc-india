@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
@include('web.custome-menu')
		<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 pt-4 pb-4">
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active  btn-block radius-0" id="pills-home-tab" data-toggle="pill" href="#pills-personal" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fa fa-user-circle" aria-hidden="true"></i>
 Personal Info</a>
  </li>

  <li class="nav-item">
    <a class="nav-link radius-0  btn-block" id="pills-contact-tab btn-primary" data-toggle="pill" href="#pills-doc" role="tab" aria-controls="pills-contact" aria-selected="false"><i class="fas fa-cloud-upload-alt"></i> Photo Upload</a>
  </li>
    <li class="nav-item">
        <a class="nav-link radius-0  btn-block" id="pills-contact-tab btn-primary" data-toggle="pill" href="#pills-password" role="tab" aria-controls="pills-password" aria-selected="false">
            <i class="fas fa-key"></i> Change Password</a>
    </li>
{{--    <li class="nav-item">--}}
{{--        <a class="nav-link radius-0  btn-block" id="pills-contact-tab btn-primary" data-toggle="pill"--}}
{{--           href="#pills-live-status" role="tab" aria-controls="pills-password" aria-selected="false">--}}
{{--            <i class="fas fa-globe"></i> Change Live Status</a>--}}
{{--    </li>--}}
</ul>
<div class="tab-content" id="pills-tabContent">
    <!-- Personal Info Body -->
  <div class="tab-pane fade show active" id="pills-personal" role="tabpanel" aria-labelledby="pills-home-tab">
<div class="card">
				<div class="card-body">
                   <form action="{{url('updatePersonalInfo')}}" method="post" id="frm-personal">
                       @csrf
        <h3 class="mb-3">Personal Info</h3>
					<div class="row mb-3">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                            <label class="control-label">Name <span class="req">*</span></label>
                            <div class="form-group">
                                <input type="text" name="name"  placeholder="name"
                                       class="form-control border-round  rounded-0 @error('name') is-invalid @enderror" required value=" @if(isset($user->name)){{$user->name}}@endif">
                            </div>
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                            <label class="control-label">Mobile </label>
                            <div class="form-group">
                                <input type="text" name="mobile"  placeholder="mobile"
                                       class="form-control border-round  rounded-0 @error('mobile') is-invalid @enderror"
                                       value=" @if(isset($user->mobile)){{$user->mobile}}@endif">
                            </div>
                            @error('mobile')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                            <label class="control-label">Email Id </label>
                            <div class="form-group">
                                <input name="email"  type="email" placeholder="email"
                                       class="form-control border-round  rounded-0 @error('email') is-invalid @enderror"
                                       value=" @if(isset($user->email)){{$user->email}}@endif">
                            </div>
                            @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-3">
                            <label class="control-label">Gender</label><br>
                            <div class="custom-control custom-radio custom-control-inline">
    <input type="radio" class="custom-control-input  @error('gender') is-invalid @enderror"
           required id="customRadio" name="gender" value="Male" @if(isset($user->profile->gender) && $user->profile->gender =="Male"){{'checked'}}@endif>
    <label class="custom-control-label" for="customRadio">Male</label>
  </div>
  <div class="custom-control custom-radio custom-control-inline">
    <input type="radio" class="custom-control-input  @error('gender') is-invalid @enderror" id="customRadio2" name="gender" value="Female"  @if(isset($user->profile->gender) && $user->profile->gender=="Female"){{'checked'}}@endif>
    <label class="custom-control-label" for="customRadio2">Female</label>
  </div>
@error('gender')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                            <label class="control-label">Speciality <span class="req">*</span></label>
                        <div class="form-group">
                        <input name="speciality"  placeholder="speciality" class="form-control border-round  rounded-0 @error('speciality') is-invalid @enderror" required value=" @if(isset($user->profile->speciality)){{$user->profile->speciality}}@endif">
                </div>
                            @error('sepciality')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
                        </div>
                         <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                            <label class="control-label">Other Speciality </label>
                        <div class="form-group">
                        <input name="other_speciality"  placeholder="i.e. languages know or something else"
                               class="form-control border-round  rounded-0 @error('other_speciality') is-invalid @enderror"
                               value=" @if(isset($user->profile->other_speciality)){{$user->profile->other_speciality}}@endif"
                                required>
                </div>
                            @error('other_sepciality')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                            <label class="control-label">Experiance </label>
                        <div class="form-group">
                        <input name="experiance" type="number" min="0"  placeholder="experiance" class="form-control border-round  rounded-0
@error('experiance') is-invalid @enderror" required value="@if(isset($user->profile->experiance)){{$user->profile->experiance}}@endif">
                </div>
                            @error('experiance')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
                        </div>
						<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                             <label class="control-label">Pincode <span class="req">*</span></label>
							<div class="form-group">
                            <input type="text" name="pincode" class="form-control rounded-0 border-round @error('pincode') is-invalid @enderror" placeholder="pincode" required oninput='this.value=(parseInt(this.value)||" ")' maxlength="6" value="@if(isset($user->profile->pincode)){{$user->profile->pincode}}@endif">
                            </div>
                             @error('pincode')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                             <label class="control-label">Qualification <span class="req">*</span></label>
							<div class="form-group">
                            <input type="text" class="form-control rounded-0 border-round @error('qualification') is-invalid @enderror" placeholder="qualification : i.e. Phd.,M.B.B.S.,M.S. etc"
                                   name="qualification" required title="Use ',' to seperate entires"
                                   value="@if(isset($user->profile->qualification)){{$user->profile->qualification}}@endif">
                            </div>
                             @error('qualification')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <label class="control-label">Address <span class="req">*</span></label>
							<div class="form-group">
                            <textarea name="address" required class="form-control rounded-0 @error('address') is-invalid @enderror"
                                      placeholder="Address">@if(isset($user->profile->address)){{$user->profile->address}}@endif
                            </textarea>

                            </div>
                             @error('address')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
						</div>
					</div>

                       <h3 class="mb-3">Services Info</h3>
{{--                       @isset($list_add)--}}
{{--                           <div class="row">--}}
{{--                               <label class="control-label ml-2">Choose Service Provider Type</label>--}}
{{--                           </div>--}}
{{--                           <?php $n; $n=1; ?>--}}
{{--                           @if(isset($list_add))--}}
{{--                               @foreach($list_add as $l)--}}
{{--                                   <div class="custom-control custom-checkbox  ml-2">--}}
{{--                                       <input type="checkbox" class="custom-control-input @error('user_role_id') is-invalid @enderror" id="customCheck{{$n}}" name="user_role_id[]" value="{{$l->id}}">--}}
{{--                                       <label class="custom-control-label" for="customCheck{{$n}}">{{$l->name}}</label>--}}
{{--                                   </div>--}}
{{--                                   <?php $n++;?>--}}
{{--                               @endforeach--}}
{{--                           @endif--}}
{{--                           @error('user_role_id')--}}
{{--                           <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                           @enderror--}}
{{--                           @endif--}}

                       @if(isset($list_add))
                           <div class="row">
                               <label class="control-label ml-2">Choose Service Provider Type</label>
                           </div>
                           <?php $n; $n=1; ?>
                           @if(isset($list_add) && count($list_add) > 0)
                               @foreach($list_add as $l)
                                   <div class="custom-control custom-checkbox  ml-2">
                                       <input type="checkbox" class="custom-control-input @error('user_role_id') is-invalid @enderror" id="customCheck{{$n}}" name="user_role_id[]" value="{{$l->id}}">
                                       <label class="custom-control-label" for="customCheck{{$n}}">{{$l->name}}</label>
                                   </div>
                                   <?php $n++;?>
                               @endforeach
                           @else
                               <h3 class="text-primary">You are listed in all service provider category.</h3>
                           @endif
                           @error('user_role_id')
                           <div class="alert alert-danger">{{ $message }}</div>
                           @enderror

                           @endif
                    <button type="submit" class="btn btn-dark" id="btn-save-personal"><i class="fa fa-user-circle" aria-hidden="true"></i> Save</button>
                </form>
        </div>
      </div>
  </div>

  <!-- Personal Info Body Ends Here -->
  <!-- Services Body -->


    <!-- End of Add New Services Tab Body -->
    <!-- Document Upload Body-->
<div class="tab-pane fade" id="pills-doc" role="tabpanel" aria-labelledby="pills-contact-tab">
<div class="card">
<div class="card-body">
<form action="{{url('updateDocuments')}}" method="post" id="frm-document" enctype="multipart/form-data">
@csrf
<h3 class="mb-3">Upload Profile Photo</h3>
<div class="row">
  <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
<label class="control-label ml-2">Upload Your Photo</label>
 <div class="custom-file">
    <input type="file" class="custom-file-input  @error('photo') is-invalid @enderror" id="picFile" name="photo" reuqired>
    <label class="custom-file-label" for="picFile">Choose file</label>
  </div>
  <span class="misc">Allowed file type: jpg, jpeg, png.</span>
   @error('photo')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">

                            @if(isset($user->profile->photo) && $user->profile->photo !='')
<a href=""><img src="{{asset('uploads/profile/'.$user->profile->photo)}}" class="img-fluid img-thumbnail"></a>
                            @else
<a href=""><img src="{{asset('web/img/nobody.jpg')}}" class="img-fluid img-thumbnail"></a>
                            @endif

						</div>
</div>
 <button type="submit" class="btn btn-dark mt-3" id="btn-save-document"><i class="fas fa-cloud-upload-alt "></i> Save</button>
</form>
    </div>
</div>

  </div>
  <!-- Document Upload Body Ends Here -->

    <!-- Change Password -->
    <div class="tab-pane fade" id="pills-password" role="tabpanel" aria-labelledby="pills-contact-tab">
        <div class="card">
            <div class="card-body">
                <form action="{{url('changePassword')}}" method="post" id="frm-password">
                    @csrf
                    <h3 class="mb-3">Change Password</h3>
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 offset-md-3">
                            <label class="control-label">Current Password <span class="req">*</span></label>
                            <div class="form-group">
                                <input type="text" class="form-control rounded-0 border-round @error('current_password') is-invalid @enderror"
                                       placeholder="Enter your current password"
                                       name="current_password" required
                                       value="">
                            </div>
                            @error('current_password')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 offset-md-3">
                            <label class="control-label">New Password <span class="req">*</span></label>
                            <div class="form-group">
                                <input type="password" class="form-control rounded-0 border-round @error('new_password') is-invalid @enderror"
                                       placeholder="Enter your new password"
                                       name="new_password" required
                                       value="">
                            </div>
                            @error('new_password')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 offset-md-3">
                            <label class="control-label">Re-enter New Password <span class="req">*</span></label>
                            <div class="form-group">
                                <input type="password" class="form-control rounded-0 border-round @error('password_confirmation') is-invalid @enderror"
                                       placeholder="Re-nter your new password"
                                       name="password_confirmation" required
                                       value="" >
                            </div>
                            @error('password_confirmation')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-dark mt-3" id="btn-save-document">
                        <i class="fas fa-key "></i> Change Password</button>
                </form>
            </div>
        </div>

    </div>
    <!--Change Password Body Ends Here -->

    <!-- Change Live Status -->
{{--    <div class="tab-pane fade" id="pills-live-status" role="tabpanel" aria-labelledby="pills-contact-tab">--}}
{{--        <div class="card">--}}
{{--            <div class="card-body">--}}
{{--                <div class="container cp-sm ">--}}
{{--                    <div class="row">--}}
{{--<div class="col-md-3 text-center">--}}
{{--    <h4>Live Doctor</h4>--}}
{{--</div>--}}
{{--                        <div class="col-md-3 col-3 text-right">--}}
{{--                            <form action="{{url('change-live-status')}}" method="post" id="frm-live-stat">--}}
{{--                                @csrf--}}
{{--                                <label class="switch">--}}
{{--                                    <input type="checkbox" onchange="$('#frm-live-stat').submit();" name="liveval" @if(Auth::user()->user_role_id==0 && Auth::user()->profile()->count() > 0 &&  Auth::user()->profile->live_status ==1)--}}
{{--                                    {{'checked'}}@endif value="@if(Auth::user()->user_role_id==0 && Auth::user()->profile()->count() > 0 &&  Auth::user()->profile->live_status ==1)--}}
{{--                                        0 @else 1 @endif">--}}
{{--                                    <span class="slider round"></span>--}}
{{--                                </label>--}}
{{--                            </form>--}}

{{--                        </div>--}}
{{--                        <div class="col-md-1 col-1">--}}
{{--                            <div class="@if(Auth::user()->user_role_id==0 && Auth::user()->profile()->count() > 0 && Auth::user()->profile->live_status ==1)--}}
{{--                            {{'text-success'}} @else {{'text-danger'}} @endif" style="font-weight:bold;" id="live-status">--}}

{{--                                @if(Auth::user()--}}
{{--                                ->user_role_id==0 && Auth::user()->profile()->count() > 0 && Auth::user()--}}
{{--                                ->profile->live_status ==1)--}}
{{--                                    {{'Online'}} @else {{'Offline'}} @endif</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    @if(isset($has_ambulance) && $has_ambulance!='')--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-3 text-center">--}}
{{--                            <h4>Live Ambulance</h4>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-3 col-3 text-right">--}}
{{--                            <form action="{{url('ambulance-live-status')}}" method="post" id="frm-amb-live-stat">--}}
{{--                                @csrf--}}
{{--                                <input type="hidden" name="longitude" id="hid-longitude" value="">--}}
{{--                                <input type="hidden" name="latitude" id="hid-latitude" value="">--}}
{{--                                <label class="switch">--}}
{{--                                    <input type="checkbox" onchange="getLocation();" name="mark_live" @if($has_ambulance->mark_live == 1)--}}
{{--                                    {{'checked'}}@endif value="{{$has_ambulance->mark_live ==1 ? 0:1}}">--}}
{{--                                    <span class="slider round"></span>--}}
{{--                                </label>--}}
{{--                            </form>--}}

{{--                        </div>--}}
{{--                        <div class="col-md-1 col-1">--}}
{{--                            <div class="@if($has_ambulance->mark_live ==1)--}}
{{--                            {{'text-success'}} @else {{'text-danger'}} @endif" style="font-weight:bold;" id="live-status">--}}

{{--                                @if($has_ambulance->mark_live ==1)--}}
{{--                                    {{'Online'}} @else {{'Offline'}} @endif</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                @endif--}}
{{--                <form action="{{url('changePassword')}}" method="post" id="frm-password">--}}
{{--                    @csrf--}}
{{--                    <h3 class="mb-3">Change Password</h3>--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 offset-md-3">--}}
{{--                            <label class="control-label">Current Password <span class="req">*</span></label>--}}
{{--                            <div class="form-group">--}}
{{--                                <input type="text" class="form-control rounded-0 border-round @error('current_password') is-invalid @enderror"--}}
{{--                                       placeholder="Enter your current password"--}}
{{--                                       name="current_password" required--}}
{{--                                       value="">--}}
{{--                            </div>--}}
{{--                            @error('current_password')--}}
{{--                            <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 offset-md-3">--}}
{{--                            <label class="control-label">New Password <span class="req">*</span></label>--}}
{{--                            <div class="form-group">--}}
{{--                                <input type="password" class="form-control rounded-0 border-round @error('new_password') is-invalid @enderror"--}}
{{--                                       placeholder="Enter your new password"--}}
{{--                                       name="new_password" required--}}
{{--                                       value="">--}}
{{--                            </div>--}}
{{--                            @error('new_password')--}}
{{--                            <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 offset-md-3">--}}
{{--                            <label class="control-label">Re-enter New Password <span class="req">*</span></label>--}}
{{--                            <div class="form-group">--}}
{{--                                <input type="password" class="form-control rounded-0 border-round @error('password_confirmation') is-invalid @enderror"--}}
{{--                                       placeholder="Re-nter your new password"--}}
{{--                                       name="password_confirmation" required--}}
{{--                                       value="" >--}}
{{--                            </div>--}}
{{--                            @error('password_confirmation')--}}
{{--                            <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <button type="submit" class="btn btn-dark mt-3" id="btn-save-document">--}}
{{--                        <i class="fas fa-key "></i> Change Password</button>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--    </div>--}}

    <!-- Change Live Status body ends here -->
  </div>

</div>
			</div>
    </section>

@endsection

{{--@section('additional_script')--}}
{{--    <script>--}}

{{--        function showLocation(position) {--}}

{{--            var latitude = position.coords.latitude;--}}
{{--            var longitude = position.coords.longitude;--}}
{{--         $('#hid-latitude').val(latitude);--}}
{{--         $('#hid-longitude').val(longitude);--}}
{{--         $('#frm-amb-live-stat').submit();--}}
{{--        }--}}
{{--        function errorHandler(err) {--}}
{{--            if(err.code == 1) {--}}
{{--                alert("Error: Access is denied!");--}}
{{--            } else if( err.code == 2) {--}}
{{--                alert("Error: Position is unavailable!");--}}
{{--            }--}}
{{--        }--}}
{{--        function getLocation(){--}}
{{--            if(navigator.geolocation){--}}
{{--                // timeout at 60000 milliseconds (60 seconds)--}}
{{--                var options = {timeout:60000};--}}
{{--                navigator.geolocation.getCurrentPosition--}}
{{--                (showLocation, errorHandler, options);--}}
{{--            } else{--}}
{{--                alert("Sorry, browser does not support geolocation!");--}}
{{--            }--}}
{{--        }--}}
{{--    </script>--}}
{{--    @endsection--}}
