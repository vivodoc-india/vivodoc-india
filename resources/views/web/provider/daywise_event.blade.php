@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
    @include('web.custome-menu')
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
        <div class="card">
            <div class="card-body">
                <table id="example" class="display responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>S.no.</th>
                        <th>Date Date</th>
                        <th>Total Allowed Bookings</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $n=1 @endphp
                    @if(isset($bookings) && $bookings->isNotEmpty())
                        @foreach($bookings as $en)
                            <tr>
                                <td>{{$n}}</td>
                                <td>{{date('d-m-Y', strtotime($en->date))}}</td>
                                <td>{{$en->total_bookings}}</td>

                                <td>
                                    <a class="btn btn-info btn-sm" href="{{url('booking/edit/'.$en->id)}}"> <i class="fas fa-edit"></i> Edit</a>
{{--                                    <a type="button" class="btn btn-danger btn-sm delete_button_action" href="javascript:void(0)" data-url="{{url('booking/remove')}}" data-id="{{$en->id}}" data-toggle="modal" data-target="#deleteModal"> <i class="fas fa-trash-alt text-white"></i> Remove</a>--}}
                                </td>
                            </tr>
                            @php $n++ @endphp
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    </div>
    </section>
@endsection
@section('jquery-ui')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection
@section('additional_script')


    <script>
        $(document).ready(function(){
            $('.datepicker').attr("placeholder", "mm/dd/yyyy").datepicker({
                overrideBrowserDefault: true,
                minDate:0
            });


            // Manage week off or leave
            $('#btn-off-yes').click(function(){
                $('.div-t-book ,.div-timming,.div-s-time').hide();
                // $('#total_booking, #f-time , #t-time')
            });

            $('#btn-off-no').click(function(){
                $('.div-t-book ,.div-timming,.div-s-time').show();
            });

            $('.btn-add-timming').click(function(){
                var from=onTimeChange($('#f-time').val());
                var to = onTimeChange($('#t-time').val());

                $("#timings").append("<option selected>" + from+" - "+ to + "</option>");
                $('#f-time').val('');
                $('#t-time').val('');
            });

            $('#btn-event-submit').click(function(e){
                e.preventDefault();
                if($(".btn-off-radio:checked").val() === "no")
                {
                    if($('#total_booking').val()==" ")
                    {
                        alertify.error('Please enter total booking allowed per day.');
                    }
                    else if($('#timings').val() == " ")
                    {
                        alertify.error('Please enter your availability time.');
                    }
                    else{
                        $('#event-frm').submit();
                    }

                }
                else if($(".btn-off-radio:checked").val() === "yes") {
                    $('#event-frm').submit();
                }
                else{
                    alertify.error('Please make sure either it is week-off/leave or not..');
                }

            });
        });
        function onTimeChange(time) {
            var timeSplit = time.split(':'),
                hours,
                minutes,
                meridian;
            hours = timeSplit[0];
            minutes = timeSplit[1];
            if (hours > 12) {
                meridian = 'PM';
                hours -= 12;
            } else if (hours < 12) {
                meridian = 'AM';
                if (hours === 0) {
                    hours = 12;
                }
            } else {
                meridian = 'PM';
            }
            return hours + ':' + minutes + ' ' + meridian;
        }
    </script>
@endsection

