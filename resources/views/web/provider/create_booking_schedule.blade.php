@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
    @include('web.custome-menu')
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                <div class="card bg-light shadow-none">
                    <div class="card-body">
                        <div class="card mb-3">
                            <div class="card-body">

                                <form action="{{url($url)}}" method="{{$method}}">
                                    @csrf
                                    <div class="row mb-3">
                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                                            <label class="control-label"> Date <span class="req">*</span></label>
                                            <div class="form-group">
                                                <input type="date" name="date"
                                                       placeholder="mm/dd/yyyy"
                                                       class="form-control border-round datepicker rounded-0 @error('date') is-invalid @enderror"
                                                       required value="@if(isset($event->date)){{$event->date}}@endif">
                                            </div>
                                            @error('date')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                                            <label class="control-label">Total Allowed Bookings <span class="req">*</span></label>
                                            <div class="form-group">
                                                <input name="total_bookings"  oninput='this.value=(parseInt(this.value)||" ")' placeholder="Enter total allowed bookings" class="form-control border-round  rounded-0 @error('total_bookings') is-invalid @enderror" required value=" @if(isset($event)){{$event->total_bookings}}@endif">
                                            </div>
                                            @error('total_bookings')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-3">
                                            <label class="control-label">Is booking allowed ?</label><br>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="btn-off-yes" class="custom-control-input btn-off-radio @error('is_off') is-invalid @enderror" required  name="is_off" value="yes">
                                                <label class="custom-control-label" for="btn-off-yes">Yes</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="btn-off-no" class="custom-control-input btn-off-radio @error('is_off') is-invalid @enderror"  name="is_off" value="no" checked>
                                                <label class="custom-control-label" for="btn-off-no">No</label>
                                            </div>
                                            @error('is_off')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                    </div>
                                    @if(isset($event))
                                        <input type="hidden" name="id" value="{{$event->id}}">
                                        <input type="hidden" name="event_id" value="{{$event->event_id}}">

                                    @endif
                                    <button type="submit" class="btn btn-info mt-3 mb-2">Submit</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

