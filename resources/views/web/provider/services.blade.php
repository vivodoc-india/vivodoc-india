@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
@include('web.custome-menu')
		<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 pt-4 pb-4">
<div id="accordion">
    <?php $i=1;
        $n=1;
        ?>
@if(isset($services))
@foreach($services as $service)
@if(!empty($service['serv']))
  <div class="card">
    <div class="card-header bg-white">
        <div class="row">
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 d-flex">
    <img src="{{asset("web/v2/img/icons/".$service['serv']->user_role->image)}}"
         alt="{{$service['serv']->user_role->name}}"
         class="me-3 img-fluid" style="width:50px; height: 50px;">
  <div class="mt-2">
  <h4>{{$service['serv']->user_role->name}}</h4>
</div>

    </div>
    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
    <a class="btn btn-light btn-sm text-warning" data-toggle="collapse" href="#collapseOne{{$i.$n}}">
<i class="fas fa-eye"></i> Show
      </a>
    <a class=" btn btn-light btn-sm btn-add-category text-success" data-toggle="collapse" href="#collapseTwo{{$i.$n}}" data="{{$service['serv']['id']}}">
      <i class="far fa-plus-square"></i> Category
      </a>
      <a class=" btn btn-light btn-sm btn-edit-profile text-primary" data-toggle="collapse" href="#collapseThree{{$i.$n}}">
<i class="fas fa-pen-fancy"></i> Edit
      </a>
       <a class=" btn btn-light btn-sm btn-remove-profile text-danger" data-toggle="collapse" href="#collapseFour{{$i.$n}}">
      <i class="far fa-trash-alt"></i> Remove
      </a>
    </div>
            </div>

    </div>
    @if($i==1 && $n==1)
    <!--Add Collapse Body -->
<div id="collapseOne{{$i.$n}}" class="collapse show" data-parent="#accordion">
    @else
    <div id="collapseOne{{$i.$n}}" class="collapse" data-parent="#accordion">
        @endif
      <div class="card-body">
          <!-- First Row -->
        <div class="row">
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
<h6 class="text-secondary">Categories Selected</h6>
</div>
<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
@if(count($service['cat']) > 0)
@for($j=0;$j<count($service['cat']);$j++)
<span class="badge badge-primary">{{ucwords($service['cat'][$j]->title)}}</span>
@endfor
@else
<span class="badge badge-secondary">No category selected yet.</span>
@endif
    </div>
            </div>
            <!-- First Row Ends Here -->
            <hr class="divider mt-3 mb-3">
            <!-- Second Row -->
            <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
<h6 class="text-secondary">Details Provided For This Service</h6>
</div>
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
      <div class="row">
 <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
<label class="control-label badge badge-info">Landmark : {{$service['serv']['landmark']}}</label>
          </div>
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
<label class="control-label badge badge-info">Area : {{$service['serv']['area']}}</label>
          </div>
 <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
<label class="control-label badge badge-info">City : {{$service['serv']['city']}}</label>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
<label class="control-label badge badge-info">State : {{$service['serv']['state']}}</label>
          </div>
 <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
   <label class="control-label badge badge-info">Available : {{$service['serv']['availability']}}</label>
          </div>
   <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
<label class="control-label badge badge-info">Fee Charges : Rs.{{$service['serv']['min_fee']." - Rs.".$service['serv']['max_fee']}}</label>
          </div>
        </div>
        <!-- Row Ends Here -->
        </div>
        <!-- Column Ends Here -->
                </div>
                <!-- Second row Ends Here -->

 <hr class="divider mt-3 mb-3">
 <!-- Third Row -->
<div class="row">
     <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
<h6 class="text-secondary">Description</h6>
</div>
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
            <p class="justify">{{$service['serv']['description']}}</p>
</div>
                </div>
<!-- Third row Ends here -->
 <hr class="divider mt-3 mb-3">
 <!-- Fourth Row -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
<h6 class="text-secondary">Images Uploaded Related To This Service</h6>
</div>
<!-- Column Ends Here -->
 <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
     <!-- row starts here -->
     <div class="row">
 @for($z=1; $z<=5; $z++)

     <div class="col-lg-4 col-md-4 col-xl-4 col-sm-12 ">
         <div class="card mb-5">
             <div class="card-body">
                 <form action="{{url('services-images')}}" method="post"  enctype="multipart/form-data">
                     <input type="hidden" name="sp_id" value="{{$service['serv']['id']}}">
                     <input type="hidden" name="sp_ur_id" value="{{$service['serv']['user_role_id']}}">
                     <input type="hidden" name="image_count" value="{{$z}}">
                     @csrf

                 @if(!empty($service['serv']['image'.$z]))
                     <a href="{{asset($service['serv']['image'.$z])}}" target="__blank">
                         <img src="{{asset($service['serv']['image'.$z])}}" class="img-fluid img-thumbnail rounded"></a>
                @else
                     <img src="{{asset('web/img/no_image.jpg')}}" class="img-fluid img-thumbnail rounded">

                 @endif
                     <div class="custom-file mt-5">
                         <input type="file" class="custom-file-input @error('image') is-invalid @enderror" id="re-doc{{$z}}" name="image" required>
                         <label class="custom-file-label" for="re-doc{{$z}}">Choose file</label>
                     </div>
                     <span class="misc">Allowed file type: jpg, jpeg, png.</span>
{{--                     @error('image')--}}
{{--                     <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                     @enderror--}}
                     <button type="submit" class="btn btn-info px-3"><i class="fas fa-cloud-upload-alt" aria-hidden="true"></i> Upload</button>
                 </form>
             </div>
         </div>
     </div>
     @endfor
 </div>
     <!-- Row ends here -->
  @if(!empty($service['serv']['images']))
<div class="row">
@php $profile= explode(",", $service['serv']['images']) @endphp
@for($z=0;$z<count($profile);$z++)
{{--<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">--}}
{{-- <a href="{{asset('uploads/service-images/category_images/'.$profile[$z])}}" target="__blank">--}}
{{--<img src="{{asset('uploads/service-images/category_images/'.$profile[$z])}}" class="img-fluid img-thumbnail rounded"></a>--}}
{{--</div>--}}
@endfor
</div>
<!-- Row Ends Here -->
  @endif
</div>
<!-- Column Ends Here -->
    </div>
<!-- Row Ends Here-->
 <!-- Fourth Row Ends Here -->
<hr class="divider mt-3 mb-3">
 <!-- Fifth Row Start Here -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
<h6 class="text-secondary">Documents Uploaded Related To This Service</h6>
</div>
<!-- Column Ends Here -->
 <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
     @if(count($service['documents']) > 0)

<div class="row">
    @foreach($service['documents'] as $document)
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
 <a href="{{asset('uploads/service/documents/'.$document->folder.'/'.$document->file)}}" target="__blank">
<img src="{{asset('uploads/service/documents/'.$document->folder.'/'.$document->file)}}" class="img-fluid img-thumbnail rounded"></a>
</div>
    @endforeach
</div>

<!-- Row Ends Here -->
  @endif
</div>
<!-- Column Ends Here -->
    </div>
<!-- Row Ends Here-->
 <!-- Fifth orw Ends here  -->
      </div>
      <!-- End Of Card Body -->
    </div>
    <!-- Show Collapse Body Ends Here -->
    <!-- Add Collapse Body -->
    <div id="collapseTwo{{$i.$n}}" class="collapse" data-parent="#accordion">
      <div class="card-body">
      <form action="{{url('services')}}" method="post" enctype="multipart/form-data">
@method('POST')
              @csrf
      <input type="hidden" name="id" value="{{$service['serv']['id']}}">
        <div class="row mb-3">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
<label class="control-label">Categories</label>
<div class="form-group">
<select class="form-control rounded-0 select-category @error('category_id') is-invalid @enderror" style="width:83%" multiple="multiple" name="category_id[]" required>
@if(count($service['cat_to_add_new']) > 0)
@for($m=0;$m<count($service['cat_to_add_new']);$m++)
<option value="{{$service['cat_to_add_new'][$m]->id}}">{{ucwords($service['cat_to_add_new'][$m]->title)}}</option>
@endfor
@endif
</select>
</div>
 @error('category_id')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    </div>
    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12">
<label class="control-label">Upload Relevant Documents For Selected Categories </label>
 <div class="custom-file">
 <input type="file" class="custom-file-input @error('related_document') is-invalid @enderror" id="re-doc" name="related_document[]" multiple>
    <label class="custom-file-label" for="re-doc">Choose file</label>
  </div>
  <span class="misc">Allowed file type: jpg, jpeg, png. Max file size: 500KB. Use  <span class="badge badge-secondary"> 'CTRL' </span> button for multiple file selection.</span>
   @error('related_document')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
        </div>
    </div>
    <button type="submit" class="btn btn-primary px-3"><i class="far fa-plus-square color-white"></i> Submit</button>
    </form>
      </div>
    </div>
    <!-- Add Collapse Body Ends Here-->
    <!-- Edit Collapse Body -->
    <div id="collapseThree{{$i.$n}}" class="collapse" data-parent="#accordion">
      <div class="card-body">
      <form action="{{url('services/'.$service['serv']['id'])}}" method="post">
           @method('PUT')
@csrf
<div class="row mb-3">
    @if($service['serv']['user_role_id']==6)
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
            <label class="control-label">Vehicle Number</label>
            <div class="form-group">
                <input type="text" class="form-control border-round" name="vehicle_no" placeholder="enter vehicle number"
                       value="{{$service['serv']['vehicle_no']}}" required>
            </div>
        </div>

        @endif
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
<label class="control-label">Registration Number</label>
<div class="form-group">
<input type="text" class="form-control border-round" name="reg_no" placeholder="enter your registration number"
       value="{{$service['serv']['reg_no']}}" required>
    </div>
    </div>

{{--    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">--}}
{{--        <label class="control-label">Availability Schedule</label>--}}
{{--        <div class="form-group">--}}
{{--            <input type="text" class="form-control border-round" name="availability" placeholder="Mon-Sun 10:00AM - 07:00PM" value="{{$service['serv']['availability']}}" required>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
<label class="control-label">{{ $service['serv']['user_role_id']==1 ? 'Online Consultation Fee':'Minimum Fee' }} </label>
<div class="form-group">
<input type="number" class="form-control border-round" name="min_fee"
       value="{{$service['serv']['min_fee']}}" placeholder="minimum fee rs. 20" required min="20">
</div>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
<label class="control-label">{{ $service['serv']['user_role_id']==1 ?'Offline Consultation Fee':'Maximum Fee'}}</label>
<div class="form-group">
    <input type="number" class="form-control border-round" name="max_fee"
           placeholder="0" value="{{$service['serv']['max_fee']}}" min="0"required>
</div>
    </div>
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
<label class="control-label">Landmark</label>
<div class="form-group">
    <input type="text" class="form-control border-round" name="landmark"
           placeholder="landmark" value="{{$service['serv']['landmark']}}" >
    </div>
    </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
            <label class="control-label">Google Latitude</label>
            <div class="form-group">
                <input type="text" class="form-control border-round" name="g_lat"
                       placeholder="google latitude" value="{{$service['serv']['g_lat']}}" >
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
            <label class="control-label">Google Longitude</label>
            <div class="form-group">
                <input type="text" class="form-control border-round" name="g_long"
                       placeholder="google longitude" value="{{$service['serv']['g_long']}}" >
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
            <label class="control-label">Google Address</label>
            <div class="form-group">
                <input type="text" class="form-control border-round" name="google_address"
                       placeholder="google address" value="{{$service['serv']['google_address']}}" >
            </div>
        </div>
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
        <label class="control-label">State</label>
        <div class="form-group">
            <select class="form-control border-round state-select " name="state" style="width:100%;" >
                <option selected disabled>Choose a state</option>
                @foreach($states as $s)
                    <option value="{{$s->name}}" >{{ucwords($s->name)}}</option>
                @endforeach
            </select>
        </div>
        @if(!empty($service['serv']['state']))
                <label>Selected : {{$service['serv']['state']}}</label>
        @endif
    </div>

    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
        <label class="control-label">City</label>
        <div class="form-group">
            <select class="form-control city-select " name="city" style="width:100%;" >
            </select>
        </div>
        @if(!empty($service['serv']['city']))
            <label class="">Selected : {{$service['serv']['city']}}</label>
        @endif
    </div>

    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
<label class="control-label">Area</label>
<div class="form-group">
    <select class="select2_demo_2 form-control area-select"   name="area" style="width:100%;" >
    </select>
    </div>
        @if(!empty($service['serv']['area']))
            <label>Selected : {{$service['serv']['area']}}</label>
        @endif
    </div>

   <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
<label class="control-label">Address</label>
<div class="form-group">
<textarea class="form-control" name="address" required>{{$service['serv']['address']}}</textarea>
    </div>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
<label class="control-label">Description</label>
<div class="form-group">
<textarea class="form-control" name="description" required>{{$service['serv']['description']!=''?$service['serv']['description']:'We are the best healthcare services provider
Get our services online on omeddr.com and OMEDDr App.
You can Call, Enquiry, Follow-up or Book the Appointment 24X7.
In COVID-19 Pandemic situation get all the possible services online to avoid getting infected your self as well as our self.
We are committed to provide the best quality healthcare services.
You can also save your time and money with discounts and promos on OMEDDr.'}}</textarea>
    </div>
    </div>
    </div>
<input type="hidden" name="id" value="{{$service['serv']['id']}}">
<button type="submit" class="btn btn-primary"><i class="fas fa-pen-fancy color-white"></i> Submit</button>
        </form>
      </div>
    </div>
    <!-- Edit Collapse Body ends Here -->
   <!-- Remove Collapse Body -->
    <div id="collapseFour{{$i.$n}}" class="collapse" data-parent="#accordion">
      <div class="card-body">
          <h6>Remove From This Service Provider Category</h6>
        <div class="row">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
<form action="{{url('services/'.$service['serv']['id'])}}" method="post">
    @method('DELETE')
    @csrf
    <input type="hidden" name="id" value="{{$service['serv']['id']}}">
<div class="custom-control custom-checkbox" align="center">
    <input type="checkbox" class="custom-control-input @error('chk_agree') is-invalid @enderror" id="customCheck{{$i.$n}}" name="chk_agree" required>
    <label class="custom-control-label" for="customCheck{{$i.$n}}">I agree.</label>
  </div>
   @error('chk_agree')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
   <div style="text-align: center;"> <button type="submit" class="btn btn-danger px-3 center"><i class="fas fa-trash-alt color-white"></i> Remove</button></div>
    </form>

</div>

            </div>
      </div>
    </div>

    <!-- Remove Collapse Body Ends Here -->
  </div>

<?php $i++;
        $n++; ?>
        @endif
@endforeach
@endif



</div>

		</div>
	</div>
</div>
    </section>
@endsection
    @section('additional_script')
        <script>
            $('.state-select').change(function() {
                $.ajax({
                    type:"GET",
                    url:"{{url('ajax/state')}}?state="+$(this).val(),
                    success: function(response) {
                        if (response.success) {
                            $('.city-select').find('option').remove().end().append('<option selected hidden disabled>City</option>');
                            var city = response.city;
                            $.each(city, function(i, d) {
                                $('.city-select').append('<option>' + d.name + '</option>');
                            });
                        }
                    },
                });
            });

            // City Change
            $('.city-select').change(function() {
                $.ajax({
                    type:"GET",
                    url:"{{url('ajax/city')}}?city="+$(this).val()+'&state='+$('.state-select').val(),
                    success: function(response) {
                        if (response.success) {
                            $('.area-select').find('option').remove().end().append('<option selected hidden disabled>Area</option>');
                            var area = response.area;
                            $.each(area, function(i, d) {
                                $('.area-select').append('<option>' + d.area + '</option>');
                            });
                        }
                    },
                });
            });
        </script>
@endsection
