@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
    @include('web.custome-menu')
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                <div class="card bg-light shadow-none">
                    <div class="card-body">
                        <div class="card mb-3">
                            <div class="card-body">

                                        <form action="{{url($url)}}" method="{{$method}}" id="event-frm">
                                            @csrf
                                            <div class="row mb-3">
                                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                                    <label class="control-label">Start Date <span class="req">*</span></label>
                                                    <div class="form-group">
         <input type="date" name="start_date"
                class="form-control border-round rounded-0 @error('start_date') is-invalid @enderror"
                required value="@if(isset($event->start_date)){{$event->start_date}}@endif"
         @if(isset($event->end_date)){{'disabled'}}@endif>
                                                    </div>
                                                    @error('start_date')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                                    <label class="control-label">End Date <span class="req">*</span></label>
                                                    <div class="form-group">
                   <input name="end_date"  type="date"
                          class="form-control border-round rounded-0 @error('end_date') is-invalid @enderror"
                          required value="@if(isset($event->end_date)){{$event->end_date}}@endif"
                   @if(isset($event->end_date)){{'disabled'}}@endif>
                                                    </div>
                                                    @error('end_date')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-3">
                                                    <div class="form-check form-check-inline">
                                                        <input type="radio" id="btn-off-yes"
                                                               class="form-check-input btn-off-radio @error('is_off') is-invalid @enderror"
                                                               required  name="is_off"
                                                               value="1" @if(isset($event->is_off) && $event->is_off == 1){{'checked'}}@endif>
                                                        <label class="form-check-label" for="btn-off-yes">Schedule For No Booking</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input type="radio" id="btn-off-no"
                                                               class="form-check-input btn-off-radio @error('is_off') is-invalid @enderror"
                                                               name="is_off" value="0"  @if(isset($event->is_off)) @if($event->is_off == 0){{'checked'}}@endif
                                                            @else{{'checked'}}@endif >
                                                        <label class="form-check-label" for="btn-off-no">Schedule For Appointment</label>
                                                    </div>
                                                    @error('is_off')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row mb-2">
{{--                                              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">--}}
{{--                                                    <label class="control-label">Color <span class="req">*</span></label>--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <input type="color" name="color" class="form-control rounded-0 border-round @error('color') is-invalid @enderror"  required value="@if(isset($event->color)){{$event->color}}@else{{'#33B5E5'}}@endif">--}}
{{--                                                    </div>--}}
{{--                                                    @error('color')--}}
{{--                                                    <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                                                    @enderror--}}
{{--                                                </div>--}}
{{--                                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">--}}
{{--                                                    <label class="control-label">Text Color <span class="req">*</span></label>--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <input type="color" name="text_color" class="form-control rounded-0 border-round @error('text_color') is-invalid @enderror" required value="@if(isset($event->text_color)){{$event->text_color}}@else{{'#ffffff'}}@endif">--}}
{{--                                                    </div>--}}
{{--                                                    @error('text_color')--}}
{{--                                                    <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                                                    @enderror--}}
{{--                                                </div>--}}
                                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 div-t-book">
                                                    <label class="control-label">Total Bookings Per Day</label>
                                                    <div class="form-group">
       <input name="total_bookings" id="total_booking"
              type="text" class="form-control border-round rounded-0 @error('total_bookings') is-invalid @enderror"
              value=" @if(isset($event->total_bookings)){{$event->total_bookings}}@endif" oninput='this.value=(parseInt(this.value)||" ")'>
                                                    </div>
                                                    @error('total_bookings')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 div-timming ">
                                                    <div class="row">
                                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                                    <label class="control-label">Available From </label>
                                                    <div class="form-group">
     <input name="" id="f-time"  type="time" class="form-control border-round  rounded-0"/>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                                    <label class="control-label">Available To </label>
                                                    <div class="form-group">
         <input name="" id="t-time"  type="time" class="form-control border-round  rounded-0"/>
                                                    </div>
                                                </div>
                                                        <div class="col-lg-4 col-md-4 col-xl-4 col-sm-12 offset-4 offset4 mt-2">
<button type="button" class="btn btn-success btn-sm btn-add-timming">Add </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-4 col-md-4 col-lg-4 col-sm-12 div-s-time">
                                                    <label class="control-label">Your Available Timings </label>
                                                    <div class="form-group">
 <select class="form-control rounded-0 @error('timings') is-invalid @enderror" style="width:100%"
    multiple="multiple" name="timings[]" required id="timings">

@if(isset($event->timings) && !empty($event->timings))
@php  $timing = explode(',',$event->timings) @endphp
@for($i=0;$i< count($timing);$i++)
    <option selected>{{$timing[$i]}}</option>
    @endfor
    @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            @if(isset($event))
        <input type="hidden" name="id" value="{{$event->id}}">
                                    @endif
                                            <button type="submit" class="btn btn-info mt-3 mb-2" id="btn-event-submit">Submit</button>
                                        </form>

                                @if(isset($event) && $event->is_off==0)
                                    <a href="{{url('schedule-by-day/'.$event->id)}}" class="btn btn-warning">View Daywise Schedule</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
@endsection
@section('additional_script')
    <script>
        $(document).ready(function(){
            // $('.datepicker').attr("placeholder", "mm/dd/yyyy").datepicker({
            //     overrideBrowserDefault: true,
            //     minDate:0
            // });


           // Manage week off or leave
            $('#btn-off-yes').click(function(){
                $('.div-t-book ,.div-timming,.div-s-time').hide();
                // $('#total_booking, #f-time , #t-time')
            });

            $('#btn-off-no').click(function(){
                $('.div-t-book ,.div-timming,.div-s-time').show();
            });

$('.btn-add-timming').click(function(){
    var from=onTimeChange($('#f-time').val());
    var to = onTimeChange($('#t-time').val());

    $("#timings").append("<option selected>" + from+" - "+ to + "</option>");
    $('#f-time').val('');
    $('#t-time').val('');
});

$('#btn-event-submit').click(function(e){
    e.preventDefault();
    if($(".btn-off-radio:checked").val() == 0)
    {
        if($('#total_booking').val()==" ")
        {

            alertFailed('Please enter total booking allowed per day.');
        }
        else if($('#timings').val() == " ")
        {
            alertFailed('Please enter your availability time.');
        }
        else{
            $('#event-frm').submit();
    }

    }
     else if($(".btn-off-radio:checked").val() == 1) {
        $('#event-frm').submit();
    }
     else{
         alertFailed('Please make sure either it is week-off/leave or not.');
    }

});
        });
        function onTimeChange(time) {
            var timeSplit = time.split(':'),
                hours,
                minutes,
                meridian;
            hours = timeSplit[0];
            minutes = timeSplit[1];
            if (hours > 12) {
                meridian = 'PM';
                hours -= 12;
            } else if (hours < 12) {
                meridian = 'AM';
                if (hours === 0) {
                    hours = 12;
                }
            } else {
                meridian = 'PM';
            }
            return hours + ':' + minutes + ' ' + meridian;
        }
    </script>
@endsection

