@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
    @include('web.custome-menu')
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <table id="example" class="display responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>S.no.</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Is Allowed</th>
                                <th>Allowed Bookings</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $n=1 @endphp
                            @if(isset($details))
                                @foreach($details as $en)
                                    <tr>
                                        <td>{{$n}}</td>
                                        <td>{{$en->start_date}}</td>
                                        <td>{{$en->end_date}}</td>
                                        <td>{{$en->is_off==1?"No":"Yes"}}</td>
                                        <td>{{$en->total_bookings}}</td>
                                        <td>
 <a class="btn btn-info btn-sm" href="{{url('event/update-event/'.$en->id)}}"> <i class="fas fa-edit"></i> Edit</a>
                                            <button type="button" class="btn btn-danger btn-sm delete_button_action"
                                                    href="javascript:void(0)" data-url="{{url('event/remove')}}"
                                                    data-id="{{$en->id}}"> <i class="fas fa-trash-alt text-white"></i> Delete</button>
                                        </td>
                                    </tr>
                                    @php $n++ @endphp
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
{{--@section('additional_script')--}}

{{--@endsection--}}
