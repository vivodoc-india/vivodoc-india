@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Ambulance Bookings</div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cp-md">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">
                @include("web.left-menu")
            </div>
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <table id="example" class="display responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>S.no.</th>
                                <th>Service Provider</th>
                                <th>Patient Name</th>
                                <th>Patient Age</th>
                                <th>Patient Gender</th>
                                <th>Booked Date</th>
                                <th>Action</th>
                                <th>Status</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php $n=1 @endphp
                            @if(isset($details))
                                @foreach($details as $en)
                                    <tr>
                                        <td>{{$n}}</td>
                                        <td>{{$en->provider->name}}</td>
                                        <td>{{ucwords($en->patient_name)}}</td>
                                        <td>{{$en->age}}</td>
                                        <td>{{$en->gender}}</td>
                                        <td>{{date('d-m-Y',strtotime($en->booking_date))}}</td>
                                        <td>
                                            <a class="btn btn-success btn-sm" href="{{route('ambulance-bookings.edit',$en->id)}}">
                                                <i class="fas fa-edit"></i> </a>
                                            @if($en->is_paid==0)
                                                {{--                                                <a class="btn btn-info btn-sm " href="{{url('patient/pay-ambulance-fee/'.$en->id)}}">--}}
                                                {{--                                                    <i class="fas fa-rupee-sign"></i> Pay Now--}}
                                                {{--                                                </a>--}}
                                            @endif
                                        </td>
                                        <td>{{$en->is_completed==1?'Completed':'Pending'}}</td>

                                    </tr>
                                    @php $n++ @endphp
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
{{--@section('additional_script')--}}

{{--@endsection--}}
