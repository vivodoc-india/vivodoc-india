@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
@include('web.custome-menu')

		<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 pt-4">
			<div class="card">

				<div class="card-body">
                    @if(isset($profile))

					<div class="row mb-5">
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 order-xs-1">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            @if($profile->user->approval == 1)
                                                <span class="badge badge-pill badge-success">Profile Approved</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">Not Approved</span>
                                            @endif
                                        </div>
                                        <div class="col-lg-12 col-sm-12 col-md-12">
                                            <div class="row mt-4">
                                                <div class="col-md-4">
                                                    <h6>Live Doctor</h6>
                                                </div>
                                                <div class="col-md-4 ">
                                                    <form action="{{url('change-live-status')}}" method="post" id="frm-live-stat">
                                                        @csrf
                                                        <label class="switch">
                                                            <input type="checkbox" onchange="$('#frm-live-stat').submit();"
                                                                   name="liveval" @if(Auth::user()->user_role_id==11 && Auth::user()->service_profile()->count() > 0 &&  Auth::user()->service_profile()->first()->mark_live ==1)
                                                                   {{'checked'}}@endif value="@if(Auth::user()->user_role_id==11 && Auth::user()->service_profile()->count() > 0 &&  Auth::user()->service_profile()->first()->mark_live ==1)
                                                                0 @else 1 @endif">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </form>
                                                </div>
                                                <div class="col-md-4 col-4">
                                                    <div class="@if(Auth::user()->user_role_id==11 && Auth::user()->service_profile()->count() > 0 && Auth::user()->service_profile()->first()->mark_live == 1)
                                                    {{'text-success'}} @else {{'text-danger'}} @endif" style="font-weight:bold;" id="live-status">

                                      @if(Auth::user()->user_role_id==11 && Auth::user()->service_profile()->count() > 0 && Auth::user()->service_profile()->first()->mark_live == 1)
                                                            {{'Online'}} @else {{'Offline'}} @endif</div>
                                                </div>
                                            </div>
                                        </div>
                                        @if(isset($has_ambulance) && $has_ambulance!='')
                                        <div class="col-lg-12 col-sm-12 col-md-12">

                                                <div class="row">
                                                    <div class="col-md-4" style="float:left;">
                                                        <h6> Live Ambulance</h6>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <form action="{{url('ambulance-live-status')}}" method="post" id="frm-amb-live-stat">
                                                            @csrf
                                                            <input type="hidden" name="longitude" id="hid-longitude" value="">
                                                            <input type="hidden" name="latitude" id="hid-latitude" value="">
                                                            <label class="switch">
                                                                <input type="checkbox" onchange="getLocation();" name="mark_live" @if($has_ambulance->mark_live == 1)
                                                                {{'checked'}}@endif value="{{$has_ambulance->mark_live ==1 ? 0:1}}">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </form>

                                                    </div>
                                                    <div class="col-md-4 ">
                                                        <div class="@if($has_ambulance->mark_live ==1)
                                                        {{'text-success'}} @else {{'text-danger'}} @endif" style="font-weight:bold;" id="live-status">

                                                            @if($has_ambulance->mark_live ==1)
                                                                {{'Online'}} @else {{'Offline'}} @endif</div>
                                                    </div>
                                                </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 order-xs-12">
                        <div class="h3-responsive">{{$profile->user->name}}</div>
 <div class="mb-2">{{$profile->qualification}}</div>
  <div class="mb-2">{{$profile->speciality}}</div>
  <div class="mb-2">{{$profile->other_speciality}}</div>
    <div class="mb-2"><i class="fas fa-map-marker-alt"></i> {{ucwords($profile->city.",".$profile->state)}}</div>
    <div class="row">
<div class="col-md-4">
    <i class="fas fa-briefcase-medical"></i> {{ucwords($profile->experiance)}}
</div>
<div class="col-md-4">
    @if($profile->gender == "Male")
<i class="fas fa-male"></i> {{$profile->gender}}
    @elseif($profile->gender == "Female")
<i class="fas fa-female"></i> {{$profile->gender}}
    @endif
</div>
@if(isset($feerange))
    <div class="col-md-4"><i class="fas fa-rupee-sign"></i> {{$feerange->min_fee." - ".$feerange->max_fee}}</div>
@endif
    </div>

                        </div>


                            </div>


</div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">

                            @if($profile->photo !='')
                                <a href=""><img src="{{asset('uploads/profile/'.$profile->photo)}}" class="img-fluid img-thumbnail" width="150"></a>
                            @else
                                <a href=""><img src="{{asset('web/img/nobody.jpg')}}" class="img-fluid img-thumbnail"></a>
                            @endif

                        </div>
							{{-- <div class="mb-2">ODEM564789</div> --}}

							{{-- <div class="mb-2">28 Years</div>
							<div class="mb-2">28 Oct, 1991</div> --}}

						</div>

					<div class="row mb-5">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
							<div class="h3-responsive mb-3">Contact Details</div>
                        <div class="mb-2">{{$profile->user->email}}</div>
                        <div class="mb-2">{{$profile->user->mobile}}</div>
                        <div class="mb-2">{{$profile->address}}</div>
                        <div class="mb-2">{{$profile->state}}</div>
                        <div class="mb-2">{{$profile->pincode}}</div>
						</div>
                    </div>

                        @else
                        <h3 class="text-center">Kindly Update Your Profile</h3>
                    <div align="center">
                        <a class="btn btn-info mt-5" href="{{url('update-profile')}}">Update Profile</a>
                    </div>

                @endif
                </div>

			</div>
		</div>
	</div>
    </section>
@endsection
@section('additional_script')
    <script>

        function showLocation(position) {

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            $('#hid-latitude').val(latitude);
            $('#hid-longitude').val(longitude);
            $('#frm-amb-live-stat').submit();
        }
        function errorHandler(err) {
            if(err.code == 1) {
                alert("Error: Access is denied!");
            } else if( err.code == 2) {
                alert("Error: Position is unavailable!");
            }
        }
        function getLocation(){
            if(navigator.geolocation){
                // timeout at 60000 milliseconds (60 seconds)
                var options = {timeout:60000};
                navigator.geolocation.getCurrentPosition
                (showLocation, errorHandler, options);
            } else{
                alert("Sorry, browser does not support geolocation!");
            }
        }
    </script>
@endsection
