@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
    @include('web.custome-menu')
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
        <div class="card">
            <div class="card-body">
                <table id="example" class="display responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>S.no.</th>
                        <th>Patient Name</th>
                        <th>Patient Age</th>
                        <th>Patient Gender</th>
                        <th>Requested On</th>
{{--                        <th>Booked By</th>--}}
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $n=1 @endphp
                    @if(isset($details))
                        @foreach($details as $en)
                            <tr>
                                <td>{{$n}}</td>
                                <td>{{ucwords($en->patient_name)}}</td>
                                <td>{{$en->age}}</td>
                                <td>{{$en->gender}}</td>
                                <td>{{date('d-m-Y',strtotime($en->booking_date))}}</td>
{{--                                <td>{{$en->patient->name}}</td>--}}
                                <td>
                                    @if($en->prescription!='' || $en->prescription_folder!='')
                                       <span class="badge badge-pill badge-success font-bold"> {{'Completed'}}</span>
                                        @elseif($en->is_request_rejected == 1)
                                    {{'Request Rejected'}}
                                        @elseif($en->is_request_accepted == 1)
                                        {{'Request Accepted'}}
                                        @else
                                        <span class="badge badge-pill badge-info font-bold"> {{'Not Answered'}}</span>
                                        @endif
                                </td>

                                <td>
  <a class="btn btn-success btn-sm"
     href="{{route('live-consultation.show',$en->id)}}"> <i class="fas fa-eye text-white"></i></a>
     <a class="btn btn-info btn-sm no-gutters "
        href="{{route('live-consultation.edit',$en->id)}}"> <i class="fas fa-edit"></i></a>
{{--     <a type="button" class="btn btn-danger btn-sm no-gutters delete_button_action" href="javascript:void(0)" data-url="{{route('live-consultation.destroy',$en->id)}}" data-id="{{$en->id}}" data-toggle="modal" data-target="#deleteModal"> <i class="fas fa-trash-alt text-white"></i> </a>--}}

                                </td>
                            </tr>
                            @php $n++ @endphp
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
    </section>
@endsection
{{--@section('additional_script')--}}

{{--@endsection--}}
