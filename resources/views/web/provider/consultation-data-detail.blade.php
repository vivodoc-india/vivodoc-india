@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
    <div class="container-fluid cp-md">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">
                @include("web.left-menu")
            </div>
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                    <div class="card">
                            <div class="card-body">
                                        @if(isset($booking))
                                            <div class="row">
                                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                                                    <div class="h3-responsive mb-2">{{$booking->provider->name}}</div>
                                                    <div class="mb-2">{{$booking->serviceProfile->reg_no}}</div>
                                                    <div class="mb-2">{{$booking->provider->profile->speciality}}</div>
                                                    <div class="mb-2">{{$booking->provider->profile->qualification}}</div>
                                                    <div class="mb-2">{{$booking->provider->profile->other_speciality}}</div>
                                                    <div class="mb-2">{{$booking->provider->profile->address}}</div>
                                                </div>
                                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 " >
                                                    <img src="{{asset('web/v2/img/logo/logo.svg')}}" class="img-fluid">
                                                </div>

                                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">

                                                    <div class="h3-responsive mb-2 text-right">{{$booking->patient_name}}</div>
                                                    <div class="mb-2 text-right"><label class="control-label">Age : </label> {{$booking->age}}</div>
                                                    <div class="mb-2 text-right"><label class="control-label">Mobile : </label> {{$booking->mobile}}</div>
                                                    <div class="mb-2 text-right"><label class="control-label">Gender : </label> {{$booking->gender}}</div>
                                                    <div class="mb-2 text-right"><label class="control-label">Address : </label> {{$booking->address}}</div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row mt-4">
                                                <div class="col-md-3 col-12">
                                                    <div class="mb-2"><label class="control-label">BP : </label> {{$booking->bp}}</div>
                                                </div>
                                                <div class="col-md-3 col-12">
                                                    <div class="mb-2"><label class="control-label">Pulse : </label> {{$booking->pulse}}</div>

                                                </div>
                                                <div class="col-md-3 col-12">
                                                    <div class="mb-2"><label class="control-label">Temperature : </label> {{$booking->temperature}}</div>

                                                </div>
                                                <div class="col-md-3 col-12">
                                                    <div class="mb-2"><label class="control-label">SPO<sub>2</sub> : </label> {{$booking->spo}}</div>

                                                </div>
                                                <div class="col-md-3 col-12">
                                                    <div class="mb-2"><label class="control-label">Weight : </label> {{$booking->weight}}</div>

                                                </div>
                                                <div class="col-md-3 col-12">
                                                    <div class="mb-2"><label class="control-label">History : </label> {{$booking->history}}</div>

                                                </div>
                                                <div class="col-md-3 col-12">
                                                    <div class="mb-2"><label class="control-label">Complain : </label> {{$booking->complain}}</div>

                                                </div>
                                                <div class="col-md-3 col-12">
                                                    <div class="mb-2"><label class="control-label">P. Diagnosis : </label> {{$booking->diagnostic}}</div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12 col-lg-12 mb-4 col-12">
                                                    <div style="min-height:400px; width:100%; border:1px solid #C7C7C7; padding: 5px;">
                                                        @if(!empty($booking->prescription) || !empty($booking->prescription_image))
                                                            {!! $booking->prescription !!}

                                                            <img src="{{$booking->prescription_image!=''? asset('uploads/patientprofile/prescription/'.$booking->prescription_folder.'/'.$booking->prescription_image):''}}" class="img-fluid">
                                                        @else
                                                            {{'Prescription is not uploaded by the doctor yet .'}}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                <div class="row mt-2">
                                    <div class="col-md-12">
                                        <h3>Reviews & Rating</h3>

                                        @php $n=1 @endphp
                                                        @php while($n <= $booking->rating){ @endphp
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        @php
                                                            $n++ @endphp
                                                        @php    }   @endphp

                                        <p>{{$booking->review}}</p>

                                    </div>
                                </div>
                                        <div class="row mt-2">
                                            <p> <span class="text-danger">Disclaimer :</span> Doctor generated this prescription Online based on your inputs. If you get any problem please visit your Doctor.</p>
                                        </div>
                            </div>
                    </div>
            </div>
        </div>
    </div>
        </div>
    </section>
@endsection
@section('additional_script')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="{{asset('web/rating/bootstrap-rating.js')}}"></script>

@endsection
