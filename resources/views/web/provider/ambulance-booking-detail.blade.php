@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Ambulance Booking Response</div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cp-md">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">
                @include("web.left-menu")
            </div>
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        @if(isset($booking))
                            <div class="row">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                                    <div class="h3-responsive mb-2">{{$booking->provider->name}}</div>
                                    <div class="mb-2">{{$booking->serviceProfile->reg_no}}</div>
                                    <div class="mb-2">{{$booking->provider->profile->speciality}}</div>
                                    <div class="mb-2">{{$booking->provider->profile->qualification}}</div>
                                    <div class="mb-2">{{$booking->provider->profile->other_speciality}}</div>
                                    <div class="mb-2">{{$booking->provider->profile->address}}</div>
                                    <div class="mb-2">{{$booking->serviceProfile->vehicle_no}}</div>

                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">
                                    <img src="{{asset('web/v2/img/logo/logo.svg')}}" class="img-fluid">
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 ">

                                    <div class="h3-responsive mb-2 text-right">{{$booking->patient_name}}</div>
                                    <div class="mb-2 text-right"><label class="control-label">Age : </label> {{$booking->age}}</div>
                                    <div class="mb-2 text-right"><label class="control-label">Mobile : </label> {{$booking->mobile}}</div>
                                    <div class="mb-2 text-right"><label class="control-label">Gender : </label> {{$booking->gender}}</div>
                                    <div class="mb-2 text-right"><label class="control-label">Address : </label> {{$booking->address}}</div>
                                </div>
                            </div>
                            <hr>
                        <h6 style="text-decoration: underline; font-weight: bold">Booking Detail</h6>
                        <hr>
                            <div class="row mt-4">
                                <div class="col-md-4 col-12">
                                    <div class="mb-2"><label class="control-label">Pickup Location  </label><br> {{$booking->address}}</div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-2"><label class="control-label">Drop Location  </label><br> {{$booking->destination}}</div>

                                </div>

                                <div class="col-md-4 col-12">
                                    <div class="mb-2"><label class="control-label">Request Status </label><br>
                                    @if($booking->is_completed)
                                        {{'Completed'}}
                                        @elseif($booking->is_request_accepted)
                                        {{'Request Accepted'}}
                                        @elseif($booking->is_request_rejected)
                                        {{'Request Rejected'}}
                                        @else
                                            {{'Not Answered'}}
                                        @endif
                                    </div>

                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-2"><label class="control-label">Approx Fair : </label><br> {{$booking->approx_fair!=''?'Rs '.$booking->approx_fair:''}}</div>

                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-2"><label class="control-label">Advance Payment : </label><br> {{$booking->advance_payment!=''?'Rs '.$booking->advance_payment:''}}</div>

                                </div>

                            </div>
                            <hr>
                            <h6 style="text-decoration: underline; font-weight: bold">Action</h6>
                            <hr>
                            <div class="row mt-4">
                                <div class="col-md-4 col-lg-4 mb-4 col-12">
                                  <button type="button" class="btn btn-success" id="btn-accept">Accept Request</button>

                                    <div id="accept-request" style="display:none;" class="mt-4">
                                        <form action="{{url('submitRequestResponse')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$booking->id}}">
                                            <input type="hidden" name="response_state" value="accepted">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Approx Journey Fair
                                                </label>
                                                <input type="text" class="form-control @error('approx_fair') is-invalid @enderror" name="approx_fair"
                                                       placeholder="Enter approx journey fair" required oninput='this.value=(parseInt(this.value)||" ")' value="{{old('approx_fair')}}">
                                            </div>
                                            @error('approx_fair')
                                            <span class="text-danger test-small">{{$message}}</span>
                                            @enderror
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Advance Payment
                                                </label>
                                                <input type="text" class="form-control @error('advance_payment') is-invalid @enderror" name="advance_payment" placeholder="Enter advance payment"
                                                       required oninput='this.value=(parseInt(this.value)||" ")' value="{{old('advance_payment')}}">
                                            </div>
                                            @error('advance_payment')
                                            <span class="text-danger test-small">{{$message}}</span>
                                            @enderror

                                            <button type="submit" class="btn btn-success">Accept</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 mb-4 col-12">
                                    <button type="button" class="btn btn-danger" id="btn-reject">Reject Request</button>

                                    <div id="reject-request" style="display:none;" class="mt-4">
                                        <form action="{{url('submitRequestResponse')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$booking->id}}">
                                            <input type="hidden" name="response_state" value="rejected">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Mention Reason
                                                </label>
                                                <textarea rows="4" name="remarks" class="form-control" placeholder="Please mention a reason for rejection this request" required>

                                                </textarea>

                                            </div>
                                            <button type="submit" class="btn btn-danger">Reject</button>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 mb-4 col-12">
                                    <button type="button" class="btn btn-info" id="btn-complete">Complete Request</button>

                                    <div id="complete-request" style="display:none;" class="mt-4">
                                        <form action="{{url('submitRequestResponse')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$booking->id}}">
                                            <input type="hidden" name="response_state" value="completed">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Remarks
                                                </label>
                                                <textarea rows="4" name="remarks" class="form-control" placeholder="Enter remarks" required></textarea>

                                            </div>
                                            <button type="submit" class="btn btn-info">Complete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endif

{{--                        <div class="row mt-2">--}}
{{--                            <div class="col-md-12">--}}
{{--                                <h3>Reviews & Rating</h3>--}}

{{--                                @php $n=1 @endphp--}}
{{--                                @php while($n <= $booking->rating){ @endphp--}}
{{--                                <i class="fa fa-star" aria-hidden="true"></i>--}}
{{--                                @php--}}
{{--                                    $n++ @endphp--}}
{{--                                @php    }   @endphp--}}

{{--                                <p>{{$booking->review}}</p>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row mt-2">--}}
{{--                            <p> <span class="text-danger">Disclaimer :</span> Doctor generated this prescription Online based on your inputs. If you get any problem please visit your Doctor.</p>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additional_script')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="{{asset('web/rating/bootstrap-rating.js')}}"></script>
<script>
    $('document').ready(function(){
        $('#btn-accept').click(function(){
            $('#accept-request').toggle();
            $('#complete-request,#reject-request').hide();
        });
        $('#btn-reject').click(function(){
            $('#reject-request').toggle();
            $('#complete-request,#accept-request').hide();
        });
        $('#btn-complete').click(function(){
            $('#complete-request').toggle();
            $('#accept-request,#reject-request').hide();
        });
    });
</script>
@endsection

