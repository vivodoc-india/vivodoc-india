@extends('layouts.layout')
@section('custom_style')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.0/main.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.0/locales-all.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.0/main.css"/>
@endsection
@section('content')
    <section class="main">
        <div class="profile-view-section">
    <div class="container-fluid  cp-md">

        <div class="card mb-5" style="background: #33B5E5; font-weight: bold;color:#fff;">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 ">
                        @if(isset($schedule) && !empty($schedule))
                            <h5>{{$schedule}}</h5>
                        @else
                            @if(isset($today_schedule))
                            <div class="row">
                                <div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">
                                    Today's Booking Allowed :  {{$today_schedule->total_bookings}}
                                </div>

                                <div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">
                                    Today's Total Bookings :  {{$today_booking}}
                                </div>

                                <div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">
                                    Today's Booking Remaining :  {{$today_schedule->total_bookings - $today_booking}}
                                </div>
                            </div>
                                @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
                        <div class="card mb-5" id="bk-schd" style="display:none; background:#00FFFF; font-weight:bold;">
                            <div class="card-body" id="bk-card-body">

                </div>
            </div>


        <div class="row">
            <div class="col-lg-6 col-sm-12 col-md-6 col-xl-6">
                <div class="card">
                    <div class="card-header bg-white">Book Appointment</div>
                    <div class="card-body">
                        @php $user =  Auth::user(); @endphp
                        <form action="{{url('patient/save-appointment')}}" method="post">
@csrf
                            <div class="row mb-3">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <label class="control-label">Booking Date <span class="req">*</span></label><br>
                                    <div class="form-group">
                                        <input name="booking_date" value="{{old('booking_date')}}" type="date" placeholder="dd/mm/yyyy" class="form-control border-round  rounded-0 @error('booking_date') is-invalid @enderror" required id="booking_date">
                                    </div>
                                    @error('booking_date')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <label class="control-label">Patient's Gender <span class="req">*</span></label><br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input  @error('gender') is-invalid @enderror" required id="customRadio"
                                               name="gender" value="Male" {{$user->profile->exists() && $user->profile->gender=="Male"?"Checked":""}}>
                                        <label class="custom-control-label" for="customRadio">Male</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input  @error('gender') is-invalid @enderror"
                                               id="customRadio2" name="gender" value="Female" {{$user->profile->exists() && $user->profile->gender=="Female"?"Checked":""}}>
                                        <label class="custom-control-label" for="customRadio2">Female</label>
                                    </div>
                                    @error('gender')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>


                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Patient Name <span class="req">*</span></label>
                                    <div class="form-group">
                                        <input name="patient_name" type="text"  placeholder="patient name"
                                               class="form-control border-round  rounded-0 @error('patient_name')
                                                   is-invalid @enderror" required value="{{old('patient_name')!='' ? old('patient_name'):$user->name}}">
                                    </div>
                                    @error('patient_name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Patient Age <span class="req">*</span></label><br>
                                    <div class="form-group">
                                        <input name="age" type="text" placeholder="patient age"
                                               class="form-control border-round  rounded-0 @error('age') is-invalid @enderror"
                                               required value="{{old('age')}}">
                                    </div>
                                    @error('age')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Patient's Mobile </label>
                                    <div class="form-group">

                                        <input name="mobile"  type="text" placeholder="Enter mobile no."
                                               class="form-control border-round  rounded-0 @error('mobile')
                                                   is-invalid @enderror" id="mobile" value="{{$user->mobile!=''?$user->mobile:old('mobile')}}" oninput='this.value=(parseInt(this.value)||" ")'>
                                    </div>
                                    @error('mobile')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                                    <label class="control-label">Patient Address <span class="req">*</span></label><br>
                                    <div class="form-group">
                                        <input name="address" type="text" placeholder="address"
                                               class="form-control border-round  rounded-0 @error('address') is-invalid @enderror"
                                               required value="{{$user->profile->exists() && $user->profile->address!=''?$user->profile->address:old('address')}}">
                                    </div>
                                    @error('address')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Diagnostic </label><br>
                                    <div class="form-group">
                                        <input name="diagnostic" type="text" placeholder="Diagnostic"
                                               class="form-control border-round  rounded-0 @error('diagnostic') is-invalid @enderror"
                                        value="{{old('diagnostic')}}">
                                    </div>
                                    @error('diagnostic')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">BP </label><br>
                                    <div class="form-group">
                                        <input name="bp" type="text" placeholder="Enter Patient's Bp"
                                               class="form-control border-round  rounded-0 @error('bp') is-invalid @enderror"
                                        value="{{old('bp')}}">
                                    </div>
                                    @error('bp')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Pulse</label><br>
                                    <div class="form-group">
                                        <input name="pulse" type="text" placeholder="Enter Patient's Pulse"
                                               class="form-control border-round  rounded-0 @error('pulse') is-invalid @enderror"
                                               value="{{old('pulse')}}">
                                    </div>
                                    @error('pulse')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Temperature</label><br>
                                    <div class="form-group">
                                        <input name="temperature" type="text" placeholder="Enter Temperature"
                                               class="form-control border-round  rounded-0 @error('temperature') is-invalid @enderror"
                                               value="{{old('temperature')}}">
                                    </div>
                                    @error('temperature')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">SPO<sub>2</sub></label><br>
                                    <div class="form-group">
                                        <input name="spo" type="text" placeholder="Enter Patient's SPO2"
                                               class="form-control border-round  rounded-0 @error('spo') is-invalid @enderror"
                                               value="{{old('spo')}}">
                                    </div>
                                    @error('spo')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Weight</label><br>
                                    <div class="form-group">
                                        <input name="weight" type="text" placeholder="Enter Patient's Weight"
                                               class="form-control border-round  rounded-0 @error('weight') is-invalid @enderror"
                                               value="{{old('weight')}}">
                                    </div>
                                    @error('weight')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">Complain </label><br>
                                    <div class="form-group">
                                        <input name="complain" type="text" placeholder="complain"
                                               class="form-control border-round  rounded-0 @error('complain') is-invalid @enderror"
                                        value="{{old('complain')}}">
                                    </div>
                                    @error('complain')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                                    <label class="control-label">History </label><br>
                                    <div class="form-group">
                                        <input name="history" type="text" placeholder="history"
                                               class="form-control border-round  rounded-0 @error('history') is-invalid @enderror"
                                               value="{{old('history')}}">
                                    </div>
                                    @error('history')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                            </div>
                            <input type="hidden" name="provider_id" value="@if(isset($provider)){{$provider}}@endif" id="provider_id">
                            <input type="hidden" name="service_profile_id" value="@if(isset($service_profile)){{$service_profile}}@endif" id="sp_id">
                            @if(isset($appointment_fee))
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <h4 class="text-info">Pay Rs.  {{$appointment_fee}} to get an appointment.</h4>
                            </div>
                            @endif
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <button type="submit" class="btn btn-success"> Pay Now</button>
                            </div>
                        </form>

                    </div>

                </div>


            </div>
            <div class="col-lg-6 col-sm-12 col-md-6 col-xl-6">
                @if(isset($calendar))
                    <div class="card  mb-4">
                        <div class="card-header bg-white">Calender</div>
                        <div class="card-body">
                            {!! $calendar->calendar() !!}

                        </div>

                    </div>
                @endif

            </div>
        </div>
    </div>
        </div>
    </section>
    @endsection
@section('additional_script')
    <script>
$(document).ready(function(){
$('#booking_date').change(function(){
    var date=$(this).val();
    var provider = $('#provider_id').val();
    $.ajax({
        type:"GET",
        url:"{{url('ajax/check-booking-schedule')}}?date="+date+"&provider="+provider,
        success:function(response){
            if(response.success){
    var html='';
    html+='<div class="row">' +' <div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">' + ' Booking Allowed :  '+response.allowed_booking+' </div>' +
        ' <div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">' +' Total Bookings Done : '+response.total_booked+' </div>' + '<div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">' +
        'Booking Remaining :  '+ (response.allowed_booking - response.total_booked) + '</div></div>'+'<div class="row mt-2">' +'<div class="col-lg-4 col-sm-12 col-md-4 col-xl-4">' + ' Doctor\'s Schedule :  '+ response.schedule + '</div><div class="col-lg-4 col-sm-12 col-md-4 col-xl-4"> Date : '+date+'</div></div>';
                $('#bk-schd').show();
                $('#bk-card-body').html(html);
            }
            else{
                $('#bk-schd').hide();
                alertify.error(response.message);
            }
        }
    });
});
});
             </script>
{{--    {!! $calendar->script() !!}--}}
@endsection


