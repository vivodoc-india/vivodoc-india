@extends('layouts.layout')
@section('content')
<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">Gallery</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid cp-lg w-default-dot">
	<div class="row no-gutters">
        @if(isset($list))
            @foreach($list as $gal)
                <div class="col-lg-3">
                    <a href="{{asset('web/img/gallery/'.$gal->image)}}" class="image-link">
                        <img src="{{asset('web/img/gallery/'.$gal->image)}}" alt="" class="img-fluid">
                    </a>
                </div>
                @endforeach
        @endif
	</div>
</div>

@endsection
