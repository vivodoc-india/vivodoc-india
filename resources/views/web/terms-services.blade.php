@extends('layouts.layout')
@section('content')
    <section class="main">
        <div class="profile-view-section">
<div class="container">
	<div class="row">
		<div class="col-12">
			<p>It is compulsory to register to avail services of VivoDoc.</p>
			<p>“Registered as patient” are users who have successfully registered themselves with us through the VivoDoc Application or VivoDoc.com website, by providing information that is true and accurate, and who can log on to their account on the Application or website by providing their username and password (“Primary User”); or and whose profile has successfully been created on the VivoDoc under the account of a Primary User, by providing information that is true and accurate (“Secondary User”).</p>
			<p>“Registered as Service Provider” by providing documents/information that is true and accurate, and who can log on to their account on the Application or website by providing their username and password are registered/licensed qualified doctors, paramedical staff, physiotherapists, pharmacists, consultants, medical equipment suppliers, laboratory service providers, other incidental and ancillary healthcare services providers who interact with Registered Users as part of the Services.</p>
			<p>“Non-Registered Users” are users who have not successfully registered themselves on the website. Non-Registered Users may not be provided access to contact with the services providers.</p>
			<p>Registered Users and Non-Registered Users shall individually or collectively be referred to as “Users”.</p>
			<h5>ACCESS AND USAGE RIGHTS OF USERS</h5>
			<p><strong>Non - Registered User:</strong></p>
			<p>A Non - Registered User is not permitted to access or make use of the Application for any purpose other than what is specifically permitted. An Unregistered User is specifically permitted to:</p>
			<ol>
				<li>To browse the website without accessing any Services</li>
				<li>To share the Application via social media applications</li>
			</ol>
			<h5>Registered User: </h5>
			<p>As required by law, A Registered User must be at least 18 years of age and have a sound mind to access and use the Application and Services. In case the Application or Services are to be accessed or used by a minor (i.e. a person less than 18 years of age), then the legal guardian/representative of such person shall register himself/herself as a Primary User and create the profile of the minor as a Secondary User.</p>
			<p>All Services will be requested for, and provided to, the Primary User only. A Secondary User may avail the Services rendered to the Primary User as a beneficiary. The Primary User will be responsible and accountable for the Secondary User’s activity on the Website/Application as if the Services were being accessed and used by the Primary User. However, this shall not discharge the Secondary User from liability towards VivoDoc or its contractors or agents and VivoDoc shall have the right to proceed against Secondary User and Primary User, either jointly or severally, for acts and omissions of Secondary User that violate the Agreement.</p>
			<p>You will use the Website/Application and the features provided by VivoDoc only in relation to and in compliance with all applicable Indian laws.</p>
			<p>You will not use the Website/Application or any feature provided by VivoDoc for any purposes not intended under this Agreement.</p>
			<p>Certain Services are location specific. Depending on Your location, some Services may not be available to You.  </p>
			<p>You will not deliberately use the Website/Application in any way that is unlawful or harms VivoDoc, its directors, employees, affiliates, distributors, partners, services providers and/or any User and/or data or content on the Website/Application.</p>
			<p>As a User, You may have access to business sensitive information. You shall not share such information with any others/competitors or use it for competitive purposes, except with VivoDoc’s prior written consent.</p>
			<p>You understand that as part of Your registration process as well as in course of Your use of the Website/Application, You may receive SMS or email communication or both from VivoDoc on Your registered contact information. These communications will relate to your registration, Services provided by VivoDoc, transactions that You carry out through the Website/Application and any such information found suitable of Your attention by VivoDoc. Please note that VivoDoc will send these communications only to the mobile number that you provided in registration. It is Your responsibility to ensure that You provide the correct number for the transaction You wish to enter into. Further, VivoDoc may also send notifications and reminders to You for the features that You may be using on the Website/Application. You hereby consent to receive such communications from VivoDoc.</p>
			<p>You will not share Your log-in details with anyone. You are responsible for maintaining the confidentiality of Your account access information and password. You shall be responsible for all usage of Your account and password, whether or not authorized by You. You shall immediately notify VivoDoc of any actual or suspected unauthorized use of the Your account or password. Although VivoDoc will not be liable for Your losses caused by any unauthorized use of Your account, You may be liable for the losses of VivoDoc or others due to such unauthorized use.</p>
			<h5>SERVICES</h5>
			<p>The VivoDoc Website/Application may be used to access a variety of healthcare and related services. You may avail of the following services through the Website/Application:</p>
			<ol>
				<li>Online Consultations (through VivoDoc doctors and Healthcare Services Providers)</li>
				<li>Consultation @Home by VivoDoc doctors and Health staff through ‘Bike Doc’</li>
				<li>Doctor’s and Diagnostic services appointment</li>
				<li>Ambulance Services</li>
				<li>Medicine and Medical Products Home delivery</li>
				<li>Lab/sample collections @Home</li>
				<li>Home services like vaccination @Home, ECG and other Tests @Home</li>
				<li>Long term care programs for chronic diseases patients</li>
				<li>Nursing Care, Post-surgery care, Follow up services</li>
				<li>Patient records storage services</li>
				<li>Features such as Edit profile, records of enquiries and replies, storage and display of Your medical records, information and history, prescriptions, lab investigations and radiology reports along with information regarding Your family members</li>
				<li>
					<p>Such other services as may be intimated from time to time.</p>
					<p>The aforesaid services are referred to as “Services”</p>
					<p>The Services are non-transferable and only cover the Registered User.</p>
					<p>You are advised not to use the VivoDoc’s  online consultation services in case of emergency or if You are in a critical condition.</p>
					<p>If You are a Registered User but do not satisfy the above pre-conditions, please do not avail the online consultation services. VivoDoc  will not be responsible or liable for any harm or loss that You may suffer if You elect to access those Services.</p>
					<p>All Users, who are patients, and who opt for online consultation services, may be required to undergo an initial medical examination to ascertain and record medical history, medication history and medical status before video-consulting support can be provided.</p>
					<p>Online consultation services may be provided via Call Center Helplines / Video Calls / Audio Calls / Online Chats / SMS &amp; Text Chats with and without camera and video facilities at the sole discretion of VivoDoc. Reasonable efforts will be made to protect your privacy and confidentiality across Health Consultation Services. </p>
				</li>
			</ol>
		</div>
	</div>
</div>
        </div>
    </section>
@endsection
