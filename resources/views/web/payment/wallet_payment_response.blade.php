
@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Payment Response</div>
                </div>
            </div>
        </div>
    </div>
    <div class="container cp-md">
        <div class="card">
            <div class="card-body text-center">
                @if(isset($signature_mismatch))
                    @if($signature_mismatch)
                        <h3 class="text-danger">Invalid Payment Signature.</h3>
                    @endif
                @endif
                @if(isset($transaction_status))
                    @if($transaction_status == "SUCCESS")
                        <h3 class="text-success">Payment done successfully.</h3>
                    @elseif($transaction_status == "FAILED")
                        <h3>Your transaction has been Failed. Try after sometime.</h3>
                    @elseif($transaction_status == "PENDING")
                        <h3>Your transaction is being pending. Wait for sometime or contact to admin.</h3>
                    @elseif($transaction_status == "CANCELLED")
                        <h3>Your transaction has been cancelled. </h3>
                    @else
                    @endif
                @endif

                <div class="text-center">
                    <a class="btn btn-info btn-lg" href="{{route('patient.wallet')}}"><i class="fas fa-home"></i> Back</a>
                </div>

            </div>
        </div>
    </div>
@endsection


