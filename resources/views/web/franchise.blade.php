@extends('layouts.layout')
@section('content')
    <section class="main">
        <div class="profile-view-section">
<div class="container ">
	<div class="row">
		<div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 mb-4">
			<p>VivoDoc offers Franchise of its Telemedicine Services.</p>
			<p>Vivo Clinics (An advance Telemedicine system with X-ray viewer, ECG, Otoscope, Dermascope, Digital stethoscope etc.)</p>
			<p>And</p>
			<p>Bike Doc (An advance Telemedicine system on a Bike Ambulance (24X7) services with Instant diagnostic, ECG, Emergency drugs and Sample collection facilities)</p>
			<p>We franchise our system to any Entrepreneur Non-medical or Medical like, leading hospitals, experienced doctors, diagnostic centers, Pharmacies, Ambulances and other Health services providers, to improve health outcomes for patients everywhere, as well as profitability for our partners. Our strong partner relationship makes deployment and speed-to-market easy to achieve. Join the growing community of VivoDoc partners across India.</p>
		</div>
		<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
			<div class="card shadow-none border border-light">
				<div class="card-body">
					<h5 class="red-text">Franchise Model</h5>
					<ul class="mb-5">
						<li>Premium Project cost : ₹ 5.00 lakhs + GST</li>
						<li>General Project cost : ₹ 3.5 lakhs + GST</li>
						<li>Project cost with Branding &amp; VivoDoc Box (without interior- Furniture &amp; Fixtures) : ₹ 2.5 lakhs + GST</li>
						<li>Investments: Minimum investment of ₹ 1.5 lakhs (Interest free loan up to ₹ 2.5 lakhs can be availed)</li>
					</ul>
					<h5 class="red-text">Repayment tenure of a maximum of 12 months with a moratorium of 2 months.</h5>
					<ul>
						<li>Own or rented space: 200-250 sqft of space at a location which has no or less medical/health services. Or at your own Residence/Villages, Previous Clinics, Multiple Clinics, Dental setup, Paramedical/Nursing colleges etc. </li>
						<li>Buy Back Guarantee after one year*</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
        </div>
    </section>
@endsection
