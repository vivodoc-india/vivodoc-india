@extends('layouts.layout')
@section('content')
    <div class="header cp-lg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white h2-responsive text-uppercase">Medical History</div>
                </div>
            </div>
        </div>
    </div>
    @include('web.custome-menu')
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <table id="example" class="display responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Document Type</th>
                                <th>Uploaded On</th>
                                <th>View</th>

                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($list) && count($list) > 0)
                                @php $n=1 @endphp

                                @foreach($list as $rec)
                                    <tr>
                                        <td>{{$n}}</td>
                                        <td>{{ucwords($rec->title)}}</td>
                                        <td>{{date('d-m-Y',strtotime($rec->updated_at))}}</td>
                                        <td>
                                            <a type="button" class="btn btn-light btn-sm"
                                               href="{{url('view-medical-data/'.$rec->id)}}">
                                                <i class="fas fa-eye"></i> </a>
                                        </td>

                                    </tr>
                                    @php $n++ @endphp
                                @endforeach
                            @endif
                            </tbody>

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
