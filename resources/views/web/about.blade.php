@extends('layouts.layout')
@section('content')
<section class="main">
    <div class="profile-view-section">

    <div class="container-fluid cp-md">
<div class="container">
	<div class="row">
        <div class="col-12">
            <div class="h3-responsive mb-2">About VivoDoc</div>
            <div class="lead mb-2">
                <p>VivoDoc (OPD-Medicine-Emergency-Diagnostic) A complete healthcare solutions with Doctors on Secure Video Call to connect patients with Online Doctors anytime anywhere through our Mobile Application.</p><p>Apart from video consultation find All types of healthcare facilities nearby like Doctors, Hospitals, Diagnostic Centres, Medical Store, Ambulance, Blood Bank etc. Book our Bike Doc
    a unique telemedicine equipment which works on real time basis carried on a bike Ambulance, bike arrives at home takes vitals calls a doctor perform tests ecg etc provides emergency medicine collects samples or transports to the hospital if advised by the doctor. We also franchise Bike Doc and OMED Digital Clinic (A unique telemedicine centre with Diagnostic, Medicine and Sample Collection facilities)</p>
            </div>
        </div>
		<div class="col-12">
			<div class="h3-responsive mb-2">About Founder</div>
			<div class="lead mb-2">Dr. Amjad Ali : Founder & CEO Always wants to do something different /extra ordinary. </div>
			<p>He is very Social, Loves to work for poor and needy people, Hence he worked with several NGOs & Government. He had participated in in Several Free Health Camps & So The Digital Health Centers also conduct Free Health Camps. He is a doctor graduated from Ukraine, Post graduate He worked at Railway Hospital, private & Government Hospitals and NGOs. He worked with several organizations like WHO, Unicef and also worked as private practitioner. He loves to do new things & believes nothing is impossible if done with honesty.</p>
			<p>Mrs. Ummi Ajami : Secretary & CFO is a Dietitian, Beautician and Social worker. Always wants to create something new, she loves creation and Innovations</p>
		</div>
	</div>
</div>
</div>

<div class="container-fluid bg-light cp-md">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="h2-responsive">How it Works?</div>
				<div class="mb-5"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-3 text-center">
				<div class="rc shadow mb-3">
					<i class="fa fa-user-plus fa-2x text-primary"></i>
				</div>
				<div class="lead mb-2">Register</div>
				<div class="text-black-50"></div>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-3 text-center">
				<div class="rc shadow mb-3">
					<i class="fas fa-map-marker-alt fa-2x text-success"></i>
				</div>
				<div class="lead mb-2">Claim Listing</div>
				<div class="text-black-50"></div>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-3 text-center">
				<div class="rc shadow mb-3">
					<i class="fab fa-sketch fa-2x text-warning"></i>
				</div>
				<div class="lead mb-2">Get Exposure</div>
				<div class="text-black-50"></div>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-3 text-center">
				<div class="rc shadow mb-3">
					<i class="fa fa-chart-line fa-2x text-danger"></i>
				</div>
				<div class="lead mb-2">Grow Business &amp; Earning</div>
				<div class="text-black-50"></div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid cp-md">
	<div class="row">
		<div class="col-12 text-center">
			<div class="h2-responsive">Why Choose Us?</div>
			<div class="mb-5"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-5 text-center">
			<div class="mb-3"><i class="fa fa-user-nurse text-danger fa-2x"></i></div>
			<div class="lead mb-2">Certified Doctors</div>
			<div class="text-black-50"></div>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-5 text-center">
			<div class="mb-3"><i class="fa fa-heartbeat text-danger fa-2x"></i></div>
			<div class="lead mb-2">High Quality Equipment</div>
			<div class="text-black-50"></div>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-5 text-center">
			<div class="mb-3"><i class="fa fa-briefcase text-danger fa-2x"></i></div>
			<div class="lead mb-2">Professional Services</div>
			<div class="text-black-50"></div>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-5 text-center">
			<div class="mb-3"><i class="fa fa-flask text-danger fa-2x"></i></div>
			<div class="lead mb-2">Mordern Technology</div>
			<div class="text-black-50"></div>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-5 text-center">
			<div class="mb-3"><i class="fa fa-handshake text-danger fa-2x"></i></div>
			<div class="lead mb-2">Advanced Treatment</div>
			<div class="text-black-50"></div>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-5 text-center">
			<div class="mb-3"><i class="fa fa-phone-alt text-danger fa-2x"></i></div>
			<div class="lead mb-2">24/7 Support</div>
			<div class="text-black-50"></div>
		</div>
	</div>
</div>
    </div>
</section>
@endsection
