@extends('layouts.layout')
@section('content')
<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">Enquiries</div>
			</div>
		</div>
	</div>
</div>
@include('web.custome-menu')
		<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
			<div class="card">
				<div class="card-body">
					<table id="example" class="display responsive nowrap" style="width:100%">
						<thead>
							<tr>
                                <th>S.no.</th>
								<th>Patient Name</th>
								<th>Question</th>
								<th>Status</th>
								<th>Asked On</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
                        @php $n=1 @endphp
						@if(isset($enquiry))
                            @foreach($enquiry as $en)
                            <tr>
                                <td>{{$n}}</td>
                                <td>{{ucwords($en->patient->name)}}</td>
                                <td>{{$en->question}}</td>
                                <td>@if($en->provider_reply == '' && $en->admin_reply == '')<span class="text-danger">Not Answered</span> @else <span class="text-primary">Answered</span>@endif</td>
                                <td>{{date('d-m-Y',strtotime($en->created_at))}}</td>
                                <td>
     <a type="button" class="btn btn-info btn-sm" href="{{url('reply-enquiry/'.$en->id)}}"> <i class="fa fa-reply"></i> View & Reply</a>
                                </td>
                            </tr>
                                @php $n++ @endphp
                            @endforeach
                            @endif
						</tbody>

					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

