@extends('layouts.layout')
@section('content')
    <section id="main">
        <div  class="login-container">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3">

			<div class="card p-4">
                <div class="row">
                    <div class="col-12 col-md-9">
                        <h1>Create an Account</h1>
                        <p>Let's help you stay on top of your health</p>
                    </div>
                    <div class="col-12 col-md-3 d-none d-md-block">
                        <img src="{{asset('web/v2//img/login-img.svg')}}" width="100%" alt="vivoDoc Login">
                    </div>
                </div>
                <form method="POST" action="{{ url('register') }}" autocomplete="false" class="frm-reg">
                    @csrf
				<div class="card-body">
                        <label class="control-label">I am a</label>
                        <div class="mt-1 mb-2">
                            @if(isset($role))
                                @if($role == 3)
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" checked class="custom-control-input" id="customRadio" required name="user_role_id" value="{{$role}}">
                                <label class="custom-control-label" for="customRadio">Patient</label>
                            </div>
                            @else
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" checked class="custom-control-input" id="customRadio2"  name="user_role_id" value="{{$role}}">
                                <label class="custom-control-label" for="customRadio2">Health Service Provider</label>
                            </div>
                                @endif
                                @endif
                        </div>
                        @error('user_role_id')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
					<div class="form-group">
                        <label for="" class="py-2">Your Name</label>
					<input type="text" name="name" class="form-control rounded-0 @error('name') is-invalid @enderror " placeholder="Your Name" value="{{old('name')}}" required>
                    </div>
                    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
					<div class="form-group">
                        <label for="" class="py-2">Your Mobile Number</label>
      <input type="text" name="mobile" class="form-control rounded-0 @error('mobile') is-invalid @enderror " placeholder="Your mobile number" maxlength="10" value="{{old('mobile')}}" oninput='this.value=(parseInt(this.value)||" ")' id="reg-mobile">
                    </div>
                    @error('mobile')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
					<div class="form-group">
                        <label for="" class="py-2">Email Address</label>
                    <input type="email" class="form-control rounded-0 @error('email') is-invalid @enderror " placeholder="Email address" name="email" value="{{old('email')}}">
                    </div>
                    @error('email')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
                    <div class="form-group">
                        <label for="" class="py-2">Gender</label>
                        <select class="form-control" name="gender" required>
                            <option selected disabled>Choose your gender</option>
                            <option value="Male" {{old('gender')=="Male" ? 'selected':'' }} >Male</option>
                            <option value="Female" {{old('gender')=="Female" ? 'selected':'' }} >Female</option>
                        </select>
                    </div>
                    @error('gender')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
					<div class="form-group">
                        <label for="" class="py-2">Password</label>
          <input type="password" class="form-control rounded-0 @error('password') is-invalid @enderror " placeholder="Enter password" name="password" value="{{old('password')}}" required>
                    </div>
                    @error('password')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
					<div class="form-group">
                        <label for="" class="py-2">Confirm Password</label>
		 <input id="password-confirm" type="password" class="form-control rounded-0" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
			{{-- <input type="password" class="form-control rounded-0 @error('c_pass') is-invalid @enderror " placeholder="Confirm Password" name="c_pass"> --}}
                    </div>
                    @if($role == 3)
                        <div class="mt-3 mb-3 text-center">I allow <strong>AAHPL</strong> to save my medical records. </div>
                    @endif
                    {{-- @error('c_pass')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror --}}
                    <div class="row pt-4">
					<button type="submit" class="btn btn-primary rounded-pill btn-block">Register</button>
                    </div>
					<div class="mt-3 text-center">Already have an account? <a href="{{url('login')}}">Sign In</a></div>
                </div>
</form>
{{--                <div class="row py-4">--}}
{{--                    <div class="col-12 text-center">--}}
{{--                        <span>OR</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="row social-login-btns">--}}
{{--                    <div class="col-12 col-md-4">--}}
{{--                        <button type="button" class="btn google-btn">--}}
{{--                            <img src="{{asset('web/v2/img/google-login.svg')}}" alt="Google login">--}}
{{--                            <span >Google</span>--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                    <div class="col-12 col-md-4">--}}
{{--                        <button type="button" class="btn fb-btn">--}}
{{--                            <img src="{{asset('web/v2/img/facebook-login.svg')}}" alt="Facebook">--}}
{{--                            <span >Facebook</span>--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                    <div class="col-12 col-md-4">--}}
{{--                        <button type="button" class="btn apple-btn">--}}
{{--                            <img src="{{asset('web/v2/img/apple-login.svg')}}" alt="Apple">--}}
{{--                            <span >Apple</span>--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </div>--}}
			</div>
		</div>
	</div>
</div>
@endsection
