@extends('layouts.layout')
@section('content')
    <section class="main">
        <div class="profile-view-section">
<div class="container">
	<div class="row">
		<div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 mb-4">
			<p><b>We support to all type of health services providers, paramedical and non-medical person,
to grow with us. VivoDoc</b> partners with leading hospitals, experienced doctors, diagnostic
centers, Pharmacies, Ambulances and other Health services providers, to improve health
outcomes for patients everywhere, as well as profitability for our partners. Our strong partner
relationship makes deployment and speed-to-market easy to achieve. Join the growing
community of VivoDoc partners across India.</p>
			<p><strong>VivoDoc also supports Entrepreneurs grow with us through Franchise of its Telemedicine
Services.</strong></p>
			<p>Vivo Clinics (An advance Telemedicine system with X-ray viewer, ECG, Otoscope,
Dermascope, Digital stethoscope etc.)</p>
			<p>And</p>
			<p>Bike Doc (An advance Telemedicine system on a Bike Ambulance (24X7) services with Instant diagnostic, ECG, Emergency drugs and Sample collection facilities)</p>
			<p>We support in marketing and promotions of our franchised partners. </p>
			<p>We also support 24X7 for smooth functioning of the Vivo clinics, Bike Doc and VivoDoc mobile App.</p>
		</div>
		<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
			<img src="https://images.pexels.com/photos/45842/clasped-hands-comfort-hands-people-45842.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" class="img-fluid img-thumbnail">
		</div>
	</div>
</div>
        </div>
    </section>
@endsection
