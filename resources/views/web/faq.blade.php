@extends('layouts.layout')
@section('content')
    <section class="main">
        <div class="profile-view-section">
<div class="container">
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="card mb-4">
				<div class="card-header">What is VivoDoc? </div>
				<div class="card-body">
					<p>VivoDoc App gives you an option to Book appointmentt or consult/schedule a video/audio/chat session with the doctor. Apart from Doctor's consultation find and contact with All types of healthcare facilities nearby like Doctors, Hospitals, Diagnostic Centers, Medical Store, Ambulance, Blood Bank etc. You can create your account, store your medical records, and view/manage all your chats/enquiries.</p>
				</div>
			</div>
			<div class="card mb-4">
				<div class="card-header">How to use VivoDoc App or website?</div>
				<div class="card-body">
					<p>Download our App from Play Store or visit www.vivodoc.in in browser, Click on register as patient, Enter your name, email id, mobile number and choose your password, your profile is created. Manage your profile with advanced profile editor, store your medical records, and view/manage all your chats/enquiries, view your reviews and ratings, update password annd Logout whenever you want.</p>
				</div>
			</div>
			<div class="card mb-4">
				<div class="card-header">Are the Doctors qualified?</div>
				<div class="card-body">
					<p>All the doctors that you consult or get appointment on VivoDoc are verified medical practitioners, their license and speciality documents are manually verified by our team. We also take feedback/ratings from patients to ensure that doctors maintain the standards.</p>
				</div>
			</div>
			<div class="card mb-4">
				<div class="card-header">Can a Doctor treat through Online consultation?</div>
				<div class="card-body">
					<p>Online doctor's consultation is a good option because, according to doctors all around the world, 90% of diagnosis can be made by asking the right questions. So, more often than not, a doctor doesn't actually have to be in the room with you to know what's wrong and prescribe medication. Most importantly You can get an immediate primary treatment/opinion and meet in person later for further management.</p>
				</div>
			</div>
			<div class="card mb-4">
				<div class="card-header">Can we trust on the service providers ?</div>
				<div class="card-body">
					<p>All the doctors and healthcare services providers listed on VivoDoc are verified,  their license and other required documents are manually verified by our team. We also take feedback/ratings from patients to ensure that they maintain the quality, you can also report/feedback and give ratings. VivoDoc reserves the right to remove the service provider after continuous POOR feedbacks.</p>
				</div>
			</div>
			<div class="card mb-4">
				<div class="card-header">Are the Pharmacies on VivoDoc genuine?</div>
				<div class="card-body">
					<p>Certainly, all the pharmacies offering prescription medications online on VivoDoc are licensed. Our team verify their licence number manually before listing them. </p>
				</div>
			</div>
			<div class="card mb-4">
				<div class="card-header">How to order medicine online?</div>
				<div class="card-body">
					<p>To contact our pharmacies online, you can send your questions by using either “Ask Questions or Call Me” option available in all the Pharmacies information details pages or by completing the form in the Contact Us section where you can ask your all type of questions, our team will contact you asap.</p>
				</div>
			</div>
			<div class="card mb-4">
				<div class="card-header">How to get other services?</div>
				<div class="card-body">
					<p>To contact our services providers you have to search required service in your area, you can call or send your questions by using either “Ask Questions or Call Me” option available in their information details pages or by completing the form in the Contact Us section where you can ask your all type of questions, our team will contact you asap.</p>
				</div>
			</div>
			<div class="card mb-4">
				<div class="card-header">How to get Free services?</div>
				<div class="card-body">
					<p>Many Doctors are providing Free Consultation on our platform (subject to availability) Apart from that VivoDoc also provide Free Services/Camps Time to Time. You can Search, Contact our services providers in your area, Send your questions by using “Ask Questions” option available in their information details pages are completly Free. Contact Us section is also Free, where you can ask your all type of questions, our team will contact you asap.</p>
				</div>
			</div>
			<div class="card mb-4">
				<div class="card-header">How to register as Doctor/Service provider and what are the benefits?</div>
				<div class="card-body">
					<p>To register as Doctor/Service provider visit www.vivodoc.in in browser, Click on register as 'service provider' Enter your name, email id, mobile number and choose your password, your profile is created. Choose your services categories and upload relavent document(s) your profile will be approved asap. Manage your profile with advanced profile editor, upload your profile picture and other services related images, Mange your services related descriptions/information and add/remove categories. View enquiries and reply to them, view reviews and ratings. If you are a Doctor, Provide online consultation to patients, Add/Manage appointmentt schedule, View calender.Easily add or modify your details, Add information that matters to your patients - timings, fees, new services, and much more,  Keep all your information up to date.</p>
				</div>
			</div>
		</div>
	</div>
</div>
        </div>
    </section>
@endsection
