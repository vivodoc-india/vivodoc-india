@extends('layouts.layout')
@section('content')
<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">Checkup Package</div>
			</div>
		</div>
	</div>
</div>
<div class="container cp-md">
	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
			<div class="view zoom">
				<img src="{{asset('web/img/339620.jpeg')}}" class="img-fluid">
			</div>
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
			<div class="h2-responsive mb-2">Health Checkup Packages</div>
			<p>There are wide range of Health Packages facility is available with All type of Blood & Urine Tests, ECG vitals, along with Doctors Consultation at our all Digital Health Centers on ultra low charges.</p>
			<p>And Also at your doorstep (Home Service Charges extra)..</p>
			<div class="h3-responsive mb-3">Schedule online. It's easy, fast and secure</div>
			<button type="button" class="btn btn-danger rounded-pill">Book an Appointment</button>
		</div>
	</div>
</div>
@endsection
