<div class="container-fluid footer-bg cp-md">
	<div class="container">
		<div class="row white-text">
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">
				<h5 class="text-uppercase">About</h5>
				<div class="divide"></div>
				<div>
					<a href="contact.php" class="d-block white-text mb-3">Contact us</a>
					<a href="#" class="d-block white-text mb-3">Support</a>
					<a href="#" class="d-block white-text mb-3">Career</a>
					<a href="#" class="d-block white-text mb-3">FAQs</a>
					<a href="register.php" class="d-block white-text mb-3">Register</a>
					<a href="login.php" class="d-block white-text">Login</a>
				</div>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">
				<h5 class="text-uppercase">Resources</h5>
				<div class="divide"></div>
				<div>
					<a href="#" class="d-block white-text mb-3">Search Doctor</a>
					<a href="#" class="d-block white-text mb-3">Search Hospital</a>
					<a href="#" class="d-block white-text mb-3">Search Diagnostic</a>
					<a href="#" class="d-block white-text mb-3">Search Medicine</a>
					<a href="#" class="d-block white-text mb-3">Search Ambulance</a>
					<a href="#" class="d-block white-text">Search Health Related Service</a>
				</div>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">
				<h5 class="text-uppercase">More</h5>
				<div class="divide"></div>
				<div>
					<a href="#" class="d-block white-text mb-3">Help</a>
					<a href="#" class="d-block white-text mb-3">Terms &amp; Services</a>
					<a href="#" class="d-block white-text mb-3">Book Appointment</a>
					<a href="#" class="d-block white-text mb-3">Privacy Policy</a>
					<a href="#" class="d-block white-text">Subscribe</a>
				</div>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
				<h5 class="text-uppercase">Contact Information</h5>
				<div class="divide"></div>
				<div class="text-uppercase">Address</div>
				<div class="mb-3">123 Street Name, City, India</div>
				<div class="text-uppercase">Phone</div>
				<div class="mb-3">+91-9638527412</div>
				<div class="text-uppercase">Email</div>
				<div class="mb-3">info@hitechecommerce.com</div>
				<div class="text-uppercase">Follow Us</div>
				<div class="divide"></div>
				<div>
					<a href="#" class="d-inline-block text-white mr-3"><i class="fab fa-facebook-f"></i></a>
					<a href="#" class="d-inline-block text-white mr-3"><i class="fab fa-google-plus-g"></i></a>
					<a href="#" class="d-inline-block text-white mr-3"><i class="fab fa-instagram"></i></a>
					<a href="#" class="d-inline-block text-white mr-3"><i class="fab fa-youtube"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid black pt-2 pb-2">
	<div class="container">
		<div class="row white-text small no-gutters">
			<div class="col-12 col-md-10 text-left">&copy; OMED 2020. All Right Reserved</div>
			<div class="col-12 col-md-2 text-white-50">Developed By : <a href="https://www.friendzitsolutions.biz/" class="text-white" target="_blank">Friendz IT Solutions</a></div>
		</div>
	</div>
</div>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/popper.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/mdb.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/gallary.js"></script>
<script type="text/javascript" src="js/jquery.viewbox.js"></script>
<script type="text/javascript">
	
	$(document).ready(function() {
		$('#example').DataTable();
	} );	
	
	$(document).ready(function() { 
	  var owl = $("#new-registered");
	  owl.owlCarousel({
		  items : 4,
		  itemsDesktop : [1000,4],
		  itemsDesktopSmall : [900,3],
		  itemsTablet: [600,1],
		  itemsMobile : false,
		  pagination : false
	  });
	  // Custom Navigation Events
	  $(".next").click(function(){
		owl.trigger('owl.next');
	  })
	  $(".prev").click(function(){
		owl.trigger('owl.prev');
	  })
	});
	
	$(document).ready(function() { 
	  var owl = $("#testimonial");
	  owl.owlCarousel({
		  items : 1,
		  itemsDesktop : [1000,1],
		  itemsDesktopSmall : [900,1],
		  itemsTablet: [600,1],
		  itemsMobile : false
	  });
	});
	
	$(document).ready(function() { 
	  var owl = $("#clients");
	  owl.owlCarousel({
		  items : 5,
		  itemsDesktop : [1000,4],
		  itemsDesktopSmall : [900,4],
		  itemsTablet: [600,1],
		  itemsMobile : false,
		  pagination: false,
		  autoPlay : 3000
	  });
	});
	
	$(document).ready(function() { 
	  var owl = $("#events");
	  owl.owlCarousel({
		  items : 3,
		  itemsDesktop : [1000,3],
		  itemsDesktopSmall : [900,3],
		  itemsTablet: [600,1],
		  itemsMobile : false,
		  pagination: false,
		  autoPlay : 3000
	  });
	});

	$(function(){
		$('.image-link').viewbox();
	});

	
</script>
</body>
</html>