@extends('layouts.layout')
@section('content')
    <section clas="main">
        <div class="profile-view-section">
{{--    <div class="container-fluid cp-md">--}}
{{--        <div class="row">--}}
{{--            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">--}}
                @include("web.left-menu")
{{--            </div>--}}
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <table id="example" class="display responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>S.no.</th>
                                <th>Date Date</th>
                                <th>Total Allowed Bookings</th>

                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $n=1 @endphp
                            @if(isset($details))
                                @foreach($details as $en)
                                    <tr>
                                        <td>{{$n}}</td>
                                        <td>{{date('d-m-Y', strtotime($en->date))}}</td>
                                        <td>{{$en->total_bookings}}</td>

                                        <td>
                                            <a class="btn btn-info btn-sm" href="{{url('booking/edit/'.$en->id)}}"> <i class="fas fa-edit"></i> Edit</a>
                                            <a type="button" class="btn btn-danger btn-sm delete_button_action" href="javascript:void(0)" data-url="{{url('booking/remove')}}" data-id="{{$en->id}}" data-toggle="modal" data-target="#deleteModal"> <i class="fas fa-trash-alt text-white"></i> Remove</a>
                                        </td>
                                    </tr>
                                    @php $n++ @endphp
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    </section>
@endsection
{{--@section('additional_script')--}}

{{--@endsection--}}

