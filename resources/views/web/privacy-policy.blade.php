@extends('layouts.layout')
@section('content')
    <section class="main">
        <div class="profile-view-section">
    <div class="container">
        <div class="row">
            <!-- Col-1 -->
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
    <div class="h3-responsive  mb-2">OBOUT VivoDoc</div>
    <p class="text-justify">AAHPL (Amjad Ali Healthcare Private Limited) is the author and publisher of the internet resource www.vivodoc.in and the mobile application ‘VivoDoc’ owns and operates the services provided through the Website and Mobile Application.
        You get an option to ‘Consult Now’ or ‘Book an Appointment’ When you choose these options, you can directly consult or schedule a video/audio/chat session with the doctor through technological services provided by VivoDoc. Apart from video consultation find All types of healthcare facilities nearby like Doctors, Hospitals, Diagnostic Centers, Medical Store, Ambulance, Blood Bank etc. Book our ‘Bike Doc’ (Telemedicine & Diagnostic devices with Emergency drugs & Sample collection kit) a Health staff on a Bike Ambulance arrives at home takes vitals, connects a doctor on video call, helps Doctor to examine the patient with the help of medical devices, performs ECG or other tests, provides emergency medicine collects samples or transports to a health facility if advised by the doctor. We also franchise ‘Bike Doc’ and ‘Vivo Digital Clinic’ a unique telemedicine centre with Pathology, ECG, Medicine, Sample Collection and other facilities.
    </p>
</div>
            <!-- Col-2 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">TERMS OF USE</div>
                <p class="text-justify">We request you to go through these ‘Terms of Use’ before you decide to avail the Services of VivoDoc.
                    By registering with the Company to avail our Services, you irretrievably accept all the obligations stipulated in these Terms of Use and agree to abide by them. Accessing the App or Website through any medium, including but not limited to accessing through mobile phones, smart phones, Laptop, Computer and tablets, are also subjected to these Terms of Use. These Terms of Use surpass all previous oral and written terms and conditions (if any) communicated to you. By using this App and website, you signify your agreement to these Terms of Use as well as the privacy policy, which is hereby incorporated by reference herein.
                    We reserve the right to modify or terminate any portion of the App, Website or the Services offered by the Company for any reason, without notice and without liability to you or any third party. To make sure you are aware of any changes, please review these Terms of Use periodically.
                    VivoDoc can be used in cases of medical emergencies just for a quick and primary assessment VivoDoc does not, and should not be considered in any form to be a substitute for a doctor or a hospital. We encourage you to independently verify any information you see on the App, Website with respect to a doctor or health care provider that you seek to make an appointment with.

                </p>
            </div>

            <!-- Col-3 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">REGISTRATION</div>
                <p class="text-justify">If you wish to consult a doctor on the App, Website, you will have to register and become a registered user of VivoDoc,  prior to the completion of any transaction on the App, Website. To register on the App, a user may have to provide information such as his/her name, email address and phone number and create a user name and password. You understand that VivoDoc may introduce a fee for registration (“Registration Fees”). Registration Fees, if and when introduced shall be non-refundable. VivoDoc may introduce special features which may be made available to VivoDoc Users who pay Registration Fees. Registration is only a one time process and if the VivoDoc User has been previously registered, s/he shall login / sign into his / her account using the same credentials as provided during the registration process. We request you to please safeguard your password to your account on the App, Website and make sure that others do not have access to it. It is your responsibility to keep your account information current. VivoDoc User may wish to consult a Doctor through his/her account for his/her family members and friends. While VivoDoc User will be able to do this, please note that such VivoDoc User will be responsible for any activity that is undertaken through his/her account on behalf of his/her family members and friends.
                </p>
            </div>

            <!-- Col-4 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">AVAILING SERVICES</div>
                <p class="text-justify">You understand and agree that all the services availed and the issues associated with the doctors on the App, Website, including but not limited to the medical outcomes and service issues, is strictly between you and the service providers, doctors. You shall not hold VivoDoc responsible for any such outcome and associated issues. VivoDoc is not involved in providing any healthcare or medical advice or diagnosis and hence is not responsible for any outcome between you and the doctor, service provider you interact with. If you decide to engage with a doctor to provide medical services to you, you do so at your own risk. The results of any search you perform on the App, website for services, doctors, or provision of access to any healthcare service provider or doctor on the basis of your specific request, should not be treat as endorsement by VivoDoc. VivoDoc shall not be responsible for any breach of service or service deficiency by any doctor or healthcare service provider. We cannot assure that all transactions will be completed nor do we guarantee the ability or intent of doctors or other health care providers to fulfill their obligations in any transaction. We advise you to perform your own investigation prior to selecting a doctor or a healthcare service provider.
                </p>
                <p class="text-justify">
                    You understand that doctors that you see on the App, Website may or may not be real doctors. While VivoDoc endeavors to only approve genuine doctors we cannot and do not guarantee that people claiming to be doctors are real doctors. We do not guarantee that the doctor who registered is the person currently consulting with you on the App, Website.  Someone else may be using the doctor’s Smartphone. VivoDoc Users have to make their own determination of the true qualifications and skills of others users of the App, website.
                    You understand that third party services may be made available by the Company on the App, Website or by a doctor using the app, Website. For example, you may be referred to a diagnostic centre to get an investigation. Please note that in the event you choose to avail any such third party services that are made available on the App, website you will be absolutely and solely responsible for your interactions with such third party service providers. VivoDoc shall not be held responsible for any lapses, shortcomings or deficiency of services by such third party service providers to you. VivoDoc hereby does not endorse the services of any third party service providers that are made available on the App, Website.

                </p>
            </div>

            <!-- Col-5 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">UPLOADING HEALTH RECORDS</div>
                <p class="text-justify">You understand that the Doctor you are chatting to may upload information for you on the app, Website. This includes reports, prescriptions, and summaries of the consultation or other information. You too have the option of uploading your health records, information on the App, Website in order to enable your doctor access your previous medical history. And you understand that all information, records uploaded together with Doctor’s summaries will be stored by VivoDoc and made available only to doctors that you chat to. While VivoDoc takes utmost care with respect to the security of the information you decide to upload, you understand that any information that you disclose on the App, Website is at your own risk. By uploading / sharing / disclosing your medical information, you hereby give your consent to VivoDoc to store such health records, information on VivoDoc’s servers. VivoDoc will retain such medical records, information you upload on the App, Website for as long as it is needed to fulfill the service you seek to avail on VivoDoc. If you delete your account, VivoDoc will delete such medical / health information. But please note: (1) there might be some latency in deleting this information from our servers and back-up storage; and (2) we may retain this information if necessary to comply with our legal obligations, resolve disputes, or enforce our agreements.
                </p>
                <p class="text-justify">
                    If you do not trust VivoDoc with such information, please do not share such information on the App. While VivoDoc endeavors to protect any information uploaded or shared by you on the App, you understand that VivoDoc shall not be held liable or responsible for the loss or damage to such information on the App. It is your responsibility to ensure that you have all your information, medical or otherwise, stored on your hard drive. You hereby agree not to use the App as a “data room” to store your medical / health records.

                </p>
            </div>

            <!-- Col-6 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">RATINGS</div>
                <p class="text-justify">After availing each service our consultation you understand that services providers, doctors may choose to rate you. You also understand that if you chat to a doctor who does not have you listed on their “My Patients” list, then other doctors chosen by VivoDoc will also be able to rate that chat. These doctors chosen by VivoDoc will be able to view the entire chat between you and the doctor but will not be shown the name of the doctor or the name of the user (your name). The chats will be rated anonymously. The chosen doctors will rate the way you chatted to on the quality of the conversation. You hereby give your consent to VivoDoc to show your chats to doctors selected by VivoDoc so that they can rate the chat. You understand that these ratings of the doctors and of the users will be shared with other doctors using the app. You understand and consent to the doctor ratings also being shared publicly, especially with the press. VivoDoc will publish lists of top rated doctors by speciality and by city so that users can learn which the best rated doctors are. Further you understand that VivoDoc may choose at its own discretion to also allow anonymous ratings of chats between you and doctors even when you are on that doctor’s “My Patients” list. You commit to hold VivoDoc harmless regardless of the impact of these ratings. VivoDoc is not responsible for any loss due to these ratings.You understand that although every effort is made to ensure high quality ratings, VivoDoc is not responsible for any errors or omissions in the ratings or the effect of such errors.
                </p>

            </div>

            <!-- Col-7 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">SMS OR NOTIFICATION</div>
                <p class="text-justify">You understand that once you register as a VivoDoc User on the App, Website, you will receive SMS or Chat messages from VivoDoc on your registered mobile number. These messages could relate to your registration, transactions that you carry out through the App, Website and promotions that are undertaken by VivoDoc. Please note that VivoDoc will send these messages only to the registered mobile number or such other number that you may designate for any particular transaction. It is your responsibility to ensure that you provide the correct number for the transaction you wish to enter. Further VivoDoc may also send notifications and reminders to you with respect to appointments scheduled by you for the Services that you have subscribed to on the VivoDoc. Please note that while VivoDoc endeavors to provide these notifications and reminders to you promptly, VivoDoc does not provide any guarantee and shall not be held liable or responsible for the failure to send such notifications or reminders to you. It is your responsibility to ensure that you attend any appointments that you may schedule with a doctor or a healthcare service provider through VivoDoc.
                </p>

            </div>

            <!-- Col-8 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">CHARGES FEES</div>
                <p class="text-justify">You understand that VivoDoc provides various Services on the App, Website. While some of these Services are provided for free, there are certain Services for which costs will be incurred by you, if you decide to avail such Services (“Paid Services”). VivoDoc may in such event provide billing services on behalf of the doctor or healthcare service provider. Should you wish to avail those Paid Services, you acknowledge that VivoDoc will collect the payment for such Paid Services for and on behalf of the doctor or healthcare service provider. You acknowledge and confirm that VivoDoc shall not be liable for the treatment, services or be treated as the healthcare service provider on account of such collection of the payments or for provision of Paid Services, for any reason whatsoever.
                </p>

            </div>

            <!-- Col-9 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">OPTING DOCTOR OR HEALTHCARE SERVICE PROVIDER</div>
                <p class="text-justify">VivoDoc may allow you to select doctors to consult with or schedule appointments with doctors or healthcare service providers registered on VivoDoc and to whom certain specific terms and conditions apply. These terms and conditions for the doctors can be accessed www.VivoDoc.com, While choosing your doctor or health care provider on the App, you understand and accept that:
                </p>
                <p class="text-justify">
                    You are ultimately responsible for choosing your own doctor or healthcare service provider, VivoDoc shall not be held responsible or liable for the selection of such doctor or healthcare service provider, VivoDoc will provide you with lists and/or profile previews of doctors and healthcare service providers who may be suitable based on information that you provide to VivoDoc (such as area of specialty and geographical location). For certain specific Services, VivoDoc may chose a doctor or healthcare service provider on your behalf based on the nature of services you request and on the doctor or healthcare service provider available to provide you with the services for the same. Please note that VivoDoc does not recommend endorse any doctors or healthcare service providers mentioned on VivoDoc  and does not make any representations or warranties with respect to these doctors or healthcare service providers or the quality of the healthcare services they may provide. VivoDoc shall not be liable for any reason whatsoever for the services provided by the healthcare service provider or doctor, and we bear no liability for the consequences to you, of your use of the App, Website or Services. The information found on the App, Website is mainly self-reported by the doctor or the healthcare service provider. Such information often changes frequently and may become out of date or inaccurate.
                </p>
                <p class="text-justify">
                    No Doctor – Patient Relationship Please note that some of the content, text, data, graphics, images, information, suggestions, guidance, and other material (collectively, “Information”) that may be available on the App, Website (including information provided in direct response to your questions or postings) may be provided by individuals in the medical profession. The provision of such Information does not create a licensed medical professional/patient relationship, between VivoDoc and you and does not constitute an opinion, medical advice, or diagnosis or treatment of any particular condition, but is only provided to assist you with locating appropriate medical care from a qualified practitioner or service provider.
                </p>

                <p class="text-justify">
                    Please note that we do not recommend or endorse any doctors, dentists, healthcare service providers, procedures, opinions, or other Information that may appear on the App, Website. If you rely on any of the Information provided by VivoDoc, you do so solely at your own risk. The Information that you obtain or receive from VivoDoc, and its employees, contractors, partners, sponsors, advertisers, licensors or otherwise on the App or Website is for informational and scheduling purposes only. All healthcare related information comes from independent healthcare service provider who use the App, Website and to whom certain specific terms and conditions apply. These terms and conditions can be accessed at www.VivoDoc.com.
                </p>
<p class="text-justify">
    Please note that the doctors who are consulting with you are not providing you with a medical consultation or any medical advice. They are only providing you with general health information and guidance. The doctors are not able to examine you physically so cannot make a diagnosis of your medical condition. You may not rely on the information provided by doctors but treat it as another online source or information.  You must continue to visit your doctor for medical consultations. Use of VivoDoc App or Website is not a substitute for medical consultations and visiting doctors and hospitals. It is purely for general information and guidance. Please visit in person your own doctor (your real-world doctor who you have a physical relationship with) or your healthcare provider if you have any questions about a symptom or a medical condition, and before starting or stopping any treatment or before taking any drug or changing your diet. You may avail physical appointment services on VivoDoc subject to availability of the particular doctor’s schedule. Visit any nearby health facility immediately if you may or do have a medical emergency. You may avail Emergency services, Ambulance services on VivoDoc subject to availability of the particular service provider. Never disregard professional medical advice or delay in seeking medical advice or treatment, because of something you read or chatted or learn on.
</p>

            </div>

            <!-- Col-10 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">TERMINATION</div>
                <p class="text-justify">VivoDoc reserves the right, in the event you breach these Terms of Use stipulated herein, to suspend and / or terminate your access to the App, Website, with or without notice to you. Any suspected illegal, fraudulent or abusive activity may be grounds for terminating your access to the App, Website. Upon suspension or termination, your right to procure the Services on the App, Website shall immediately cease and the Company reserves the right to remove or delete your information that is available with the Company, including but not limited to login and account information.
                </p>
            </div>

            <!-- Col-11 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">Limitation of Liability of VivoDoc</div>
                <p class="text-justify">You expressly understand that under no circumstances, including, but not limited to, negligence, shall the Company be liable to you or any other person or entity for any direct, indirect, incidental, special, remote or consequential damages, including, but not limited to damages for loss of profits, goodwill, use, data or other intangible losses, resulting from circumstances, including but not limited to:<br>
                    The use or the inability to use the App and / or Services; or the cost of procurement of substitute goods and services resulting from any goods, data, information or services purchased or obtained or messages received or transactions entered into through or from the App, Website and / or Services; or unauthorized access to or alteration of your transmissions or data; or any other matter relating to the App, Website and / or services.<br>
                    To the fullest extent permitted by law, in no event will the Company or its affiliates be liable for any direct, indirect, special, incidental, punitive, exemplary or consequential damages, whether or not the Company has been warned of the possibility of such damages. Notwithstanding anything to the contrary in these Terms of Use, the Company’s liability under these Terms of Use shall in no event exceed the Service fee amounts collected from you by VivoDoc, but excluding any amounts collected by it for and on behalf of the doctor or healthcare service provider as a collection agent, for the provision of any medical service by such healthcare service provider.

                </p>
            </div>

            <!-- Col-12 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">Limitation of Liability of DOCTORS</div>
                <p class="text-justify">You expressly understand that under no circumstances, including, but not limited to, negligence, shall the doctor be liable to you or any other person or entity for any direct, indirect, incidental, special, remote or consequential damages, including, but not limited to damages for loss of profits, goodwill, use, data or other intangible losses, resulting from circumstances, including but not limited to:<br>
                    The use or the inability to use the App, Website and / or Services. . You understand that a Doctor you wish to consult on the App may not be available at any given time. The Doctor and nULTA do not warrant constant Doctor Availability on the App, Website. Even when a Doctor appears online, he may not be able to respond to a query immediately. Doctors may also choose not to respond to queries for any reason including but not limited to the query being unrelated to their specialty; or the cost of procurement of substitute goods and services resulting from any goods, data, information or services purchased or obtained or messages received or transactions entered into through or from the App, Website and / or Services; or unauthorized access to or alteration of your transmissions or data; or any other matter relating to the App, Website and / or services.<br>
                    To the fullest extent permitted by law, in no event will the doctor or its affiliates be liable for any direct, indirect, special, incidental, punitive, exemplary or consequential damages, whether or not the Company has been warned of the possibility of such damages. Notwithstanding anything to the contrary in these Terms of Use, the doctor’s liability under these Terms of Use shall in no event exceed the Service fee amounts collected from you by VivoDoc for and on behalf of the doctor or healthcare service provider.<br>
                    By Using VivoDoc App or Website You Represent And Warrant That:
                    You are 18 years of age or older and that your use of the App, Website shall not violate any applicable law or regulation;<br>
                    All registration information you submit is truthful and accurate and that you agree to maintain the accuracy of such information;<br>
                    Your registration on the App, Website is solely for your personal and non-commercial use. Any use of this App, Website or its content other than for personal purposes is prohibited.<br>
                    Your personal and non-commercial use of this App, Website shall be subjected to the following restrictions:
                    You may not modify, decompile, reverse engineer, or disassemble any content of the App, Website; or you may not remove any copyright, trademark registration, or other proprietary notices from the App, Website. You further agree not to access or use the App, Website in any manner that may be harmful to the operation of VivoDoc or its content.<br>
                    You will not use the Service available on the App, Website for commercial purposes of any kind; or use the App, Website and / or services in any way that is unlawful, or harms the Company or any other person or entity, as determined in the Company’s sole discretion.<br>
                    You will not post, submit, upload, distribute, or otherwise transmit or make available any software or other computer files that contain a virus or other harmful component, or otherwise impair or damage the App, Website and / or Services or any connected network, or otherwise interfere with any person or entity’s use or enjoyment of the App, Website and / or the Services.<br>
                    You will not engage in any form of antisocial, disrupting, or destructive acts, including ‘flaming’, ‘spamming’, ‘flooding’, ‘trolling’, ‘phishing’ and ‘griefing’ as those terms are commonly understood and used on the Internet.<br>
                    The Company cannot and will not assure you that other VivoDoc users are or will be complying with the foregoing rules or any other provisions of these Terms of Use, and, as between you and the Company, you hereby assume all risk of harm or injury resulting from any such lack of compliance.<br>
                    All information, content and material contained in the App, Website and / or Services are the Company’s intellectual property. All trademarks, services marks, trade names, and trade dress are proprietary to the Company. No information, content or material from the App, Website and / or Services may be copied, reproduced, republished, uploaded, posted, transmitted or distributed in any way without the Company’s express written permission.<br>
                    You acknowledge that when you access a link that leaves the App or Website, the site you will enter into is not controlled by the Company and different terms of use and privacy policies may apply. By accessing links to other sites, you acknowledge that the Company is not responsible for those sites. The Company reserves the right to disable links from third-party sites to the App, Website.<br>
                    You expressly understand and agree that:<br>
                    The information, content and materials on the App, Website and / or Services are provided on an “as is” and “as available” basis. The Company and all its subsidiaries, affiliates, officers, employees, agents, partners and licensors disclaim all warranties of any kind, either express or implied, including but not limited to, implied warranties on merchantability, fitness for a particular purpose and non-infringement.<br>
                    The Company does not warrant that the functions contained in content, information and materials on the App, Website and / or Services, including, without limitation any third party sites or services linked to the App, Website and / or services will be uninterrupted, timely or error-free, that the defects will be rectified, or that the App, Website or the servers that make such content, information and materials available are free of viruses or other harmful components.<br>
                    Any material downloaded or otherwise obtained through the App, Website and / or Services are accessed at your own risk, and you will be solely responsible for any damage or loss of data that results from such download to your computer system.<br>
                    You hereby indemnify, defend, and hold the Company, the Company’s distributors, agents, representatives and other authorized users, and each of the foregoing entities’ respective resellers, distributors, service providers and suppliers, and all of the foregoing entities’ respective officers, directors, owners, employees, agents, representatives, harmless from and against any and all losses, damages, liabilities and costs arising from your use of the App or Website.<br>
                    You hereby indemnify, defend, and hold the doctor harmless from and against any and all losses, damages, liabilities and costs arising from your use of the App, Website.<br>
                    Applicable Law The interpretation of this Agreement and the resolution of any disputes arising under these Terms of Use shall be governed by the laws of India. If any action or other proceeding is brought on or in connection with this Agreement, the venue of such action shall be exclusively at Patna, Bihar, India.
                </p>
            </div>

            <!-- Col-13 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">ARBITRATION</div>
                <p class="text-justify">Any dispute, claim or controversy arising out of or relating to this notice or the breach, termination, enforcement, interpretation or validity thereof, including the determination of the scope or applicability of these Terms of Use to arbitrate, or to your use of the App, Website or the Services or information to which it gives access, shall be determined by arbitration in India, before a single arbitrator. Arbitration shall be conducted in accordance with the Arbitration and Conciliation Act, 1996. The venue of such arbitration shall be Patna, Bihar, India. All proceedings of such arbitration, including, without limitation, any awards, shall be in the English language. The award shall be final and binding on the Parties.<br>
                    The Parties shall have the right to apply to a court of competent jurisdiction to obtain interim injunctive relief in respect of any dispute, pending resolution of such dispute in accordance with Clause 11(a).<br>
                    Severability If any provision of these Terms of Use is held by a court of competent jurisdiction or arbitral tribunal to be unenforceable under applicable law, then such provision shall be excluded from these Terms of Use and the remainder of these Terms of Use shall be interpreted as if such provision were so excluded and shall be enforceable in accordance with its terms; provided however that, in such event these Terms of Use shall be interpreted so as to give effect, to the greatest extent consistent with and permitted by applicable law, to the meaning and intention of the excluded provision as determined by such court of competent jurisdiction or arbitral tribunal.


                </p>
            </div>

            <!-- Col-14 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">PRIVACY UPDATES</div>
                <p class="text-justify">VivoDoc reserves the right to change or update the privacy and policy at anytime. Such changes shall be effective immediately after posting it on App, Website.
                </p>
            </div>

            <!-- Col-15 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">NOTICE</div>
                <p class="text-justify">Any notice or other communication required or permitted to be given between the Parties hereto shall be in writing, in the English language, and shall be sent by registered post, e-mail, or mailed by prepaid internationally-recognized courier, or otherwise delivered by hand or by messenger, addressed to such Party’s address as set forth below or at such other address as the Party shall have furnished to the other Party in writing in accordance with this provision. The notice to you will be sent at the phone number provided by you to us when you registered as VivoDoc User, and the notice to VivoDoc will be sent at the contact details provided by VivoDoc.<br>
                    You have read these Terms of Use and agree to all of the provisions contained above.

                </p>
            </div>
            <!-- Col-16 -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  mb-2">
                <div class="h3-responsive  mb-2">CONTACT DETAILS</div>
                <p class="text-justify">Amjad Ali Healthcare Private Limited,<br>
                    Vivo Digital Clinic, AIIMS Road, Nayatola<br>
                    Phulwarisharif, Patna (801505) Bihar, India.<br>
                    Phone: +919471002110<br>
                    Email: aalihealthcare@gmail.com
                </p>
            </div>

        </div>
    </div>
        </div>
    </section>
@endsection
