@extends('layouts.layout')
@section('content')
<div class="header cp-lg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-white h2-responsive text-uppercase">Medical Product</div>
			</div>
		</div>
	</div>
</div>
<div class="container cp-md">
	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
			<div class="view zoom">
				<img src="{{asset('web/img/248526.jpeg')}}" class="img-fluid">
			</div>
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-4">
		<div class="h2-responsive mb-2">Medicine &amp; Medical Products</div>
			<p>Get appointment for all type of diagnostic services, appointment will be taken on your behalf with online payment to us. (our Charges will be less than the actual charges of the respsctive Diagnostic service provider )</p>
			<p>All type of medicines and wide range of other medical products with discounts is also available at our multiple  AAHcenters.</p>
			<div class="h3-responsive mb-3">Order online. It's easy, fast and secure.</div>
			<button type="button" class="btn btn-danger rounded-pill">Order Now</button>
		</div>
	</div>
</div>
@endsection
