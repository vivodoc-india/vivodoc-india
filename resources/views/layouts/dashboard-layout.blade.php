<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{isset($title)?$title:config('app.name')}}</title>
    <link rel="icon" type="image/png" href="{{asset('web/img/favicon.png')}}">
{{--    <link href="https://fonts.googleapis.com/css?family=Raleway:500|Roboto:500&display=swap" rel="stylesheet">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" />
    {{-- <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/> --}}
    <link rel="stylesheet" href="{{asset('web/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/alertify.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/mdb.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/drawer.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/owl.theme.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/viewbox.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/style.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('web/rating/bootstrap-rating.css')}}">

    {{--     <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css" rel="stylesheet" />--}}
    <link href="{{asset('web/dist/css/select2.min.css')}}" rel="stylesheet"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    @yield('custom_style')
</head>
<body class="drawer drawer--left">
<div class="modal fade" id="otpModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-notify modal-primary modal-dialog-centered" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header text-center">
                <h4 class="modal-title white-text w-100 font-weight-bold py-2">Verify Mobile</h4>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="md-form mb-5">
                    <i class="fas fa-key prefix grey-text"></i>
                    <input type="text" name="otp_input" id="form3" class="form-control validate mob_otp" oninput='this.value=(parseInt(this.value)||" ")'>
                    <label data-error="wrong" data-success="right" for="form3">Enter OTP</label>
                    <span class="msg_otp small"></span>
                </div>

                <!--Footer-->
                <div class="row mb-3">
                    <div class="col-md-6 offset-md-3">
                        <center><span id="time" style="color:#0474A4; font-weight:bold;font-size:1.5em;">0:30</span></center>
                    </div>
                </div>
                <div class="justify-content-center">
                    <center>

                        <button type="button" class="btn btn-outline-warning rounded-0 resend_otp"  style="display:none;" id="btn-resend">Re-Send OTP</button></center>
                </div>
            </div>

            <!--/.Content-->
        </div>
    </div>
</div>

<!-- Delete Button triggerd modal -->

<div class="modal fade" id="deleteModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-notify modal-danger modal-dialog-centered" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header text-center">
                <h4 class="modal-title white-text w-100 font-weight-bold py-2">Remove Record</h4>
            </div>

            <!--Body-->
            <form action="" method="POST" class="remove-record-model">
                <div class="modal-body">
                    <span class="adminpro-icon adminpro-warning-danger modal-check-pro information-icon-pro"></span>
                    <h2>Are you sure want to delete?</h2>
                    <p class="text-center">You will not be able to recover it back!</p>
                </div>

                <div class="modal-footer">
                    <a type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                    <a  type="button" href="javascript:void(0)" class="modal-btn-submit btn btn-danger btn-sm">Proceed</a>
                </div>
            </form>

            <!--/.Content-->
        </div>
    </div>
</div>

<div class="top-header">
    <div class="container cp-xs">
        <div class="row ">
            <div class="col-md-6 col-12 ">
                <div class="d-flex justify-content-center">
                @if(!Session::has('user_id')) <a href="{{url('register/patient')}}" class="text-light">Patients/Customer ? <i class="fas fa-sign-in-alt text-primary"></i> Register</a>
                @endif
                </div>
            </div>

            <div class="col-md-6 col-12">
                <div class="d-flex justify-content-center">
                    @guest
                        <div class="">Health Service Provider/ Doctor ? <a href="{{url('register/provider')}}" class="text-light"><i class="fas fa-sign-in-alt text-primary"></i> {{ __('Join Us') }}</a>
                        </div>

                        {{--                            @if (Route::has('register'))--}}
                        {{--                            <div class="pl-4 font-sm"> <a class="text-light" href="{{ url('register') }}"><i class="fa fa-user text-primary"></i> {{ __('Register') }}</a>--}}
                        {{--			</div>--}}
                        {{--                            @endif--}}
                    @else
                        @if(Auth::user()->user_role_id == 3)
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{url('patient/dashboard')}}"><i class="fas fa-home"></i> &nbsp;Dashboard</a>
                                        <a class="dropdown-item" href="{{url('patient/update-profile')}}"><i class="fas fa-user-md"></i> &nbsp;Update Profile</a>

{{--                                        <a class="dropdown-item" href="{{url('patient/enquiries')}}"><i class="fas fa-envelope"></i> Enquiries</a>--}}
                                        <a class="dropdown-item" href="{{url('patient/medical-data')}}"><i class="fas fa-file-prescription"></i> &nbsp;Medical History</a>
    {{--                                    <a class="dropdown-item" href="{{url('patient/appoinment-booking')}}"><i class="fas fa-file-medical"></i> &nbsp;Appointment Bookings</a>--}}
                                        <a class="dropdown-item" href="{{url('patient/consultation-booking')}}"><i class="fas fa-file-video"></i> &nbsp;Live Consultation</a>
                                        <a class="dropdown-item" href="{{url('patient/ambulance-bookings')}}"><i class="fas fa-ambulance"></i> Ambulance Bookings</a>
                                        <a class="dropdown-item" href="{{ url('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <i class="fas fa-sign-out-alt"></i> &nbsp;  {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            </ul>

                        @else
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle text-light" href="#"
                                       role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{url('/dashboard')}}"><i class="fas fa-home"></i> &nbsp;Dashboard</a>
                                        <a class="dropdown-item" href="{{url('/update-profile')}}"><i class="fas fa-user-md"></i> &nbsp;Update Profile</a>
                                        <a class="dropdown-item" href="{{url('/services')}}"><i class="fas fa-layer-group"></i> &nbsp;Update Services</a>
                                        <a class="dropdown-item" href="{{url('event/add')}}"><i class="fas fa-calendar-plus-o"></i>&nbsp;Add Schedule</a>
                                        <a class="dropdown-item" href="{{url('event/manage')}}"><i class="fas fa-calendar-check-o" aria-hidden="true"></i>&nbsp;Manage Schedule</a>
    {{--                                    <a class="dropdown-item" href="{{url('event')}}"><i class="fas fa-calendar-alt"></i> View Calendar</a>--}}
                                        <a class="dropdown-item" href="{{url('/booking/get-bookings')}}"><i class="fas fa-notes-medical"></i> Appointment Bookings</a>
                                        <a class="dropdown-item" href="{{url('/live-consultation')}}"><i class="fas fa-video"></i> Live Consultation</a>
{{--                                        <a class="dropdown-item" href="{{url('/enquiries')}}"><i class="fas fa-envelope"></i> Enquiries</a>--}}
                                        @if(Auth::user()->service_profile->where('user_role_id',6)->count())
                                            <a class="dropdown-item" href="{{url('ambulance-bookings')}}"><i class="fas fa-ambulance"></i> Ambulance Bookings</a>
                                        @endif
                                        <a class="dropdown-item" href="{{ url('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <i class="fas fa-sign-out-alt"></i> &nbsp;  {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            </ul>

                        @endif

                    @endguest

                </div>
            </div>
        </div>
        {{-- <div class="d-flex justify-content-end">
            <div class="pl-4 font-sm"><a href="{{url('registration')}}" class="text-dark"><i class="fa fa-user text-primary"></i> Register</a>
            </div>
        <div class="pl-4 font-sm"><a href="{{url('userLogin')}}" class="text-dark"><i class="fa fa-sign-in-alt text-primary"></i> Login</a>
            </div>
            <div class="pl-4 font-sm"><a href="" class="text-dark"><i class="fa fa-home text-primary"></i> Dashboard</a>
            </div>
        </div> --}}
    </div>
</div>
<div class="main-header bg-blue-gradiant">
    <div class="container cp-xs">
    <div class="row">
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6 d-flex align-self-center">
            <a href="{{url('/')}}" class="align-self-center">
                <img src="{{asset('web/v2/img/logo/logo.svg')}}" class="img-fluid" width="200px"></a>
        </div>
        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 align-self-center d-mob justify-content-end">
            <div class="d-flex justify-content-end">
                @guest
                    <a href="{{url('login')}}" class="nav-link login-btn">Login
                    </a>

                @endguest
                <a href="{{url('/search')}}" type="button" class="btn-src text-muted align-self-center" id="btn-src">
                    <i class="fa fa-search fa-lg"></i></a>
                <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars fa-lg text-black-50"></i>
                </button>
            </div>
        </div>
        <div class="col-xl-7 col-md-7 col-12 col-sm-12 align-self-center text-right">
            <nav class="navbar navbar-expand-lg navbar-light shadow-none">
                <div class="container">
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item"><a class="nav-link" href="{{url('/')}}">Home</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="{{url('aboutus')}}">About Us</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Our Services</a>
                                <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{url('lab-services')}}">Diagnostic &amp; Lab Services</a>
                                    <a class="dropdown-item" href="{{url('medical-product')}}">Medicine &amp; Medical Products</a>
                                    <a class="dropdown-item" href="{{url('checkup-package')}}">Checkup Packages</a>
                                    <a class="dropdown-item" href="{{url('immunization-services')}}">Immunization Services</a>
                                    <a class="dropdown-item" href="{{url('doctor-appointment')}}">Doctor Appointment</a>
                                </div>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="{{url('gallery')}}">Gallery</a>
                            </li>
                            <!--<li class="nav-item"><a class="nav-link" href="#">Blood Bank</a>
                            </li>-->
                            <li class="nav-item"><a class="nav-link" href="{{url('contactus')}}">Contact Us</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 align-self-center h-mob p-0">

            <div class="row">
                <div class="col-12 d-flex justify-content-end">
                    <div class="d-flex">
                        @if(!Session::has('user_id'))
                            <a href="{{url('login')}}" class="nav-link login-btn">Login
                            </a>

                        @endif
                        <a href="{{url('/search')}}" type="button" class="btn-src text-muted align-self-center search-btn" id="btn-src">
                            <i class="fa fa-search fa-lg"></i> Search</a>
                        {{--                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"--}}
                        {{--                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">--}}
                        {{--                        <i class="fa fa-bars fa-lg text-black-50"></i>--}}
                        {{--                    </button>--}}

                    </div>
                </div>
            </div>
        </div>
        {{--        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 align-self-center h-mob">--}}
        {{--            <div class="row">--}}
        {{--                <div class="col-12 text-right">--}}
        {{--                            <a href="https://play.google.com/store/apps/details?id=com.app.omeddr&hl=en" target="_blank">--}}
        {{--                            <img src="{{asset("web/img/google-plus.png")}}" class="g-plus-icon"></a>--}}

        {{--            </div>--}}
        {{--        </div>--}}
        {{--    </div>--}}
    </div>
</div>
</div>
<!--<div class="hr-line"></div>-->
<!-- Contect section start's here -->
@yield('content')

<!-- Content section ends here -->

<div class="container-fluid footer-bg cp-md">
    <div class="container">
        <div class="row white-text">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">
                <h5 class="text-uppercase">About</h5>
                <div class="divide"></div>
                <div>
                    <a href="{{url('contactus')}}" class="d-block white-text mb-3">Contact us</a>
                    <a href="{{route('page','support')}}" class="d-block white-text mb-3">Support</a>
                    <a href="{{route('page','career')}}" class="d-block white-text mb-3">Career</a>
                    <a href="{{route('page','faq')}}" class="d-block white-text mb-3">FAQs</a>
                    {{--                    <a href="{{url('register')}}" class="d-block white-text mb-3">Register</a>--}}
                    {{--                    <a href="{{url('login')}}" class="d-block white-text">Login</a>--}}
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">
                <h5 class="text-uppercase">Resources</h5>
                <div class="divide"></div>
                <div>
                    {{--                    @php $category = Category::where('status',1)->get() @endphp--}}
                    <a href="{{url('search?search_for=1')}}"
                       class="d-block white-text mb-3 ">Search Doctor</a>
                    <a href="{{url('search?search_for=2')}}" class="d-block white-text mb-3"
                    >Search Hospital</a>
                    <a href="{{url('search?search_for=4')}}"
                       class="d-block white-text mb-3">Search Diagnostic</a>
                    <a href="{{url('search?search_for=5')}}"
                       class="d-block white-text mb-3 ">Search Medicine</a>
                    <a href="{{url('search?search_for=6')}}"
                       class="d-block white-text mb-3">Search Ambulance</a>
                    <a href="{{url('search?search_for=9')}}" class="d-block white-text">Search Health Related Service</a>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 mb-4">
                <h5 class="text-uppercase">More</h5>
                <div class="divide"></div>
                <div>
                    <a href="{{route('page','partner-with-us')}}" class="d-block white-text mb-3">Partner With Us</a>
                    <a href="{{route('page','terms-services')}}" class="d-block white-text mb-3">Terms &amp; Services</a>
                    <a href="{{route('page','franchise')}}" class="d-block white-text mb-3">Franchise</a>
                    <a href="{{url('privacy-policy')}}" class="d-block white-text mb-3">Privacy Policy</a>
                    <a href="{{url('our-products')}}" class="d-block white-text mb-3">Our Products</a>
                    <a href="{{url('guideline')}}" class="d-block white-text mb-3">Guidelines on Online Consultation</a>
                    {{--                    <a href="#" class="d-block white-text">Subscribe</a>--}}
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                <h5 class="text-uppercase">Contact Information</h5>
                <div class="divide"></div>
                <div class="text-uppercase">Address</div>
                <div class="mb-3">AAH Digital Health Center AIIMS Road, Naya Tola Phulwarisharif, Patna</div>
                <div class="text-uppercase">Phone</div>
                <div class="mb-3"> +91 9471002110</div>
                <div class="text-uppercase">Email</div>
                <div class="mb-3">aalihealthcare@gmail.com</div>
                <div class="text-uppercase">Follow Us</div>
                <div class="divide"></div>
                <div>
                    <a href="https://facebook.com/aalihealthcare/?ref=m_notif&notif_t=page_user_activity" class="d-inline-block text-white mr-3"><i class="fab fa-facebook-f"></i></a>
                    <a href="https://twitter.com/dramjadalisiwan" class="d-inline-block text-white mr-3"><i class="fab fa-twitter"></i></a>
                    <a href="https://www.instagram.com/aah_omed" class="d-inline-block text-white mr-3"><i class="fab fa-instagram"></i></a>
                    <a href="https://youtube.com/channel/UCO4dwacM_rGVqrax6zC-VAA" class="d-inline-block text-white mr-3"><i class="fab fa-youtube"></i></a>
                    <a href="https://wa.me/+917992394071?text=" class="d-inline-block text-white mr-3"><i class="fab fa-whatsapp"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid black pt-2 pb-2">
    <div class="container">
        <div class="row white-text small no-gutters">
            <div class="col-12 col-md-10 text-left">&copy; {{config('app.name')." ".date('Y')}} . All Right Reserved</div>
            <div class="col-12 col-md-2 text-white-50">Developed By : <span class="text-white">AAHPL</span></div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{asset('web/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/popper.js')}}"></script>
@yield('jquery-ui')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="{{asset('web/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/mdb.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/owl.carousel.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/gallary.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/jquery.viewbox.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/drawer.js')}}"></script>

{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>--}}
<script src="{{asset('web/dist/js/select2.min.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.2.0/iscroll.js"></script>

@if ($message = Session::get('success'))
    <script>
        alertify.set('notifier','position', 'top-right');
        alertify.success('{{ $message }}');
    </script>
@elseif  ($message = Session::get('error'))
    <script>
        alertify.set('notifier','position', 'top-right');
        alertify.error('{{ $message }}');
    </script>
@endif
@include('layouts.custom_script')
<script type="text/javascript">
    $(document).ready(function() {
        $('.drawer').drawer();

    });

    $(document).ready(function() {
        $("#slider").owlCarousel({
            navigation : false,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            autoPlay : true,
            pagination:false,
        });
    });
    $(document).ready(function() {
        $('#example').DataTable();
        $('.alert-danger').fadeToggle(60000);
    });

    $(document).ready(function() {
        var owl = $("#new-registered");
        owl.owlCarousel({
            items : 2,
            itemsDesktop : [1000,2],
            itemsDesktopSmall : [900,2],
            itemsTablet: [600,1],
            itemsMobile : false,
            pagination : false
        });
        // Custom Navigation Events
        $(".next").click(function(){
            owl.trigger('owl.next');
        })
        $(".prev").click(function(){
            owl.trigger('owl.prev');
        })
    });

    $(document).ready(function() {
        var owl = $("#testimonial");
        owl.owlCarousel({
            items : 1,
            itemsDesktop : [1000,1],
            itemsDesktopSmall : [900,1],
            itemsTablet: [600,1],
            itemsMobile : false
        });
    });

    $(document).ready(function() {
        var owl = $("#clients");
        owl.owlCarousel({
            items : 4,
            itemsDesktop : [1000,4],
            itemsDesktopSmall : [900,4],
            itemsTablet: [600,1],
            itemsMobile : false,
            pagination: false,
            autoPlay : 3000
        });
    });
    $(document).ready(function() {
        var owl = $("#clients2");
        owl.owlCarousel({
            items : 5,
            itemsDesktop : [1000,4],
            itemsDesktopSmall : [900,4],
            itemsTablet: [600,1],
            itemsMobile : false,
            pagination: false,
            autoPlay : 3000
        });
    });
    $(document).ready(function() {
        var owl = $("#events");
        owl.owlCarousel({
            items : 3,
            itemsDesktop : [1000,3],
            itemsDesktopSmall : [900,3],
            itemsTablet: [600,1],
            itemsMobile : false,
            pagination: false,
            autoPlay : 3000
        });
    });

    $(function(){
        $('.image-link').viewbox();
    });

    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    $(document).ready(function(){

        $(".select-category").select2({
            placeholder: "Select Categories",
            allowClear: true,
        });
        $(".state-select").select2({
            placeholder: "Select State",
            allowClear: true,
        });
        $(".city-select").select2({
            placeholder: "Select City",
            allowClear: true,
        });
        $(".area-select").select2({
            placeholder: "Select Area",
            allowClear: true,
        });

        $('#state-select').select2({
            placeholder: "Select State",
            allowClear: true,
        });
        $('#city-select').select2({
            placeholder: "Select City",
            allowClear: true,
        });
        $('#area-select').select2({
            placeholder: "Select Area",
            allowClear: true,
        });
        $('#role-select').select2({
            placeholder: "Select one",
            allowClear: true,
        });
        $('#category').select2();
        $('#timings').select2();
    });

    $(document).ready(function(){
        $("#reg-mobile").blur(function () {
            var phone = $("#reg-mobile").val();
            if (phone === "") {
                $("#reg-mobile").addClass('is-invalid');
                return false;
            }
            else{
                $('form.frm-reg').find("input #reg-mobile").removeClass('is-invalid');
            }
            sendOTP(phone);
        });

        // Resend Button Action
        $(".resend_otp").click(function () {
            var phone = $("#reg-mobile").val();
            if (phone == " ") {
                return false;
            }
            else{
                $('form.frm-reg').find("input #reg-mobile").removeClass('is-invalid');
            }
            sendOTP(phone);
        });
    });

    // Send OTP Function
    function sendOTP(phone)
    {
        $("#reg-mobile").removeClass('is-invalid');
        $("#otpModal").modal('show');
        $.ajax({
            type: 'post',
            url: "{{url('send-otp')}}",
            data: {phone: phone,
                _token: "{{ csrf_token() }}", },
            success: function (data) {
                if (data.status == false) {

                    $("#otpModal").modal('hide');
                    alertify.error(data.message);
                }
                if (data.status == true) {

                    $("#otpModal").modal('show');
                    $('#time').show();
                    countdown();
                }
            }
        });
    }

    // Validate OTP
    $(".mob_otp").keyup(function () {
        var mob_otp = $(".mob_otp").val();
        $.ajax({
            type: 'post',
            url: "{{url('check-otp')}}",
            data: {mob_otp: mob_otp,
                _token: "{{ csrf_token() }}", },
            success: function (data) {
                if (data.status == false) {
                    $('.msg_otp').removeClass("text-success");
                    $('.msg_otp').text(data.message).addClass("text-danger");

                }
                if (data.status == true) {
                    $('#time').hide();
                    $('.msg_otp').removeClass("text-danger");
                    $('.msg_otp').text('OTP verified!').addClass("text-success");
                    $("#otpModal").modal('hide');
                    $('#reg-mobile').removeClass('is-invalid');
                }
            }
        });
    });

    // Countdown Timer
    var interval;
    function countdown() {
        clearInterval(interval);
        interval = setInterval( function() {
            var timer = $('#time').html();
            timer = timer.split(':');
            var minutes = timer[0];
            var seconds = timer[1];
            seconds -= 1;
            if (seconds < 10 && length.seconds != 2) seconds = '0' + seconds;
            $('#time').html(minutes + ':' + seconds);
            if (minutes == 0 && seconds == 0) {
                clearInterval(interval);
                $('#time').text("0:30");
                $('#time').hide();
                $('#btn-resend').show();
            }
        }, 1000);
    }

    $(document).ready(function(){
        // $('.select-category').select2();
        $(document).on('click','.delete_button_action', function () {

            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var token = '{{ csrf_token() }}';
            $('#deleteModal').modal('show');
            $(".remove-record-model").attr("action",url);

            $('body').find('.remove-record-model').append('<input name="_token" type="hidden" value="'+ token +'">');
            $('body').find('.remove-record-model').append('<input name="_method" type="hidden" value="DELETE">');
            $('body').find('.remove-record-model').append('<input name="id" type="hidden" value="'+ id +'">');
        });
        $('.remove-data-from-delete-form').click(function() {
            $('body').find('.remove-record-model').find( "input" ).remove();
        });
    });
    $('.modal-btn-submit').click(function(){
        $('.remove-record-model').submit();
    });
    $("#btn-src").click(function(){
        $("#fnmf").slideDown();
    });

    $("#btn-close").click(function(){
        $("#fnmf").slideUp();
    });

    $("#btn-sort").click(function(){
        $("#cat").slideToggle();
    });

    // Change Live Status
    @if(Auth::check() && Auth::user()->user_role_id==0 && Auth::user()->profile()->count() > 0)
    {{--    @if(Auth::user()->profile->live_status===1)--}}

    {{--    if($('#customSwitch1').prop('checked') == false)--}}
    {{--{--}}
    {{--    $('#customSwitch1').attr('checked','true');--}}
    {{--    $('#live-status').removeClass('text-danger').addClass('text-success').html('Online');--}}
    {{--}--}}
    {{--    @else--}}
    {{--    if($('#customSwitch1').prop('checked') == true)--}}
    {{--    {--}}
    {{--        $('#customSwitch1').attr('checked','false');--}}
    {{--        $('#live-status').removeClass('text-success').addClass('text-danger').html('Offline');--}}
    {{--    }--}}
    {{--    @endif--}}

    $('#customSwitch1').change(function(){
        var switchStatus = false;
        if ($(this).is(':checked')) {
            changeFlag('profiles','live_status',{{Auth::user()->profile->id}});
            // $(this).removeClass('is-invalid').addClass('is-valid');

        } else {
            changeFlag('profiles','live_status',{{Auth::user()->profile->id}});
            // $(this).removeClass('is-valid').addClass('is-invalid');

        }
    });
    @endif

    function changeFlag(table,column,id){
        $.ajax({
            type:"GET",
            url:"{{url('provider/change-flag')}}?table="+table+"&column="+column+"&id="+id,
            success:function(res){
                if(res.success)
                {
                    if(res.status=='1'){
                        location.reload(true);
                        // $(document).ready(function(){
                        //     alertify.set('notifier','position', 'top-right');
                        //     alertify.success('You are now Online');
                        // });

                        $('#customSwitch1').attr('checked','true');
                        $('#live-status').removeClass('text-danger').addClass('text-success').html('Online');
                    }
                    if(res.status == '0'){
                        location.reload(true);
                        // $(document).ready(function(){
                        //     alertify.set('notifier','position', 'top-right');
                        //     alertify.success('You are now Offline');
                        // });

                        $('#customSwitch1').attr('checked','false');
                        $('#live-status').removeClass('text-success').addClass('text-danger').html('Offline');
                    }
                }
                else{
                    alertify.set('notifier','position', 'top-right');
                    alertify.error('Something went wrong.');
                }
            }
        });
    }
    $(document).ready(function(){
        $("#btn-filter").click(function(){
            $("#filter-box").toggle();
        });
    });
</script>
@yield('additional_script')
</body>
</html>
