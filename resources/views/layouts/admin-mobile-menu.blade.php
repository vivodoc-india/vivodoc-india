<!-- Mobile Menu start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul class="mobile-menu-nav">
                                        <li><a href="">Dashboard <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>

                                        </li>
                                        <li><a data-toggle="collapse" data-target="#demo" href="#">Category <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                            <ul id="demo" class="collapse dropdown-header-top">
                                             <li> <a href="{{url('omed/category/create')}}">Add Category</a></li>
                                                <li><a href="{{url('omed/category')}}">Manage Category</a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#others" href="#">Users <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                            <ul id="others" class="collapse dropdown-header-top">
                                                <li><a href="{{url('omed/user-management')}}">Manage Users</a>
                                                </li>

                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#Miscellaneousmob" href="#">Patients <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                            <ul id="Miscellaneousmob" class="collapse dropdown-header-top">
                                                <li><a href="{{url('omed/patient-management')}}">Manage Patients</a>
                                                </li>

                                            </ul>
                                        </li>
                                         <li><a data-toggle="collapse" data-target="#Tablesmob" href="#">Bookings <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                            <ul id="Tablesmob" class="collapse dropdown-header-top">
                                                <li><a href="{{url('omed/online-booking-management')}}">Live Consultations</a>
                                                </li> <li><a href="{{url('omed/offline-booking-management')}}">Appointment bookings</a>
                                                </li>
                                                <li><a href="{{url('omed/ambulance-booking-management')}}">Ambulance Bookings</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#Chartsmob" href="#">CMS <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                            <ul id="Chartsmob" class="collapse dropdown-header-top">
                                                <li><a href="{{url('omed/media?type=banner')}}">Banners</a>
                                                </li>
                                                <li><a href="{{url('omed/media?type=app_banner')}}">App Banners</a>
                                                </li>
                                                <li><a href="{{url('omed/media?type=partner')}}">Our Partners</a>
                                                </li>
                                                <li><a href="{{url('omed/media?type=testimonial')}}">Testimonials</a>
                                                </li>
                                                <li><a href="{{url('omed/gallery')}}">Gallery</a></li>
                                                <li><a href="{{url('omed/pages')}}">Pages</a></li>

                                            </ul>
                                        </li>

                                        <li><a data-toggle="collapse" data-target="#formsmob" href="#">Manage Locations <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                            <ul id="formsmob" class="collapse dropdown-header-top">
                                            <li><a href="{{url('omed/state')}}">Manage States</a>
                                                </li>
                                                 <li><a href="{{url('omed/district')}}">Manage City</a>
                                                </li>
                                                 <li><a href="{{url('omed/area')}}">Manage Area</a>
                                                </li>

                                            </ul>
                                        </li>
                                    <li><a data-toggle="collapse" data-target="#Appviewsmob" href="#">Settings <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                            <ul id="Appviewsmob" class="collapse dropdown-header-top">
                                                <li><a href="{{url('omed/manage-settings')}}">Manage Settings</a>
                                                </li>


                                            </ul>
                                        </li>

                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu end -->
            <!-- Breadcome start-->
            <div class="breadcome-area des-none mg-b-30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                 <ul class="breadcome-menu">
                                            <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Dashboard</span>
                                            </li>
                                        </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Breadcome End-->
