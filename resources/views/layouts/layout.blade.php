<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{isset($title)?$title." | ".config('app.name'):config('app.name')}}</title>
    <link href="{{asset('web/v2/img/logo/favicon.ico')}}" rel="icon">
    <link href="{{asset('web/v2/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('web/v2/fonts/boostrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{asset('web/v2/vendor/owl/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('web/v2/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
    <link href="{{asset('web/v2/css/bootstrap-select.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.3.0/mdb.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="{{asset('web/v2/vendor/changetheword/animate.changethewords.css')}}" rel="stylesheet">
    <link href="{{asset('web/v2/css/style.css')}}" rel="stylesheet">
@yield('custom_style')
</head>
<body>
<!-- ======= Header ======= -->
<header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex justify-content-between">

        <div class="d-flex align-items-center">
            <a href="/" class="logo d-flex align-items-center mr-auto my-0">
                <img src="{{asset('web/v2/img/logo/logo.svg')}}" alt="" width="214">
            </a>
        </div>
        <nav id="navbar" class="navbar mr-auto mr-0">
            <ul>
                @guest
                <li>
                    <a class="btn btn-outline-secondary" href="{{route('register_provider')}}">JOIN US</a>
                </li>
                <li class="my-menu">
                    <a href="#" class="btn-read-more">Login/Register</a>
                    <ul class="my-menu-ul">
                        <li>
                            <div id="accordion">
                                <div id="headingOne">
                                    <a class="d-flex justify-content-between" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Patient <i class="fa-solid fa-caret-down"></i>
                                    </a>
                                </div>
                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <ul class="my-menu-ul-ul">
                                        <li><a href="{{url('login')}}">Login</a></li>
                                        <li><a href="{{route('register_patient')}}">Register</a></li>
                                    </ul>
                                </div>
                                <div id="headingTwo">
                                    <a class="d-flex justify-content-between" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                        Provider<i class="fa-solid fa-caret-down"></i>
                                    </a>
                                </div>

                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <ul class="my-menu-ul-ul">
                                        <li><a href="{{url('login')}}">Login</a></li>
                                        <li><a href="{{route('register_provider')}}">Register</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                @else
                    @if(Auth::user()->user_role_id == 3)
                    <div class="dropdown p-3">
                        <a class="dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if(Auth::user()->profile->photo !='')

                                <img src="{{asset('uploads/profile/'.Auth::user()->profile->photo)}}"
                                     class="rounded-circle" style="width: 30px;"
                                     alt="{{auth()->user()->name}}">
                            @else
                                <img src="{{asset('web/img/nobody.jpg')}}"
                                     class="rounded-circle" style="width: 30px;"
                                     alt="{{auth()->user()->name}}">
                        @endif

                        <!--                            <img src="https://i.pravatar.cc/50" class="rounded-circle" style="width: 30px;" alt="Avatar" />-->
                            <span>{{auth()->user()->name}}</span>

                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a href="{{url('/patient/dashboard')}}" class="dropdown-item">
                                <div><i class="bi bi-person-circle"></i> Profile</div>
                            </a>
                            <a class="dropdown-item" href="#!">
                                <div><i class="bi bi-key-fill"></i> Change Password</div>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ url('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <div><i class="fas fa-sign-out-alt"></i> &nbsp;  {{ __('Logout') }}</div>
                            </a>

                            <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            <!--                                                            <a class="dropdown-item" href="#!"> -->
                            <!--                                                                Logout</a>-->
                        </div>
                    </div>
                    @endif
                    @if(Auth::user()->user_role_id == 11)
                            <div class="dropdown p-3">
                                <a class="dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(Auth::user()->profile->photo !='')

                                            <img src="{{asset('uploads/profile/'.Auth::user()->profile->photo)}}"
                                                 class="rounded-circle" style="width: 30px;"
                                                 alt="{{auth()->user()->name}}">
                                    @else
                                    <img src="{{asset('web/img/nobody.jpg')}}"
                                         class="rounded-circle" style="width: 30px;"
                                         alt="{{auth()->user()->name}}">
                                    @endif

                                    <!--                            <img src="https://i.pravatar.cc/50" class="rounded-circle" style="width: 30px;" alt="Avatar" />-->
                                    <span>{{auth()->user()->name}}</span>

                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a href="{{url('/dashboard')}}" class="dropdown-item">
                                        <div><i class="bi bi-person-circle"></i> Profile</div>
                                    </a>
                                    <a class="dropdown-item" href="#!">
                                        <div><i class="bi bi-key-fill"></i> Change Password</div></a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ url('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <div><i class="fas fa-sign-out-alt"></i> &nbsp;  {{ __('Logout') }}</div>
                                    </a>

                                    <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    <!--                                                            <a class="dropdown-item" href="#!"> -->
                                    <!--                                                                Logout</a>-->
                                </div>
                            </div>
                        @endif
                @endguest
            </ul>
            <i class="bi bi-list mobile-nav-toggle text-right"></i>
        </nav>
        <!-- .navbar -->

    </div>
</header>
@yield('content')

<!-- ======= Footer ======= -->
<footer id="footer" class="footer">
    <div id="ContactUs" class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 col-lg-7">
                    <h2>Get the best in health and well-being delivered to your inbox</h2>
                </div>
                <div class="col-12 col-md-5 col-lg-5">
                    <div class="subscribe-form-control">
                        <form >
                            <div  class="form-group" style="margin-bottom: 0px;">
                                <div><i class="bi bi-envelope-fill"></i></div>
                                <input formcontrolname="email" type="text" placeholder="yourmail@mail.com" class="form-control">
                                <button  type="submit" class="btn btn-read-more" style="padding-left: 20px; padding-right: 20px;">Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row gy-4 footer-links">
                <!-- <div class="col-12 col-lg-5 col-md-12 footer-info">
                    <a href="index.html" class="logo d-flex align-items-center">
                        <img src="assets/img/logo/logo.svg" alt="" width="214">
                    </a>
                    <p>VivoDoc is moving healthcare into the modern era with virtual Doctor's Consultation and
                        easy online booking. Certified and Top rated Doctors, Hospitals, Diagnostic Centres,
                        Medical store and Ambulance are available to address immediate medical issues or
                        healthcare needs.</p>
                    <div class="social-links mt-3">
                        <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                        <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                        <a href="#" class="instagram"><i class="bi bi-instagram bx bxl-instagram"></i></a>
                        <a href="#" class="linkedin"><i class="bi bi-linkedin bx bxl-linkedin"></i></a>
                    </div>
                </div> -->

                <div class="col-12 col-lg-3 col-6">
                    <h3>Resources</h3>
                    <ul>

                          <li>  <a href="{{route('page','partner-with-us')}}">Partner With Us</a> </li>
                           <li> <a href="{{route('page','terms-services')}}">Terms &amp; Services</a> </li>
                           <li> <a href="{{route('page','franchise')}}">Franchise</a> </li>
                           <li> <a href="{{url('privacy-policy')}}">Privacy Policy</a> </li>
                           <li> <a href="{{url('our-products')}}">Our Products</a> </li>
                           <li> <a href="{{url('guideline')}}">Guidelines on Online Consultation</a> </li>
                            {{--                    <a href="#" class="d-block white-text">Subscribe</a>--}}

                    </ul>
                </div>

                <div class="col-12 col-lg-3 col-6">
                    <h3>Our Services</h3>
                    <ul>
                      <li>  <a href="{{url('search?search_for=1')}}"
                           class="d-block white-text mb-3 ">Search Doctor</a></li>
                     <li>   <a href="{{url('search?search_for=2')}}"
                        >Search Hospital</a></li>
                     <li>   <a href="{{url('search?search_for=4')}}"
                           >Search Diagnostic</a></li>
                    <li>    <a href="{{url('search?search_for=5')}}"
                           class="d-block white-text mb-3 ">Search Medicine</a></li>
                     <li>   <a href="{{url('search?search_for=6')}}"
                           >Search Ambulance</a></li>
                     <li>   <a href="{{url('search?search_for=9')}}">Search Health Related Service</a></li>
                    </ul>
                </div>
                <div class="col-12 col-lg-3 col-6">
                    <h3>Company</h3>
                    <ul>
                        <li><a href="{{url('contactus')}}" >Contact us</a></li>
                        <li><a href="{{route('page','support')}}" >Support</a></li>
                        <li><a href="{{route('page','career')}}" >Career</a></li>
                        <li><a href="{{route('page','faq')}}" >FAQs</a></li>
                    </ul>
                </div>

            </div>
            <div class="row contact-info">
                <div class="col-md-4">
                    <div class="icon-text-card">
                        <div class="the-icon">
                            <i class="bi bi-envelope-fill"></i>
                        </div>
                        <div class="the-caption">
                            <h4>Email</h4>
                            <p>
                                <a href="mailto:info@vivodoc.com" style="color: white;">info@vivodoc.com</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-text-card">
                        <div class="the-icon">
                            <i class="bi bi-geo-alt-fill"></i>
                        </div>
                        <div class="the-caption">
                            <h4>Address</h4>
                            <p>
                                <a href="#" style="color: white;">AAH Digital Health Center AIIMS Road, Naya Tola Phulwarisharif, Patna</a>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container-fluid bg-gold">
        <div class="row copyright">
            <div class="col-md-3">
                <p>&copy; Copyright <strong><span>VivoDoc India</span></strong>. All Rights Reserved</p>
            </div>
            <div class="col-md-2">
                <a href="#">Terms of Use</a>
            </div>
            <div class="col-md-2">
                <a href="#">Privacy Policy</a>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
        class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="{{asset('web/v2/js/vendors.min.js')}}"></script>
<script src="{{asset('web/v2/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.3.0/mdb.min.js"></script>
<script src="{{asset('web/v2/vendor/owl/owl.carousel.min.js')}}"></script>
<script src="{{asset('web/v2/vendor/glightbox/js/glightbox.min.js')}}"></script>
<script src="{{asset('web/v2/vendor/changetheword/jquery.changethewords.js')}}"></script>
<!-- Template Main JS File -->
<script src="{{asset('web/v2/js/main.js')}}"></script>
<script src="{{asset('web/v2/js/dashboard.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.2.0/iscroll.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(document).ready(function () {
        $('.dropdown-submenu a.test').on("click", function (e) {
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });

</script>
        @if ($message = Session::get('success'))
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: '{{$message}}',
                showConfirmButton: false,
                timer: 1500
            })
        {{--alertify.set('notifier','position', 'top-right');--}}
        {{--alertify.success('{{ $message }}');--}}
</script>
@elseif  ($message = Session::get('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{$message}}',
            showConfirmButton: false,
            timer: 1500
        })
    </script>
@endif
@include('layouts.custom_script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
        $('.alert-danger').fadeToggle(60000);
    });
      $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    $(document).ready(function(){

        $(".select-category").select2({
            placeholder: "Select Categories",
            allowClear: true,
        });
        $(".state-select").select2({
            placeholder: "Select State",
            allowClear: true,
        });
        $(".city-select").select2({
            placeholder: "Select City",
            allowClear: true,
        });
        $(".area-select").select2({
            placeholder: "Select Area",
            allowClear: true,
        });

        $('#state-select').select2({
            placeholder: "Select State",
            allowClear: true,
        });
        $('#city-select').select2({
            placeholder: "Select City",
            allowClear: true,
        });
        $('#area-select').select2({
            placeholder: "Select Area",
            allowClear: true,
        });
        $('#role-select').select2({
            placeholder: "Select one",
            allowClear: true,
        });
        $('#category').select2();
        $('#timings').select2();
    });

    $(document).ready(function(){
        $("#reg-mobile").blur(function () {
            var phone = $("#reg-mobile").val();
            if (phone === "") {
                $("#reg-mobile").addClass('is-invalid');
                return false;
            }
            else{
                $('form.frm-reg').find("input #reg-mobile").removeClass('is-invalid');
            }
            sendOTP(phone);
        });

        // Resend Button Action
        $(".resend_otp").click(function () {
            var phone = $("#reg-mobile").val();
            if (phone == " ") {
                return false;
            }
            else{
                $('form.frm-reg').find("input #reg-mobile").removeClass('is-invalid');
            }
            sendOTP(phone);
        });
    });

    // Send OTP Function
    function sendOTP(phone)
    {
        $("#reg-mobile").removeClass('is-invalid');
        $("#otpModal").modal('show');
        $.ajax({
            type: 'post',
            url: "{{url('send-otp')}}",
            data: {phone: phone,
                _token: "{{ csrf_token() }}", },
            success: function (data) {
                if (data.status == false) {

                    $("#otpModal").modal('hide');
                    alertify.error(data.message);
                }
                if (data.status == true) {

                    $("#otpModal").modal('show');
                    $('#time').show();
                    countdown();
                }
            }
        });
    }

    // Validate OTP
    $(".mob_otp").keyup(function () {
        var mob_otp = $(".mob_otp").val();
        $.ajax({
            type: 'post',
            url: "{{url('check-otp')}}",
            data: {mob_otp: mob_otp,
                _token: "{{ csrf_token() }}", },
            success: function (data) {
                if (data.status == false) {
                    $('.msg_otp').removeClass("text-success");
                    $('.msg_otp').text(data.message).addClass("text-danger");

                }
                if (data.status == true) {
                    $('#time').hide();
                    $('.msg_otp').removeClass("text-danger");
                    $('.msg_otp').text('OTP verified!').addClass("text-success");
                    $("#otpModal").modal('hide');
                    $('#reg-mobile').removeClass('is-invalid');
                }
            }
        });
    });

    // Countdown Timer
    var interval;
    function countdown() {
        clearInterval(interval);
        interval = setInterval( function() {
            var timer = $('#time').html();
            timer = timer.split(':');
            var minutes = timer[0];
            var seconds = timer[1];
            seconds -= 1;
            if (seconds < 10 && length.seconds != 2) seconds = '0' + seconds;
            $('#time').html(minutes + ':' + seconds);
            if (minutes == 0 && seconds == 0) {
                clearInterval(interval);
                $('#time').text("0:30");
                $('#time').hide();
                $('#btn-resend').show();
            }
        }, 1000);
    }

    $(document).ready(function(){
        // $('.select-category').select2();
        $(document).on('click','.delete_button_action', function () {

            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var token = '{{ csrf_token() }}';
            $('#deleteModal').modal('show');
            $(".remove-record-model").attr("action",url);

            $('body').find('.remove-record-model').append('<input name="_token" type="hidden" value="'+ token +'">');
            $('body').find('.remove-record-model').append('<input name="_method" type="hidden" value="DELETE">');
            $('body').find('.remove-record-model').append('<input name="id" type="hidden" value="'+ id +'">');
        });
        $('.remove-data-from-delete-form').click(function() {
            $('body').find('.remove-record-model').find( "input" ).remove();
        });
    });
    $('.modal-btn-submit').click(function(){
        $('.remove-record-model').submit();
    });
    $("#btn-src").click(function(){
        $("#fnmf").slideDown();
    });

    $("#btn-close").click(function(){
        $("#fnmf").slideUp();
    });

    $("#btn-sort").click(function(){
        $("#cat").slideToggle();
    });

    // Change Live Status
    @if(Auth::check() && Auth::user()->user_role_id==0 && Auth::user()->profile()->count() > 0)
    $('#customSwitch1').change(function(){
        var switchStatus = false;
        if ($(this).is(':checked')) {
            changeFlag('profiles','live_status',{{Auth::user()->profile->id}});
            // $(this).removeClass('is-invalid').addClass('is-valid');

        } else {
            changeFlag('profiles','live_status',{{Auth::user()->profile->id}});
            // $(this).removeClass('is-valid').addClass('is-invalid');

        }
    });
    @endif

    function changeFlag(table,column,id){
        $.ajax({
            type:"GET",
            url:"{{url('provider/change-flag')}}?table="+table+"&column="+column+"&id="+id,
            success:function(res){
                if(res.success)
                {
                    if(res.status=='1'){
                        location.reload(true);
                        // $(document).ready(function(){
                        //     alertify.set('notifier','position', 'top-right');
                        //     alertify.success('You are now Online');
                        // });

                        $('#customSwitch1').attr('checked','true');
                        $('#live-status').removeClass('text-danger').addClass('text-success').html('Online');
                    }
                    if(res.status == '0'){
                        location.reload(true);
                        // $(document).ready(function(){
                        //     alertify.set('notifier','position', 'top-right');
                        //     alertify.success('You are now Offline');
                        // });

                        $('#customSwitch1').attr('checked','false');
                        $('#live-status').removeClass('text-success').addClass('text-danger').html('Offline');
                    }
                }
                else{
                    alertify.set('notifier','position', 'top-right');
                    alertify.error('Something went wrong.');
                }
            }
        });
    }
    $(document).ready(function(){
        $("#btn-filter").click(function(){
            $("#filter-box").toggle();
        });

        $('#sidebarButton').on('click', function () {
            $('.left-side-menu').toggleClass('active');
        });
        $(".left-side-menu").on('click',function(){
            $(this).removeClass('active');
        });

    });


</script>

@yield('additional_script')
</body>
</html>
