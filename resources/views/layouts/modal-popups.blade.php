 <div id="WarningModalhdbgcl" class="modal modal-adminpro-general Customwidth-popup-WarningModal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header header-color-modal bg-color-3">
                                            <h4 class="modal-title">Remove Record</h4>
                                            <div class="modal-close-area modal-close-df">
                                                <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                            </div>
                                        </div>
                                        <form method="post" id="remove-record-model">
                                            @method('DELETE')
                                            @csrf
                                            <input type="hidden" name="id" value="">
                                        </form>

                                        <div class="modal-body">
                                            <span class="adminpro-icon adminpro-warning-danger modal-check-pro information-icon-pro"></span>
                                             <h2 id="head2">Are you sure want to delete?</h2>
                                           <p>You will not be able to recover it back!</p>
                                        </div>

                                        <div class="modal-footer">
                                            <a data-dismiss="modal" href="#">Cancel</a>
                                            <a href="javascript:void(0)" class="modal-btn-submit">Process</a>
                                        </div>

                                    </div>

                                </div>
                            </div>
