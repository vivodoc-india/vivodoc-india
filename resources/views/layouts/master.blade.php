<!doctype html>
<html class="no-js" lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{$title}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
  <link rel="icon" type="image/png" href="{{asset('web/img/favicon.png')}}">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <!-- adminpro icon CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/adminpro-custon-icon.css')}}">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/meanmenu.min.css')}}">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/jquery.mCustomScrollbar.min.css')}}">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <!-- jvectormap CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/jvectormap/jquery-jvectormap-2.0.3.css')}}">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/data-table/bootstrap-table.css')}}">
    <link rel="stylesheet" href="{{asset('css/data-table/bootstrap-editable.css')}}">
      <!-- buttons CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/buttons.css')}}">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <!-- modals CSS
============================================ -->
<link rel="stylesheet" href="{{asset('css/modals.css')}}">
 <!-- notifications CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/Lobibox.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/notifications.css')}}">
    <!-- charts CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/c3.min.css')}}">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- select2 CSS
      ============================================ -->
    <link rel="stylesheet" href="{{asset('css/select2/select2.min.css')}}">
    <!-- chosen CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/chosen/bootstrap-chosen.css')}}">
    <!-- forms CSS
============================================ -->
<link rel="stylesheet" href="{{asset('css/form/all-type-forms.css')}}">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <!-- modernizr JS
		============================================ -->
    <script src="{{asset('js/vendor/modernizr-2.8.3.min.js')}}"></script>
</head>
<body class="materialdesign">
    @include('layouts.modal-popups')
<div class="wrapper-pro">
        <div class="left-sidebar-pro">
            <nav id="sidebar">
                <div class="sidebar-header">
                <a href="#"><img src="{{asset('img/omed_logo_circle.png')}}" alt="" class="img-fluid"/>
                    </a>
                    <h3>{{ucfirst(Session::get('admin_name'))}}</h3>
                    {{-- <p>Developer</p> --}}
                    <small>OMEDDr</small>
                </div>
                <div class="left-custom-menu-adp-wrap">
                    <ul class="nav navbar-nav left-sidebar-menu-pro">
                        <li class="nav-item">
                             <a href="" class="nav-link"><i class="fa big-icon fa-home"></i> <span class="mini-dn">Dashboard</span></a>

                        </li>
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa fa-sitemap" aria-hidden="true"></i> <span class="mini-dn">Category</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                <a href="{{url('omed/category/create')}}" class="dropdown-item">Add Category</a>
                                <a href="{{url('omed/category')}}" class="dropdown-item">Manage Categories</a>

                            </div>
                        </li>

                        <li class="nav-item">
                        <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                        <i class="fa fa-hospital-o" aria-hidden="true"></i> <span class="mini-dn">Users</span> <span class="indicator-right-menu mini-dn">
                        <i class="fa indicator-mn fa-angle-left"></i></span>
                        </a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                <a href="{{url('omed/user-management')}}" class="dropdown-item">Manage Users</a>
                            </div>
                        </li>

                        <li class="nav-item">
                        <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                        <i class="fa fa-users" aria-hidden="true"></i> <span class="mini-dn">Patients</span> <span class="indicator-right-menu mini-dn">
                        <i class="fa indicator-mn fa-angle-left"></i></span>
                        </a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                            <a href="{{url('omed/patient-management')}}" class="dropdown-item">Manage Patients</a>
                            </div>
                        </li>

                        <li class="nav-item">
     <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
         <i class="fa fa-book" aria-hidden="true"></i>
         <span class="mini-dn">Bookings</span> <span class="indicator-right-menu mini-dn">
                        <i class="fa indicator-mn fa-angle-left"></i></span>
                            </a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
   <a href="{{url('omed/offline-booking-management')}}" class="dropdown-item">Appointment Bookings</a>
                                <a href="{{url('omed/online-booking-management')}}" class="dropdown-item">Live Consultations</a>
                                <a href="{{url('omed/ambulance-booking-management')}}" class="dropdown-item">
                                    Ambulance Bookings</a>
                            </div>

                        </li>

                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa fa-file-text-o" aria-hidden="true"></i> <span class="mini-dn">CMS</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown chart-left-menu-std animated flipInX">

{{--                       <a href="{{url('omed/content?type=content')}}" class="dropdown-item">Contents</a>--}}
                            <a href="{{url('omed/media?type=banner')}}" class="dropdown-item">Banner</a>
                            <a href="{{url('omed/media?type=app_banner')}}" class="dropdown-item">App Banner</a>
                                <a href="{{url('omed/media?type=partner')}}" class="dropdown-item">Our Partners</a>
                                <a href="{{url('omed/media?type=testimonial')}}" class="dropdown-item">Testimonials</a>
{{--                            <a href="{{url('omed/media?type=image')}}" class="dropdown-item">Images</a>--}}
                                <a href="{{url('omed/gallery')}}" class="dropdown-item">Gallery</a>
                                <a href="{{url('omed/pages')}}" class="dropdown-item">Pages</a>

                            </div>
                        </li>
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                                                class="nav-link dropdown-toggle"><i class="fa fa-location-arrow"
                                                                                    aria-hidden="true"></i> <span
                                    class="mini-dn">Manage Locations</span> <span class="indicator-right-menu
                                    mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown chart-left-menu-std animated flipInX">

                                {{--                       <a href="{{url('omed/content?type=content')}}" class="dropdown-item">Contents</a>--}}
                                {{--                            <a href="{{url('omed/media?type=banner')}}" class="dropdown-item">Banner</a>--}}
                                <a href="{{url('omed/state')}}" class="dropdown-item">State</a>
                                <a href="{{url('omed/district')}}" class="dropdown-item">District</a>
                                <a href="{{url('omed/area')}}" class="dropdown-item">Area</a>

                            </div>
                        </li>
{{--                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa fa-envelope" aria-hidden="true"></i> <span class="mini-dn">Enquiry</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>--}}
{{--                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">--}}
{{--                            <a href="{{url('omed/enquiry-management')}}" class="dropdown-item">Manage Enquiries</a>--}}
{{--                            </div>--}}
{{--                        </li>--}}
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button"
                                                aria-expanded="false" class="nav-link dropdown-toggle">
                                <i class="fa fa-cog" aria-hidden="true"></i> <span class="mini-dn">Settings</span>
                                <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i>
                                </span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                <a href="{{url('omed/manage-settings')}}" class="dropdown-item">Manage Settings</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- Header top area start-->
        <div class="content-inner-all">
            <div class="header-top-area">
                <div class="fixed-header-top">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-1 col-md-6 col-sm-6 col-xs-12">
                                <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <div class="admin-logo logo-wrap-pro">
                                    <a href="#"><img src="{{asset('web/img/favicon.png')}}" alt="" />
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-1 col-sm-1 col-xs-12">

                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                                <div class="header-right-info">
                                    <ul class="nav navbar-nav mai-top-nav header-right-menu">

                                        <li class="nav-item dropdown">
                                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                <span class="adminpro-icon adminpro-user-rounded header-riht-inf"></span>
                                                <span class="admin-name">{{ucfirst(Session::get('admin_name'))}}</span>
                                                <span class="author-project-icon adminpro-icon adminpro-down-arrow"></span>
                                            </a>
                                            <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated flipInX">
{{--                                                <li><a href="{{url('change-password')}}"><span class="adminpro-icon adminpro-home-admin author-log-ic"></span>Change Password</a>--}}
{{--                                                </li>--}}

                                                <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="adminpro-icon adminpro-locked author-log-ic"></span>Log Out</a>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header top area end-->
            @yield('breadcumb')
            <!-- Breadcome End-->
@include('layouts.admin-mobile-menu')
            @yield('content')

</div>
<!-- Footer Start-->
    <div class="footer-copyright-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-copy-right">
                        <p>{{date('Y')}} &copy; OMEDDr.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End-->
</div>
    <!-- jquery
		============================================ -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
{{--    <script src="{{asset('js/vendor/jquery-1.11.3.min.js')}}"></script>--}}
    <!-- bootstrap JS
		============================================ -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="{{asset('js/jquery.meanmenu.js')}}"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <!-- sticky JS
		============================================ -->
    <script src="{{asset('js/jquery.sticky.js')}}"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="{{asset('js/jquery.scrollUp.min.js')}}"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="{{asset('js/wow/wow.min.js')}}"></script>
    <!-- counterup JS
		============================================ -->
    <script src="{{asset('js/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('js/counterup/waypoints.min.js')}}"></script>
    <script src="{{asset('js/counterup/counterup-active.js')}}"></script>
    <!-- jvectormap JS
		============================================ -->
    <script src="{{asset('js/jvectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{asset('js/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('js/jvectormap/jvectormap-active.js')}}"></script>
    <!-- peity JS
		============================================ -->
    <script src="{{asset('js/peity/jquery.peity.min.js')}}"></script>
    <script src="{{asset('js/peity/peity-active.js')}}"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="{{asset('js/sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('js/sparkline/sparkline-active.js')}}"></script>
    <!-- flot JS
		============================================ -->
    <script src="{{asset('js/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('js/flot/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{asset('js/flot/jquery.flot.spline.js')}}"></script>
    <script src="{{asset('js/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('js/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('js/flot/jquery.flot.symbol.js')}}"></script>
    <script src="{{asset('js/flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('js/flot/dashtwo-flot-active.js')}}"></script>
    <!-- data table JS
		============================================ -->
    <script src="{{asset('js/data-table/bootstrap-table.js')}}"></script>
    <script src="{{asset('js/data-table/tableExport.js')}}"></script>
    <script src="{{asset('js/data-table/data-table-active.js')}}"></script>
    <script src="{{asset('js/data-table/bootstrap-table-editable.js')}}"></script>
    <script src="{{asset('js/data-table/bootstrap-editable.js')}}"></script>
    <script src="{{asset('js/data-table/bootstrap-table-resizable.js')}}"></script>
    <script src="{{asset('js/data-table/colResizable-1.5.source.js')}}"></script>
    <script src="{{asset('js/data-table/bootstrap-table-export.js')}}"></script>
    <!-- modal JS
============================================ -->
<script src="{{asset('js/modal-active.js')}}"></script>
  <!-- select2 JS
		============================================ -->
    <script src="{{asset('js/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('js/select2/select2-active.js')}}"></script>
    <!-- icheck JS
============================================ -->
<script src="{{asset('js/icheck/icheck.min.js')}}"></script>
<script src="{{asset('js/icheck/icheck-active.js')}}"></script>
    <!-- main JS
		============================================ -->
    <script src="{{asset('js/main.js')}}"></script>
    <!-- notification JS
		============================================ -->
    <script src="{{asset('js/Lobibox.js')}}"></script>
    <script src="{{asset('js/notification-active.js')}}"></script>
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>--}}
   @yield('additional_script')
    @if ($message = Session::get('success'))
<script>
    Lobibox.notify('success', {
    delay: 15000,
    sound:true,
    msg: '{{ $message }}'
  });

</script>
@endif
@if ($message = Session::get('error'))
<script>
    Lobibox.notify('error', {
    delay: 15000,
    sound:true,
    msg: '{{ $message }}'
  });

</script>
@endif
@if ($message = Session::get('info'))
<script>
    Lobibox.notify('info', {
    delay: 15000,
    sound:true,
    msg: '{{ $message }}'
  });

</script>
@endif
    <script>
        $(document).ready(function(){
            var id;
            var url;
            $('.delete_button_action').on('click', function () {
         id = $(this).attr('data-id');
        url = $(this).attr('data-url');
                $('#remove-record-model').attr('action', url);
        $('#remove-record-model').find('input[name="id"]').val(id);
    });
    // $('.remove-data-from-delete-form').click(function() {
    //     $('body').find('#WarningModalhdbgcl').find('#remove-record-model').find( "input" ).remove();
    // });

            $('.modal-btn-submit').click(function(){
                $('#remove-record-model').submit();
            });
});

        function changeFlag(data,table,column,id){
    var that=$(data);
    $.ajax({
        type:"GET",
        url:"{{url('ajax/change-flag')}}?table="+table+"&column="+column+"&id="+id,
        success:function(res){
            if(res.status=='1'){
                that.text('Active');
                that.removeClass('btn-danger');
                that.addClass('btn-success');
                $.notify("Status Changed Successfully",'success');
            }
            else if(res.status == '2')
            {
                $.notify(res.message,'error');
            }
            else{
                that.text('Idle');
                that.removeClass('btn-success');
                that.addClass('btn-danger');
                $.notify("Status Changed Successfully",'success');
            }
        }
    });
}

function changeFlag2(data,table,column,id){
    var that=$(data);
    $.ajax({
        type:"GET",
        url:"{{url('ajax/change-flag')}}?table="+table+"&column="+column+"&id="+id,
        success:function(res){
            if(res.status=='1'){
                that.html('<i class="fa fa-check fa-lg" aria-hidden="true"></i>');
                that.removeClass('danger');
                that.addClass('green');
  Lobibox.notify('success', {
    delay: 15000,
    sound:true,
    msg: 'Status Changed Successfully'
  });

            }
            else{
                that.html('<i class="fa fa-times fa-lg" aria-hidden="true"></i>');
                that.removeClass('green');
                that.addClass('danger');
                that.attr('title','Not Approved');
                  Lobibox.notify('success', {
    delay: 15000,
    sound:true,
    msg: 'Status Changed Successfully'
  });

            }
        }
    });
}
        function changeFlag3(data,table,column,id){
            var that=$(data);
            $.ajax({
                type:"GET",
                url:"{{url('ajax/change-flag')}}?table="+table+"&column="+column+"&id="+id,
                success:function(res){
                    if(res.status=='1'){
                        that.text('Approved');
                        that.removeClass('bg-danger');
                        that.addClass('green');
                        Lobibox.notify('success', {
                            delay: 15000,
                            sound:true,
                            msg: 'Status Changed Successfully'
                        });

                    }
                    else{
                        that.text('Not Approved');
                        that.removeClass('green');
                        that.addClass('bg-danger');

                        Lobibox.notify('success', {
                            delay: 15000,
                            sound:true,
                            msg: 'Status Changed Successfully'
                        });

                    }
                }
            });
        }
</script>
</body>
</html>
