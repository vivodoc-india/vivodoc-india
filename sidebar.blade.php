<style>
  .sub_masters_menus{
    font-size: 17px;
    line-height: 20px;
    width: 26px; margin-left: 15px; text-align: center;
    color: #1aa5b8;float: left;
  }
  .sub_masters_menus_catby{
    margin-left: 21px;
  }
</style>
<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="{{ route('home') }}" class="simple-text logo-normal">
      <img src="/material/img/fitcru.png" style="width:200px;">
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
          <p>{{ __('Dashboard') }}</p>
        </a>
      </li>



      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#laravelExample" aria-expanded="true">
          <i class="material-icons">assessment</i>
          <p>{{ __('Masters') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="laravelExample"> <!-- sub menu collapse -->
          <ul class="nav"><!--ul nav Admin Related -->
            <li class="nav-item{{ $activePage == 'cardioexercise' ? ' active' : '' }}">
              <a class="nav-link" data-toggle="collapse" href="#laravelExample2" aria-expanded="true">
              <span class="material-icons sub_masters_menus">admin_panel_settings</span>
                  <span class="sidebar-normal">{{ __('Admin Related ') }} <b class="caret"></b></span>
              </a>
              <div class="collapse sub_masters_menus_catby"  id="laravelExample2">
                <ul class="nav">
                  <li class="nav-item{{ $activePage == 'userroles' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/userroles">
                      <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('User Roles') }} </span>
                    </a>
                  </li>                 
                  <li class="nav-item{{ $activePage == 'coach_category' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/coach_category">
                      <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('Coach Category') }} </span>
                    </a>
                  </li>                  
                  <li class="nav-item{{ $activePage == 'certificates' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/certificates">
                      <span class="material-icons sub_masters_menus">chevron_right</span>
                       <span class="sidebar-normal">{{ __('Certificates') }} </span>
                    </a>
                  </li>
                  <li class="nav-item{{ $activePage == 'tags' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/tags">
                      <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('Tags') }} </span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
          </ul><!--// ul nav Admin Related -->

          <ul class="nav"><!--ul nav Clients Specific -->
            <li class="nav-item{{ $activePage == 'cardioexercise' ? ' active' : '' }}">
              <a class="nav-link" data-toggle="collapse" href="#laravelExample3" aria-expanded="true">
              <span class="material-icons sub_masters_menus">settings_input_composite</span>
                  <span class="sidebar-normal">{{ __('Clients Specific ') }} <b class="caret"></b></span>
              </a>
              <div class="collapse sub_masters_menus_catby" id="laravelExample3">
                <ul class="nav">
                  <li class="nav-item{{ $activePage == 'alergies' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/alergies">
                      <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('Allergies') }} </span>
                    </a>
                  </li>                 
                  <li class="nav-item{{ $activePage == 'cardioexercise' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/cardioexercise">
                      <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('Cardio Exercise') }} </span>
                    </a>
                  </li>                  
                  <li class="nav-item{{ $activePage == 'health_issues' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/health_issues">
                      <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('Health Issues') }} </span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>   
          </ul><!--// ul nav Clients Specific -->
          
          <ul class="nav"><!--ul nav Exercise -->
            <li class="nav-item{{ $activePage == 'cardioexercise' ? ' active' : '' }}">
              <a class="nav-link" data-toggle="collapse" href="#laravelExample4" aria-expanded="true">
              <span class="material-icons sub_masters_menus">snowboarding</span>
                  <span class="sidebar-normal">{{ __('Exercise ') }} <b class="caret"></b></span>
              </a>
              <div class="collapse sub_masters_menus_catby" id="laravelExample4">
                <ul class="nav">
                <li class="nav-item{{ $activePage == 'habit_category' ? ' active' : '' }}">
                  <a class="nav-link" href="/masters/list/habit_category">
                    <span class="material-icons sub_masters_menus">chevron_right</span>
                    <span class="sidebar-normal">{{ __('Habit Category') }} </span>
                  </a>
                </li>                  
                <li class="nav-item{{ $activePage == 'excersizetype' ? ' active' : '' }}">
                  <a class="nav-link" href="/masters/list/excersizetype">
                    <span class="material-icons sub_masters_menus">chevron_right</span>
                    <span class="sidebar-normal">{{ __('Excersize Type') }} </span>
                  </a>
                </li>     
                <li class="nav-item{{ $activePage == 'equipments' ? ' active' : '' }}">
                  <a class="nav-link" href="/masters/list/equipments">
                    <span class="material-icons sub_masters_menus">chevron_right</span>
                    <span class="sidebar-normal">{{ __('Equipments') }} </span>
                  </a>
                </li>
                <li class="nav-item{{ $activePage == 'studiostyle' ? ' active' : '' }}">
                  <a class="nav-link" href="/masters/list/studiostyle">
                    <span class="material-icons sub_masters_menus">chevron_right</span>
                    <span class="sidebar-normal">{{ __('Studio Style') }} </span>
                  </a>
                </li>
                </ul>
              </div>
            </li>
          </ul><!--// ul nav Exercise -->
          
          <ul class="nav"><!--ul nav Nutrition -->
            <li class="nav-item{{ $activePage == 'cardioexercise' ? ' active' : '' }}">
              <a class="nav-link" data-toggle="collapse" href="#laravelExample5" aria-expanded="true">
              <span class="material-icons sub_masters_menus">dinner_dining</span>
                  <span class="sidebar-normal">{{ __('Nutrition ') }} <b class="caret"></b></span>
              </a>
              <div class="collapse sub_masters_menus_catby" id="laravelExample5">
                <ul class="nav">
                  <li class="nav-item{{ $activePage == 'dietpreference' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/dietpreference">
                      <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('Diet Preference') }} </span>
                    </a>
                  </li>                    
                  <li class="nav-item{{ $activePage == 'foodtype' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/foodtype">
                      <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('Food Type') }} </span>
                    </a>
                  </li>                    
                  <li class="nav-item{{ $activePage == 'mealtype' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/mealtype">
                      <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('Meal Type') }} </span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
          </ul><!--// ul nav Nutrition -->
          
          <ul class="nav"><!--ul nav Questionnare-->
            <li class="nav-item{{ $activePage == 'cardioexercise' ? ' active' : '' }}">
              <a class="nav-link" data-toggle="collapse" href="#laravelExample6" aria-expanded="true">
              <span class="material-icons sub_masters_menus">swipe_vertical</span>
                  <span class="sidebar-normal">{{ __('Questionnare') }} <b class="caret"></b></span>
              </a>
              <div class="collapse sub_masters_menus_catby" id="laravelExample6">
                <ul class="nav">
                <li class="nav-item{{ $activePage == 'howactive' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/howactive">
                    <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('Active Level') }} </span>
                    </a>
                  </li>                   
                  <li class="nav-item{{ $activePage == 'dietpreference' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/dietpreference">
                    <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('Diet Preference') }} </span>
                    </a>
                  </li>                    
                  <li class="nav-item{{ $activePage == 'fitnesslevel' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/fitnesslevel">
                    <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('Fitness Level') }} </span>
                    </a>
                  </li>
                  <li class="nav-item{{ $activePage == 'reasonchanges' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/reasonchanges">
                    <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('Reason Changes') }} </span>
                    </a>
                  </li>
                  <li class="nav-item{{ $activePage == 'what_brings' ? ' active' : '' }}">
                    <a class="nav-link" href="/masters/list/what_brings">
                    <span class="material-icons sub_masters_menus">chevron_right</span>
                      <span class="sidebar-normal">{{ __('What Brings') }} </span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
          </ul><!--// ul nav Questionnare-->

        </div><!-- // sub menu collapse -->
      </li>
      



      <li class="nav-item {{ ($activePage == 'workoutlist' || $activePage == 'programs') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#masterlib" aria-expanded="true">
          <i class="material-icons">assessment</i>
          <p>{{ __('Master Libraries') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse {{ ($activePage == 'workoutlist' || $activePage == 'programs') ? ' show' : '' }}" id="masterlib">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'programs' ? ' active' : '' }}">
              <a class="nav-link" href="/masters/program">
                <span class="sidebar-normal">{{ __('Programs') }} </span>
              </a>
            </li>

            <li class="nav-item{{ $activePage == 'workoutlist' ? ' active' : '' }}">
              <a class="nav-link" href="/workout/list">
                <span class="sidebar-normal">{{ __('Workouts') }} </span>
              </a>
            </li>

            <li class="nav-item{{ $activePage == 'habits' ? ' active' : '' }}">
              <a class="nav-link" href="/masters/habits">
                <span class="sidebar-normal">{{ __('Habits') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'exercise' ? ' active' : '' }}">
              <a class="nav-link" href="/exercise/list">
                <span class="sidebar-normal">{{ __('Exercise') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item{{ $activePage == 'food' ? ' active' : '' }}">
        <a class="nav-link" href="/foods/list">
          <i class="material-icons">restaurant</i>
          <p>{{ __('Manage Foods') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'mealplan' ? ' active' : '' }}">
        <a class="nav-link" href="/mealmaster">
          <i class="material-icons">lunch_dining</i>
          <p>{{ __('Meals Master') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'plan' ? ' active' : '' }}">
        <a class="nav-link" href="/plan/list">
          <i class="material-icons">local_offer</i>
          <p>{{ __('Manage Plan') }}</p>
        </a>
      </li>

      <li class="nav-item{{ $activePage == 'clients' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('profile.clients') }}">
          <i class="material-icons">account_box</i>
          <p>{{ __('Manage Clients') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
        <a class="nav-link" href="/user">
          <i class="material-icons">verified_user</i>
          <p>{{ __('Manage Users') }}</p>
        </a>
      </li>

      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'coachlist') ? ' active' : '' }}">
        <a class="nav-link" href="/coach/list">
          <i class="material-icons">sports</i>
          <p>{{ __('Manage Coach') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'coachcertificateslist') ? ' active' : '' }}">
        <a class="nav-link" href="/coachcertificates/list">
          <i class="material-icons">sports</i>
          <p>{{ __('Manage Coach Certificates') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'order' || $activePage == 'orderlist') ? ' active' : '' }}">
        <a class="nav-link" href="/order/list">
          <i class="material-icons">update</i>
          <p>{{ __('Manage Orders') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'challenges' || $activePage == 'challengeslist') ? ' active' : '' }}">
        <a class="nav-link" href="/challenges/list">
          <i class="material-icons">recommend</i>
          <p>{{ __('Manage Challenges') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'studio' || $activePage == 'studiolist') ? ' active' : '' }}">
        <a class="nav-link" href="/studio/list">
          <i class="material-icons">self_improvement</i>
          <p>{{ __('Manage Studio') }}</p>
        </a>
      </li>
      <!-- <li class="nav-item {{ ($activePage == 'exercise' || $activePage == 'exerciselist') ? ' active' : '' }}">
        <a class="nav-link" href="/exercise/list">
          <i class="material-icons">fitness_center</i>
          <p>{{ __('Manage Exercise') }}</p>
        </a>
      </li> -->
      <li class="nav-item {{ ($activePage == 'cardio' || $activePage == 'cardiolist') ? ' active' : '' }}">
        <a class="nav-link" href="/cardio/list">
          <i class="material-icons">sports_gymnastics</i>
          <p>{{ __('Manage Cardio') }}</p>
        </a>
      </li>



    </ul>
  </div>
</div>