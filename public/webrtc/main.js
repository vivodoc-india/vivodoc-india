$(document).on("click", ".remote", function() {
    var rmt = $(this).attr("data");
    $(this).removeClass("remote");
    if (rmt == "remote") {
        $(".localVideo").addClass("remote");
    } else {
        $(".remoteVideo").addClass("remote");
    }
    // console.log(rmt);
});
$(document).on("click", ".setting-button", function() {
    $(".setting-container").css({
        "left": "0px"
    });
});


$(document).on("click", ".setting-close", function() {
    $(".setting-container").css({
        "left": "-300px"
    });
});
$(document).on("click", ".video-container", function() {
    $(".setting-container").css({
        "left": "-300px"
    });
});
$(document).on("click",".live-alert-close", function(){
    $(".live-alert").css({"display":"none"});
});
$(document).on("click",".live-alert-action-button", function(){
    $(".live-alert").css({"display":"none"});
});
$(document).on("click",".full-button",function(){
    $(".main-container").toggleClass("full-screen");
    toggleFullScreen();
});



function toggleFullScreen() {
  if (!document.fullscreenElement &&    // alternative standard method
      !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
    if (document.documentElement.requestFullscreen) {
      document.documentElement.requestFullscreen();
    } else if (document.documentElement.msRequestFullscreen) {
      document.documentElement.msRequestFullscreen();
    } else if (document.documentElement.mozRequestFullScreen) {
      document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.webkitRequestFullscreen) {
      document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
    }
    $('body').css({"overflow":"hidden"});
  } else {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
    $('body').css({"overflow":"inherit"});
  }
}



















