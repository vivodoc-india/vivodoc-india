$(function(){
    var options = {
        thumbUl : $('#thumbnail'),
        mainPhoto : $('#main_photo'),
        parentDiv : $('#photo_container'),
        slideSpeed: 3000,
        fadeSpeed: 500,
        startPlay: true,
        maxWidth : 700, // parentDiv max-width
        thumbMaxWidth : 80,
        thumbMinWidth : 65 //  min-width
    };

    var thumbs = options.thumbUl.find('a'),
        mainPhoto = options.mainPhoto,
        thumbFiles = [],
        mainFiles = [],
        currentNum = 0,
        nextBtn = $('#next'),
        prevBtn = $('#prev'),
        nowPlay = false,
        timer,
        playBtn = $('#play_btn'),
        stopBtn = $('#stop_btn');

    // #main_photo
    window.onload = function(){
        mainPhoto.height(mainPhoto.children('img').outerHeight());
    }

    // Div Div へmax-width・width
    options.parentDiv.css('maxWidth', options.maxWidth);
    var liWidth = Math.floor((options.thumbMaxWidth / options.maxWidth) * 100);
    options.thumbUl.children('li').css({
        width : liWidth + '%',
        maxWidth : options.thumbMaxWidth,
        minWidth : options.thumbMinWidth
    });


    for(var i = 0; i < thumbs.length; i++){
        mainFiles[i] = $('<img class="img-fluid" style="max-height:430px;" />');
        mainFiles[i].attr({
            src: $(thumbs[i]).attr('href'),
            alt: $(thumbs[i]).children('img').attr('alt')

        });
        mainFiles[i] = mainFiles[i][0];
        thumbFiles[i] = $(thumbs[i]).children('img');
        thumbFiles[i] = thumbFiles[i][0];
    }

    mainPhoto.prepend(mainFiles[0]);

    $(thumbFiles[0]).parent().parent().addClass('current');


    if(options.startPlay) {
        currentNum--;
        autoPlay();
        playBtnHide();
    } else {
        playBtnShow();
    }




    thumbs.on('click', function(){
        currentNum = $.inArray($("img",this)[0], thumbFiles);
        mainView();
        stopPlay();
        playBtnShow();
        return false;
    });


    prevBtn.on('click', function(){
        currentNum--;
        if(currentNum < 0){
			currentNum = mainFiles.length - 1;
		}
        mainView();
        stopPlay();
        playBtnShow();
    });


    nextBtn.on('click', function(){
        currentNum++;
        if(currentNum > mainFiles.length - 1){
			currentNum = 0;
		}
        mainView();
        stopPlay();
        playBtnShow();
    });


    playBtn.on('click', function(){
		if(nowPlay) return;
		autoPlay();
		playBtnHide();
	});


	stopBtn.on('click', function(){
        stopPlay();
        playBtnShow();

	});


    $(window).on('resize', function(){
        mainPhoto.height(mainPhoto.find('img').outerHeight());
    });




    function mainView(){
        mainPhoto.prepend(mainFiles[currentNum]).find('img').show();
        mainPhoto.find('img:not(:first)').stop(true, true).fadeOut(options.fadeSpeed, function(){
		    $(this).remove();
		});

        thumbs.eq(currentNum).parent().addClass('current').siblings().removeClass('current');
    }


    function autoPlay(){
        nowPlay = true;
		currentNum++;
        if(currentNum > mainFiles.length - 1){
			currentNum = 0;
		}
        mainView();
		timer = setTimeout(function(){
	       autoPlay();
        }, options.slideSpeed);
	}


    function stopPlay(){
        clearTimeout(timer);
        nowPlay = false;
    }


	function playBtnShow(){
		if(nowPlay === false){
			stopBtn.hide();
			playBtn.show();
		}
	}
	function playBtnHide(){
		if(nowPlay === true){
			playBtn.hide();
			stopBtn.show();
		}
	}

});
