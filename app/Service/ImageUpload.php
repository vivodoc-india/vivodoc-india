<?php


namespace App\Service;


use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ImageUpload
{

    public static function uploadImage($file,$path,$directory="")
    {
        if($directory!="")
        {
            if(!File::exists($path.'/'.$directory))
            {
                File::makeDirectory($path.'/'.$directory,0755,true,true);
            }
        }
        $photo=$file;
        $data=array();
        $data['status']= false;
        $imagename=rand(). '.' . $photo->getClientOriginalExtension();
        $path = $directory!=''?$path.'/'.$directory:$path;
    if(Image::make($photo->getRealPath())->save($path . '/' . $imagename, 100))
    {
        $data['file'] = $imagename;
        $data['folder_name'] = $directory;
        $data['status'] = true;
    }
    else{
        return sendFailureMessage("Unable to upload document.");
    }
    return $data;
    }
}
