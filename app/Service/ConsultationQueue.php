<?php


namespace App\Service;


use App\Models\LiveConsultation;
use App\Models\ServiceProfile;

class ConsultationQueue
{

    public static function countRequests($user)
    {
        return LiveConsultation::where('provider_id',$user)
            ->where('booking_date',today())
            ->where('request_status','3')
            ->count();
    }

    public static function providerStatus($user,$sp){
        $serv_pro = ServiceProfile::where('id',$sp)->where('user_id',$user)->first();
        return $serv_pro->is_available;
    }
}
