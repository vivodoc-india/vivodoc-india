<?php


namespace App\Service;


class AppNotificationService
{

//    public function sendToSingleUser(){
//
//        $token = "fqwYlhB-RUCz4rcqj1QX8s:APA91bFePbaMT4PQXPqE_Q7MSdhdTnBtRIVIr500SuZ75_3NvWemYvhKX9LCi2EXP8RBrDD8PIYjAPpk8vxexN1MHYUBLJCSBV4GcRhk4vuQJLslkyEv8ZBsnUTmdGXJ84JlPRBvLaoj";
//        return $this->sendSingleUserNotification($token, array(
//            "title" => "Offer for you",
//            "body" => "Testing"
//        ));
//    }

//    public static function sendSingleUserNotification($token, $notification)
    public static function sendSingleUserNotification($token,$content, $notification)
    {
        $SERVER_API_KEY = 'AAAAIHrymbw:APA91bEuH71RsG5rOUAYE6BslzpFgJsMXwgfPBXjtt4YWU7EUfHt1-t_OLHUyQARWJ4e8e1UIidfm9I238qX3_QCbJ-vMyzszrKz_X1qwNy4Ep9X4VzuIS-HCVffoYgFqoHUVHiQi-cZ';

        // payload data, it will vary according to requirement
        $data = [
            "to" => $token, // for single device id
            "data" => $content,
//            "data" => $notification,
            "notification" => $notification
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization:key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }
}
