<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [

        'https://vivodoc.in/cf-appointment/return',
        'https://vivodoc.in/cf-appointment/notify',
        'https://vivodoc.in/cf-ambulance/return',
        'https://vivodoc.in/cf-ambulance/notify',
        'https://vivodoc.in/cf-payment/notify',
        'https://vivodoc.in/cf-payment/return',
        'https://vivodoc.in/omed-api/user/notify',
        'https://vivodoc.in/omed-api/user/return',
        'https://vivodoc.in/omed-api/user/ambulance-fee/notify',
        'https://vivodoc.in/omed-api/user/ambulance-fee/return',
        'https://vivodoc.in/omed-api/user/appointment-fee/notify',
        'https://vivodoc.in/omed-api/user/appointment-fee/return',
        'https://vivodoc.in/omed-api/user/coin-purchase-payment/notify',
        'https://vivodoc.in/omed-api/user/coin-purchase-payment/return',
        'https://vivodoc.in/wl-payment/notify',
        'https://vivodoc.in/wl-payment/return',

    ];
}
