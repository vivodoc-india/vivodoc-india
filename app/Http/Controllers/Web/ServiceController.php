<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ServiceProfile;
use App\Models\ServiceProfileCategory;
use App\Models\ServiceProfileDocument;
use App\Service\ImageUpload;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\File;
use Session;
use DB;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Category;
use App\Models\Profile;
use Image;
use App\Models\State;
use Illuminate\Support\Facades\Validator;
class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] =" Services";
$data['states']=State::select('name')->get();
        // Services And Profile Details Selected by User
        $service=ServiceProfile::where('user_id',auth()->user()->id)->orderBy('user_role_id','asc')->get();
        $n=0;
    $sel_cat='';
       foreach($service as $s)
       {
           $data['services'][$n]['serv']=$s;
           $cat_to_update= Category::where('user_role_id',$s->user_role_id)->where('status','1')->where('is_private',0)->select('id','title')->get();
           $data['services'][$n]['cat_to_update']=$cat_to_update;
            $cat= ServiceProfileCategory::where('service_profile_id',$s->id)->pluck('category_id')->toArray();
           $data['services'][$n]['documents'] = ServiceProfileDocument::where('service_profile_id',$s->id)
                                                            ->select('id','folder_name','file')
                                                            ->get();
           if($s->category_id !=" ")
           {
$category=Category::whereIn('id',$cat)->where('user_role_id',$s->user_role_id)->where('status','1')->select('id','title')->get();
$data['services'][$n]['cat']=$category;

$cat_to_add=Category::whereNotIn('id',$cat)->where('user_role_id',$s->user_role_id)->where('status','1')->where('is_private',0)->select('id','title')->get();
$data['services'][$n]['cat_to_add_new']=$cat_to_add;
           }
$n++;
       }

       return view('web.provider.services',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'category_id' => 'required|array|min:1',
            'category_id.*' =>'required|numeric',
            'related_document' =>'nullable|array|min:1',
            'related_document.*' => 'image|required|mimes:jpg,jpeg,png|max:500|min:100',
        ]);
if(!isset($request->id))
{
    return redirect()->back()->with('error','Something went wrong.');
}
else
{
$service=ServiceProfile::findOrFail($request->id);
for($i=0;$i<count($request->category_id);$i++)
{
    $category['category_id']= $request->category_id[$i];
    $category['user_id']=auth()->user()->id;
    $category['service_profile_id'] = $service->id;
    $category['user_role_id'] = $service->user_role_id;

    $isExists = ServiceProfileCategory::where('service_profile_id',$request->id)
        ->where('user_id',auth()->user()->id)
        ->where('user_role_id',$service->user_role_id)
        ->where('category_id',$request->category_id[$i])
        ->exists();
    if(!$isExists)
    {
       ServiceProfileCategory::create($category);
    }
    else{
        return redirect()->back()->with('error','Some categories are already added to your profile.');
    }
}
if($request->has('related_document') && !empty($request->related_document)){
    for($j=0;$j<count($request->related_document);$j++)
    {
        if(!$request->file('related_document')[$j]->isValid())
        {
            return redirect()->back()->with('error','Something went wrong.');
        }
        $file = $request->file('related_document')[$j];
        $path= public_path('uploads/service/documents');
        $directory = auth()->user()->mobile.'_'.$service->user_role_id.$request->id;
        $upload = ImageUpload::uploadImage($file,$path,$directory);
        if($upload['status'])
        {
            $doc['user_id'] = auth()->user()->id;
            $doc['user_role_id'] = $service->user_role_id;
            $doc['service_profile_id'] = $request->id;
            $doc['folder_name'] = $upload['folder_name'];
            $doc['file'] = $upload['file'];
            ServiceProfileDocument::create($doc);
        }
        else{
            return redirect()->back()->with('error','Some documents are not uploaded.');
        }
    }
}

return redirect()->back()->with('success','Categories added to your profile.');
}

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $validator= Validator::make($request->all(),[
           'reg_no' => 'nullable|string',
           'description' => 'nullable|string',
           'landmark' => 'nullable|string',
           'area' => 'nullable|string',
           'city' => 'nullable|string',
           'address'=> 'nullable|string',
           'state' => 'nullable|string',
           'min_fee' => 'nullable|numeric',
           'max_fee' => 'nullable|numeric',
           'vehicle_no' => 'nullable|string',
           'google_address' => 'nullable|string',
           'g_lat' => 'nullable|string',
           'g_long' => 'nullable|string',

       ])->stopOnFirstFailure(true);

       if($validator->fails())
       {
           return redirect()->back()->with('error',$validator->errors())->withInput();
       }
        if(!isset($request->id))
{
    return redirect()->back()->with('error','Something went wrong.');
}
else
{
$service= ServiceProfile::findOrFail($request->id);
if($service->trashed())
{
    $service->restore();
}
$data= $request->except('_token','id','_method');
$data['updated_at'] = date('Y-m-d H:i:s');
$chk=ServiceProfile::where('id',$request->id)->update($data);
if($chk)
{
    return  redirect()->back()->with('success','Records updated successfully.');
}
else
{
    return  redirect()->back()->with('error','Unable to update records.');
}
}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $delete=ServiceProfile::find($id);
        if($delete->delete())
        {
    return  redirect()->back()->with('success','You are removed from selected service provider.');
}
else
{
    return  redirect()->back()->with('error','Unable to remove.');
}
    }

    // Save Services Related Images
    public function saveServicesImages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'image|mimes:jpg,jpeg,png|max:2048',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Invalid data sent')->withErrors($validator->errors())->withInput();
        }
        if(!isset($request->sp_id) || !isset($request->sp_ur_id) || !isset($request->image_count) || !is_numeric($request->sp_id) || !is_numeric($request->sp_ur_id) || !is_numeric($request->image_count))
        {
            return redirect()->back()->with('error' ,'Something went wrong. unable to process.');
        }
        if(!$request->file('image')->isValid())
        {
            return redirect()->back()->with('error','Something went wrong.');
        }
        $service = ServiceProfile::where('id',$request->sp_id)->pluck('image'.$request->image_count);
            $photo=$request->file('image');

        $path= public_path('uploads/service/banner_images');
        $directory = auth()->user()->mobile.'_'.$request->sp_ur_id.$request->sp_id;
        $upload = ImageUpload::uploadImage($photo,$path,$directory);
        if($upload['status'])
        {
            if($service!='' && File::exists(public_path($service)))
            {
                File::delete(public_path($service));
            }
            $image = 'uploads/service/banner_images/'.$upload['folder_name'].'/'.$upload['file'];

            if(ServiceProfile::where('id',$request->sp_id)->update(['image'.$request->image_count => $image]))
            {
                return redirect()->back()->with('success','Image updated successfully.');
            }
            else{
                return redirect()->back()->with('error','Unable to update image.');
            }
        }

else{
    return redirect()->back()->with('error','Unable to process image.');
}
    }
}
