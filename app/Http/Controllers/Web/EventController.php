<?php

namespace App\Http\Controllers\Web;

use App\Models\BookingSchedule;
use App\Models\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
//use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\UserRole;

class EventController extends Controller
{
    public function index()
    {
        $data['title']=' Manage Schedule';
        $data['details']=Event::where('user_id',auth()->user()->id)->orderby('id','desc')->get();
        return view('web.provider.manage_schedule',$data);
    }
    public function createEvent()
    {
        $data['title'] = ' Create Schedule';
        $data['method'] ="POST";
        $data['url'] = "event/add";
        return view('web.provider.create_event',$data);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_date' => 'required|date',
            'end_date' => 'required|date',
//            'color' => 'string',
//            'text_color' => 'string',
            'is_off' => 'required|numeric|in:1,0',
            'total_bookings' => 'nullable|numeric',
            'timings' =>'array',
            'timing.*' => 'string',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong.')->withErrors($validator)->withInput();
        }
        if(isset($request->is_off) && !$request->is_off)
        {
            if(!isset($request->timings))
            {
                return redirect()->back()->with('error','Please mention your timings when you will available.')->withInput();
            }
            if(!isset($request->total_bookings) || $request->total_bookings <=0)
            {
                return redirect()->back()->with('error','Please mention total number of bookings per day you will accept.')->withInput();
            }
        }
        $startTime = date('Y-m-d',strtotime($request->start_date));
        $endTime = date('Y-m-d',strtotime($request->end_date));
        $event= new Event();
        $event->user_id=auth()->user()->id;
        $event->start_date=date('Y-m-d',strtotime($request->start_date));
        $event->end_date=date('Y-m-d',strtotime($request->end_date));
//        $event->color =$request->color;
//        $event->text_color = $request->text_color;
        $event->is_off = $request->is_off;
        if(!$request->is_off)
        {
            $event->total_bookings = $request->total_bookings;
            $event->timings = implode(',',$request->timings);
        }

        $eventsCount=Event::where('user_id',auth()->user()->id)->whereBetween('start_date',[$startTime,$endTime])->orWhereBetween('end_date',[$startTime,$endTime])->count();

        if($eventsCount > 0)
        {
            return redirect()->back()->with('error',' You have already make schedule for selected date range.')->withInput();
        }

         if($event->save() && !$request->is_off)
         {
//             $bs= new BookingSchedule();
             $s_date=date('Y-m-d',strtotime($request->start_date));
             $e_date = date('Y-m-d',strtotime($request->end_date));

             while (strtotime($s_date )<= strtotime( $e_date))
             {
                 $ins= BookingSchedule::create([
                  'user_id' => auth()->user()->id,
                 'event_id' => $event->id,
                 'date' => $s_date,
                'total_bookings' => $request->total_bookings,
                 ]);

                 if($ins)
                 {
                     $s_date = date('Y-m-d',strtotime('+1 day',strtotime($s_date)));
                 }
             }

         }
        return redirect('event/manage')->with('success', 'Event has been added');
    }

    public function editEvent($id)
    {

        $data['title'] = ' Edit Schedule';
        $data['method'] ="post";
        $data['url'] = "event/update";
        $data['event'] = Event::where('id',$id)->first();
        return view('web.provider.create_event',$data);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
//            'start_date' => 'required|date',
//            'end_date' => 'required|date',
//            'color' => 'string',
//            'text_color' => 'string',
            'is_off' => 'required|numeric|in:1,0',
            'total_bookings' => 'nullable|numeric',
            'timings' =>'array',
            'timing.*' => 'string',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong.')->withErrors($validator)->withInput();
        }
        if(isset($request->is_off) && $request->is_off)
        {
            if(!isset($request->timings))
            {
                return redirect()->back()->with('error','Please mention your timings when you will available.')->withInput();
            }
            if(!isset($request->total_bookings) || $request->total_bookings <=0)
            {
                return redirect()->back()->with('error','Please mention total number of bookings per day you will accept.')->withInput();
            }
        }

        $event=Event::find($request->id);
        //        $event->color =$request->color;
//        $event->text_color = $request->text_color;
        $event->is_off = $request->is_off;
        if(!$request->is_off)
        {
            $event->total_bookings = $request->total_bookings;
            $event->timings = implode(',',$request->timings);
        }
        if($request->is_off)
        {
            $event->total_bookings = 0;
            $event->timings = " ";
        }
        if($event->save() && !$request->is_off)
        {
            $s_date=date('Y-m-d',strtotime($event->start_date));
            $e_date = date('Y-m-d',strtotime($event->end_date));

            while (strtotime($s_date )<= strtotime( $e_date))
            {

                $chk=BookingSchedule::where('user_id',auth()->user()->id)->where('event_id',$event->id)->where('date',$s_date)->count();
                if($chk > 0)
                {
                    $ins =BookingSchedule::where('user_id',auth()->user()->id)->where('event_id',$event->id)->where('date',$s_date)->update([
                        'total_bookings' => $request->total_bookings,
                    ]);
                }else{
                    $ins= BookingSchedule::create([
                        'user_id' => auth()->user()->id,
                        'event_id' => $event->id,
                        'date' => $s_date,
                        'total_bookings' => $request->total_bookings,
                    ]);
                }
                if($ins)
                {
                    $s_date = date('Y-m-d',strtotime('+1 day',strtotime($s_date)));
                }
            }
            return redirect('event/manage')->with('success', 'Event has been updated.');
        }
       elseif ($event->save() && $request->is_off)
       {
           $bs=BookingSchedule::where('user_id',auth()->user()->id)->where('event_id',$event->id)->delete();
               return redirect('event/manage')->with('success', 'Event has been updated.');
       }
        else
        {
            return redirect('event/manage')->with('error', 'Unable to update event.');
        }
    }
    public function calender()
    {
//        $events = [];
//        $data = Event::where('user_id',auth()->user()->id)->get();
//        if($data->count())
//        {
//            foreach ($data as $key => $value)
//            {
//                $events[] = Calendar::event(
//                    $value->title,
//                    true,
//                    new \DateTime($value->start_date),
//                    new \DateTime($value->end_date.'+1 day'),
//                    null,
//                    // Add color
//                    [
//                        'color' => $value->color,
//                        'textColor' => $value->text_color,
//
//                    ]
//                );
//            }
//        }
//        $data['calendar'] = Calendar::addEvents($events);
//        $data['title'] = " My Calender";
//        return view('web.calendar', $data);
    }

    public function destroy(Request $request)
    {
        $event=Event::where('id',$request->id)->where('user_id',auth()->user()->id)->first();
        if($event)
        {
            $event->delete();
            return redirect()->back()->with('success','Schedule removed successfully.');
        }
        else{
            return redirect()->back()->with('error','Unable to remove schedule.');
        }
    }

    public function getScheduleByDate($id)
    {
        $data['title'] = ' Edit Schedule';
        $data['event'] = Event::where('id',$id)->first();
        $data['bookings'] = BookingSchedule::where('event_id',$id)->get();
        return view('web.provider.daywise_event',$data);

    }
}

