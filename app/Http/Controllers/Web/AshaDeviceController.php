<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\AshaDeviceData;
use Illuminate\Support\Facades\Validator;

class AshaDeviceController extends Controller
{
    public function storeData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'patientId' => 'required|string',
            'device' => 'required|string',
            'pulse' => 'required|numeric',
            'temperature' => 'required|string',
            'sys' => 'required|numeric',
            'dia' => 'required|numeric',
            'spo2' => 'required|numeric',
            'glucose' => 'required|numeric',
            'steth' => 'required|string',
            'ecg' => 'required|string',
            'date' => 'required|date',
            'time' => 'required|string',
        ]);
        if ($validator->fails()) {
            $response['status'] = false;
            $response['message']= "Invalid data submission.";
            return response()->json(['response' => $response],422);

        }
        $data = array();
        $data['patient_id'] = $request->patientId;
        $data['device_id'] = $request->device;
        $data['pulse'] = $request->pulse;
        $data['temperature'] = $request->temperature;
        $data['sys'] = $request->sys;
        $data['dia'] = $request->dia;
        $data['spo'] = $request->spo2;
        $data['glucose'] = $request->glucose;
        $data['steth'] = $request->steth;
        $data['ecg'] = $request->ecg;
        $data['date'] = date('Y-m-d',strtotime($request->date));
        $data['time'] = date('H:i:s',strtotime($request->time));
        if(AshaDeviceData::create($data))
        {
            $response['status'] = true;
            $response['message']= "Data submitted.";
            return response()->json(['response' => $response],200);
        }
        else{
            $response['status'] = false;
            $response['message']= "Unable to submit data.";
            return response()->json(['response' => $response],500);
        }
    }

}
