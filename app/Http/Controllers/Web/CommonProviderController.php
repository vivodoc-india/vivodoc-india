<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\PatientEnquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Category;
use App\Models\Profile;
use App\Models\ServiceProfile;
use Image;
use Illuminate\Support\Facades\File;
//use App\Models\ReviewRating;
use App\Models\EnquiryChatNode;

class CommonProviderController extends Controller
{

    public function changeProviderPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' =>'required|string',
            'new_password' => 'required|string',
            'password_confirmation' =>'required|string|same:new_password'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong')->withInput()->withErrors($validator);
        }

        $user = User::where('id',Auth::user()->id)->first();
        if(Hash::check($request->current_password,$user->password))
        {
        $user->password = Hash::make($request->new_password);
        if($user->save())
        {
            Auth::logout();
            return redirect('/login')->with('success','Login again with newly generated password.');
        }
        else{
            Auth::logout();
            return redirect('/login');
        }
        }

    }
    // Function to get all enquiries
    public function myEnquiries()
    {
$data['title']=" My Enquiries";
$data['enquiry']=PatientEnquiry::where('provider_id',Auth::user()->id)->orderby('id','desc')->get();
return view('web.enquiries',$data);
    }

    // Load Reply view for Patient enquiry
    public function loadReplyView($id)
    {
        $data['title']=" Patient Enquiry Reply";
        $data['enquiry']=PatientEnquiry::where('id',$id)->first();
        return view('web.reply-enquiry',$data);
    }

    // Function to save Patient enquiry reply

    public function savePatientEnquiryReply(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'provider_reply' =>'required|string',
            'prov_doc' =>'array|min:1',
            'prov_doc.*' => 'image|mimes:jpg,jpeg,png',
        ]);
        if(!isset($request->reply_of) || $request->reply_of=='')
        {
            return redirect()->back()->with('error','Somthing went wrong.');
        }
        $frd='';
        $destinationPath='';
        if(!empty($request->file('prov_doc')))
        {

            $mno= Auth::user()->mobile;
            $directory=$mno.'_'.time();
            $def_path=public_path('uploads/patientprofile/enquiry/');
            if(!is_dir($def_path.$directory))
            {
                if(mkdir($def_path.$directory,0777))
                {
                    $destinationPath=$def_path.$directory;
                }
            }

            for($j=0;$j<count($request->prov_doc);$j++)
            {
                $photo=$request->file('prov_doc')[$j];
                $imagename=rand() . '.' . $photo->getClientOriginalExtension();
                $thumb_img = Image::make($photo->getRealPath())->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($destinationPath.'/'.$imagename,80);
                if($frd!='')
                {
                    $frd=$frd.",".$imagename;
                }
                else
                {
                    $frd=$imagename;
                }

            }
        }
        if($destinationPath!='' && $frd!=' ')
        {
            $node = PatientEnquiry::where('id',$request->reply_of)->first();
         $data['patient_enquiry_id'] = $node->id;
         $data['patient_id'] = $node->patient_id;
         $data['provider_id'] = $node->provider_id;
         $data['question'] = $node->question;
         $data['provider_reply'] = $node->provider_reoply;
         $data['admin_reply'] = $node->admin_reply;
         $data['pat_directory'] = $node->pat_directory;
         $data['pat_files'] = $node->pat_files;
            $data['prov_directory'] = $node->prov_directory;
            $data['prov_files'] = $node->prov_files;
            $data['status'] = $node->status;
            $data['created_at'] = $node->created_at;
            $data['updated_at'] = $node->updated_at;
            $data['admin_updated_at'] = $node->admin_updated_at;

            EnquiryChatNode::create($data);
            $update=PatientEnquiry::where('id',$request->reply_of)->update([
                'provider_reply' => $request->provider_reply,
                'prov_directory' => $directory,
                'prov_files' =>$frd,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        else{
            $node = PatientEnquiry::where('id',$request->reply_of)->first();
            $data['patient_enquiry_id'] = $node->id;
            $data['patient_id'] = $node->patient_id;
            $data['provider_id'] = $node->provider_id;
            $data['question'] = $node->question;
            $data['provider_reply'] = $node->provider_reoply;
            $data['admin_reply'] = $node->admin_reply;
            $data['pat_directory'] = $node->pat_directory;
            $data['pat_files'] = $node->pat_files;
            $data['prov_directory'] = $node->prov_directory;
            $data['prov_files'] = $node->prov_files;
            $data['status'] = $node->status;
            $data['created_at'] = $node->created_at;
            $data['updated_at'] = $node->updated_at;
            $data['admin_updated_at'] = $node->admin_updated_at;

            EnquiryChatNode::create($data);
            $update=PatientEnquiry::where('id',$request->reply_of)->update([
                'provider_reply' => $request->provider_reply,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }

        if($update)
        {
            return redirect()->back()->with('success','Your reply has been saved.');
        }
        else{
            return redirect()->back()->with('error','Unable to save your reply.');
        }
    }
    // Function to get all reviews and ratings

    public function myReviewRating()
    {
        $data['title']=" My Reviews & Ratings";
        $data['review']=ReviewRating::where('provider_id',Auth::user()->id)->where('status','1')->orderby('id','desc')->paginate(5);
        return view('web.reviews',$data);
    }


}
