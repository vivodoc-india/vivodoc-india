<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\MedicalData;
use App\Service\ImageUpload;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Facades\Validator;
use Image;
use App\Models\Setting;
use Illuminate\Support\Facades\File;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class MedicalHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        if(Auth::user()->user_role_id!=3)
//        {
//            return redirect('patient/login')->with('error','Invalid user access found');
//        }
    }
    public function index()
    {
      $data['title']=" Medical History";
      $data['list'] = MedicalData::where('user_id',auth()->user()->id)->where('parent',0)->orderby('updated_at','desc')->get();
      return view('web.patient.medical-history',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'data_type' =>'required|string|in:lab_test,prescription',
        ]);

        $directory = auth()->user()->mobile.'_'.rand();
            $def_path=public_path('uploads/patientprofile/medical_data/');
            if(!is_dir($def_path.$directory))
            {
                mkdir($def_path.$directory,0777);

            }
            $chk=MedicalData::create([
                'user_id' => auth()->user()->id,
                'title' => $request->title,
                'data_type' => $request->data_type,
                'parent' => 0,
                'folder'=>$directory,
            ]);
            if($chk)
            {
                return redirect()->back()->with('success','Records stored succesfully.');
            }
            else{
                return redirect()->back()->with('error','Unable to process your records.');
            }
    }


    public function addMoreData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'parent' =>'required|numeric',
            'docs' =>'required|array|min:1',
            'docs.*' => 'image|required|mimes:jpg,jpeg,png',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error',$validator->errors());
        }
        $parent=MedicalData::where('id',$request->parent)->first();
        $path= public_path('uploads/patientprofile/medical_data/');
        $directory =$parent->folder;
        $data['user_id'] = auth()->user()->id;
        $data['parent'] = $request->parent;
        $data['data_type'] = $parent->data_type;

        for($j=0;$j<count($request->docs);$j++)
        {
            $file=$request->file('docs')[$j];
            $upload = ImageUpload::uploadImage($file,$path,$directory);
            if($upload['status'])
            {
                $data['folder'] = $upload['folder_name'];
                $data['file'] = 'uploads/patientprofile/medical_data/'.$upload['folder_name'].'/'.$upload['file'];
                 MedicalData::create($data);
            }

        }

        if($request->parent > 0)
        {
            MedicalData::where('id',$request->parent)->update(['updated_at'=>now()]);

        }
        return redirect()->back()->with('success','Record added successfully.');
//        else{
//            return redirect()->back()->with('error','Something went wrong.');
//        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data['title']=" Medical History Detail";
      $data['parent']= $id;
      $data['record']=MedicalData::where('parent',$id)->orderby('created_at','desc')->get();
      return view('web.patient.medical-data-detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=MedicalData::find($id);
      $path=public_path('uploads/patientprofile/medical_data/'.$data->directory_name);
            if(File::exists($path)) {
                File::deleteDirectory($path);
            }

        $data->delete();
        return redirect('patient/medical-data')->with('success','Record deleted successfully');
    }

    public function removeData(Request $request)
    {
        $record = MedicalData::where('user_id',auth()->user()->id)->where('id',$request->id)->first();
        if($record!='')
        {
            if($record->parent > 0)
            {
                MedicalData::where('id',$record->parent)->update(['updated_at'=>now()]);
            }
            if($record->parent == 0)
            {
                MedicalData::where('parent',$record->id)->delete();
            }
            if($record->delete())
            {

                return redirect()->back()->with('success','Data removed.');
            }
            else{
                return redirect()->back()->with('error','unable to remove selected data.');
            }
        }
    }
}
