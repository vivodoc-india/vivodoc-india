<?php

namespace App\Http\Controllers\Web;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\State;
use App\District;
use App\Location;
use DB;
use Illuminate\Support\Facades\Session;

class WebAjaxController extends Controller
{
    public function getCity(Request $request)
    {
        $city=District::where('state',$request->state)->get();
        return response()->json(['city'=>$city,'success'=>true]);
    }

    public function getArea(Request $request)
    {
        $area=Location::where('city',$request->city)->where('state',$request->state)->select('area')->orderby('area','asc')->get();
        return response()->json(['area'=>$area,'success'=>true]);
    }

    public function savePatientLocation(Request $request){
        if($request->longitude!='' && $request->latitude!='')
        {
            Session::put('cur_longitude',$request->longitude);
            Session::put('cur_latitude',$request->latitude);
        }
              return response()->json(['success'=>true]);
    }
    public function changeFlag(Request $request){
        DB::table($request->table)->where('id',$request->id)->update([$request->column=>1]);
        return response()->json(['status'=>1,'success'=>true]);
//        $get=DB::table($request->table)->where('id',$request->id)->pluck($request->column)->first();
//        if($get==1){
//            DB::table($request->table)->where('id',$request->id)->update([$request->column=>0]);
//            return response()->json(['status'=>0,'success'=>true]);
//        }else{
//
//        }
    }

    public function changeLiveStatus(Request $request){
        $get=DB::table($request->table)->where('id',$request->id)->pluck($request->column)->first();
        if($get==1){
            DB::table($request->table)->where('id',$request->id)->update([$request->column=>0]);
            return response()->json(['status'=>0,'success'=>true]);
        }elseif($get==0){
            DB::table($request->table)->where('id',$request->id)->update([$request->column=>1]);
            return response()->json(['status'=>1,'success'=>true]);
        }
        else{

        }
    }

    public function getSubcategory(Request $request){
        $category = Category::where('user_role_id',$request->role)->where('status',1)->select('id','title')->get();
        return response()->json(['category'=>$category,'success'=>true]);
    }
}
