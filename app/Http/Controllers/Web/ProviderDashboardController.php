<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Image;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Category;
use App\Models\Profile;
use App\Models\ServiceProfile;
use App\Models\Setting;
use Illuminate\Support\Facades\File;
use App\Http\Requests\PersonalInfoRequest;
use App\Http\Requests\DocumentUpdateRequest;
class ProviderDashboardController extends Controller
{
    private $config;
   public function __construct()
   {
       $this->middleware('provider');
       $setting = Setting::all();
       foreach ($setting as $data)
       {
           $this->config[$data->param] = $data->data;
       }

   }

   public function index()
   {
       $ini_update=Auth::user()->initial_update;
       if($ini_update == 0)
       {
           return redirect('/update-profile');
       }
       $data['title'] =" Dashboard";
       $data['profile']=Profile::where('user_id',Auth::user()->id)->first();
       $data['has_ambulance'] = ServiceProfile::where('user_role_id',6)->where('user_id',auth()->user()->id)->first();
       $data['feerange']=ServiceProfile::where('user_id',Auth::user()->id)->first();

       return view('web.provider.dashboard',$data);
   }

   // Change Live Status
    public function changeLiveStatus(Request $request)
    {
        $val=$request->liveval;
//        $status = Profile::where('user_id',Auth::user()->id)->first();
        $status = ServiceProfile::where('user_id',auth()->id())->where('user_role_id','!=',6)->first();
        if($status->mark_live==1 && $val==0)
        {

            if(ServiceProfile::where('user_id',auth()->user()->id)->where('user_role_id','!=',6)->update(['mark_live'=>0]))
            {
                return redirect()->back()->with('success','Status changed successfully');
            }
            else{
                return redirect()->back()->with('error','Something went wrong.');
            }
        }
        if($status->mark_live==0 && $val==1)
        {

            if(ServiceProfile::where('user_id',auth()->user()->id)->where('user_role_id','!=',6)->update(['mark_live'=>1]))
            {
                return redirect()->back()->with('success','Status changed successfully');
            }
            else{
                return redirect()->back()->with('error','Something went wrong.');
            }
        }
    }
    public function changeAmbulanceLiveStatus(Request $request)
    {
        $val=$request->mark_live;
        $status = ServiceProfile::where('user_id',Auth::user()->id)->where('user_role_id',6)->first();
        if($status->mark_live==1 && $val==0)
        {
            if($status->update(['mark_live'=>0,'longitude'=>$request->longitude,'latitude'=>$request->latitude]))
            {
                return redirect()->back()->with('success','Status changed successfully');
            }
            else{
                return redirect()->back()->with('error','Something went wrong.');
            }
        }
        if($status->mark_live==0 && $val==1)
        {
            if($status->update(['mark_live'=>1,'longitude'=>$request->longitude,'latitude'=>$request->latitude]))
            {
                return redirect()->back()->with('success','Status changed successfully');
            }
            else{
                return redirect()->back()->with('error','Something went wrong.');
            }
        }
    }
   // Load Update Profile view
   public function updateProfile()
   {
    $data['title']=" Update Profile";
//    $ini_update=Auth::user()->initial_update;
       $cat_except=[3,7,8];

$data['user']=User::where('id',Auth::user()->id)->first();
if(auth()->user()->initial_update)
{
    $live_cat = ServiceProfile::where('user_id',auth()->user()->id)->whereIn('user_role_id',[1,2,9])->get();
    if($live_cat->isNotEmpty() && count($live_cat) > 0)
    {
        array_push($cat_except,6);
    }
    $live_amb = ServiceProfile::where('user_id',auth()->user()->id)->where('user_role_id',6)->first();
    if($live_amb!='')
    {
        array_push($cat_except,1);
        array_push($cat_except,2);
        array_push($cat_except,9);
    }
    $user_role_sel=ServiceProfile::where('user_id',auth()->user()->id)->select('user_role_id')->get();
    if(count($user_role_sel) > 0)
    {
        foreach($user_role_sel as $role)
        {
            if(!in_array($role->user_role_id,$cat_except))
            {
                array_push($cat_except,$role->user_role_id);
            }
        }

    }
    $data['has_ambulance'] = $live_amb;
//$data['list']=UserRole::where('status',1)->where('id','!=',7)->where('id','!=',8)->where('id','!=',3)->get();
}
//else{
//    $user_role_sel=ServiceProfile::where('user_id',Auth::user()->id)->select('user_role_id')->get();

    $data['list_add']=UserRole::whereNotIn('id',$cat_except)->where('status','1')->select('id','name')->get();
//}

       return view('web.provider.update-profile',$data);
   }

   // Update Personal Info
   public function personalInfoUpdate(PersonalInfoRequest $request)
   {
    $validated = $request->validated();
     $data=$request->except(['_token','name','mobile','email','user_role_id']);
     if(!$request->has('mobile') && !$request->has('email'))
     {
         return redirect()->back()->with('error','Please enter either mobilenumber or email id.');
     }
     $user['name'] = $request->name;
     $user['email'] = $request->email;
     $user['mobile'] = $request->mobile;

       $user_roles=array();
     if($request->has('user_role_id') && !empty($request->user_role_id))
     {
         $user_roles=$request->user_role_id;
     }
User::where('id',Auth::user()->id)->update($user);
    $chk=Profile::where('user_id',Auth::user()->id)->get();
    if(count($chk) > 0)
    {
    $data['updated_at']=date("Y-m-d h:i:s");
       $ins=DB::table('profiles')->where('user_id',Auth::user()->id)->update($data);
    }
    else {
    $data['created_at']=date("Y-m-d h:i:s");
    $data['updated_at']=date("Y-m-d h:i:s");
    $data['user_id']=Auth::user()->id;
    $ins=DB::table('profiles')->insertGetId($data);
    }
    if(count($user_roles) > 0)
    {
        $this->serviceInfoUpdate($user_roles);
    }

    if($ins > 0)
    {
        return redirect()->back()->with('success','Profile updated.');
    }
    else {
        return redirect()->back()->with('error','Unable to update details.');
    }

   }

   // Save Services Info
   public function serviceInfoUpdate($roles)
   {
        $j=count($roles);
        for($i=0;$i<count($roles);$i++)
        {
            $rec['user_role_id']=$roles[$i];
            if($roles[$i]==6)
            {
               if(ServiceProfile::where('user_id',auth()->user()->id)->whereIn('user_role_id',[1,2,9])->exists()){
                   return redirect()->back()->with('error','Ambulance profile is not allowed.');
               }
            }
            if(in_array($roles[$i],[1,2,9]))
            {
                if(ServiceProfile::where('user_id',auth()->user()->id)->where('user_role_id',6)->exists()){
                    return redirect()->back()->with('error','Some profiles are not allowed with Ambulance Profile.');
                }
            }
            $rec['user_id']=auth()->id();
            $rec['created_at']=date("Y-m-d h:i:s");
            $rec['updated_at']=date("Y-m-d h:i:s");
            $find= ServiceProfile::where('user_id',auth()->user()->id)->where('user_role_id',$rec['user_role_id'])->first();
            if($find!='' && $find->trashed())
            {
                $find->restore();
            }
            else{
                ServiceProfile::create($rec);
            }
            $j--;
        }
        if($j==0)
        {
            if(Auth::user()->initial_update == 0)
            {
                User::where('id',Auth::user()->id)->update(['initial_update'=>1]);
            }
//            $text = "Dear Admin, a service provider (". Auth::user()->name. ") has updated his/her service provider category. Kindly check and approve. Regards ". $this->config['sms_regard'].".";
            $text = "Dear Admin, a service provider has updated his/her service provider category. Regards ". $this->config['sms_regard'].".";
            sms_sender(trim($this->config['admin_mno']), $text,$this->config['sms_notify_admin']);
        }

   }

   // Update Documents
   public function documentUpdate(DocumentUpdateRequest $request)
   {
        $validated = $request->validated();
        // Profile Pic
       $prev_image = Profile::where('user_id',auth()->user()->id)->first();
       $path = public_path('uploads/profile/');
       $frd ='';
       $photo=$request->file('photo');
       $imagename=rand(). '.' . $photo->getClientOriginalExtension();
       $thumb_img = Image::make($photo->getRealPath())->save($path . '/' . $imagename, 80);
       if(File::exists($path.$prev_image->photo)) {
           File::delete($path.$prev_image->photo);
       }
       $prev_image->photo = $imagename;
            if($prev_image->save())
            {
                return redirect()->back()->with('success','Documents uploaded successfully.');
            }
            else {
                return redirect()->back()->with('error','Unable to upload documents.');
            }
   }
}
