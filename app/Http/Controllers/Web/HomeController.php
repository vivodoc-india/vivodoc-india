<?php

namespace App\Http\Controllers\Web;


use App\Http\Requests\StoreContactUsPost;
use App\Models\AppointmentBooking;
use App\Models\Enquiry;
use App\Models\Event;
use App\Models\Gallery;
use App\Models\LiveConsultation;
use App\Models\Media;
use App\Models\ServiceProfileCategory;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserRole;
use App\Models\User;
use Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use DB;
use App\Models\State;
use App\Models\Category;
use App\Models\ServiceProfile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination;
use Image;
use File;
//use Calendar;
use App\Models\BookingSchedule;
class HomeController extends Controller
{
    private $config;
    public function __construct()
    {
        $setting = Setting::all();
        foreach ($setting as $data)
            $this->config[$data->param] = $data->data;
    }
    public function index()
    {
        $data['title']="VivoDoc";
        $categories=UserRole::where('id','!=','8')->where('id','!=','7')->where('id','!=','3')->where('status','1')->get();
        $data['new_registration']=User::has('profile')->where('approval','1')->whereHas('service_profile')->orderby('created_at','desc')->limit(10)->get();
        $omed_cat= Category::where('title','like','%OMED%')->get();

        $n=0;
        foreach($omed_cat as $omd)
        {
            $count_cent=ServiceProfileCategory::where('category_id','like','%'.$omd->id.'%')->count();
            $n=$n + $count_cent;
        }
        $data['banners']=Media::where('media_category_id',1)->where('status','1')->orderby('id','desc')->get();
    $data['partners'] = Media::where('media_category_id',5)->where('status','1')->orderby('id','desc')->get();
    $data['testimonial'] = Media::where('media_category_id',4)->where('status','1')->orderby('id','desc')->get();
        $data['t_omed_center']=$n;
        $data['t_doc']=ServiceProfile::where('user_role_id','1')->count('id');
        $data['t_sp']=User::where('user_role_id','11')->where('approval','1')->count('id');
        $data['t_patient'] =User::where('user_role_id','3')->count('id');
        $data['t_city'] = ServiceProfile::where('city','!=',' ')->distinct('city')->count('city');
        $data['categories']=$categories;
        $data['config'] = $this->config;
        Session::has('private_access')?Session::forget('private_access'):'';
        Session::has('validate_private')?Session::forget('validate_private'):'';
        Session::has('private_category')?Session::forget('private_category'):'';
        return  view('web.front-end.index',$data);
    }
// Function to load registration view
    public function providerRegistrationView()
    {
        $data['title']=" Service Provider Registration";
        $data['role'] = 11;
        return view('web.register',$data);
    }

    public function patientRegistrationView()
    {
        $data['title']="Patient Registration";
        $data['role'] = 3;
        return view('web.register',$data);
    }

    // Function to load login view
    public function loadLoginView()
    {
        $data['title']=" Login";
        return view('web.login',$data);
    }

    // Function to load about us view
    public function loadAboutUsView()
    {
        $data['title']=" About Us";
        return view('web.about',$data);
    }
// Function to load Our Products view
    public function loadProductView()
    {
        $data['title']=" Our Products";
        return view('web.our_product',$data);
    }

    public function loadGuidelineView()
    {
        $data['title']=" Guidelines on Online Consultation";
        return view('web.guideline',$data);
    }
    // Load Contact Us View
    public function loadContactUsView()
    {
        $data['title']=" Contact Us";
        return view('web.contact',$data);
    }

    // Save Contact Us Form  Posted Data
    public function saveEnquiry(StoreContactUsPost $request)
    {
        $validated = $request->validated();
        // if($validated->fails())
        // {
        //     return redirect()->back()->withErrors($validated)->withInput($request->all());
        // }
        // else {
        $enquiry = new Enquiry;
        $enquiry->name = $request->name;
        $enquiry->email = $request->email;
        $enquiry->mobile =$request->mobile;
        $enquiry->message = $request->message;
        if($enquiry->save())
        {
            $text = "Dear Admin, a visitor (". $enquiry->name. ") has made an inquiry. Regards ".$this->config['sms_regard'].".";
            sms_sender(trim($this->config['admin_mno']), $text,$this->config['sms_patient_enquiry']);
            return redirect()->back()->with('success','Thanks for contacting with us.');
        }
        else {
            return redirect()->back()->with('error','Something went wrong.');
        }

    }

    // save user registration details

    public function registrationPost(UserRegistrationRequest $request)
    {

        // return "fh";
        $validated = $request->validated();
        //   if ($request->mob_otp == Session::get('mob_otp')[0]) {

        $data = $request->all();
        unset($data['c_pass']);
        $data['name'] = ucwords($data['name']);
        $data['password'] = bcrypt($data['password']);
        $create = User::create($data);
        $user = User::where('mobile', $request->mobile)->first();
        if (isset($user->mobile)) {
            Session::put('userName', $user->name);
//                Session::put('customerEmail', $customer->email);
            Session::put('userPhone', $user->mobile);
            Session::put('userId', $user->id);
            if($user->initial_update == 0)
            {
                return redirect('update-profile')->with('success', 'OMEDDr welcomes you. Please complete your profile.');
            }
            else{
                return redirect('dashboard')->with('success', 'OMEDDr welcomes you.');
            }

        }
        // } else {
        //     return redirect()->back()->with('error', 'Invalid OTP');
        // }
    }

// Function for category listings
    public function category($name, $id)
    {
        $data['title']=" ".ucwords($name);
        $data['category'] = ucwords($name);
        // $categories=DB::select("SELECT id, title,image, count(*) as total FROM `categories` where user_role_id like '%$id%' and status=1 group by id,title,image");
        $user_role=UserRole::where('id',$id)->first();
        $data['com_image']=$user_role->cat_img;
        $data['role'] = $id;
//        $categories=DB::select("SELECT id, title,image,general_title FROM `categories` where user_role_id like '%$id%' and status=1 group by id,title,image order by title asc");
        $categories=DB::table('categories')
                        ->select('id','title','image','general_title')
                        ->where('user_role_id',$id)
                        ->where('status',1)
                        ->orderby('title','asc')
                        ->get();
        $data['list'] = $categories;
        return view('web.front-end.sub-category',$data);
    }

    // Validate Private Membership

    public function validatePrivateCategoryMember(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'validate' => 'required|numeric|in:1,0',
            'category' => 'required|numeric',

        ]);
        if($validator->fails())
        {
            return redirect()->back()->with('error','Invalid data passed.');
        }

        if($request->validate==0)
        {
            Session::forget('private_access',true);
            Session::forget('validate_private',false);
            return redirect('/');
        }
        $cat = Category::where('id',$request->category)->first();
        if($request->has('secret_key') && $cat->secret_key == $request->secret_key)
        {
            Session::put('private_access',true);
            Session::put('validate_private',false);
            return redirect()->back()->with('success','Your membership has been verified. Now you have access providers of this category.');
        }
        else{
            return redirect()->back()->with('error','Invalid membership key.');
        }
    }
// Function to get the list of service providers of specific category
    public function serviceProviders(Request $request, $role ,$name, $id='')
    {
        $data['title']=" ".ucwords($name);
        $data['category'] = ucwords($name);
        $data['config'] = $this->config;
        $data['cat_id']=$id;
        $quiries=$request->all();
        $data['states']=State::select('name')->get();
        $categories=UserRole::whereNotIn('id',[3,8,7])->where('status','1')->get();
        $data['categories']=$categories;

        $data['quiries']=$quiries;
        if($id!='') {
            $cat = Category::where('id', $id)->first();
            if ($cat->is_private == 1 && !Session::has('private_access') && !Session::get('private_access')) {
                $data['is_private'] = $cat->is_private;
                $data['private_cat'] = $cat;
                Session::put('private_access', false);
                Session::put('validate_private', true);
            }
        }
        $serv_profile=new ServiceProfile;
        $serv_profile = $serv_profile->newQuery();
        if($role!='')
        {
            $data['search_for'] = $role;
            $serv_profile = $serv_profile->where('user_role_id',$role);
        }
        if($id !='')
        {
            Session::put('category_id',$id);

            $serv_profile= $serv_profile->whereHas('categories',function ($query) use ($id) {
                $query->where('category_id', $id);
            });
        }

        if($request->has('gender') && $request->gender != "All")
        {
            $data['gender'] = $request->gender;
            $serv_profile= $serv_profile->whereHas('user.profile',function ($query) use ($request) {
                $query->where('profiles.gender', $request->input('gender'));
            });
        }
        else
        {
            $data['gender'] = "All";
        }
        if($request->has('live_status') && $request->live_status != "All")
        {
            $data['live_status'] = $request->live_status;
            $serv_profile = $serv_profile->where('mark_live',$request->input('live_status'));
        }
        else
        {
            $data['live_status'] = "All";
        }

        if($request->has('min_fee_from') && $request->has('min_fee_to'))
        {
            $data['min_fee_from'] = $request->min_fee_from;
            $data['min_fee_to'] = $request->min_fee_to;
            $serv_profile = $serv_profile->where('min_fee','>=',$request->min_fee_from)->where('min_fee','<=',$request->min_fee_to);
        }
        if($request->has('max_fee_from') && $request->has('max_fee_to'))
        {
            $data['max_fee_from'] = $request->max_fee_from;
            $data['max_fee_to'] = $request->max_fee_to;
            $serv_profile = $serv_profile->where('max_fee','>=',$request->max_fee_from)->where('max_fee','<=',$request->max_fee_to);
        }
        if($request->has('rating'))
        {
            $data['rating'] =  $request->rating;
            $serv_profile= $serv_profile->whereIn('rating', [$request->input('rating')]);

        }
        $serv_profile = $serv_profile->whereHas('user', function ($query) use ($request) {
            $query->where('users.approval', 1)->whereNotIn('user_role_id',[3,7,8]);
        });

//return $serv_profile->get();
        if($serv_profile->count() > 0)
        {
            $data['list'] =  $serv_profile->paginate('14')->appends($quiries);;
        }
        else{
            $data['search'] = "No record found...";
        }

        return view('web.front-end.listing',$data);
    }

// load serach Page

public function loadSearch(Request $request)
{

    $data['title']="Search";
    $data['config'] = $this->config;
    $data['states']= State::select('name')->get();
    $categories=UserRole::whereNotIn('id',[3,7,8])->where('status','1')->select('id','name')->get();
    $data['categories']=$categories;
//if($request->has('search_for')) {
    $data['search_for'] =  $request->search_for;
//$data['all_category'] = Category::where('user_role_id',$request->search_for)->select('id','title')->get();
    $serv_profile = new ServiceProfile;
    $serv_profile = $serv_profile->newQuery();

    if ($request->has('search_for')) {
        $serv_profile = $serv_profile->where('user_role_id', $request->search_for);
        $data['search_type'] = UserRole::where('id',$request->search_for)->where('status',1)->pluck('name');
    }
    else{
        $serv_profile= $serv_profile->where('user_role_id','=',1)->with('user');
        $serv_profile= $serv_profile->whereHas('categories',function ($query)  {
            $query->where('category_id',14);
        });
        $data['search_type']="Doctors";
    }
    if($request->has('search_field'))
    {
        $data['search_field'] = $request->search_field;
        $serv_profile = $serv_profile->whereHas('user', function ($query) use ($request) {
            $query->where('users.name', 'like', '%' . $request->search_field . '%');
        });
//        $serv_profile= $serv_profile->orWhereHas('categories',function ($query) use ($request)  {
//            $query->whereHas('category',function($q) use ($request){
//                $q->where('categories.title','like', '%' . $request->search_field . '%');
//            });
//        });
    }
    if($request->has('search_loc'))
    {
        $data['search_loc'] = $request->search_loc;
        $serv_profile = $serv_profile->where('state','like','%'. $request->search_loc.'%');
//        $serv_profile = $serv_profile->orWhere('city', 'like','%'.$request->search_loc.'%');
//        $serv_profile = $serv_profile->orWhere('area', 'like','%'.$request->search_loc.'%');
    }
    if($request->has('gender') && $request->gender != "All")
    {
        $data['gender'] = $request->gender;
        $serv_profile= $serv_profile->whereHas('user.profile',function ($query) use ($request) {
            $query->where('profiles.gender', $request->input('gender'));
//                $quiries['gender']=$request->input('gender');
        });
    }
    else
    {
        $data['gender'] = "All";
    }
    if($request->has('live_status') && $request->live_status != "All")
    {
        $data['live_status'] = $request->live_status;
        $serv_profile= $serv_profile->whereHas('user.profile',function ($query) use ($request) {
            $query->where('profiles.live_status', $request->input('live_status'));
//                $quiries['gender']=$request->input('gender');
        });
    }
    else
    {
        $data['live_status'] = "All";
    }

    if($request->has('min_fee_from') && $request->has('min_fee_to'))
    {
        $data['min_fee_from'] = $request->min_fee_from;
        $data['min_fee_to'] = $request->min_fee_to;
        $serv_profile = $serv_profile->where('min_fee','>=',$request->min_fee_from)->where('min_fee','<=',$request->min_fee_to);
    }
    if($request->has('max_fee_from') && $request->has('max_fee_to'))
    {
        $data['max_fee_from'] = $request->max_fee_from;
        $data['max_fee_to'] = $request->max_fee_to;
        $serv_profile = $serv_profile->where('max_fee','>=',$request->max_fee_from)->where('max_fee','<=',$request->max_fee_to);
    }
    if($request->has('rating'))
    {
        $data['rating'] =  $request->rating;
        $serv_profile= $serv_profile->whereIn('rating', [$request->input('rating')]);

    }
    $serv_profile = $serv_profile->whereHas('user', function ($query) use ($request) {
        $query->where('users.approval', 1);
    });

    $quiries = $request->all();
    $data['quiries'] = $quiries;
    $data['total_count'] = $serv_profile->count();
    $serv_profile = $serv_profile->paginate('10')->appends($quiries);

    if (count($serv_profile) > 0) {

        $data['list'] = $serv_profile;
    } else {
        $data['search'] = "No record found...";
    }
//}

    return view('web.front-end.search',$data);
}


    public function searchResult(Request $request)
    {
        $quiries=$request->all();
        $data['title']=" Search Result";
        if(!Session::has('search_field') && isset($request->search_field))
        {
            Session::put('search_field',$request->search_field);
        }
        $serv_profile=new ServiceProfile;
        $serv_profile = $serv_profile->newQuery();
        if($request->has('search_for'))
        {
            $serv_profile= $serv_profile->where('user_role_id',$request->search_for);
        }
        if($request->has('state'))
        {
            $serv_profile= $serv_profile->where('state',$request->state);
        }

        if($request->has('city'))
        {
            $serv_profile=$serv_profile->where('city',$request->city);
        }
        if($request->has('area'))
        {
            $serv_profile = $serv_profile->where('area',$request->area);
        }
          if($request->has('name'))
        {
            $serv_profile = $serv_profile->whereHas('user',function($query) use ($request){
                $query->where('users.name','like','%'.$request->name.'%');
            });
        }
        if($request->has('category'))
        {
                $cat = Category::where('id', $request->category)->first();
                if ($cat->is_private == 1 && !Session::has('private_access') && !Session::get('private_access')) {
                    $data['is_private'] = $cat->is_private;
                    $data['private_cat'] = $cat;
                    Session::put('private_access', false);
                    Session::put('validate_private', true);
                }
            Session::put('category_id',$request->category);
            $id=$request->category;
            $serv_profile= $serv_profile->whereHas('categories',function ($query) use ($id) {
                $query->where('category_id', $id);
            });

        }
        if($request->has('gender') && $request->gender != "All")
        {
            $serv_profile= $serv_profile->whereHas('user.profile',function ($query) use ($request) {
                $query->where('profiles.gender', $request->input('gender'));
//                $quiries['gender']=$request->input('gender');
            });
        }

        if($request->has('minrating') && $request->has('maxrating') && $request->input('minrating')!='undefined' &&
            $request->input('maxrating')!='undefined')
        {
            $serv_profile= $serv_profile->whereHas('user.profile',function ($query) use ($request) {
                $query->whereBetween('profiles.rating', [$request->input('minrating'),$request->input('maxrating')]);
            });
        }
        if($request->has('minprice') && $request->has('maxrprice') && $request->input('minprice')!='undefined' &&
            $request->input('maxprice')!='undefined')
        {
            $serv_profile =  $serv_profile->where('min_fee','>=',$request->input('minprice'))->where('max_fee',
                '<=',$request->input('maxprice'));
        }

        if($serv_profile->count() > 0)
        {
            $data['list'] =$serv_profile->paginate(20)->appends($quiries);
        }
        else{
            $data['search'] = "No record found...";
        }

        $data['quiries']=$quiries;
        return view('web.front-end.listing',$data);

    }
// Show The selected Service Provider Details

    public function showServiceProviderDetail(Request $request ,$id)
    {
        $validator = Validator::make($request->all(),[
            'req_type' => 'nullable|numeric|in:1,2',
        ]);
        if($validator->fails())
        {
            return abort(404,'Data not found');
        }

        $data['title']=" Service Provider Details";
        $data['config'] = $this->config;
        $provider=ServiceProfile::where('id',$id)->with('user')->first();
        if(empty($provider))
        {
            return redirect()->back()->with('error','No provider were found');
        }
        $data['radio_check'] = $request->has('req_type') ? $request->req_type:'';
        if(!empty($data['radio_check']) && $data['radio_check']==1)
        {
            $data['today_consultation'] = LiveConsultation::where('provider_id',$provider->user_id)
                ->where('booking_date',today())
                ->where('request_status','3')
                ->count();
        }
        if(!empty($data['radio_check']) && $data['radio_check']==2)
        {
            $today_schedule = BookingSchedule::where('user_id', $provider->user_id)->where('date', date('Y-m-d'))->first();
            if (!empty($today_schedule)) {
                $data['today_schedule'] = $today_schedule;
            } else {
                $data['schedule'] = "No schedule found for today.";
            }
            $data['today_booking'] = AppointmentBooking::where('provider_id', $provider->user_id)
                ->where('booking_date', date('Y-m-d'))
                ->count();
        }

        $data['detail']=$provider;
//        $events = [];
//        $schedule = Event::where('user_id',$data['detail']->user_id)->get();
//        if($schedule->count())
//        {
//            foreach ($schedule as $key => $value)
//            {
//                $events[] = Calendar::event(
//                    $value->title,
//                    true,
//                    new \DateTime($value->start_date),
//                    new \DateTime($value->end_date.'+1 day'),
//                    null,
//                    // Add color
//                    [
//                        'color' => $value->color,
//                        'textColor' => $value->text_color,
//                    ]
//                );
//            }
//        }
//        $data['calendar'] = Calendar::addEvents($events)->setOptions([
//            'headerToolbar' => [
//                'left' => 'prev,next',
//                'center' => 'title',
//                'right' => ''
//            ],
//        ]);
        return view('web.front-end.detail',$data);
    }

// Get The Service Provider By IT's Selected Category
    public function getProviderByCategory($cat_id,$category,$user_id)
    {
        $sp=ServiceProfile::where('user_id',$user_id)->where('category_id','like','%'.$cat_id.'%')->first();
// return redirect()->action('Homecontroller@showServiceProviderDetail',['cat_id'=>$cat_id,'$category'=>$category,'id'=>$sp->id]);
        return redirect()->to('detail/'.$sp->id);
    }
// Load Lab Services View
    public function labServices()
    {
        $data['title'] = " Lab Services";
        return view('web.lab-services',$data);
    }

// Load Medical Product View
    public function medicalProduct()
    {
        $data['title'] = " Medical Product";
        return view('web.medical-product', $data);
    }

// Load Checkup Package View
    public function  checkPackage()
    {
        $data['title'] =" Check Package";
        return view('web.checkup-package',$data);
    }

// Load Immunization-Services view
    public function immunizationServices()
    {
        $data['title'] =" Immunization Services";
        return view('web.immunization-services',$data);
    }

// Laod Doctor Appoinment View
    public function doctorAppointment()
    {
        $data['title'] = " Doctor Appoinment";
        return view('web.doctor-appointment',$data);
    }

// Load Gallary View
    public function gallery()
    {
        $data['title']= " Gallery";
        $data['list'] = Gallery::where('status','1')->get();
        return view('web.gallery',$data);
    }


//Load privacy & Policy Page
    public function privacy()
    {
        $data['title']= " Privacy & Policy";
        return view('web.privacy-policy',$data);
    }
// Send OTP
    public function sendOTP(Request $request)
    {
        if($request->ajax())
        {
            $data=array('status'=>false, 'message'=>'');
            if(!is_numeric($request->phone))
            {
                $data['message']="Mobile no. should be only numbers.";
            }
            elseif(strlen($request->phone)!=10)
            {
                $data['message']="Mobile no. must be 10 digits.";
            }
            else {
                if (!empty(Session::get('mob_otp')[0])) {
                    $mob_otp = Session::get('mob_otp')[0];
                } else {
                    $mob_otp = rand(1000, 9999);
                    Session::forget('mob_otp');
                    Session::put('mob_otp', [$mob_otp]);
                }

                $text = "Dear User, your otp for mobile verification is $mob_otp. Regards OMEDDr.";
               sms_sender(trim($request->phone), $text,$this->config['sms_common_otp']);
                $data['status']=true;
            }

            return response()->json($data);
        }
    }

// Validate OTP
    public function checkOTP(Request $request)
    {
        if($request->ajax())
        {
            $data=array('status'=>false, 'message'=>'');
            if ($request->mob_otp != '') {
                if ($request->mob_otp == Session::get('mob_otp')[0]) {
                    Session::forget('mob_otp');
                    $data['status']=true;
                } else {
                    $data['message']="Invalid OTP";
                }
            } else {
                $data['message']="Please enter a valid OTP.";
            }
            return response()->json($data);

        }
    }

// Save Questions asked by the patient
    public function savePatientEnquiry(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'provider_id' => 'required|numeric',
            'question' =>'required|string',
            'patient_doc' =>'array|min:1',
            'patient_doc.*' => 'image|mimes:jpg,jpeg,png',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong.');
        }
        if(Auth::check() && Auth::user()->user_role_id==3)
        {
            $frd='';
            $destinationPath='';
            if(!empty($request->file('patient_doc')))
            {

                $mno=Auth::user()->mobile;
                $directory=$mno.'_'.time();
                $def_path=public_path('uploads/patientprofile/enquiry/');
                if(!is_dir($def_path.$directory))
                {
                    if(mkdir($def_path.$directory,0777))
                    {
                        $destinationPath=$def_path.$directory;
                    }
                }


                for($j=0;$j<count($request->patient_doc);$j++)
                {
                    $photo=$request->file('patient_doc')[$j];
                    $imagename=rand() . '.' . $photo->getClientOriginalExtension();
                    $thumb_img = Image::make($photo->getRealPath())->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumb_img->save($destinationPath.'/'.$imagename,80);
                    if($frd!='')
                    {
                        $frd=$frd.",".$imagename;
                    }
                    else
                    {
                        $frd=$imagename;
                    }

                }
            }

            $enquiry = new PatientEnquiry;
            $enquiry->patient_id = Auth::user()->id;
            $enquiry->provider_id = $request->provider_id;
            $enquiry->question =$request->question;
        if($destinationPath!='' && $frd!=' ')
        {
            $enquiry->pat_directory =$directory;
            $enquiry->pat_files =$frd;
        }
            if($enquiry->save())
            {
                $provider=User::where('id',$request->provider_id)->first();
                $text = "Dear User, a user (". Auth::user()->name. ") has made following an inquiry. Regards OMEDDr.";
                sms_sender(trim($provider->mobile), $text,$this->config['sms_patient_enquiry']);
                return redirect()->back()->with('success','Thanks for asking. Service provider will respond you soon.');
            }
        }
        else{
            return redirect('patient/login')->with('error','You need to login first.');
        }
    }

    public function loadBookingView($id)
    {
            if(Auth::check() ) {
                if (Auth::user()->user_role_id == 3) {
                    $provider = ServiceProfile::where('id',$id)->first();
                    $data['title'] = "  Appoinment Booking";
                    $today_schedule = BookingSchedule::where('user_id', $provider->user_id)->where('date', date('Y-m-d'))->first();
                    if (!empty($today_schedule)) {
                        $data['today_schedule'] = $today_schedule;

                    } else {
                        $data['schedule'] = "No schedule found for today.";
                    }
                    $data['today_booking'] = AppointmentBooking::where('provider_id', $provider->user_id)->where('booking_date', date('Y-m-d'))->count();
                    $events = [];
                    $schedule = Event::where('user_id', $provider->user_id)->get();
                    if ($schedule->count()) {
                        foreach ($schedule as $key => $value) {
                            $events[] = Calendar::event(
                                $value->title,
                                true,
                                new \DateTime($value->start_date),
                                new \DateTime($value->end_date . '+1 day'),
                                null,
                                // Add color
                                [
                                    'color' => $value->color,
                                    'textColor' => $value->text_color,
                                ]
                            );
                        }
                    }
                    $data['provider'] = $provider->user_id;
                    $data['service_profile'] =  $id;
                    $data['appointment_fee'] = $this->config['appointment_fee'];
                    $data['calendar'] = Calendar::addEvents($events);
                    return view('web.appoinment_booking', $data);

                } else {
                    return redirect()->back()->with('error', 'Invalid user access found.');
                }
            }
    else{
        return redirect('/login')->with('error','You need to login first.');
    }
    }

// Save Patient's Booking Request
public function saveBookingRequest(Request $request)
{
    $validator = Validator::make($request->all(), [
        'provider_id' => 'required|numeric',
        'patient_name' =>'required|string',
        'service_profile_id' =>'required|numeric',
        'age' =>'required|string',
        'gender' => 'required|string',
        'booking_date' => 'required|date',
        'address' => 'required|string',
        'mobile' => 'nullable|numeric',
        'vital' => 'nullable|string',
        'complain' => 'nullable|string',
        'diagnosis' => 'nullable|string',
    ]);
    if ($validator->fails()) {
        return redirect()->back()->with('error','Something went wrong.')->withInput()->withErrors($validator);
    }
//    if(Auth::check() )
//    {
        if(Auth::user()->user_role_id==3)
        {
            $t_book = AppointmentBooking::where('provider_id',$request->provider_id)->where('booking_date',date('Y-m-d',strtotime($request->booking_date)))->count();
            $today_schedule = BookingSchedule::where('user_id',$request->provider_id)->where('date',date('Y-m-d',strtotime($request->booking_date)))->first();
            if(!empty($today_schedule))
            {
                if($t_book > 0 && $t_book >= $today_schedule->total_bookings)
                {
                    return redirect()->back()->with('error','Bookings are full for the selected date.');
                }

            }
            else{
                return redirect()->back()->with('error','No booking schedule is found for the selected date.');
            }
$serviceProfile= ServiceProfile::where('id',$request->service_profile_id)->first();
            $booking = new Booking;
            $booking->provider_id = $request->provider_id;
            $booking->service_profile_id = $request->service_profile_id;
            $booking->patient_id  = Auth::user()->id;
            $booking->patient_name =$request->patient_name;
            $booking->age = $request->age;
            $booking->gender  =$request->gender;
            $booking->address =$request->address;
            $booking->mobile = $request->mobile;
            $booking->vital =$request->vital;
            $booking->complain =$request->complain;
            $booking->diagnosis= $request->diagnosis;
            $booking->booking_date = $request->booking_date;
            $booking->fee_to_pay = $serviceProfile->min_fee - (($serviceProfile->min_fee*$this->config['appointment_discount'])/100);
             if($booking->save())
            {
               return $this->payAppointmentFee($request->getClientIp(),$booking);
//                return redirect()->back()->with('success','Your appoint booking have been successfully done.');
            }
            else{
                return redirect()->back()->with('errors','Unable to book appointment.');
            }
         }
        else{
            return redirect()->back()->with('error','Invalid user access found.');
        }

//    else{
//        return redirect()->back()->with('error','You need to login first.');
//    }
}

public function appointmentFeePayment(Request $request,$id)
{
    $booking = AppointmentBooking::where('id',$id)->first();
    return $this->payAppointmentFee($request->getClientIp(),$booking);
}
private function payAppointmentFee($ip,$booking)
{
    $data=array();
    $data['orderId'] = Str::random(15);
    $data['customerName']=$booking->patient->name;
    $data['customerEmail'] = $booking->patient->email;
    $data['customerPhone'] = $booking->patient->mobile;
    $data['orderAmount'] = $this->config['appointment_fee'];
    $data['orderNote'] = "Appointment Booking Fee";
    $data['return'] = '/cf-appointment/return';
    $data['notify'] = '/cf-appointment/notify';
    $orderDetails = payNow($data);

    $data['title'] =" Pay Now";
    $data['orderDetails'] =$orderDetails;
    $update=array();
    $update['temp_txn_number'] =$data['orderId'];
//    $update['appointment_fee'] = $data['orderAmount'];
//    $update['who_pay'] = $ip;
    if($booking->update($update))
    {
        return view('web.payment.appointment_checkout',$data);
    }
}
// Check Booking Schedule
public function checkBookingSchedule(Request $request)
{
$chk_booking_schedule=BookingSchedule::where('user_id',$request->provider)
                                        ->where('date',date('Y-m-d',strtotime($request->date)))
                                        ->count();
if($chk_booking_schedule > 0)
{
    $chk_booking_schedule=BookingSchedule::where('user_id',$request->provider)
                                            ->where('date',date('Y-m-d',strtotime($request->date)))
                                            ->first();
    $allowed_booking=$chk_booking_schedule->total_bookings;
    $t_book = AppointmentBooking::where('provider_id',$request->provider)
                        ->where('booking_date',date('Y-m-d',strtotime($request->date)))
                        ->count();
    $timing= $chk_booking_schedule->event->timings;
    return response()->json(['allowed_booking'=>$allowed_booking,'total_booked'=>$t_book,'schedule'=>$timing,'success'=>true]);
}
else
{
    return response()->json(['message'=>'No booking schedule available for selected date.','success'=>false]);
}
}

// Payment response for Appointment Booking
    public function notifyPaymentRequest(Request $request)
    {

    }


    public function returnPaymentResponse(Request $request)
    {

            $response= $request->all();
        $detail= $request->all();
        $booking= AppointmentBooking::where('temp_txn_number',$detail['orderId'])->first();
        $update=array();
        $update['transaction_status'] =$detail['txStatus'];
        $update['txn_amount'] = $detail['orderAmount'];
        $update['txn_date'] =$detail['txTime'];
        $update['txn_response'] = $detail['txStatus'];
        $update['txn_number'] = $detail['orderId'];
        $update['is_paid'] = 1;
        $update['referenceId'] = $detail['referenceId'];
        $update['payment_mode'] = $detail['paymentMode'];
        $update['booking_number'] =  mt_rand();

        if(validateSignature($detail))
        {
            if($detail['txStatus'] == "SUCCESS")
            {
                 if($booking->update($update))
                    {
                        $update['title'] = " Payment Response";
                        $update['booking_id'] = $booking->id;
                        $text = "Dear User, appointment has been booked. Your booking number is ".$update['booking_number'].". Regards OMEDDr.";
                        $text2 = "Dear User, a patient has booked an appointment with booking no ".$update['booking_number'].". Regards OMEDDr.";
                        sms_sender(trim($booking->patient->mobile), $text,$this->config['sms_pat_appointment_book']);
                        sms_sender(trim($booking->provider->mobile), $text2,$this->config['sms_provider_appointment']);
                        return view('web.payment.appointment_payment_response',$update)->with('success','Payment done successfully.');
                    }
                    else
                    {
                        return view('web.payment.appointment_payment_response',$update)->with('error','Unable to proceed.');

                    }
            }
            else
            {
                return view('web.payment.appointment_payment_response',$update)->with('error','Your transaction was not successful.');
            }

        }
        else{
            $update['signature_mismatch'] = true;
            return view('web.payment.appointment_payment_response',$update)->with('error','Invalid payment signature found.');
        }
    }

    public function passwordResetViaOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|numeric',
            'password' =>'required',
            'password_confirmation' => 'required|same:password'
        ]);
        if ($validator->fails()) {
           return redirect()->back()->with('error','Invalid data submition')->withErrors($validator->errors())->withInput();
        }
        $user = User::where('mobile',$request->mobile)->first();
        if($user!='')
        {
            $user->password = bcrypt($request->password);
            if($user->save())
            {
                return redirect('/login')->with('success','Password has been reset.');
            }
        }
        else{
            return redirect()->back()->with('error','Invalid mobile number entered');
        }
    }
}



