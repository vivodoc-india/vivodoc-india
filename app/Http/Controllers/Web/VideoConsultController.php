<?php

namespace App\Http\Controllers\Web;

//use App\AshaDeviceData;
use App\Models\LiveConsultation;
use App\Models\Setting;
use App\Models\WalletTransaction;
use App\Service\AppNotificationService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserRole;
use App\Models\User;
use Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\ServiceProfile;
use Illuminate\Support\Facades\Auth;
use Image;
use File;
use App\Models\Profile;


class VideoConsultController extends Controller
{
    private $config;

    public function __construct()
    {
        $setting = Setting::all();

        foreach ($setting as $data)
            $this->config[$data->param] = $data->data;
    }
public function loadPatientView($id)
{
    $data['title'] = "Consult Now";
    $provider = ServiceProfile::where('id',$id)->first();
    $live= $provider->mark_live;
    $data['today_consultation'] = LiveConsultation::where('provider_id',$provider->user_id)
                                                    ->where('booking_date',today())
                                                    ->where('request_status','3')
                                                    ->count();
//    $live= $provider->user->profile->live_status;
    if($live == 0)
    {
        return redirect()->back()->with('error','Service Provider is offline now.');
    }
    $data['help_support'] = $this->config['help_support'];
    $data['provider'] = $provider;
//    $data['vitals'] = AshaDeviceData::where('patient_id',Auth::user()->puid)->orderby('id','desc')->first();
//$data['booking'] = LiveConsultation::where('service_profile_id',$id)
//                                ->where('provider_id',$provider->user->id)
//                                ->where('patient_id',Auth::user()->id)
//                                ->where('booking_date',date('Y-m-d'))->first();
    return view('web.patient.video-consultation-form',$data);
}

// Edit Consultation View
    public function editConsultationView($id)
    {
    $booking = LiveConsultation::where('id',$id)->where('booking_date',date('Y-m-d'))->first();

      if($booking!='')
      {
          $today_consultation = LiveConsultation::where('provider_id',$booking->provider_id)
                                                            ->where('booking_date',today())
                                                            ->where('request_status','3')
                                                            ->select('id')
                                                            ->orderBy('id','asc')
                                                            ->get();
       $n=1;
       foreach($today_consultation as $tcon)
       {
           if($tcon->id===$booking->id && $booking->request_status=='3')
           {
               $data['mynumber'] = $n;
           }
           else{
               $n++;
           }
       }
          $data['today_consultation']=$n;
          $data['title'] = "Consult Now";
//          $provider = ServiceProfile::where('user_id',$booking->provider_id)->where('category_id','like','%'.$booking->service_profile_id.'%')->first();
          $provider = ServiceProfile::where('user_id',$booking->provider_id)
                                    ->where('id',$booking->service_profile_id)
                                    ->first();
//          $live= $provider->user->profile->live_status;
          $live= $provider->mark_live;
          $data['help_support'] = $this->config['help_support'];
          if($live == 0)
          {
              return redirect()->back()->with('error','Service Provider is offline now.');

          }
          $data['provider'] = $provider;
//          return view('web.patient.webrtc.index',$data);
          $is_page_refreshed = (isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] == 'max-age=0');
    if($is_page_refreshed ) {
        $booking->con_link='';
        $booking->caller=null;
        $booking->save();
    }
          $data['booking'] = $booking;
          return view('web.patient.receipt-form',$data);
      }
      else{
          return redirect()->back()->with('error','Data is not accessible.');
      }



    }
// Send Consultation Request
public function sendConsultationRequest(Request $request)
{
        $validator = Validator::make($request->all(), [
            'patient_name' =>'required|string',
            'age' => 'required|string',
            'gender' => 'required|string',
            'address' => 'required|string',
            'mobile' => 'nullable|numeric|digits:10',
            'bp' => 'nullable|string',
            'pulse' => 'nullable|string',
            'temperature' => 'nullable|string',
            'spo' => 'nullable|string',
            'weight' =>'nullable|string',
            'history' => 'nullable|string',
            'complain' => 'nullable|string',
            'omed_code' => 'nullable|string',
            'bike_dr' => 'nullable|string',
            'provider_id' => 'required|numeric',
            'service_profile_id' => 'required|numeric',
//            'patient_id' => 'required|numeric',
            'category_id' => 'nullable|numeric',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong.')->withErrors($validator->errors())->withInput();

//            return response()->json(['success'=>'false','message'=>'Something went wrong.','validation'=>$validator->errors()]);

        }
$is_live=ServiceProfile::where('id',$request->service_profile_id)->first();
        if($is_live->mark_live)
        {
            $input = $request->except(['_token','request_for']);
            $input['patient_id']=auth()->user()->id;
            $chk_appoinment=LiveConsultation::where('provider_id',$request->provider_id)
                                    ->where('service_profile_id',$request->service_profile_id)
                                    ->where('patient_id',auth()->user()->id)
                                    ->where('patient_name',$request->patient_name)
                                    ->where('request_status','<=','3')
                                    ->where('booking_date',date('Y-m-d'))->get();
if(count($chk_appoinment) > 0)
{
    return redirect()->back()->with('error','You have already a active request on today which is not completed.')->withInput();
}
else{
$input['booking_date'] = date('Y-m-d');
   $online_book= LiveConsultation::create($input);
if($online_book)
{
    $online_book->update(['patient_requested'=>1]);
            if($online_book->serviceProfile->user_role_id==9) {
                $book['con_link_access'] = 1;
                $book['transaction_status'] = 'SUCCESS';
                $book['txn_amount'] = 0;
                $book['txn_date'] = date('Y-m-d H:i:s');
                $book['txn_response'] = 'Service is free for this category.';
                $book['txn_number'] = 0;
                $book['request_status'] = '3';
                $book['referenceId'] = 'NA';
                $book['is_con_link_available'] = 1;
                $online_book->update($book);
                if($online_book->patient->device_id!='')
                {
                    $message = array(
                        "title" => "Live Consultation",
                        "body" => "You can now consult with the provider (".$online_book->provider->name.")",
                    );
//                    AppNotificationService::sendSingleUserNotification($online_book->patient->device_id, $message);
                }
                if($online_book->provider->device_id!='')
                {
                    $message = array(
                        "title" => "Live Consultation",
                        "body" => "Your video consultation link from the patient( ".$online_book->patient_name." ) has been activated",
                    );
//                     AppNotificationService::sendSingleUserNotification($online_book->provider->device_id, $message);
                }
            }

//}
    $text= "Dear User, A patient ".$online_book->patient->name." sent you request for video consultation. Regards OMEDDr.";
    sms_sender(trim($online_book->provider->mobile), $text,$this->config['sms_patient_request']);
    return redirect('patient/review-consultation-request/'.$online_book->id)->with('success','Request sent. Pay the consultation fee.');
        }
        else
        {
           return response()->json(['success'=>'false','message'=>'Service Provider is not live.']);
        }
}
        }
}

// Check Response Status Of Service Provider For Online Consultation
public function checkResponseStatus(Request $request)
{
    if($request->ajax() && $request->isMethod('post')){
        $validator = Validator::make($request->all(), [
        'res_code' =>'required|numeric',
            'uid' => 'required|numeric'
            ]);

        if ($validator->fails()) {
            return response()->json(['success'=>'false','message'=>'Something went wrong.','stop'=>'true']);
        }
        $stat= LiveConsultation::where('id',$request->res_code)->where('patient_id',$request->uid)->first();
        if($stat->is_request_accepted && $stat->is_paid==0)
        {
            return response()->json(['success'=>'true','message'=>'Service Provider accepted your request.']);

        }
        elseif($stat->is_request_accepted && $stat->is_paid==1)
        {
            return response()->json(['success'=>'false','message'=>'Service Provider accepted your request.','stop'=>'true']);

        }
        elseif($stat->is_request_rejected){
            return response()->json(['success'=>'false','message'=>'Service Provider reject your request.','stop'=>'true']);
        }
        else{
            return response()->json(['success'=>'false','res_code'=>$request->res_code]);
        }
    }
}
// initiate Call By Patient
public function initiateCallByPatient(Request $request)
{
    if($request->ajax() && $request->isMethod('post')){
        $validator = Validator::make($request->all(), [
            'res_code' =>'required|numeric',
            'ident' => 'required|numeric',
            'caller' => 'required|numeric',
            'id' => 'required|string',
            ]);

        if ($validator->fails()) {
            return response()->json(['success'=>'false','message'=>'Something went wrong.']);
        }
        $stat= LiveConsultation::where('id',$request->res_code)->where('patient_id',$request->ident)->first();
        if($stat->request_status >= 3)
        {
            if($stat->caller!='' && $stat->caller!=auth()->user()->id)
            {
                return response()->json(['success'=>'true','roomExists'=>'true','id'=>$stat->con_link]);
            }
            else{
                $stat->con_link = $request->id;
                $stat->caller = $request->caller;
                if($stat->save())
                    return response()->json(['success'=>'true','roomExists'=>'false','message'=>'Call initiated.']);
            }
        }

        else{
            return response()->json(['success'=>'false','res_code'=>$request->res_code]);
        }
    }
}
public function removeCallByPatient(Request $request)
{
    if($request->ajax() && $request->isMethod('post')){
        $validator = Validator::make($request->all(), [
            'res_code' =>'required|numeric',
            'ident' => 'required|numeric',
            ]);

        if ($validator->fails()) {
            return response()->json(['success'=>'false','message'=>'Something went wrong.']);
        }
        $stat= LiveConsultation::where('id',$request->res_code)->where('patient_id',$request->ident)->first();
        if($stat->request_status >= 3)
        {
            $stat->con_link = '';
            $stat->caller = null;
            if($stat->save())
            {
                return response()->json(['success'=>'true','message'=>'Call Closed.']);
            }
        }

        else{
            return response()->json(['success'=>'false','res_code'=>$request->res_code]);
        }
    }
}

// Check if prescription is uploaded or not
public function checkPrescription(Request $request){
    if($request->ajax() && $request->isMethod('post')){
        $validator = Validator::make($request->all(), [
            'res_code' =>'required|numeric',
            'uid' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['success'=>'false','message'=>'Something went wrong.','stop'=>'true']);
        }
        $stat= LiveConsultation::where('id',$request->res_code)->where('patient_id',$request->uid)->first();
        if($stat->prescription!='' || $stat->prescription_image!='')
        {
            return response()->json(['success'=>true,'stop'=>true]);
        }

    }
}
// Function to pay live consultation fee
public function payConsultationFee(Request $request)
{
    if(!$request->has('booking'))
    {
        return redirect()->back()->with('error','Invalid request found.');
    }
if(Session::has('previous_consultation'))
{
    Session::forget('previous_consultation');
    Session::put('previous_consultation',URL::previous());
}
    $id=base64_decode($request->booking);
    $booking= LiveConsultation::where('id',$id)->where('booking_date',date('Y-m-d'))->first();
    if($booking->patient_id!= auth()->user()->id)
    {
        return redirect()->back()->with('error','Logged In user does not belongs to this live consultation booking.');
    }
    if($booking->serviceProfile->mark_live == 0)
    {
        return redirect()->back()->with('error','The service provider goes offline now.Pay latter for this request or consult with other service provider');
    }
    if( (int) $booking->request_status >= 3)
    {
        return redirect()->back()->with('error','You have already paid for this request.');
    }
    $orderAmount = $booking->serviceProfile->min_fee;
    $wallet_pay =0;
    $wallet_deduction = 0;
        if($request->has('wallet_pay') && $request->wallet_pay==1 && auth()->user()->wallet_amount > 0 && auth()->user()->wallet_amount > $orderAmount)
        {
            $wallet_pay=1;
            $wallet_deduction=$orderAmount;

            $update['cons_amount'] = $orderAmount;
            $update['who_pay'] = $request->getClientIp();
            $update['wallet_pay'] = $wallet_pay;
            $update['wallet_deduction']= $wallet_deduction;
            $update['con_link_access']=1;
            $update['transaction_status'] ='SUCCESS';
            $update['txn_amount'] = $wallet_deduction;
            $update['txn_date'] =date('Y-m-d');
            $update['txn_response'] = "Wallet Payment";
            $update['txn_number'] = Str::random(15);
            $update['request_status'] = '3';
            $update['referenceId'] = 'NA';
            $update['payment_mode'] = 'OMEDDr_WALLET';
            $update['is_con_link_available']=1;
            if($booking->update($update))
            {
                $wallet['user_id'] = auth()->user()->id;
                $wallet['transaction_number'] = $update['txn_number'];
                $wallet['transaction_status'] = $update['transaction_status'];
                $wallet['transaction_date'] =  $update['txn_date'];
                $wallet['transaction_status'] =$update['transaction_status'];
                $wallet['transaction_response'] = $update['txn_response'];
                $wallet['transaction_amount'] = $update['txn_amount'];
                $wallet['payment_mode'] = $update['payment_mode'];
                $wallet['referenceId'] =  $update['referenceId'];
                $wallet['who_pay'] =   $update['who_pay'];
                $wallet['transaction_type'] =   "debit";
                if(WalletTransaction::create($wallet))
                {
                    $newWalletAmount = auth()->user()->wallet_amount - $wallet_deduction;
                    User::where('id',auth()->user()->id)->update(['wallet_amount'=>$newWalletAmount]);
                }
                $text = "Dear User,your video consultation link for recently accepted request has been activated. Regards OMEDDr.";
                sms_sender(trim($booking->provider->mobile), $text,$this->config['sms_active_video_link']);
                return redirect()->back()->with('success','Payment done successfully.You can join the consultation.');
            }
        }
        if($request->has('wallet_pay') && $request->wallet_pay==1 && auth()->user()->wallet_amount > 0 && auth()->user()->wallet_amount < $orderAmount){
        $wallet_pay=1;
        $wallet_deduction=auth()->user()->wallet_amount;
        $orderAmount =  $orderAmount - auth()->user()->wallet_amount;
        }

    $data=array();
    $data['orderId'] = Str::random(15);
    $data['customerName']=$booking->patient->name;
    $data['customerEmail'] = $booking->patient->email!=''?$booking->patient->email:$this->config['master_email'];
    $data['customerPhone'] = $booking->patient->mobile;
   $data['orderAmount'] = $orderAmount;
   $data['notify'] = '/cf-payment/notify';
    $data['return'] = '/cf-payment/return';
 $data['orderNote'] = "Online Consultation Fee";
   $orderDetails = payNow($data);
        $data['title'] ="Pay Now";
    $data['orderDetails'] =$orderDetails;
$update=array();
$update['temp_txn_number'] =$data['orderId'];
$update['cons_amount'] = $data['orderAmount'];
$update['who_pay'] = $request->getClientIp();
$update['wallet_pay'] = $wallet_pay;
$update['wallet_deduction']= $wallet_deduction;
if($booking->update($update))
{
    return view('web.front-end.checkout',$data);
}
}
public function notifyPaymentRequest(Request $request)
{

}
public function returnPaymentResponse(Request $request)
{
    $detail= $request->all();
    $booking= LiveConsultation::where('temp_txn_number',$detail['orderId'])->first();
$book['booking'] = $booking;
    if(validateSignature($detail))
    {
        if($detail['txStatus'] == "SUCCESS" || $detail['txStatus'] == "PENDING")
        {
            $update=array();
            $update['con_link_access']=1;
            $update['transaction_status'] =$detail['txStatus'];
            $update['txn_amount'] = $detail['orderAmount'];
            $update['txn_date'] =$detail['txTime'];
            $update['txn_response'] = $detail['txStatus'];
            $update['txn_number'] = $detail['orderId'];
            $update['request_status'] = '3';
            $update['referenceId'] = $detail['referenceId'];
            $update['is_con_link_available']=1;
                if($booking->update($update))
                {
                    if($booking->wallet_pay==1)
                    {
                        $wallet['user_id'] = $booking->patient_id;
                        $wallet['transaction_number'] = $update['txn_number'];
                        $wallet['transaction_status'] = $update['transaction_status'];
                        $wallet['transaction_date'] =  $update['txn_date'];
                        $wallet['transaction_status'] =$update['transaction_status'];
                        $wallet['transaction_response'] = $update['txn_response'];
                        $wallet['transaction_amount'] = $update['txn_amount'];
                        $wallet['payment_mode'] = 'OMEDDr_WALLET';
                        $wallet['referenceId'] =  $update['referenceId'];
                        $wallet['who_pay'] =   $booking->who_pay;
                        $wallet['transaction_type'] =   "debit";
                        if(WalletTransaction::create($wallet))
                        {
                            $newWalletAmount = $booking->patient->wallet_amount - $booking->wallet_deduction;
                            User::where('id',$booking->patient_id)->update(['wallet_amount'=>$newWalletAmount]);
                        }
                    }
                    $update['title'] = "Payment Response";
                    $update['booking_id'] = $booking->id;
                    $text = "Dear User,your video consultation link for recently accepted request has been activated. Regards OMEDDr.";
                    sms_sender(trim($booking->provider->mobile), $text,$this->config['sms_active_video_link']);
                    return view('web.front-end.payment_response',$update)->with('success','Payment done successfully.You can join the consultation.');
                }
                else
                {
                    return view('web.front-end.payment_response',$update)->with('error','Unable to proceed');
                }
        }
        else
        {
            return view('web.front-end.payment_response',$detail,$book)->with('error','Your transaction was not successful.');
        }
    }
    else{
        return view('web.front-end.payment_response',$detail,$book)->with('error','Invalid payment signature found.');
    }
}

/*
 * Patient's Dashboard section Start here
 */

public function getConsultationList()
{
    if(Auth::user()->user_role_id!=3)
    {
        return redirect('/login');
    }

    $data['title'] = "Live Consultation";
    $data['details']= LiveConsultation::where('patient_id',Auth::user()->id)->orderby('id','desc')->get();
    return view('web.patient.consultation-list',$data);

}

//get the selected consultation result

public function getConsultationResult($id)
{
    if(Auth::user()->user_role_id!=3)
    {
        return redirect('/login');
    }

    $data['title'] = "Live Consultation Response";
    $data['booking']= LiveConsultation::where('id',$id)->first();
    return view('web.patient.consultation-response',$data);
}

// Print Prescription
public function printPrescription($id)
{
    if(Auth::user()->user_role_id!=3)
    {
        return redirect('/login');
    }
    $data['title'] = "Print Prescription";
    $check= LiveConsultation::where('id',$id)->first();
    if($check =='')
    {
        $data['title'] = '404';
        $data['message'] = 'Page not found';
        return response()
            ->view('web.404',$data,404);
    }
    if($check->patient_id == Auth::user()->id)
    {
      $data['booking'] = $check;
    }
    else{
        return redirect('/')->with('error','Invalid data access.');
    }
    return view('web.patient.print-response',$data);
}
}
