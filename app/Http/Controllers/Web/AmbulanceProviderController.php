<?php

namespace App\Http\Controllers\Web;

use App\Models\AmbulanceBooking;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
class AmbulanceProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $config;

    public function __construct()
    {
        $setting = Setting::all();
        foreach ($setting as $data)
            $this->config[$data->param] = $data->data;
    }
    public function index()
    {
        $data['title'] = ' Ambulance Bookings';
        $data['details'] = AmbulanceBooking::where('provider_id',Auth::user()->id)->orderby('id','desc')->get();
        return view('web.provider.ambulance-booking-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['booking']= AmbulanceBooking::findOrFail($id);
        $data['title'] = ' Edit Ambulance Booking Request';
        return view('web.provider.ambulance-booking-detail',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateRequestResponse(Request $request)
    {
        $validator=Validator::make($request->all(),[
        'id'=>'bail|required|numeric',
            'response_state'=>'required|string|in:accepted,rejected,completed',
            'approx_fair' =>'required_if:response_state,==,accepted|numeric',
            'advance_payment' =>'required_if:response_state,==,accepted|numeric',
            'remarks' => 'required_if:response_state,==,rejected|required_if:response_state,==,completed|string',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->with('error',$validator->errors())->withInput();
        }

        if($request->response_state=="accepted"){
            if($this->isRejected($request->id) || $this->isCompleted($request->id) || $this->isAccepted($request->id)){
                return redirect()->back()->with('error','Request can not be process.');
            }
            $ambulance = AmbulanceBooking::where('id',$request->id)->first();
            $ambulance->is_request_accepted = 1;
            $ambulance->approx_fair = $request->approx_fair;
            $ambulance->advance_payment = $request->advance_payment;
            if($ambulance->save())
            {
                $text = "Dear User, your request has been accepted by the service provider. Regards OMEDDr.";
                sms_sender(trim($ambulance->patient->mobile), $text,$this->config['sms_provider_request']);
                return redirect()->back()->with('success','Request accepted.');
            }
            else{
                return redirect()->back()->with('error','Unable to process request.');
            }
        }
        elseif($request->response_state=="rejected"){
            if($this->isAccepted($request->id) || $this->isCompleted($request->id) || $this->isRejected($request->id)){
                return redirect()->back()->with('error','Request can not be process.');
            }
            $ambulance = AmbulanceBooking::where('id',$request->id)->first();
            $ambulance->is_request_rejected = 1;
            $ambulance->is_completed = 1;
            $ambulance->remarks = $request->remarks;
            if($ambulance->save())
            {
                $text = "Dear User, your request has been rejected by the service provider. Regards OMEDDr.";
                sms_sender(trim($ambulance->patient->mobile), $text,$this->config['sms_provider_request']);
                return redirect()->back()->with('success','Request rejected.');
            }
            else{
                return redirect()->back()->with('error','Unable to process request.');
            }
        }
        elseif ($request->response_state=="completed"){
            if( $this->isCompleted($request->id)){
                return redirect()->back()->with('error','Request can not be process.');
            }
            $ambulance = AmbulanceBooking::where('id',$request->id)->first();
            $ambulance->is_completed = 1;
            $ambulance->remarks = $request->remarks;
            if($ambulance->save())
            {
                return redirect()->back()->with('success','Request completed.');
            }
            else{
                return redirect()->back()->with('error','Unable to process request.');
            }
        }
        else{
            return redirect()->back()->with('error','Invalid request.');
        }
    }

    private function isAccepted($id)
    {
        $request = AmbulanceBooking::where('id',$id)->first();
        if($request->is_request_accepted)
        {
            return true;
        }
        return false;
    }
    private function isRejected($id)
    {
        $request = AmbulanceBooking::where('id',$id)->first();
        if($request->is_request_rejected)
        {
            return true;
        }
        return false;
    }
    private function isCompleted($id)
    {
        $request = AmbulanceBooking::where('id',$id)->first();
        if($request->is_completed)
        {
            return true;
        }
        return false;
    }
}
