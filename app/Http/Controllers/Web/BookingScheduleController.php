<?php

namespace App\Http\Controllers\Web;

//use App\BookingHistory;
use App\Models\AppointmentBooking;
use App\Models\Event;
use App\Http\Controllers\Controller;
//use App\OnlineBooking;
use App\Models\BookingSchedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Support\Facades\Session;
//use Illuminate\Support\Facades\DB;
//use App\User;
//use App\UserRole;
class BookingScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title']=' Manage Booking Schedule';
        $data['details']=BookingSchedule::where('provider_id',auth()->user()->id)->get();
        return view('web.manage_booking_schedule',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = ' Create Booking Schedule';
        $data['method'] ="POST";
        $data['url'] = "booking/add";
        return view('web.create_booking_schedule',$data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'total_bookings' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong.');
        }
        $chk=BookingSchedule::where('date',date('Y-m-d',strtotime($request->date)))->where('provider_id',auth()->user()->id)->count();
        if($chk > 0 )
        {
            return redirect()->back()->with('error','You have already a schedule on this date');
        }
        $event= new BookingSchedule();
        $event->provider_id=auth()->user()->id;
        $event->date=date('Y-m-d',strtotime($request->date));
        $event->total_bookings =$request->total_bookings;
        $event->save();
        return redirect('booking/manage')->with('success', 'Booking event has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = ' Edit Booking Schedule';
        $data['method'] ="post";
        $data['url'] = "booking/update";
        $data['event'] = BookingSchedule::where('id',$id)->first();
        return view('web.provider.create_booking_schedule',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'total_bookings' => 'required|numeric',
            'is_off' => 'required|string',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong.');
        }

        $event=BookingSchedule::find($request->id);

        $event->date=date('Y-m-d',strtotime($request->date));
        $event->total_bookings =$request->total_bookings;
        if($request->is_off=="yes")
        {
            $data['start_date'] = date('Y-m-d',strtotime($request->date));
            $data['end_date'] = date('Y-m-d',strtotime($request->date));
            $data['total_bookings'] = 0;
            $data['user_id'] = auth()->user()->id;
            $data['is_off'] = $request->is_off=="yes"?1:0;
            if(Event::create($data))
            {
                if($event->delete())
                {
                    return redirect('schedule-by-day/'.$request->event_id)->with('success','Booking schedule has been updated.');
                }
                else
                {
                    return redirect()->back()->with('error', 'Unable to update event.');
                }

            }
            else{
                return redirect()->back()->with('error', 'Unable to update event.');
            }

        }
        if($request->is_off=="no")
        {
            if(BookingSchedule::where('date',date('Y-m-d',strtotime($request->date)))->where('user_id',auth()->user()->id)->count())
            {
                BookingSchedule::where('date',date('Y-m-d',strtotime($request->date)))->where('user_id',auth()->user()->id)->update([
                    'total_bookings'=>$request->total_bookings
                ]);
                return redirect()->back()->with('success','Booking schedule has been updated.');
            }
            if($event->save())
            {
                return redirect()->back()->with('success', 'Booking schedule has been updated.');
            }
            else
            {
                return redirect()->back()->with('error', 'Unable to update event.');
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $event=BookingSchedule::where('id',$request->id)->where('provider_id',auth()->user()->id)->first();
        if($event)
        {
            $event->delete();
            return redirect()->back()->with('success','Booking schedule removed successfully.');
        }
        else{
            return redirect()->back()->with('error','Unable to remove booking schedule.');
        }
    }

    // get my bookings

    public function getMyBooking()
    {
        $data['title'] = ' My Bookings';
        $data['details'] = AppointmentBooking::where('provider_id',auth()->user()->id)->where('booking_number','!=','')->orderby('id','desc')->get();
        return view('web.provider.my_booking',$data);
    }

    //
//
//public function getAppointmentDetail($id){
//    $data['title'] = ' Appointment Detail';
//    $data['booking'] = AppointmentBooking::where('id',$id)->first();
//    return view('web.provider.show_appointment_detail',$data);
//}

public function editAppointmentDetail($id){
    $data['title'] = ' Edit Appointment';
    $data['booking'] = AppointmentBooking::where('id',$id)->first();
    return view('web.provider.edit_appointment',$data);
}

public function updateAppointment(Request $request)
{
    $validator = Validator::make($request->all(), [
        'patient_name' =>'required|string',
        'age' => 'required|string',
        'gender' => 'required|string',
        'address' => 'required|string',
        'mobile' => 'nullable|numeric',
        'bp' => 'nullable|string',
        'pulse' => 'nullable|string',
        'temperature' => 'nullable|string',
        'spo' => 'nullable|string',
        'weight' =>'nullable|string',
        'history' => 'nullable|string',
        'complain' => 'nullable|string',
        'diagnostic' => 'required|string',
        'prescription' => 'nullable|string',
        'prescription_image' => 'nullable|image|mimes:jpg,jpeg,png',
        'provider_id' => 'required|numeric',
        'patient_id' => 'required|numeric',
        'booking_id' => 'required|numeric',
        'remarks' => 'required|string',
    ]);
    if ($validator->fails()) {
        return redirect()->back()->with('error','Something went wrong.')->withErrors($validator->errors())->withInput();
    }

    $booking= AppointmentBooking::where('id',$request->booking_id)->first();
    $data=$request->except('_token','booking_id');
    $mno = $booking->patient->mobile;
    $directory = $mno . '_' . time();
    if($request->prescription=='')
    {
        $data['prescription'] = $booking->prescription;
    }
    if($request->has('prescription_image')) {
        $def_path = public_path('uploads/patientprofile/prescription/');
        if (!is_dir($def_path . $directory)) {
            if (mkdir($def_path . $directory, 0777)) {
                $destinationPath = $def_path . $directory;
            }
        }

        $photo = $request->file('prescription_image');
        $imagename = rand() . '.' . $photo->getClientOriginalExtension();
        $photo->move($destinationPath, $imagename);
        if (!empty($imagename)) {
            $data['prescription_folder'] = $directory;
            $data['prescription_image'] = $imagename;
        }
    }
    else{
        $data['prescription_folder'] = $booking->prescription_folder;
        $data['prescription_image'] = $booking->prescription_image;
    }
    $history['prescription'] = $booking->prescription;
    $history['prescription_folder'] = $booking->prescription_folder;
    $history['prescription_image'] = $booking->prescription_image;
    $history['remarks'] = $booking->remarks;
    $history['booking_id'] =  $booking->id;
//    BookingHistory::create($history);
    if($request->has('remarks'))
    {
        $data['is_completed'] = 1;
    }
    if($booking->update($data))
    {
        return redirect()->back()->with('success','Data has been updated.');
    }
    else{
        return redirect()->back()->with('error','Something went wrong. Unable to process data.');
    }
}

public function updateAppointmentTime(Request $request){
    $validator = Validator::make($request->all(), [
        'id' => 'required|numeric',
        'date' => 'required|date',
        'time' => 'required|string',
        ]);
    if ($validator->fails()) {
        return redirect()->back()->with('error',$validator->errors()->toArray())->withInput();
    }
    $datetime = $request->date.''.$request->time;
    $datetime = date('Y-m-d H:i:s',strtotime($datetime));
    $appointment = AppointmentBooking::where('id',$request->id)->where('provider_id',auth()->user()->id)->first();
    if($appointment!='')
    {
        if($appointment->is_completed)
        {
            return redirect()->back()->with('error',"This appointment has been already marked as completed.");
        }
        $appointment->appointment_time = $datetime;
        if($appointment->save())
        {
            return redirect()->back()->with('success',"Appointment time has been updated.");
        }
        else{
            return redirect()->back()->with('error',"Unable to update appointment time.");
        }
    }
    return redirect()->back()->with('error','Data not found');
}
}
