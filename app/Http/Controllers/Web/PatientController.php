<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\DocumentUpdateRequest;
use App\Http\Requests\PatientInfoUpdateRequest;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Facades\Validator;
use Image;
use App\Models\Setting;
use Illuminate\Support\Facades\File;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


class PatientController extends Controller
{
    private $config;
    public function __construct()
    {
        $setting = Setting::all();
        foreach ($setting as $data)
            $this->config[$data->param] = $data->data;
    }
    public function loadLoginView()
    {
        $data['title']= " Patient Login";
        return view('auth.login',$data);
    }

//    public function login(Request $request)
//    {
//        $validator = Validator::make($request->all(), [
//            'email' => 'required|email',
//            'password' =>'required',
//        ]);
//        if ($validator->fails()) {
//            return redirect()->back()->with('error',$validator->errors());
//        }
//        $credentials = $request->only('email', 'password');
//
//        if (Auth::attempt($credentials)){
//            $user=Auth::user()->first();
//            if($user->user_role_id == 3)
//            {
//                Session::put('user_is',$user->user_role->name);
//                Session::put('user_email',$user->email);
//                Session::put('user_id',$user->id);
//                Session::put('user_name',$user->name);
//                return redirect('patient/dashboard');
//            }
//            else{
//                return redirect()->back()->with('error','Unknown user access found..');
//            }
//
//        }
//        else{
//            return redirect()->back()->with('error','Invalid login credential.');
//        }
//    }
    public function loadRegistationView()
    {
        $data['title']= " Patient Registration";
        return view('web.patient.register',$data);
    }
public function registerPatient(Request $request)
{
    $this->validate($request,[
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:8', 'confirmed'],
        'mobile' => ['required', 'numeric', 'unique:users', 'digits:10'],
    ]);

    $create =  User::create([
        'name' => $request->name,
        'email' =>$request->email,
        'mobile' => $request->mobile,
        'password' => bcrypt($request->passwor),
    ]);
    User::where('id',$create->id)->update(['user_role_id'=>'3','approval' => '1']);
if($create){
    return redirect('patient/login')->with('success','Registration successfull.');
}
else
{
    return redirect()->back()->with('error','Something went wrong.');
}
}
    public function dashboard()
    {
        $data['title'] =" Dashboard";
        $data['profile']=Profile::where('user_id',Auth::user()->id)->first();
        return view('web.patient.dashboard',$data);
    }
    // Load Update Profile view
    public function updateProfile()
    {
        $data['title']=" Update Profile";
        $data['user']=User::where('id',Auth::user()->id)->first();
//
        return view('web.patient.update-profile',$data);
    }

    // Update Personal Info
    public function personalInfoUpdate(PatientInfoUpdateRequest $request)
    {
        $validated = $request->validated();
        if(!$request->has('mobile') && !$request->has('email'))
        {
            return redirect()->back()->with('error','Please provide either mobile no. or email id');
        }
        $data=$request->except(['_token','name','mobile','email']);
        $user['name'] =$request->name;
        $user['mobile'] =$request->mobile;
        $user['email'] =$request->email;
        User::where('id',Auth::user()->id)->update($user);
        $chk=Profile::where('user_id',Auth::user()->id)->get();
        if(count($chk) > 0)
        {
            $data['updated_at']=date("Y-m-d h:i:s");
            $ins=DB::table('profiles')->where('user_id',Auth::user()->id)->update($data);
        }
        else {
            $data['created_at']=date("Y-m-d h:i:s");
            $data['updated_at']=date("Y-m-d h:i:s");
            $data['user_id']=Auth::user()->id;
            $ins=DB::table('patient_profiles')->insertGetId($data);
        }

        if($ins > 0)
        {
            return redirect()->back()->with('success','Personal info updated. Proceed for further updates.');
        }
        else {
            return redirect()->back()->with('error','Unable to update details.');
        }

    }
    // Update Documents
    public function documentUpdate(DocumentUpdateRequest $request)
    {
        $validated = $request->validated();
        // Profile Pic
        $save=Profile::where('user_id',Auth::user()->id)->first();
        $rsftu=rand();
        $photo=$request->file('photo');
        $imagename=$rsftu . '.' . $photo->getClientOriginalExtension();

        $destinationPath = public_path('uploads/profile/');
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath,0777);
        }
        $thumb_img = Image::make($photo->getRealPath())->resize(200, 200);
        $thumb_img->save($destinationPath.'/'.$imagename,80);

        if(isset($imagename))
        {
            $propic=$imagename;
            if(!empty($save->photo))
            {
                $image_path = $destinationPath.$save->photo;
                unlink($image_path);
            }

        }
        $update=   DB::table('profiles')->where('user_id',Auth::user()->id)->update([
            'photo' => $propic,
            'updated_at' => date("Y-m-d h:i:s")
        ]);

        if(isset($update) && !empty($update))
        {
            return redirect()->back()->with('success','Profile Photo uploaded successfully.');
        }
        else {
            return redirect()->back()->with('error','Unable to upload profile photo.');
        }
    }
}
