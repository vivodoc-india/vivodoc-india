<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function __invoke($page)
    {
        $title= __('Meta Title: ' .$page);
        if($title == 'Meta Title: ' .$page)
        {
            $title = "OMEDDr";
        }
        else{
            $title= " ".$title;
        }
        return view ('web.'.$page,['title'=>$title]);
    }
}
