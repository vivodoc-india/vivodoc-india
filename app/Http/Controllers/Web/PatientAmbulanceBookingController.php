<?php

namespace App\Http\Controllers\Web;

use App\Models\AmbulanceBooking;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\ServiceProfile;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PatientAmbulanceBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $config;

    public function __construct()
    {
        $this->middleware('patient');
        $setting = Setting::all();
        foreach ($setting as $data)
            $this->config[$data->param] = $data->data;
    }
    public function index()
    {
        $data['title'] = ' Ambulance Bookings';
        $data['details'] = AmbulanceBooking::where('patient_id',Auth::user()->id)->orderby('id','desc')->get();
        return view('web.patient.ambulance-booking-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data['provider'] = ServiceProfile::where('id',$id)->first();
        $data['profile'] = Profile::where('user_id',auth()->user()->id)->first();
        $data['title'] = ' Ambulance Bookings';
        $data['appointment_fee'] = $this->config['appointment_fee'];
        return view('web.front-end.book-ambulance',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
           'patient_name' => 'required|string',
           'age' => 'required|string',
           'mobile' => 'nullable|numeric|digits:10',
            'gender' => 'required|string|in:Male,Female',
            'address' => 'required|string',
            'longitude' => 'required|string',
            'latitude' => 'required|string',
            'destination' => 'required|string',
            'provider_id' => 'required|numeric',
            'service_profile_id' => 'required|numeric',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->with('error','Invalid data submitted')->withErrors($validator->errors())->withInput();
        }

        $data = $request->all();
        $isAvaliable = ServiceProfile::where('id',$request->service_profile_id)->where('user_id',$request->provider_id)->first();
        if(!$isAvaliable->mark_live)
        {
            return redirect()->back()->with('error','Service Provider is offline now.');
        }
        if(!$isAvaliable->is_available)
        {
            return redirect()->back()->with('error','Service Provider is not free for accepting any bookings.');
        }
        $data['patient_id'] = auth()->user()->id;
        $data['ambulance_fee'] = $this->config['appointment_fee'];
        $data['booking_date'] = date('Y-m-d');
        $amb_book = AmbulanceBooking::create($data);
        if($amb_book!='')
        {
            $text = "Dear User, A patient ".auth()->user->name." sent you request for Ambulance booking. Regards OMEDDr.";
            sms_sender(trim($amb_book->provider->mobile), $text,$this->config['sms_patient_request']);
          return redirect('book-ambulance/'.$amb_book->id.'/edit')->with('success','Request sent. Please wait for provider\'s response.');
        }
        else{
            return redirect()->back()->with('error','Something went wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['booking']= AmbulanceBooking::findOrFail($id);
        $data['title'] = ' View Ambulanace Booking';
        return view('web.patient.ambulance-booking-detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['booking'] = AmbulanceBooking::findOrFail($id);
        $data['provider'] = ServiceProfile::where('id',$data['booking']->service_profile_id)->first();
        $data['profile'] = Profile::where('user_id',auth()->user()->id)->first();
        $data['title'] = ' Ambulance Bookings';
        return view('web.front-end.book-ambulance',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    // Check Response Status Of Service Provider For Ambulance Booking
    public function checkStatus(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'res_code' =>'required|numeric',
                'uid' => 'required|numeric'
            ]);

            if ($validator->fails()) {
                return response()->json(['success'=>'false','message'=>'Something went wrong.','stop'=>'true']);
            }
            $stat= AmbulanceBooking::where('id',$request->res_code)->where('patient_id',$request->uid)->first();
            if($stat->is_request_accepted && $stat->is_paid==0)
            {
                return response()->json(['success'=>'true','message'=>'Service Provider accepted your request.']);

            }
            elseif($stat->is_request_accepted && $stat->is_paid==1)
            {
                return response()->json(['success'=>'false','message'=>'Service Provider accepted your request.','stop'=>'true']);

            }
            elseif($stat->is_request_rejected){
                return response()->json(['success'=>'false','message'=>'Service Provider reject your request.','stop'=>'true']);
            }
            else{
                return response()->json(['success'=>'false','res_code'=>$request->res_code]);
            }
        }
    }
    public function payBookingFee(Request $request)
    {
        if(!$request->has('id'))
        {
            return redirect()->back()->with('error','Unable to process this request.');
        }
        $booking= AmbulanceBooking::where('id',$request->id)->where('patient_id',auth()->user()->id)->first();
        if($booking=='')
        {
            return redirect()->back()->with('error','Unable to process this request.');
        }
        else{
           return $this->payAmbulanceFee($booking);
        }
    }
    private function payAmbulanceFee($booking)
    {
        $data=array();
        $data['orderId'] = Str::random(15);
        $data['customerName']=$booking->patient->name;
        $data['customerEmail'] = $booking->patient->email;
        $data['customerPhone'] = $booking->patient->mobile;
        $data['orderAmount'] = $booking->ambulance_fee;
        $data['orderNote'] = "Ambulance Booking Fee";
        $data['return'] = '/cf-ambulance/return';
        $data['notify'] = '/cf-ambulance/notify';
        $orderDetails = payNow($data);

        $data['title'] =" Pay Now";
        $data['orderDetails'] =$orderDetails;
        $update=array();
        $update['temp_txn_number'] =$data['orderId'];

        if($booking->update($update))
        {
            return view('web.payment.ambulance_checkout',$data);
        }
    }
    // Payment response for Appointment Booking
    public function notifyPaymentRequest(Request $request)
    {

    }


    public function returnPaymentResponse(Request $request)
    {

        $response= $request->all();
        $detail= $request->all();
        $booking= AmbulanceBooking::where('temp_txn_number',$detail['orderId'])->first();
        $update=array();
        $update['transaction_status'] =$detail['txStatus'];
        $update['txn_amount'] = $detail['orderAmount'];
        $update['txn_date'] =$detail['txTime'];
        $update['txn_response'] = $detail['txStatus'];
        $update['txn_number'] = $detail['orderId'];
        $update['is_paid'] = 1;
        $update['referenceId'] = $detail['referenceId'];
        $update['payment_mode'] = $detail['paymentMode'];

        if(validateSignature($detail))
        {
            if($detail['txStatus'] == "SUCCESS")
            {
                if($booking->update($update))
                {
                    $update['title'] = " Payment Response";
                    $update['booking_id'] = $booking->id;
//                    $text = "Dear User, appointment has been booked. Your booking number is ".$update['booking_number'].". Regards OMEDDr.";
                    $text = "Dear User, your ambulance booking successful. Your booked ambulance is ".$booking->serviceProfile->vehicle_no.". Regards OMEDDr.";
                    $text2 = "Dear User, you have a confirmed ambulance booking request. Your are booked by ".$booking->patient->name.". Regards OMEDDr.";
//                    $text2 = "Dear User, a patient has booked an appointment with booking no ".$update['booking_number'].". Regards OMEDDr.";
                    sms_sender(trim($booking->patient->mobile), $text,$this->config['sms_pat_ambulance_book']);
                    sms_sender(trim($booking->provider->mobile), $text2,$this->config['sms_provider_ambulance_booking']);
                    return view('web.payment.ambulance_payment_response',$update)->with('success','Payment done successfully.');
                }
                else
                {
                    return view('web.payment.ambulance_payment_response',$update)->with('error','Unable to proceed.');

                }
            }
            else
            {
                return view('web.payment.ambulance_payment_response',$update)->with('error','Your transaction was not successful.');
            }

        }
        else{
            $update['signature_mismatch'] = true;
            return view('web.payment.ambulance_payment_response',$update)->with('error','Invalid payment signature found.');
        }
    }
}
