<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\AppointmentBookingRequest;
use App\Models\BookingSchedule;
use App\Models\Event;
use App\Models\AppointmentBooking;
use App\Models\EnquiryChatNode;
use App\Http\Controllers\Controller;
use App\Models\PatientEnquiry;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Category;
use App\Models\Profile;
use App\Models\ServiceProfile;
//use App\Models\ReviewRating;
//use App\Models\Booking;
use Illuminate\Support\Str;
use Image;
//use Calendar;
use Illuminate\Support\Facades\Validator;

class CommonPatientController extends Controller
{
    private $config;
    public function __construct()
    {
        $setting = Setting::all();
        foreach ($setting as $data)
            $this->config[$data->param] = $data->data;
    }

    public function changePatientPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' =>'required|string',
            'new_password' => 'required|string',
            'password_confirmation' =>'required|string|same:new_password'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong')->withInput()->withErrors($validator);
        }
        if(!Auth::user()->user_role_id==3)
        {
            Auth::logout();
            return '/';
        }
        $user = User::where('id',Auth::user()->id)->first();
        if(Hash::check($request->current_password,$user->password))
        {
            $user->password = Hash::make($request->new_password);
            if($user->save())
            {
                Auth::logout();
                return redirect('/login')->with('success','Login again with newly generated password.');
            }
            else{
                Auth::logout();
                return redirect('/login');
            }
        }


//        Hash::make($password);
    }
    // Function to get all enquiries
    public function myEnquiries()
    {
        $data['title']=" My Enquiries";
        $data['enquiry']=PatientEnquiry::where('patient_id',Auth::user()->id)->orderby('id','desc')->get();
        return view('web.patient.enquiries',$data);
    }

    // Load Reply view for Patient enquiry
    public function loadReplyView($id)
    {
        $data['title']=" Patient Enquiry Reply";
        $data['enquiry']=PatientEnquiry::where('id',$id)->first();
        return view('web.patient.reply-enquiry',$data);
    }

    // Function to save Patient enquiry reply

    public function savePatientEnquiry(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'provider_id' => 'required|numeric',
            'question' =>'required|string',
            'patient_doc' =>'array|min:1',
            'patient_doc.*' => 'image|mimes:jpg,jpeg,png',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong.')->withErrors($validator->errors());
        }
        $frd='';
        $destinationPath='';
        if(!empty($request->file('patient_doc')))
        {

            $mno=Auth::user()->mobile;
            $directory=$mno.'_'.time();
            $def_path=public_path('uploads/patientprofile/enquiry/');
            if(!is_dir($def_path.$directory))
            {
                if(mkdir($def_path.$directory,0777))
                {
                    $destinationPath=$def_path.$directory;
                }
            }


            for($j=0;$j<count($request->patient_doc);$j++)
            {
                $photo=$request->file('patient_doc')[$j];
                $imagename=rand() . '.' . $photo->getClientOriginalExtension();
                $thumb_img = Image::make($photo->getRealPath())->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($destinationPath.'/'.$imagename,80);
                if($frd!='')
                {
                    $frd=$frd.",".$imagename;
                }
                else
                {
                    $frd=$imagename;
                }

            }
        }

        $enquiry = new PatientEnquiry;
        $enquiry->patient_id = Auth::user()->id;
        $enquiry->provider_id = $request->provider_id;
        $enquiry->question =$request->question;
        if($destinationPath!='' && $frd!=' ')
        {
            $enquiry->pat_directory =$directory;
            $enquiry->pat_files =$frd;
        }
        if($enquiry->save())
        {
            $provider=User::where('id',$request->provider_id)->first();
            $text = "Dear User, a user (". Auth::user()->name. ") has made an inquiry. Regards OMEDDr.";
            sms_sender(trim($provider->mobile), $text,$this->config['sms_patient_enquiry']);
            return redirect()->back()->with('success','Thanks for asking. Service provider will respond you soon.');
        }
        else{
            return redirect()->back()->with('error','Unable to save your enquiry.');
        }
    }
    // Function to get all reviews and ratings

    public function myReviewRating()
    {
        $data['title']=" My Reviews & Ratings";
        $data['review']=ReviewRating::where('patient_id',Auth::user()->id)->orderby('id','desc')->paginate(5);
        return view('web.patient.reviews',$data);
    }

    public function myAppoinmentBookings()
    {
        $data['title'] = ' My Appoinment Bookings';
        $data['details'] = AppointmentBooking::where('patient_id',Auth::user()->id)->orderby('id','desc')->get();
        return view('web.patient.my_booking',$data);
    }

    public function getAppointmentDetail($id)
    {
        $data['title'] = ' Appointment Detail';
        $data['booking']=  AppointmentBooking::findOrFail($id);
        return view('web.patient.appointment_detail',$data);
    }

    public function printAppointmentPrescription($id)
    {

    }

    public function updateAppointmentReview(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'appoint_id' => 'required|numeric',
            'review' =>'required|string',
            'rating' =>'required|numeric',

        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong.')->withErrors($validator->errors());
        }

       if(AppointmentBooking::where('id',$request->appoint_id)->update(['review'=>$request->review,'rating'=>$request->rating]))
       {
           return redirect()->back()->with('success','Thanks for your valuable reviews and ratings.');
       }
       else{
           return redirect()->back()->with('error','Unable to update your reviews and ratings.');
       }
    }

    public function updateConsultationReview(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'booking_id' => 'required|numeric',
            'review' =>'required|string',
            'rating' =>'required|numeric',

        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong.')->withErrors($validator->errors());
        }

        if(OnlineBooking::where('id',$request->booking_id)->update(['review'=>$request->review,'rating'=>$request->rating]))
        {
            return redirect()->back()->with('success','Thanks for your valuable reviews and ratings.');
        }
        else{
            return redirect()->back()->with('error','Unable to update your reviews and ratings.');
        }
    }

    public function editAppointment($id)
    {
        $data['title'] = 'Edit Appointment';
        $data['booking']=  AppointmentBooking::findOrFail($id);
        return view('web.patient.edit_appointment_detail',$data);
    }

    public function getAppointmentBookingView($id){
        $provider = ServiceProfile::where('id',$id)->first();
        $data['title'] = "Appointment Booking";
        $today_schedule = BookingSchedule::where('user_id', $provider->user_id)->where('date', date('Y-m-d'))->first();
        if (!empty($today_schedule)) {
            $data['today_schedule'] = $today_schedule;

        } else {
            $data['schedule'] = "No schedule found for today.";
        }
        $data['today_booking'] = AppointmentBooking::where('provider_id', $provider->user_id)
            ->where('booking_date', date('Y-m-d'))
            ->count();
//        $events = [];
//        $schedule = Event::where('user_id', $provider->user_id)->get();
//        if ($schedule->count()) {
//            foreach ($schedule as $key => $value) {
//                $events[] = Calendar::event(
//                    $value->title,
//                    true,
//                    new \DateTime($value->start_date),
//                    new \DateTime($value->end_date . '+1 day'),
//                    null,
//                    // Add color
//                    [
//                        'color' => $value->color,
//                        'textColor' => $value->text_color,
//                    ]
//                );
//            }
//        }
        $data['provider'] = $provider->user_id;
        $data['service_profile'] =  $id;
        $data['appointment_fee'] = $this->config['appointment_fee'];
//        $data['calendar'] = Calendar::addEvents($events)->setOptions([
//            'headerToolbar' => [
//                'left' => 'prev,next',
//                'center' => 'title',
//                'right' => ''
//            ],
//        ]);
        return view('web.appoinment_booking', $data);
    }

    public function saveAppointmentBookingRequest(AppointmentBookingRequest $request){
        $data = $request->validated();
        if(strtotime($request->booking_date) - strtotime(date('d-m-Y')) < 0)
        {
            return redirect()->back()->with("error","Backdate bookings are not allowed.");
        }
        $data['booking_date'] = date('Y-m-d',strtotime($request->booking_date));
        $data['patient_id']=auth()->user()->id;
        $chk = AppointmentBooking::where('provider_id',$request->provider_id)
            ->where('patient_id',auth()->user()->id)
            ->where('booking_date',date('Y-m-d',strtotime($request->booking_date)))
            ->where('is_paid',0)
            ->first();
        if($chk!='')
        {
            $serviceProfile= ServiceProfile::where('id',$request->service_profile_id)->first();
//     $detail['fee_to_pay'] = $serviceProfile->max_fee - (($serviceProfile->max_fee*$this->config['appointment_discount'])/100);
//            $detail['url'] = url('omed-api/user/pay-appointment-fee/'.auth()->user()->id.'/'.$chk->id);
//            $detail['booking_id'] = $chk->id;
            return $this->payAppointmentFee($request->getClientIp(),$chk);
            return sendSuccessResponse("You have already an appointment request for selected date. Proceed for payment.",$detail);
        }
        $total_book = AppointmentBooking::where('provider_id',$request->provider_id)
            ->where('booking_date',date('Y-m-d',strtotime($request->booking_date)))
            ->where('is_paid',1)
            ->count();
        $today_schedule = BookingSchedule::where('user_id',$request->provider_id)
            ->where('date',date('Y-m-d',strtotime($request->booking_date)))
            ->first();
        if(!empty($today_schedule))
        {
            if($total_book > 0 && $total_book >= $today_schedule->total_bookings)
            {
                return redirect()->back()->with("error","Bookings are full for the selected date.");
            }
        }
        else{

            return redirect()->back()->with("error","No booking schedule is found for the selected date.");
        }
        $serviceProfile= ServiceProfile::where('id',$request->service_profile_id)->first();
        $booking=AppointmentBooking::create($data);
//        $detail['fee_to_pay'] = $serviceProfile->max_fee - (($serviceProfile->max_fee*$this->config['appointment_discount'])/100);
        if($booking)
        {
            return $this->payAppointmentFee($request->getClientIp(),$booking);
//            $detail['url'] = url('omed-api/user/pay-appointment-fee/'.$booking->patient_id.'/'.$booking->id);
//            $detail['booking_id'] = $booking->id;
//            return sendSuccessResponse("Request Saved. Proceed for payment.",$detail);
        }
        else{
            return redirect()->back()->with("error","Unable to book appointment.");
        }
    }

    private function payAppointmentFee($ip,$booking)
    {
        $data=array();
        $data['orderId'] = Str::random(15);
        $data['customerName']=$booking->patient->name;
        $data['customerEmail'] = $booking->patient->email;
        $data['customerPhone'] = $booking->patient->mobile;
        $data['orderAmount'] = $this->config['appointment_fee'];
        $data['orderNote'] = "Appointment Booking Fee";
        $data['return'] = '/cf-appointment/return';
        $data['notify'] = '/cf-appointment/notify';
        $orderDetails = payNow($data);

        $data['title'] =" Pay Now";
        $data['orderDetails'] =$orderDetails;
        $update=array();
        $update['temp_txn_number'] =$data['orderId'];
//        $update['appointment_fee'] = $data['orderAmount'];
//        $update['who_pay'] = $ip;
        if($booking->update($update))
        {
            return view('web.payment.appointment_checkout',$data);
        }
    }
}
