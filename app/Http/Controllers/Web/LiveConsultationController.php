<?php

namespace App\Http\Controllers\Web;

//use App\Models\Booking;
use App\Http\Controllers\Controller;
use App\Models\LiveConsultation;
use App\Models\ServiceProfile;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use App\Models\MedicalData;
class LiveConsultationController extends Controller
{
    private $config;

    public function __construct()
    {
        $setting = Setting::all();
        foreach ($setting as $data)
            $this->config[$data->param] = $data->data;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = ' Video Consultation Bookings';
        $data['details'] = LiveConsultation::where('provider_id',Auth::user()->id)
                                            ->where('request_status','>=',3)
                                            ->orderby('id','desc')->get();
        return view('web.provider.live-consultation',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAllMedicalDataList($id)
    {
        $data['title']=" Medical Data Access";
        $data['list'] = MedicalData::where('user_id',$id)->where('parent',0)->orderby('id','desc')->get();
        return view('web.medical-data-access',$data);
    }

    public function getAllLiveConsultationDataList($id)
    {
        $data['title']=" Live Consultation Data Access";
        $data['list'] = LiveConsultation::where('patient_id',$id)->select('id','provider_id','patient_id','patient_name','age','booking_date')->orderby('id','desc')->get();
        return view('web.provider.consultation-data-list',$data);
    }

    public function getAllAppointmentDataList($id)
    {
        $data['title']=" Appointment Data Access";
        $data['list'] = Booking::where('patient_id',$id)->where('is_completed',1)->select('id','provider_id','patient_id','patient_name','age','booking_date')->orderby('id','desc')->get();
        return view('web.provider.appointment-data-list',$data);
    }

    public function getMedicalDataDetail($id)
    {
        $data['title']=" Medical Data Detail";
        $data['record']=MedicalData::where('parent',$id)->orderby('created_at','desc')->get();;
        return view('web.medical-data-detail',$data);
    }

    public function getLiveConsultationDataDetail($id)
    {
        $data['title']=" Live Consultation Data Detail";
        $data['booking']=LiveConsultation::where('id',$id)->first();
        return view('web.provider.consultation-data-detail',$data);
    }

    public function getAppointmentDataDetail($id)
    {
//        $data['title']=" Appointment Booking Data Detail";
//        $data['booking']=Booking::where('id',$id)->first();
//        return view('web.provider.appointment-data-detail',$data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $data['booking']= LiveConsultation::findOrFail($id);
        $data['title'] = ' View Consultation Report';
        return view('web.provider.consultation-data-detail',$data);
    }
    public function edit($id)
    {
        $booking = LiveConsultation::find($id);
        $is_page_refreshed = (isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] == 'max-age=0');
        if($is_page_refreshed ) {
            $booking->con_link='';
            $booking->caller=null;
            $booking->save();
            ServiceProfile::where('user_id',$booking->provider_id)->where('id',$booking->servive_profile_id)->update(['is_available'=>1]);
        }
        $data['booking']= LiveConsultation::find($id);

        $data['title'] = ' Edit Video Consultation Request';
        return view('web.consultation-form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'patient_name' =>'required|string',
            'age' => 'required|numeric',
            'gender' => 'required|string',
            'address' => 'required|string',
            'mobile' => 'nullable|numeric',
            'bp' => 'nullable|string',
            'pulse' => 'nullable|string',
            'temperature' => 'nullable|string',
            'spo' => 'nullable|string',
            'weight' =>'nullable|string',
            'history' => 'nullable|string',
            'complain' => 'nullable|string',
            'diagnostic' => 'required|string',
            'prescription' => 'nullable|string',
            'prescription_image' => 'nullable|image|mimes:jpg,jpeg,png',
            'provider_id' => 'required|numeric',
            'patient_id' => 'required|numeric',
            'omed_code' => 'nullable|string',
            'bike_dr' => 'nullable|string',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong.')->withErrors($validator->errors())->withInput();
        }
        $booking= LiveConsultation::where('id',$id)->first();
        if($booking!='' && $booking->prescription_folder=='' && $booking->prescription_image=='')
        {
            if($request->prescription=='' && $request->prescription_image=='')
            {
                return redirect()->back()->with('error','You will have to provide either prescription note or upload prescription image.')->withInput();
            }
        }


//if($booking->doc_consulted==0)
//{
//    return redirect()->back()->with('error','You need to consult with patient first.')->withInput($request->input());
//}
        $data=$request->except('_token');
        $mno = $booking->patient->mobile;
        $directory = $mno . '_' . time();
        if($request->prescription=='')
        {
            $data['prescription'] = $booking->prescription;
        }
        if($request->has('prescription_image')) {
            $def_path = public_path('uploads/patientprofile/prescription/');
            if (!is_dir($def_path . $directory)) {
                if (mkdir($def_path . $directory, 0777)) {
                    $destinationPath = $def_path . $directory;
                }
            }
            if (!empty($booking->prescription_folder)) {
                $path = public_path('uploads/patientprofile/prescription/' . $booking->prescription_folder);
                if (File::exists($path)) {
                    File::deleteDirectory($path);
                }
            }
            $photo = $request->file('prescription_image');
            $imagename = rand() . '.' . $photo->getClientOriginalExtension();
            $photo->move($destinationPath, $imagename);
            if (!empty($imagename)) {
                $data['prescription_folder'] = $directory;
                $data['prescription_image'] = $imagename;
                $data['con_link_access'] = 0;
                $data['is_con_link_available'] = 0;
                $data['request_status'] = '4';
            }
        }
        else{
            $data['prescription_folder'] = $booking->prescription_folder;
            $data['prescription_image'] = $booking->prescription_image;
            $data['request_status'] = '4';
        }
if($booking->update($data))
{
    return redirect()->back()->with('success','Data has been updated. Thanks for consulatation.');
}
else{
    return redirect()->back()->with('error','Something went wrong. Unable to process data.');
}

    }

    public function saveResponse(Request $request)
    {
        if ($request->ajax() && $request->isMethod('post'))
        {
            $validator = Validator::make($request->all(), [
                'resp_request' => 'required|string',
                'status' => 'required|string',
                'id' => 'required|numeric',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => 'false', 'message' => 'Unable to process your request.']);
            }
if(LiveConsultation::where('id',$request->id)->count()<=0)
{
    return response()->json(['success' => 'false', 'message' => 'Record not found.']);
}
$book=LiveConsultation::where('id',$request->id)->first();
if(!empty($book->con_link))
{
    return response()->json(['success' => 'false', 'message' => 'Conference link already created for this booking.']);
}
if($book->booking_date!=date('Y-m-d'))
{
    return response()->json(['success' => 'false', 'message' => "You can only accept or reject same date's request."]);
}
            if($request->resp_request == "is_request_accepted")
            {
                if($request->status == "true")
                {
//                    $data['is_request_accepted'] =1;
//                    $data['con_link_access'] = 1;
//                    $data['is_paid'] = 1;
//                    $data['is_con_link_available'] = 1;
//                    $book->update($data);
//                    return response()->json(['success' => 'true', 'message' => 'Conference link created. Wait for patient\'s response.','resp_code'=>$request->id]);
//
//                    if($book->update(['is_request_accepted'=>1]))
//                    {
                        $confrence="OMED".time();
                        $response = createConfrence($confrence);
                        $response= json_decode($response);
                        if($response->status=="Added")
                        {
                            $data['is_request_accepted'] =1;
                            $data['con_hash']= $response->hashKey;
                            $data['con_name'] =$response->name;
                            $data['con_added'] =1;
                            $data['con_created_at'] = date('Y-m-d H:i:s');
                            $data['con_response'] = json_encode($response);

                            if($book->serviceProfile->user_role_id==9) {
                                $activateLink = enableConferenceLink($response->name);

                                $activateLink = json_decode($activateLink);
                                $data['con_link_access'] = 1;
                                $data['transaction_status'] = 'SUCCESS';
                                $data['txn_amount'] = 0;
                                $data['txn_date'] = date('Y-m-d H:i:s');
                                $data['txn_response'] = 'Service is free for this category.';
                                $data['txn_number'] = 0;
                                $data['is_paid'] = 1;
                                $data['referenceId'] = 'NA';

//                $update['con_embed'] = $embedCode;
                                $data['is_con_link_available'] = 1;

                                if ($activateLink->status == "Added") {
                                    $data['con_link'] = $activateLink->conferencelink;
                                }
                            }
                            if($book->update($data))
                            {
                                $text = "Dear User, your request has been accepted by the service provider. Regards OMEDDr.";
                                sms_sender(trim($book->patient->mobile), $text,$this->config['sms_provider_request']);

                                return response()->json(['success' => 'true', 'message' => 'Conference link created. Wait for patient\'s response.','resp_code'=>$request->id]);
                            }
                            else{
                                return response()->json(['success' => 'false', 'message' => 'Unable to update your response.']);
                            }
                        }
                        else{
                            $fail['con_response'] = json_encode($response);
                            $book->update($fail);
                            return response()->json(['success' => 'false', 'message' => 'Unable to generate conference link.']);
                        }

//                    }
                }
            }
            elseif($request->resp_request == "is_request_rejected")
            {
                    if($request->status == "true")
                    {
                       if($book->update(['is_request_rejected'=>1]))
                       {
                           $text = "Dear User, your request has been rejected by the service provider. Regards OMEDDr.";
                           sms_sender(trim($book->patient->mobile), $text,$this->config['sms_provider_request']);
                           return response()->json(['success' => 'true', 'message' => 'Thanks for reply.']);
                       }
                       else{
                         return response()->json(['success' => 'false', 'message' => 'Unable to process your request.']);
                       }

                    }
            }
            else{
                return response()->json(['success' => 'false', 'message' => 'Unidentified request made.']);
            }
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function isPaid(Request $request)
    {
        if ($request->ajax() && $request->isMethod('post'))
        {
        $validator = Validator::make($request->all(), [
            'uid' => 'required|numeric',
            'res_code' =>'required|numeric',
        ]);
            if ($validator->fails()) {
                return response()->json(['success'=>false,'message'=>'Invalid data sent.','stop'=>true]);
            }

            $booking= LiveConsultation::where('id',$request->res_code)
                ->where('provider_id',$request->uid)
                ->where('booking_date',date('Y-m-d'))
                ->first();

            if($booking=='')
            {
                return response()->json(['success'=>false,'message'=>'No data available.','stop'=>true]);
            }

            if($booking->is_paid==1){
                return response()->json(['success'=>true,'message'=>'Patient has paid the consultation fee. Start the consultation','stop'=>true]);
            }

        }

        else{
            return response()->json(['success' => false, 'message' => 'Unidentified request made.']);
        }
    }

    // initiate call

    // initiate Call By Provider
    public function initiateCall(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'res_code' =>'required|numeric',
                'ident' => 'required|numeric',
                'caller' => 'required|numeric',
                'id' => 'required|string',
            ]);

            if ($validator->fails()) {
                return response()->json(['success'=>'false','message'=>'Something went wrong.']);
            }
            $stat= LiveConsultation::where('id',$request->res_code)->where('provider_id',$request->ident)->first();
            if($stat->request_status >= 3)
            {
                if($stat->caller!='' && $stat->caller!=auth()->user()->id)
                {
                    return response()->json(['success'=>'true','roomExists'=>'true','id'=>$stat->con_link]);
                    ServiceProfile::where('user_id',$stat->provider_id)->where('id',$stat->servive_profile_id)->update(['is_available'=>0]);
                }
                else{
                    $stat->con_link = $request->id;
                    $stat->caller = $request->caller;
                    if($stat->save())
                    {

                        ServiceProfile::where('user_id',$stat->provider_id)->where('id',$stat->servive_profile_id)->update(['is_available'=>0]);
                        return response()->json(['success'=>'true','roomExists'=>'false','message'=>'Call initiated.']);
                    }

                }

            }

            else{
                return response()->json(['success'=>'false','res_code'=>$request->res_code]);
            }
        }
    }
    public function removeCall(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'res_code' =>'required|numeric',
                'ident' => 'required|numeric',
            ]);

            if ($validator->fails()) {
                return response()->json(['success'=>'false','message'=>'Something went wrong.']);
            }
            $stat= LiveConsultation::where('id',$request->res_code)->where('provider_id',$request->ident)->first();
            if($stat->request_status >= 3)
            {

                $stat->con_link = '';
                $stat->caller = null;
                if($stat->save())
                {
                    ServiceProfile::where('user_id',$stat->provider_id)->where('id',$stat->servive_profile_id)->update(['is_available'=>1]);
                    return response()->json(['success'=>'true','message'=>'Call Closed.']);
                }

            }

            else{
                return response()->json(['success'=>'false','res_code'=>$request->res_code]);
            }
        }
    }
}
