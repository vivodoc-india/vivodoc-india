<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use App\Models\WalletTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PatientWalletController extends Controller
{
    private $config;

    public function __construct()
    {
        $setting = Setting::all();

        foreach ($setting as $data)
            $this->config[$data->param] = $data->data;
    }
    public function index(){
        $data['title'] = " OMED Coins";
        $data['transaction'] = WalletTransaction::where('user_id',auth()->user()->id)
                                                    ->select('id','transaction_date','transaction_status','transaction_amount','transaction_type')
                                                    ->orderByDesc('id')
                                                    ->get();
        return view('web.patient.wallet.index',$data);
    }

    public function show($id){
$data['title'] = " OMED Coin Transactions";
$data['detail'] = WalletTransaction::findOrFail($id);
return view('web.patient.wallet.show',$data);
    }

    public function addMoney(Request $request){
        if(!$request->has('amount'))
        {
            return redirect()->back()->with('error','Please enter amount you want to add.');
        }
        if(!is_numeric($request->amount))
        {
            return redirect()->back()->with('error','Please enter a valid amount.');
        }
        if($request->amount<=0)
        {
            return redirect()->back()->with('error','Request cann\'t be processable.');
        }

        $data=array();
        $data['orderId'] = Str::random(15);
        $data['customerName']=auth()->user()->name;
        $data['customerEmail'] = auth()->user()->email!=''?auth()->user()->email:$this->config['master_email'];
        $data['customerPhone'] = auth()->user()->mobile;
        $data['orderAmount'] = $request->amount;
        $data['notify'] = '/wl-payment/notify';
        $data['return'] = '/wl-payment/return';
        $data['orderNote'] = "Purchase of OMED Coins";
        $orderDetails = payNow($data);

        $data['title'] =" Pay Now";
        $data['orderDetails'] =$orderDetails;
        $create=array();
        $create['transaction_number'] =$data['orderId'];
        $create['who_pay'] = $request->getClientIp();
        $create['user_id'] =auth()->user()->id;
        if(WalletTransaction::create($create))
        {
            return view('web.payment.wallet_checkout',$data);
        }
    }

    public function notifyPaymentRequest(Request $request)
    {

    }
    public function returnPaymentResponse(Request $request)
    {
        $detail= $request->all();
        $wallet= WalletTransaction::where('transaction_number',$detail['orderId'])
                                    ->orderByDesc('created_at')
                                    ->first();
        $book['booking'] = $wallet;
//    return $booking;
        if(validateSignature($detail))
        {
            $update=array();
            $update['transaction_status'] =$detail['txStatus'];
            $update['transaction_amount'] = $detail['orderAmount'];
            $update['transaction_date'] =$detail['txTime'];
            $update['transaction_response'] = $detail['txStatus'];
            $update['referenceId'] = $detail['referenceId'];
            $update['transaction_type'] = "credit";
            if($detail['txStatus'] == "SUCCESS" || $detail['txStatus'] == "PENDING")
            {
                if($wallet->update($update))
                {
                    $addAmount= User::where('id',$wallet->user_id)->first();
                    $newAmount = $addAmount->wallet_amount + $detail['orderAmount'];
                    User::where('id',$wallet->user_id)->update(['wallet_amount'=>$newAmount]);
                    $update['title'] = " Payment Response";
                    $update['wallet_id'] = $wallet->id;
                    return view('web.payment.wallet_payment_response',$update)->with('success','Payment done successfully.');
                }
                else
                {
                    return view('web.payment.wallet_payment_response',$update)->with('error','Unable to proceed');
                }
            }
            else
            {
                $wallet->update($update);
                return view('web.payment_response',$detail,$book)->with('error','Your transaction was not successful.');
            }
        }
        else{
            return view('web.payment_response',$detail,$book)->with('error','Invalid payment signature found.');

        }
    }
}
