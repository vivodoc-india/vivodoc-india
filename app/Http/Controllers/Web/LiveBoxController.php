<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\LiveConsultation;
use Illuminate\Http\Request;

class LiveBoxController extends Controller
{
    public function previewConsultation($id)
    {
        $data['title'] = " Live Consultation";
        $data['booking'] =  LiveConsultation::where('id',$id)->first();
        return view('web.video-consultation-preview',$data);
    }
}
