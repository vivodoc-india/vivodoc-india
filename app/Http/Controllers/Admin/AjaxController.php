<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\District;
use App\Models\Location;
use App\Models\Profile;
use App\Models\ServiceProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
class AjaxController extends Controller
{
//   public function getCity(Request $request){
//        $cities=DB::table('districts')->where('state_id',$request->state_id)->where('active',1)->pluck('name','id');
//        return response()->json($cities);
//    }
//    public function getArea(Request $request){
//        $area=DB::table('locations')->where('city_id',$request->city_id)->pluck('name','id');
//        return response()->json($area);
//    }
    public function getCity(Request $request)
    {
        $city=District::where('state',$request->state)->get();
        return response()->json(['city'=>$city,'success'=>true]);
    }

    public function getArea(Request $request)
    {
        $area=Location::where('city',$request->city)->where('state',$request->state)->select('area')->orderby('area','asc')->get();
        return response()->json(['area'=>$area,'success'=>true]);
    }
    public function getSubCategory($role){
        $subcategory=Category::where('user_role_id',$role)->select('title','id')->get();
        return response()->json(['category'=>$subcategory,'success'=>true]);
    }
    public function getSubCategoryFilter(Request $request){
        $subcategory=DB::table('categories')->where('id',$request->category_id)->first();
        $return = explode(',', $subcategory->filter);
        return response()->json($return);
    }

    public function changeFlag(Request $request){
       if($request->table == "users")
       {
  $chkprofile=Profile::where('user_id',$request->id)->get();
      $chkserv_profile= ServiceProfile::where('user_id',$request->id)->get();
    if(count($chkprofile) == 0 || count($chkserv_profile) == 0)
    {
        return response()->json(['status'=>2 , 'message'=>'User did not complete either profile or service profile.']);
    }
    else{
        return $this->flagChange($request->table, $request->id,$request->column);
//        $get=DB::table($request->table)->where('id',$request->id)->pluck($request->column)->first();
//        if($get==1){
//            DB::table($request->table)->where('id',$request->id)->update([$request->column=>0]);
//            return response()->json(['status'=>0]);
//        }else{
//            DB::table($request->table)->where('id',$request->id)->update([$request->column=>1]);
//            return response()->json(['status'=>1]);
//        }
    }
       }
       elseif($request->table == "profiles")
       {
           $chkprofile=Profile::where('id',$request->id)->first();
           if($chkprofile!='')
           {
               return $this->flagChange($request->table, $request->id,$request->column);
           }
           else{
               return response()->json(['status'=>2 , 'message'=>'Provider\'s profile is not completed yet.']);
           }
       }
       elseif ($request->table == "service_profiles"){
           $sp= ServiceProfile::where('id',$request->id)->first();
           if(in_array($sp->user_role_id,[1,2,9]))
           {
               if($sp->mark_live==0)
               {
                   if(ServiceProfile::where('user_id',$sp->user_id)->whereIn('user_role_id',[1,2,9])->update([$request->column => 1])){
                       return response()->json(['status'=>1]);
                   }
               }
               else{
                   if(ServiceProfile::where('user_id',$sp->user_id)->whereIn('user_role_id',[1,2,9])->update([$request->column => 0])){
                       return response()->json(['status'=>0]);
                   }
               }

           }
           else{
               return $this->flagChange($request->table, $request->id,$request->column);
           }
       }
       else
       {
           return $this->flagChange($request->table, $request->id,$request->column);
//           $get=DB::table($request->table)->where('id',$request->id)->pluck($request->column)->first();
//           if($get==1){
//               DB::table($request->table)->where('id',$request->id)->update([$request->column=>0]);
//               return response()->json(['status'=>0]);
//           }else{
//               DB::table($request->table)->where('id',$request->id)->update([$request->column=>1]);
//               return response()->json(['status'=>1]);
//           }
       }

    }

    private function flagChange($table,$id,$column)
    {
        $get=DB::table($table)->where('id',$id)->pluck($column)->first();
        if($get==1){
            DB::table($table)->where('id',$id)->update([$column=>0]);
            return response()->json(['status'=>0]);
        }else{
            DB::table($table)->where('id',$id)->update([$column=>1]);
            return response()->json(['status'=>1]);
        }
    }

    // Generate PUID
    public function generatePUID(Request $request)
    {
        $user = User::findOrFail($request->id);
        if($user->puid=='')
        {
            $user->puid = "OMED".date('Y').$user->id;
            if($user->save())
            {
                return response()->json(['status'=>1]);
            }
            else{
                return response()->json(['status'=>0]);
            }
        }
        else{
            return response()->json(['status'=>1]);
        }
    }
}
