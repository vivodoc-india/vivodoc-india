<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\User;
use Session;
use App\Gallery;
use File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = " Gallery";
        $data['record'] = Gallery::where('status','1')->get();
        return view('admin.gallery.index',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] =" Create Gallery";
        return view('admin.gallery.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'image|mimes:jpg,jpeg,png',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error',$validator)->withInput();
        }


        $photo=$request->file('image');
        $image_name=rand() . '.' . $photo->getClientOriginalExtension();
        $destinationPath = public_path('web/img/gallery/');
        $thumb_img = Image::make($photo->getRealPath())->resize(800, 500);
//                $constraint->aspectRatio();
//            });
        $thumb_img->save($destinationPath.'/'.$image_name,80);
if(isset($image_name))
{
    $gallery= new Gallery;
    $gallery->image =$image_name;
    $gallery->status = $request->status;
    if($gallery->save())
    {
        return redirect()->back()->with('success','Image added to gallery.');
    }
    else{
        return redirect()->back()->with('error','Unable to add image in gallery.');
    }
}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] =" Edit Galllery";
        $data['gallery'] = Gallery::find($id);
        return view('admin.gallery.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'image' => 'image|mimes:jpg,jpeg,png',
        ]);
        $gallery=Gallery::find($id);
        if($request->hasFile('image')){
            if(file_exists(public_path('web/img/gallery/'.$gallery->image)) && $gallery->image)
                unlink(public_path('web/img/gallery/'.$gallery->image));
            $photo=$request->file('image');
            $image_name=rand() . '.' . $photo->getClientOriginalExtension();
            $destinationPath = public_path('web/img/gallery/');
            $thumb_img = Image::make($photo->getRealPath())->resize(800, 500);
//                $constraint->aspectRatio();
//            });
            $thumb_img->save($destinationPath.'/'.$image_name,80);

            $gallery->image=$image_name;
        }
        if( $gallery->save())
        {
            return redirect()->back()->with('success','Image updated to gallery.');
        }
        else{
            return redirect()->back()->with('error','Unable to update image in gallery.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery=Gallery::find($id);
        if(file_exists(public_path('web/img/gallery/'.$gallery->image)) && $gallery->image)
            unlink(public_path('web/img/gallery/'.$gallery->image));
        $gallery->delete();
        return redirect('omed/gallery')->with('success','Image deleted successfully');
    }
}
