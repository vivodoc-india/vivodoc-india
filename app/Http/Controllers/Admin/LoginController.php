<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{

    public function __construct()
    {

    }

    public function showLoginForm()
    {
$data['title']= "OMEDDr Backend";
return view('admin.login',$data);
    }

    public function login(Request $request)
    {

$validator = Validator::make($request->all(),[
    'email' => 'required|email',
    'password' => 'required|string',
]);
 if($validator->fails())
 {
     return redirect()->back()->with('error','Something went wrong.')->withErrors($validator->errors())->withInput();
 }

    if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password]))
        {

            $user = Auth::user();
             $user->update([
                'last_login_at' => Carbon::now()->toDateTimeString(),
                'last_login_ip' => $request->getClientIp()
            ]);
//                    Session::put('admin_is',$user->user_role->name);
//                    Session::put('admin_email',$user->email);
//                    Session::put('admin_id',$user->id);
//                    Session::put('admin_name',$user->name);
//            return redirect()->back()->with('success', 'User logged in.');
                    return redirect('omed/dashboard');
                }

        else {
             return redirect()->back()->with('error', 'These credentials do not match our records.');
        }
    }

    public function logout()
    {
         Session::flush();
        return redirect()->route('master-access');
    }
}
