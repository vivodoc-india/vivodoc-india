<?php

namespace App\Http\Controllers\Admin;

use App\Content;
use App\ContentCategory;
use Faker\Provider\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
Use Illuminate\Support\Facades\URL;
use Session;
use App\Http\Controllers\Controller;

class ContentController extends Controller {

    public function index(Request $request) {
        $data['title'] = " Content";
        $data['content_category'] = ContentCategory::where('title', $request->type)->first();
        if (isset($data['content_category']->id)) {
            $data['list'] = Content::where('content_category_id', $data['content_category']->id)->orderBy('meta_title', 'asc')->get();
            return view('admin.content.index', $data);
        }
        else
            return redirect()->back()->with('error','Nothing found.');
    }

    public function create(Request $request) {
        $data['content_category'] = ContentCategory::where('title', $request->type)->first();
        if (isset($data['content_category']->id)) {
            $data['data'] = new Content;
            $data['action'] = url('omed/content?type=' . $request->type);
            return view('admin.content.form', $data);
        } else
            return redirect()->back()->with('error','Nothing found.');
    }

    public function store(Request $request) {
        $folder = $request->type;
        $this->validate($request, [
            'name' => 'required|alpha_dash',
            'meta_title' => 'required',
            'file' => 'image|mimes:jpeg,png,jpg,gif|max:1024'
        ]);
        $content_category = ContentCategory::where('id', $request->content_category_id)->where('title', $request->type)->first();
        if (isset($content_category->id)) {
            $data = $request->all();
            $data['meta_title'] = ucwords($request->meta_title);
            if (isset($request->file)) {
                $fileName = time() . '.' . request()->file->getClientOriginalExtension();
                $request->file->move(public_path('uploads/' . $folder), $fileName);
            }
            if (isset($fileName))
                $data['file'] = $fileName;
            else
                unset($data['file']);
            unset($data['type']);
            $create = Content::create($data);
            return redirect('admin/content?type=' . $folder)->with('success', 'Content created successfully');
        } else
            return redirect('omed/dashboard');
    }

    public function show($id) {
        $data['data'] = Content::where('id', $id)->first();
        return view('admin.content.show', $data);
    }

    public function edit($id) {
        $data['data'] = Content::find($id);
        if (isset($data['data'] ->id)) {
            $data['content_category'] = ContentCategory::where('id', $data['data'] ->content_category_id)->first();
            $data['action'] = url('omed/content', ['id' => $id]);
            return view('admin.content.form', $data);
        } else
            return redirect('omed/dashboard');
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|alpha_dash',
            'meta_title' => 'required',
            'file' => 'image|mimes:jpeg,png,jpg,gif|max:1024'
        ]);
        $save = Content::findOrFail($id);
        $data = $request->all();
        $data['meta_title'] = ucwords($request->meta_title);
        $content_category = ContentCategory::where('id', $request->content_category_id)->first();
        $folder = $content_category->title;
        if (isset($content_category->id)) {
            if (isset($request->file)) {
                $fileName = time() . '.' . request()->file->getClientOriginalExtension();
                $request->file->move(public_path('uploads/' . $folder), $fileName);
                if ($save->file && \File::exists(public_path('uploads/' . $folder . '/' . $save->file)))
                    \File::delete(public_path('uploads/' . $folder . '/' . $save->file));
            }
            if (isset($fileName))
                $data['file'] = $fileName;
            else
                unset($data['file']);
            $save->update($data);
            return redirect('omed/content?type=' . $folder)->with('success', 'Content updated successfully');
        } else
            return redirect('omed/dashboard');
    }

    public function destroy($id) {
        $delete = Content::find($id);
        $content_category = ContentCategory::where('id', $delete->content_category_id)->first();
        $folder = $content_category->title;
        if ($delete->file && \File::exists(public_path('uploads/' . $folder . '/' . $delete->file)))
            \File::delete(public_path('uploads/' . $folder . '/' . $delete->file));
        $delete->delete();
        return redirect('omed/content?type=' . $folder)->with('success', 'Content deleted successfully');
    }
}
