<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\WalletTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PatientOmedCoinController extends Controller
{
    public function index($id)
    {
        $data['title'] = " OMED Coins";
        $data['patient'] = User::find($id);
        $data['transaction'] = WalletTransaction::where('user_id',$id)
            ->select('id','transaction_date','transaction_status','transaction_amount','transaction_type')
            ->orderByDesc('id')
            ->get();
        return view('admin.patient.wallet.index',$data);
    }

    public function show($id)
    {
        $data['title'] = " OMED Coins Transaction Detail";
        $data['transaction'] = WalletTransaction::where('id',$id)->first();
        return view('admin.patient.wallet.show',$data);
    }
    public function store(Request $request){
       $validator = Validator::make($request->all(),[
           'amount' => 'required|numeric',
           'user_id' => 'required|numeric',
           'action_type' => 'required|in:credit,debit|string',
           'remarks' => 'nullable|string',

       ]);
       if($validator->fails())
       {
           return redirect()->back()->with('error','Something went wrong. Invalid data passed.');
       }
       $wallet = User::where('id',$request->user_id)->first();
       $camount = $wallet->wallet_amount;
       if($request->action_type=="credit")
       {
        $camount = $camount + $request->amount;
       }
        if($request->action_type=="debit")
        {
            $camount = $camount - $request->amount;
        }
        $wallet->wallet_amount = $camount;
        if($wallet->save())
        {
            $create=array();
            $create['transaction_status'] ='SUCCESS';
            $create['transaction_amount'] = $request->amount;
            $create['transaction_date'] = today();
            $create['transaction_response'] = $request->remarks;
            $create['referenceId'] = 'OMEDDr';
            $create['user_id'] = $request->user_id;
            $create['transaction_type'] = $request->action_type;
            $create['payment_mode'] = 'OMEDDr Admin';
            $create['is_paid'] = 1;
            $create['transaction_number'] = Str::random(15);
            if(WalletTransaction::create($create))
            {
                return redirect()->back()->with('success','Transaction completed successfully.');
            }
            return redirect()->back()->with('error','Unable to generate transaction record.');
        }
        return redirect()->back()->with('error','Unable to perform action.');

    }

    public function destroy($id){
$data= WalletTransaction::findOrFail($id);
if($data->delete())
{
    return redirect()->back()->with('success','Data removed.');
}
return redirect()->back()->with('error','Something went wrong.');
    }
}
