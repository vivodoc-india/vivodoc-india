<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Http\Controllers\Controller;
use App\Models\State;
use App\Models\District;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Models\UserRole;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title']= " Area";
        $data['states'] = State::select('name')->get();
        return view('admin.location.area',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['url']="area.store";
        $data['method'] ="POST";
        $data['title']= "  Create Area";
        $data['page'] ="area";
        $data['states'] =State::select('name')->get();
        return view('admin.location.area_form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['state'] = $request->state;
        $data['city'] = $request->city;
        $data['area'] = $request->area;
        $data['pincode'] = $request->pincode;
        Location::create($data);
        return redirect()->back()->with('success','Area added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['url']="omed/area";
        $data['method'] ="PATCH";
        $data['title']= " Edit Area";
        $data['page'] ="area";
        $data['states'] =State::select('name')->get();
        $data['area'] = Location::find($id);
$data['city'] = District::where('state',$data['area']->state)->select('name')->get();
return view('admin.location.area_form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['state'] = $request->state;
        $data['city'] = $request->city;
        $data['area'] = $request->area;
        $data['pincode'] = $request->pincode;
        Location::where('id',$id)->update($data);
        return redirect()->back()->with('success','Area Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area=Location::find($id);
        if($area->delete())
        {
            return redirect()->back()->with('success','Record removed successfullty.');
        }
        else{
            return redirect()->back()->with('error','Unable to remove record.');
        }
    }

    public function getarea(Request $request)
    {
        $data['title']= " Area";
        $data['states'] = State::select('name')->get();
        $data['city'] = District::where('state',$request->state)->get();
        $data['list']=Location::where('state',$request->state)->where('city',$request->city)->orderby('area','asc')->get();
        return view('admin.location.area',$data);
    }
}
