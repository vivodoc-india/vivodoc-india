<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Http\Controllers\Controller;
use App\Models\State;
use App\Models\District;
use Illuminate\Http\Request;
use App\Models\UserRole;



class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title']= " District";
                $data['list'] =District::all();
        return view('admin.location.district',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['url']="district.store";
        $data['method'] ="POST";
        $data['title']= "  Create City";
        $data['page'] ="city";
        $data['states'] =State::select('name')->get();
        return view('admin.location.city_form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['state'] = $request->state;
        $data['name'] = $request->name;
        District::create($data);
        return redirect()->back()->with('success','City added.');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['url']="omed/district";
        $data['method'] ="PATCH";
        $data['title']= " Edit City";
        $data['page'] ="city";
        $data['states'] =State::select('name')->get();
        $data['city'] = District::find($id);
        return view('admin.location.city_form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['state'] = $request->state;
        $data['name'] = $request->name;
        District::where('id',$id)->update($data);
        return redirect()->back()->with('success','City updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area=District::find($id);
        if($area->delete())
        {
            return redirect()->back()->with('success','Record removed successfullty.');
        }
        else{
            return redirect()->back()->with('error','Unable to remove record.');
        }
    }
}
