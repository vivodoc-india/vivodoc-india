<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\CategoryRequest;
use DB;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\UserRole;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{    public function index()
    {
        $title= " Category";
        $category=Category::orderby('id','desc')->get();
    //    $category=DB::select("SELECT CONCAT_WS('->',p.title,c.title) as name,c.parent_id,c.featured,c.id,c.status,c.image from categories c left outer join categories p on c.parent_id=p.id order by name");
       return view('admin.category.index',compact('category','title'));
    }


    public function create()
    {
        $data['title']= " Create Category";
    $data['user_roles']=UserRole::where('id','!=',7)->where('id','!=',8)->where('id','!=',3)->where('status',1)->get();
        return view('admin.category.create',$data);
    }


    public function store(CategoryRequest $request)
    {
        $data = $request->validated();
            $photoName="";
            $directory = 'uploads/category';
            $path=public_path($directory);
        if($request->hasFile('image')) {
            $photoName = rand(). '.' . $request->file('image')->getClientOriginalExtension();
            Image::make($request->file('image'))->resize(50,50)->save($path . '/' . $photoName, 100);

        }
        $data['image'] = $directory.'/'.$photoName;
        if(Category::create($data))
        {
            return redirect()->back()->with('success','Category added successfully');
        }
        return redirect()->back()->with('error','Unable to save category.');

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $data['title']=" Edit Category";
         $category=Category::find($id);
          $data['user_roles']=UserRole::where('id','!=',7)->where('id','!=',8)->where('id','!=',3)->where('status',1)->get();
         $data['category']=$category;
         return view('admin.category.edit',$data);
    }


    public function update(CategoryRequest $request, $id)
    {

        $category=Category::find($id);
        $category->title=$request->get('title');
        $category->status=$request->get('status');
          $category->general_title =$request->get('general_title');
        $category->user_role_id=$request->get('user_role_id');

        if($request->hasFile('image')){
            if($category->image!='' && File::exists($category->image)) {
                File::delete($category->image);
            }
            $directory = 'uploads/category';
            $path=public_path($directory);
            $photoName = rand(). '.' . $request->file('image')->getClientOriginalExtension();
            Image::make($request->file('image'))->resize(50,50)->save($path . '/' . $photoName, 100);
            $category->image=$directory.'/'.$photoName;
        }
        if($request->is_private==0)
        {
            $category->secret_key='';
        }
        else{
            $category->secret_key=$request->secret_key;
        }
        $category->is_private = $request->is_private;
        if($category->save())
        {
            return redirect()->back()->with('success','Category updated successfully');
        }
        return redirect()->back()->with('error','Unable to update category.');

    }


    public function destroy($id)
    {
         $category=Category::find($id);
        if($category->image!='' && File::exists($category->image)) {
            File::delete($category->image);
        }
         $category->delete();
         return redirect('omed/category')->with('success','Category deleted successfully');
    }
}
