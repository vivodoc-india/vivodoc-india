<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Http\Controllers\Controller;
use App\Enquiry;
use Illuminate\Http\Request;
use Image;


class EnquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }
    public function index()
    {
        $data['title']= " Enquiries";
        $data['enquiry']=Enquiry::orderby('id','desc')->get();
        return view('admin.enquiry.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['title']=" Show Enquiry";
        $data['enquiry']=Enquiry::where('id',$id)->first();
        return view('admin.enquiry.show',$data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $enq=Enquiry::find($id);
        //  if(file_exists(public_path('media/category/'.$category->image)) && $category->image)
        //     unlink(public_path('media/category/'.$category->image));
        $enq->delete();
        return redirect('omed/enquiry-management')->with('success','Record removed successfully');
    }
}
