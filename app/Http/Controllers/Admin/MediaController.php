<?php

namespace App\Http\Controllers\Admin;

use App\Models\Media;
use App\Models\MediaCategory;
use Faker\Provider\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
Use Illuminate\Support\Facades\URL;
use Session;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    public function index(Request $request)
    {
        $data['title'] = " Media";
        $data['media_category']=MediaCategory::where('title',$request->type)->first();
        if(isset($data['media_category']->id)) {
            $data['list'] = Media::where('media_category_id',$data['media_category']->id)->orderBy('id', 'desc')->get();
            return view('admin.media.index', $data);
        }else return redirect('omed/dashboard');
    }
    public function create(Request $request)
    {
        $data['title'] = " Content Management";
        $data['media_category']=MediaCategory::where('title',$request->type)->first();
        if(isset($data['media_category']->id)) {
            $data['data'] = new Media;
            $data['action'] = url('omed/media?type='.$request->type);
            return view('admin.media.form', $data);
        }else return redirect('omed/dashboard');
    }
    public function store(Request $request)
    {
        $folder=$request->type;
        if($folder=='Video'){
            $this->validate($request, [
                'title' => 'required',
                'image' =>'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100040|required'
            ]);
        }else {
            $this->validate($request, [
                'title' => 'required',
                'file' => 'required|image|mimes:jpeg,png,jpg,gif'
            ]);
        }
        $media_category=MediaCategory::where('id',$request->media_category_id)->where('title',$request->type)->first();
        if(isset($media_category->id)) {
            $data = $request->all();
            $data['title'] = ucwords($request->title);
            if (isset($request->file)) {
                $fileName = time() . '.' . request()->file->getClientOriginalExtension();
                $request->file->move(public_path('uploads/'.$folder), $fileName);

            }
            if (isset($fileName))
                $data['file'] = $fileName;
            else unset($data['file']);
            unset($data['type']);
            $create = Media::create($data);
            return redirect('omed/media?type='.$folder)->with('success', 'Media created successfully');
        }else return redirect('omed/dashboard');
    }
    public function show($id)
    {
        $data['title'] = " Content Management";
        $data['data']=Media::where('id',$id)->first();
        return view('admin.media.show', $data);
    }
    public function edit($id)
    {
        $data['title'] =" Content Management";
        $data['data']=Media::find($id);
        if(isset($data['data']->id)) {
            $data['media_category']=MediaCategory::where('id',$data['data']->media_category_id)->first();
            $data['action']=route('media.update',$id);
            return view('admin.media.form', $data);
        }else return redirect('omed/dashboard');
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'file'=>'image|mimes:jpeg,png,jpg,gif'
        ]);
        $save=Media::findOrFail($id);
        $data=$request->all();
        $data['title'] = ucwords($request->title);
        $media_category=MediaCategory::where('id',$request->media_category_id)->first();
        $folder=$media_category->title;
        if(isset($media_category->id)) {
            if (isset($request->file)) {
                $fileName = time() . '.' . request()->file->getClientOriginalExtension();
                $request->file->move(public_path('uploads/'.$folder), $fileName);
                if ($save->file && \File::exists(public_path('uploads/'.$folder.'/'. $save->file)))
                    \File::delete(public_path('uploads/'.$folder.'/'. $save->file));
            }
            if (isset($fileName))
                $data['file'] = $fileName;
            else unset($data['file']);
            $save->update($data);
//            return redirect('omed/media?type='.$folder)->with('success', 'Media updated successfully');
            return redirect()->back()->with('success', 'Media updated successfully');
        }else return redirect('omed/dashboard');
    }
    public function destroy($id)
    {
        $delete=Media::find($id);
        $media_category=MediaCategory::where('id',$delete->media_category_id)->first();
        $folder=$media_category->title;
        if($delete->file && \File::exists(public_path('uploads/'.$folder.'/'.$delete->file)))
            \File::delete(public_path('uploads/'.$folder.'/'.$delete->file));
        $delete->delete();
        return redirect('omed/media?type='.$folder)->with('success','Media deleted successfully');
    }
}
