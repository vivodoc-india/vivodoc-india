<?php

namespace App\Http\Controllers\Admin;

//use App\Models\Booking;
use App\Http\Controllers\Controller;
//use App\Http\Requests\DocumentUpdateRequest;
//use App\Models\OnlineBooking;
//use App\Models\PatientEnquiry;
//use App\Models\ReviewRating;
//use Aws\Api\Service;
use App\Models\AppointmentBooking;
use App\Models\LiveConsultation;
use App\Models\ServiceProfileCategory;
use App\Models\ServiceProfileDocument;
use App\Service\ImageUpload;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use App\Models\UserRole;
use App\Models\ServiceProfile;
use App\Models\Profile;
use App\Models\User;
use App\Models\Category;
use App\Models\State;
use App\Models\District;
use App\Models\Location;
class UserManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $data['title']=" User Management";
    $data['user']=User::where('user_role_id',11)->orderby('id','desc')->get();
         return view('admin.provider.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $data['title']=" User Management";
         $data['user']=User::where('id',$id)->first();

        //  return $data['user']->profile->id;
          $service=ServiceProfile::where('user_id',$id)->get();
          $profile=Profile::where('user_id',$id)->get();
          if(count($profile) <= 0 )
          {
              return redirect()->back()->with('info','Service provider has not been updated his profile yet.');
          }
          if(count($service) <= 0)
          {
              return redirect()->back()->with('info','Service provider has not been selected any service category.');
          }
        $n=0;
    $sel_cat='';
       foreach($service as $s)
       {
           $data['services'][$n]['serv']=$s;        // $s->toArray()

        $cat= ServiceProfileCategory::where('user_id',$s->user_id)->where('service_profile_id',$s->id)->select('category_id')->get()->toArray();
//            $cat= explode(",", $s->category_id);

//           if($s->category_id !=" ")
//           {
$category=Category::whereIn('id',$cat)->where('user_role_id',$s->user_role_id)->where('status','1')->select('id','title')->get();
$documents = ServiceProfileDocument::where('user_id',$s->user_id)->where('service_profile_id',$s->id)->select('folder_name','file')->get();
$data['services'][$n]['cat']=$category;
$data['services'][$n]['rel_doc']=$documents;
//           }
$n++;
       }
        return view('admin.provider.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $data['title']=" User Management";
            $data['states']=State::select('name')->get();
          // Services And Profile Details Selected by User
        $service=ServiceProfile::where('user_id',$id)->get();
        $n=0;
    $sel_cat='';
       foreach($service as $s)
       {
           $data['services'][$n]['serv']=$s;        // $s->toArray()
           $cat_to_update=Category::where('user_role_id',$s->user_role_id)
                                    ->where('status','1')
                                    ->select('id','title')
                                    ->get();
           $data['services'][$n]['cat_to_update']=$cat_to_update;
//            $cat= explode(",", $s->category_id);
           $cat= ServiceProfileCategory::where('user_id',$s->user_id)
                                        ->where('service_profile_id',$s->id)
                                        ->select('category_id')
                                        ->get()
                                        ->toArray();
//           if($s->category_id !=" ")
//           {
$category=Category::whereIn('id',$cat)
                    ->where('user_role_id',$s->user_role_id)
                    ->where('status','1')
                    ->pluck('id')
                    ->toArray();
 $data['services'][$n]['cat']=$category;         //$category->toarray()

$cat_to_add=Category::whereNotIn('id',$cat)
                        ->where('user_role_id',$s->user_role_id)
                        ->where('status','1')
                        ->select('id','title')
                        ->get();
$data['services'][$n]['cat_to_add_new']=$cat_to_add;
//           }
$n++;
       }
         return view('admin.provider.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
$this->validate($request, [
            'category_id' => 'required|array|min:1',
            'category_id.*' =>'required|numeric',
             'reg_no' => 'nullable|string',
                'description' => 'nullable|string',
    'landmark' => 'nullable|string',
    'area' => 'nullable|string',
    'city' => 'nullable|string',
    'address'=> 'nullable|string',
    'state' => 'nullable|string',
    'min_fee' => 'nullable|numeric',
    'max_fee' => 'nullable|numeric',
    'vehicle_no' => 'nullable|string',
    'longitude' => 'nullable|string',
    'latitude' => 'nullable|string',
        ]);

        if(!isset($request->id))
{
    return redirect()->back()->with('error','Something went wrong.');
}
else
{
    $sp = ServiceProfile::withTrashed()->where('id',$request->id)->first();
  if($request->has('category_id') && count($request->category_id) > 0)
  {
      ServiceProfileCategory::whereNotIn('category_id',$request->category_id)
                                ->where('service_profile_id',$sp->id)
                                ->delete();
      for($i=0;$i<count($request->category_id);$i++)
      {
          $category['category_id']= $request->category_id[$i];
          $category['user_id']=$sp->user_id;
          $category['service_profile_id'] = $sp->id;
          $category['user_role_id'] = $sp->user_role_id;

          $isExists = ServiceProfileCategory::where('service_profile_id',$sp->id)
              ->where('user_id',$sp->user_id)
              ->where('user_role_id',$sp->user_role_id)
              ->where('category_id',$request->category_id[$i])
              ->exists();
          if(!$isExists)
          {
              ServiceProfileCategory::create($category);
          }

      }
  }
$sp->address = $request->address;
$sp->landmark = $request->landmark;
$sp->description = $request->description;
$sp->min_fee = $request->min_fee;
$sp->max_fee = $request->max_fee;
if($request->has('longitude'))
{
    $sp->longitude = $request->longitude;
}
 if($request->has('latitude'))
    {
        $sp->latitude = $request->latitude;
    }
$sp->updated_at = date('Y-m-d H:i:s');
if($request->has('state'))
{
    $sp->state = $request->state;
}
    if($request->has('city'))
    {
        $sp->city = $request->city;
    }
    if($request->has('area'))
    {
        $sp->area = $request->area;
    }
if($sp->save())
{
    return  redirect()->back()->with('success','Records updated successfully.');
}
else
{
    return  redirect()->back()->with('error','Unable to update records.');
}
}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

         $user=User::find($id);
        //  if(file_exists(public_path('media/category/'.$category->image)) && $category->image)
        //     unlink(public_path('media/category/'.$category->image));
         $user->delete();
         return redirect('omed/user-management')->with('success','User removed successfully');
    }

    // Store New Category
    public function storeNewCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|array|min:1',
            'category_id.*' =>'required|numeric',
            'related_document' =>'nullable|array|min:1',
            'related_document.*' => 'image|nullable|mimes:jpg,jpeg,png|max:500|min:100',
            'id' =>'required|numeric',
            'user_id' =>'required|numeric',
        ]);
        if($validator->failed())
        {
            return redirect()->back()->with('error','Something went wrong.')->withErrors($validator->errors())->withInput();
        }
        else
        {
            $service=ServiceProfile::withTrashed()->where('id',$request->id)->first();
            for($i=0;$i<count($request->category_id);$i++)
            {
                $category['category_id']= $request->category_id[$i];
                $category['user_id']=$request->user_id;
                $category['service_profile_id'] = $service->id;
                $category['user_role_id'] = $service->user_role_id;

                $isExists = ServiceProfileCategory::where('service_profile_id',$request->id)
                    ->where('user_id',$request->user_id)
                    ->where('user_role_id',$service->user_role_id)
                    ->where('category_id',$request->category_id[$i])
                    ->exists();
                if(!$isExists)
                {
                    ServiceProfileCategory::create($category);
                }
                else{
                    return redirect()->back()->with('error','Some categories are already added to your profile.');
                }
            }
            if($request->has('related_document'))
            {
            for($j=0;$j<count($request->related_document);$j++)
            {
                if(!$request->file('related_document')[$j]->isValid())
                {
                    return redirect()->back()->with('error','Something went wrong.');
                }
                $file = $request->file('related_document')[$j];
                $path= public_path('uploads/service/documents');
                $directory = $service->user->mobile.'_'.$service->user_role_id.$request->id;
                $upload = ImageUpload::uploadImage($file,$path,$directory);
                if($upload['status'])
                {
                    $doc['user_id'] = $request->user_id;
                    $doc['user_role_id'] = $service->user_role_id;
                    $doc['service_profile_id'] = $request->id;
                    $doc['folder_name'] = $upload['folder_name'];
                    $doc['file'] = $upload['file'];
                    ServiceProfileDocument::create($doc);
                }
                else{
                    return redirect()->back()->with('error','Some documents are not uploaded.');
                }
            }
            }
            return redirect()->back()->with('success','Categories added to your profile.');
        }
    }

    // Remove From Selected Service

    public function serviceRemove(Request $request)
    {
        $delete=ServiceProfile::find($request->id);
        if($delete->delete())
        {
    return  redirect()->back()->with('success','Service provider removed from selected service.');
}
else
{
    return  redirect()->back()->with('error','Unable to remove.');
}
    }

    // Update Images Relatted To Services
    public function updateServicesImages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpg,jpeg,png',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error',$validator->errors());
        }
        if(!isset($request->sp_id) || !isset($request->sp_ur_id) || !isset($request->image_count) || !is_numeric($request->sp_id) || !is_numeric($request->sp_ur_id) || !is_numeric($request->image_count))
        {
            return redirect()->back()->with('error' ,'Something went wrong. unable to process.');
        }
        $service = ServiceProfile:: withTrashed()->where('id',$request->sp_id)->first();
        $file = $request->file('image');
        $path= public_path('uploads/service/banner_images');
        $directory = $service->user->mobile.'_'.$request->sp_ur_id.$request->sp_id;
        $upload = ImageUpload::uploadImage($file,$path,$directory);
        $field = 'image'.$request->image_count;
        if($upload['status'])
        {
            if($service->$field!='' && File::exists(public_path($service->$field)))
            {
                File::delete(public_path($service->$field));
            }
            $image = 'uploads/service/banner_images/'.$upload['folder_name'].'/'.$upload['file'];

            if(ServiceProfile::where('id',$request->sp_id)->update([$field=>$image]))
            {
                return redirect()->back()->with('success','Image updated successfully.');
            }
            else{
                return redirect()->back()->with('error','Unable to update image.');
            }
        }
    }

    // Update Profile load view

    public function loadUpdateProfile($id)
    {
        $data['title'] = " Update Profile";
//        $ini_update=Auth::user()->initial_update;
        $data['user']=User::where('id',$id)->first();
        if($data['user']->initial_update==0)
        {
            $data['list']=UserRole::where('status',1)->where('id','!=',7)->where('id','!=',8)->where('id','!=',3)->get();
// $data['sub_category']=DB::select('SELECT c.id, c.title,u.name FROM `categories` c inner join user_roles u  where c.user_role_id = u.id and u.status=1 order by u.id asc,c.id asc ');
        }
        else{
            $user_role_sel=ServiceProfile::where('user_id',$id)->select('user_role_id')->get();

            $data['list_add']=UserRole::whereNotIn('id',$user_role_sel)->where('status',1)->where('id','!=',7)->where('id','!=',8)->where('id','!=',3)->get();
        }
        return view('admin.provider.update-profile',$data);
    }

    public function updateServiceInfo(Request $request)
{
    if(!isset($request->user_role_id) || count($request->user_role_id) < 0 )
    {
        return redirect()->back()->with('error','You need to choose at least one service.');
    }
    else {
        $data=$request->except(['_token']);

        $j=count($data['user_role_id']);

        for($i=0;$i<count($data['user_role_id']);$i++)
        {
            $rec['user_role_id']=$data['user_role_id'][$i];
            if($data['user_role_id'][$i]==6)
            {
                if(ServiceProfile::where('user_id',$request->id)->whereIn('user_role_id',[1,2,9])->exists()){
                    return redirect()->back()->with('error','Ambulance profile is not allowed.');
                }
            }
            if(in_array($data['user_role_id'][$i],[1,2,9]))
            {
                if(ServiceProfile::where('user_id',$request->id)->where('user_role_id',6)->exists()){
                    return redirect()->back()->with('error','Some profiles are not allowed with Ambulance Profile.');
                }
            }
            $rec['user_id']=$request->id;
            $rec['created_at']=date("Y-m-d h:i:s");
            $rec['updated_at']=date("Y-m-d h:i:s");
            $find= ServiceProfile::where('user_id',$request->id)->where('user_role_id',$rec['user_role_id'])->first();
            if($find!='' && $find->trashed())
            {
                $find->restore();
            }
            else{
                ServiceProfile::create($rec);
            }
            $j--;
        }
        if($j==0)
        {
            $user=User::where('id',$request->id)->first();
            if($user->initial_update == 0)
            {
                $user->initial_update = 1;
                $user->save();
            }
            return redirect()->back()->with('success','Service updated successfully.');
        }
        else {
            return redirect()->back()->with('error','Unable to update');
        }
    }
}

public function updateDocument(Request $request)
{
    $validator = Validator::make($request->all(), [
        'photo' => 'image|mimes:jpg,jpeg,png',
    ]);
    if ($validator->fails()) {
        return  redirect()->back()->with('error','Invalid File Type.')->withErrors($validator)->withInput();
    }

    // Profile Pic
    $save=Profile::where('user_id',$request->id)->first();
    $rsftu=rand();
    $photo=$request->file('photo');
    $imagename=$rsftu . '.' . $photo->getClientOriginalExtension();
    $destinationPath = public_path('uploads/profile/');
    $thumb_img = Image::make($photo->getRealPath());
    $thumb_img->save($destinationPath.'/'.$imagename,100);
    if(isset($imagename))
    {
        $propic=$imagename;
        if(!empty($save->photo))
        {
            $image_path = $destinationPath.$save->photo;
                if(File::exists($image_path)) {
                    File::deleteDirectory($image_path);
                }

        }
    }

    $update=   DB::table('profiles')->where('user_id',$request->id)->update([
        'photo' => $propic,
        'updated_at' => date("Y-m-d h:i:s")
    ]);

    if($update)
    {
        return redirect()->back()->with('success','Profile Photo uploaded successfully.');
    }
    else {
        return redirect()->back()->with('error','Unable to update profile photo.');
    }
}

    public function updatePersonalInfo(Request $request)
    {

        $data=$request->except(['_token','id','name','mobile','email']);
        $personal['name'] = $request->name;
        $personal['mobile'] = $request->mobile;
        $personal['email'] = $request->email;
        User::where('id',$request->id)->update($personal);
        $chk=Profile::where('user_id',$request->id)->count();
        if($chk > 0)
        {
            $data['updated_at']=date("Y-m-d h:i:s");
            $ins=DB::table('profiles')->where('user_id',$request->id)->update($data);
        }
        else {
            $data['created_at']=date("Y-m-d h:i:s");
            $data['updated_at']=date("Y-m-d h:i:s");
            $data['user_id']=$request->id;
            $ins=DB::table('profiles')->insertGetId($data);
        }

        if($ins > 0)
        {
            return redirect()->back()->with('success','Personal info updated. Proceed for further updates.');
        }
        else {
            return redirect()->back()->with('error','Unable to update details.');
        }

    }


    public function getAllReviews($id)
    {
        $data['title'] =" Reviews & Ratings";
        $data['record'] = ReviewRating::where('provider_id',$id)->get();
        return view('admin.provider.review-rating',$data);
    }

    public function getAllEnquiries($id)
    {
        $data['title']=" Patients Enquiries";
        $data['record'] = PatientEnquiry::where('provider_id',$id)->get();
        return view('admin.provider.patient-enquiry',$data);
    }
    public function removeReviewRatings(Request $request)
    {
        $data=ReviewRating::find($request->record_id);
        $data->delete();
        return redirect()->back()->with('success','Data removed successfully.');
    }

    public function removeEnquiry(Request $request)
    {
        $data=PatientEnquiry::find($request->record_id);
        $data->delete();
        return redirect()->back()->with('success','Data removed successfully.');
    }
    public function getEnquiryDetails($id)
    {
        $data['title'] =" Enquiry Detail";
        $data['detail'] =PatientEnquiry::where('id',$id)->first();
        return view('admin.provider.enquiry-detail',$data);
    }

    public function saveAdminReply(Request $request)
    {
        $update=PatientEnquiry::where('id',$request->reply_id)->update([
            'admin_reply'=>$request->admin_reply,
            'admin_updated_at' => date('Y-m-d H:i:s'),
            ]);
        if($update)
        {
            return redirect()->back()->with('success','Reply saved.');
        }
        else{
            return redirect()->back()->with('error','Unable to save reply.');
        }
    }

    public function markAsTop(Request$request)
    {
        $serv_profile= ServiceProfile::where('id',$request->id)->first();
        if(!empty($serv_profile))
        {
            if($serv_profile->mark_top==0)
            {
                $serv_profile->mark_top = 1;
                if($serv_profile->save())
                {
                    return redirect()->back()->with('success','Marked as top service provider in selected category.');
                }
            }
            else{
                $serv_profile->mark_top = 0;
                if($serv_profile->save())
                {
                    return redirect()->back()->with('success','Removed top service provider in selected category.');
                }
            }
        }
        else
        {
            return redirect()->back()->with('error','Selected Profile not available.');
        }
    }

    public function manageButtonAccess(Request $request)
    {
        $serv_profile= ServiceProfile::where('id',$request->id)->first();
        if(!empty($serv_profile))
        {
          if($request->has('btn_call'))
          {
              $serv_profile->btn_call=1;
          }
          else{
              $serv_profile->btn_call=0;
          }
            if($request->has('btn_video'))
            {
                $serv_profile->btn_video=1;
            }
            else{
                $serv_profile->btn_video=0;
            }
            if($request->has('btn_booknow'))
        {
            $serv_profile->btn_booknow=1;
        }
        else{
            $serv_profile->btn_booknow=0;
        }
            if($request->has('btn_enquiry'))
            {
                $serv_profile->btn_enquiry=1;
            }
            else{
                $serv_profile->btn_enquiry=0;
            }
                if($serv_profile->save())
                {
                    return redirect()->back()->with('success','Data updated successfully.');
                }

            else{

                    return redirect()->back()->with('success','unable to update records.');
            }
        }
        else
        {
            return redirect()->back()->with('error','Selected Profile not available.');
        }
    }

    // Get All Offline bookings
    public function getAllOfflineBookings($id)
    {
        $data['title']=" Offline Bookings";
        $data['record'] = AppointmentBooking::where('provider_id',$id)->orderby('id','desc')->get();
        return view('admin.provider.offline-booking',$data);
    }

    // Get Offline booking Details
    public function getBookingDetails($id)
    {
        $data['title'] =" Booking Detail";
        $data['detail'] =AppointmentBooking::where('id',$id)->first();
        return view('admin.provider.booking-detail',$data);
    }

    // Remove Offline Booking
    public function removeOfflineBooking(Request $request)
    {
        $data=AppointmentBooking::find($request->record_id);
        $data->delete();
        return redirect()->back()->with('success','Data removed successfully.');
    }

    // Get All Offline bookings
    public function getAllOnlineBookings($id)
    {
        $data['title']=" Live Consultation Bookings";
        $data['record'] = LiveConsultation::where('provider_id',$id)->orderby('id','desc')->get();
        return view('admin.provider.online-booking',$data);
    }

    // Get Offline booking Details
    public function getOnlineBookingDetails($id)
    {
        $data['title'] =" Booking Detail";
        $data['detail'] =LiveConsultation::where('id',$id)->first();
        return view('admin.provider.online-booking-detail',$data);
    }

    // Remove Offline Booking
    public function removeOnlineBooking(Request $request)
    {
        $data=LiveConsultation::find($request->record_id);
        $data->delete();
        return redirect()->back()->with('success','Data removed successfully.');
    }
}
