<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;
class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title']= " Settings";
        $data['settings']=Setting::all();
        return view('admin.setting.settings',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title']= " Create New Config";
        $data['method']="post";
        $data['url'] = route('manage-settings.store');
        return view('admin.setting.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->has('param'))
        {
            return redirect()->back()->with('error','Param is required')->withInput();
        } if(!$request->has('data'))
        {
            return redirect()->back()->with('error','Data is required')->withInput();
        }
        $data['param']  =$request->param;
        $data['data'] = $request->data;
        if(Setting::create($data))
        {
            return redirect()->back()->with('success','New Configuration added.');
        }
        else{
            return redirect()->back()->with('error','Unable to save data')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title']= " Edit Config Setting";
        $data['setting'] = Setting::where('id',$id)->first();
        $data['method']="post";
        $data['url'] = route('manage-settings.update',$id);
        return view('admin.setting.form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$request->has('param'))
        {
            return redirect()->back()->with('error','Param is required')->withInput();
        } if(!$request->has('data'))
    {
        return redirect()->back()->with('error','Data is required')->withInput();
    }
        $setting = Setting::findOrFail($id);
        $setting->param = $request->param;
        $setting->data = $request->data;
        if($setting->save())
        {
            return redirect()->back()->with('success','Configuration setting updated.');
        }
        else{
            return redirect()->back()->with('error','Unable to update settings.')->withInput();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $conf=Setting::find($id);
        $conf->delete();
        return redirect()->back()->with('success','Record removed successfully');
    }
}
