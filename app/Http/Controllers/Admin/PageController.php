<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = " Pages";
        $data['list'] = Page::select('id','title','status')->get();
        return view('admin.page.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = " Add New Page";
        return view('admin.page.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'status' => 'required|numeric',
            'description'=> 'required|string',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error',$validator)->withInput();
        }

        $data['title'] = $request->title;
        $data['status'] = $request->status;
        $data['description'] = $request->description;
        $data['slug'] = Str::slug($data['title']);
        if(Page::where('slug',$data['slug'])->count())
        {
            return redirect()->back()->with('error','Slug for given title is already exists.');
        }
        else{
            if(Page::create($data))
            {
                return redirect()->back()->with('success','Page added.');
            }
            else{
                return redirect()->back()->with('error','Unable to add page.');
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] =" Edit Page";
        $data['page'] = Page::findOrFail($id);
        return view('admin.page.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'status' => 'required|numeric',
            'description'=> 'required|string',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error',$validator)->withInput();
        }

        $data['title'] = $request->title;
        $data['status'] = $request->status;
        $data['description'] = $request->description;
        $data['slug'] = Str::slug($data['title']);
        if(Page::where('slug',$data['slug'])->where('id','!=',$id)->count())
        {
            return redirect()->back()->with('error','Slug for given title is already exists.');
        }
        else{
            if(Page::where('id',$id)->update($data))
            {
                return redirect()->back()->with('success','Page updated.');
            }
            else{
                return redirect()->back()->with('error','Unable to update page.');
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page=Page::findorFail($id);
        $page->delete();
        return redirect('omed/pages')->with('success','Page removed.');
    }
}
