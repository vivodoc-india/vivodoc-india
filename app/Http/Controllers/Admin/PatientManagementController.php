<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use App\Http\Controllers\Controller;
use App\Models\AppointmentBooking;
use App\Models\LiveConsultation;
use App\Models\MedicalData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Image;
use App\Models\UserRole;
//use App\MedicalHistory;
use App\Models\Profile;
use App\Models\User;

class PatientManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title']=" Patient Management";
        $data['user']=User::where('user_role_id',3)->orderby('id','desc')->get();
        return view('admin.patient.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['title']=" Patient Management";

        $data['user']=User::where('id',$id)->first();

        $medical=MedicalData::where('user_id',$id)->where('parent',0)->orderby('updated_at','desc')->get();;
        $data['medical_data']=$medical;
        $profile=Profile::where('user_id',$id)->get();

        if(count($profile) <= 0 )
        {
            return redirect()->back()->with('info','Patient has not been updated his profile yet.');
        }

        return view('admin.patient.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title']=" Patient Management";
        $data['user']=User::where('id',$id)->first();
        return view('admin.patient.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=$request->except(['_token','_method']);
        $chk=Profile::where('user_id',$id)->get();
        if(count($chk) > 0)
        {
            $data['updated_at']=date("Y-m-d h:i:s");
            $ins=DB::table('profiles')->where('user_id',$id)->update($data);
        }
        else {
            $data['created_at']=date("Y-m-d h:i:s");
            $data['updated_at']=date("Y-m-d h:i:s");
            $data['user_id']=$id;
            $ins=DB::table('profiles')->insertGetId($data);
        }
        if($ins > 0)
        {
            return redirect()->back()->with('success','Details updated.');
        }
        else {
            return redirect()->back()->with('error','Unable to update details.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::findOrFail($id);
        if(!empty($user->medical_history->directory_name))
        {
            $path=public_path('uploads/patientprofile/medical_data/'.$user->medical_history->directory_name);
            if(File::exists($path)) {
                File::deleteDirectory($path);
            }
        }

        if($user->delete())
        {
            return redirect('omed/patient-management')->with('success','Patient removed successfully');
        }
        else
        {
            return redirect('omed/patient-management')->with('error','Unable to remove this patient.');
        }
    }
    public function browsMedicalDirectory($id)
    {
        $data['files']=MedicalData::where('parent',$id)->orderby('created_at','desc')->get();
        $data['doc'] = MedicalData::where('id',$id)->first();
        $data['title'] ="OMEDDr| Brows Medical Directory";
        return view('admin.patient.medical_data',$data);
    }
    public function removeMedicalDirectory(Request $request)
    {
        $data=MedicalData::find($request->record_id);
        $path=public_path('uploads/patientprofile/medical_data/'.$data->directory_name);
        if(File::exists($path)) {
            File::deleteDirectory($path);
        }
        $data->delete();
        return redirect()->back()->with('success','Directory deleted successfully');
    }

    // Get All Offline bookings
    public function getAllOfflineBookings($id)
    {
        $data['title']=" Offline Bookings";
        $data['record'] = AppointmentBooking::where('patient_id',$id)->get();
        return view('admin.patient.offline-booking',$data);
    }

    // Get Offline booking Details
    public function getBookingDetails($id)
    {
        $data['title'] =" Booking Detail";
        $data['detail'] =AppointmentBooking::where('id',$id)->first();
        return view('admin.patient.booking-detail',$data);
    }

    // Remove Offline Booking
    public function removeOfflineBooking(Request $request)
    {
        $data=AppointmentBooking::find($request->record_id);
        $data->delete();
        return redirect()->back()->with('success','Data removed successfully.');
    }

    // Get All Offline bookings
    public function getAllOnlineBookings($id)
    {
        $data['title']=" Live Consultation Bookings";
        $data['record'] = LiveConsultation::where('patient_id',$id)->orderby('id','desc')->get();
        return view('admin.patient.online-booking',$data);
    }

    // Get Offline booking Details
    public function getOnlineBookingDetails($id)
    {
        $data['title'] =" Booking Detail";
        $data['detail'] =LiveConsultation::where('id',$id)->first();
        return view('admin.patient.online-booking-detail',$data);
    }

    // Remove Offline Booking
    public function removeOnlineBooking(Request $request)
    {
        $data=LiveConsultation::find($request->record_id);
        $data->delete();
        return redirect()->back()->with('success','Data removed successfully.');
    }

}
