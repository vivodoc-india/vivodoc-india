<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Http\Controllers\Controller;
use App\Models\State;
use Illuminate\Http\Request;
use App\Models\UserRole;


class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title']= " State";
        $data['list'] = State::all();
        return view('admin.location.state',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['url']="state.store";
        $data['method'] ="POST";
        $data['title']= "  Create State";
        $data['page'] ="state";
        return view('admin.location.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['name'] = $request->state;
        State::create($data);
        return redirect()->back()->with('success','State added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['url']="state.update";
        $data['method'] ="PUT";
        $data['title']= " Edit State";
        $data['page'] ="state";
        $data['state'] = State::find($id);
        return view('admin.location.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['name'] = $request->state;
        State::where('id',$id)->update($data);
        return redirect()->back()->with('success','State Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area=State::find($id);
        if($area->delete())
        {
            return redirect()->back()->with('success','Record removed successfullty.');
        }
        else{
            return redirect()->back()->with('error','Unable to remove record.');
        }
    }
}
