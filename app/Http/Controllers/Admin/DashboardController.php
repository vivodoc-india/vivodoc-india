<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\User;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $data['title']= " Admin";
        return view('admin.home',$data);
    }
}
