<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Http\Resources\BookingScheduleResource;
use App\Http\Resources\ServiceProfileResource;
use App\Models\Event;
use App\Models\ServiceProfile;
use Illuminate\Http\Request;

class DefController extends Controller
{
    public function getProviderDetail($id){
        $provider=  ServiceProfile::with(['user','categories'])->where('id',$id)->first();
        $data['provider'] = new ServiceProfileResource($provider);
        $schedule=Event::where('user_id',$provider->user_id)->select('id','title','start_date','end_date','is_off')->orderby('id','desc')->get();
        $data['schedules'] = BookingScheduleResource::collection($schedule);
        return sendSuccessResponse("",$data);

    }
}
