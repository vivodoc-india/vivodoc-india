<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserLoginRequest;
use App\Models\Profile;
use App\Models\ServiceProfile;
use App\Models\ServiceProfileCategory;
use App\Models\ServiceProfileDocument;
use App\Models\Setting;
use App\Models\User;
use App\Service\AppNotificationService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class DashboardController extends Controller
{
    private $config;
    public function __construct()
    {
        $setting = Setting::all();
        foreach ($setting as $data)
            $this->config[$data->param] = $data->data;
    }
    public function getAppVersion()
    {
        $version=Setting::where('param','app_version')->first();
        if($version!='')
        {
            $response['version']=$version->data;
            $response['appointment_fee'] = $this->config['appointment_fee'];
            $response['appointment_discount'] = $this->config['appointment_discount'];
            $response['help_support'] = $this->config['help_support'];
            return sendSuccessResponse("",$response);
        }
        else{
           return sendFailureMessage("No Data Available.");
        }
    }
    public function login(UserLoginRequest $request)
    {
        if(is_numeric($request->user_id))
        {
            if(Auth::attempt(['mobile'=>request('user_id'), 'password'=>request('password')]))
            {
                $details=array();
                $user = Auth::user();
                $user->update([
                    'device_id' => $request->device_id,
                    'last_login_at' => Carbon::now()->toDateTimeString(),
                    'last_login_ip' => $request->getClientIp()
                ]);
                $details=$this->getUerDetails($user);
//                $user->update([
//                    'last_login_at' => Carbon::now()->toDateTimeString(),
//                    'last_login_ip' => $request->getClientIp()
//                ]);
                $details['token'] =$user->createToken(request('password'))->accessToken;
                $message="Login Successful.";
                return sendSuccessResponse($message,$details);
            }
            else
            {
               return sendFailureMessage("invalid login credentials");
            }
        }
        elseif(filter_var($request->user_id, FILTER_VALIDATE_EMAIL))
        {
            if(Auth::attempt(['email'=>request('user_id'), 'password'=>request('password')]))
            {
                    $details=array();
                    $user = Auth::user();
                     $details=$this->getUerDetails($user);
                $user->update([
                    'device_id' => $request->device_id,
                    'last_login_at' => Carbon::now()->toDateTimeString(),
                    'last_login_ip' => $request->getClientIp()
                ]);
                    $details['token'] =$user->createToken(request('password'))->accessToken;
                    $message="Login Successful.";
                    return sendSuccessResponse($message,$details);

            }
            else{
               return sendFailureMessage("Invalid login credentials");
            }
        }
        else
        {
            return sendFailureMessage("Invalid login credentials");
        }
    }

    public function registerUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'mobile' => 'nullable|numeric|unique:users|digits:10',
            'email' => 'nullable|email|unique:users',
            'password' => 'required|string',
            'confirm_pass' => 'required|same:password',
            'user_role_id' => 'required|numeric',
            'device_id' => 'sometimes|string',
        ]);
        if ($validator->fails()) {
            return sendValidationError($validator->errors()->toArray());
        }
        if (!$request->hasAny(['mobile', 'email'])) {
            return sendFailureMessage("Please enter either mobile number or email id ");
        }

        $input = $request->only('name', 'mobile', 'email', 'password', 'user_role_id','device_id');
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        if ($user) {
            if ($request->user_role_id == 3) {
                $user->approval = 1;
                $user->puid = "OMED" . date('Y') . $user->id;
                $user->save();
            }
            $profile['user_id'] = $user->id;
            Profile::create($profile);

                if ($user->mobile_otp != '' && $user->mobile_otp > 0) {
                    $mob_otp = $user->mobile_otp;
                } else {
                    $mob_otp = rand(1000, 9999);
                    User::where('id', $user->id)->update(['mobile_otp' => $mob_otp]);
                }

                $text = "Dear user, your otp for mobile verification is $mob_otp. Regards OMEDDr.";
                sms_sender(trim($request->mobile), $text, $this->config['sms_common_otp']);

                $details = $this->getUerDetails($user);
                $message = "Registration completed.";
                $details['token'] = $user->createToken($input['password'])->accessToken;
                    $user->last_login_at = Carbon::now()->toDateTimeString();
                    $user->last_login_ip = $request->getClientIp();

                     $user->save();

                return sendSuccessResponse($message, $details);
            } else {
                return sendFailureMessage("Unable to register.");
            }
        }


    // Forget Password Section Starts here
    public function validateMobile(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'mobile' => 'required|numeric|digits:10',
        ]);
        if ($validator->fails()) {
            return sendValidationError($validator->errors()->toArray());
        }
        $user=User::where('mobile',$request->mobile)->first();
        if($user!='')
        {
            if ($user->mobile_otp!='' && $user->mobile_otp > 0) {
                $mob_otp = $user->mobile_otp;
            } else {
                $mob_otp = rand(1000, 9999);
                User::where('id',$user->id)->update(['mobile_otp'=>$mob_otp]);
            }
            $text = "Dear user, your otp for forget password is $mob_otp. Regards OMEDDr.";
            sms_sender(trim($request->mobile), $text, $this->config['sms_common_otp']);


            $details['otp']=$mob_otp;
            $details['mobile']=$request->mobile;
            $details['user_id']=$user->id;
            return sendSuccessResponse("OTP has been sent.",$details);
        }
        else{
           return sendFailureMessage("No user found.");
        }
    }

    // Verify forget password otp
    public function verifyOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' =>'required|numeric',
            'otp' => 'required|numeric|digits:4',
        ]);
        if ($validator->fails()) {
           return sendValidationError($validator->errors()->toArray());
        }

        return  $this->optVerification($request->user_id,$request->otp);
    }

    // Reset Password
    public function passwordReset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'password' =>'required|string',
            'confirm_pass' => 'required|same:password'
        ]);
        if ($validator->fails()) {
           return sendValidationError($validator->errors()->toArray());
        }
        $input['password']=$request->password;
        $input['password']=bcrypt($input['password']);
        $update=User::where('id',$request->user_id)->update(['password'=>$input['password']]);
        if($update)
        {
            return sendSuccessResponse("Your password has been changed. Kindly login with new password.");
        }
        else{
            return sendFailureMessage("Unable to update your password.");
        }
    }

    // Change Password
    public function passwordChange(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' =>'required',
            'password' =>'required',
            'confirm_pass' => 'required|same:password'
        ]);
        if ($validator->fails()) {
          return sendValidationError($validator->errors()->toArray());
        }
        $input['password']=$request->password;
        $input['password']=bcrypt($input['password']);
        $user=User::where('id',auth()->user()->id)->first();
        if(Hash::check($request->current_password, $user->password))
        {
            $user->password = $input['password'];
            if($user->save())
            {
              return sendSuccessResponse("Your password has been changed.");
            }
            else{
                return sendFailureMessage("Unable to change your password..");
            }
        }
        else
        {
            return sendFailureMessage("Invalid current password.");
        }

    }

    // Common Function for OTP Verification
    private function optVerification($user_id,$value)
    {
        if(isset($user_id)) {
            $user=User::where('id',$user_id)->first();
            if($user=='')
            {
                return sendFailureMessage("Unknown user access found.");
            }

                if($user->mobile_otp==$value)
                {
                    User::where('id',$user_id)->update([
                        'mobile_verified'=>'1',
                        'mobile_otp' => '0',
                    ]);
                    $details['user_id']=$user_id;
                    return sendSuccessResponse("OTP verification successful.",$details);

                    }
                else{
                    return sendFailureMessage("Invalid OTP.");
                }
        }
        else{
           return sendFailureMessage("Unknown user access found.");
        }
    }

    // Mobile Verification -Send OTP
    public function sendOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|numeric|digits:10',
        ]);
        if ($validator->fails()) {
          return sendValidationError($validator->errors()->toArray());
        }
     $user = User::where('id',auth()->user()->id)->where('mobile',$request->mobile)->first();
        if($user=='')
        {
            return sendFailureMessage("User not found.");
        }
                if ($user->mobile_otp!='' && $user->mobile_otp > 0) {
                    $mob_otp = $user->mobile_otp;
                } else {
                    $mob_otp = rand(1000, 9999);
                  $user->mobile_otp = $mob_otp;
                  $user->save();
                }

        $text = "Dear user, your otp for mobile verification is $mob_otp. Regards OMEDDr.";
        sms_sender(trim($request->mobile), $text, $this->config['sms_common_otp']);
                $details['otp']=$mob_otp;
                return sendSuccessResponse("OTP has been sent.",$details);
    }

    protected function getUerDetails($user)
        {
            $details=array();
            $details['id']=$user->id;
            $details['name']= $user->name;
            $details['email'] = $user->email;
            $details['mobile'] = (int) $user->mobile;
            $details['approval_status'] =$user->approval==1?1:0;
            $details['user_role_id'] = (int) $user->user_role_id;
            $details['mobile_verified'] = (int) $user->mobile_verified;
            if($user->user_role_id==3)
            {
                $details['wallet_amount'] = $user->wallet_amount;
            }
            return $details;
        }

        public function dataService()
        {
            $users=DB::table('users_copy')->where('user_role_id',11)->select('id')->get();
            foreach($users as $user) {
                $serv = DB::table('service_profiles_copy')
                                ->where('user_id', $user->id)
                                ->select('id', 'user_id', 'user_role_id','reg_no','description','landmark','area','city','state',
                                'address','min_fee','max_fee','mark_top','rating','btn_call','btn_video','btn_enquiry','btn_booknow','mark_live','is_available','vehicle_no',
                                'longitude','latitude','created_at','updated_at')
                                ->get();
//                return $serv;
//                ServiceProfile::create($serv);
                foreach ($serv as $sr) {
                   $input['id'] = $sr->id;
                   $input['user_id'] = $sr->user_id;
                   $input['user_role_id'] = $sr->user_role_id;
                   $input['reg_no'] = $sr->reg_no;
                   $input['description'] = $sr->description;
                   $input['landmark'] = $sr->landmark;
                   $input['area'] = $sr->area;
                   $input['city'] = $sr->city;
                   $input['state'] = $sr->state;
                   $input['address'] = $sr->address;
                   $input['min_fee'] = $sr->min_fee;
                   $input['max_fee'] = $sr->max_fee;
                   $input['mark_top'] = $sr->mark_top;
//                   $input['rating'] = $sr->rating;
                   $input['btn_call'] = $sr->btn_call;
                   $input['btn_video'] = $sr->btn_video;
                   $input['btn_enquiry'] = $sr->btn_enquiry;
                   $input['btn_booknow'] = $sr->btn_booknow;
                   $input['mark_live'] = $sr->mark_live;
                   $input['is_available'] = $sr->is_available;
                   $input['vehicle_no'] = $sr->vehicle_no;
                   $input['longitude'] = $sr->longitude;
                   $input['latitude'] = $sr->latitude;
                   $input['created_at'] = $sr->created_at;
                   $input['updated_at'] = $sr->updated_at;

                    ServiceProfile::create($input);
                }
            }
        }
        public function dataRoll()
        {
            $users =DB::table('users_copy')->where('user_role_id',11)->select('id')->get();
foreach($users as $user)
{
    $serv = DB::table('service_profiles_copy')->where('user_id',$user->id)->select('id','user_id','user_role_id','category_id')->get();
   foreach($serv as $sr)
   {
       if($sr->category_id!='')
       {
        $categories = explode(',',$sr->category_id);
        for($i=0;$i<count($categories);$i++){
            if($categories[$i]!='')
            {
                if(!ServiceProfileCategory::where('user_id',$sr->user_id)->where('user_role_id',$sr->user_role_id)->where('service_profile_id',$sr->id)->where('category_id',$categories[$i])->exists())
                {
                    $servicecat['user_id'] = $sr->user_id;
                    $servicecat['user_role_id'] = $sr->user_role_id;
                    $servicecat['service_profile_id'] = $sr->id;
                    $servicecat['category_id'] = $categories[$i];
                    ServiceProfileCategory::create($servicecat);
                }

            }

        }
       }

   }
}
        }

    public function documentRoll()
    {
        ini_set('max_execution_time', -1);
        $users = DB::table('users_copy')->where('user_role_id',11)->select('id')->get();
        foreach($users as $user)
        {
            $serv = DB::table('service_profiles_copy')->where('user_id',$user->id)->select('id','user_id','user_role_id','related_document')->get();
            foreach($serv as $sr)
            {
                $usr =User::where('id',$sr->user_id)->first();
                $directory = $usr->mobile.'_'.$sr->user_role_id.$sr->id;
                $path= public_path('uploads/service/documents');

                if(!File::exists($path.'/'.$directory))
                {
                    File::makeDirectory($path.'/'.$directory,0755,true,true);
                }
                if($sr->related_document!='')
                {
                    $documents = explode(',',$sr->related_document);
                    for($i=0;$i<count($documents);$i++){
                        if($documents[$i]!='')
                        {
     $source = public_path('uploads/prev/service-images/related_document/'.$documents[$i]);

                           $dest = $path.'/'.$directory.'/'.$documents[$i];
                            if(File::exists($source) && File::copy($source,$dest))
                            {
                                $servicecat['user_id'] = $sr->user_id;
                                $servicecat['user_role_id'] = $sr->user_role_id;
                                $servicecat['service_profile_id'] = $sr->id;
                                $servicecat['file'] = $documents[$i];
                                $servicecat['folder_name'] = $directory;
                                ServiceProfileDocument::create($servicecat);
                            }
                        }

                    }
                }

            }
        }
    }

    public function bannerRoll()
    {
        ini_set('max_execution_time', -1);
        $users = DB::table('users_copy')->where('user_role_id',11)->select('id')->get();
        foreach($users as $user)
        {
        $serv = DB::table('service_profiles_copy')->where('user_id',$user->id)->select('id','user_id','user_role_id','image1','image2','image3','image4','image5')->get();
            foreach($serv as $sr)
            {
                $usr =User::where('id',$sr->user_id)->first();
                $directory = $usr->mobile.'_'.$sr->user_role_id.$sr->id;
                $path= public_path('uploads/service/banner_images');
if($sr->image1!='' || $sr->image2!='' || $sr->image3!='' || $sr->image4!='' || $sr->image5!='')
{
    if(!File::exists($path.'/'.$directory))
    {
        File::makeDirectory($path.'/'.$directory,0755,true,true);
    }
}

                if($sr->image1!='')
                {

                            $source = public_path('uploads/prev/service-images/category_images/'.$sr->image1);
                            $dest = $path.'/'.$directory.'/'.$sr->image1;
                            if(File::exists($source) && File::copy($source,$dest))
                            {
                                $image1 = 'uploads/service/banner_images/'.$directory.'/'.$sr->image1;

                            ServiceProfile::where('user_id',$sr->user_id)->where('user_role_id',$sr->user_role_id)->update(['image1'=>$image1]);
                        }

                    }
                if($sr->image2!='')
                {

                    $source = public_path('uploads/prev/service-images/category_images/'.$sr->image2);
                    $dest = $path.'/'.$directory.'/'.$sr->image2;
                    if(File::exists($source) && File::copy($source,$dest))
                    {
                        $image2 = 'uploads/service/banner_images/'.$directory.'/'.$sr->image2;

                        ServiceProfile::where('user_id',$sr->user_id)->where('user_role_id',$sr->user_role_id)->update(['image2'=>$image2]);
                    }

                }
                if($sr->image3!='')
                {

                    $source = public_path('uploads/prev/service-images/category_images/'.$sr->image3);
                    $dest = $path.'/'.$directory.'/'.$sr->image3;
                    if(File::exists($source) && File::copy($source,$dest))
                    {
                        $image3 = 'uploads/service/banner_images/'.$directory.'/'.$sr->image3;

                        ServiceProfile::where('user_id',$sr->user_id)->where('user_role_id',$sr->user_role_id)->update(['image3'=>$image3]);
                    }

                }
                if($sr->image4!='')
                {

                    $source = public_path('uploads/prev/service-images/category_images/'.$sr->image4);
                    $dest = $path.'/'.$directory.'/'.$sr->image4;
                    if(File::exists($source) && File::copy($source,$dest))
                    {
                        $image4 = 'uploads/service/banner_images/'.$directory.'/'.$sr->image4;

                        ServiceProfile::where('user_id',$sr->user_id)->where('user_role_id',$sr->user_role_id)->update(['image4'=>$image4]);
                    }

                }
                if($sr->image5!='')
                {

                    $source = public_path('uploads/prev/service-images/category_images/'.$sr->image5);
                    $dest = $path.'/'.$directory.'/'.$sr->image5;
                    if(File::exists($source) && File::copy($source,$dest))
                    {
                        $image5 = 'uploads/service/banner_images/'.$directory.'/'.$sr->image5;

                        ServiceProfile::where('user_id',$sr->user_id)->where('user_role_id',$sr->user_role_id)->update(['image5'=>$image5]);
                    }

                }
                }
            }

            }
       public function providerPhoto()
       {
           $users = DB::table('users_copy')->where('user_role_id',3)->select('id')->get();
           foreach($users as $user){
               $propic = DB::table('patient_profiles')->where('user_id',$user->id)->select('user_id','photo')->get();
               foreach($propic as $prev_pic)
               {
                   $usr =User::where('id',$prev_pic->user_id)->first();

                   if($prev_pic->photo!='')
                   {
                       $path = public_path('uploads/profile/');
                       $source = public_path('uploads/prev/patientprofile/'.$prev_pic->photo);
                       $dest = $path.'/'.$prev_pic->photo;
                       if(File::exists($source) && File::copy($source,$dest)) {
                           Profile::where('user_id', $prev_pic->user_id)->update(['photo' => $prev_pic->photo]);
                   }

                   }
               }
           }
       }

//public function notify(){
//        $notification = new AppNotificationService();
//        return $notification->sendToSingleUser();
//}
}
