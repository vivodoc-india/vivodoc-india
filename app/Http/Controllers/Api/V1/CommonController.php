<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewRatingRequest;
use App\Http\Resources\Banner;
use App\Http\Resources\BookingScheduleResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\ParentCategoryResource;
use App\Http\Resources\ProviderProfileResource;
use App\Http\Resources\ServiceProfileResource;
use App\Http\Resources\SubCategoryResource;
use App\Http\Resources\UserResource;
use App\Models\AmbulanceBooking;
use App\Models\AppointmentBooking;
use App\Models\Category;
use App\Models\District;
use App\Models\Event;
use App\Models\LiveConsultation;
use App\Models\Location;
use App\Models\Media;
use App\Models\ServiceProfile;
use App\Models\Setting;
use App\Models\State;
use App\Models\User;
use App\Models\UserRole;
use App\Service\AppNotificationService;
use App\Service\ConsultationQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class CommonController extends Controller
{
    private $config;

    public function __construct()
    {
        $setting = Setting::all();
        foreach ($setting as $data)
            $this->config[$data->param] = $data->data;
    }
    public function parentCategories()
    {
        $cat = UserRole::whereNotIn('id',[3,7,8])->select('id','name','prefix','image')->get();
            $detail['parent_category'] = ParentCategoryResource::collection($cat);
        return sendSuccessResponse("",$detail);
    }

    public function banners()
    {
        $banner = Media::where('media_category_id',3)
                        ->where('status',1)
                        ->select('id','title','file')->orderByDesc('id')->get();
        $data['banners'] = Banner::collection($banner);
        return sendSuccessResponse("",$data);
    }

    public function subCategories($id)
    {
        $subcat = Category::where('user_role_id',$id)->where('status',1)->orderby('title','asc')->get();
        $detail['sub_category']= SubCategoryResource::collection($subcat);
        return sendSuccessResponse("",$detail);
    }

    public function verifyPrivateMembership(Request $request)
    {
        $validator = Validator::make($request->all(),[
           'secret_key' => 'required|string',
            'category' => 'required|numeric',
        ]);
        if($validator->fails())
        {
            return sendValidationError($validator->errors()->toArray());
        }
        $cat = Category::where('id',$request->category)->first();
        if($request->has('secret_key') && $cat->secret_key == $request->secret_key)
        {
            return sendSuccessResponse('Your membership has been verified. Now you have access providers of this category.');
        }
        else{
            return sendFailureMessage('Invalid membership key.');
        }

    }
    public function currentQueue($user,$sp)
    {
        $data['today_consultation'] = ConsultationQueue::countRequests($user);
        $data['provider_status']=ConsultationQueue::providerStatus($user,$sp);
        return sendSuccessResponse("",$data);

    }

    public function liveDoctors(){
       $livedr = ServiceProfile::with(['user'])->whereHas('user', function ($query) {
           $query->where('users.approval', 1);})->where('user_role_id',1)->where('mark_live',1)->limit(6)->get();
       $data['live_doctor']= ProviderProfileResource::collection($livedr);

        return sendSuccessResponse("",$data);
    }
    public function topProviders(){
        $cat = UserRole::whereNotIn('id',[3,7,8])->select('id','name')->get();
        foreach($cat as $top_prov)
        {
            $top = ServiceProfile::with(['user'])->whereHas('user', function ($query) {
            $query->where('users.approval', 1);})->where('user_role_id',$top_prov->id)->where('mark_top',1)->limit(6)->get();
           if($top_prov->id==9)
           {
               $data['top_other_'.strtolower(str_replace(' ','_',$top_prov->name))] = ProviderProfileResource::collection($top);
           }
            $data['top_'.strtolower(str_replace(' ','_',$top_prov->name))] = ProviderProfileResource::collection($top);

        }

        return sendSuccessResponse("",$data);
    }

    public function providersListing(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_role_id' => 'required|numeric',
            'category_id' => 'nullable|numeric',
            'limit' => 'nullable|numeric',
            'offset' => 'nullable|numeric',
        ]);
        if($validator->fails())
        {
            return sendValidationError($validator->errors()->toArray());
        }
        $serv_profile = new ServiceProfile;
        $serv_profile = $serv_profile->newQuery();

        $serv_profile->with('user')->whereHas('user', function ($query) use ($request) {
            $query->where('users.approval', 1);});

        if ($request->has('user_role_id')) {
            $serv_profile = $serv_profile->where('user_role_id',$request->user_role_id);
        }
        if ($request->has('category_id') && $request->category_id!='' && $request->category_id!=0) {
          $serv_profile=  $serv_profile->whereHas('categories', function ($query) use ($request) {
                $query->where('service_profile_categories.category_id', $request->category_id);});
        }
        if ($request->has('state') && $request->state!='') {
            $serv_profile = $serv_profile->where('state', $request->state);
        }

        if ($request->has('city') && $request->city!='') {
            $serv_profile = $serv_profile->where('city', $request->city);
        }
        if ($request->has('area') && $request->area!='') {
            $serv_profile = $serv_profile->where('area', $request->area);
        }
        if ($request->has('name') && $request->name!='') {
            $serv_profile = $serv_profile->whereHas('user', function ($query) use ($request) {
                $query->where('users.name', 'like', '%' . $request->name . '%');
            });
        }

        if($request->has('gender') && $request->gender != "All")
        {
            $serv_profile= $serv_profile->whereHas('user.profile',function ($query) use ($request) {
                $query->where('profiles.gender', $request->input('gender'));

            });
        }
        if($request->has('live_status'))
        {

  $serv_profile= $serv_profile->where('mark_live',$request->live_status);

            }

        if($request->has('min_fee_from') && $request->has('min_fee_to'))
        {
            $serv_profile = $serv_profile->where('min_fee','>=',$request->min_fee_from)->where('min_fee','<=',$request->min_fee_to);
        }
        if($request->has('max_fee_from') && $request->has('max_fee_to'))
        {

            $serv_profile = $serv_profile->where('max_fee','>=',$request->max_fee_from)->where('max_fee','<=',$request->max_fee_to);
        }

        if($request->has('rating'))
        {
            $serv_profile= $serv_profile->whereHas('user.profile',function ($query) use ($request) {
                $query->whereIn('profiles.rating', [$request->input('rating')]);
            });
        }

        $provider = $serv_profile->limit($request->has('limit')?$request->limit:50)->offset($request->has('offset')?$request->offset:0)->get();
        $data['limit'] = $request->has('limit')?(int) $request->limit:50;
        $data['offset'] = $request->has('offset')?(int) $request->offset:0;
        $data['providers'] = ProviderProfileResource::collection($provider);
        return sendSuccessResponse("",$data);
    }

    // V1
    public function getProviderDetail($id){
            $provider=  ServiceProfile::with(['user','categories'])->where('id',$id)->get();
            $data['provider'] = ServiceProfileResource::collection($provider);
            foreach($provider as $pr){
               $schedule=Event::where('user_id',$pr->user_id)->select('id','title','start_date','end_date','is_off')->orderby('id','desc')->get();;
                $data['schedules'] = BookingScheduleResource::collection($schedule);
            }

            return sendSuccessResponse("",$data);

    }

    public function initiateAmbulancePayment($userId,$bookingId)
    {
        if(!is_numeric($userId) && !is_numeric($bookingId))
        {
            return sendFailureMessage("Inavlid data access.");
        }

            $booking= AmbulanceBooking::where('id',$bookingId)
                ->where('patient_id',$userId)
                ->where('booking_date','>=',today())
                ->first();

            if($booking =='')
            {
                return sendFailureMessage("Data is not available.");
            }
            if($booking->is_completed)
            {
                return sendFailureMessage("This booking request is marked as completed by service provider.");
            }
            $data=array();
            $data['orderId'] = Str::random(15);
            $data['customerName']=$booking->patient->name;
            $data['customerEmail'] = $booking->patient->email;
            $data['customerPhone'] = $booking->patient->mobile;
            $data['orderAmount'] = $booking->advance_payment;
            $data['orderNote'] = "Ambulance Booking Fee";
            $data['notify'] = '/omed-api/user/ambulance-fee/notify';
            $data['return'] = '/omed-api/user/ambulance-fee/return';
            $orderDetails = payNow($data);
            $data['title'] =" Pay Now";
            $data['orderDetails'] =$orderDetails;
            $update=array();
            $update['temp_txn_number'] =$data['orderId'];
//            $update['ambulance_fee'] = $data['orderAmount'];
            if($booking->update($update))
            {
                return view('api.checkout',$data);
            }
            else{
                return sendFailureMessage("Something went wrong.");
            }

    }
    public function notifyAmbulancePaymentResponse(Request $request)
    {

    }
    public function ambulancePaymentResponse(Request $request)
    {
        $response= $request->all();
        $detail= $request->all();
        $booking= AmbulanceBooking::where('temp_txn_number',$detail['orderId'])->first();
        $update=array();
        $update['transaction_status'] =$detail['txStatus'];
        $update['txn_amount'] = $detail['orderAmount'];
        $update['txn_date'] =$detail['txTime'];
        $update['txn_response'] = $detail['txStatus'];
        $update['txn_number'] = $detail['orderId'];
        $update['is_paid'] = 1;
        $update['referenceId'] = $detail['referenceId'];
        $update['payment_mode'] = $detail['paymentMode'];

        if(validateSignature($detail))
        {
            if($detail['txStatus'] == "SUCCESS")
            {
                if(AmbulanceBooking::where('id',$booking->id)->update($update))
                {
                    ServiceProfile::where('id',$booking->service_profile_id)->update(['is_available'=>0]);
                    $update['title'] = " Payment Response";
                    $update['booking_id'] = $booking->id;
//                    $text = "Dear User, Booking successful. Now you can access the service provider contact details. Regards OMEDDr.";
                    $text = "Dear User, your ambulance booking successful. Your booked ambulance is". $booking->serviceProfile->vehicle_no.". Regards OMEDDr.";
//                    $text2 = "Dear User, You have got a confirmed ambulance booking request. Regards OMEDDr.";
                    $text2 = "Dear User, you have a confirmed ambulance booking request. Regards OMEDDr.";
                    sms_sender(trim($booking->patient->mobile), $text,$this->config['sms_pat_ambulance_book']);
                    sms_sender(trim($booking->provider->mobile), $text2,$this->config['sms_provider_ambulance_booking']);
                    if($booking->patient->device_id!='')
                    {
                        $content =array(
                            "category" => "ambulance",
                            "type" => "booking",
                            "user_role" => 0,
                            "booking_id" =>$booking->id,
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                        );
                        $notification = array(
                            "title" => "Ambulance Booking",
                            "body" => "Your ambulance booking successful. Your booked ambulance is". $booking->serviceProfile->vehicle_no,
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK"
                        );
                         AppNotificationService::sendSingleUserNotification($booking->patient->device_id,$content, $notification);
                    }
                    if($booking->provider->device_id!='')
                    {
                        $content =array(
                            "category" => "ambulance",
                            "type" => "booking",
                            "user_role" => 1,
                            "booking_id" =>$booking->id,
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                        );
                        $notification = array(
                            "title" => "Ambulance Booking",
                            "body" => "you have a confirmed ambulance booking request",
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK"
                        );
                         AppNotificationService::sendSingleUserNotification($booking->provider->device_id,$content, $notification);
                    }
                    return view('api.payment_response',$update)->with('success','Payment done successfully.');
                }
                else
                {
                    return view('api.payment_response',$update)->with('error','Unable to proceed.');

                }
            }
            else
            {
                return view('api.payment_response',$update)->with('error','Your transaction was not successful.');
            }
        }
        else{
            $update['signature_mismatch'] = true;
            return view('api.payment_response',$update)->with('error','Invalid payment signature found.');
        }
    }

    public function initiatePayment($userId,$bookingId)
    {
        if(!is_numeric($userId) && !is_numeric($bookingId))
        {
            return sendFailureMessage("Inavlid data access.");
        }
        $booking= LiveConsultation::where('id',$bookingId)
            ->where('patient_id',$userId)
            ->where('booking_date',date('Y-m-d'))
            ->first();
        if($booking=='')
        {
            return sendFailureMessage("Data is not available.");
        }
        if($booking->serviceProfile->mark_live == 0)
        {
            return sendFailureMessage("The service goes offline now.Pay latter for this request or consult with other service provider");
        }
        if($booking->request_status >= '3')
        {
            $detail['request_status'] = (int) $booking->request_status;
            $detail['con_link_access'] = (int) $booking->con_link_access;
            $detail['con_link'] =   $booking->con_link_access==1 ?url('omed-api/patient-rtc/'.$booking->patient_id.'/'.$booking->id):'';
            return sendSuccessResponse("You have already paid for this request.",$detail);

        }
        $orderAmount = $booking->serviceProfile->min_fee;
        if($booking->wallet_pay==1 && $booking->wallet_deduction < $orderAmount)
        {
            $orderAmount = $orderAmount - $booking->wallet_deduction;
        }
        $data=array();
        $data['orderId'] = Str::random(15);
        $data['customerName']=$booking->patient->name;
        $data['customerEmail'] = $booking->patient->email;
        $data['customerPhone'] = $booking->patient->mobile;
        $data['orderAmount'] = $orderAmount;
        $data['orderNote'] = "Online Consultation Fee";
        $data['notify'] = '/omed-api/user/notify';
        $data['return'] = '/omed-api/user/return';
        $orderDetails = payNow($data);
        $data['title'] =" Pay Now";
        $data['orderDetails'] =$orderDetails;
        $update=array();
        $update['temp_txn_number'] =$data['orderId'];
        $update['cons_amount'] = $data['orderAmount'];
        if($booking->update($update))
        {
            return view('api.checkout',$data);
        }
        else{
            return sendFailureMessage("Something went wrong.");
        }
    }

    public function paymentResponse(Request $request)
    {
        $response= $request->all();
        $detail= $request->all();
        $booking= LiveConsultation::where('temp_txn_number',$detail['orderId'])->first();
        if(validateSignature($detail))
        {
            if($detail['txStatus'] == "SUCCESS")
            {
//                $activateLink= enableConferenceLink($booking->con_name);
//            $embedCode = getEmbedCode($booking->con_name);

//                $activateLink=json_decode($activateLink);
                $update=array();
                $update['con_link_access']=1;
                $update['transaction_status'] =$detail['txStatus'];
                $update['txn_amount'] = $detail['orderAmount'];
                $update['txn_date'] =$detail['txTime'];
                $update['txn_response'] = $detail['txStatus'];
                $update['txn_number'] = $detail['orderId'];
                $update['request_status'] = 3;
                $update['payment_mode'] = $detail['paymentMode'];
                $update['referenceId'] = $detail['referenceId'];
//                $update['con_embed'] = $embedCode;
                $update['is_con_link_available']=1;
//                if($activateLink->status == "Added")
//                {
//                    $update['con_link'] =$activateLink->conferencelink;
                    if(LiveConsultation::where('id',$booking->id)->update($update))
                    {
                        $update['title'] = " Payment Response";
//                    $update['provider_id'] = $booking->service_profile_id;
                        $update['booking_id'] = $booking->id;
//                        $text = "Dear User, Live consultation link with a patient (".$booking->patient->name.") has been activated for the recently accepted request. Regards OMEDDr.";
                        $text = "Dear User,your video consultation link for recently accepted request has been activated. Regards OMEDDr.";
                        sms_sender(trim($booking->provider->mobile), $text,$this->config['sms_active_video_link']);

                        if($booking->provider->device_id!='')
                        {
                            $content =array(
                                "category" => "consultation",
                                "type" => "live_calling",
                                "user_role" => 1,
                                "booking_id" =>$booking->id,
                                "con_link" =>url('omed-api/provider-rtc/'.$booking->provider_id.'/'.$booking->id),
                                "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                            );
                            $notification = array(
                                "title" => "Live Consultation",
                                "body" => "Your video consultation link from the patient( ".$booking->patient_name." ) has been activated",
                                "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                            );
                             AppNotificationService::sendSingleUserNotification($booking->provider->device_id, $content,$notification);
                        }
                        if($booking->patient->device_id!='')
                        {
                            $content =array(
                                "category" => "consultation",
                                "type" => "live_calling",
                                "user_role" => 0,
                                "booking_id" =>$booking->id,
                                "con_link" =>url('omed-api/patient-rtc/'.$booking->patient_id.'/'.$booking->id),
                                "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                            );
                            $notification = array(
                                "title" => "Live Consultation",
                                "body" => "You can now consult with the provider (".$booking->provider->name.")",
                                "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                            );
                             AppNotificationService::sendSingleUserNotification($booking->patient->device_id,$content, $notification);
                        }
                        return view('api.payment_response',$update)->with('success','Payment done successfully.You can join the consultation.');
                    }
            }
            else
            {
                $update['transaction_status'] =$detail['txStatus'];
                return view('api.payment_response',$update)->with('error','Something went wrong.');
            }

        }
        else{
            $update['signature_mismatch'] = true;
            return view('api.payment_response',$update)->with('error','Something went wrong.');
        }
    }
    public function notifyPaymentResponse(Request $request)
    {

    }

    /*
     * Appointment Payment
     */
    public function initiateAppointmentPayment($userId,$bookingId)
    {
        if(!is_numeric($userId) && !is_numeric($bookingId))
        {
            return sendFailureMessage("Invalid User Access!!!");
        }

            $booking= AppointmentBooking::where('id',$bookingId)
                ->where('patient_id',$userId)
                ->where('booking_date','>=',today())
                ->first();

            if($booking =='')
            {
                return sendFailureMessage("Data is not available.");
            }

            $data=array();
            $data['orderId'] = Str::random(15);
            $data['customerName']=$booking->patient->name;
            $data['customerEmail'] = $booking->patient->email;
            $data['customerPhone'] = $booking->patient->mobile;
            $data['orderAmount'] = $this->config['appointment_fee'];
            $data['orderNote'] = "Appointment Booking Fee";
            $data['notify'] = '/omed-api/user/appointment-fee/notify';
            $data['return'] = '/omed-api/user/appointment-fee/return';
            $orderDetails = payNow($data);
            $data['title'] =" Pay Now";
            $data['orderDetails'] =$orderDetails;
            $update=array();
            $update['temp_txn_number'] =$data['orderId'];
//            $update['appointment_fee'] = $data['orderAmount'];
            if($booking->update($update))
            {
                return view('api.checkout',$data);
            }
            else{
                return sendFailureMessage("Something went wrong.");
            }

    }
    public function appointmentPaymentResponse(Request $request)
    {
        $response= $request->all();
        $detail= $request->all();
        $booking= AppointmentBooking::where('temp_txn_number',$detail['orderId'])->first();
        $update=array();
        $update['transaction_status'] =$detail['txStatus'];
        $update['txn_amount'] = $detail['orderAmount'];
        $update['txn_date'] =$detail['txTime'];
        $update['txn_response'] = $detail['txStatus'];
        $update['txn_number'] = $detail['orderId'];
        $update['is_paid'] = 1;
        $update['referenceId'] = $detail['referenceId'];
        $update['payment_mode'] = $detail['paymentMode'];
        $update['booking_number'] =  rand();
        if(validateSignature($detail))
        {
            if($detail['txStatus'] == "SUCCESS")
            {
                if($booking->update($update))
                {
                    $update['title'] = " Payment Response";
                    $update['booking_id'] = $booking->id;
//                    $text = "Dear User, Booking Successful. Your Booking No. is ".$update['booking_number'].". Regards OMEDDr.";
                    $text = "Dear User, appointment has been booked. Your booking number is ".$update['booking_number'].". Regards OMEDDr.";
                    $text2 = "Dear User, a patient has booked an appointment with booking no ".$update['booking_number'].". Regards OMEDDr.";
                    sms_sender(trim($booking->patient->mobile), $text,$this->config['sms_pat_appointment_book']);
                    sms_sender(trim($booking->provider->mobile), $text2,$this->config['sms_provider_appointment']);
                    if($booking->patient->device_id!='')
                    {
                        $content =array(
                            "category" => "appointment",
                            "type" => "booking",
                            "user_role" => 0,
                            "booking_id" =>$booking->id,
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                        );
                        $notification = array(
                            "title" => "Appointment Booking",
                            "body" => "Your appointment has been booked. Your booking number is ".$update['booking_number'],
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                            );
                         AppNotificationService::sendSingleUserNotification($booking->patient->device_id,$content, $notification);
                    }
                    if($booking->provider->device_id!='')
                    {
                        $content =array(
                            "category" => "appointment",
                            "type" => "booking",
                            "user_role" => 1,
                            "booking_id" =>$booking->id,
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                        );
                        $notification = array(
                            "title" => "Appointment Booking",
                            "body" => "A patient has booked an appointment with booking no.".$update['booking_number'],
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                            );
                         AppNotificationService::sendSingleUserNotification($booking->provider->device_id,$content, $notification);
                    }
                    return view('api.payment_response',$update)->with('success','Payment done successfully.');
                }
                else
                {
                    return view('api.payment_response',$update)->with('error','Unable to proceed.');

                }
            }
            else
            {
                return view('api.payment_response',$update)->with('error','Your transaction was not successful.');
            }
        }
        else{
            $update['signature_mismatch'] = true;
            return view('api.payment_response',$update)->with('error','Invalid payment signature found.');
        }
    }
    public function notifyAppointmentPaymentResponse(Request $request)
    {

    }

    //function to get all states
    public function getStates(Request $request)
    {
        $record=State::select('name')->get();
        if($record->isNotEmpty())
        {
            return sendSuccessResponse("",$record->toArray());
        }
        else{
            return sendFailureMessage("No data were found.");
        }

    }

    // Function to get cities
    public function getCity(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'state' => 'required|string',

        ]);
        if ($validator->fails()) {
           return sendValidationError($validator->errors()->toArray());
        }

        $record=District::where('state',$request->state)->select('name')->get();
        if($record->isNotEmpty())
        {
           return sendSuccessResponse("",$record->toArray());
        }
        else{
            return sendFailureMessage("No data were found.");
        }

    }

    // Function to get area
    public function getArea(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'state' => 'required|string',
            'city' => 'required|string',

        ]);
        if ($validator->fails()) {
           return sendValidationError($validator->errors()->toArray());
        }

        $record=Location::where('city',$request->city)->where('state',$request->state)->select('area')->orderby('area','asc')->get();
        if($record->isNotEmpty())
        {
            return sendSuccessResponse("",$record->toArray());
        }
        else{
            return sendFailureMessage("No data were found.");
        }
    }

    public function storeReviewRating(ReviewRatingRequest $request){

        if($request->type=="consultation"){
            $con = LiveConsultation::where('id',$request->id)->where('patient_id',auth()->user()->id)->first();
            if($con!='')
            {
                $con->review=$request->review;
                $con->rating = $request->rating;
                if($con->save())
                {
                    return sendSuccessResponse("Your review and ratings are submitted. Thanks for review.");
                }
                else{
                    return sendFailureMessage("Unable to submit your review.");
                }
            }
            return sendFailureMessage("Consultation data not found.");
        }
        if($request->type=="appointment"){
            $apnt = AppointmentBooking::where('id',$request->id)->where('patient_id',auth()->user()->id)->first();
            if($apnt!='')
            {
                $apnt->review=$request->review;
                $apnt->rating = $request->rating;
                if($apnt->save())
                {
                    return sendSuccessResponse("Your review and ratings are submitted. Thanks for review.");
                }
                else{
                    return sendFailureMessage("Unable to submit your review.");
                }
            }
            return sendFailureMessage("Consultation data not found.");
        }
        if($request->type=="ambulance"){
            $amb = AmbulanceBooking::where('id',$request->id)->where('patient_id',auth()->user()->id)->first();
            if($amb!='')
            {
                $amb->review=$request->review;
                $amb->rating = $request->rating;
                if($amb->save())
                {
                    return sendSuccessResponse("Your review and ratings are submitted. Thanks for review.");
                }
                else{
                    return sendFailureMessage("Unable to submit your review.");
                }
            }
            return sendFailureMessage("Consultation data not found.");
        }
        return sendFailureMessage("Invalid request.");
    }
}
