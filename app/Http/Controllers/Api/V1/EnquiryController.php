<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\EnquiryChatResource;
use App\Service\ImageUpload;
use Illuminate\Http\Request;
use App\Models\Enquiry;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class EnquiryController extends Controller
{

    public function uniqueUsers(){
    if (auth()->user()->user_role_id==3)
    {
        $users = Enquiry::where('receiver','!=',auth()->user()->id)
                            ->where('sender',auth()->user()->id)
                            ->distinct('receiver')
                            ->orderby('created_at','desc')
                            ->get();
        $detail=array();
        if($users->isNotEmpty())
        {
            $n=0;
            $users=$users->unique('receiver');
            foreach($users as $user)
            {
                $detail[$n]['id'] = $user->receiver;
                $detail[$n]['name'] = $user->user_receiver->name;
                $detail[$n]['photo'] = !empty($user->user_receiver->profile->photo)?asset('uploads/profile/'.$user->user_receiver->profile->photo):'';
                $last_chat = Enquiry::where('receiver',$user->receiver)
                    ->where('sender',auth()->user()->id)
                    ->orWhere('receiver',auth()->user()->id)
                    ->where('sender',$user->receiver)
                    ->select('created_at')
                    ->limit(1)
                    ->orderby('id','desc')
                    ->first();
                $detail[$n]['last_chat'] = date('d-m-Y',strtotime($last_chat->created_at));
            $n++;
            }
        }
        return sendSuccessResponse("",$detail);
        }

        if (auth()->user()->user_role_id==11)
    {
        $users = Enquiry::where('receiver',auth()->user()->id)
                            ->where('sender','!=',auth()->user()->id)
                            ->distinct('sender')
                            ->orderby('created_at','desc')
                            ->get();
        $detail=array();
        if($users->isNotEmpty())
        {
            $n=0;
            $users=$users->unique('sender');
            foreach($users as $user)
            {
                $detail[$n]['id'] = $user->sender;
                $detail[$n]['name'] = $user->user_sender->name;
                $detail[$n]['photo'] = !empty($user->user_sender->profile->photo)?asset('uploads/profile/'.$user->user_sender->profile->photo):'';
                $last_chat = Enquiry::where('receiver',$user->sender)
                    ->where('sender',auth()->user()->id)
                    ->orWhere('receiver',auth()->user()->id)
                    ->where('sender',$user->sender)
                    ->select('created_at')
                    ->limit(1)
                    ->orderby('id','desc')
                    ->first();
                $detail[$n]['last_chat'] = date('d-m-Y',strtotime($last_chat->created_at));
            $n++;
            }
        }
        return sendSuccessResponse ("", $detail);
        }

    }

    public function getChats(Request $request){
       $validator =Validator::make($request->all(),[
           'receiver' => 'required|numeric',
           'offset' => 'nullable|numeric',
           'limit' => 'nullable|numeric',
       ]);
       if($validator->failed())
       {
           return sendValidationError($validator->errors()->toArray());
       }

            $chats = Enquiry::where('receiver',$request->receiver)
                                ->where('sender',auth()->user()->id)
                                ->orWhere('receiver',auth()->user()->id)
                                ->where('sender',$request->receiver)
//                                ->limit($request->has('limit')?$request->limit:20)
//                                ->offset($request->has('offset')?$request->offset:0)
                                ->orderby('id','asc')
                                ->get();
            if($chats->isNotEmpty())
            {
                $data['limit'] = $request->has('limit')?$request->limit:20;
                $data['offset'] = $request->has('offset')?$request->offset:0;
                $data['chats'] = EnquiryChatResource::collection($chats);
                return sendSuccessResponse("",$data);
            }
            else
            {
                return sendFailureMessage("No data available.");
            }


    }

    public function sendMessage(Request $request)
    {
        $validator =Validator::make($request->all(),[
            'receiver' => 'required|numeric',
            'message' => 'nullable|string',
            'attachment' => 'nullable|image|mimes:jpg,jpeg,png',
        ]);
        if($validator->failed())
        {
            return sendValidationError($validator->errors()->toArray());
        }

        $data=$request->only('receiver','message');
        $data['sender'] = auth()->user()->id;
        if($request->hasFile('attachment'))
        {
            $mno = auth()->user()->mobile;
            $directory = $mno . '_' . $request->receiver;
                $file = $request->file('attachment');
                $path = public_path('uploads/enquiry/');
                $upload = ImageUpload::uploadImage($file, $path, $directory);
                if ($upload['status']) {
                    $data['attachment'] = 'uploads/enquiry/'.$upload['folder_name'].'/'.$upload['file'];

                } else {
                    return sendFailureMessage("Unable to send attachment.");
                }
            }
        if(Enquiry::create($data))
        {
            return sendSuccessResponse("Message sent.");
        }
        else{
            return sendFailureMessage("Unable to send message.");
        }

    }
}
