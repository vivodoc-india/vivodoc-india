<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotificationController extends Controller
{

    public static function sendSingleUserNotification(Request $request)
    {
        $SERVER_API_KEY = 'AAAAIHrymbw:APA91bEuH71RsG5rOUAYE6BslzpFgJsMXwgfPBXjtt4YWU7EUfHt1-t_OLHUyQARWJ4e8e1UIidfm9I238qX3_QCbJ-vMyzszrKz_X1qwNy4Ep9X4VzuIS-HCVffoYgFqoHUVHiQi-cZ';

        // payload data, it will vary according to requirement
        $data_send = [
            "to" => $request->token, // for single device id
            "data" => json_decode($request->data),
            "notification" => json_decode($request->notification)
        ];
        $dataString = json_encode($data_send);

        $headers = [
            'Authorization:key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }
}
