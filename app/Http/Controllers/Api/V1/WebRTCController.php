<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\LiveConsultation;
use App\Service\AppNotificationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WebRTCController extends Controller
{
    public function patientWebrtc($patient,$booking){
        $booking_detail= LiveConsultation::where('id',$booking)
            ->where('patient_id',$patient)
            ->with(['patient','provider'])
            ->first();
        $data['user'] = $patient;
        $content =array(
            "category" => "consultation",
            "type" => "live_calling",
            "user_role" => 1,
            "booking_id" =>$booking_detail->id,
            "con_link" =>url('omed-api/provider-rtc/'.$booking_detail->provider_id.'/'.$booking_detail->id),
            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
        );
        $notification = array(
            "title" => "Live Consultation",
            "body" => "A patient( ".$booking_detail->patient_name." ) want to connect with you through live video consultation.",
            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
        );
        AppNotificationService::sendSingleUserNotification($booking_detail->provider->device_id, $content,$notification);
        $data['booking'] = $booking_detail;
        return view('api.api-webrtc',$data);
    }

    public function providerWebrtc($provider,$booking){
        $booking_detail= LiveConsultation::where('id',$booking)
            ->where('provider_id',$provider)
            ->with(['patient','provider'])
            ->first();
        $data['user'] = $provider;
        $content =array(
            "category" => "consultation",
            "type" => "live_calling",
            "user_role" => 0,
            "booking_id" =>$booking_detail->id,
            "con_link" =>url('omed-api/patient-rtc/'.$booking_detail->patient_id.'/'.$booking_detail->id),
            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
        );
        $notification = array(
            "title" => "Live Consultation",
            "body" => "A provider( ".$booking_detail->provider->name." ) want to connect with you through live video consultation.",
            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
        );
        AppNotificationService::sendSingleUserNotification($booking_detail->patient->device_id, $content,$notification);
        $data['booking']= $booking_detail;
        return view('api.api-webrtc',$data);
    }

    public function createRoom(Request $request){
        if($request->ajax() && $request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'res_code' =>'required|numeric',
                'ident' => 'required|numeric',
                'caller' => 'required|numeric',
                'id' => 'required|string',
            ]);

            if ($validator->fails()) {
                return response()->json(['success'=>'false','message'=>'Something went wrong.']);
            }
            $stat= LiveConsultation::where('id',$request->res_code)->first();
            if($stat->request_status >= 3)
            {
                if($stat->caller!='' && $stat->caller!=$request->ident)
                {
                    return response()->json(['success'=>'true','roomExists'=>'true','id'=>$stat->con_link]);
                }
                else{
                    $stat->con_link = $request->id;
                    $stat->caller = $request->caller;
                    if($stat->save())
                        return response()->json(['success'=>'true','roomExists'=>'false','message'=>'Call initiated.']);
                }

            }

            else{
                return response()->json(['success'=>'false','res_code'=>$request->res_code]);
            }
        }
    }

    public function removeRoom(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'res_code' =>'required|numeric',
            ]);

            if ($validator->fails()) {
                return response()->json(['success'=>'false','message'=>'Something went wrong.']);
            }
            $stat= LiveConsultation::where('id',$request->res_code)->first();
            if($stat->request_status >= 3)
            {
                $stat->con_link = '';
                $stat->caller = null;
                if($stat->save())
                    return response()->json(['success'=>'true','message'=>'Call Closed.']);
            }

            else{
                return response()->json(['success'=>'false','res_code'=>$request->res_code]);
            }
        }
    }


}
