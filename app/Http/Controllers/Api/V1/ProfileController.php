<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfilePhotoRequest;
use App\Models\Profile;
use App\Models\ServiceProfile;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{

    public function __construct()
    {

    }

    public function getProfile()
    {

       $data= User::with('profile')->where('id',auth()->user()->id)->get();
        if(auth()->user()->user_role_id==11)
        {
            $profile = $this->getProviderProfile($data);
        }
        if(auth()->user()->user_role_id==3)
        {
            $profile = $this->getPatientProfile($data);
        }
       if(!empty($profile))
       {
           return sendSuccessResponse("",$profile);
       }
       return sendFailureMessage("Profile not available");
    }

    public function updateProfile(ProfileUpdateRequest $request)
    {
        $data = $request->validated();
        if(!$request->has('mobile') && !$request->has('email'))
        {
            return sendFailureMessage("Please enter either mobile no or email id");
        }
        $data['user_id'] = auth()->user()->id;
        $user['name'] = $data['name'];
        $user['mobile'] = $data['mobile'];
        $user['email'] = $data['email'];
        unset($data['name'],$data['mobile'],$data['email']);
        $data['blood_group'] = $data['bloodgroup'];
        unset($data['bloodgroup']);
        User::updateUser($user);
       if(Profile::where('user_id',auth()->user()->id)->count())
       {
       if(Profile::where('user_id',auth()->user()->id)->update($data))
       {
           return sendSuccessResponse("Profile updated.");
       }
       else{
           return sendFailureMessage("Unable to update your profile");
       }
       }
       else{
           if(Profile::create($data))
           {
               return sendSuccessResponse("Profile created.");
           }
           else{
               return sendFailureMessage("Unable to create your profile");
           }
       }
    }

    public function updateProfilePhoto(ProfilePhotoRequest $request)
    {
        $prev_image = Profile::where('user_id',auth()->user()->id)->first();
        $path = public_path('uploads/profile/');
        $frd ='';
        $photo=$request->file('image');
        $imagename=rand(). '.' . $photo->getClientOriginalExtension();
        $thumb_img = Image::make($photo->getRealPath())->save($path . '/' . $imagename, 80);
        if(File::exists($path.$prev_image->photo)) {
            File::delete($path.$prev_image->photo);
        }
$prev_image->photo = $imagename;
        if($prev_image->save())
        {
            $detail['photo'] = asset('uploads/profile/'.$prev_image->photo);
            return sendSuccessResponse("Photo uploaded.",$detail);
        }
        else{
            return sendFailureMessage("Unable to update profile photo.");
        }

    }
    private function getProviderProfile($data){
        $profile =array();
        foreach($data as $prof){
            $profile['id']=$prof->id;
            $profile['name']=$prof->name;
            $profile['mobile']=$prof->mobile;
            $profile['email']=$prof->email;
            $profile['user_rol_id']=$prof->user_role_id;
            $profile['approval'] = $prof->approval;
            $profile['city'] = $prof->profile->city;
            $profile['landmark'] = $prof->profile->landmark;
            $profile['area'] = $prof->profile->area;
            $profile['address'] = $prof->profile->address;
            $profile['gender'] =$prof->profile->gender;
            $profile['photo'] = $prof->profile->photo!=''?asset('uploads/profile/'.$prof->profile->photo):'';
            $profile['pincode'] = $prof->profile->pincode;
            $profile['blood_group'] = $prof->profile->blood_group;
            $profile['speciality'] = $prof->profile->speciality;
            $profile['other_speciality'] = $prof->profile->other_speciality;
            $profile['experiance'] = $prof->profile->experiance;
            $profile['qualification'] = $prof->profile->qualification;
            $serviceDoc = ServiceProfile::where('user_id',$prof->id)->whereIn('user_role_id',[1,2,9])->first();
            $serviceAmb = ServiceProfile::where('user_id',$prof->id)->where('user_role_id',6)->first();
            if($serviceDoc!='')
            {
                $profile['live_status'] = $serviceDoc->mark_live;
            }
            if($serviceAmb!=''){
                $profile['live_status'] = $serviceAmb->mark_live;
            }

        }
        return $profile;
    }

    private function getPatientProfile($data)
    {
        $profile =array();
        foreach($data as $prof){
            $profile['id']=$prof->id;
            $profile['name']=$prof->name;
            $profile['mobile']=$prof->mobile;
            $profile['email']=$prof->email;
            $profile['user_rol_id']=$prof->user_role_id;
            $profile['puid'] = $prof->puid;
            $profile['city'] = $prof->profile->city;
            $profile['landmark'] = $prof->profile->landmark;
            $profile['area'] = $prof->profile->area;
            $profile['address'] = $prof->profile->address;
            $profile['gender'] =$prof->profile->gender;
            $profile['photo'] = $prof->profile->photo!=''?asset('uploads/profile/'.$prof->profile->photo):'';
            $profile['pincode'] = $prof->profile->pincode;
            $profile['blood_group'] = $prof->profile->blood_group;
        }
        return $profile;
    }
}
