<?php

namespace App\Http\Controllers\Api\V1\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImagesRequest;
use App\Http\Requests\LiveStatusUpdateRequest;
use App\Http\Requests\ServiceProfileRequest;
use App\Http\Requests\StoreSubcategoryRequest;
use App\Http\Resources\ServiceProfileResource;
use App\Models\Category;
use App\Models\Profile;
use App\Models\ServiceProfile;
use App\Models\ServiceProfileCategory;
use App\Models\ServiceProfileDocument;
use App\Models\User;
use App\Models\UserRole;
use App\Service\ImageUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class ServiceProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('provider');
    }

    public function index()
    {
            $serviceProfile= ServiceProfile::where('user_id',auth()->user()->id)->orderBy('user_role_id','asc')->get();
            $data['remaining'] = 6 - count($serviceProfile);
            $data['service_profiles']= ServiceProfileResource::collection($serviceProfile);
            return sendSuccessResponse("",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cat_except=[3,7,8];
        if(auth()->user()->initial_update)
        {
            $live_cat = ServiceProfile::where('user_id',auth()->user()->id)->whereIn('user_role_id',[1,2,9])->get();
            if($live_cat->isNotEmpty() && count($live_cat) > 0)
            {
                array_push($cat_except,6);
            }
            $live_amb = ServiceProfile::where('user_id',auth()->user()->id)->where('user_role_id',6)->first();
            if($live_amb!='')
            {
                array_push($cat_except,1);
                array_push($cat_except,2);
                array_push($cat_except,9);
            }
            $user_role_sel=ServiceProfile::where('user_id',auth()->user()->id)->select('user_role_id')->get();
            if(count($user_role_sel) > 0)
            {
                foreach($user_role_sel as $role)
                {
                    if(!in_array($role->user_role_id,$cat_except))
                    {
                        array_push($cat_except,$role->user_role_id);
                    }
                }

            }
        }
        $categories=UserRole::whereNotIn('id',$cat_except)->where('status','1')->select('id','name')->get()->toArray();
        if(count($categories)==0)
        {
            return sendFailureMessage("You have already selected all categories.");
        }
        return sendSuccessResponse("",$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceProfileRequest $request)
    {
        $sp['user_role_id'] = $request->user_role_id;
        $sp['user_id'] = auth()->user()->id;
        $find= ServiceProfile::where('user_id',auth()->user()->id)->where('user_role_id',$sp['user_role_id'])->first();
        if($find!='' && $find->trashed())
        {
            $find->restore();
            return sendSuccessResponse("Service profile restored.");
        }
        else {
            if (ServiceProfile::create($sp)) {
                if (auth()->user()->initial_update == 0) {
                    User::where('id', auth()->user()->id)->update(['initial_update' => 1]);
                }
                return sendSuccessResponse("Service Profile added.");
            } else {
                return sendFailureMessage("Unable to create service profile.");
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $service = ServiceProfile::with(['categories','documents'])->where('id',$id)->get();
    $data['service_profile']=ServiceProfileResource::collection($service);

        return sendSuccessResponse("",$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = ServiceProfile::where('id',$id)
                    ->select('id','reg_no','description',
                    'landmark','area','city','state','address','min_fee','max_fee','vehicle_no','vehicle_model')
                    ->first();
        if($service!='')
        {
            return sendSuccessResponse("",$service->toArray());
        }
        return sendFailureMessage("No data available.");

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceProfileRequest $request, $id)
    {

            $data = $request->validated();
            if(ServiceProfile::where('id',$id)->update($data))
            {
                return sendSuccessResponse("Service profile updated.");
            }
            else
                {
                    return sendFailureMessage("Unable to update service profile");
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sp=ServiceProfile::findOrFail($id);
        if($sp->delete())
        {
            return sendSuccessResponse("Service profile removed.");
        }
        else{
            return sendFailureMessage("Unable to remove service profile.");
        }

    }

    /*
     * Add Service Profile Categories
     * along with related certificates
     * and documents
     */
    public function getCategory($serviceid,$role){
    $selected = ServiceProfileCategory::where('service_profile_id',$serviceid)
                                        ->where('user_id',auth()->user()->id)
                                        ->where('user_role_id',$role)
                                        ->select('category_id')
                                        ->get();
    $categories = Category::whereNotIn('id',$selected)->where('user_role_id',$role)
                                                    ->where('status',1)
                                                    ->where('is_private',0)
                                                    ->select('id','title')
                                                    ->orderby('title','asc')
                                                    ->get()
                                                    ->toArray();
    return sendSuccessResponse("",$categories);
    }

    /*
     * Store Subcategories for
     * particular Service Profile
     */

    public function storeSubCategory(StoreSubcategoryRequest $request)
    {
        $category= $request->validated();
        $category['user_id']=auth()->user()->id;
        $isExists = ServiceProfileCategory::where('service_profile_id',$request->service_profile_id)
                                            ->where('user_id',auth()->user()->id)
                                            ->where('user_role_id',$request->user_role_id)
                                            ->where('category_id',$request->category_id)
                                            ->exists();
        if(!$isExists)
        {
            if(ServiceProfileCategory::create($category))
            {
                return sendSuccessResponse("Category added.");
            }

            return sendFailureMessage("Unable to add category.");

        }
        else{
        return sendFailureMessage("Category already added to your profile.");
        }

        }

        /*
         * Upload and Update service profile image
         */
    public function storeServiceProfileDocuments(ImagesRequest $request)
    {
        $file = $request->file('image');
        $path= public_path('uploads/service/documents');
        $directory = auth()->user()->mobile.'_'.$request->user_role_id.$request->service_profile_id;
        $upload = ImageUpload::uploadImage($file,$path,$directory);
        if($upload['status'])
        {
        $doc['user_id'] = auth()->user()->id;
        $doc['user_role_id'] = $request->user_role_id;
        $doc['service_profile_id'] = $request->service_profile_id;
        $doc['folder_name'] = $upload['folder_name'];
        $doc['file'] = $upload['file'];
        if(ServiceProfileDocument::create($doc))
        {
            $detail['upload_image'] = asset('uploads/service/documents/'.$upload['folder_name'].'/'.$upload['file']);
            return sendSuccessResponse("Document uploaded.",$detail);
        }
        else{
            return sendFailureMessage("Unable to save document.");
        }
        }
        else{
            return sendFailureMessage("Unable to upload document.");
        }
    }

    /*
     * Save 5 banner images related
     * to each service profile category
     */

    public function saveBannerImages(ImagesRequest $request)
    {
        if(!$request->has('image_name'))
        {
            return sendFailureMessage("Please provider the image name.");
        }
        $service = ServiceProfile::where('id',$request->service_profile_id)->pluck($request->image_name);
        $file = $request->file('image');
        $path= public_path('uploads/service/banner_images');
        $directory = auth()->user()->mobile.'_'.$request->user_role_id.$request->service_profile_id;
        $upload = ImageUpload::uploadImage($file,$path,$directory);
        if($upload['status'])
        {
            if($service!='' && File::exists(public_path($service)))
            {
                File::delete(public_path($service));
            }
          $image = 'uploads/service/banner_images/'.$upload['folder_name'].'/'.$upload['file'];

            if(ServiceProfile::where('id',$request->service_profile_id)->update([$request->image_name=>$image]))
            {
                $detail['upload_image'] = asset($image);
                return sendSuccessResponse("Image uploaded.",$detail);
            }
            else{
                return sendFailureMessage("Unable to save image.");
            }
        }
        else{
            return sendFailureMessage("Unable to upload image.");
        }
    }

 /*
  * Update live status of different user roles
  */
    public function updateLiveStatus(LiveStatusUpdateRequest $request)
    {
            if($request->request_for=="dr") {
                $role = 1;
                $is_exists = ServiceProfile::where('user_id',auth()->user()->id)->whereIn('user_role_id',[1,2,9])->first();
            }
        if($request->request_for=="ambulance") {
            $role = 6;
            $is_exists = ServiceProfile::where('user_id',auth()->user()->id)->where('user_role_id',$role)->first();
        }

                if($is_exists!='')
                {
                    if($is_exists->mark_live == 1)
                    {
                        if($role==1)
                        {
                            ServiceProfile::where('user_id',auth()->user()->id)->whereIn('user_role_id',[1,2,9])->update(['mark_live'=>0]);
                        }

                        $is_exists->mark_live=0;
                        if($is_exists->save())
                        {
                            $response['live_status'] = $is_exists->mark_live;
                            return sendSuccessResponse("You are now offline.",$response);
                        }
                        else
                        {
                            return sendFailureMessage("Unable to change your live status.");
                        }
                    }
                    elseif($is_exists->mark_live == 0){
                        if($request->has('longitude') && $request->has('latitude'))
                        {
                            $is_exists->mark_live=1;
                            $is_exists->longitude = $request->longitude;
                            $is_exists->latitude = $request->latitude;
                            if($is_exists->save())
                            {
                                $response['live_status'] = $is_exists->mark_live;
                                return sendSuccessResponse("You are now online.",$response);
                            }
                            else
                            {
                                return sendFailureMessage("Unable to change your live status.");
                            }
                        }
                        else{
                            $is_exists->mark_live=1;
                            if($role==1){
                                ServiceProfile::where('user_id',auth()->user()->id)->whereIn('user_role_id',[1,2,9])->update(['mark_live'=>1]);
                            }

                            if($is_exists->save())
                            {
                                $response['live_status'] = $is_exists->mark_live;
                                return sendSuccessResponse("You are now online.",$response);
                            }
                            else
                            {
                                return sendFailureMessage("Unable to change your live status.");
                            }
                        }

                    }
                    else{
                        return sendFailureMessage("Unable to change your live status.");
                    }
                }
                else{
                    return sendFailureMessage("You don't have access to this profile.");
                }
            }


}
