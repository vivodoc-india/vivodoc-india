<?php

namespace App\Http\Controllers\Api\V1\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\LiveConsultationRequest;
use App\Http\Requests\ProviderConsultationUpdateRequest;
use App\Http\Resources\AppointmentBookingResource;
use App\Http\Resources\AppointmentDetailResource;
use App\Http\Resources\LiveConsultationDetailResource;
use App\Http\Resources\LiveConsultationResource;
use App\Http\Resources\MedicalDataResource;
use App\Models\AppointmentBooking;
use App\Models\LiveConsultation;
use App\Models\MedicalData;
use App\Models\Setting;
use App\Service\ImageUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class ConsultationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $config;
    public function __construct()
    {
        $this->middleware('provider');
        $setting = Setting::all();
        foreach ($setting as $data)
            $this->config[$data->param] = $data->data;
    }
    public function index(Request $request)
    {
        if($request->has('status'))
        {
            $bookings = LiveConsultation::where('provider_id',auth()->user()->id)
                ->where('request_status','>=',(string)$request->status)
                ->orderby('id','desc')->get();
        }
        else{
             $bookings = LiveConsultation::where('provider_id',auth()->user()->id)->orderby('id','desc')->get();
        }
        if($bookings->isNotEmpty())
        {
            $data['bookings'] = LiveConsultationResource::collection($bookings);
            return sendSuccessResponse("",$data);
        }
        else{
            return sendFailureMessage("No data available.");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = LiveConsultation::where('id',$id)->where('provider_id',auth()->user()->id)->get();
        if($booking->isEmpty())
        {
            return sendFailureMessage("No data available.");
        }
        else{
        $data['consultation_detail'] = LiveConsultationDetailResource::collection($booking);
        return sendSuccessResponse("",$data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProviderConsultationUpdateRequest $request, $id)
    {
        $data = $request->validated();
        if(!$request->has('prescription') && !$request->has('prescription_image'))
        {
            return sendFailureMessage("Please upload either prescription image or write prescription.");
        }

        $booking=  LiveConsultation::findOrFail($id);
            $mno = $booking->patient->mobile;
            $directory = $mno . '_' . rand();
            if($request->prescription == " ")
            {
                $data['prescription'] = $booking->prescription;
            }
            if($request->hasFile('prescription_image')) {
                $file = $request->file('prescription_image');
                $path = public_path('uploads/patientprofile/prescription/');
                $upload = ImageUpload::uploadImage($file, $path, $directory);
                if ($upload['status']) {
                    if (!empty($booking->prescription_folder)) {
                        $path = public_path('uploads/patientprofile/prescription/' . $booking->prescription_folder.'/'.$booking->prescription_image);
                        if (File::exists($path)) {
                            File::deleteDirectory($path);
                        }
                    }
                    $data['prescription_folder'] = $upload['folder_name'];
                    $data['prescription_image'] = $upload['file'];
//                    $data['is_con_link_expired'] = 1;

                }
                else{
                    return sendFailureMessage("Unable to upload prescription.");
                }
            }
            else{
                $data['prescription_folder'] = $booking->prescription_folder;
                $data['prescription_image'] = $booking->prescription_image;
            }
            $data['is_con_link_available'] = '0';
            $data['con_link_access'] ='0';
            $data['request_status'] = (string) 4;
            if(LiveConsultation::where('id',$id)->update($data)){
              return sendSuccessResponse("Consultation data updated.");
            }
            else{
                return sendFailureMessage("Unable to update consultation data.");
            }

        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*
     * Patient Medical Records
     */

    public function getMedicalData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'patient_id' => 'required|numeric',

        ]);
        if ($validator->fails()) {
           return sendValidationError($validator->errors()->toArray());
        }

            $medical = MedicalData::where('user_id',$request->patient_id)
                                    ->where('parent',0)
                                     ->select('id','title')
                                    ->orderby('updated_at','desc')
                                    ->get();

            if($medical->isEmpty())
            {
               return sendFailureMessage("This patient has not uploaded any medical data yet.");
            }
            else{
               $detail['medical_records']=$medical->toArray();
            return sendSuccessResponse("",$detail);
            }

    }

    public function getMedicalDataDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'patient_id' => 'required|numeric',
            'mid' => 'required|numeric',

        ]);
        if ($validator->fails()) {
           return sendValidationError($validator->errors()->toArray());
        }

            $medical = MedicalData::where('parent',$request->mid)
                ->where('user_id',$request->patient_id)
                ->select('id','file','updated_at')
                ->orderby('updated_at','desc')
                ->get();
            if($medical->isEmpty())
            {
                return sendFailureMessage("No data available.");
            }
            else{
                $data['records']=MedicalDataResource::collection($medical);
                return sendSuccessResponse("",$data);
            }

    }

    public function getAppointmentData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'patient_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return sendValidationError($validator->errors()->toArray());
        }

            $appointments = AppointmentBooking::where('patient_id',$request->patient_id)
                ->where('is_completed',1)
                ->orderby('id','desc')
                ->get();
            if($appointments->isEmpty())
            {
                return sendFailureMessage("No data found.");
            }
            else{
                $data['details'] = AppointmentBookingResource::collection($appointments);
                return sendSuccessResponse("",$data);
            }
    }
//
    public function getAppointmentDataDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'patient_id' => 'required|numeric',
            'appointment_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return sendValidationError($validator->errors()->toArray());
        }
            $booking = AppointmentBooking::where('id',$request->appointment_id)
                                        ->where('patient_id',$request->patient_id)
                                        ->where('is_completed',1)
                                        ->where('prescription','!=','')
                                        ->orWhere('prescription_image','!=','')
                                        ->get();
        if($booking->isNotEmpty())
        {
            $data['detail'] = AppointmentDetailResource::collection($booking);
            return sendSuccessResponse("",$data);
        }
        return sendFailureMessage("No data were found.");
    }

    public function getConsultationData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'patient_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
           return sendValidationError($validator->errors()->toArray());
        }

        $bookings = LiveConsultation::where('patient_id',$request->patient_id)
            ->where('request_status','=','4')
            ->orderby('id','desc')->get();
        if($bookings->isNotEmpty())
        {
            $data['bookings'] = LiveConsultationResource::collection($bookings);
            return sendSuccessResponse("",$data);
        }
        else{
            return sendFailureMessage("No data available.");
        }

    }

    public function getConsultationDataDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'patient_id' => 'required|numeric',
            'consultation_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
           return sendValidationError($validator->errors()->toArray());
        }
        $booking = LiveConsultation::where('id',$request->consultation_id)
                                        ->where('patient_id',$request->patient_id)
                                        ->get();
        if($booking->isEmpty())
        {
            return sendFailureMessage("No data available.");
        }
        else{
            $data['consultation_detail'] = LiveConsultationDetailResource::collection($booking);
            return sendSuccessResponse("",$data);
        }
    }
    /*
     * END Of Patient Medical Records
     */
    // initiate Call By Provider
    public function initiateCall(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' =>'required|numeric',
            'room' => 'required|string',
        ]);

        if ($validator->fails()) {
            return sendValidationError($validator->errors()->toArray());
        }
        $stat= LiveConsultation::where('id',$request->id)->where('provider_id',auth()->user()->id)->first();
        if($stat->request_status >= 3)
        {
            if($stat->caller!='' && $stat->caller!=auth()->user()->id)
            {
                $data['roomExists']= true;
                $data['con_link']=$stat->con_link;
                return sendSuccessResponse("",$data);
            }
            else{
                $stat->con_link = $request->room;
                $stat->caller = auth()->user()->id;
                if($stat->save())
                {
                    $data['roomExists']= false;
                    $data['con_link']=$stat->con_link;
                    return sendSuccessResponse("",$data);
                }

            }
        }

        else{
            return sendFailureMessage("Something went wrong.");
        }

    }
    public function removeCall($id)
    {
        $stat= LiveConsultation::where('id',$id)->where('provider_id',auth()->user()->id)->first();
        if($stat->request_status >= 3)
        {
            $stat->con_link = '';
            $stat->caller = null;
            if($stat->save())
                return sendSuccessResponse("Call disconnected.");
        }
        else{
            return sendFailureMessage("Something went wrong.");
        }

    }
}
