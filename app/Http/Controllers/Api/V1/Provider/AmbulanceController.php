<?php

namespace App\Http\Controllers\Api\V1\Provider;

use App\Http\Controllers\Controller;
use App\Http\Resources\AmbulanceBookingResource;
use App\Models\AmbulanceBooking;
use App\Models\ServiceProfile;
use App\Models\Setting;
use App\Service\AppNotificationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AmbulanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $config;
    public function __construct(){
        $this->middleware('provider');
        $setting = Setting::all();
        foreach ($setting as $data)
        {
            $this->config[$data->param] = $data->data;
        }
    }
    public function index()
    {
        $bookings = AmbulanceBooking::where('provider_id',auth()->user()->id)->orderby('id','desc')->get();
        if(!$bookings->isEmpty())
        {
          $data['bookings'] = AmbulanceBookingResource::collection($bookings);
          return sendSuccessResponse("",$data);
        }
        else{
            return sendFailureMessage("No Ambulance Booking Were Found.");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'booking_id' => 'required|numeric',
            'remarks' => 'required|string',
        ]);
        if ($validator->fails()) {
          return sendValidationError($validator->errors());
        }

            $booking= AmbulanceBooking::where('id',$request->booking_id)->where('provider_id',auth()->user()->id)->first();
            if($booking!='')
            {
                $booking->remarks = $request->remarks;
                $booking->is_completed =  1;
                if($booking->save())
                {
                    ServiceProfile::where('id',$booking->service_profile_id)->update(['is_available'=>1]);
                    return sendSuccessResponse("Booking updated successfully.");
                }
                else{
                    return sendFailureMessage("Unable to update booking.");

                }
            }
            else{
              return sendFailureMessage("Data not available.");
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = AmbulanceBooking::where('id', $id)
            ->where('provider_id', auth()->user()->id)
            ->first();
        if ($booking == '') {
            return sendFailureMessage("Data not accessible.");
        } else {
            $detail = array();
            $detail['request_accepted'] = $booking->is_request_accepted;
            $detail['request_rejected'] = $booking->is_request_rejected;
            $detail['is_paid'] = $booking->is_paid;
            $detail['is_completed'] = $booking->is_completed;
            $detail['approx_fair'] = $booking->approx_fair;
//            $detail['url'] = $booking->is_request_accepted && !$booking->is_paid ? url('api/user/pay-ambulance-fee/'.$booking->patient_id.'/'.$booking->id):'';
return sendSuccessResponse("",$detail);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'request_status' => 'required|string',
            'approx_fair' => 'nullable|numeric',
            'advance_payment' => 'required|numeric',
        ]);
        if ($validator->fails()) {
           return sendValidationError($validator->errors()->toArray());
        }
        $booking = AmbulanceBooking::where('id', $id)->first();
        if ($booking->booking_date != date('Y-m-d')) {
            return sendFailureMessage("You can only accept or reject same date requests.");
        }
        if ($request->request_status == "Accepted") {
            if ($booking->is_request_rejected) {
                return sendFailureMessage("You have already rejected this request.Action denied");
            }
            if(!$request->has('approx_fair'))
            {
                return sendFailureMessage("Kindly enter approx fair of the complete journey.");
            }
            if ($booking->is_request_accepted) {

               return sendSuccessResponse("You have already accepted this request. Wait for patient's response.");

            } else {
if($request->advance_payment < $this->config['ambulance_advance'])
{
    return sendFailureMessage("Advance payment amount is not allowed. Minimum amount allowed is Rs. ". $this->config['ambulance_advance']);
}
                $data['is_request_accepted'] = 1;
                $data['approx_fair'] = $request->approx_fair;
                $data['advance_payment'] = $request->advance_payment;
                if ($booking->update($data)) {
                    $text = "Dear User, your request has been accepted by the service provider. Regards OMEDDr.";
                    sms_sender(trim($booking->patient->mobile), $text,$this->config['sms_provider_request']);
                    if($booking->patient->device_id!='')
                    {
                        $content = array(
                            "category" => "ambulance",
                            "type" => "booking",
                            "user_role" => 0,
                            "booking_id" => $booking->id,
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                        );

                        $notification = array(
                            "title" => "Ambulance Booking",
                            "body" => "Your recent ambulance booking request has been accepted by the service provider.",
                       "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                        );
                         AppNotificationService::sendSingleUserNotification($booking->patient->device_id,$content, $notification);
                    }
                }

              return sendSuccessResponse("Thanks for accepting this request. Wait for patient's response.");

            }
        }

        if($request->request_status == "Rejected")
        {
            if($booking->is_request_accepted)
            {
             return sendFailureMessage("You have already accepted this request.Action denied");

            }
            if($booking->is_request_rejected)
            {
              return sendSuccessResponse("You have already rejected this request.");
                          }
            else{
                if($booking->update(['is_request_rejected'=>1]))
                {
                    $text = "Dear User, your request has been rejected by the service provider. Regards OMEDDr.";
                    sms_sender(trim($booking->patient->mobile), $text,$this->config['sms_provider_request']);
                    if($booking->patient->device_id!='')
                    {
                        $content = array(
                            "category" => "ambulance",
                            "type" => "booking",
                            "user_role" => 0,
                            "booking_id" => $booking->id,
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                        );
                        $notification = array(
                            "title" => "Ambulance Booking",
                            "body" => "Your recent ambulance booking request has been rejected by the service provider.",
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                        );
                         AppNotificationService::sendSingleUserNotification($booking->patient->device_id,$content, $notification);
                    }
                  return sendSuccessResponse("Request rejected. Thanks for your reponse.");
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ab= AmbulanceBooking::FindOrFail($id);
        if($ab->delete()){
            return sendSuccessResponse("Booking removed.");
        }
        else{
            return sendFailureMessage("Unable to remove this booking.");
        }
    }
}
