<?php

namespace App\Http\Controllers\Api\V1\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppointmentBookingRequest;
use App\Http\Resources\AppointmentBookingResource;
use App\Http\Resources\AppointmentDetailResource;
use App\Models\AppointmentBooking;
use App\Service\ImageUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class AppointmentBookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('provider');
    }

    public function index()
    {
        $appoint = AppointmentBooking::where('provider_id',auth()->user()->id)
                                        ->where('is_paid',1)
                                        ->orderby('id','desc')
                                        ->get();
        if($appoint->isNotEmpty())
        {
            $detail['appointment'] = AppointmentBookingResource::collection($appoint);
            return sendSuccessResponse("",$detail);
        }
        return sendFailureMessage("You have not any appointment yet.");

    }

    public function setAppointmentTiming(Request $request,$id){
        $validator=Validator::make($request->all(),[
            'appointment_time' => 'required|datetime'
        ]);
        if($validator->fails())
        {
            return sendValidationError($validator->errors()->toArray());
        }
        $appoint= AppointmentBooking::where('id',$id)->where('provider_id',auth()->user()->id)->first();
        if($appoint!='')
        {
            $appoint->appointment_time = $request->appointment_time;
            if($appoint->save())
            {
                return sendSuccessResponse("Appointment time has been updated.");
            }
            return sendFailureMessage("Unable to update appointment time.");
        }
        return sendFailureMessage("Unable to update appointment time.");
    }

//     Update Appointment Booking

    public function updateAppointment(AppointmentBookingRequest $request)
    {
        $data = $request->validated();
         $booking = AppointmentBooking::where('id', $request->booking_id)->first();
//            $data=$request->except('booking_id');
        $mno = $booking->patient->mobile;
        $directory = $mno . '_' . time();
        if ($request->prescription == '') {
            $data['prescription'] = $booking->prescription;
        }
        if ($request->hasFile('prescription_image')) {
            $file = $request->file('prescription_image');
            $path = public_path('uploads/patientprofile/prescription/');
            $upload = ImageUpload::uploadImage($file, $path, $directory);
            if ($upload['status']) {
                if (!empty($booking->prescription_folder)) {
                    $path = public_path('uploads/patientprofile/prescription/' . $booking->prescription_folder . '/' . $booking->prescription_image);
                    if (File::exists($path)) {
                        File::deleteDirectory($path);
                    }
                }
                $data['prescription_folder'] = $upload['folder_name'];
                $data['prescription_image'] = $upload['file'];
//                    $data['is_con_link_expired'] = 1;

            } else {
                return sendFailureMessage("Unable to upload prescription.");
            }
        } else {
            $data['prescription_folder'] = $booking->prescription_folder;
            $data['prescription_image'] = $booking->prescription_image;
        }

        if ($request->has('remarks')) {
            $data['is_completed'] = 1;
        }
        $data['booking_date'] =date('Y-m-d',strtotime($data['booking_date']));
        if (AppointmentBooking::where('id', $request->booking_id)->update($data)) {
            return sendSuccessResponse("Appointment has been updated.");
            } else {
            return sendFailureMessage("Unable to update appointment.");
        }

    }

    public function getAppointmentDetail($id)
    {
        $booking=AppointmentBooking::where('id',$id)->where('provider_id',auth()->user()->id)->get();
        if($booking->isNotEmpty())
        {
            $data['appointment'] = AppointmentDetailResource::collection($booking);
            return sendSuccessResponse("",$data);
        }
        return sendFailureMessage("No data were found.");
    }
}
