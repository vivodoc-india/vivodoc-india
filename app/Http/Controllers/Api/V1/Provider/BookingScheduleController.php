<?php

namespace App\Http\Controllers\Api\V1\Provider;

use App\Http\Resources\BookingScheduleResource;
use App\Models\BookingSchedule;
use App\Models\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookingScheduleRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookingScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('provider');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $schedule=Event::where('user_id',auth()->user()->id)->orderby('id','desc')->get();
        if($schedule->isNotEmpty()) {
       $data['schedules'] = BookingScheduleResource::collection($schedule);
       return sendSuccessResponse("",$data);
        }
        else{
            return sendFailureMessage("You have not created any schedule.");
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookingScheduleRequest $request)
    {
        $startTime = date('Y-m-d',strtotime($request->start_date));
        $endTime = date('Y-m-d',strtotime($request->end_date));
        $eventsCount=BookingSchedule::where('user_id',auth()->user()->id)
                                    ->whereBetween('date',[$startTime,$endTime])
                                    ->orWhereBetween('date',[$startTime,$endTime])
                                    ->count();
        if($eventsCount > 0)
        {
        return sendFailureMessage("You have already scheduled for selected date.");
        }
        $event= new Event();
        $event->title=$request->title;
        $event->user_id=auth()->user()->id;
        $event->start_date=date('Y-m-d',strtotime($request->start_date));
        $event->end_date=date('Y-m-d',strtotime($request->end_date));
        $event->is_off = $request->is_off;
        if(!$request->is_off)
        {
            $event->total_bookings = $request->total_bookings;
            $event->timings = implode(',',$request->timings);
        }

        if($event->save())
        {
            if(!$request->is_off){
                $s_date=date('Y-m-d',strtotime($request->start_date));
                $e_date = date('Y-m-d',strtotime($request->end_date));

                while (strtotime($s_date )<= strtotime( $e_date))
                {
                    $ins= BookingSchedule::create([
                        'user_id' => auth()->user()->id,
                        'event_id' => $event->id,
                        'date' => $s_date,
                        'total_bookings' => $request->total_bookings,
                        'timings' => implode(',',$request->timings),
                    ]);

                    if($ins)
                    {
                        $s_date = date('Y-m-d',strtotime('+1 day',strtotime($s_date)));
                    }
                }
            }
            return sendSuccessResponse("Schedule created successfully.");
        }
        else{
            return sendFailureMessage("Unbale to save this schedule.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event= Event::with(['booking_schedule'])->where('id',$id)->get();
        if($event->isEmpty())
        {
            return sendFailureMessage("Data not available.");
        }
       $data['booking_events'] = BookingScheduleResource::collection($event);
       return sendSuccessResponse("",$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookingScheduleRequest $request, $id)
    {

            $event = Event::find($id);
            $event->title = $request->title;
            $event->color = $request->color;
            $event->text_color = $request->text_color;
            $event->is_off = $request->is_off;
            if (!$request->is_off) {
                $event->total_bookings = $request->total_bookings;
                $event->timings = implode(',', $request->timings);
            }
            if ($request->is_off) {
                $event->total_bookings = 0;
                $event->timings = " ";
            }
            if ($event->save() && !$request->is_off) {
                $s_date = date('Y-m-d', strtotime($event->start_date));
                $e_date = date('Y-m-d', strtotime($event->end_date));

                while (strtotime($s_date) <= strtotime($e_date)) {

                    $chk = BookingSchedule::where('provider_id', auth()->user()->id)->where('event_id', $event->id)->where('date', $s_date)->count();
                    if ($chk > 0) {
                        $ins = BookingSchedule::where('provider_id', auth()->user()->id)
                            ->where('event_id', $event->id)
                            ->where('date', $s_date)
                            ->update([
                            'total_bookings' => $request->total_bookings,
                        ]);
                    } else {
                        $ins = BookingSchedule::create([
                            'provider_id' => auth()->user()->id,
                            'event_id' => $event->id,
                            'date' => $s_date,
                            'total_bookings' => $request->total_bookings,
                        ]);
                    }
                    if ($ins) {
                        $s_date = date('Y-m-d', strtotime('+1 day', strtotime($s_date)));
                    }
                }
               return sendSuccessResponse("Schedule updated.");
            }
            elseif($event->save() && $request->is_off) {
                $bs = BookingSchedule::where('provider_id', auth()->user()->id)->where('event_id', $event->id)->delete();
                return sendSuccessResponse("Schedule updated.");
            } else {
                return sendFailureMessage("Unable to update schedule.");
            }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteEvent= Event::where('id',$id)->where('user_id',auth()->user()->id)->first();
        if($deleteEvent!='')
        {
            if($deleteEvent->delete())
            {
                return sendSuccessResponse("Schedule removed.");
            }
            else{
                return sendFailureMessage("Unable to remove schedule.");
            }
        }
        else{
            return sendFailureMessage("No booking schedule were found.");
        }

    }

    public function updateDaywiseSchedule(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bs_id' => 'required|numeric',
            'date' => 'required|date',
            'total_bookings' => 'required|numeric',
            'is_off' => 'required|numeric|in:1,0',
        ]);
        if ($validator->fails()) {
         return sendValidationError($validator->errors()->toArray());
        }

            $event=BookingSchedule::findOrFail($request->bs_id);
            $event->date=date('Y-m-d',strtotime($request->date));
            $event->total_bookings =$request->total_bookings;
            if($request->is_off)
            {
                $data['start_date'] = date('Y-m-d',strtotime($request->date));
                $data['end_date'] = date('Y-m-d',strtotime($request->date));
                $data['total_bookings'] = 0;
                $data['user_id'] = auth()->user()->id;
//                $data['color'] = '#FA291F';
//                $data['text_color'] = "#FFFFFF";
                $data['is_off'] = $request->is_off;
                $data['title'] = "No Appointments";
                if(Event::create($data))
                {
                    if($event->delete())
                    {
                       return sendSuccessResponse("Your schedule has been updated for selected date.");
                    }
                    else
                    {
                        return sendFailureMessage("Unable to update selected date's schedule.");
                    }
                }
                else{
                    return sendFailureMessage("Unable to update selected date's schedule.");
                }

            }
            if(!$request->is_off && $event->save()) {
                return sendSuccessResponse("Your schedule has been updated for selected date.");
            }
             else
                {
                    return sendFailureMessage("Unable to update selected date's schedule.");
                }

    }
}
