<?php

namespace App\Http\Controllers\Api\V1\Patient;

use App\Http\Controllers\Controller;
use App\Http\Requests\LiveConsultationRequest;
use App\Http\Resources\LiveConsultationDetailResource;
use App\Http\Resources\LiveConsultationResource;
use App\Models\LiveConsultation;
use App\Models\ServiceProfile;
use App\Models\Setting;
use App\Models\User;
use App\Models\WalletTransaction;
use App\Service\AppNotificationService;
use App\Service\ConsultationQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class LiveConsultationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $config;

    public function __construct()
    {
        $this->middleware('patient');
        $setting = Setting::all();
        foreach ($setting as $data)
            $this->config[$data->param] = $data->data;
    }
    public function index(Request $request)
    {
        if($request->has('status'))
        {
            $bookings = LiveConsultation::where('patient_id',auth()->user()->id)
            ->where('request_status','=',(string)$request->status)
                                        ->orderby('id','desc')->get();
        }
        else{
            $bookings = LiveConsultation::where('patient_id',auth()->user()->id)->orderby('id','desc')->get();
        }

        if($bookings->isNotEmpty())
        {
            $data['bookings'] = LiveConsultationResource::collection($bookings);
            return sendSuccessResponse("",$data);
        }
        else{
            return sendFailureMessage("No data available.");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LiveConsultationRequest $request)
    {
        $data = $request->validated();
        $data['patient_id'] = auth()->user()->id;
        $data['booking_date'] = date('Y-m-d');
        if (isServiceProfileExists($request->provider_id, $request->service_profile_id) && isLive($request->service_profile_id)) {
            $existingRequest = LiveConsultation::where('provider_id', $request->provider_id)
                ->where('service_profile_id', $request->service_profile_id)
                ->where('patient_id', $request->user_id)
                ->where('request_status', 0)
                ->where('patient_name', $request->patient_name)
                ->where('booking_date', date('Y-m-d'))
                ->count();
            if ($existingRequest > 0) {
                return sendFailureMessage("You have already made a request today for this patient.");
            }
            $booking = LiveConsultation::create($data);
            if ($booking) {
                    if($booking->serviceProfile->user_role_id==9) {
                    $book['con_link_access'] = 1;
                    $book['transaction_status'] = 'SUCCESS';
                    $book['txn_amount'] = 0;
                    $book['txn_date'] = date('Y-m-d H:i:s');
                    $book['txn_response'] = 'Service is free for this category.';
                    $book['txn_number'] = 0;
                    $book['request_status'] = '3';
                    $book['referenceId'] = 'NA';
                    $book['is_con_link_available'] = 1;
                        if(LiveConsultation::where('id',$booking->id)->update($book))
                        {
                            $update=LiveConsultation::where('id',$booking->id)->select('id','provider_id','service_profile_id','con_link_access','request_status')->first();
                       $text = "Dear User, your request has been accepted by the service provider. Regards OMEDDr.";
                      sms_sender(trim($booking->patient->mobile), $text,$this->config['sms_provider_request']);
                            if($booking->patient->device_id!='')
                            {
                                $content = array(
                                "category" => "consultation",
                                "type" => "booking",
                                "user_role" => 0,
                                "booking_id" => $update->id,
                                "con_link" => $update->con_link_access==1 ?url('omed-api/patient-rtc/'.$booking->patient_id.'/'.$update->id):'',
                                "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                            );

                                $message = array(
                                    "title" => "Live Consultation",
                                    "body" => "You can now consult with the provider (".$booking->provider->name.")",
                                    "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                                );
                                 AppNotificationService::sendSingleUserNotification($booking->patient->device_id, $content,$message);
                            }

                            if($booking->provider->device_id!='')
                            {
                                $content = array(
                                    "category" => "consultation",
                                    "type" => "booking",
                                    "user_role" => 1,
                                    "booking_id" => $booking->id,
                                    "con_link" => $update->con_link_access==1 ?url('omed-api/provider-rtc/'.$booking->patient_id.'/'.$update->id):'',
                                    "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                                );
                                $message = array(
                                    "title" => "Live Consultation",
                                    "body" => "Your video consultation link from the patient( ".$booking->patient_name." ) has been activated",
                                    "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                                    );
                                 AppNotificationService::sendSingleUserNotification($booking->provider->device_id,$content, $message);
                            }

                            $detail = array();
                            $detail['booking_id'] = $update->id;
                            $detail['provider_id'] = (string) $update->provider_id;
                            $detail['service_profile_id'] = (string) $update->service_profile_id;
                            $detail['con_link_access'] = (int) $update->con_link_access;
                            $detail['con_link'] = $update->con_link_access==1 ?url('omed-api/patient-rtc/'.$booking->patient_id.'/'.$update->id):'';
                            $detail['request_status']=(int) $update->request_status;
                 return sendSuccessResponse("Request sent. Wait for the provider to join the consultation.", $detail);
                        }
                    }
                    else{
//                        LiveConsultation::where('id',$booking->id)->update($book);
                        $detail['booking_id'] = $booking->id;
                        $detail['provider_id'] = $booking->provider_id;
                        $detail['service_profile_id'] = $booking->service_profile_id;
                        $detail['con_link_access'] = (int) $booking->con_link_access;
                        $detail['con_link'] = $booking->con_link_access==1 ?url('omed-api/patient-rtc/'.$booking->patient_id.'/'.$booking->id):'';;
                        $detail['request_status']=(int) $booking->request_status;
                        $detail['wallet_amount'] = auth()->user()->wallet_amount;
                        $detail['consultation_fee'] = $booking->serviceProfile->min_fee;
                        return sendSuccessResponse("Pay the consultation fee.", $detail);
                    }
            } else {
                return sendFailureMessage("Service provider is not available.");
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $n=1;
        $booking = LiveConsultation::where('id',$id)->where('patient_id',auth()->user()->id)->get();
        foreach($booking as $book)
        {
            $today_consultation = LiveConsultation::where('provider_id',$book->provider_id)
                ->where('booking_date',today())
                ->where('request_status','3')
                ->select('id')
                ->orderBy('id','asc')
                ->get();

            foreach($today_consultation as $tcon)
            {
                if($tcon->id===$book->id && $book->request_status=='3')
                {
                    $data['mynumber'] = $n;
                }
                else{
                    $n++;
                }
            }
            $data['today_consultation']=ConsultationQueue::countRequests($book->provider_id);
            $data['provider_status']=ConsultationQueue::providerStatus($book->provider_id,$book->service_profile_id);
        }

        if($booking->isEmpty())
        {
            return sendFailureMessage("No data available.");
        }
        else{
            $data['consultation_detail'] = LiveConsultationDetailResource::collection($booking);
            return sendSuccessResponse("",$data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $booking= LiveConsultation::where('id',$id)
                                    ->where('patient_id',auth()->user()->id)
                                    ->where('booking_date',date('Y-m-d'))
                                    ->first();
        if($booking=='')
        {
         return sendFailureMessage("Data not available.");
        }
        if($booking->serviceProfile->mark_live == 0)
        {
            return sendFailureMessage("The service goes offline now.Pay latter for this request or consult with other service provider");
        }
        if($booking->request_status >= '3')
        {
            $detail['request_status'] = (int) $booking->request_status;
            return sendSuccessResponse("You have already paid for this request.",$detail);
        }
        $orderAmount = $booking->serviceProfile->min_fee;
        $wallet_pay =0;
        $wallet_deduction = 0;
        if($request->has('wallet_pay') && $request->wallet_pay==1 && auth()->user()->wallet_amount > 0 && auth()->user()->wallet_amount > $orderAmount)
        {
            $wallet_pay=1;
            $wallet_deduction=$orderAmount;

            $update['cons_amount'] = $orderAmount;
            $update['who_pay'] = $request->getClientIp();
            $update['wallet_pay'] = $wallet_pay;
            $update['wallet_deduction']= $wallet_deduction;
            $update['con_link_access']=1;
            $update['transaction_status'] ='SUCCESS';
            $update['txn_amount'] = $wallet_deduction;
            $update['txn_date'] =date('Y-m-d');
            $update['txn_response'] = "OMED Coin Payment";
            $update['txn_number'] = Str::random(15);
            $update['request_status'] = '3';
            $update['referenceId'] = 'NA';
            $update['payment_mode'] = 'OMED Coins';
            $update['is_con_link_available']=1;
            if($booking->update($update))
            {
                $wallet['user_id'] = auth()->user()->id;
                $wallet['transaction_number'] = $update['txn_number'];
                $wallet['transaction_status'] = $update['transaction_status'];
                $wallet['transaction_date'] =  $update['txn_date'];
                $wallet['transaction_status'] =$update['transaction_status'];
                $wallet['transaction_response'] = $update['txn_response'];
                $wallet['transaction_amount'] = $update['txn_amount'];
                $wallet['payment_mode'] = $update['payment_mode'];
                $wallet['referenceId'] =  $update['referenceId'];
                $wallet['who_pay'] =   $update['who_pay'];
                $wallet['transaction_type'] =   "debit";
                if(WalletTransaction::create($wallet))
                {
                    $newWalletAmount = auth()->user()->wallet_amount - $wallet_deduction;
                    User::where('id',auth()->user()->id)->update(['wallet_amount'=>$newWalletAmount]);
                }
                $text = "Dear User,your video consultation link for recently accepted request has been activated. Regards OMEDDr.";
                sms_sender(trim($booking->provider->mobile), $text,$this->config['sms_active_video_link']);
                if($booking->provider->device_id!='')
                {
                    $content =array(
                        "category" => "consultation",
                        "type" => "live_calling",
                        "user_role" => 1,
                        "booking_id" =>$booking->id,
                        "con_link" =>url('omed-api/provider-rtc/'.$booking->provider_id.'/'.$booking->id),
                        "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                    );
                    $notification = array(
                        "title" => "Live Consultation",
                        "body" => "Your video consultation link from the patient( ".$booking->patient_name." ) has been activated",
                        "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                    );
                    AppNotificationService::sendSingleUserNotification($booking->provider->device_id, $content,$notification);
                }
                if($booking->patient->device_id!='')
                {
                    $content =array(
                        "category" => "consultation",
                        "type" => "live_calling",
                        "user_role" => 0,
                        "booking_id" =>$booking->id,
                        "con_link" =>url('omed-api/patient-rtc/'.$booking->patient_id.'/'.$booking->id),
                        "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                    );
                    $notification = array(
                        "title" => "Live Consultation",
                        "body" => "You can now consult with the provider (".$booking->provider->name.")",
                        "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                    );
                    AppNotificationService::sendSingleUserNotification($booking->patient->device_id,$content, $notification);
                }
                $detail['request_status']= (int) $booking->request_status;
                $detail['con_link_access'] = (int) $booking->con_link_access;
                $detail['con_link'] =   $booking->con_link_access==1 ?url('omed-api/patient-rtc/'.$booking->patient_id.'/'.$booking->id):'';
                return sendSuccessResponse("",$detail);
            }
        }
        elseif($request->has('wallet_pay') && $request->wallet_pay==1 && auth()->user()->wallet_amount > 0 && auth()->user()->wallet_amount < $orderAmount){
            $wallet_pay=1;
            $wallet_deduction=auth()->user()->wallet_amount;
            $update['wallet_pay'] = $wallet_pay;
            $update['wallet_deduction']= $wallet_deduction;
           if($booking->update($update))
           {
               $detail['url'] = url('omed-api/user/pay-consultation-fee/'.$booking->patient_id.'/'.$booking->id);
               $detail['request_status']= (int) $booking->request_status;
               $detail['con_link_access'] = (int) $booking->con_link_access;
               $detail['con_link'] =   $booking->con_link_access==1 ?url('omed-api/patient-rtc/'.$booking->patient_id.'/'.$booking->id):'';
               return sendSuccessResponse("",$detail);
           }
        }
        else{
            $wallet_pay=0;
            $wallet_deduction=0;
            $update['wallet_pay'] = $wallet_pay;
            $update['wallet_deduction']= $wallet_deduction;
            if($booking->update($update))
            {
                $detail['url'] = url('omed-api/user/pay-consultation-fee/'.$booking->patient_id.'/'.$booking->id);
                $detail['request_status']= (int) $booking->request_status;
                $detail['con_link_access'] = (int) $booking->con_link_access;
                $detail['con_link'] =   $booking->con_link_access==1 ?url('omed-api/patient-rtc/'.$booking->patient_id.'/'.$booking->id):'';
                return sendSuccessResponse("",$detail);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function isPaid($id)
    {
        $booking= LiveConsultation::where('id',$id)
            ->where('patient_id',auth()->user()->id)
            ->first();
        if($booking=='')
        {
            return sendFailureMessage("Booking data not accessible.");
        }
        else{
            $detail['request_status']= (int) $booking->request_status;
            $detail['con_link_access'] = (int) $booking->con_link_access;
            $detail['con_link'] =   $booking->con_link_access==1 ?url('omed-api/patient-rtc/'.$booking->patient_id.'/'.$booking->id):'';
            return sendSuccessResponse("",$detail);
        }
    }

    // initiate Call By Patient
    public function initiateCallByPatient(Request $request)
    {
            $validator = Validator::make($request->all(), [
                'id' =>'required|numeric',
                'room' => 'required|string',
            ]);

            if ($validator->fails()) {
                return sendValidationError($validator->errors()->toArray());
            }
        $stat= LiveConsultation::where('id',$request->id)->where('patient_id',auth()->user()->id)->first();
        if($stat->request_status >= 3)
        {
            if($stat->caller!='' && $stat->caller!=auth()->user()->id)
            {
               $data['roomExists']= true;
               $data['con_link']=$stat->con_link;
                return sendSuccessResponse("",$data);
            }
            else{
                $stat->con_link = $request->room;
                $stat->caller = auth()->user()->id;
                if($stat->save())
                {
                    $data['roomExists']= false;
                    $data['con_link']=$stat->con_link;
                    return sendSuccessResponse("",$data);
                }

            }
        }

        else{
         return sendFailureMessage("Something went wrong.");
        }

    }
    public function removeCall($id)
    {
            $stat= LiveConsultation::where('id',$id)->where('patient_id',auth()->user()->id)->first();
            if($stat->request_status >= 3)
            {
                $stat->con_link = '';
                $stat->caller = null;
                if($stat->save())
                   return sendSuccessResponse("Call disconnected.");
            }
            else{
               return sendFailureMessage("Something went wrong.");
            }

    }

}
