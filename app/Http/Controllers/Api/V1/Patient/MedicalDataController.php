<?php

namespace App\Http\Controllers\Api\V1\Patient;

use App\Http\Controllers\Controller;
use App\Http\Requests\MedicalDataRequest;
use App\Http\Resources\MedicalDataResource;
use App\Models\MedicalData;
use App\Service\ImageUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class MedicalDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      $med_data = MedicalData::where('data_type',$request->data_type)
                                ->where('user_id',auth()->user()->id)
                                ->where('parent',0)
                                ->select('id','title')
                                ->orderby('updated_at','desc')
                                ->get()
                                ->toArray();
        return sendSuccessResponse("",$med_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedicalDataRequest $request)
    {
        $data = $request->validated();
        $data['user_id']= auth()->user()->id;
        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $path= public_path('uploads/patientprofile/medical_data/');
            $directory = auth()->user()->mobile.'_'.rand();
            $upload = ImageUpload::uploadImage($file,$path,$directory);
            if($upload['status'])
            {
                $data['folder'] = $upload['folder_name'];
                $data['file'] = 'uploads/patientprofile/medical_data/'.$upload['folder_name'].'/'.$upload['file'];

            }
        }
        $record = MedicalData::create($data);
        if($record)
        {
           $detail=array();
            if($request->parent > 0)
            {
                $detail['id'] = $record->id;
                $detail['file'] = asset($record->file);
                $detail['uploaded_on'] = date('d-m-Y h:i:s a',strtotime($record->updated_at));
                MedicalData::where('id',$request->parent)->update(['updated_at'=>now()]);
            }
            else{
                $detail['id'] = $record->id;
                $detail['title'] = $record->title;
            }
            return sendSuccessResponse("Data uploaded.",$detail);
        }
        else{
            return sendFailureMessage("Unable to save your data.");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $med_data = MedicalData::where('parent',$id)
            ->where('user_id',auth()->user()->id)
            ->select('id','file','updated_at')
            ->orderby('updated_at','desc')
            ->get();
         if($med_data->isNotEmpty())
         {
             $data['records']=MedicalDataResource::collection($med_data);
             return sendSuccessResponse("",$data);
         }
         else{
             return sendFailureMessage("This folder is empty.");
         }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MedicalDataRequest $request, $id)
    {
//    $validator =Validator::make($request->all,[
//        'data_type' => 'required|string|in:lab_test,prescription',
//        'parent' => 'required|numeric',
//        'title' => 'required|string',
//    ]);
//    if($validator->fails())
//    {
//        return sendValidationError($validator->errors()->toArray());
//    }
//    $data=$request->only('data_type','parent','title');
    $data=$request->validated();
        $data['user_id']= auth()->user()->id;
        if(MedicalData::where('id',$id)->update($data))
        {
            if($request->parent > 0)
            {
                MedicalData::where('id',$request->parent)->update(['updated_at'=>now()]);
            }
            return sendSuccessResponse("Data updated.");
        }
        else{
            return sendFailureMessage("Unable to update your data.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $record = MedicalData::where('user_id',auth()->user()->id)->where('id',$id)->first();
    if($record!='')
    {
        if($record->parent > 0)
        {
            MedicalData::where('id',$record->parent)->update(['updated_at'=>now()]);
        }
        if($record->parent == 0)
        {
            MedicalData::where('parent',$record->id)->delete();
        }
            if($record->delete())
            {

                return sendSuccessResponse("Data removed.");
            }
            else{
                return sendFailureMessage("unable to remove selected data.");
            }
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateRecordFile(MedicalDataRequest $request)
    {
        if(!$request->has('id') && !is_numeric($request->id))
        {
            return sendFailureMessage("Data not found.");
        }
        $data=$request->validated();
        $data['user_id']= auth()->user()->id;
        $medical_data= MedicalData::findOrFail($request->id);
        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $path= public_path('uploads/patientprofile/medical_data/');
            $directory = auth()->user()->mobile.'_'.rand();
            $upload = ImageUpload::uploadImage($file,$path,$directory);
            if($upload['status'])
            {
                $data['folder'] = $upload['folder_name'];
                $data['file'] = 'uploads/patientprofile/medical_data/'.$upload['folder_name'].'/'.$upload['file'];
                if(File::exists(asset($medical_data->file))) {
                    File::delete(asset($medical_data->file));
                }
            }
        }
        if(MedicalData::where('id',$request->id)->update($data))
        {
            if($request->parent > 0)
            {
                MedicalData::where('id',$request->parent)->update(['updated_at'=>now()]);
            }
            return sendSuccessResponse("Data updated.");
        }
        else{
            return sendFailureMessage("Unable to update your data.");
        }
    }
}
