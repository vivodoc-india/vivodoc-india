<?php

namespace App\Http\Controllers\Api\V1\Patient;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppointmentBookingRequest;
use App\Http\Resources\AppointmentBookingResource;
use App\Http\Resources\AppointmentDetailResource;
use App\Models\AppointmentBooking;
use App\Models\BookingSchedule;
use App\Models\ServiceProfile;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AppointmentController extends Controller
{
    private $config;
    public function __construct()
    {
        $this->middleware('patient');
        $setting = Setting::all();
        foreach ($setting as $data)
        {
            $this->config[$data->param] = $data->data;
        }

    }

    public function index(Request $request)
    {
        if($request->has('status') && $request->status==1)
        {
            $appoint = AppointmentBooking::where('patient_id',auth()->user()->id)
                ->where('is_completed',1)
                ->where('prescription','!=','')
                ->orWhere('prescription_image','!=','')
                ->orderby('id','desc')
                ->get();
        }
        else{
            $appoint = AppointmentBooking::where('patient_id',auth()->user()->id)
                ->orderby('id','desc')
                ->get();
        }

if($appoint->isNotEmpty())
{
$detail['appointment'] = AppointmentBookingResource::collection($appoint);
return sendSuccessResponse("",$detail);
}
return sendFailureMessage("You have not booked any appointment yet.");

    }

    public function providerSchedule(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'provider_id' => 'required|numeric',
            'date' => 'required|date',
        ]);
        if ($validator->fails()) {
           return sendValidationError($validator->errors()->toArray());
        }
        $today_schedule = BookingSchedule::where('user_id',$request->provider_id)
                                            ->where('date',date('Y-m-d',strtotime($request->date)))
                                            ->first();
        if($today_schedule=='')
        {
            return sendFailureMessage("No appointment schedule found for ".date('d-m-Y',strtotime($request->date)).".");
        }
        $detail['total_booking'] =$today_schedule->total_bookings;
        $today_booking =  AppointmentBooking::where('provider_id',$request->provider_id)
                                                ->where('booking_date',date('Y-m-d',strtotime($request->date)))
                                                ->where('is_paid',1)
                                                ->count();
        $detail['today_booking'] =$today_booking;
        $detail['timing']=$today_schedule->event->timings;
        return sendSuccessResponse("",$detail);
    }

    public function store(AppointmentBookingRequest $request)
    {
 $data = $request->validated();
 if(strtotime($request->booking_date) - strtotime(date('d-m-Y')) < 0)
 {
     return sendFailureMessage("Backdate bookings are not allowed.");
 }
 $data['booking_date'] = date('Y-m-d',strtotime($request->booking_date));
 $data['patient_id']=auth()->user()->id;
 $chk = AppointmentBooking::where('provider_id',$request->provider_id)
     ->where('patient_id',auth()->user()->id)
     ->where('booking_date',date('Y-m-d',strtotime($request->booking_date)))
     ->where('is_paid',0)
     ->first();
 if($chk!='')
 {
     $serviceProfile= ServiceProfile::where('id',$request->service_profile_id)->first();
//     $detail['fee_to_pay'] = $serviceProfile->max_fee - (($serviceProfile->max_fee*$this->config['appointment_discount'])/100);
     $detail['url'] = url('omed-api/user/pay-appointment-fee/'.auth()->user()->id.'/'.$chk->id);
     $detail['booking_id'] = $chk->id;
     return sendSuccessResponse("You have already an appointment request for selected date. Proceed for payment.",$detail);
 }
        $total_book = AppointmentBooking::where('provider_id',$request->provider_id)
                    ->where('booking_date',date('Y-m-d',strtotime($request->booking_date)))
                    ->where('is_paid',1)
                    ->count();
        $today_schedule = BookingSchedule::where('user_id',$request->provider_id)
                        ->where('date',date('Y-m-d',strtotime($request->booking_date)))
                        ->first();
        if(!empty($today_schedule))
        {
            if($total_book > 0 && $total_book >= $today_schedule->total_bookings)
            {
                return sendFailureMessage("Bookings are full for the selected date.");
            }
        }
        else{
            $response['status']=0;
            return sendFailureMessage("No booking schedule is found for the selected date.");
        }
        $serviceProfile= ServiceProfile::where('id',$request->service_profile_id)->first();
        $booking=AppointmentBooking::create($data);
//        $detail['fee_to_pay'] = $serviceProfile->max_fee - (($serviceProfile->max_fee*$this->config['appointment_discount'])/100);
        if($booking)
        {
            $detail['url'] = url('omed-api/user/pay-appointment-fee/'.$booking->patient_id.'/'.$booking->id);
            $detail['booking_id'] = $booking->id;
           return sendSuccessResponse("Request Saved. Proceed for payment.",$detail);
        }
        else{
            return sendFailureMessage("Unable to book appointment.");
        }
    }
    public function show($id)
    {
        $booking=AppointmentBooking::where('id',$id)->where('patient_id',auth()->user()->id)->get();
        if($booking->isNotEmpty())
        {
            $data['appointment'] = AppointmentDetailResource::collection($booking);
            return sendSuccessResponse("",$data);
        }
        return sendFailureMessage("No data were found.");
    }

    // Check Status Of Appointment Payment Status
    public function isPaidAppointment($id)
    {
            $booking= AppointmentBooking::where('id',$id)
                ->where('patient_id',auth()->user()->id)
                ->first();

            if($booking=='')
            {
               return sendFailureMessage("Appointment data is not accessible.");

            }
            else{
                $detail['is_paid']= $booking->is_paid;
               return sendSuccessResponse("",$detail);

            }
        }

}
