<?php

namespace App\Http\Controllers\Api\V1\Patient;

use App\Http\Controllers\Controller;
use App\Http\Requests\AmbulanceBookingRequest;
use App\Http\Requests\AmbulancePaymentRequest;
use App\Http\Resources\AmbulanceBookingResource;
use App\Models\AmbulanceBooking;
use App\Models\ServiceProfile;
use App\Models\Setting;
use App\Service\AppNotificationService;
use Illuminate\Http\Request;

class AmbulanceBookingController extends Controller
{
private $config;

    public function __construct()
    {
        $this->middleware('patient');

        $setting = Setting::all();
        foreach ($setting as $data)
        {
            $this->config[$data->param] = $data->data;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $bookings = AmbulanceBooking::where('patient_id',auth()->user()->id)->orderby('id','desc')->get();
            if(!$bookings->isEmpty())
            {
$data['bookings'] = AmbulanceBookingResource::collection($bookings);
return sendSuccessResponse("",$data);

            }
            else{
                return sendFailureMessage("No Ambulance Booking Were Found.");
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AmbulanceBookingRequest $request)
    {
        $data= $request->validated();
        $data['patient_id'] = auth()->user()->id;
        $is_live=ServiceProfile::where('id',$request->service_profile_id)->where('user_id',$request->provider_id)->first();
        if($is_live->mark_live && $is_live->is_available)
        {
$data['ambulance_booking_fee'] =$is_live->min_fee;
            $chk_appointment= AmbulanceBooking::where('provider_id',$request->provider_id)
                ->where('service_profile_id',$request->service_profile_id)
                ->where('patient_id',auth()->user()->id)
                ->where('is_request_rejected','0')
                ->where('is_paid','0')
                ->where('is_completed','0')
                ->where('patient_name',$request->patient_name)
                ->where('booking_date',date('Y-m-d'))
                ->count();
            if($chk_appointment)
            {
                return sendFailureMessage("You have already made a request today for this patient.");
                           }
            else{
                $data['booking_date'] = date('Y-m-d');
                $ambulance_book= AmbulanceBooking::create($data);
                if($ambulance_book)
                {
                    $text = "Dear User, A patient ( ".$ambulance_book->patient->name." ) sent you request for ambulance booking. Regards OMEDDr.";
                    sms_sender(trim($ambulance_book->provider->mobile), $text,$this->config['sms_patient_request']);
                    if($ambulance_book->provider->device_id!='')
                    {
                        $content = array(
                          "category" => "ambulance",
                          "type" => "booking",
                          "user_role" => 1,
                          "booking_id" => $ambulance_book->id,
                          "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                        );
                        $notification = array(
                            "title" => "Ambulance Booking",
                            "body" => "A patient ( ".$ambulance_book->patient->name." ) sent you request for ambulance booking.",
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                            );
                         AppNotificationService::sendSingleUserNotification($ambulance_book->provider->device_id,$content, $notification);
                    }
                    $detail=array();
                    $detail['booking_id'] = $ambulance_book->id;
                    $detail['provider_id'] = $ambulance_book->provider_id;
                    $detail['service_profile_id'] = $ambulance_book->service_profile_id;
                    return sendSuccessResponse("Request sent. Wait for approval.",$detail);
                }
                else{
                    return sendFailureMessage("Unable to send request.");
                }
            }
        }
        else
        {
            return sendFailureMessage("Service Provider is not available for booking.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = AmbulanceBooking::where('id',$id)
            ->where('patient_id',auth()->user()->id)
            ->first();
        if($booking=='')
        {
            return sendFailureMessage("Data not accessible.");
        }
        else{
            $detail = array();
            $detail['request_accepted'] = $booking->is_request_accepted;
            $detail['request_rejected'] = $booking->is_request_rejected;
            $detail['is_paid'] = $booking->is_paid;
            $detail['is_completed'] = $booking->is_completed;
            $detail['amount'] = $booking->advance_payment;
            $detail['approx_fair'] = (string) $booking->approx_fair;
            $detail['url'] = $booking->is_request_accepted && !$booking->is_paid ? url('omed-api/user/pay-ambulance-fee/'.$booking->patient_id.'/'.$booking->id):'';
            return sendSuccessResponse("",$detail);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AmbulancePaymentRequest $request, $id)
    {
        $data = $request->validated();
        $data['is_paid'] = 1;
        if($data['transaction_status'] == "SUCCESS")
        {
            $amb = AmbulanceBooking::findOrFail($id);
            if($amb->update($data))
            {
                ServiceProfile::where('id',$amb->service_profile_id)->update(['is_available'=>0]);
               return sendSuccessResponse("Payment successful.");
            }
            else
            {
               return sendFailureMessage("Unable to process request.");

            }
        }
        else
        {
            return sendFailureMessage("Unable to process request.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ab= AmbulanceBooking::FindOrFail($id);
        if($ab->delete()){
            return sendSuccessResponse("Booking removed.");
        }
        else{
            return sendFailureMessage("Unable to remove this booking.");
        }

    }
    // Check Status Of Appointment Payment Status
    public function isPaid($id)
    {
            $booking= AmbulanceBooking::where('id',$id)
                ->where('patient_id',auth()->user()->id)
                ->first();
            if($booking=='')
            {
                return sendFailureMessage("Booking data not accessible.");
            }
            else{
                $detail['is_paid']= $booking->is_paid;
                return sendSuccessResponse("",$detail);
            }

    }
}
