<?php

namespace App\Http\Controllers\Api\V1\Patient;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use App\Models\WalletTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class WalletController extends Controller
{
    private $config;

    public function __construct()
    {
        $setting = Setting::all();
        foreach ($setting as $data)
        $this->config[$data->param] = $data->data;
    }
    public function index(){
        $data['wallet_amount'] = auth()->user()->wallet_amount;
        $data['transaction'] = WalletTransaction::where('user_id',auth()->user()->id)
            ->select('id','transaction_date','transaction_status','transaction_amount','transaction_type')
            ->orderByDesc('id')
            ->get()->toArray();
        return sendSuccessResponse("",$data);
    }
    public function show($id){
        $data['wallet_amount'] = auth()->user()->wallet_amount;
        $data['transaction'] = WalletTransaction::where('id',$id)
                                            ->where('user_id',auth()->user()->id)
                                            ->select('id','transaction_number','transaction_status',
                                                'transaction_date','transaction_response','transaction_amount','referenceId','transaction_type')
                                            ->get();
        if(!empty($data['transaction']))
        {
            return sendSuccessResponse("",$data);
        }
        return sendFailureMessage("No data available.");
    }

    public function addMoney(Request $request){
        if(!$request->has('amount'))
        {
            return redirect()->back()->with('error','Please enter amount you want to add.');
        }
        if(!is_numeric($request->amount))
        {
            return redirect()->back()->with('error','Please enter a valid amount.');
        }
        if($request->amount<=0)
        {
            return redirect()->back()->with('error','Request cann\'t be processable.');
        }
        $create=array();
        $create['transaction_number'] =Str::random(15);
        $create['who_pay'] = $request->getClientIp();
        $create['transaction_amount'] = $request->amount;
        $create['is_paid'] =0;
        $create['user_id'] =auth()->user()->id;
        $coin=WalletTransaction::create($create);
        if($coin)
        {
            $data['url'] = url('omed-api/user/purchase-coin/'.auth()->user()->id.'/'.$coin->id);
            $data['id'] = $coin->id;
            return sendSuccessResponse("",$data);
        }
        else{
            return sendFailureMessage("Unable to proceed further. Try after some time.");
        }
    }

    public function payForCoin($user, $id)
    {
        $coinPurchase= WalletTransaction::where('id',$id)->where('user_id',$user)->where('is_paid',0)->first();
        $user= User::where('id',$user)->first();
        $data=array();
        $data['orderId'] = $coinPurchase->transaction_number;
        $data['customerName']=$user->name;
        $data['customerEmail'] = $user->email!=''?$user->email:$this->config['master_email'];
        $data['customerPhone'] = $user->mobile;
        $data['orderAmount'] = $coinPurchase->transaction_amount;
        $data['notify'] = '/omed-api/user/coin-purchase-payment/notify';
        $data['return'] = '/omed-api/user/coin-purchase-payment/return';
        $data['orderNote'] = "OMED Coin Purchase";
        $orderDetails = payNow($data);
        $data['title'] =" Pay Now";
        $data['orderDetails'] =$orderDetails;
        return view('api.checkout',$data);
    }

    public function coinPurchasePaymentResponse(Request $request){
        $detail= $request->all();
        $wallet= WalletTransaction::where('transaction_number',$detail['orderId'])
            ->orderByDesc('created_at')
            ->first();
        if(validateSignature($detail))
        {
            $update=array();
            $update['transaction_status'] =$detail['txStatus'];
            $update['transaction_amount'] = $detail['orderAmount'];
            $update['transaction_date'] =$detail['txTime'];
            $update['transaction_response'] = $detail['txStatus'];
            $update['referenceId'] = $detail['referenceId'];
            $update['transaction_type'] = "credit";
            if($detail['txStatus'] == "SUCCESS" || $detail['txStatus'] == "PENDING")
            {
                $update['is_paid'] = 1;
                if($wallet->update($update))
                {
                    $addAmount= User::where('id',$wallet->user_id)->first();
                    $newAmount = $addAmount->wallet_amount + $detail['orderAmount'];
                    User::where('id',$wallet->user_id)->update(['wallet_amount'=>$newAmount]);
                    $update['title'] = " Payment Response";
                    $update['wallet_id'] = $wallet->id;
                    return view('api.payment_response',$update)->with('success','Payment done successfully.');
                }
                else
                {
                    return view('api.payment_response',$update)->with('error','Unable to proceed');
                }
            }
            else
            {
                $wallet->update($update);
                return view('api.payment_response',$detail)->with('error','Your transaction was not successful.');
            }
        }
        else{
            return view('api.payment_response',$detail)->with('error','Invalid payment signature found.');
        }
    }
    public function notifyPaymentResponse(Request $request){

    }

    public function isPurchased($id)
    {
        $cpur= WalletTransaction::where('id',$id)
            ->where('user_id',auth()->user()->id)
            ->first();
        if($cpur=='')
        {
            return sendFailureMessage("Data Not available.");
        }
        else{
            $detail['is_paid']= (int) $cpur->is_paid;
            return sendSuccessResponse("",$detail);
        }
    }

    public function destroy($id){
        $record =WalletTransaction::where('id',$id)->where('user_id',auth()->user()->id)->first();
        if($record=='')
        {
            return sendFailureMessage("Data not available.");
        }
        if($record->delete())
        {
            return sendSuccessResponse("Data removed.");
        }
        else{
            return sendFailureMessage("Something went wrong.");
        }
    }
}
