<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'nullable|string|email|max:255|unique:users',
            'mobile' => ['nullable', 'numeric', 'unique:users', 'digits:10'],
            'user_role_id' => ['required', 'numeric'],
            'gender' => ['required','string','in:Male,Female'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        if(empty($request->mobile) && empty($request->email))
        {
            return redirect()->back()->with('error','Please enter either mobile no. or an valid email id.');
        }
        $user = User::create([

            'name' => $request->name,
            'email' => $request->email,
            'user_role_id' =>$request->user_role_id,
            'mobile' => $request->mobile,
            'password' => Hash::make($request->password),
        ]);
        event(new Registered($user));
        Auth::login($user);
        if(auth()->user()->user_role_id == 3)
        {
            $puid= "OMED".date('Y').auth()->user()->id;
            $patient['user_id'] = auth()->user()->id;
            $patient['gender'] = $request->gender;
            if(Profile::create($patient))
            {
                User::where('id',auth()->user()->id)->update(['approval'=>1,'puid'=>$puid]);
                return redirect('patient/dashboard');
            }

        }
        if(auth()->user()->user_role_id == 11)
        {
            $provider['user_id'] = auth()->user()->id;
            $provider['gender'] = $request->gender;
            if(Profile::create($provider))
            {
                return redirect('/dashboard');
            }
        }

//        event(new Registered($user));
//
//        Auth::login($user);
//
//        return redirect(RouteServiceProvider::HOME);
    }
}
