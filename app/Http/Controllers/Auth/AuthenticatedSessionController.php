<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $data['title'] = ' Login';
        return view('auth.login',$data);
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $request->authenticate();

        $request->session()->regenerate();
        $user=auth()->user();
//        $user->last_login_at =Carbon::now()->toDateTimeString();
//            $user->last_login_ip = $request->getClientIp();
//        $user->save();
        if(auth()->user()->user_role_id==3)
        {
            Session::put('user_email',$user->email);
            Session::put('user_id',$user->id);
            Session::put('user_name',$user->name);
            return redirect()->intended('/');
        }
        elseif(auth()->user()->user_role_id==11) {
//            Session::put('user_is',$user->user_role->name);
            Session::put('user_email',$user->email);
            Session::put('user_id',$user->id);
            Session::put('user_name',$user->name);
            return redirect()->intended('/dashboard');
        }
        else {
            Auth::guard('web')->logout();

            $request->session()->invalidate();

            $request->session()->regenerateToken();

            return redirect('/');
        }
//        return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
