<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LiveConsultationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' =>$this->id,
            $this->mergeWhen(auth()->user()->user_role_id==3,[
                'provider_id' => $this->provider_id,
                'provider' => $this->provider->name,
                'photo' => $this->provider->profile->photo!=''?asset('uploads/profile/'.$this->provider->profile->photo):'',
                'con_link' => $this->con_link_access==1 ?url('omed-api/patient-rtc/'.auth()->user()->id.'/'.$this->id):'',
                $this->mergeWhen((int) $this->request_status < 3,[
                    'wallet_amount' => $this->patient->wallet_amount,
                    'consultation_fee' => $this->serviceProfile->min_fee,
                ]),
            ]),
            $this->mergeWhen(auth()->user()->user_role_id==11,[
                'booked_by' => $this->patient->name,
                'patient_id' => $this->patient_id,
                'photo' => $this->patient->profile()->exists() && $this->patient->profile->photo!=''?asset('uploads/profile/'.$this->patient->profile->photo):'',
                'con_link' => $this->con_link_access==1 ?url('omed-api/provider-rtc/'.auth()->user()->id.'/'.$this->id):'',
            ]),
            'provider_id' => $this->provider_id,
            'provider' => $this->provider->name,
            'service_profile_id' => $this->service_profile_id,
            'patient_name' => $this->patient_name,
            'age' => $this->age,
            'gender' => $this->gender,
            'omed_code' => $this->omed_code!='' ? $this->omed_code:'',
            'rating' => $this->rating!=''?$this->rating:'',
            'review' => $this->review!=''?$this->review:'',
            'booking_date' => date('d-m-Y',strtotime($this->booking_date)),
            'requested_on' => date('d-m-Y h:i:s a',strtotime($this->created_at)),
            'request_status' => (int) $this->request_status,
            'con_link_access' => (int) $this->con_link_access,

            'caller' => $this->caller!=''?$this->caller:'',
//            'con_link' => $this->con_link_access==1 && $this->con_link!=''?url('preview-consultation/'.$this->id):'',
        ];
    }
}
