<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LiveConsultationDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       return[
           'id' =>$this->id,
               'provider_id' => $this->provider_id,
               'provider' => $this->provider->name,
               'booked_by' => $this->patient->name,
               'patient_id' => $this->patient_id,
        'provider_detail' => [
            'name' => $this->provider->name,
            'qualification' => $this->provider->profile->qualification,
            'speciality' => $this->provider->profile->speciality,
            'address' => $this->provider->profile->address,
            'reg_no' => $this->serviceProfile->reg_no,
        ],
           'service_profile_id' => $this->service_profile_id,
           'patient_name' => $this->patient_name,
           'age' => $this->age,
           'gender' => $this->gender,
           'mobile' => $this->mobile!=''?$this->mobile:'',
           'omed_code' => $this->omed_code!=''?$this->omed_code:'',
           'bike_dr' => $this->bike_dr!=''?$this->bike_dr:'',
            'address' => $this->address!=''?$this->address:'',
           'pulse' => $this->pulse!=''?$this->pulse:'',
           'temperature' => $this->temperature!=''?$this->temperature:'',
           'spo2' => $this->spo!=''?$this->spo:'',
           'bp' => $this->bp!=''?$this->bp:'',
           'weight' => $this->weight!=''?$this->weight:'',
           'history' => $this->history!=''?$this->history:'',
           'complain' => $this->complain!=''?$this->complain:'',
           'diagnostic' =>$this->diagnostic!=''?$this->diagnostic:'',
           'prescription' => $this->prescription!=''?$this->prescription:'',
            'prescription_image' => $this->prescription_folder!='' && $this->prescription_image!=''?asset('uploads/patientprofile/prescription/'.$this->prescription_folder.'/'.$this->prescription_image):'',
           'rating' => $this->rating!=''?$this->rating:'',
           'review' => $this->review!=''?$this->review:'',
           'booking_date' => date('d-m-Y',strtotime($this->booking_date)),
           'requested_on' => date('d-m-Y',strtotime($this->created_at)),
           'con_link_access' => (int) $this->con_link_access,
           'con_name' => $this->con_name!=''?$this->con_name:'',
           'caller' => $this->caller!=''?$this->caller:'',
           'request_status' => (int) $this->request_status,
           $this->mergeWhen(auth()->user()->user_role_id==3,[
               'con_link' => $this->con_link_access==1 ?url('omed-api/patient-rtc/'.auth()->user()->id.'/'.$this->id):'',
               $this->mergeWhen((int) $this->request_status < 3,[
                   'wallet_amount' => $this->patient->wallet_amount,
                   'consultation_fee' => $this->serviceProfile->min_fee,
               ]),
           ]),
           $this->mergeWhen(auth()->user()->user_role_id==11,[
               'con_link' => $this->con_link_access==1 ?url('omed-api/provider-rtc/'.auth()->user()->id.'/'.$this->id):'',
           ]),
       ];
    }
}
