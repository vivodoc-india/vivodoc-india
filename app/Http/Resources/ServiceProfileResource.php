<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public static function additionalRequest(array $data)
    {
return $data;
    }

    public function toArray($request)
    {
        $categories = $this->whenLoaded('categories');
        $documents = $this->whenLoaded('documents');
        $user = $this->whenLoaded('user');
        return [
            'service_profile_id' => $this->id,
            'user_role_id' =>$this->user_role_id,
            'profile_name' => $this->user_role->name,
            'icon' => asset('web/img/'.$this->user_role->image),
            $this->mergeWhen($this->whenLoaded('categories'),[
                'reg_no' => $this->reg_no,
                $this->mergeWhen($this->user_role_id==6,[
                 'vehicle_no' => $this->vehicle_no,
                  'vehicle_model' => $this->vehicle_model,
                ]),
                'description' => $this->description,
                'image1' => $this->image1!=''?asset($this->image1):'',
                'image2' => $this->image2!=''?asset($this->image2):'',
                'image3' => $this->image3!=''?asset($this->image3):'',
                'image4' => $this->image4!=''?asset($this->image4):'',
                'image5' => $this->image5!=''?asset($this->image5):'',
                'landmark' => $this->landmark,
                'area' => $this->area,
                'city' => $this->city,
                'state' => $this->state,
                'address' => $this->address,
                'min_fee' => $this->min_fee,
                'max_fee' => $this->max_fee,
                'live_status' => $this->mark_live,
                'is_top' => $this->mark_top,
                'longitude' => $this->longitude,
                'latitude' => $this->latitude,
                'btn_call' => $this->btn_call,
                'btn_video' => $this->btn_video,
                'btn_enquiry' => $this->btn_enquiry,
                'btn_booknow' => $this->btn_booknow,
            ]),
            'categories' =>  CategoryResource::collection($categories),
            'documents' => ServiceDocumentsResource::collection($documents),
            'user'=> UserResource::make($user),
        ];

    }
}
