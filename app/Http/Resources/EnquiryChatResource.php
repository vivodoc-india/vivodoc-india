<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EnquiryChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,
            'sender' => $this->sender,
            'receiver' =>$this->receiver,
            'message' => $this->message,
            'attachment' => !empty($this->attachment)?asset($this->attachment):'',
            'sent_on' => date('d-m-Y h:i:s a',strtotime($this->created_at)),
        ];
    }
}
