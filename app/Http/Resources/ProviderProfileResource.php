<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProviderProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->whenLoaded('user');
        return [
            'service_profile_id' => $this->id,
            'user_role_id' =>$this->user_role_id,
            'profile_name' => $this->user_role->name,
            'icon' => asset('web/img/'.$this->user_role->image),
            $this->mergeWhen($this->whenLoaded('user'),[
                'reg_no' => $this->reg_no,
                $this->mergeWhen($this->user_role_id==6,[
                    'vehicle_no' => $this->vehicle_no,
                ]),
                'area' => $this->area,
                'city' => $this->city,
                'state' => $this->state,
                'min_fee' => $this->min_fee,
                'max_fee' => $this->max_fee,
                'live_status' => $this->mark_live,
                'is_top' => $this->mark_top,
                'longitude' => $this->longitude,
                'latitude' => $this->latitude,
                'btn_call' => $this->btn_call,
                'btn_video' => $this->btn_video,
                'btn_enquiry' => $this->btn_enquiry,
                'btn_booknow' => $this->btn_booknow,
                'is_available' => $this->is_available,

            ]),
            'user'=> UserResource::make($user),

        ];
    }
}
