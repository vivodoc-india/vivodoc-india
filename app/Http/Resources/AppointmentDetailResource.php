<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            $this->mergeWhen(auth()->user()->user_role_id==3,[
                'provider_id' => $this->provider_id,
                'provider' => $this->provider->name,
                'btn_pay' => strtotime($this->booking_date) - strtotime(date('Y-m-d')) >= 0 && $this->is_paid==0? 1:0,
                'is_paid' => (int) $this->is_paid,
                'url' => url('omed-api/user/pay-appointment-fee/'.auth()->user()->id.'/'.$this->id),
            ]),
            $this->mergeWhen(auth()->user()->user_role_id==11,[
                'booked_by' => $this->patient->name,
                'patient_id' => $this->patient_id,
            ]),
            'provider_detail' => [
                'name' => $this->provider->name,
                'qualification' => $this->provider->profile->qualification,
                'speciality' => $this->provider->profile->speciality,
                'address' => $this->provider->profile->address,
                'reg_no' => $this->service_profile->reg_no,
            ],
            'service_profile_id' => $this->service_profile_id,
            'patient_name' => $this->patient_name,
            'age' => $this->age,
            'gender' => $this->gender,
            'mobile' => $this->mobile,
            'address' => $this->address,
            'bp' => $this->bp,
            'pulse' =>$this->pulse,
            'temperature' =>$this->temperature,
            'spo' => $this->spo,
            'weight' => $this->weight,
            'history' => $this->history,
            'complain' => $this->complain,
            'diagnostic'=>$this->diagnostic,
            'prescription' => $this->prescription,
            'prescription_image' =>$this->prescription_folder!='' && $this->prescription_image!=''?asset('uploads/patientprofile/prescription/'.$this->prescription_folder.'/'.$this->prescription_image):'',
            'rating' => $this->rating!=''?$this->rating:'',
            'review' => $this->review!=''?$this->review:'',
            'booking_date' => date('d-m-Y',strtotime($this->booking_date)),
            'requested_on' => date('d-m-Y',strtotime($this->created_at)),
            'booking_no' => $this->booking_number,
            'is_completed' => $this->is_completed,
            'remarks' => $this->remarks,
            'appointment_time' => $this->appointment_time,
        ];
    }
}
