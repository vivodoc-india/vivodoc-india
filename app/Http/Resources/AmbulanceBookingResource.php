<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AmbulanceBookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       return[
           'booking_id'=> $this->id,
            'provider_id'=> $this->provider_id,
            'service_profile_id'=> $this->service_profile_id,
            'patient_name'=> $this->patient_name,
            'age'=> $this->age,
            'address' => $this->address,
           'destination' => $this->destination!=''? $this->destination:'',
            'gender'=> $this->gender,
            'mobile'=> $this->mobile,
            'provider'=> $this->provider->name,
            'is_paid'=> $this->is_paid,
            'is_completed'=> $this->is_completed,
            'is_request_accepted' => $this->is_request_accepted,
            'is_request_rejected' => $this->is_request_rejected,
           'longitude' => $this->longitude,
           'latitude' => $this->latitude,
           'rating' => $this->rating!=''?$this->rating:'',
           'review' => $this->review!=''?$this->review:'',
           'approx_fair' => $this->approx_fair,
           'advance_payment' => $this->advance_payment,
           'booked_date' => date('d-m-Y',strtotime($this->booking_date)),
           'requested_on' =>  date('d-m-Y',strtotime($this->created_at)),
       ];
    }
}
