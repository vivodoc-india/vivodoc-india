<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
//use Illuminate\Http\Resources\Json\ResourceCollection;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->whenLoaded('user');
        return[
          'id'=> $this->id,
          'gender'=> $this->gender,
          'pincode'=> $this->pincode,
          'address'=> $this->address,
          'user' => new UserResource($user),
        ];
    }
}
