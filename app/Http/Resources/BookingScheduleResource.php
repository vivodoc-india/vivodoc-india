<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookingScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $booking_schedules = $this->whenLoaded('booking_schedule');
        return [
         'event_id' => $this->id,
        'title' => $this->title,
        'start_date' => $this->start_date,
        'end_date' => $this->end_date,
        'is_off' => $this->is_off==1?'Schedule for no booking':'Schedule for booking',

            $this->mergeWhen($this->total_bookings!='',[
                'total_bookings' => $this->total_bookings,
                'btn_edit' => strtotime(date('d-m-Y')) <= strtotime($this->end_date)?1:0,
            ]),
            $this->mergeWhen($this->timings!='',[
                'timings' => $this->timings,
            ]),
            $this->mergeWhen($this->created_at!='',[
                'created_at' => date('d-m-Y', strtotime($this->created_at)),
            ]),
            $this->mergeWhen($this->updated_at!='',[
                'updated_at' => date('d-m-Y', strtotime($this->updated_at)),
            ]),
         'scheduled_days' => DaywiseBookingScheduleResource::collection($booking_schedules),
        ];
    }
}
