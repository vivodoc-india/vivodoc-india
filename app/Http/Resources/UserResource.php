<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
//use Illuminate\Http\Resources\Json\ResourceCollection;

class UserResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
          'id' => $this->id,
          'name' =>$this->name,
          'email' => $this->email,
          'mobile' => $this->mobile,
          'photo' => !empty($this->profile->photo)?asset('uploads/profile/'.$this->profile->photo):'',
            'gender' => !empty($this->profile->gender)?$this->profile->gender:'',
            $this->mergeWhen($this->user_role_id==11,[
                'speciality' => !empty($this->profile->speciality)?$this->profile->speciality:'',
                'experiance' => !empty($this->profile->experiance)?$this->profile->experiance:0,
                'qualification' => !empty($this->profile->qualification)?$this->profile->qualification:'',
                'other_speciality' => !empty($this->profile->other_speciality)?$this->profile->other_speciality:'',
            ]),
        ];
    }
}
