<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DaywiseBookingScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'schedule_id' => $this->id,
            'date' => date('d-m-Y',strtotime($this->date)),
            'total_booking' => $this->total_bookings,
            'timings' => $this->timings
        ];
    }
}
