<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MedicalDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id'=>$this->id,
            'file'=>asset($this->file),
            'uploaded_on' =>date('d-m-Y h:i:s a',strtotime($this->updated_at)),
        ];
    }
}
