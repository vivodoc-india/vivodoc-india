<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|string',
                        'general_title' =>'nullable|string',
                        'user_role_id' =>'required|numeric',
                        'image' => 'nullable|image|mimes:jpeg,jpg,png',
                        'status' =>'required|numeric|in:1,0',
                        'is_private' =>'required|numeric|in:1,0',
                        'secret_key' => 'required_if:is_private,1|string',
        ];
    }
}
