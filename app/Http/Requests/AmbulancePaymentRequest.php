<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AmbulancePaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transaction_status' => 'required|string',
            'txn_amount' => 'required_if:transaction_status,SUCCESS',
            'txn_date' => 'required',
            'txn_number' => 'required_if:transaction_status,SUCCESS',
            'referenceId' => 'required_if:transaction_status,SUCCESS|string',
            'payment_mode' => 'required_if:transaction_status,SUCCESS|string',
        ];
    }
}
