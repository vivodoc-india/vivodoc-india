<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileUpdateRequest extends FormRequest
{

    protected $stopOnFirstFailure = true;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'gender' =>'required|string|in:Male,Female',
            'speciality' => auth()->user()->user_role_id==11?'required':''.'|string',
            'pincode' => 'required|numeric|digits:6',
            'qualification' => auth()->user()->user_role_id==11?'required':''.'|string',
            'address' => 'required|string',
            'mobile' =>'nullable|numeric|digits:10|unique:users,mobile,'.auth()->user()->id,
            'email' => 'nullable|email|unique:users,email,'.auth()->user()->id,
            'name' => 'required|string',
            'experiance' => auth()->user()->user_role_id==11?'required':''.'|numeric',
            'bloodgroup' => 'nullable|string',
            'city' => 'nullable|string',
            'state' => 'nullable|string',
            'area' => 'nullable|string',
            'landmark' => 'nullable|string',
        ];
    }
}
