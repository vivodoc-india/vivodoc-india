<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppointmentBookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider_id' => auth()->user()->user_role_id==3?'required':'nullable'.'|numeric',
            'service_profile_id' => 'required|numeric',
            'patient_name' =>'required|string',
            'age' =>'required|string',
            'gender' => 'required|string',
            'booking_date' => auth()->user()->user_role_id==3?'required':'nullable'.'|date',
            'address' => 'required|string',
            'mobile' => 'nullable|numeric',
            'bp' => 'nullable|string',
            'pulse' => 'nullable|string',
            'temperature' => 'nullable|string',
            'spo' => 'nullable|string',
            'weight' =>'nullable|string',
            'history' => 'nullable|string',
            'complain' => 'nullable|string',
            'diagnostic' => 'nullable|string',
            'prescription' => 'nullable|string',
            'prescription_image' => 'nullable|image|mimes:jpg,jpeg,png',
            'remarks' => auth()->user()->user_role_id==11?'required':''.'|string',
        ];
    }
}
