<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceProfileRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules= [
            'reg_no' => 'nullable|string',
            'description' => 'nullable|string',
            'image1' => 'nullable|image|mimes:jpg,jpeg,png,webp|max:10240',
            'image2' => 'nullable|image|mimes:jpg,jpeg,png,webp|max:10240',
            'image3' => 'nullable|image|mimes:jpg,jpeg,png,webp|max:10240',
            'image4' => 'nullable|image|mimes:jpg,jpeg,png,webp|max:10240',
            'image5' => 'nullable|image|mimes:jpg,jpeg,png,webp|max:10240',
            'landmark' => 'nullable|string',
            'area' => 'nullable|string',
            'city' => 'nullable|string',
            'address'=> 'nullable|string',
            'state' => 'nullable|string',
            'min_fee' => 'nullable|numeric',
            'max_fee' => 'nullable|numeric',
            'vehicle_no' => 'nullable|string',
            'vehicle_model' => 'nullable|string',
        ];
        if($this->getMethod()=="PUT" || $this->getMethod()=="PATCH")
        {
            $rules += [ 'user_role_id'=>'nullable|numeric'];
        }
        else{
            $rules += [ 'user_role_id'=>'required|numeric'];
        }
        return $rules;
    }
}
