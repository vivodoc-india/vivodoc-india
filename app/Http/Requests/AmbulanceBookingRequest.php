<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AmbulanceBookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return[
            'patient_name' =>'required|string',
            'age' => 'required|numeric',
            'gender' => 'required|string',
            'address' => 'required|string',
            'mobile' => 'required|numeric|digits:10',
            'longitude' => 'required|string',
            'latitude' => 'required|string',
            'provider_id' => 'required|numeric',
            'service_profile_id' => 'required|numeric',
            'destination' => 'required|string',
        ];
    }
}
