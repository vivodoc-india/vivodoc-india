<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProviderConsultationUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patient_name' =>'required|string',
            'age' => 'required|string',
            'gender' => 'required|string',
            'address' => 'required|string',
            'mobile' => 'nullable|numeric|digits:10',
            'bp' => 'nullable|string',
            'pulse' => 'nullable|string',
            'temperature' => 'nullable|string',
            'spo' => 'nullable|string',
            'weight' =>'nullable|string',
            'history' => 'nullable|string',
            'complain' => 'nullable|string',
            'omed_code' => 'nullable|string',
            'bike_dr' => 'nullable|string',
            'provider_id' => 'required|numeric',
            'service_profile_id' => 'required|numeric',
            'prescription' => 'nullable|string',
            'diagnostic' => 'required|string',
            'prescription_image' => 'nullable|image|mimes:jpg,jpeg,png',
        ];
    }
}
