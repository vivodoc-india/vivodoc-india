<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules= [
            'title' =>'required|string',
            'is_off' => 'required|numeric|in:1,0',
            'total_bookings' => 'required_if:is_off,0|numeric',
            'timings' =>'required_if:is_off,0|array',
            'timings.*' => 'string',
        ];
        if($this->getMethod=="POST")
    {
     $rules += [
         'start_date' => 'required|date',
         'end_date' => 'required|date',
         ];
    }
        else {
            $rules += [
                'start_date' => 'nullable|date',
                'end_date' => 'nullable|date',
            ];
        }
        return $rules;

    }
}
