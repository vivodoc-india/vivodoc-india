<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PatientInfoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'mobile' => 'nullable|numeric|digits:10|unique:users,mobile,'.Auth::user()->id,
            'email' => 'nullable|email|string|unique:users,email,'.Auth::user()->id,
            'gender' =>'required|string',
            'landmark' => 'required|string',
            'area' =>'required|string',
            'city' => 'required|string',
            'state' => 'required|string',
            'pincode' => 'required|numeric|digits:6',
            'address' => 'required|string',
            'blood_group' =>'nullable|string',
        ];
    }
}
