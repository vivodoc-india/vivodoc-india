<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicalDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=[
            'data_type' => 'required|string|in:lab_test,prescription',
            'parent' => 'required|numeric',
            'title' => 'required_if:parent,0|string',
            ];
        if($this->parent > 0)
        {
            $rules +=[
                'file' => 'required|image|mimes:jpg,jpeg,png|max:6150',
            ];
        }
        else{
            $rules +=[
                'file' => 'nullable|image|mimes:jpg,jpeg,png|max:6150',
            ];
        }
        return $rules;
    }
}
