<?php

namespace App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Foundation\Http\FormRequest;

class PersonalInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
           'name' => 'required|string',
           'mobile' => 'nullable|numeric|digits:10|unique:users,mobile,'.Auth::user()->id,
           'email' => 'nullable|email|string|unique:users,email,'.Auth::user()->id,
            'gender' =>'required|string',
            'speciality' => 'required|string',
            'pincode' => 'required|numeric|digits:6',
            'qualification' => 'required',
            'address' => 'required',
        ];
    }
}
