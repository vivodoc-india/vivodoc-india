<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enquiry extends Model
{
    use SoftDeletes;
   protected $guarded=[];

    public function user_sender()
    {
        return $this->belongsTo(User::class,'sender','id');
    }

    public function user_receiver()
    {
        return $this->belongsTo(User::class,'receiver','id');
    }

}
