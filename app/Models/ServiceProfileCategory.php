<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceProfileCategory extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function serviceProfile()
    {
        return $this->belongsTo(ServiceProfile::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function category(){
    return $this->belongsTo(Category::class);
    }
}
