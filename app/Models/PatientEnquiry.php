<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientEnquiry extends Model
{
    protected $guarded=[];
    protected $table='patient_enquiry';

    public function patient()
    {
        return $this->belongsTo(User::class,'patient_id','id');
    }

    public function provider()
    {
        return $this->belongsTo(User::class,'provider_id','id');
    }
}
