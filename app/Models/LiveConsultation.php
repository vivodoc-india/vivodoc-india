<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LiveConsultation extends Model
{
    use HasFactory,SoftDeletes;

    protected $table="live_consultations";
    protected $guarded=[];


    public function patient()
    {
        return $this->belongsTo(User::class, 'patient_id', 'id');
    }

    public function provider()
    {
        return $this->belongsTo(User::class, 'provider_id', 'id');
    }

    public function serviceProfile()
    {
        return $this->belongsTo(ServiceProfile::class,'service_profile_id','id');
    }
}
