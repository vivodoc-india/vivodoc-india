<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceProfile extends Model
{
    use HasFactory,SoftDeletes;

    protected $guarded=[];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function user_role()
    {
        return $this->belongsTo(UserRole::class,'user_role_id');
    }

    public function categories()
    {
        return $this->hasMany(ServiceProfileCategory::class,'service_profile_id');
    }

    public function documents()
    {
        return $this->hasMany(ServiceProfileDocument::class);
    }

    public function topProvider()
    {
        return $this->where('mark_top',1);
    }

    public function online()
    {
        return $this->where('mark_live',1);
    }

    public function liveConsultation()
    {
        return $this->hasMany(LiveConsultation::class,'service_profile_id','id');
    }

}
