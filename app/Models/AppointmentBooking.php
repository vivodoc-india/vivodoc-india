<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppointmentBooking extends Model
{
    use HasFactory,SoftDeletes;
    protected $guarded = [];

    public function patient(){
        return $this->belongsTo(User::class,'patient_id');
    }

    public function provider()
    {
        return $this->belongsTo(User::class, 'provider_id');
    }
    public function service_profile()
    {
        return $this->belongsTo(ServiceProfile::class,'service_profile_id');
    }
}
