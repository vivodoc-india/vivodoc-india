<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnquiryChatNode extends Model
{
    protected $table= "enquiry_chat_nodes";

    protected $guarded = [];
}
