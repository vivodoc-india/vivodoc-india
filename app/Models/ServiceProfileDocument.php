<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceProfileDocument extends Model
{
    use HasFactory,SoftDeletes;
    protected $guarded =[];

    public function serviceProfile()
    {
        return $this->belongsTo(ServiceProfile::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
