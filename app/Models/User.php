<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Authenticatable
{
    use HasApiTokens,HasFactory, Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'name',
//        'email',
//        'mobile',
//        'user_role_id',
//        'password',
//        'device_id',
//    ];
protected $guarded=[];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user_role()
    {
        return $this->hasOne(UserRole::class);
    }
    public function service_profile()
    {
        return $this->hasMany(ServiceProfile::class);
    }
    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function serviceProfileCategories()
    {
        return $this->hasMany(ServiceProfileCategory::class);
    }
    public function serviceProfileDocuments()
    {
        return $this->hasMany(ServiceProfileDocument::class);
    }
    public function bookingSchedule()
    {
        return $this->hasMany(BookingSchedule::class);
    }
    public static function updateUser($user)
    {
        if(Auth::check())
        {
            return User::where('id',auth()->user()->id)->update($user);
        }

    }

    public function patient_consultation()
    {
        return $this->hasMany(LiveConsultation::class,'patient_id','id');
    }


    public function provider_consultation()
    {
        return $this->hasMany(LiveConsultation::class,'provider_id','id');
    }

    public function patient_appointment()
    {
        return $this->hasMany(AppointmentBooking::class,'patient_id');
    }


    public function provider_appointment()
    {
        return $this->hasMany(AppointmentBooking::class,'provider_id');
    }

    public function enquiry_sender()
    {
        return $this->hasMany(Enquiry::class,'sender','id');
    }
    public function enquiry_receiver()
    {
        return $this->hasMany(Enquiry::class,'receiver','id');
    }

    public function wallet_transaction(){
        return $this->hasMany(WalletTransaction::class);
    }
}
