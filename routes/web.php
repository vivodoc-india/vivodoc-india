<?php

use App\Http\Controllers\Admin\AjaxController;
use App\Http\Controllers\Admin\AmbulanceBookingController;
use App\Http\Controllers\Admin\AreaController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ContentController;
use App\Http\Controllers\Admin\DistrictController;
use App\Http\Controllers\Admin\EnquiryController;
use App\Http\Controllers\Admin\GalleryController;
use App\Http\Controllers\Admin\MediaController;
use App\Http\Controllers\Admin\OfflineBookingController;
use App\Http\Controllers\Admin\OnlineBookingController;
//use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\PatientManagementController;
use App\Http\Controllers\Admin\PatientOmedCoinController;
use App\Http\Controllers\Admin\SettingController;
//use App\Http\Controllers\Admin\StateController;
use App\Http\Controllers\Admin\UserManagementController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Web\AmbulanceProviderController;
use App\Http\Controllers\Web\BookingScheduleController;
use App\Http\Controllers\Web\CommonPatientController;
use App\Http\Controllers\Web\CommonProviderController;
use App\Http\Controllers\Web\EventController;
use App\Http\Controllers\Web\LiveBoxController;
use App\Http\Controllers\Web\HomeController;
use App\Http\Controllers\Web\LiveConsultationController;
use App\Http\Controllers\Web\MedicalHistoryController;
use App\Http\Controllers\Web\PageController;
use App\Http\Controllers\Web\PatientAmbulanceBookingController;
use App\Http\Controllers\Web\PatientController;
use App\Http\Controllers\Web\PatientWalletController;
use App\Http\Controllers\Web\ProviderDashboardController;
use App\Http\Controllers\Web\ServiceController;
use App\Http\Controllers\Web\VideoConsultController;
use App\Http\Controllers\Web\WebAjaxController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

require __DIR__.'/auth.php';
Route::get('password-reset',function(){
    return view('auth.passwords.password_reset');
});
Route::get('password/reset-via-otp',function(){
    return view('auth.passwords.reset_via_otp');
});
Route::get('/',[HomeController::class,'index'])->name('home');
Route::post('reset-password-via-otp',[HomeController::class,'passwordResetViaOTP']);
Route::get('register/provider',[HomeController::class,'providerRegistrationView'])->name('register_provider');
Route::get('register/patient',[HomeController::class,'patientRegistrationView'])->name('register_patient');
Route::get('/{page}',PageController::class)->name('page')->where('page','career|faq|support|partner-with-us|terms-services|franchise');
Route::get('aboutus',[HomeController::class,'loadAboutUsView']);
Route::get('contactus',[HomeController::class,'loadContactUsView']);
Route::get('lab-services',[HomeController::class,'labServices']);
Route::get('medical-product',[HomeController::class,'medicalProduct']);
Route::get('checkup-package',[HomeController::class,'checkPackage']);
Route::get('immunization-services',[HomeController::class,'immunizationServices']);
Route::get('doctor-appointment',[HomeController::class,'doctorAppointment']);
Route::get('gallery',[HomeController::class,'gallery']);
//Prev
Route::get('search/{data?}',[HomeController::class,'loadSearch']);
Route::get('search-provider/{data?}',[HomeController::class,'searchResult'])->name('search-provider');
// End Of Prev
//Updated
Route::get('/provider/search/{data?}',[HomeController::class,'loadSearch'])->name('provider.search');
Route::get('privacy-policy',[HomeController::class,'privacy']);
Route::get('our-products',[HomeController::class,'loadProductView']);
Route::get('guideline',[HomeController::class,'loadGuidelineView']);
Route::post('send-otp',[HomeController::class,'sendOTP']);
Route::post('check-otp',[HomeController::class,'checkOTP']);
Route::get('category/{name}/{id}', [HomeController::class,'category']);
Route::get('service/{role}/{name}/{id?}/{data?}', [HomeController::class,'serviceProviders']);
Route::post('verify-private-member',[HomeController::class,'validatePrivateCategoryMember'])->name('validate-membership');
Route::get('detail/{cat_id}/{category}/{id}',[HomeController::class,'showServiceProviderDetail']);
Route::get('detail/{id}',[HomeController::class,'showServiceProviderDetail']);
//Auth::routes();
// Routes for admin
Route::get('preview-consultation/{id}',[LiveBoxController::class,'previewConsultation']);

Route::group(['prefix'=>'medpro'],function(){
    Route::get('medpro-master-access',[LoginController::class,'showLoginForm'])->name('master-access');
    Route::post('login',[LoginController::class,'login'])->name('admin-login');
});
/*
 *
 * Patient Modules
 *
 */
Route::post('cf-payment/notify',[VideoConsultController::class,'notifyPaymentRequest']);
Route::post('cf-payment/return',[VideoConsultController::class,'returnPaymentResponse']);
Route::post('wl-payment/notify',[PatientWalletController::class,'notifyPaymentRequest']);
Route::post('wl-payment/return',[PatientWalletController::class,'returnPaymentResponse']);
Route::post('cf-appointment/return',[HomeController::class,'returnPaymentResponse']);
Route::post('cf-appointment/notify',[HomeController::class,'notifyPaymentRequest']);
Route::post('cf-ambulance/return',[PatientAmbulanceBookingController::class,'returnPaymentResponse']);
Route::post('cf-ambulance/notify',[PatientAmbulanceBookingController::class,'notifyPaymentRequest']);
//Route::post('consultation-request',[VideoConsultController::class,'sendConsultationRequest']);

Route::group(['prefix'=>'patient','middleware'=>['auth','patient']],function(){
    Route::get('login',[PatientController::class,'loadLoginView']);
    Route::post('changePassword',[CommonPatientController::class,'changePatientPassword']);
    Route::post('login',[PatientController::class,'login']);
    Route::get('register',[PatientController::class,'loadRegistationView']);
    Route::post('register',[PatientController::class,'registerPatient']);
    Route::get('dashboard',[PatientController::class,'dashboard'])->name('patient.dashboard');
    Route::get('update-profile',[PatientController::class,'updateProfile']);
    Route::post('updateProfileInfo',[PatientController::class,'personalInfoUpdate']);
    Route::post('updateDocuments',[PatientController::class,'documentUpdate']);
    Route::get('enquiries',[CommonPatientController::class,'myEnquiries']);
    Route::get('reviews-ratings',[CommonPatientController::class,'myReviewRating']);
    Route::get('ask-enquiry/{id}',[CommonPatientController::class,'loadReplyView']);
    Route::post('ask-new-enquiry',[CommonPatientController::class,'savePatientEnquiry']);
    Route::post('remove-medical-data',[MedicalHistoryController::class,'removeData']);
    Route::post('add-more-records',[MedicalHistoryController::class,'addMoreData']);
    Route::resource('medical-data',MedicalHistoryController::class);
    Route::get('book-appointment/{id}',[CommonPatientController::class,'getAppointmentBookingView']);
    Route::post('save-appointment',[CommonPatientController::class,'saveAppointmentBookingRequest']);
    Route::get('appoinment-booking',[CommonPatientController::class,'myAppoinmentBookings']);
    Route::get('consult-now/{id}',[VideoConsultController::class,'loadPatientView']);
    Route::post('consultation-request',[VideoConsultController::class,'sendConsultationRequest']);
    Route::get('review-consultation-request/{id}',[VideoConsultController::class,'editConsultationView']);
    Route::post('initiate-call',[VideoConsultController::class,'initiateCallByPatient']);
    Route::post('hang-call',[VideoConsultController::class,'removeCallByPatient']);
    Route::get('consultation-booking',[VideoConsultController::class,'getConsultationList']);
    Route::get('view-consultation/{id}',[VideoConsultController::class,'getConsultationResult']);
    Route::get('print/prescription/{id}',[VideoConsultController::class,'printPrescription']);
    Route::get('view-appointment/{id}',[CommonPatientController::class,'getAppointmentDetail']);
    Route::get('print/appointment-prescription/{id}',[CommonPatientController::class,'printAppointmentPrescription']);
    Route::post('update-appointment-reviews',[CommonPatientController::class,'updateAppointmentReview']);
    Route::post('update-consultation-reviews',[CommonPatientController::class,'updateConsultationReview']);
    Route::get('pay-appointment-fee/{id}',[HomeController::class,'appointmentFeePayment']);
    Route::post('pay-consultation-fee',[VideoConsultController::class,'payConsultationFee']);
    Route::post('checkAmbulanceBookingResponse',[PatientAmbulanceBookingController::class,'checkStatus']);
    Route::post('pay-ambulance-fee',[PatientAmbulanceBookingController::class,'payBookingFee']);
    Route::get('book-ambulance/{id}',[PatientAmbulanceBookingController::class,'create']);
    Route::get('book-ambulance/{id}/edit',[PatientAmbulanceBookingController::class,'edit']);
    Route::resource('ambulance-bookings',PatientAmbulanceBookingController::class);
//    Route::get('book-ambulance/{id}',[PatientAmbulanceBookingController::class,'create']);
//    Route::get('book-ambulance/{id}/edit',[PatientAmbulanceBookingController::class,'edit']);
    Route::get('my-wallet',[PatientWalletController::class,'index'])->name('patient.wallet');
    Route::get('wallet-transaction-detail/{id}',[PatientWalletController::class,'show'])->name('patient.wallet-detail');
    Route::post('recharge-wallet',[PatientWalletController::class,'addMoney'])->name('patient.wallet-recharge');
});

/*
 * End Of Patient Module
 */

/*
 *Provider module
 */

Route::group(['middleware'=>'auth','provider'],function(){
    Route::get('dashboard',[ProviderDashboardController::class,'index'])->name('provider.dashboard');
    Route::get('update-profile',[ProviderDashboardController::class,'updateProfile']);
    Route::post('updatePersonalInfo',[ProviderDashboardController::class,'personalInfoUpdate']);
//Route::post('updateServicesInfo','Web\DashboardController::class,'serviceInfoUpdate')->middleware('auth');
    Route::post('updateDocuments',[ProviderDashboardController::class,'documentUpdate']);
    Route::resource('services',ServiceController::class);
    Route::post('services-images',[ServiceController::class,'saveServicesImages']);
    Route::get('enquiries',[CommonProviderController::class,'myEnquiries']);
    Route::get('reviews-ratings',[CommonProviderController::class,'myReviewRating']);
    Route::get('reply-enquiry/{id}',[CommonProviderController::class,'loadReplyView']);
    Route::post('save-reply',[CommonProviderController::class,'savePatientEnquiryReply']);
    Route::get('event/manage',[EventController::class,'index']);
    Route::get('event/update-event/{id}',[EventController::class,'editEvent']);
    Route::post('event/update',[EventController::class,'update']);
    Route::get('event/add',[EventController::class,'createEvent']);
    Route::post('event/add',[EventController::class,'store']);
    Route::get('event',[EventController::class,'calender']);
    Route::delete('event/remove',[EventController::class,'destroy']);
    Route::get('schedule-by-day/{id}',[EventController::class,'getScheduleByDate']);
    Route::post('change-live-status',[ProviderDashboardController::class,'changeLiveStatus']);
    Route::post('consultation-response',[LiveConsultationController::class,'saveResponse']);
    Route::get('medical-data/{id}',[LiveConsultationController::class,'getAllMedicalDataList']);
    Route::get('consultation-data/{id}',[LiveConsultationController::class,'getAllLiveConsultationDataList']);
    Route::get('appointment-data/{id}',[LiveConsultationController::class,'getAllAppointmentDataList']);
    Route::get('view-medical-data/{id}',[LiveConsultationController::class,'getMedicalDataDetail']);
    Route::get('view-live-consultation-data/{id}',[LiveConsultationController::class,'getLiveConsultationDataDetail']);
    Route::get('view-appointment-booking-data/{id}',[LiveConsultationController::class,'getAppointmentDataDetail']);
    Route::post('checkPaymentStatus',[LiveConsultationController::class,'isPaid']);
    Route::post('initiate-consultation-call',[LiveConsultationController::class,'initiateCall']);
    Route::post('hang-call',[LiveConsultationController::class,'removeCall']);
    Route::resource('live-consultation',LiveConsultationController::class);
    Route::post('changePassword',[CommonProviderController::class,'changeProviderPassword']);
    Route::post('submitRequestResponse',[AmbulanceProviderController::class,'updateRequestResponse']);
    Route::resource('ambulance-bookings',AmbulanceProviderController::class);
    Route::get('booking/get-bookings',[BookingScheduleController::class,'getMyBooking']);
    Route::get('booking/edit/{id}',[BookingScheduleController::class,'edit']);
    Route::post('booking/update',[BookingScheduleController::class,'update']);
    Route::delete('booking/remove',[BookingScheduleController::class,'destroy']);
    Route::get('booking/edit-appointment/{id}',[BookingScheduleController::class,'editAppointmentDetail']);
    Route::post('appointment-update',[BookingScheduleController::class,'updateAppointment']);
    Route::post('update-appointment-time',[BookingScheduleController::class,'updateAppointmentTime']);
});

/*
 * End Of Provider Module
 */
Route::group(['prefix' => 'omed','middleware'=>'admin'],function(){
    Route::get('dashboard',[DashboardController::class,'index']);
    Route::post('logout',[LoginController::class,'logout']);
    Route::resource('category',CategoryController::class);
    Route::resource('user-management',UserManagementController::class);
    Route::get('update-provider-profile/{id}',[UserManagementController::class,'loadUpdateProfile']);
    Route::get('patient/online-bookings/{id}',[PatientManagementController::class,'getAllOnlineBookings',]);
    Route::get('patient/omed-coins/{id}',[PatientOmedCoinController::class,'index',]);
    Route::get('patient-management/show-consultation-booking/{id}',[PatientManagementController::class,'getOnlineBookingDetails',]);
    Route::post('patient-management/remove-consultation-booking',[PatientManagementController::class,'removeOnlineBooking',]);
    Route::post('patient-management/remove-directory',[PatientManagementController::class,'removeMedicalDirectory']);
    Route::resource('gallery',GalleryController::class);
//    Route::resource('pages',PageController::class);
    Route::get('show-medical-data/{id}',[PatientManagementController::class,'browsMedicalDirectory']);
    Route::resource('enquiry-management',EnquiryController::class);
    Route::post('addNewCategory',[UserManagementController::class,'storeNewCategory']);
    Route::post('removeFromService',[UserManagementController::class,'serviceRemove']);
    Route::post('manageContactButton',[UserManagementController::class,'manageButtonAccess']);
    Route::post('update-services-images',[UserManagementController::class,'updateServicesImages']);
    Route::resource('media', MediaController::class)->middleware('admin');
    Route::resource('content', ContentController::class)->middleware('admin');
    Route::post('updateProviderServiceInfo',[UserManagementController::class,'updateServiceInfo']);
    Route::post('updateProviderDocuments',[UserManagementController::class,'updateDocument']);
    Route::post('updateProviderServicesInfo',[UserManagementController::class,'updateServiceInfo']);
    Route::post('updateProviderPersonalInfo',[UserManagementController::class,'updatePersonalInfo']);
    Route::get('service-provider/ratings/{id}',[UserManagementController::class,'getAllReviews']);
    Route::get('service-provider/patient-enquiries/{id}',[UserManagementController::class,'getAllEnquiries']);
    Route::get('service-provider/offline-bookings/{id}',[UserManagementController::class,'getAllOfflineBookings',]);
    Route::get('service-provider/online-bookings/{id}',[UserManagementController::class,'getAllOnlineBookings',]);
    Route::get('user-management/show-booking/{id}',[UserManagementController::class,'getBookingDetails',]);
    Route::post('user-management/remove-booking',[UserManagementController::class,'removeOfflineBooking',]);
    Route::get('user-management/show-consultation-booking/{id}',[UserManagementController::class,'getOnlineBookingDetails',]);
    Route::post('user-management/remove-consultation-booking',[UserManagementController::class,'removeOnlineBooking',]);
    Route::post('user-management/remove-rating',[UserManagementController::class,'removeReviewRatings']);
    Route::post('user-management/remove-enquiry',[UserManagementController::class,'removeEnquiry']);
    Route::get('user-management/show-enquiry/{id}',[UserManagementController::class,'getEnquiryDetails']);
    Route::post('user-management/admin-reply',[UserManagementController::class,'saveAdminReply']);
    Route::get('view-prescription/{id}',[OnlineBookingController::class,'getPrescription']);
    Route::get('patient/offline-bookings/{id}',[PatientManagementController::class,'getAllOfflineBookings',]);
    Route::get('patient/show-booking/{id}',[PatientManagementController::class,'getBookingDetails',]);
    Route::post('patient/remove-booking',[PatientManagementController::class,'removeOfflineBooking',]);
    Route::resource('patient-management',PatientManagementController::class);
    Route::post('markTopCategory',[UserManagementController::class,'markAsTop']);
//    Route::resource('state',StateController::class);
    Route::resource('district',DistrictController::class);
    Route::resource('area',AreaController::class);
    Route::resource('offline-booking-management',OfflineBookingController::class);
    Route::resource('online-booking-management',OnlineBookingController::class);
    Route::resource('ambulance-booking-management',AmbulanceBookingController::class);
    Route::resource('manage-settings',SettingController::class);
    Route::get('patient/omed-coins/{id}',[PatientOmedCoinController::class,'index'])->name('omed-coins.index');
    Route::get('patient/omed-coins/{id}/show',[PatientOmedCoinController::class,'show'])->name('omed-coins.show');
    Route::post('patient/omed-coins/store',[PatientOmedCoinController::class,'store'])->name('omed-coins.store');
    Route::delete('patient/omed-coins/{id}',[PatientOmedCoinController::class,'destroy'])->name('omed-coins.destroy');
});

Route::get('ajax/change-flag',[AjaxController::class,'changeFlag'])->middleware('admin');
Route::get('ajax/generate-puid',[AjaxController::class,'generatePUID'])->middleware('admin');
Route::post('change-flag',[AjaxController::class,'changeFlag']);
Route::get('ajax/state',[AjaxController::class,'getCity']);
Route::get('ajax/subcategory/{role}',[AjaxController::class,'getSubCategory']);
Route::get('ajax/city',[AjaxController::class,'getArea']);
Route::get('ajax/saveLocation',[WebAjaxController::class,'savePatientLocation']);
Route::get('ajax/check-booking-schedule',[HomeController::class,'checkBookingSchedule']);
