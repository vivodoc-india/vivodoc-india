<?php

use App\Http\Controllers\Api\V1\CommonController;
use App\Http\Controllers\Api\V1\EnquiryController;
use App\Http\Controllers\Api\V1\NotificationController;
use App\Http\Controllers\Api\V1\Patient\AmbulanceBookingController;
use App\Http\Controllers\Api\V1\Patient\AppointmentController;
use App\Http\Controllers\Api\V1\Patient\MedicalDataController;
use App\Http\Controllers\Api\V1\Patient\WalletController;
use App\Http\Controllers\Api\V1\Provider\AppointmentBookingController;
use App\Http\Controllers\Api\V1\Provider\BookingScheduleController;
use App\Http\Controllers\Api\V1\Provider\AmbulanceController;
use App\Http\Controllers\Api\V1\WebRTCController;
use App\Http\Controllers\Api\V2\DefController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Api\V1\DashboardController;
use App\Http\Controllers\Api\V1\ProfileController;
use App\Http\Controllers\Api\V1\Provider\ServiceProfileController;
use App\Http\Controllers\Api\V1\Patient\LiveConsultationController;
use App\Http\Controllers\Api\V1\Provider\ConsultationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return 'Config cache cleared';
});
Route::get('route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return 'Routes cache cleared';
});
// Clear application cache:
Route::get('cache-clear', function() {
    $exitCode = Artisan::call('cache:clear');
    return 'Application cache cleared';
});
Route::post('test-notification',[NotificationController::class,'sendSingleUserNotification']);
Route::get('data-service',[DashboardController::class,'dataService']);
Route::get('data-roll',[DashboardController::class,'dataRoll']);
Route::get('document-roll',[DashboardController::class,'documentRoll']);
Route::get('banner-roll',[DashboardController::class,'bannerRoll']);
Route::get('provider-photo',[DashboardController::class,'providerPhoto']);
Route::get('patient-rtc/{patient}/{booking}',[WebRTCController::class,'patientWebrtc']);
Route::get('provider-rtc/{provider}/{booking}',[WebRTCController::class,'providerWebrtc']);
Route::post('room-created',[WebRTCController::class,'createRoom']);
Route::post('remove-room',[WebRTCController::class,'removeRoom']);

Route::group(['prefix'=>'user'],function(){
    Route::get('app-version',[DashboardController::class,'getAppVersion' ]);
    Route::post('login', [DashboardController::class, 'login']);
    Route::post('register-user', [DashboardController::class, 'registerUser']);
    Route::post('forget-password',[DashboardController::class,'validateMobile']);
    Route::post('verify-otp',[DashboardController::class,'verifyOTP']);
    Route::post('reset-password',[DashboardController::class,'passwordReset']);
    Route::get('pay-ambulance-fee/{userId}/{bookingId}',[CommonController::class,'initiateAmbulancePayment']);
    Route::post('ambulance-fee/notify',[CommonController::class,'notifyAmbulancePaymentResponse']);
    Route::post('ambulance-fee/return',[CommonController::class,'ambulancePaymentResponse']);
    Route::get('purchase-coin/{user}/{id}',[WalletController::class,'payForCoin']);
    Route::post('coin-purchase-payment/notify',[WalletController::class,'notifyPaymentResponse']);
    Route::post('coin-purchase-payment/return',[WalletController::class,'coinPurchasePaymentResponse']);

   /*
    * Live Consultation Fee Payment
    */
    Route::get('pay-consultation-fee/{userId}/{bookingId}',[CommonController::class,'initiatePayment']);
    Route::post('notify',[CommonController::class,'notifyPaymentResponse']);
    Route::post('return',[CommonController::class,'paymentResponse']);

    /*
     * Appointment Payement
     *
     */
    Route::get('pay-appointment-fee/{userId}/{bookingId}',[CommonController::class,'initiateAppointmentPayment']);
    Route::post('appointment-fee/notify',[CommonController::class,'notifyAppointmentPaymentResponse']);
    Route::post('appointment-fee/return',[CommonController::class,'appointmentPaymentResponse']);

});

Route::group(['prefix' => 'v1','middleware'=>'auth:api'],function() {
    Route::get('get-profile',[ProfileController::class,'getProfile']);
    Route::post('send-otp',[DashboardController::class,'sendOTP']);
    Route::put('change-password',[DashboardController::class,'passwordChange']);
    Route::post('profile-update',[ProfileController::class,'updateProfile']);
    Route::post('profile-photo-update',[ProfileController::class,'updateProfilePhoto']);
    Route::get('get-subcategory/{serviceid}/{role}',[ServiceProfileController::class,'getCategory']);
    Route::post('add-subcategory',[ServiceProfileController::class,'storeSubCategory']);
    Route::post('add-documents',[ServiceProfileController::class,'storeServiceProfileDocuments']);
    Route::post('add-banner-images',[ServiceProfileController::class,'saveBannerImages']);
    Route::post('change-live-status',[ServiceProfileController::class,'updateLiveStatus']);
    Route::resource('service-profile',ServiceProfileController::class);
    route::post('update-daywise-schedule',[BookingScheduleController::class,'updateDaywiseSchedule']);
    Route::resource('booking-schedule',BookingScheduleController::class);
    Route::get('check-payment-status/{id}',[AmbulanceBookingController::class,'isPaid']);
    Route::get('consultation-payment-status/{id}',[LiveConsultationController::class,'isPaid']);
    Route::resource('book-ambulance',AmbulanceBookingController::class);
    Route::resource('ambulance-bookings',AmbulanceController::class);
    Route::post('initiate-consultation',[LiveConsultationController::class,'initiateCallByPatient']);
    Route::delete('hang-call/{id}',[LiveConsultationController::class,'removeCall']);
    Route::resource('live-consultation',LiveConsultationController::class);
    Route::resource('consultation',ConsultationController::class);
    Route::post('consultation/{id}',[ConsultationController::class,'update']);
    Route::post('initiate-call',[ConsultationController::class,'initiateCall']);
    Route::delete('remove-call/{id}',[ConsultationController::class,'removeCall']);

    /*======================
     * Medical Records
     * Patient Module
     * =====================
     */
    Route::post('update-record',[MedicalDataController::class,'updateRecordFile'])->middleware('patient');
    Route::resource('medical-data',MedicalDataController::class)->middleware('patient');
Route::post('save-review-rating',[CommonController::class,'storeReviewRating'])->middleware('patient');
    /* ====================
     * Appointment Booking
     * ====================
     * Patient Module
     */
    Route::get('appointment-list',[AppointmentController::class,'index']);
    Route::post('provider-datewise-schedule',[AppointmentController::class,'providerSchedule']);
    Route::post('send-appointment-request',[AppointmentController::class,'store']);
Route::get('show-appointment/{id}',[AppointmentController::class,'show']);
Route::get('appointment-payment-status/{id}',[AppointmentController::class,'isPaidAppointment']);

    /*
     * Provider Module
     */
    Route::get('appointment-booking-list',[AppointmentBookingController::class,'index']);
    Route::put('allot-appointment-time/{id}',[AppointmentBookingController::class,'setAppointmentTiming']);
    Route::post('update-appointment',[AppointmentBookingController::class,'updateAppointment']);
    Route::get('view-appointment/{id}',[AppointmentBookingController::class,'getAppointmentDetail']);
    /*
     * Patient Medical Data Access
     */
    Route::post('uploaded-medical-data',[ConsultationController::class,'getMedicalData']);
    Route::post('uploaded-medical-data-detail',[ConsultationController::class,'getMedicalDataDetail']);
    Route::post('consultation-data',[ConsultationController::class,'getConsultationData']);
    Route::post('consultation-data-detail',[ConsultationController::class,'getConsultationDataDetail']);
    Route::post('appointment-data',[ConsultationController::class,'getAppointmentData']);
    Route::post('appointment-data-detail',[ConsultationController::class,'getAppointmentDataDetail']);

    /*
     * Enquiry Module
     */
//Route::get('send-notification',[DashboardController::class,'notify']);
    Route::get('users-list',[EnquiryController::class,'uniqueUsers']);
    Route::post('view-chats',[EnquiryController::class,'getChats']);
    Route::post('send-message',[EnquiryController::class,'sendMessage']);

    /*
     * Wallet Module (For Patient)
     */
    Route::group(['middleware'=>'patient'],function(){
        Route::get('get-wallet-transaction',[WalletController::class,'index']);
        Route::get('wallet-transaction-detail/{id}',[WalletController::class,'show']);
        Route::post('purchase-coin',[WalletController::class,'addMoney']);
        Route::get('is-coin-purchase/{id}',[WalletController::class,'isPurchased']);
        Route::delete('remove-transaction/{id}',[WalletController::class,'destroy']);
    });

});

Route::group(['prefix'=>'v1'],function(){
    Route::get('app-banner',[CommonController::class,'banners']);
 Route::get('parent-category',[CommonController::class,'parentCategories']);
 Route::get('sub-category/{id}',[CommonController::class,'subCategories']);
 Route::get('live-doctor',[CommonController::class,'liveDoctors']);
 Route::get('top-providers',[CommonController::class,'topProviders']);
 Route::post('providers-listing',[CommonController::class,'providersListing']);
 Route::post('verify-membership',[CommonController::class,'verifyPrivateMembership']);
 Route::get('get-provider-detail/{id}',[CommonController::class,'getProviderDetail']);
 Route::get('state',[CommonController::class,'getStates']);
 Route::post('city',[CommonController::class,'getCity']);
 Route::post('area',[CommonController::class,'getArea']);
 Route::get('current-queue/{user}/{sp}',[CommonController::class,'currentQueue']);
});

Route::group(['prefix'=>'v2'],function(){
    Route::get('get-provider-detail/{id}',[DefController::class,'getProviderDetail']);
});

